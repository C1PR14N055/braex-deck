#!/usr/bin/env bash

printf "
  _    _          _           _    _   _         _    _    _    _
 |_)  |_)   /\\   |_  \\/  __  | \  |_  /   |/    |_)  |_)  / \\  | \\
 |_)  | \\  /--\\  |_  /\\      |_/  |_  \\_  |\\    |    | \  \\_/  |_/
===================================================================\n"

if [[ -d "dist/braex-deck" ]]; then
  printf "\n>>> Removing old build directory..."
  rm -R dist/braex-deck;
fi

printf "\n>>> Building the desktop app..."
ng build braex-deck-desktop -c overwolf-prod
printf "\n\n>>> Building the in-game app..."
ng build braex-deck-in-game -c overwolf-prod

printf "\n>>> Removing the in-game assets..."
rm -R dist/braex-deck/in-game/assets

printf "\n>>> Moving the desktop assets to the root..."
mv dist/braex-deck/desktop/assets dist/braex-deck

printf "\n>>> Removing store assets..."
rm -Rf dist/braex-deck/store
