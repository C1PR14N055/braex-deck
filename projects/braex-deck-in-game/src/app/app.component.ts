import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import { MapCanvasComponent } from "../../../braex-deck-shared/src/lib/map-canvas/map-canvas/map-canvas.component";
// tslint:disable-next-line:max-line-length
import { PlayerMovementCanvasModule } from "../../../braex-deck-shared/src/lib/map-canvas/canvas-modules/modules/player-movement.canvas-module";
// tslint:disable-next-line:max-line-length
import { RtLocalPlayerMovementService } from "../../../braex-deck-shared/src/lib/map-canvas/services/rt-local-player-movement.service";
import { RtTeamMovementService } from "../../../braex-deck-shared/src/lib/map-canvas/services/rt-team-movement.service";
import { Subscription } from "rxjs";
import { OverwolfGameEventsService } from "../../../braex-deck-shared/src/lib/overwolf/overwolf-game-events.service";
// tslint:disable-next-line:max-line-length
import { MapBackgroundCanvasModule } from "../../../braex-deck-shared/src/lib/map-canvas/canvas-modules/modules/map-background.canvas-module";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
  private _subs: Subscription[] = [];
  @ViewChild(MapCanvasComponent)
  private _mapCanvas: MapCanvasComponent;

  // the unused services are actually necessary so that an instance of the service is created and it's not tree shaked
  constructor(
    private _playerMovementMod: PlayerMovementCanvasModule,
    private _rtLocalPlayerMovement: RtLocalPlayerMovementService,
    private _rtTeamMovement: RtTeamMovementService,
    private _gameEvents: OverwolfGameEventsService,
    private _mapBackground: MapBackgroundCanvasModule
  ) {}

  ngOnInit() {
    let sub = this._gameEvents.currentMap().subscribe(pubgMap => {
      this._mapBackground.currentMap = pubgMap;
    });
    this._subs.push(sub);

    sub = this._gameEvents.onMatchLeft().subscribe(() => {
      this._mapBackground.currentMap = null;
    });
    this._subs.push(sub);
  }

  ngAfterViewInit() {
    this._mapCanvas.start(this._playerMovementMod);
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }
}
