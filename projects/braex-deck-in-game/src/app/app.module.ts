import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { MapCanvasModule } from "../../../braex-deck-shared/src/lib/map-canvas/map-canvas.module";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, MapCanvasModule],
  bootstrap: [AppComponent]
})
export class AppModule {}
