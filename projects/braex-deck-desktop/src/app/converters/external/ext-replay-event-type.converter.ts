import { ExtReplayEventType } from "../../ds/external/ext-replay-event-type.enum";
import { ReplayEventType } from "../../ds/internal/replay-event-type.enum";
import { Converter } from "../../../../../braex-deck-shared/src/lib/converters/converter";
import { EXT_REPLAY_EVENT_TYPE_MAPPING } from "../enum-mapping/ext-replay-event-type.enum-mapping";
import { MappingError } from "../../../../../braex-deck-shared/src/lib/error/mapping.error";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ExtReplayEventTypeConverter
  implements Converter<ExtReplayEventType, ReplayEventType> {
  convert(source: ExtReplayEventType) {
    if (!EXT_REPLAY_EVENT_TYPE_MAPPING.has(source)) {
      throw new MappingError(
        `No mapping found for the ExtReplayEventType \`${source}\``
      );
    }

    return EXT_REPLAY_EVENT_TYPE_MAPPING.get(source);
  }
}
