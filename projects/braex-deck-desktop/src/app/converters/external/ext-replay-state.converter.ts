import { Converter } from "../../../../../braex-deck-shared/src/lib/converters/converter";
import { ExtReplayState } from "../../ds/external/ext-replay-state";
import { ReplayState } from "../../ds/internal/replay-state";
import { ExtReplayEventTypeConverter } from "./ext-replay-event-type.converter";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ExtReplayStateConverter
  implements Converter<ExtReplayState, ReplayState> {
  constructor(private _extReplayEventType: ExtReplayEventTypeConverter) {}

  convert(source: ExtReplayState) {
    return new ReplayState(
      source.owner,
      source.time,
      this._extReplayEventType.convert(source.type)
    );
  }
}
