import { ReplayEventType } from "../../ds/internal/replay-event-type.enum";
import { ExtReplayEventType } from "../../ds/external/ext-replay-event-type.enum";
import { Converter } from "../../../../../braex-deck-shared/src/lib/converters/converter";
import { Injectable } from "@angular/core";
import { EXT_REPLAY_EVENT_TYPE_MAPPING } from "../enum-mapping/ext-replay-event-type.enum-mapping";
import { MappingError } from "../../../../../braex-deck-shared/src/lib/error/mapping.error";

@Injectable({
  providedIn: "root"
})
export class ReplayEventTypeConverter
  implements Converter<ReplayEventType, ExtReplayEventType> {
  convert(source: ReplayEventType) {
    for (const key of Array.from(EXT_REPLAY_EVENT_TYPE_MAPPING.keys())) {
      if (EXT_REPLAY_EVENT_TYPE_MAPPING.get(key) === source) {
        return key;
      }
    }

    throw new MappingError(
      `No mapping found for the ReplayEventType \`${source}\``
    );
  }
}
