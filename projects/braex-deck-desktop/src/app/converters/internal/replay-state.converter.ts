import { Converter } from "../../../../../braex-deck-shared/src/lib/converters/converter";
import { ReplayState } from "../../ds/internal/replay-state";
import { ExtReplayState } from "../../ds/external/ext-replay-state";
import { Injectable } from "@angular/core";
import { ReplayEventTypeConverter } from "./replay-event-type.converter";

@Injectable({
  providedIn: "root"
})
export class ReplayStateConverter
  implements Converter<ReplayState, ExtReplayState> {
  constructor(private _replayEventType: ReplayEventTypeConverter) {}

  convert(source: ReplayState): ExtReplayState {
    return {
      time: source.timestamp,
      owner: source.owner,
      type: this._replayEventType.convert(source.type)
    };
  }
}
