import { ExtReplayEventType } from "../../ds/external/ext-replay-event-type.enum";
import { ReplayEventType } from "../../ds/internal/replay-event-type.enum";

export const EXT_REPLAY_EVENT_TYPE_MAPPING = new Map([
  [ExtReplayEventType.PLAY, ReplayEventType.PLAY],
  [ExtReplayEventType.PAUSE, ReplayEventType.PAUSE],
  [ExtReplayEventType.GOTO, ReplayEventType.GOTO]
]);
