import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import {
  faArrowLeft,
  faBinoculars,
  faCog,
  faHome,
  faPalette,
  faRedoAlt,
  faSearch,
  faSprayCan,
  faTrash,
  faUndoAlt,
  faUsers,
  faWindowMaximize,
  faWindowMinimize,
  faWindowClose
} from "@fortawesome/free-solid-svg-icons";
import { DropdownService } from "../../../braex-deck-shared/src/lib/dropdown/dropdown.service";
// tslint:disable-next-line:max-line-length
import { ColorPickerDropdownComponent } from "./color-picker-dropdown/color-picker-dropdown/color-picker-dropdown.component";
import { ThicknessDropdownComponent } from "./thickness-dropdown/thickness-dropdown/thickness-dropdown.component";
import { DropdownRef } from "../../../braex-deck-shared/src/lib/dropdown/dropdown-ref";
import { BrushConfigService } from "../../../braex-deck-shared/src/lib/map-canvas/services/brush-config.service";
import { Subscription } from "rxjs";
import {
  DEFAULT_TOOL,
  MapToolService
} from "../../../braex-deck-shared/src/lib/map-canvas/services/map-tool.service";
import { MapCanvasTool } from "../../../braex-deck-shared/src/lib/map-canvas/ds/map-canvas-tool.enum";
import { MatDialog, MatSnackBar } from "@angular/material";
import { SettingsDialogComponent } from "./settings-dialog/settings-dialog.component";
import { AppSettingsService } from "../../../braex-deck-shared/src/lib/services/app-settings.service";
import { PubgNicknameDialogComponent } from "./pubg-nickname-dialog/pubg-nickname-dialog.component";
// tslint:disable-next-line:max-line-length
import { SuccessSnackBarComponent } from "../../../braex-deck-shared/src/lib/success-snack-bar/success-snack-bar/success-snack-bar.component";
import { ControlsHelpService } from "./services/controls-help.service";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { QuickAccessMenuService } from "../../../braex-deck-shared/src/lib/quick-access-menu/quick-access-menu.service";
import { InGameMapWindowController } from "./window-controller/in-game-map.window-controller";
import { TeamSessionService } from "../../../braex-deck-shared/src/lib/map-canvas/services/team-session.service";
import { OverwolfGameEventsService } from "../../../braex-deck-shared/src/lib/overwolf/overwolf-game-events.service";
import { NavigationEnd, NavigationStart, Router } from "@angular/router";
import { GamePhase } from "../../../braex-deck-shared/src/lib/ds/game/game-phase.enum";
import { Stack } from "immutable";
import { MapMarkerType } from "../../../braex-deck-shared/src/lib/map-canvas/ds/map-marker-type.enum";
import { MarkerConfigService } from "../../../braex-deck-shared/src/lib/map-canvas/services/marker-config.service";
// tslint:disable-next-line:max-line-length
import { SidebarToggleButtonComponent } from "../../../braex-deck-shared/src/lib/sidebar/sidebar-toggle-button/sidebar-toggle-button.component";
import { DiscordHelpDialogComponent } from "./discord-help-dialog/discord-help-dialog.component";
import { OverwolfCoreService } from "../../../braex-deck-shared/src/lib/overwolf/overwolf-core.service";
import { ChangeHistoryService } from "../../../braex-deck-shared/src/lib/map-canvas/services/change-history.service";
// tslint:disable-next-line:max-line-length
import { BrushLinesCanvasModule } from "../../../braex-deck-shared/src/lib/map-canvas/canvas-modules/modules/brush-lines.canvas-module";
import { WelcomeScreenComponent } from "./welcome-screen/welcome-screen.component";

class ViewContext {
  faUndoAlt = faUndoAlt;
  faRedoAlt = faRedoAlt;
  faPalette = faPalette;
  faTrash = faTrash;
  faSprayCan = faSprayCan;
  faUsers = faUsers;
  faLeftArrow = faArrowLeft;
  faHome = faHome;
  mapCanvasTool = MapCanvasTool;
  faCog = faCog;
  faSearch = faSearch;
  faBinoculars = faBinoculars;
  faQuestionCircle = faQuestionCircle;
  mapMarkerType = MapMarkerType;
  faWindowMaximize = faWindowMaximize;
  faWindowMinimize = faWindowMinimize;
  faWindowClose = faWindowClose;
}

export const FORBIDDEN_BACK_ROUTES = ["/real-time"];

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit, OnDestroy {
  readonly viewContext = new ViewContext();
  private _subs: Subscription[] = [];
  private _brushColorRef: DropdownRef<ColorPickerDropdownComponent>;
  private _brushThicknessRef: DropdownRef<ThicknessDropdownComponent>;
  @ViewChild("brushColorButton", { read: ElementRef })
  private _brushColorButton: ElementRef<HTMLElement>;
  @ViewChild("brushThicknessButton", { read: ElementRef })
  private _brushThicknessButton: ElementRef<HTMLElement>;
  private _markerColorRef: DropdownRef<ColorPickerDropdownComponent>;
  @ViewChild("markerColorButton", { read: ElementRef })
  private _markerColorButton: ElementRef<HTMLElement>;
  @ViewChild("currentLobbyButton")
  private _currentLobbyButton: SidebarToggleButtonComponent;
  private _activeTool = DEFAULT_TOOL;
  private _currentRoute: string | null = null;
  private _routeHistory = Stack<string>();
  private _saveNextRouteInHistory = true;
  private _showLobbySidebar = false;
  private _showGameReviewSidebar = false;
  private _inGame = false;
  @ViewChild("gameReviewSidebar")
  private _gameReviewSidebar: SidebarToggleButtonComponent;

  constructor(
    private _dropdown: DropdownService,
    private _brushConfig: BrushConfigService,
    private _mapToolService: MapToolService,
    private _dialog: MatDialog,
    private _appSettings: AppSettingsService,
    private _snackBar: MatSnackBar,
    private _brushLinesModule: BrushLinesCanvasModule,
    private _controlsHelp: ControlsHelpService,
    private _quickAccess: QuickAccessMenuService,
    private _inGameMapController: InGameMapWindowController,
    private _teamSession: TeamSessionService,
    private _gameEvents: OverwolfGameEventsService,
    private _router: Router,
    private _markerConfig: MarkerConfigService,
    private _overwolfCore: OverwolfCoreService,
    private _changeHistory: ChangeHistoryService,
    private _changeDetector: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this._initToolSelection();
    this._initSessionConnectionNotification();
    this._showStartupPubgNicknameDialog();
    this._initMapAutoSwitch();
    this._initBackButton();
    this._initAutoOpenLobbySidebar();
  }

  private _showStartupPubgNicknameDialog() {
    setTimeout(() => {
      if (!this._appSettings.localPlayerNickname) {
        this._dialog
          .open(PubgNicknameDialogComponent, {
            width: "600px",
            disableClose: true
          })
          .afterClosed()
          .subscribe(() => {
            this._dialog
              .open(WelcomeScreenComponent, { width: "80%" })
              .afterClosed()
              .subscribe(() => {
                this._controlsHelp.open();
              });
          });
      }
    }, 0);
  }

  private _initToolSelection() {
    const sub = this._mapToolService.activeToolChanged().subscribe(tool => {
      this._activeTool = tool;
    });
    this._subs.push(sub);
  }

  private _initSessionConnectionNotification() {
    const sub = this._teamSession.connected().subscribe(() => {
      this._snackBar.openFromComponent(SuccessSnackBarComponent, {
        data: "Connected successfully",
        duration: 2000
      });
    });
    this._subs.push(sub);
  }

  /**
   * Init the auto switching to the real time map when the player starts a game.
   */
  private _initMapAutoSwitch() {
    let sub = this._gameEvents.onPhaseChanged().subscribe(phase => {
      this._showLobbySidebar = phase !== GamePhase.LOBBY;
      if (phase !== GamePhase.LOBBY) {
        if (!this._router.isActive("/real-time", false)) {
          this._router.navigate(["/real-time"]);
        }
      }
      this._changeDetector.detectChanges();
    });
    this._subs.push(sub);

    sub = this._gameEvents.onMatchLeft().subscribe(() => {
      this._router.navigate(["/home"]);
    });
    this._subs.push(sub);

    // TODO(pmo): add a test when pubg is terminated to switch back from the real-time map
    sub = this._overwolfCore.onPubgTerminated().subscribe(() => {
      if (this._router.isActive("/real-time", false)) {
        this._router.navigate(["/home"]);
      }
    });
    this._subs.push(sub);
  }

  private _initBackButton() {
    const sub = this._router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this._saveNextRouteInHistory) {
          if (
            this._currentRoute &&
            !FORBIDDEN_BACK_ROUTES.includes(this._currentRoute) &&
            this._currentRoute !== this._routeHistory.first()
          ) {
            this._routeHistory = this._routeHistory.push(this._currentRoute);
          }
        }

        this._currentRoute = event.url;
        this._saveNextRouteInHistory = true;
      }

      if (event instanceof NavigationEnd) {
        const gameReviewActive = event.url.includes("/game-review");
        if (
          this._showGameReviewSidebar &&
          !gameReviewActive &&
          this._gameReviewSidebar.active
        ) {
          this._gameReviewSidebar.actionToggleActive();
        }

        this._showGameReviewSidebar = gameReviewActive;
        setTimeout(() => {
          this._changeDetector.detectChanges();
        }, 0);
      }
    });
    this._subs.push(sub);
  }

  private _initAutoOpenLobbySidebar() {
    const sub = this._gameEvents.onPhaseChanged().subscribe(phase => {
      if (phase === GamePhase.AIRFIELD) {
        this._currentLobbyButton.actionToggleActive();
      }
    });
    this._subs.push(sub);
  }

  actionOpenBrushColorDropdown() {
    if (this._brushColorRef && this._brushColorRef.visible) {
      return;
    }

    this._brushColorRef = this._dropdown.show(
      ColorPickerDropdownComponent,
      this._brushColorButton.nativeElement,
      "Brush color"
    );
    this._brushColorRef.componentInstance.selectedColor = this._brushConfig.color;
    this._brushColorRef.componentInstance.colorSelected.subscribe(color => {
      this._brushConfig.color = color;
    });
  }

  actionOpenBrushThicknessDropdown() {
    if (this._brushThicknessRef && this._brushThicknessRef.visible) {
      return;
    }

    this._brushThicknessRef = this._dropdown.show(
      ThicknessDropdownComponent,
      this._brushThicknessButton.nativeElement,
      "Brush thickness"
    );
  }

  actionUseMarkerType(markerType: MapMarkerType) {
    this._markerConfig.type = markerType;
  }

  actionOpenMarkerColorDropdown() {
    if (this._markerColorRef && this._markerColorRef.visible) {
      return;
    }

    this._markerColorRef = this._dropdown.show(
      ColorPickerDropdownComponent,
      this._markerColorButton.nativeElement,
      "Marker color"
    );
    this._markerColorRef.componentInstance.selectedColor = this._markerConfig.color;
    this._markerColorRef.componentInstance.colorSelected.subscribe(color => {
      this._markerConfig.color = color;
    });
  }

  actionClearDrawings() {
    this._brushLinesModule.clear();
  }

  actionUndoBrush() {
    this._changeHistory.undo();
  }

  actionRedoBrush() {
    this._changeHistory.redo();
  }

  actionOpenSettingsDialog() {
    this._dialog.open(SettingsDialogComponent, {
      width: "600px"
    });
  }

  actionOpenMapControlsHelp() {
    this._controlsHelp.open();
  }

  actionGoBack() {
    const route = this._routeHistory.first();
    this._routeHistory = this._routeHistory.pop();
    this._saveNextRouteInHistory = false;
    this._router.navigate([route]);
  }

  actionGoHome() {
    return this._router.navigate(["/home"]);
  }

  actionOpenDiscordDialog() {
    this._dialog.open(DiscordHelpDialogComponent, { width: "600px" });
  }

  actionOpenWelcomeDialog() {
    this._dialog.open(WelcomeScreenComponent, { width: "80%" });
  }

  actionWindowMinimize() {
    // TODO:
  }

  actionWindowMaximize() {
    // TODO:
  }

  actionWindowClose() {
    // TODO
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  get activeTool() {
    return this._activeTool;
  }

  get canUndo() {
    return this._brushLinesModule.canUndo;
  }

  get canRedo() {
    return this._brushLinesModule.canRedo;
  }

  get canGoBack() {
    return !this._routeHistory.isEmpty();
  }

  get activeMarkerType() {
    return this._markerConfig.type;
  }

  get showLobbySidebar() {
    return this._showLobbySidebar;
  }

  get showGameReviewSidebar() {
    return this._showGameReviewSidebar;
  }

  get genericMarkerHotkey() {
    return this._appSettings.genericMarkerHotkey;
  }

  get carMarkerHotkey() {
    return this._appSettings.carMarkerHotkey;
  }

  get weaponMarkerHotkey() {
    return this._appSettings.weaponMarkerHotkey;
  }

  get itemMarkerHotkey() {
    return this._appSettings.itemMarkerHotkey;
  }

  get brushColor() {
    return this._brushConfig.color;
  }

  get brushThickness() {
    return this._brushConfig.thickness;
  }

  get markerColor() {
    return this._markerConfig.color;
  }

  get showHome() {
    return !this._router.isActive("/home", true);
  }
}
