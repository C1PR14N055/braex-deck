import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SharedEventMatchSearchComponent } from "./shared-event-match-search.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { MatSnackBarModule } from "@angular/material";

describe("SharedEventMatchSearchComponent", () => {
  let component: SharedEventMatchSearchComponent;
  let fixture: ComponentFixture<SharedEventMatchSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SharedEventMatchSearchComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [HttpClientTestingModule, MatSnackBarModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedEventMatchSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
