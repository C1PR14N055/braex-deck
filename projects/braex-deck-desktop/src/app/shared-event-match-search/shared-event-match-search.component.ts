import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  ViewChild
} from "@angular/core";
import { GameService } from "../../../../braex-deck-shared/src/lib/services/game.service";
import { MatSnackBar } from "@angular/material";
// tslint:disable-next-line:max-line-length
import { SCROLL_THRESHOLD } from "../shared/scroll";
import { EagerGame } from "../../../../braex-deck-shared/src/lib/ds/game/eager-game";
import { BaseEventsGamesSearch } from "../shared/game-search/base-event-games-search";

@Component({
  selector: "app-shared-event-match-search",
  templateUrl: "./shared-event-match-search.component.html",
  styleUrls: [
    "./shared-event-match-search.component.scss",
    "../../../../styles/theme/scroll.scss",
    "../../../../styles/theme/messages.scss"
  ],
  host: { class: "d-flex flex-column flex-grow-1" },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SharedEventMatchSearchComponent extends BaseEventsGamesSearch {
  private _currentEvent: string | null = null;
  @ViewChild("gameEntriesWrapper")
  private _gameEntriesWrapper: ElementRef<HTMLDivElement>;

  constructor(
    gameService: GameService,
    changeDetector: ChangeDetectorRef,
    snackBar: MatSnackBar
  ) {
    super(gameService, snackBar, changeDetector);
  }

  searchEvent(eventId: string) {
    this._currentEvent = eventId;
    this._fetchMatchesOfEvent(eventId, this._gameEntriesWrapper.nativeElement);
  }

  actionGameEntriesScroll() {
    if (this._loading || this._currentPage === this._totalPages - 1) {
      return;
    }

    if (
      this._gameEntriesWrapper.nativeElement.scrollTop +
        this._gameEntriesWrapper.nativeElement.clientHeight >=
      this._gameEntriesWrapper.nativeElement.scrollHeight - SCROLL_THRESHOLD
    ) {
      this._fetchNextPage(
        this._currentEvent,
        this._gameEntriesWrapper.nativeElement
      );
    }
  }

  trackByGameId(index: number, item: EagerGame) {
    return item.info.gameId;
  }

  get currentEvent() {
    return this._currentEvent;
  }
}
