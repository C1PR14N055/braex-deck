import { Component, OnDestroy, OnInit } from "@angular/core";
import { AppSettingsService } from "../../../../braex-deck-shared/src/lib/services/app-settings.service";
import { Subscription } from "rxjs";
import { FormControl, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material";

@Component({
  selector: "app-pubg-nickname-dialog",
  templateUrl: "./pubg-nickname-dialog.component.html",
  styleUrls: ["./pubg-nickname-dialog.component.scss"]
})
export class PubgNicknameDialogComponent implements OnInit, OnDestroy {
  nicknameControl = new FormControl("", Validators.required);
  private _subs: Subscription[] = [];

  constructor(
    private _dialogRef: MatDialogRef<PubgNicknameDialogComponent>,
    private _appSettings: AppSettingsService
  ) {}

  ngOnInit() {
    const sub = this._appSettings
      .localPlayerNicknameChanged()
      .subscribe(nickname => {
        if (nickname) {
          this._dialogRef.close(true);
        }
      });
    this._subs.push(sub);
  }

  actionSave() {
    if (this.nicknameControl.invalid) {
      return;
    }

    this._appSettings.localPlayerNickname = (this.nicknameControl
      .value as string).trim();
    this._dialogRef.close(true);
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }
}
