import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PubgNicknameDialogComponent } from "./pubg-nickname-dialog.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { MatDialogModule, MatDialogRef } from "@angular/material";

describe("PubgNicknameDialogComponent", () => {
  let component: PubgNicknameDialogComponent;
  let fixture: ComponentFixture<PubgNicknameDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PubgNicknameDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [MatDialogModule],
      providers: [{ provide: MatDialogRef, useValue: {} }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubgNicknameDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
