import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from "@angular/core/testing";

import {
  FAST_FORWARD_SIZE,
  ReviewControlsComponent
} from "./review-controls.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { DurationModule } from "../../../../braex-deck-shared/src/lib/duration/duration.module";
import { Subscription } from "rxjs";

describe("ReviewControlsComponent", () => {
  let component: ReviewControlsComponent;
  let fixture: ComponentFixture<ReviewControlsComponent>;
  let subs: Subscription[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReviewControlsComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [DurationModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    subs = [];

    fixture = TestBed.createComponent(ReviewControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should fast forward by the FAST_FORWARD_SIZE", () => {
    const time = 0;
    component.duration = 100;
    component.currentTime = time;
    component.actionFastForward();
    expect(component.currentTime).toEqual(time + FAST_FORWARD_SIZE);
  });

  it("should fast backward by the FAST_FORWARD_SIZE", () => {
    const time = 50;
    component.duration = 100;
    component.currentTime = time;
    component.actionFastBackward();
    expect(component.currentTime).toEqual(time - FAST_FORWARD_SIZE);
  });

  it("should fast forward by the remaining time", () => {
    const duration = 100;
    component.duration = duration;
    component.currentTime = 93;
    component.actionFastForward();
    expect(component.currentTime).toEqual(duration);
  });

  it("should fast backward by the remaining time", () => {
    component.duration = 100;
    component.currentTime = 5;
    component.actionFastBackward();
    expect(component.currentTime).toEqual(0);
  });

  it("should notify all observers that the playback has started", fakeAsync(() => {
    component.currentTime = 23;
    component.duration = 100;
    let received: number;
    const sub = component.play.subscribe(time => {
      received = time;
    });
    subs.push(sub);

    component.actionPlayPause();
    tick(200);

    expect(received).toEqual(component.currentTime);
  }));

  it("should notify all observers that the playback has been paused", fakeAsync(() => {
    component.actionPlayPause();
    expect(component.playbackActive).toBeTruthy();

    component.currentTime = 64;
    component.duration = 100;
    let received: number;
    const sub = component.pause.subscribe(time => {
      received = time;
    });
    subs.push(sub);

    component.actionPlayPause();
    tick(200);

    expect(received).toEqual(component.currentTime);
  }));

  it("should notify all observers that a fast forward has occurred", fakeAsync(() => {
    const initialTime = 64;
    component.currentTime = initialTime;
    component.duration = 100;
    let received: number;
    const sub = component.fastForward.subscribe(time => {
      received = time;
    });
    subs.push(sub);

    component.actionFastForward();
    tick(200);

    expect(received).toEqual(initialTime + FAST_FORWARD_SIZE);
  }));

  it("should notify all observers that a fast backward has occurred", fakeAsync(() => {
    const initialTime = 42;
    component.currentTime = initialTime;
    component.duration = 100;
    let received: number;
    const sub = component.fastBackward.subscribe(time => {
      received = time;
    });
    subs.push(sub);

    component.actionFastBackward();
    tick(200);

    expect(received).toEqual(initialTime - FAST_FORWARD_SIZE);
  }));

  it("should notify all observers when the duration changes", fakeAsync(() => {
    let received: number;
    const sub = component.durationChanged.subscribe(time => {
      received = time;
    });
    subs.push(sub);

    const duration = 72;
    component.duration = duration;
    tick(200);

    expect(received).toEqual(duration);
  }));

  it("should notify all observers when the current time changes", fakeAsync(() => {
    let received: number;
    const sub = component.currentTimeChanged.subscribe(time => {
      received = time;
    });
    subs.push(sub);

    component.duration = 2999;
    const currentTime = 83;
    component.currentTime = currentTime;
    tick(200);

    expect(component.currentTime).toEqual(currentTime);
  }));

  it("should recompute the progress percentage", fakeAsync(() => {
    expect(component.timelineProgressPercent).toEqual(0);
    component.duration = 100;
    component.currentTime = 30;
    tick(200);

    expect(component.timelineProgressPercent).toBeCloseTo(30);
  }));

  afterEach(() => {
    component.ngOnDestroy();
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
