import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from "@angular/core";
import {
  faBackward,
  faForward,
  faPause,
  faPlay
} from "@fortawesome/free-solid-svg-icons";
import { MouseButton } from "../../../../braex-deck-shared/src/lib/ds/mouse-button.enum";
import { Subscription } from "rxjs";
import { AppSettingsService } from "../../../../braex-deck-shared/src/lib/services/app-settings.service";
import { HotkeyService } from "../../../../braex-deck-shared/src/lib/services/hotkey.service";

export const FAST_FORWARD_SIZE = 10;

class ViewContext {
  faPlay = faPlay;
  faBackward = faBackward;
  faForward = faForward;
  faPause = faPause;
}

@Component({
  selector: "app-review-controls",
  templateUrl: "./review-controls.component.html",
  styleUrls: [
    "./review-controls.component.scss",
    "../../../../styles/utils.scss"
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReviewControlsComponent implements OnInit, OnDestroy {
  readonly viewContext = new ViewContext();
  private _timelineProgressPercent = 0;
  @ViewChild("inputOverlay")
  private _inputOverlay: ElementRef<HTMLDivElement>;
  @ViewChild("progressDot")
  private _progressDot: ElementRef<HTMLDivElement>;
  private _currentTime = 0;
  private _duration = 260;
  private _skippingActive = false;
  private _playbackActive = false;
  private _subs: Subscription[] = [];
  /**
   * Event emitted when the playback of the review starts.
   * Emits the time.
   */
  @Output()
  play = new EventEmitter<number>();
  /**
   * Event emitted when the playback of the review is paused.
   * Emits the time.
   */
  @Output()
  pause = new EventEmitter<number>();
  /**
   * Event emitted when the playback of the review is fasted forward.
   * Emits the time AFTER the fast forward.
   */
  @Output()
  fastForward = new EventEmitter<number>();
  /**
   * Event emitted when the playback of the review is fasted backward.
   * Emits the time AFTER the fast backward.
   */
  @Output()
  fastBackward = new EventEmitter<number>();
  @Output()
  durationChanged = new EventEmitter<number>();
  @Output()
  currentTimeChanged = new EventEmitter<number>();
  @Output()
  skippingActiveChanged = new EventEmitter<boolean>();
  @Output()
  skipFinished = new EventEmitter<number>();
  @Output()
  playbackActiveChange = new EventEmitter<boolean>();

  constructor(
    private _renderer: Renderer2,
    private _changeDetector: ChangeDetectorRef,
    private _appSettings: AppSettingsService,
    private _hotkey: HotkeyService
  ) {}

  ngOnInit() {
    this._initPercentReComputation();
    this._initInputOverlayEvents();
    this._initHotkeys();
  }

  private _initPercentReComputation() {
    const sub = this.currentTimeChanged.subscribe(() => {
      this._timelineProgressPercent =
        (100 / this._duration) * this._currentTime;
      this._changeDetector.detectChanges();
    });
    this._subs.push(sub);
  }

  private _initInputOverlayEvents() {
    this._renderer.listen(
      this._inputOverlay.nativeElement,
      "mousedown",
      event => {
        if (event.button === MouseButton.LMB) {
          this._skippingActive = true;
          this.skippingActiveChanged.emit(this._skippingActive);
        }
      }
    );
    this._renderer.listen("document", "mouseup", event => {
      if (!this._skippingActive) {
        return;
      }

      if (event.button === MouseButton.LMB) {
        this._computeTimelinePercentageByPosition(event.pageX);
        this._computeTimestampByPercentage();
        this._skippingActive = false;
        this.skippingActiveChanged.emit(this._skippingActive);
        this.skipFinished.emit(this._duration);
      }
    });
    this._renderer.listen("document", "mousemove", event => {
      if (!this._skippingActive) {
        return;
      }

      this._computeTimelinePercentageByPosition(event.pageX);
      this._computeTimestampByPercentage();
    });
  }

  private _initHotkeys() {
    let playPauseHotkey = this._appSettings.playPauseHotkey;
    this._hotkey.registerHotkey(playPauseHotkey, () => this.actionPlayPause());
    this._appSettings.playPauseHotkeyChanged().subscribe(hotkey => {
      this._hotkey.unregisterHotkey(playPauseHotkey);
      this._hotkey.registerHotkey(hotkey, () => this.actionPlayPause());
      playPauseHotkey = hotkey;
    });

    let fastForwardHotkey = this._appSettings.fastForwardHotkey;
    this._hotkey.registerHotkey(fastForwardHotkey, () =>
      this.actionFastForward()
    );
    this._appSettings.fastForwardHotkeyChanged().subscribe(hotkey => {
      this._hotkey.unregisterHotkey(fastForwardHotkey);
      this._hotkey.registerHotkey(hotkey, () => this.actionFastForward());
      fastForwardHotkey = hotkey;
    });

    let fastBackwardHotkey = this._appSettings.fastBackwardHotkey;
    this._hotkey.registerHotkey(fastBackwardHotkey, () =>
      this.actionFastBackward()
    );
    this._appSettings.fastBackwardHotkeyChanged().subscribe(hotkey => {
      this._hotkey.unregisterHotkey(fastBackwardHotkey);
      this._hotkey.registerHotkey(hotkey, () => this.actionFastBackward());
      fastBackwardHotkey = hotkey;
    });
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  /**
   * Compute the percentage of the progress/playback/how much time has passed in
   * percent depending by the x position of the mouse and the width and position
   * of the timeline.
   * @param x The X position of the mouse.
   */
  private _computeTimelinePercentageByPosition(x: number) {
    const rect = this._inputOverlay.nativeElement.getBoundingClientRect();
    const width = this._inputOverlay.nativeElement.offsetWidth;
    const start = rect.left;
    let result = (100 / width) * (x - start);
    if (result < 0) {
      result = 0;
    }
    if (result > 100) {
      result = 100;
    }

    this._timelineProgressPercent = result;
  }

  private _computeTimestampByPercentage() {
    this.currentTime = (this.duration / 100) * this._timelineProgressPercent;
  }

  actionPlayPause() {
    this._playbackActive = !this._playbackActive;
    this._changeDetector.detectChanges();
    if (this._playbackActive) {
      this.play.emit(this._currentTime);
    } else {
      this.pause.emit(this._currentTime);
    }
  }

  actionFastForward() {
    const newTime = this._currentTime + FAST_FORWARD_SIZE;
    if (newTime <= this._duration) {
      this.currentTime = newTime;
    } else {
      this.currentTime = this._duration;
    }

    this.fastForward.emit(this._currentTime);
  }

  actionFastBackward() {
    const newTime = this._currentTime - FAST_FORWARD_SIZE;
    if (newTime >= 0) {
      this.currentTime = newTime;
    } else {
      this.currentTime = 0;
    }

    this.fastBackward.emit(this._currentTime);
  }

  /**
   * How much percent of the whole review have been watched.
   */
  get timelineProgressPercent() {
    return this._timelineProgressPercent;
  }

  /**
   * The current time of the playback.
   */
  get currentTime() {
    return this._currentTime;
  }

  @Input()
  set currentTime(value: number) {
    const changed = this._currentTime !== value;
    this._currentTime = value;

    if (changed) {
      this.currentTimeChanged.emit(this._currentTime);
    }
  }

  /**
   * The overall duration of the review.
   */
  get duration() {
    return this._duration;
  }

  @Input()
  set duration(value: number) {
    const changed = this._duration !== value;
    this._duration = value;

    if (changed) {
      this.durationChanged.emit(value);
    }
  }

  /**
   * Whether the user is currently manually skipping through the timeline
   * i.e. dragging the progress dot on the timeline.
   */
  get skippingActive() {
    return this._skippingActive;
  }

  /**
   * Whether the playback is currently active.
   */
  get playbackActive() {
    return this._playbackActive;
  }

  @Input()
  set playbackActive(value: boolean) {
    const changed = this._playbackActive !== value;
    this._playbackActive = value;

    if (changed) {
      this._changeDetector.detectChanges();
    }
  }
}
