import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { DropdownService } from "../../../braex-deck-shared/src/lib/dropdown/dropdown.service";
import { OverlayModule } from "@angular/cdk/overlay";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { OverwolfGameEventsService } from "../../../braex-deck-shared/src/lib/overwolf/overwolf-game-events.service";
// tslint:disable-next-line:max-line-length
import { FakeOverwolfGameEventsService } from "../../../braex-deck-shared/src/lib/overwolf/testing/fake-overwolf-game-events.service";
import { BrushConfigService } from "../../../braex-deck-shared/src/lib/map-canvas/services/brush-config.service";
import { PlayerStatsService } from "../../../braex-deck-shared/src/lib/services/player-stats.service";
import { MapToolService } from "../../../braex-deck-shared/src/lib/map-canvas/services/map-tool.service";
import { GameTrackerService } from "../../../braex-deck-shared/src/lib/services/game-tracker.service";
import {
  MatDialogModule,
  MatMenuModule,
  MatSnackBarModule
} from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { PubgNicknameDialogComponent } from "./pubg-nickname-dialog/pubg-nickname-dialog.component";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { QuickAccessMenuModule } from "../../../braex-deck-shared/src/lib/quick-access-menu/quick-access-menu.module";
import { OverwolfCoreService } from "../../../braex-deck-shared/src/lib/overwolf/overwolf-core.service";
// tslint:disable-next-line:max-line-length
import { FakeOverwolfCoreService } from "../../../braex-deck-shared/src/lib/overwolf/testing/fake-overwolf-core.service";
import { Observable, Subscription } from "rxjs";
import { Router } from "@angular/router";
import { GamePhase } from "../../../braex-deck-shared/src/lib/ds/game/game-phase.enum";
import { SidebarModule } from "../../../braex-deck-shared/src/lib/sidebar/sidebar.module";

@NgModule({
  declarations: [PubgNicknameDialogComponent],
  exports: [PubgNicknameDialogComponent],
  imports: [CommonModule, ReactiveFormsModule, MatDialogModule],
  entryComponents: [PubgNicknameDialogComponent]
})
class PubgNicknameDialogModule {}

describe("AppComponent", () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  let fakeGameEventsService: FakeOverwolfGameEventsService;
  let fakeOverwolfCore: FakeOverwolfCoreService;
  let subs: Subscription[];
  let fakeRouter: any;

  beforeEach(async(() => {
    fakeRouter = {
      navigate: jasmine.createSpy("navigate"),
      isActive: jasmine.createSpy("isActive"),
      events: Observable.create()
    };

    TestBed.configureTestingModule({
      declarations: [AppComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: Router,
          useValue: fakeRouter
        },
        DropdownService,
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        },
        BrushConfigService,
        PlayerStatsService,
        MapToolService,
        { provide: GameTrackerService, useValue: {} },
        {
          provide: OverwolfCoreService,
          useClass: FakeOverwolfCoreService
        }
      ],
      imports: [
        OverlayModule,
        HttpClientTestingModule,
        MatDialogModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        PubgNicknameDialogModule,
        MatMenuModule,
        QuickAccessMenuModule,
        SidebarModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fakeGameEventsService = TestBed.get(OverwolfGameEventsService);
    fakeOverwolfCore = TestBed.get(OverwolfCoreService);
  }));

  beforeEach(() => {
    subs = [];
    component.ngOnInit();
  });

  it("should create the app", () => {
    expect(component).toBeTruthy();
  });

  it("should navigate to the real time map when the game phase is not lobby", fakeAsync(() => {
    fakeRouter.isActive.and.returnValue(false);
    fakeGameEventsService.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(400);

    expect(fakeRouter.navigate).toHaveBeenCalledWith(["/real-time"]);
  }));

  it("should NOT navigate to the real time map when the lobby phase is received", fakeAsync(() => {
    fakeRouter.isActive.and.returnValue(false);
    fakeGameEventsService.triggerPhaseChanged(GamePhase.LOBBY);
    tick(400);

    expect(fakeRouter.navigate).not.toHaveBeenCalled();
  }));

  it("should NOT navigate each time the phase changes", fakeAsync(() => {
    fakeRouter.isActive.and.returnValue(false);
    fakeGameEventsService.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(400);

    expect(fakeRouter.navigate).toHaveBeenCalledTimes(1);
    fakeRouter.isActive.and.returnValue(true);
    fakeGameEventsService.triggerPhaseChanged(GamePhase.AIRCRAFT);
    tick(400);

    expect(fakeRouter.navigate).toHaveBeenCalledTimes(1);
    fakeGameEventsService.triggerPhaseChanged(GamePhase.FREE_FLY);
    tick(400);

    expect(fakeRouter.navigate).toHaveBeenCalledTimes(1);
  }));

  it("should navigate to the root when the match ends", fakeAsync(() => {
    fakeRouter.isActive.and.returnValue(false);
    fakeGameEventsService.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(400);

    fakeGameEventsService.triggerPhaseChanged(GamePhase.LOBBY);
    tick(400);
    expect(fakeRouter.navigate).toHaveBeenCalledWith(["/"]);
  }));

  afterEach(() => {
    component.ngOnDestroy();
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
