import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ReviewTeamEntryComponent } from "./review-team-entry.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("ReviewTeamEntryComponent", () => {
  let component: ReviewTeamEntryComponent;
  let fixture: ComponentFixture<ReviewTeamEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReviewTeamEntryComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewTeamEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
