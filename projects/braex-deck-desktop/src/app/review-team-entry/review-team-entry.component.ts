import { Component, OnDestroy, OnInit } from "@angular/core";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
// tslint:disable-next-line:max-line-length
import { TelemetryActiveFeedService } from "../../../../braex-deck-shared/src/lib/map-canvas/services/telemetry-active-feed.service";
// tslint:disable-next-line:max-line-length
import { TelemetryCharacter } from "../../../../braex-deck-shared/src/lib/map-canvas/ds/telemetry/objects/telemetry-character";
import { Subscription } from "rxjs";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-review-team-entry",
  templateUrl: "./review-team-entry.component.html",
  styleUrls: [
    "./review-team-entry.component.scss",
    "../../../../styles/theme/messages.scss"
  ]
})
export class ReviewTeamEntryComponent implements OnInit, OnDestroy {
  private _subs: Subscription[] = [];
  teams: TelemetryCharacter[] = [];

  constructor(
    private _telemetryFeedPlayers: TelemetryActiveFeedService,
    private _route: ActivatedRoute
  ) {
    const sub = this._route.params.subscribe(params => {
      if (params && params["gameId"]) {
        this.teams = [];
      }
    });
    this._subs.push(sub);
  }

  ngOnInit() {
    const sub = TelemetryActiveFeedService.getCharacters().subscribe(
      (characters: TelemetryCharacter[]) => {
        this.teams = characters.sort((a, b) => {
          return a.ranking - b.ranking;
        });
      }
    );
    this._subs.push(sub);
  }

  trackByCharName(index: number, item: TelemetryCharacter) {
    return item.name;
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }
}
