import { PubgMap } from "../../../../braex-deck-shared/src/lib/ds/game/pubg-map.enum";

/**
 * An array of maps which are unsupported for the game review and for the real time map.
 */
export const UNSUPPORTED_MAPS = [PubgMap.RANGE_MAIN];
