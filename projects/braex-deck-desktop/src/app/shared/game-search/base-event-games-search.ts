import { GameService } from "../../../../../braex-deck-shared/src/lib/services/game.service";
import { ChangeDetectorRef } from "@angular/core";
import { MatSnackBar } from "@angular/material";
import { List } from "immutable";
import { PagingOptions } from "../../../../../braex-deck-shared/src/lib/ds/paging-options";
// tslint:disable-next-line:max-line-length
import { ErrorSnackBarComponent } from "../../../../../braex-deck-shared/src/lib/error-snack-bar/error-snack-bar/error-snack-bar.component";
import { EagerGame } from "../../../../../braex-deck-shared/src/lib/ds/game/eager-game";
import { GameRoster } from "../../../../../braex-deck-shared/src/lib/ds/game/game-roster";
import { GameParticipant } from "../../../../../braex-deck-shared/src/lib/ds/game/game-participant";
import { EventNotInDbError } from "../../../../../braex-deck-shared/src/lib/error/event-not-in-db.error";

export const EVENT_NOT_IN_DB_TIMEOUT = 20;

export class BaseEventsGamesSearch {
  protected _loading = false;
  protected _eventInDb = true;
  protected _games = List<EagerGame>();
  protected _totalGames: number | null = null;
  protected _currentPage = 0;
  protected _totalPages = 0;
  protected _refreshInterval: number | null = null;
  /**
   * The time in seconds until the search is automatically re-triggered
   * when the player is not in the db.
   */
  protected _timeUntilRefresh: number | null = null;

  constructor(
    protected _gameService: GameService,
    protected _snackBar: MatSnackBar,
    protected _changeDetector: ChangeDetectorRef
  ) {}

  protected _fetchMatchesOfEvent(
    eventId: string,
    scrollContainer: HTMLElement,
    options?: PagingOptions
  ) {
    if (this._refreshInterval) {
      window.clearInterval(this._refreshInterval);
      this._refreshInterval = null;
    }

    this._timeUntilRefresh = null;
    scrollContainer.scrollTop = 0;
    this._loading = true;
    this._changeDetector.detectChanges();

    // first get all games of the given player
    this._gameService.getGamesOfEvent(eventId, options).subscribe(
      pageable => {
        // reset the paging stuff
        this._eventInDb = true;
        this._totalPages = pageable.totalPages || 0;
        this._totalGames = pageable.totalElements || 0;
        this._games = this._games.clear();
        this._games = this._games.push(
          ...pageable.content.toArray().map(x => new EagerGame(x))
        );
        if (!pageable.content.size) {
          this._loading = false;
          this._changeDetector.detectChanges();
        }

        // then get the rosters for each game and determine the placement of the player
        this._games.forEach(game => {
          this._gameService.getGameRosters(game.info.gameId).subscribe(
            rosters => {
              this._processGameRosters(game, rosters, eventId);

              if (game === this._games.last()) {
                this._loading = false;
                this._changeDetector.detectChanges();
              }
            },
            error => {
              console.error(error);

              if (game === this._games.last()) {
                this._loading = false;
                this._changeDetector.detectChanges();
              }
            }
          );
        });
      },
      error => {
        if (error instanceof EventNotInDbError) {
          this._waitUntilPlayerInDb(eventId, scrollContainer);
        } else {
          console.error(error);
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: "Failed to load the recent matches",
            duration: 2500
          });
        }

        this._loading = false;
        this._changeDetector.detectChanges();
      }
    );
  }

  protected _processGameRosters(
    game: EagerGame,
    rosters: List<GameRoster>,
    nickname: string
  ) {
    game.stats = rosters;
    game.totalTeams = rosters.size;
    // search for the roster of the player
    const matchingRoster: GameRoster = rosters
      .filter(x =>
        x.participants
          .map(y => y.name.toLowerCase())
          .includes(nickname.toLowerCase())
      )
      .first();

    // if we have found a roster get the placement of the player
    if (matchingRoster) {
      const player: GameParticipant = matchingRoster.participants
        .filter(x => x.name.toLowerCase() === nickname.toLowerCase())
        .first();
      game.placement = player.winPlace;
      if (player.kills) {
        game.kills = player.kills;
      }
      if (player.headshotKills) {
        game.headshots = player.headshotKills;
      }
    }
  }

  protected _waitUntilPlayerInDb(
    eventId: string,
    scrollContainer: HTMLElement,
    options?: PagingOptions
  ) {
    this._eventInDb = false;
    if (this._refreshInterval) {
      window.clearInterval(this._refreshInterval);
      this._refreshInterval = null;
    }

    this._timeUntilRefresh = EVENT_NOT_IN_DB_TIMEOUT;
    this._changeDetector.detectChanges();
    this._refreshInterval = window.setInterval(() => {
      if (this._timeUntilRefresh > 0) {
        this._timeUntilRefresh--;
        this._changeDetector.detectChanges();
      } else if (this._timeUntilRefresh === 0) {
        window.clearInterval(this._timeUntilRefresh);
        this._timeUntilRefresh = null;
        this._changeDetector.detectChanges();
        this._fetchMatchesOfEvent(eventId, scrollContainer);
      }
    }, 1000);
  }

  protected _fetchNextPage(
    eventId: string,
    scrollContainer: HTMLElement,
    pageSize?: number
  ) {
    // if we have fetched all pages of the player then don't do anything
    if (this._currentPage === this._totalPages - 1) {
      return;
    }

    this._loading = true;
    this._currentPage++;
    this._changeDetector.detectChanges();
    const lastScroll = scrollContainer.scrollTop;

    // first get all games of the player from the next page
    this._gameService
      .getGamesOfEvent(eventId, {
        size: pageSize,
        page: this._currentPage
      })
      .subscribe(
        pageable => {
          this._totalPages = pageable.totalPages;
          this._totalGames = pageable.totalElements;
          const newGames = pageable.content.map(x => new EagerGame(x));

          // then get the rosters of the games which we have newly fetched
          newGames.forEach(game => {
            this._gameService.getGameRosters(game.info.gameId).subscribe(
              rosters => {
                this._processGameRosters(game, rosters, "");

                // if we've iterated through all new games append them to the actual list and display them
                if (game === newGames.last()) {
                  this._games = this._games.push(...newGames.toArray());
                  this._loading = false;
                  this._changeDetector.detectChanges();
                  // add some random ass value cuz otherwise it will have a snappy scroll wtf?? what is this number?
                  scrollContainer.scrollTop = lastScroll + 37;
                }
              },
              error => {
                console.error(error);

                if (game === newGames.last()) {
                  this._loading = false;
                  this._changeDetector.detectChanges();
                  scrollContainer.scrollTop = lastScroll;
                }
              }
            );
          });
        },
        error => {
          console.error(error);
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: "Failed to load the matches",
            duration: 2100
          });
          this._loading = false;
          this._changeDetector.detectChanges();
        }
      );
  }

  get eventInDb() {
    return this._eventInDb;
  }

  get loading() {
    return this._loading;
  }

  get games() {
    return this._games;
  }

  /**
   * The total amount of games that have been found of the player.
   * This might not be the amount of games that have been fetched.
   */
  get totalGames() {
    return this._totalGames;
  }

  get currentPage() {
    return this._currentPage;
  }

  get totalPages() {
    return this._totalPages;
  }

  get timeUntilRefresh() {
    return this._timeUntilRefresh;
  }
}
