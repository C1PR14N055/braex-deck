import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SessionConnectBarComponent } from "./session-connect-bar.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe("SessionConnectBarComponent", () => {
  let component: SessionConnectBarComponent;
  let fixture: ComponentFixture<SessionConnectBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SessionConnectBarComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [HttpClientTestingModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionConnectBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
