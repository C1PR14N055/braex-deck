import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from "@angular/core";
import * as uuidv4 from "uuid/v4";
import { faPlug, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FormControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { TeamSessionService } from "../../../../braex-deck-shared/src/lib/map-canvas/services/team-session.service";

class ViewContext {
  faPlug = faPlug;
  faTimes = faTimes;
}

@Component({
  selector: "app-session-connect-bar",
  templateUrl: "./session-connect-bar.component.html",
  styleUrls: ["./session-connect-bar.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SessionConnectBarComponent implements OnInit, OnDestroy {
  readonly viewContext = new ViewContext();
  private _subs: Subscription[] = [];
  sessionIdControl = new FormControl();
  private _currentSessionId: string | null = null;
  private _placeholderSessionId: string;
  private _connected = false;
  @ViewChild("sessionIdInput")
  private _sessionInput: ElementRef<HTMLInputElement>;

  constructor(
    private _teamSession: TeamSessionService,
    private _changeDetector: ChangeDetectorRef,
    private _renderer: Renderer2
  ) {}

  ngOnInit() {
    this._renderer.listen(this._sessionInput.nativeElement, "focus", () => {
      this._sessionInput.nativeElement.select();
    });
    this._placeholderSessionId = uuidv4();
    this.sessionIdControl.setValue(this._placeholderSessionId);
    let sub = this._teamSession.connected().subscribe(sessionId => {
      this._currentSessionId = sessionId;
      this._connected = true;
      this._changeDetector.detectChanges();
    });
    this._subs.push(sub);

    sub = this._teamSession.disconnected().subscribe(() => {
      this._currentSessionId = null;
      this._connected = false;
      this.sessionIdControl.setValue("");
      this._changeDetector.detectChanges();
    });
    this._subs.push(sub);
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  actionConnectToSession() {
    if (!this.sessionIdControl.value) {
      return;
    }

    this._teamSession.connect((this.sessionIdControl.value as string).trim());
  }

  actionDisconnectFromSession() {
    if (!this._teamSession.active) {
      return;
    }

    this._teamSession.disconnect();
  }

  get currentSessionId() {
    return this._currentSessionId;
  }

  get placeholderSessionId() {
    return this._placeholderSessionId;
  }

  get connected() {
    return this._connected;
  }
}
