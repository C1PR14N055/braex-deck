import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import { MapCanvasComponent } from "../../../../braex-deck-shared/src/lib/map-canvas/map-canvas/map-canvas.component";
// tslint:disable-next-line:max-line-length
import {
  TelemetryParserService,
  TelemetryParserState
} from "../../../../braex-deck-shared/src/lib/map-canvas/services/telemetry-parser.service";
// tslint:disable-next-line:max-line-length
import { PlayerMovementCanvasModule } from "../../../../braex-deck-shared/src/lib/map-canvas/canvas-modules/modules/player-movement.canvas-module";
import { MapPlayer } from "../../../../braex-deck-shared/src/lib/map-canvas/ds/map-player";
// tslint:disable-next-line:max-line-length
import { TimelineTelemetryEvents } from "../../../../braex-deck-shared/src/lib/map-canvas/ds/telemetry/timeline-telemetry-event";
// tslint:disable-next-line:max-line-length
import { InGameUtils } from "../../../../braex-deck-shared/src/lib/map-canvas/utils/in-game-utils";
// tslint:disable-next-line:max-line-length
import { MapBackgroundCanvasModule } from "../../../../braex-deck-shared/src/lib/map-canvas/canvas-modules/modules/map-background.canvas-module";
import { Vector3 } from "../../../../braex-deck-shared/src/lib/map-canvas/ds/vector3";
import {
  CircleZone,
  CircleZoneCanvasModule
} from "../../../../braex-deck-shared/src/lib/map-canvas/canvas-modules/modules/circle-zone.canvas-module";
import { GameService } from "../../../../braex-deck-shared/src/lib/services/game.service";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
// tslint:disable-next-line:max-line-length
import { TelemetryActiveFeedService } from "../../../../braex-deck-shared/src/lib/map-canvas/services/telemetry-active-feed.service";
import { TeamSessionService } from "../../../../braex-deck-shared/src/lib/map-canvas/services/team-session.service";
import { WebSocketBroker } from "../../../../braex-deck-shared/src/lib/websockets/web-socket-broker";
import { ExtReplayState } from "../ds/external/ext-replay-state";
import { environment } from "../../environments/environment";
import * as format from "string-format";
import { ExtReplayStateConverter } from "../converters/external/ext-replay-state.converter";
import { ReplayEventType } from "../ds/internal/replay-event-type.enum";
import { LocalPlayerInfoService } from "../../../../braex-deck-shared/src/lib/services/local-player-info.service";
import { ExtReplayEventType } from "../ds/external/ext-replay-event-type.enum";
import { OverwolfGameEventsService } from "../../../../braex-deck-shared/src/lib/overwolf/overwolf-game-events.service";
// tslint:disable-next-line:max-line-length
import { MapMarkerCanvasModule } from "../../../../braex-deck-shared/src/lib/map-canvas/canvas-modules/modules/map-marker.canvas-module";
// tslint:disable-next-line:max-line-length
import { TelemetryLocation } from "../../../../braex-deck-shared/src/lib/map-canvas/ds/telemetry/objects/telemetry-location";
import { Vector2 } from "../../../../braex-deck-shared/src/lib/map-canvas/ds/vector2";
// tslint:disable-next-line:max-line-length
import { BrushLinesCanvasModule } from "../../../../braex-deck-shared/src/lib/map-canvas/canvas-modules/modules/brush-lines.canvas-module";

// The desired frames per second to trigger canvas updates
export const FRAMES_PER_SECOND = 60;
// Fast forward OR slow motion speed if below 1
export const FAST_FORWARD_SPEED = 20;

// TODO 's
// floor positions for even more performance?
// implement slow mo / fast fwd speed button?
// FIXME 's
// skippin if u didnt play yet

@Component({
  selector: "app-game-review-map-view",
  templateUrl: "./game-review-map-view.component.html",
  styleUrls: [
    "./game-review-map-view.component.scss",
    "../../styles/content-container.scss"
  ],
  host: { class: "h-100" },
  providers: [TelemetryParserService, TelemetryActiveFeedService]
})
export class GameReviewMapViewComponent
  implements OnInit, AfterViewInit, OnDestroy {
  private _subs: Subscription[] = [];
  @ViewChild(MapCanvasComponent)
  private _mapCanvas: MapCanvasComponent;
  private _telemetryUrl: string;
  private _telemetryParserState: TelemetryParserState;
  private _timelineTelemtryEvents: TimelineTelemetryEvents;

  private _playInterval;
  private _initMatchStarted = false;
  private _time = 0; // current play time

  private _replayBroker: WebSocketBroker<ExtReplayState>;
  private _localPlayerNickname: string | null = null;
  playbackActive = false;
  private _skippingActive = false;

  constructor(
    private _playerMovement: PlayerMovementCanvasModule,
    private _circleZone: CircleZoneCanvasModule,
    private _mapBg: MapBackgroundCanvasModule,
    private _telemetryService: TelemetryParserService,
    private _activatedRoute: ActivatedRoute,
    private _gameService: GameService,
    private _telemetryActiveFeedService: TelemetryActiveFeedService,
    private _teamSession: TeamSessionService,
    private _extReplayStateConverter: ExtReplayStateConverter,
    private _localPlayerInfo: LocalPlayerInfoService,
    private _changeDetector: ChangeDetectorRef,
    private _gameEvents: OverwolfGameEventsService,
    private _mapMarker: MapMarkerCanvasModule,
    private _brushLines: BrushLinesCanvasModule
  ) {
    const sub = this._activatedRoute.params.subscribe(params => {
      if (params["gameId"]) {
        const sub2 = this._gameService
          .getGameById(params["gameId"])
          .subscribe(game => {
            this._telemetryUrl = game.telemetryUrl;
            this._initTelemetryData();
          });
        this._subs.push(sub2);
      }
    });
    this._subs.push(sub);
  }

  ngOnInit() {
    let sub = this._localPlayerInfo
      .localPlayerNickname()
      .subscribe(nickname => {
        this._localPlayerNickname = nickname;
      });
    this._subs.push(sub);
    sub = this._teamSession.connected().subscribe(sessionId => {
      this._replayBroker = new WebSocketBroker<ExtReplayState>({
        brokerUrl: environment.webSockets.reviewSync.brokerUrl,
        pushPath: format(environment.webSockets.reviewSync.pushPath, {
          sessionId: sessionId
        }),
        pullPath: format(environment.webSockets.reviewSync.pullPath, {
          sessionId: sessionId
        })
      });
      this._replayBroker.pull().subscribe(event => {
        if (event.owner === this._localPlayerNickname) {
          return;
        }

        const processedEvent = this._extReplayStateConverter.convert(event);
        switch (processedEvent.type) {
          case ReplayEventType.PLAY:
            this._play();
            this.time = processedEvent.timestamp;
            break;
          case ReplayEventType.PAUSE:
            this.time = processedEvent.timestamp;
            this._pause();
            break;
          case ReplayEventType.GOTO:
            this.time = processedEvent.timestamp;
            break;
        }
      });
    });
    this._subs.push(sub);

    sub = this._teamSession.disconnected().subscribe(() => {
      if (this._replayBroker) {
        this._replayBroker.destroy();
        this._replayBroker = undefined;
      }
    });
    this._subs.push(sub);
  }

  ngAfterViewInit() {
    this._mapCanvas.start(this._playerMovement, this._circleZone, this._mapBg);
  }

  private _initTelemetryData() {
    this._telemetryService
      .downloadAndParseTelemetryData(this._telemetryUrl)
      .subscribe(data => {
        if (typeof data === "string") {
          this._telemetryParserState = data;
        } else {
          this._timelineTelemtryEvents = data;
          this._mapBg.currentMap = InGameUtils.getTelemetryMapToPubgMap(
            this._timelineTelemtryEvents.matchStartEvent.mapName
          );
        }
      });
  }

  private _initMatchStart() {
    this._playerMovement.removePlayer(
      ...this._timelineTelemtryEvents.matchStartEvent.characters.map(
        character => character.name
      )
    );
    this._playerMovement.addPlayer(
      ...this._timelineTelemtryEvents.matchStartEvent.characters.map(
        character =>
          MapPlayer.create(
            character.name,
            InGameUtils.telemetryMapLocationToCanvasSpace(
              this._mapBg.currentMap,
              this._mapBg.mapSize,
              Vector3.create(
                character.location.x,
                character.location.y,
                character.location.z
              )
            ),
            true
          )
      )
    );
  }

  actionPlay() {
    this._play();
    if (this._replayBroker) {
      this._replayBroker.push({
        owner: this._localPlayerNickname,
        time: this.time,
        type: ExtReplayEventType.PLAY
      });
    }
  }

  private _play() {
    this.playbackActive = true;
    if (!this._initMatchStarted) {
      this._initMatchStart();
      this._initMatchStarted = true;
    }
    this._animate();
  }

  actionPause() {
    this._pause();
    if (this._replayBroker) {
      this._replayBroker.push({
        owner: this._localPlayerNickname,
        type: ExtReplayEventType.PAUSE,
        time: this.time
      });
    }
  }

  private _pause() {
    this.playbackActive = false;
  }

  private _animate() {
    requestAnimationFrame(() => {
      setTimeout(() => {
        this._drawPlayersAtMoment();
        this._drawZonesAtMoment();
        this._time += (1 / FRAMES_PER_SECOND) * FAST_FORWARD_SPEED;
        if (this._time > this._timelineTelemtryEvents.matchDuration) {
          this._time = 0;
          this.playbackActive = false;
        } else {
          if (this.playbackActive) {
            this._animate();
          }
        }
      }, 1000 / FRAMES_PER_SECOND);
    });
  }

  private _drawPlayersAtMoment() {
    // TODO: dont redraw players that didnt move
    const latestTime = this._getLatestTimeWithData(
      this._timelineTelemtryEvents.charactersAtMoment,
      this._time
    );

    // TODO: dont redraw if last time this was redrawn is the same
    if (latestTime < 0) {
      return;
    }
    const charactersLatest = this._timelineTelemtryEvents.charactersAtMoment.get(
      latestTime
    );

    for (let i = 0; i < charactersLatest.length; i++) {
      const characterPrevPos = this._playerMovement.players.find(
        player => player.nickname === charactersLatest[i].name
      ).position;

      const characterLatestPos = this._transformLocation(
        charactersLatest[i].location
      );

      let nextPos: Vector2;
      // while skipping dont lerp player positions
      if (this._skippingActive) {
        nextPos = characterLatestPos;
      } else {
        nextPos = characterPrevPos.lerp(
          characterLatestPos,
          1 / FRAMES_PER_SECOND
        );
      }

      this._playerMovement.updatePlayer(
        MapPlayer.create(
          charactersLatest[i].name,
          nextPos,
          !(
            charactersLatest[i].health <= 0 &&
            nextPos.floor().equals(characterLatestPos.floor())
          )
        )
      );
    }

    TelemetryActiveFeedService.sendCharacters(
      this._timelineTelemtryEvents.charactersAtMoment.get(latestTime)
    );
  }

  // TODO: implement lerp for circle radiuses
  private _drawZonesAtMoment() {
    const latestTime = this._getLatestTimeWithData(
      this._timelineTelemtryEvents.gameStateAtMoment,
      this._time
    );
    // TODO: dont redraw id last time this was redrawn is the same
    if (latestTime < 0) {
      return;
    }
    const gameState = this._timelineTelemtryEvents.gameStateAtMoment.get(
      latestTime
    ).gameState;
    if (gameState.poisonGasWarningPosition) {
      const transformedLocation = InGameUtils.telemetryMapLocationToCanvasSpace(
        this._mapBg.currentMap,
        this._mapBg.mapSize,
        Vector3.create(
          gameState.poisonGasWarningPosition.x,
          gameState.poisonGasWarningPosition.y,
          gameState.poisonGasWarningPosition.z
        )
      );

      const transformedRadius =
        (gameState.poisonGasWarningRadius /
          InGameUtils.getPubgMapSizeFromTelemetry(this._mapBg.currentMap).x) *
        this._mapBg.mapSize.x;

      this._circleZone.setBlueZone(
        new CircleZone(transformedLocation, transformedRadius)
      );
    }

    if (gameState.safetyZonePosition) {
      const transformedLocation = InGameUtils.telemetryMapLocationToCanvasSpace(
        this._mapBg.currentMap,
        this._mapBg.mapSize,
        Vector3.create(
          gameState.safetyZonePosition.x,
          gameState.safetyZonePosition.y,
          gameState.safetyZonePosition.z
        )
      );

      const transformedRadius =
        (gameState.safetyZoneRadius /
          InGameUtils.getPubgMapSizeFromTelemetry(this._mapBg.currentMap).x) *
        this._mapBg.mapSize.x;

      this._circleZone.setSafeZone(
        new CircleZone(transformedLocation, transformedRadius)
      );
    }

    if (gameState.redZonePosition) {
      const transformedLocation = InGameUtils.telemetryMapLocationToCanvasSpace(
        this._mapBg.currentMap,
        this._mapBg.mapSize,
        Vector3.create(
          gameState.redZonePosition.x,
          gameState.redZonePosition.y,
          gameState.redZonePosition.z
        )
      );

      const transformedRadius =
        (gameState.redZoneRadius /
          InGameUtils.getPubgMapSizeFromTelemetry(this._mapBg.currentMap).x) *
        this._mapBg.mapSize.x;

      this._circleZone.setRedZone(
        new CircleZone(transformedLocation, transformedRadius)
      );
    }
  }

  private _transformLocation(location: TelemetryLocation) {
    return InGameUtils.telemetryMapLocationToCanvasSpace(
      this._mapBg.currentMap,
      this._mapBg.mapSize,
      Vector3.create(location.x, location.y, location.z)
    );
  }

  private _getLatestTimeWithData(
    eventsMap: Map<number, any>,
    time: number
  ): number {
    let lastestTimeWithData = -1;
    for (let i = Math.floor(time); i > 0; i--) {
      if (eventsMap.has(i)) {
        lastestTimeWithData = i;
        break;
      }
    }

    return lastestTimeWithData;
  }

  private _getNextTimeWithData(
    eventsMap: Map<number, any>,
    time: number
  ): number {
    let latestTimeWithData = -1;
    for (
      let i = Math.floor(time + 1);
      i < this._timelineTelemtryEvents.matchDuration;
      i++
    ) {
      if (eventsMap.has(i)) {
        latestTimeWithData = i;
        break;
      }
    }

    return latestTimeWithData;
  }

  // FIXME: skipping forward goes back to 0 if at end
  actionSkipForward(time: number) {
    this.skipForward(time);
    if (this._replayBroker) {
      this._replayBroker.push({
        owner: this._localPlayerNickname,
        type: ExtReplayEventType.GOTO,
        time: this.time
      });
    }
  }

  skipForward(time: number) {
    this._time = time;
  }

  actionSkipBackward(time: number) {
    this.skipBackward(time);
    if (this._replayBroker) {
      this._replayBroker.push({
        owner: this._localPlayerNickname,
        type: ExtReplayEventType.GOTO,
        time: this.time
      });
    }
  }

  skipBackward(time: number) {
    this._time = time;
  }

  setSkippingActive(value: boolean) {
    this._skippingActive = value;
  }

  actionSkippingFinished(time: number) {
    this.time = time;
    if (this._replayBroker) {
      this._replayBroker.push({
        owner: this._localPlayerNickname,
        time: this.time,
        type: ExtReplayEventType.GOTO
      });
    }
  }

  actionCurrentTimeChanged(time: number) {
    this.time = time;
  }

  ngOnDestroy() {
    this._playerMovement.clear();
    this._circleZone.clear();
    this._mapMarker.clearContext();
    this._brushLines.clearContext();
    clearInterval(this._playInterval);
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  get time() {
    return this._time;
  }

  set time(time: number) {
    if (time > 0 && time < this._timelineTelemtryEvents.matchDuration) {
      this._time = time;
    }
  }

  get matchDuration() {
    if (this._telemetryParserState === TelemetryParserState.DONE) {
      return this._timelineTelemtryEvents.matchDuration;
    }

    return 260;
  }

  get loading() {
    return (
      this._telemetryParserState === TelemetryParserState.DOWNLOADING ||
      this._telemetryParserState === TelemetryParserState.PARSING
    );
  }
}
