import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { GameReviewMapViewComponent } from "./game-review-map-view.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { MapCanvasModule } from "../../../../braex-deck-shared/src/lib/map-canvas/map-canvas.module";

describe("GameReviewMapViewComponent", () => {
  let component: GameReviewMapViewComponent;
  let fixture: ComponentFixture<GameReviewMapViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameReviewMapViewComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [MapCanvasModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameReviewMapViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
