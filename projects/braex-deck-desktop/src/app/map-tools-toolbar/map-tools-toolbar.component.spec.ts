import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { MapToolsToolbarComponent } from "./map-tools-toolbar.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("MapToolsToolbarComponent", () => {
  let component: MapToolsToolbarComponent;
  let fixture: ComponentFixture<MapToolsToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MapToolsToolbarComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapToolsToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
