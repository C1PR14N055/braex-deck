import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit
} from "@angular/core";
import { MapCanvasTool } from "../../../../braex-deck-shared/src/lib/map-canvas/ds/map-canvas-tool.enum";
import {
  faEraser,
  faMapMarkerAlt,
  faMousePointer,
  faPaintBrush
} from "@fortawesome/free-solid-svg-icons";
import { MapToolService } from "../../../../braex-deck-shared/src/lib/map-canvas/services/map-tool.service";
import { Subscription } from "rxjs";
import { AppSettingsService } from "../../../../braex-deck-shared/src/lib/services/app-settings.service";

class ViewContext {
  faMousePointer = faMousePointer;
  faPaintBrush = faPaintBrush;
  faMapMarkerAlt = faMapMarkerAlt;
  faEraser = faEraser;
  mapCanvasTool = MapCanvasTool;
}

@Component({
  selector: "app-map-tools-toolbar",
  templateUrl: "./map-tools-toolbar.component.html",
  styleUrls: ["./map-tools-toolbar.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapToolsToolbarComponent implements OnInit, OnDestroy {
  readonly viewContext = new ViewContext();
  private _subs: Subscription[] = [];

  constructor(
    private _mapTool: MapToolService,
    private _changeDetector: ChangeDetectorRef,
    private _appSettings: AppSettingsService
  ) {}

  ngOnInit() {
    const sub = this._mapTool.activeToolChanged().subscribe(() => {
      this._changeDetector.detectChanges();
    });
    this._subs.push(sub);
  }

  actionUseTool(tool: MapCanvasTool) {
    this._mapTool.activeTool = tool;
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  get activeTool() {
    return this._mapTool.activeTool;
  }

  get brushToolHotkey() {
    return this._appSettings.brushToolHotkey;
  }

  get mapMarkerToolHotkey() {
    return this._appSettings.mapMarkerToolHotkey;
  }

  get eraserToolHotkey() {
    return this._appSettings.eraserToolHotkey;
  }
}
