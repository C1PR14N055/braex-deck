import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { EventSearchSidebarContentComponent } from "./event-search-sidebar-content.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { MatSnackBarModule } from "@angular/material";
// tslint:disable-next-line:max-line-length
import { SearchInputComponent } from "../../../../braex-deck-shared/src/lib/search-input/search-input/search-input.component";
import { SharedEventMatchSearchComponent } from "../shared-event-match-search/shared-event-match-search.component";

describe("EventSearchSidebarContentComponent", () => {
  let component: EventSearchSidebarContentComponent;
  let fixture: ComponentFixture<EventSearchSidebarContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EventSearchSidebarContentComponent,
        SharedEventMatchSearchComponent,
        SearchInputComponent
      ],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [HttpClientTestingModule, MatSnackBarModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventSearchSidebarContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
