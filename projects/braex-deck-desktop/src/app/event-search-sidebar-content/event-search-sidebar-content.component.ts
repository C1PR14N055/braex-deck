import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ViewChild
} from "@angular/core";
// tslint:disable-next-line:max-line-length
import { SearchInputComponent } from "../../../../braex-deck-shared/src/lib/search-input/search-input/search-input.component";
import { SharedEventMatchSearchComponent } from "../shared-event-match-search/shared-event-match-search.component";

@Component({
  selector: "app-event-search-sidebar-content",
  templateUrl: "./event-search-sidebar-content.component.html",
  styleUrls: [
    "./event-search-sidebar-content.component.scss",
    "../../../../styles/theme/messages.scss"
  ],
  host: { class: "w-100 h-100" },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventSearchSidebarContentComponent implements AfterViewInit {
  @ViewChild(SharedEventMatchSearchComponent)
  private _sharedEventMatchSearch: SharedEventMatchSearchComponent;
  @ViewChild(SearchInputComponent)
  private _searchInput: SearchInputComponent;
  private _currentlySearchedEvent: string | null = null;

  ngAfterViewInit() {
    setTimeout(() => {
      this._searchInput.focus();
    }, 0);
  }

  actionSearchEvent(eventId: string) {
    this._currentlySearchedEvent = eventId;
    this._sharedEventMatchSearch.searchEvent(eventId);
  }

  actionRefresh() {
    if (!this._currentlySearchedEvent) {
      return;
    }

    this._sharedEventMatchSearch.searchEvent(this._currentlySearchedEvent);
  }

  get currentEvent() {
    return this._sharedEventMatchSearch.currentEvent;
  }
}
