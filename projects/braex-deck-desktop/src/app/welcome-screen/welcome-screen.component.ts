import { Component, OnInit } from "@angular/core";
import { MatDialogRef } from "@angular/material";

@Component({
  selector: "app-welcome-screen",
  templateUrl: "./welcome-screen.component.html",
  styleUrls: [
    "./welcome-screen.component.scss",
    "../../../../styles/theme/scroll.scss"
  ]
})
export class WelcomeScreenComponent implements OnInit {
  constructor(private _dialogRef: MatDialogRef<WelcomeScreenComponent>) {}

  ngOnInit() {}

  actionOk() {
    this._dialogRef.close();
  }
}
