import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ControlsHelpComponent } from "./controls-help.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

describe("ControlsHelpComponent", () => {
  let component: ControlsHelpComponent;
  let fixture: ComponentFixture<ControlsHelpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ControlsHelpComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [BrowserAnimationsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlsHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
