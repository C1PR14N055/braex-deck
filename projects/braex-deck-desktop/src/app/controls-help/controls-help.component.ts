import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";
import { AppSettingsService } from "../../../../braex-deck-shared/src/lib/services/app-settings.service";
import { List } from "immutable";
import { Subscription } from "rxjs";
import { HOTKEY_SEPARATOR } from "../../../../braex-deck-shared/src/lib/services/hotkey.service";

class ViewContext {
  faTimes = faTimes;
}

@Component({
  selector: "app-controls-help",
  templateUrl: "./controls-help.component.html",
  styleUrls: [
    "./controls-help.component.scss",
    "../../../../styles/theme/messages.scss",
    "../../../../styles/utils.scss"
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger("windowState", [
      state(
        "visible",
        style({ opacity: 1, transform: "translateY(0) scaleY(1)" })
      ),
      state(
        "hidden",
        style({ opacity: 0, transform: "translateY(-50%) scaleY(0.4)" })
      ),
      transition("hidden => visible", animate(140))
    ])
  ]
})
export class ControlsHelpComponent implements OnInit, OnDestroy {
  readonly viewContext = new ViewContext();
  private _subs: Subscription[] = [];
  private _visible = false;
  private _dragging = false;
  /**
   * Event emitted when the help window should be closed.
   */
  @Output()
  close = new EventEmitter<void>();
  private _brushToolKeys = List<string>();
  private _mapMarkerToolKeys = List<string>();
  private _eraserToolKeys = List<string>();

  constructor(
    private _changeDetector: ChangeDetectorRef,
    private _appSettings: AppSettingsService
  ) {}

  ngOnInit() {
    this._initToolKeys();
  }

  private _initToolKeys() {
    this._brushToolKeys = List(
      this._appSettings.brushToolHotkey.split(HOTKEY_SEPARATOR)
    );
    let sub = this._appSettings.brushToolHotkeyChanged().subscribe(hotkey => {
      this._brushToolKeys = List(hotkey.split(HOTKEY_SEPARATOR));
      this._changeDetector.detectChanges();
    });
    this._subs.push(sub);

    this._mapMarkerToolKeys = List(
      this._appSettings.mapMarkerToolHotkey.split(HOTKEY_SEPARATOR)
    );
    sub = this._appSettings.mapMarkerToolHotkeyChanged().subscribe(hotkey => {
      this._mapMarkerToolKeys = List(hotkey.split(HOTKEY_SEPARATOR));
      this._changeDetector.detectChanges();
    });
    this._subs.push(sub);

    this._eraserToolKeys = List(
      this._appSettings.eraserToolHotkey.split(HOTKEY_SEPARATOR)
    );
    sub = this._appSettings.eraserToolHotkeyChanged().subscribe(hotkey => {
      this._eraserToolKeys = List(hotkey.split(HOTKEY_SEPARATOR));
      this._changeDetector.detectChanges();
    });
    this._subs.push(sub);
  }

  open() {
    setTimeout(() => {
      this._visible = true;
      this._changeDetector.detectChanges();
    }, 0);
  }

  actionDragStarted() {
    this._dragging = true;
  }

  actionDragEnded() {
    this._dragging = false;
  }

  actionClose() {
    this._visible = false;
    this._changeDetector.detectChanges();
    setTimeout(() => {
      this.close.emit(null);
    }, 0);
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  get visible() {
    return this._visible;
  }

  get dragging() {
    return this._dragging;
  }

  get brushToolKeys() {
    return this._brushToolKeys;
  }

  get mapMarkerToolKeys() {
    return this._mapMarkerToolKeys;
  }

  get eraserToolKeys() {
    return this._eraserToolKeys;
  }
}
