import { TableGameParticipant } from "./table-game-participant";
import { List } from "immutable";

export class TableGameRoster {
  private _participantsNicknames: string;

  constructor(
    public id?: number,
    public rank?: number,
    public won?: boolean,
    public teamId?: number,
    public kills?: number,
    public damageDealt?: number,
    public dbnos?: number,
    public localPlayerTeam = false,
    private _participants = List<TableGameParticipant>()
  ) {
    if (!_participants.isEmpty()) {
      this._computeParticipantsNicknames();
    }
  }

  private _computeParticipantsNicknames() {
    this._participantsNicknames = this._participants
      .map(x => x.nickname)
      .join(", ");
  }

  get participantsNicknames() {
    return this._participantsNicknames;
  }

  get participants() {
    return this._participants;
  }

  set participants(value: List<TableGameParticipant>) {
    this._participants = value;
    this._computeParticipantsNicknames();
  }
}
