import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import { MatSnackBar, MatSort, MatTableDataSource } from "@angular/material";
import { GameService } from "../../../../braex-deck-shared/src/lib/services/game.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription, zip } from "rxjs";
import { LazyGame } from "../../../../braex-deck-shared/src/lib/ds/game/lazy-game";
import { List } from "immutable";
// tslint:disable-next-line:max-line-length
import { ErrorSnackBarComponent } from "../../../../braex-deck-shared/src/lib/error-snack-bar/error-snack-bar/error-snack-bar.component";
import { faCalendarAlt, faClock } from "@fortawesome/free-regular-svg-icons";
import { faBinoculars, faRunning } from "@fortawesome/free-solid-svg-icons";
import { environment } from "../../../../braex-deck-shared/src/environments/environment";
import { TableGameRoster } from "./ds/table-game-roster";
import { GameRoster } from "../../../../braex-deck-shared/src/lib/ds/game/game-roster";
import { GameParticipant } from "../../../../braex-deck-shared/src/lib/ds/game/game-participant";
import { TableGameParticipant } from "./ds/table-game-participant";
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";
import { LocalPlayerInfoService } from "../../../../braex-deck-shared/src/lib/services/local-player-info.service";
import { UNSUPPORTED_MAPS } from "../shared/unsupported-maps";
// tslint:disable-next-line:max-line-length
import { OverwolfGameEventsService } from "../../../../braex-deck-shared/src/lib/overwolf/overwolf-game-events.service";
import { GamePhase } from "../../../../braex-deck-shared/src/lib/ds/game/game-phase.enum";

class ViewContext {
  faCalendarAlt = faCalendarAlt;
  faRunning = faRunning;
  faClock = faClock;
  faBinoculars = faBinoculars;
  dateTimeFormat = environment.dateTimeFormat;
  unsupportedMaps = UNSUPPORTED_MAPS;
}

@Component({
  selector: "app-game-details",
  templateUrl: "./game-details.component.html",
  styleUrls: [
    "./game-details.component.scss",
    "../../../../styles/theme/scroll.scss",
    "../../styles/content-container.scss"
  ],
  host: { class: "h-100" },
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger("detailsExpanded", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("200ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})
export class GameDetailsComponent implements OnDestroy, OnInit {
  readonly viewContext = new ViewContext();
  private _tableDataSource: MatTableDataSource<TableGameRoster> | null = null;
  private _columnsToDisplay = ["rank", "name", "kills", "damageDealt", "dbnos"];
  private _subs: Subscription[] = [];
  private _gameId: string | null = null;
  private _game: LazyGame | null = null;
  private _rosters = List<TableGameRoster>();
  @ViewChild(MatSort)
  private _tableSort: MatSort;
  private _localPlayerNickname: string | null = null;
  private _loading = true;
  private _matchRunning = false;

  constructor(
    private _gameService: GameService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _snackBar: MatSnackBar,
    private _changeDetector: ChangeDetectorRef,
    private _localPlayer: LocalPlayerInfoService,
    private _gameEvents: OverwolfGameEventsService
  ) {
    const sub = this._localPlayer.localPlayerNickname().subscribe(nickname => {
      this._localPlayerNickname = nickname;
    });
    this._subs.push(sub);
  }

  ngOnInit() {
    this._initRouteParams();
    this._initMatchRunningEvents();
  }

  private _initRouteParams() {
    const sub = this._activatedRoute.params.subscribe(params => {
      if (params["gameId"]) {
        this._gameId = params["gameId"];
        this._loadGameDetails();
      }
    });
    this._subs.push(sub);
  }

  private _initMatchRunningEvents() {
    const sub = this._gameEvents.onPhaseChanged().subscribe(phase => {
      this._matchRunning = phase !== GamePhase.LOBBY;
    });
    this._subs.push(sub);
  }

  private _loadGameDetails() {
    this._loading = true;
    this._changeDetector.detectChanges();

    zip(
      this._gameService.getGameById(this._gameId),
      this._gameService.getGameRosters(this._gameId)
    ).subscribe(
      result => {
        this._game = result[0];
        this._rosters = List(result[1].map(x => this._createMiniGameRoster(x)));
        this._tableDataSource = new MatTableDataSource(this._rosters.toArray());
        this._loading = false;
        this._changeDetector.detectChanges();
        this._tableDataSource.sort = this._tableSort;
      },
      error => {
        console.error(error);
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: "Failed to load the game",
          duration: 2100
        });
      }
    );
  }

  private _createMiniGameRoster(roster: GameRoster) {
    const result = new TableGameRoster();
    result.id = roster.id;
    result.rank = roster.rank;
    result.won = roster.won;
    result.kills = roster.participants
      .map(x => x.kills)
      .reduce((a, b) => a + b);
    result.damageDealt = roster.participants
      .map(x => x.damageDealt)
      .reduce((a, b) => a + b);
    result.dbnos = roster.participants
      .map(x => x.dbnos)
      .reduce((a, b) => a + b);
    result.participants = roster.participants.map(x =>
      this._createMiniGameParticipant(x)
    );
    if (this._localPlayerNickname) {
      result.localPlayerTeam = roster.participants
        .map(x => x.name)
        .includes(this._localPlayerNickname);
    }

    return result;
  }

  private _createMiniGameParticipant(participant: GameParticipant) {
    const result = new TableGameParticipant();
    result.nickname = participant.name;
    result.damageDealt = participant.damageDealt;
    result.kills = participant.kills;
    result.dbnos = participant.dbnos;

    return result;
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  actionOpenGameReview() {
    if (!this._game) {
      return;
    }
    if (!this._game.gameId) {
      return;
    }

    // this._telemetryService.downloadAndParse();
    // this._telemetryService.downloadTelemetryData();
    // this._telemetryService.parseTelemetryData();

    // noinspection JSIgnoredPromiseFromCall
    this._router.navigate(["/game-review", this._game.gameId]);
  }

  get game() {
    return this._game;
  }

  get tableDataSource() {
    return this._tableDataSource;
  }

  get columnsToDisplay() {
    return this._columnsToDisplay;
  }

  get loading() {
    return this._loading;
  }

  get matchRunning() {
    return this._matchRunning;
  }

  get statusBarMessage() {
    if (this.matchRunning) {
      return "Cannot review a match while being in an on-going match";
    }
    if (UNSUPPORTED_MAPS.includes(this._game.map)) {
      return "The match review does <b>not</b> support this map";
    }

    return "Review the whole match in the <b>2D replay player</b>";
  }
}
