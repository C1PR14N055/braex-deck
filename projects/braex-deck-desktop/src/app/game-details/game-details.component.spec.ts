import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { GameDetailsComponent } from "./game-details.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { MapIconModule } from "../../../../braex-deck-shared/src/lib/map-icon/map-icon.module";
import { MapNameModule } from "../../../../braex-deck-shared/src/lib/map-name/map-name.module";
import { DurationModule } from "../../../../braex-deck-shared/src/lib/duration/duration.module";
import { MatSnackBarModule, MatTableModule } from "@angular/material";
import { PlayerStatsService } from "../../../../braex-deck-shared/src/lib/services/player-stats.service";
import { GameService } from "../../../../braex-deck-shared/src/lib/services/game.service";
import { RouterTestingModule } from "@angular/router/testing";
import { EnumerationModule } from "../../../../braex-deck-shared/src/lib/enumeration/enumeration.module";
import { LocalPlayerInfoService } from "../../../../braex-deck-shared/src/lib/services/local-player-info.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { GameTrackerService } from "../../../../braex-deck-shared/src/lib/services/game-tracker.service";

describe("GameDetailsComponent", () => {
  let component: GameDetailsComponent;
  let fixture: ComponentFixture<GameDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameDetailsComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        MapIconModule,
        MapNameModule,
        DurationModule,
        MatTableModule,
        RouterTestingModule,
        MatSnackBarModule,
        EnumerationModule,
        HttpClientTestingModule
      ],
      providers: [
        PlayerStatsService,
        GameService,
        LocalPlayerInfoService,
        { provide: GameTrackerService, useValue: {} }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
