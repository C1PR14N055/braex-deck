import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators
} from "@angular/forms";
import { GameMode } from "../../../../braex-deck-shared/src/lib/ds/game/game-mode.enum";
import { Perspective } from "../../../../braex-deck-shared/src/lib/ds/game/perspective.enum";
import { AppSettingsService } from "../../../../braex-deck-shared/src/lib/services/app-settings.service";
import { Set } from "immutable";
import { HOTKEY_SEPARATOR } from "../../../../braex-deck-shared/src/lib/services/hotkey.service";
import { sortBy } from "lodash";

@Component({
  selector: "app-settings-dialog",
  templateUrl: "./settings-dialog.component.html",
  styleUrls: [
    "./settings-dialog.component.scss",
    "../../../../styles/theme/scroll.scss"
  ],
  encapsulation: ViewEncapsulation.None
})
export class SettingsDialogComponent implements OnInit {
  readonly generalGroup = new FormGroup({
    defaultSearchGameMode: new FormControl("", Validators.required),
    preferredPerspective: new FormControl("", Validators.required)
  });
  readonly hotkeysGroup = new FormGroup({
    brushTool: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ]),
    markerTool: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ]),
    eraserTool: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ]),
    genericMarker: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ]),
    carMarker: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ]),
    weaponMarker: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ]),
    itemMarker: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ]),
    focusPlayer: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ]),
    undo: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ]),
    redo: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ]),
    playPause: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ]),
    fastForward: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ]),
    fastBackward: new FormControl("", [
      Validators.required,
      control => this._validateDuplicateHotkeys(control)
    ])
  });
  private _gameModes: GameMode[] = [];
  private _perspectives: Perspective[] = [];
  private _pressedKeys = Set<string>();
  private _releasedKeys = Set<string>();

  constructor(
    private _dialogRef: MatDialogRef<SettingsDialogComponent>,
    private _appSettings: AppSettingsService
  ) {}

  ngOnInit() {
    this._initGameModes();
    this._initGameModeTypes();
    this._initGeneralFormControls();
    this._initHotkeyFormControls();
  }

  private _initGameModes() {
    Object.keys(GameMode).forEach(x => {
      this._gameModes.push(GameMode[x]);
    });
  }

  private _initGameModeTypes() {
    Object.keys(Perspective).forEach(x => {
      this._perspectives.push(Perspective[x]);
    });
  }

  private _initGeneralFormControls() {
    this.generalGroup.controls["defaultSearchGameMode"].setValue(
      this._appSettings.defaultSearchGameMode
    );
    this.generalGroup.controls["preferredPerspective"].setValue(
      this._appSettings.preferredPerspective
    );
  }

  private _initHotkeyFormControls() {
    this.hotkeysGroup.controls["brushTool"].setValue(
      this._appSettings.brushToolHotkey
    );
    this.hotkeysGroup.controls["markerTool"].setValue(
      this._appSettings.mapMarkerToolHotkey
    );
    this.hotkeysGroup.controls["eraserTool"].setValue(
      this._appSettings.eraserToolHotkey
    );
    this.hotkeysGroup.controls["genericMarker"].setValue(
      this._appSettings.genericMarkerHotkey
    );
    this.hotkeysGroup.controls["carMarker"].setValue(
      this._appSettings.carMarkerHotkey
    );
    this.hotkeysGroup.controls["weaponMarker"].setValue(
      this._appSettings.weaponMarkerHotkey
    );
    this.hotkeysGroup.controls["itemMarker"].setValue(
      this._appSettings.itemMarkerHotkey
    );
    this.hotkeysGroup.controls["focusPlayer"].setValue(
      this._appSettings.focusPlayerHotkey
    );
    this.hotkeysGroup.controls["undo"].setValue(this._appSettings.undoHotkey);
    this.hotkeysGroup.controls["redo"].setValue(this._appSettings.redoHotkey);
    this.hotkeysGroup.controls["playPause"].setValue(
      this._appSettings.playPauseHotkey
    );
    this.hotkeysGroup.controls["fastForward"].setValue(
      this._appSettings.fastForwardHotkey
    );
    this.hotkeysGroup.controls["fastBackward"].setValue(
      this._appSettings.fastBackwardHotkey
    );
  }

  private _validateDuplicateHotkeys(
    control: AbstractControl
  ): ValidationErrors | null {
    if (!control.value) {
      return;
    }

    if (control !== this.hotkeysGroup.controls["brushTool"]) {
      if (control.value === this.hotkeysGroup.controls["brushTool"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    if (control !== this.hotkeysGroup.controls["markerTool"]) {
      if (control.value === this.hotkeysGroup.controls["markerTool"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    if (control !== this.hotkeysGroup.controls["eraserTool"]) {
      if (control.value === this.hotkeysGroup.controls["eraserTool"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    if (control !== this.hotkeysGroup.controls["genericMarker"]) {
      if (control.value === this.hotkeysGroup.controls["genericMarker"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    if (control !== this.hotkeysGroup.controls["carMarker"]) {
      if (control.value === this.hotkeysGroup.controls["carMarker"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    if (control !== this.hotkeysGroup.controls["weaponMarker"]) {
      if (control.value === this.hotkeysGroup.controls["weaponMarker"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    if (control !== this.hotkeysGroup.controls["itemMarker"]) {
      if (control.value === this.hotkeysGroup.controls["itemMarker"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    if (control !== this.hotkeysGroup.controls["focusPlayer"]) {
      if (control.value === this.hotkeysGroup.controls["focusPlayer"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    if (control !== this.hotkeysGroup.controls["undo"]) {
      if (control.value === this.hotkeysGroup.controls["undo"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    if (control !== this.hotkeysGroup.controls["redo"]) {
      if (control.value === this.hotkeysGroup.controls["redo"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    if (control !== this.hotkeysGroup.controls["playPause"]) {
      if (control.value === this.hotkeysGroup.controls["playPause"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    if (control !== this.hotkeysGroup.controls["fastForward"]) {
      if (control.value === this.hotkeysGroup.controls["fastForward"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    if (control !== this.hotkeysGroup.controls["fastBackward"]) {
      if (control.value === this.hotkeysGroup.controls["fastBackward"].value) {
        return {
          duplicateHotkey: true
        };
      }
    }

    return null;
  }

  actionHotkeyKeyDown(event: KeyboardEvent) {
    event.preventDefault();
    this._pressedKeys = this._pressedKeys.add(event.key.toLowerCase());
  }

  actionHotkeyKeyUp(event: KeyboardEvent, formControlName: string) {
    this._releasedKeys = this._releasedKeys.add(event.key.toLowerCase());
    if (this._releasedKeys.size === this._pressedKeys.size) {
      this.hotkeysGroup.controls[formControlName].setValue(
        sortBy(
          this._releasedKeys.toArray().map(x => this._normalizeKeyName(x))
        ).join(HOTKEY_SEPARATOR)
      );
      this._releasedKeys = this._releasedKeys.clear();
      this._pressedKeys = this._pressedKeys.clear();
    }
  }

  private _normalizeKeyName(key: string) {
    switch (key) {
      case " ":
        return "space";
      case "control":
        return "ctrl";
      case "arrowdown":
        return "down";
      case "arrowup":
        return "up";
      case "arrowright":
        return "right";
      case "arrowleft":
        return "left";
    }

    return key;
  }

  actionCancel() {
    this._dialogRef.close(false);
  }

  actionApply() {
    this._applyGeneralFormChanges();
    this._applyHotkeysFormChanges();
    this._dialogRef.close(true);
  }

  private _applyGeneralFormChanges() {
    if (this.generalGroup.controls["defaultSearchGameMode"].valid) {
      this._appSettings.defaultSearchGameMode = this.generalGroup.controls[
        "defaultSearchGameMode"
      ].value;
    }
    if (this.generalGroup.controls["preferredPerspective"].valid) {
      this._appSettings.preferredPerspective = this.generalGroup.controls[
        "preferredPerspective"
      ].value;
    }
  }

  private _applyHotkeysFormChanges() {
    if (this.hotkeysGroup.valid) {
      this._appSettings.brushToolHotkey = this.hotkeysGroup.controls[
        "brushTool"
      ].value;
      this._appSettings.mapMarkerToolHotkey = this.hotkeysGroup.controls[
        "markerTool"
      ].value;
      this._appSettings.eraserToolHotkey = this.hotkeysGroup.controls[
        "eraserTool"
      ].value;
      this._appSettings.genericMarkerHotkey = this.hotkeysGroup.controls[
        "genericMarker"
      ].value;
      this._appSettings.carMarkerHotkey = this.hotkeysGroup.controls[
        "carMarker"
      ].value;
      this._appSettings.weaponMarkerHotkey = this.hotkeysGroup.controls[
        "weaponMarker"
      ].value;
      this._appSettings.itemMarkerHotkey = this.hotkeysGroup.controls[
        "itemMarker"
      ].value;
      this._appSettings.focusPlayerHotkey = this.hotkeysGroup.controls[
        "focusPlayer"
      ].value;
      this._appSettings.undoHotkey = this.hotkeysGroup.controls["undo"].value;
      this._appSettings.redoHotkey = this.hotkeysGroup.controls["redo"].value;
      this._appSettings.playPauseHotkey = this.hotkeysGroup.controls[
        "playPause"
      ].value;
      this._appSettings.fastForwardHotkey = this.hotkeysGroup.controls[
        "fastForward"
      ].value;
      this._appSettings.fastBackwardHotkey = this.hotkeysGroup.controls[
        "fastBackward"
      ].value;
    }
  }

  get gameModes() {
    return this._gameModes;
  }

  get perspectives() {
    return this._perspectives;
  }

  get duplicateHotkeysMsg() {
    return "Duplicate hotkey";
  }
}
