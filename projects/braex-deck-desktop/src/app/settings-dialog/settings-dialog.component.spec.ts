import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SettingsDialogComponent } from "./settings-dialog.component";
import { MatDialogModule, MatDialogRef } from "@angular/material";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { SharedPipesModule } from "../../../../braex-deck-shared/src/lib/shared-pipes/shared-pipes.module";

describe("SettingsDialogComponent", () => {
  let component: SettingsDialogComponent;
  let fixture: ComponentFixture<SettingsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule, SharedPipesModule],
      declarations: [SettingsDialogComponent],
      providers: [{ provide: MatDialogRef, useValue: {} }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
