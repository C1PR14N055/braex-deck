import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SharedPlayerMatchSearchComponent } from "./shared-player-match-search.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { MatSnackBarModule } from "@angular/material";

describe("SharedPlayerMatchSearchComponent", () => {
  let component: SharedPlayerMatchSearchComponent;
  let fixture: ComponentFixture<SharedPlayerMatchSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SharedPlayerMatchSearchComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [HttpClientTestingModule, MatSnackBarModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedPlayerMatchSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
