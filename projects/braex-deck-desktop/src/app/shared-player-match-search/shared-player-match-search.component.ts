import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  ViewChild
} from "@angular/core";
import { GameService } from "../../../../braex-deck-shared/src/lib/services/game.service";
import { MatSnackBar } from "@angular/material";
// tslint:disable-next-line:max-line-length
import { SCROLL_THRESHOLD } from "../shared/scroll";
import { BasePlayerGamesSearch } from "../shared/game-search/base-player-games-search";
import { EagerGame } from "../../../../braex-deck-shared/src/lib/ds/game/eager-game";

@Component({
  selector: "app-shared-player-match-search",
  templateUrl: "./shared-player-match-search.component.html",
  styleUrls: [
    "./shared-player-match-search.component.scss",
    "../../../../styles/theme/scroll.scss",
    "../../../../styles/theme/messages.scss"
  ],
  host: { class: "d-flex flex-column flex-grow-1" },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SharedPlayerMatchSearchComponent extends BasePlayerGamesSearch {
  private _currentPlayer: string | null = null;
  @ViewChild("gameEntriesWrapper")
  private _gameEntriesWrapper: ElementRef<HTMLDivElement>;

  constructor(
    gameService: GameService,
    changeDetector: ChangeDetectorRef,
    snackBar: MatSnackBar
  ) {
    super(gameService, snackBar, changeDetector);
  }

  searchPlayer(nickname: string) {
    this._currentPlayer = nickname;
    this._fetchMatchesOfPlayer(
      nickname,
      this._gameEntriesWrapper.nativeElement
    );
  }

  actionGameEntriesScroll() {
    if (this._loading || this._currentPage === this._totalPages - 1) {
      return;
    }

    if (
      this._gameEntriesWrapper.nativeElement.scrollTop +
        this._gameEntriesWrapper.nativeElement.clientHeight >=
      this._gameEntriesWrapper.nativeElement.scrollHeight - SCROLL_THRESHOLD
    ) {
      this._fetchNextPage(
        this._currentPlayer,
        this._gameEntriesWrapper.nativeElement
      );
    }
  }

  trackByGameId(index: number, item: EagerGame) {
    return item.info.gameId;
  }

  get currentPlayer() {
    return this._currentPlayer;
  }
}
