import { Injectable, OnDestroy } from "@angular/core";
import { OverwolfCoreService } from "../../../../braex-deck-shared/src/lib/overwolf/overwolf-core.service";
import { MatSnackBar } from "@angular/material";
import { Observable, Observer, Subscription } from "rxjs";
import { InGameMapState } from "../ds/in-game-map-state.enum";
import { WindowNames } from "../../../../braex-deck-shared/src/lib/ds/window-names.enum";
import { HotkeyActions } from "../../../../braex-deck-shared/src/lib/ds/hotkey-actions.enum";
// tslint:disable-next-line:max-line-length
import { ErrorSnackBarComponent } from "../../../../braex-deck-shared/src/lib/error-snack-bar/error-snack-bar/error-snack-bar.component";
import { OverwolfGameEventsService } from "../../../../braex-deck-shared/src/lib/overwolf/overwolf-game-events.service";
import { GamePhase } from "../../../../braex-deck-shared/src/lib/ds/game/game-phase.enum";
import { Vector2 } from "../../../../braex-deck-shared/src/lib/map-canvas/ds/vector2";

export const WINDOW_CREATE_TIMEOUT = 2000;
export const IN_GAME_WINDOW_SIZE = Vector2.create(900, 900);

@Injectable({
  providedIn: "root"
})
export class InGameMapWindowController implements OnDestroy {
  private _subs: Subscription[] = [];
  private _windowState = InGameMapState.HIDDEN;
  private _windowCreated = false;
  private _stateChangedObservers: Observer<InGameMapState>[] = [];
  private _pubgLaunched = false;
  private _allowedToOpenMap = false;
  private _pubgResolution: Vector2 | null = null;

  constructor(
    private _overwolfCore: OverwolfCoreService,
    private _snackBar: MatSnackBar,
    private _gameEvents: OverwolfGameEventsService
  ) {
    let sub = this._overwolfCore.onPubgLaunched().subscribe(info => {
      if (info && info.width && info.height) {
        this._pubgResolution = Vector2.create(info.width, info.height);
      }
    });
    this._subs.push(sub);

    sub = this._overwolfCore.onGameInfoUpdated().subscribe(info => {
      if (
        info &&
        info.gameInfo &&
        info.gameInfo.width &&
        info.gameInfo.height
      ) {
        this._pubgResolution = Vector2.create(
          info.gameInfo.width,
          info.gameInfo.height
        );
      }
    });
    this._subs.push(sub);

    this._overwolfCore.registerHotkey(HotkeyActions.IN_GAME_MAP, () => {
      if (!this._pubgLaunched) {
        return;
      }

      if (this._windowState === InGameMapState.HIDDEN) {
        if (this._allowedToOpenMap) {
          this._windowState = InGameMapState.VISIBLE;
          this._overwolfCore.showWindow(WindowNames.IN_GAME).subscribe(
            () => {
              const windowPosition = this._pubgResolution
                .divide(IN_GAME_WINDOW_SIZE)
                .subtract(IN_GAME_WINDOW_SIZE.divide(2));
              this._overwolfCore.changeWindowPosition(
                WindowNames.IN_GAME,
                windowPosition.x,
                windowPosition.y
              );
            },
            error => {
              console.error(error);
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: "Failed to open the in-game map",
                duration: 3000
              });
            }
          );
        }
      } else {
        this._windowState = InGameMapState.HIDDEN;
        this._overwolfCore.hideWindow(WindowNames.IN_GAME).subscribe(
          () => {},
          error => {
            console.error(error);
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: "Failed to open the in-game map",
              duration: 3000
            });
          }
        );
      }

      this._triggerStateChanged(this._windowState);
    });

    sub = this._overwolfCore.onPubgLaunched().subscribe(() => {
      this._pubgLaunched = true;
      this._createInGameWindow();
    });
    this._subs.push(sub);

    sub = this._overwolfCore.onPubgTerminated().subscribe(() => {
      this._pubgLaunched = false;
      this._overwolfCore.closeWindow(WindowNames.IN_GAME).subscribe(() => {
        this._windowCreated = false;
      });
    });
    this._subs.push(sub);

    sub = this._gameEvents.onPhaseChanged().subscribe(phase => {
      this._allowedToOpenMap = phase !== GamePhase.LOBBY;
    });
    this._subs.push(sub);
  }

  private _createInGameWindow() {
    if (this._windowCreated) {
      return;
    }

    this._overwolfCore.createWindow(WindowNames.IN_GAME).subscribe(
      () => {},
      error => {
        console.error(error);
      }
    );
    setTimeout(() => {
      this._overwolfCore.hideWindow(WindowNames.IN_GAME).subscribe(
        () => {
          this._windowCreated = true;
        },
        error => {
          console.error(error);
        }
      );
    }, 100);

    setTimeout(() => {
      this._createInGameWindow();
    }, WINDOW_CREATE_TIMEOUT);
  }

  ngOnDestroy() {
    if (this._windowCreated) {
      this._overwolfCore.closeWindow(WindowNames.IN_GAME).subscribe();
    }

    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  private _triggerStateChanged(state: InGameMapState) {
    this._stateChangedObservers.forEach(x => {
      x.next(state);
    });
  }

  stateChanged(): Observable<InGameMapState> {
    return Observable.create(observer => {
      this._stateChangedObservers.push(observer);
    });
  }
}
