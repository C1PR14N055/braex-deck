import { fakeAsync, TestBed, tick } from "@angular/core/testing";
import { InGameMapWindowController } from "./in-game-map.window-controller";
import { MatSnackBarModule } from "@angular/material";
import { Observable, Subscription } from "rxjs";
// tslint:disable-next-line:max-line-length
import { FakeOverwolfCoreService } from "../../../../braex-deck-shared/src/lib/overwolf/testing/fake-overwolf-core.service";
import { OverwolfCoreService } from "../../../../braex-deck-shared/src/lib/overwolf/overwolf-core.service";
import { HotkeyActions } from "../../../../braex-deck-shared/src/lib/ds/hotkey-actions.enum";
import { InGameMapState } from "../ds/in-game-map-state.enum";
import { WindowNames } from "../../../../braex-deck-shared/src/lib/ds/window-names.enum";

describe("InGameMapWindowController", () => {
  let controller: InGameMapWindowController;
  let fakeOverwolfCore: FakeOverwolfCoreService;
  let subs: Subscription[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        InGameMapWindowController,
        { provide: OverwolfCoreService, useClass: FakeOverwolfCoreService }
      ],
      imports: [MatSnackBarModule]
    });

    controller = TestBed.get(InGameMapWindowController);
    fakeOverwolfCore = TestBed.get(OverwolfCoreService);
    subs = [];
  });

  it("should be created", () => {
    expect(controller).toBeTruthy();
  });

  it("should register the in-game map shortcut", () => {
    expect(fakeOverwolfCore.hasHotkey(HotkeyActions.IN_GAME_MAP)).toBeTruthy();
  });

  it("should notify all observers when the in-game map is toggled visible", fakeAsync(() => {
    _triggerPubgLaunched();
    tick(500);

    let received: InGameMapState;
    const sub = controller.stateChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    fakeOverwolfCore.triggerHotkey(HotkeyActions.IN_GAME_MAP);
    tick(200);

    expect(received).toEqual(InGameMapState.VISIBLE);
  }));

  it("should notify all observers when the in-game map is toggled hidden", fakeAsync(() => {
    _triggerPubgLaunched();
    tick(500);

    let received: InGameMapState;
    const sub = controller.stateChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    fakeOverwolfCore.triggerHotkey(HotkeyActions.IN_GAME_MAP);
    tick(200);

    fakeOverwolfCore.triggerHotkey(HotkeyActions.IN_GAME_MAP);
    tick(200);

    expect(received).toEqual(InGameMapState.HIDDEN);
  }));

  it("should create the in-game window when pubg is started", fakeAsync(() => {
    const spy = spyOn(fakeOverwolfCore, "createWindow").and.returnValue(
      Observable.create()
    );
    _triggerPubgLaunched();
    tick(500);

    expect(spy).toHaveBeenCalledWith(WindowNames.IN_GAME);
  }));

  function _triggerPubgLaunched() {
    fakeOverwolfCore.triggerPubgLaunched({
      allowsVideoCapture: true,
      commandLine: "C:\\test",
      detectedRenderer: "D3D11",
      executionPath: "C:\\some-path",
      height: 1280,
      id: 10906,
      isInFocus: true,
      isRunning: true,
      logicalHeight: 1920,
      logicalWidth: 1080,
      renderers: ["D3D11", "D3D9"],
      sessionId: "test-session-id",
      title: "PUBG",
      width: 720
    });
  }

  it("should destroy the in-game window when pubg is terminated", fakeAsync(() => {
    _triggerPubgLaunched();
    tick(500);

    const spy = spyOn(fakeOverwolfCore, "closeWindow").and.returnValue(
      Observable.create()
    );
    fakeOverwolfCore.triggerPubgTerminated();
    tick(500);

    expect(spy).toHaveBeenCalledWith(WindowNames.IN_GAME);
  }));

  it("should show the in-game window", fakeAsync(() => {
    _triggerPubgLaunched();
    tick(500);

    const spy = spyOn(fakeOverwolfCore, "showWindow").and.returnValue(
      Observable.create()
    );

    fakeOverwolfCore.triggerHotkey(HotkeyActions.IN_GAME_MAP);
    tick(200);

    expect(spy).toHaveBeenCalledWith(WindowNames.IN_GAME);
  }));

  it("should hide the in-game window", fakeAsync(() => {
    _triggerPubgLaunched();
    tick(500);

    fakeOverwolfCore.triggerHotkey(HotkeyActions.IN_GAME_MAP);
    tick(200);

    const spy = spyOn(fakeOverwolfCore, "hideWindow").and.returnValue(
      Observable.create()
    );
    fakeOverwolfCore.triggerHotkey(HotkeyActions.IN_GAME_MAP);
    tick(200);

    expect(spy).toHaveBeenCalledWith(WindowNames.IN_GAME);
  }));

  afterEach(() => {
    controller.ngOnDestroy();
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
