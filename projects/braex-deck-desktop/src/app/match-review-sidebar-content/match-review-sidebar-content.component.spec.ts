import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { MatchReviewSidebarContentComponent } from "./match-review-sidebar-content.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("MatchReviewSidebarContentComponent", () => {
  let component: MatchReviewSidebarContentComponent;
  let fixture: ComponentFixture<MatchReviewSidebarContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MatchReviewSidebarContentComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchReviewSidebarContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
