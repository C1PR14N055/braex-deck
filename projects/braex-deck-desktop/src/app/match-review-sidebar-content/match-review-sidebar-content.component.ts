import { Component } from "@angular/core";

@Component({
  selector: "app-match-review-sidebar-content",
  templateUrl: "./match-review-sidebar-content.component.html",
  styleUrls: [
    "./match-review-sidebar-content.component.scss",
    "../../../../styles/theme/scroll.scss"
  ],
  host: { class: "w-100 h-100" }
})
export class MatchReviewSidebarContentComponent {}
