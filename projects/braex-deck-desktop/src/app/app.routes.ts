import { RouterModule, Routes } from "@angular/router";
import { GameDetailsComponent } from "./game-details/game-details.component";
import { RealTimeMapViewComponent } from "./real-time-map-view/real-time-map-view.component";
import { GameReviewMapViewComponent } from "./game-review-map-view/game-review-map-view.component";
import { HomeComponent } from "./home/home.component";

export const AppRoutes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "home" },
  { path: "home", component: HomeComponent },
  { path: "real-time", component: RealTimeMapViewComponent },
  {
    path: "game-review/:gameId",
    component: GameReviewMapViewComponent,
    runGuardsAndResolvers: "always"
  },
  { path: "gameDetails/:gameId", component: GameDetailsComponent },
  { path: "**", redirectTo: "home" }
];

export const AppRouting = RouterModule.forRoot(AppRoutes, {
  onSameUrlNavigation: "reload"
});
