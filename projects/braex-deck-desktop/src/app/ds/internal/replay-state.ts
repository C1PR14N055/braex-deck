import { ReplayEventType } from "./replay-event-type.enum";

export class ReplayState {
  constructor(
    public owner: string,
    public timestamp: number,
    public type: ReplayEventType
  ) {}
}
