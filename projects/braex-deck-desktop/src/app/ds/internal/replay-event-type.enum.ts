export enum ReplayEventType {
  PLAY = "play",
  PAUSE = "pause",
  GOTO = "goto"
}
