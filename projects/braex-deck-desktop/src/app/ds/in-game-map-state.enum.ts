export enum InGameMapState {
  VISIBLE = "visible",
  HIDDEN = "hidden"
}
