import { ExtReplayEventType } from "./ext-replay-event-type.enum";

export interface ExtReplayState {
  owner: string;
  time: number;
  type: ExtReplayEventType;
}
