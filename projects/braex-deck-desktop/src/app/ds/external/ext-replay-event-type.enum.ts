export enum ExtReplayEventType {
  PLAY = "PLAY",
  PAUSE = "PAUSE",
  GOTO = "GOTO"
}
