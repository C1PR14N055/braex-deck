import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { ToolbarModule } from "../../../braex-deck-shared/src/lib/toolbar/toolbar.module";
import { DropdownModule } from "../../../braex-deck-shared/src/lib/dropdown/dropdown.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ColorPickerDropdownModule } from "./color-picker-dropdown/color-picker-dropdown.module";
import { TooltipModule } from "../../../braex-deck-shared/src/lib/tooltip/tooltip.module";
import { ThicknessDropdownModule } from "./thickness-dropdown/thickness-dropdown.module";
import { SidebarModule } from "../../../braex-deck-shared/src/lib/sidebar/sidebar.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { MapCanvasModule } from "../../../braex-deck-shared/src/lib/map-canvas/map-canvas.module";
import { HttpClientModule } from "@angular/common/http";
// tslint:disable-next-line:max-line-length
import { CompactPlayerStatsModule } from "../../../braex-deck-shared/src/lib/compact-player-stats/compact-player-stats.module";
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatInputModule,
  MatMenuModule,
  MatRippleModule,
  MatSelectModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule
} from "@angular/material";
import { SettingsDialogComponent } from "./settings-dialog/settings-dialog.component";
import { DefaultModeOverlayComponent } from "./default-mode-overlay/default-mode-overlay.component";
// tslint:disable-next-line:max-line-length
import { CurrentLobbySidebarContentComponent } from "./current-lobby-sidebar-content/current-lobby-sidebar-content.component";
import { NgSelectModule } from "@ng-select/ng-select";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedPipesModule } from "../../../braex-deck-shared/src/lib/shared-pipes/shared-pipes.module";
import { StatusBarComponent } from "./status-bar/status-bar.component";
import { SatPopoverModule } from "@ncstate/sat-popover";
import { LocalPlayerNicknameComponent } from "./local-player-nickname/local-player-nickname.component";
import { PubgNicknameDialogComponent } from "./pubg-nickname-dialog/pubg-nickname-dialog.component";
import { SessionConnectBarComponent } from "./session-connect-bar/session-connect-bar.component";
import { CopyButtonModule } from "../../../braex-deck-shared/src/lib/copy-button/copy-button.module";
import { SuccessSnackBarModule } from "../../../braex-deck-shared/src/lib/success-snack-bar/success-snack-bar.module";
// tslint:disable-next-line:max-line-length
import { RecentMatchesSidebarContentComponent } from "./recent-matches-sidebar-content/recent-matches-sidebar-content.component";
// tslint:disable-next-line:max-line-length
import { CompactGameEntryModule } from "../../../braex-deck-shared/src/lib/compact-game-entry/compact-game-entry.module";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { ErrorSnackBarModule } from "../../../braex-deck-shared/src/lib/error-snack-bar/error-snack-bar.module";
import { LoadingOverlayModule } from "../../../braex-deck-shared/src/lib/loading-overlay/loading-overlay.module";
// tslint:disable-next-line:max-line-length
import { PlayerSearchSidebarContentComponent } from "./player-search-sidebar-content/player-search-sidebar-content.component";
import { SearchInputModule } from "../../../braex-deck-shared/src/lib/search-input/search-input.module";
import { SharedPlayerMatchSearchComponent } from "./shared-player-match-search/shared-player-match-search.component";
import { StatusBarMessageDirective } from "./directives/status-bar-message.directive";
import { RouterModule } from "@angular/router";
import { AppRouting } from "./app.routes";
import { GameDetailsComponent } from "./game-details/game-details.component";
import { MapIconModule } from "../../../braex-deck-shared/src/lib/map-icon/map-icon.module";
import { MapNameModule } from "../../../braex-deck-shared/src/lib/map-name/map-name.module";
import { GameModeIconModule } from "../../../braex-deck-shared/src/lib/game-mode-icon/game-mode-icon.module";
import { DurationModule } from "../../../braex-deck-shared/src/lib/duration/duration.module";
import { InfoButtonModule } from "../../../braex-deck-shared/src/lib/info-button/info-button.module";
import { ControlsHelpComponent } from "./controls-help/controls-help.component";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { OverlayModule } from "@angular/cdk/overlay";
import { QuickAccessMenuModule } from "../../../braex-deck-shared/src/lib/quick-access-menu/quick-access-menu.module";
import { ReviewControlsComponent } from "./review-controls/review-controls.component";
import { EnumerationModule } from "../../../braex-deck-shared/src/lib/enumeration/enumeration.module";
import { GameReviewMapViewComponent } from "./game-review-map-view/game-review-map-view.component";
import { RealTimeMapViewComponent } from "./real-time-map-view/real-time-map-view.component";
import { MapToolsToolbarComponent } from "./map-tools-toolbar/map-tools-toolbar.component";
import { HomeComponent } from "./home/home.component";
import { GameCardComponent } from "./game-card/game-card.component";
import { MapThumbnailModule } from "../../../braex-deck-shared/src/lib/map-thumbnail/map-thumbnail.module";
import { GameModeNameModule } from "../../../braex-deck-shared/src/lib/game-mode-name/game-mode-name.module";
import { SmallLoadingBarModule } from "../../../braex-deck-shared/src/lib/small-loading-bar/small-loading-bar.module";
import { DiscordHelpDialogComponent } from "./discord-help-dialog/discord-help-dialog.component";
import { ReviewTeamEntryComponent } from "./review-team-entry/review-team-entry.component";
import { MatchReviewSidebarContentComponent } from "./match-review-sidebar-content/match-review-sidebar-content.component";
import { EventSearchSidebarContentComponent } from "./event-search-sidebar-content/event-search-sidebar-content.component";
import { SharedEventMatchSearchComponent } from "./shared-event-match-search/shared-event-match-search.component";
import { WelcomeScreenComponent } from "./welcome-screen/welcome-screen.component";

@NgModule({
  declarations: [
    AppComponent,
    SettingsDialogComponent,
    DefaultModeOverlayComponent,
    CurrentLobbySidebarContentComponent,
    StatusBarComponent,
    LocalPlayerNicknameComponent,
    PubgNicknameDialogComponent,
    SessionConnectBarComponent,
    RecentMatchesSidebarContentComponent,
    PlayerSearchSidebarContentComponent,
    EventSearchSidebarContentComponent,
    SharedPlayerMatchSearchComponent,
    StatusBarMessageDirective,
    GameDetailsComponent,
    SharedEventMatchSearchComponent,
    ControlsHelpComponent,
    ReviewControlsComponent,
    GameReviewMapViewComponent,
    RealTimeMapViewComponent,
    MapToolsToolbarComponent,
    HomeComponent,
    GameCardComponent,
    DiscordHelpDialogComponent,
    ReviewTeamEntryComponent,
    MatchReviewSidebarContentComponent,
    WelcomeScreenComponent
  ],
  entryComponents: [
    SettingsDialogComponent,
    PubgNicknameDialogComponent,
    ControlsHelpComponent,
    DiscordHelpDialogComponent,
    WelcomeScreenComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToolbarModule,
    DropdownModule,
    ColorPickerDropdownModule,
    TooltipModule,
    ThicknessDropdownModule,
    SidebarModule,
    MapCanvasModule,
    FontAwesomeModule,
    HttpClientModule,
    CompactPlayerStatsModule,
    MatDialogModule,
    MatButtonModule,
    MatCheckboxModule,
    NgSelectModule,
    SharedPipesModule,
    ReactiveFormsModule,
    SatPopoverModule,
    MatRippleModule,
    CopyButtonModule,
    MatSnackBarModule,
    SuccessSnackBarModule,
    CompactGameEntryModule,
    ScrollingModule,
    ErrorSnackBarModule,
    LoadingOverlayModule,
    SearchInputModule,
    RouterModule,
    AppRouting,
    MapIconModule,
    MapNameModule,
    GameModeIconModule,
    DurationModule,
    MatTableModule,
    MatSortModule,
    InfoButtonModule,
    DragDropModule,
    MatMenuModule,
    OverlayModule,
    QuickAccessMenuModule,
    EnumerationModule,
    MapThumbnailModule,
    GameModeNameModule,
    MatTabsModule,
    MatSelectModule,
    MatInputModule,
    SmallLoadingBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
