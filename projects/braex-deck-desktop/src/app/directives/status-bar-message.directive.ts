import { Directive, HostListener, Input } from "@angular/core";
import { StatusBarService } from "../services/status-bar.service";

@Directive({
  selector: "[appStatusBarMessage]"
})
export class StatusBarMessageDirective {
  @Input("appStatusBarMessage")
  message = "";

  constructor(private _statusBarService: StatusBarService) {}

  @HostListener("mouseenter")
  onMouseEnter() {
    if (!this.message) {
      return;
    }

    this._statusBarService.message = this.message;
  }

  @HostListener("mouseleave")
  onMouseLeave() {
    if (!this.message) {
      return;
    }

    this._statusBarService.clearCurrentMessage();
  }
}
