import { StatusBarMessageDirective } from "./status-bar-message.directive";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Component, DebugElement } from "@angular/core";
import { By } from "@angular/platform-browser";
import { StatusBarService } from "../services/status-bar.service";

export const TEST_MESSAGE = "Some test message";

// noinspection AngularMissingOrInvalidDeclarationInModule
@Component({
  template: `
    <button appStatusBarMessage="${TEST_MESSAGE}">Text</button>
  `
})
class TestDirectiveComponent {}

describe("StatusBarMessageDirective", () => {
  let component: TestDirectiveComponent;
  let fixture: ComponentFixture<TestDirectiveComponent>;
  let buttonEl: DebugElement;
  let statusBarService: StatusBarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StatusBarMessageDirective, TestDirectiveComponent],
      providers: [StatusBarService]
    });

    fixture = TestBed.createComponent(TestDirectiveComponent);
    component = fixture.componentInstance;
    buttonEl = fixture.debugElement.query(By.css("button"));
    statusBarService = TestBed.get(StatusBarService);
    fixture.detectChanges();
  });

  it("should create an instance", () => {
    const directive = new StatusBarMessageDirective(statusBarService);
    expect(directive).toBeTruthy();
  });

  it("should set the message of the status bar when the mouse enters the host", () => {
    buttonEl.triggerEventHandler("mouseenter", null);
    fixture.detectChanges();
    expect(statusBarService.message).toEqual(TEST_MESSAGE);
  });

  it("should unset the message of the status bar when the mouse leaves the host", () => {
    buttonEl.triggerEventHandler("mouseenter", null);
    fixture.detectChanges();

    const spy = spyOn(statusBarService, "clearCurrentMessage");
    buttonEl.triggerEventHandler("mouseleave", null);
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
  });
});
