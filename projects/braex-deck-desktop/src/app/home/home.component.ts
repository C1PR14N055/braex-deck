import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from "@angular/core";
import { GameService } from "../../../../braex-deck-shared/src/lib/services/game.service";
import { LocalPlayerInfoService } from "../../../../braex-deck-shared/src/lib/services/local-player-info.service";
import { Subscription } from "rxjs";
import { faHistory, faSyncAlt } from "@fortawesome/free-solid-svg-icons";
import { MatSnackBar } from "@angular/material";
// tslint:disable-next-line:max-line-length
import { GameCardComponent } from "../game-card/game-card.component";
import { SCROLL_THRESHOLD } from "../shared/scroll";
import { BasePlayerGamesSearch } from "../shared/game-search/base-player-games-search";
import { EagerGame } from "../../../../braex-deck-shared/src/lib/ds/game/eager-game";

class ViewContext {
  faHistory = faHistory;
  faSyncAlt = faSyncAlt;
}

export const PAGE_SIZE = 30;

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: [
    "./home.component.scss",
    "../../styles/content-container.scss",
    "../../../../styles/theme/scroll.scss",
    "../../../../styles/theme/messages.scss"
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: { class: "h-100" }
})
export class HomeComponent extends BasePlayerGamesSearch
  implements OnInit, OnDestroy {
  readonly viewContext = new ViewContext();
  private _subs: Subscription[] = [];
  private _localPlayerNickname: string | null = null;
  @ViewChild("gameCardsContainer")
  private _gameCardsContainer: ElementRef<HTMLDivElement>;
  @ViewChildren(GameCardComponent, { read: ElementRef })
  private _gameCards: QueryList<ElementRef<HTMLElement>>;

  constructor(
    gameService: GameService,
    private _localPlayer: LocalPlayerInfoService,
    changeDetector: ChangeDetectorRef,
    snackBar: MatSnackBar
  ) {
    super(gameService, snackBar, changeDetector);
  }

  ngOnInit() {
    const sub = this._localPlayer.localPlayerNickname().subscribe(nickname => {
      this._localPlayerNickname = nickname;
      this._fetchMatchesOfPlayer(
        this._localPlayerNickname,
        this._gameCardsContainer.nativeElement,
        { size: PAGE_SIZE }
      );
    });
    this._subs.push(sub);
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  trackByGameId(index: number, item: EagerGame) {
    return item.info.gameId;
  }

  actionGameCardsScroll() {
    if (this._loading || this._currentPage === this._totalPages - 1) {
      return;
    }

    if (
      this._gameCardsContainer.nativeElement.scrollTop +
        this._gameCardsContainer.nativeElement.clientHeight >=
      this._gameCardsContainer.nativeElement.scrollHeight - SCROLL_THRESHOLD
    ) {
      this._fetchNextPage(
        this._localPlayerNickname,
        this._gameCardsContainer.nativeElement,
        PAGE_SIZE
      );
    }
  }

  actionRefresh() {
    this._fetchMatchesOfPlayer(
      this._localPlayerNickname,
      this._gameCardsContainer.nativeElement,
      { size: PAGE_SIZE }
    );
  }
}
