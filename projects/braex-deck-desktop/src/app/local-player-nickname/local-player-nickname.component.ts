import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from "@angular/core";
import { Subscription } from "rxjs";
import { faCheck, faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { SatPopover } from "@ncstate/sat-popover";
import { FormControl } from "@angular/forms";
import { AppSettingsService } from "../../../../braex-deck-shared/src/lib/services/app-settings.service";

class ViewContext {
  faPencilAlt = faPencilAlt;
  faCheck = faCheck;
}

@Component({
  selector: "app-local-player-nickname",
  templateUrl: "./local-player-nickname.component.html",
  styleUrls: ["./local-player-nickname.component.scss"]
})
export class LocalPlayerNicknameComponent implements OnInit, OnDestroy {
  readonly viewContext = new ViewContext();
  nicknameFormControl = new FormControl();
  private _localPlayerNickname: string;
  private _subs: Subscription[] = [];
  @ViewChild("editNicknamePopover")
  private _editNicknamePopover: SatPopover;
  @ViewChild("editNicknameWrapper")
  private _editNicknameWrapper: ElementRef<HTMLDivElement>;
  @ViewChild("wrapper")
  private _wrapper: ElementRef<HTMLDivElement>;

  constructor(
    private _appSettings: AppSettingsService,
    private _renderer: Renderer2
  ) {}

  ngOnInit() {
    const nickname = this._appSettings.localPlayerNickname;
    if (nickname) {
      this._localPlayerNickname = nickname;
      this.nicknameFormControl.setValue(nickname);
    }

    const sub = this._appSettings
      .localPlayerNicknameChanged()
      .subscribe(nickname => {
        if (this._localPlayerNickname !== nickname) {
          this._localPlayerNickname = nickname;
          this.nicknameFormControl.setValue(nickname);
        }
      });
    this._subs.push(sub);

    this._renderer.listen("document", "click", event => {
      if (event.target) {
        const target = event.target as HTMLElement;
        if (
          !this._wrapper.nativeElement.contains(target) &&
          !this._editNicknameWrapper.nativeElement.contains(target)
        ) {
          this._editNicknamePopover.close();
        }
      }
    });
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  actionApplyNickname() {
    if (this.nicknameFormControl.value) {
      this._appSettings.localPlayerNickname = (this.nicknameFormControl
        .value as string).trim();
    }
    this._editNicknamePopover.close();
  }

  get localPlayerNickname() {
    return this._localPlayerNickname;
  }
}
