import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { LocalPlayerNicknameComponent } from "./local-player-nickname.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("LocalPlayerNicknameComponent", () => {
  let component: LocalPlayerNicknameComponent;
  let fixture: ComponentFixture<LocalPlayerNicknameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LocalPlayerNicknameComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalPlayerNicknameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
