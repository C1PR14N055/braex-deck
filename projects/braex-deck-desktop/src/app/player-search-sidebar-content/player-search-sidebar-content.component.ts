import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ViewChild
} from "@angular/core";
import { SharedPlayerMatchSearchComponent } from "../shared-player-match-search/shared-player-match-search.component";
// tslint:disable-next-line:max-line-length
import { SearchInputComponent } from "../../../../braex-deck-shared/src/lib/search-input/search-input/search-input.component";

@Component({
  selector: "app-player-search-sidebar-content",
  templateUrl: "./player-search-sidebar-content.component.html",
  styleUrls: [
    "./player-search-sidebar-content.component.scss",
    "../../../../styles/theme/messages.scss"
  ],
  host: { class: "w-100 h-100" },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerSearchSidebarContentComponent implements AfterViewInit {
  @ViewChild(SharedPlayerMatchSearchComponent)
  private _sharedPlayerSearch: SharedPlayerMatchSearchComponent;
  @ViewChild(SearchInputComponent)
  private _searchInput: SearchInputComponent;
  private _currentlySearchedPlayer: string | null = null;

  ngAfterViewInit() {
    setTimeout(() => {
      this._searchInput.focus();
    }, 0);
  }

  actionSearchPlayer(nickname: string) {
    this._currentlySearchedPlayer = nickname;
    this._sharedPlayerSearch.searchPlayer(nickname);
  }

  actionRefresh() {
    if (!this._currentlySearchedPlayer) {
      return;
    }

    this._sharedPlayerSearch.searchPlayer(this._currentlySearchedPlayer);
  }

  get currentPlayer() {
    return this._sharedPlayerSearch.currentPlayer;
  }
}
