import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PlayerSearchSidebarContentComponent } from "./player-search-sidebar-content.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { SharedPlayerMatchSearchComponent } from "../shared-player-match-search/shared-player-match-search.component";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { MatSnackBarModule } from "@angular/material";
import { SearchInputComponent } from "../../../../braex-deck-shared/src/lib/search-input/search-input/search-input.component";

describe("PlayerSearchSidebarContentComponent", () => {
  let component: PlayerSearchSidebarContentComponent;
  let fixture: ComponentFixture<PlayerSearchSidebarContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PlayerSearchSidebarContentComponent,
        SharedPlayerMatchSearchComponent,
        SearchInputComponent
      ],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [HttpClientTestingModule, MatSnackBarModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerSearchSidebarContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
