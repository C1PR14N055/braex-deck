import { Component } from "@angular/core";

@Component({
  selector: "app-discord-help-dialog",
  templateUrl: "./discord-help-dialog.component.html",
  styleUrls: ["./discord-help-dialog.component.scss"]
})
export class DiscordHelpDialogComponent {}
