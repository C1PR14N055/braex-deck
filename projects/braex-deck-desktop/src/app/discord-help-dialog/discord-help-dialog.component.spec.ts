import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { DiscordHelpDialogComponent } from "./discord-help-dialog.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("DiscordHelpDialogComponent", () => {
  let component: DiscordHelpDialogComponent;
  let fixture: ComponentFixture<DiscordHelpDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DiscordHelpDialogComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscordHelpDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
