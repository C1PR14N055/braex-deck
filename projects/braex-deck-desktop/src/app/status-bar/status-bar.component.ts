import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { DrawingSyncService } from "../../../../braex-deck-shared/src/lib/map-canvas/services/drawing-sync.service";
import { SatPopover } from "@ncstate/sat-popover";
import { StatusBarService } from "../services/status-bar.service";
import { Subscription } from "rxjs";
import { TeamSessionService } from "../../../../braex-deck-shared/src/lib/map-canvas/services/team-session.service";

@Component({
  selector: "app-status-bar",
  templateUrl: "./status-bar.component.html",
  styleUrls: [
    "./status-bar.component.scss",
    "../../../../styles/theme/messages.scss"
  ]
})
export class StatusBarComponent implements OnInit, OnDestroy {
  @ViewChild("connectionPopover")
  private _connectionPopover: SatPopover;
  private _subs: Subscription[] = [];
  private _message = "";

  constructor(
    private _statusBarService: StatusBarService,
    private _teamSession: TeamSessionService
  ) {}

  ngOnInit() {
    this._initStatusBarServiceEvents();
  }

  private _initStatusBarServiceEvents() {
    const sub = this._statusBarService.messageChanged().subscribe(message => {
      if (this._message !== message) {
        this._message = message;
      }
    });
    this._subs.push(sub);
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  actionIndicatorMouseEnter() {
    this._connectionPopover.open();
  }

  actionIndicatorMouseLeave() {
    this._connectionPopover.close();
  }

  get message() {
    return this._message;
  }

  get sessionActive() {
    return this._teamSession.active;
  }
}
