import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ColorPickerDropdownComponent } from "./color-picker-dropdown/color-picker-dropdown.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [ColorPickerDropdownComponent],
  imports: [CommonModule, FontAwesomeModule],
  exports: [ColorPickerDropdownComponent],
  entryComponents: [ColorPickerDropdownComponent]
})
export class ColorPickerDropdownModule {}
