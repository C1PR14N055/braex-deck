import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Color } from "../../../../../braex-deck-shared/src/lib/map-canvas/ds/color.enum";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { DEFAULT_COLOR } from "../../../../../braex-deck-shared/src/lib/map-canvas/ds/default-drawing-config";

class ViewContext {
  faCheck = faCheck;
  color = Color;
}

@Component({
  selector: "app-color-picker-dropdown",
  templateUrl: "./color-picker-dropdown.component.html",
  styleUrls: ["./color-picker-dropdown.component.scss"]
})
export class ColorPickerDropdownComponent implements OnInit {
  viewContext = new ViewContext();
  private _colors: Color[][] = [];
  @Input()
  selectedColor = DEFAULT_COLOR;
  @Output()
  colorSelected = new EventEmitter<Color>();

  ngOnInit() {
    this._initColors();
  }

  private _initColors() {
    const colors: Color[] = [];
    Object.keys(Color).forEach(color => {
      colors.push(Color[color]);
    });

    for (let i = 0; i < colors.length; i += 2) {
      const result: Color[] = [];
      result.push(colors[i]);
      if (i + 1 < colors.length) {
        result.push(colors[i + 1]);
      }

      this._colors.push(result);
    }
  }

  actionSelectColor(color: Color) {
    this.selectedColor = color;
    this.colorSelected.emit(color);
  }

  get colors() {
    return this._colors;
  }
}
