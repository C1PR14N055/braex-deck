import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ColorPickerDropdownComponent } from "./color-picker-dropdown.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("ColorPickerDropdownComponent", () => {
  let component: ColorPickerDropdownComponent;
  let fixture: ComponentFixture<ColorPickerDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ColorPickerDropdownComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorPickerDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
