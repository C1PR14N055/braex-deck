import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { GameTrackerService } from "../../../../braex-deck-shared/src/lib/services/game-tracker.service";
import { Participant } from "../../../../braex-deck-shared/src/lib/ds/player/participant";
import { List } from "immutable";
import { GameMode } from "../../../../braex-deck-shared/src/lib/ds/game/game-mode.enum";
import { FormControl } from "@angular/forms";
import { Perspective } from "../../../../braex-deck-shared/src/lib/ds/game/perspective.enum";
import { AppSettingsService } from "../../../../braex-deck-shared/src/lib/services/app-settings.service";
import { OverwolfGameEventsService } from "../../../../braex-deck-shared/src/lib/overwolf/overwolf-game-events.service";
import { OverwolfCoreService } from "../../../../braex-deck-shared/src/lib/overwolf/overwolf-core.service";
import { LocalPlayerInfoService } from "../../../../braex-deck-shared/src/lib/services/local-player-info.service";

@Component({
  selector: "app-current-lobby-sidebar-content",
  templateUrl: "./current-lobby-sidebar-content.component.html",
  styleUrls: [
    "./current-lobby-sidebar-content.component.scss",
    "../../../../styles/theme/scroll.scss"
  ],
  host: { class: "w-100 h-100" }
})
export class CurrentLobbySidebarContentComponent implements OnInit, OnDestroy {
  defaultGameModeControl = new FormControl();
  preferredPerspectiveControl = new FormControl();
  private _subs: Subscription[] = [];
  private _currentLobbyParticipants = List<Participant>();
  private _displayDefaultModeSelection = true;
  private _displayPreferredPerspective = true;
  private _gameModes: GameMode[] = [];
  private _gameModeTypes: Perspective[] = [];
  private _preferredModeType: Perspective;
  private _inGame = false;
  private _localPlayerNickname: string;

  constructor(
    private _gameTrackerService: GameTrackerService,
    private _appSettings: AppSettingsService,
    private _gameEvents: OverwolfGameEventsService,
    private _overwolfCore: OverwolfCoreService,
    private _localPlayerInfo: LocalPlayerInfoService
  ) {}

  ngOnInit() {
    this._initLocalPlayerInfo();
    this._initGameEvents();
    this._initGameModes();
    this._initGameModeTypes();
    this._initDefaultGameModeSelection();
    this._initPreferredPerspective();
    this._initGameTrackerEvents();
  }

  private _initLocalPlayerInfo() {
    const sub = this._localPlayerInfo
      .localPlayerNickname()
      .subscribe(nickname => {
        this._localPlayerNickname = nickname;
      });
    this._subs.push(sub);
  }

  private _initGameEvents() {
    let sub = this._gameEvents.onPhaseChanged().subscribe(() => {
      this._inGame = true;
    });
    this._subs.push(sub);

    sub = this._gameEvents.onMatchCompleted().subscribe(() => {
      this._inGame = false;
    });
    this._subs.push(sub);

    sub = this._overwolfCore.onPubgTerminated().subscribe(() => {
      this._inGame = false;
    });
    this._subs.push(sub);
  }

  private _initGameModes() {
    Object.keys(GameMode).forEach(x => {
      this._gameModes.push(GameMode[x]);
    });
  }

  private _initGameModeTypes() {
    Object.keys(Perspective).forEach(x => {
      this._gameModeTypes.push(Perspective[x]);
    });
  }

  private _initDefaultGameModeSelection() {
    const defaultSearchGameMode = this._appSettings.defaultSearchGameMode;
    if (defaultSearchGameMode) {
      this._displayDefaultModeSelection = false;
    }

    let sub = this._appSettings.defaultSearchGameModeChanged().subscribe(x => {
      this._setDefaultSearchGameMode(x);
    });
    this._subs.push(sub);

    sub = this.defaultGameModeControl.valueChanges.subscribe(value => {
      this._setDefaultSearchGameMode(value);
    });
    this._subs.push(sub);
  }

  private _setDefaultSearchGameMode(mode: GameMode) {
    this._appSettings.defaultSearchGameMode = mode;
    this._displayDefaultModeSelection = false;
  }

  private _initPreferredPerspective() {
    const preferredModeType = this._appSettings.preferredPerspective;
    if (preferredModeType) {
      this._displayPreferredPerspective = false;
      this._preferredModeType = preferredModeType;
    }

    let sub = this._appSettings.preferredPerspectiveChanged().subscribe(x => {
      if (x) {
        this._setPreferredPerspective(x);
      }
    });
    this._subs.push(sub);

    sub = this.preferredPerspectiveControl.valueChanges.subscribe(value => {
      this._setPreferredPerspective(value);
    });
    this._subs.push(sub);
  }

  private _setPreferredPerspective(perspective: Perspective) {
    this._preferredModeType = perspective;
    this._appSettings.preferredPerspective = perspective;
    this._displayPreferredPerspective = false;
  }

  private _initGameTrackerEvents() {
    const sub = this._gameTrackerService.playersChanged().subscribe(players => {
      const matchingPlayers = players.filter(
        x => x.stats.nickname === this._localPlayerNickname
      );
      if (!matchingPlayers.isEmpty()) {
        const localPlayerIndex = players.indexOf(matchingPlayers.get(0));
        const localPlayer = matchingPlayers.get(0);
        this._currentLobbyParticipants = players
          .delete(localPlayerIndex)
          .insert(0, localPlayer);
      } else {
        this._currentLobbyParticipants = players;
      }
    });
    this._subs.push(sub);
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  participantsTrackBy(index: number, item: Participant) {
    return item.stats.nickname;
  }

  get currentLobbyParticipants() {
    return this._currentLobbyParticipants;
  }

  get displayDefaultModeSelection() {
    return this._displayDefaultModeSelection;
  }

  get gameModes() {
    return this._gameModes;
  }

  get gameModeTypes() {
    return this._gameModeTypes;
  }

  get displayPreferredPerspective() {
    return this._displayPreferredPerspective;
  }

  get inGame() {
    return this._inGame;
  }
}
