import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CurrentLobbySidebarContentComponent } from "./current-lobby-sidebar-content.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { LocalStorageService } from "../../../../braex-deck-shared/src/lib/services/local-storage.service";
import { GameTrackerService } from "../../../../braex-deck-shared/src/lib/services/game-tracker.service";
// tslint:disable-next-line:max-line-length
import { FakeGameTrackerService } from "../../../../braex-deck-shared/src/lib/services/testing/fake-game-tracker.service";
import { SharedPipesModule } from "../../../../braex-deck-shared/src/lib/shared-pipes/shared-pipes.module";

describe("CurrentLobbySidebarContentComponent", () => {
  let component: CurrentLobbySidebarContentComponent;
  let fixture: ComponentFixture<CurrentLobbySidebarContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CurrentLobbySidebarContentComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        LocalStorageService,
        { provide: GameTrackerService, useClass: FakeGameTrackerService }
      ],
      imports: [SharedPipesModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentLobbySidebarContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
