import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { LocalPlayerInfoService } from "../../../../braex-deck-shared/src/lib/services/local-player-info.service";
import { Subscription } from "rxjs";
import { faSyncAlt } from "@fortawesome/free-solid-svg-icons";
import { SharedPlayerMatchSearchComponent } from "../shared-player-match-search/shared-player-match-search.component";

class ViewContext {
  faSyncAlt = faSyncAlt;
}

@Component({
  selector: "app-recent-matches-sidebar-content",
  templateUrl: "./recent-matches-sidebar-content.component.html",
  styleUrls: [
    "./recent-matches-sidebar-content.component.scss",
    "../../../../styles/theme/scroll.scss",
    "../../../../styles/theme/messages.scss"
  ],
  host: { class: "w-100 h-100" }
})
export class RecentMatchesSidebarContentComponent implements OnInit, OnDestroy {
  readonly viewContext = new ViewContext();
  private _subs: Subscription[] = [];
  private _localPlayerNickname: string | null = null;
  @ViewChild(SharedPlayerMatchSearchComponent)
  private _sharedPlayerSearch: SharedPlayerMatchSearchComponent;

  constructor(private _localPlayerInfo: LocalPlayerInfoService) {}

  ngOnInit() {
    this._initLocalPlayerInfo();
  }

  private _initLocalPlayerInfo() {
    const sub = this._localPlayerInfo
      .localPlayerNickname()
      .subscribe(nickname => {
        this._localPlayerNickname = nickname;
        this._sharedPlayerSearch.searchPlayer(this._localPlayerNickname);
      });
    this._subs.push(sub);
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  actionRefreshMatches() {
    this._sharedPlayerSearch.searchPlayer(this._localPlayerNickname);
  }

  get games() {
    return this._sharedPlayerSearch.games;
  }

  get totalGames() {
    return this._sharedPlayerSearch.totalGames;
  }
}
