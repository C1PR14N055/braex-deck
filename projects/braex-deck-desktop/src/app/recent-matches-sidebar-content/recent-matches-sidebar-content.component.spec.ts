import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { RecentMatchesSidebarContentComponent } from "./recent-matches-sidebar-content.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { MatSnackBarModule } from "@angular/material";
import { SharedPlayerMatchSearchComponent } from "../shared-player-match-search/shared-player-match-search.component";

describe("RecentMatchesSidebarContentComponent", () => {
  let component: RecentMatchesSidebarContentComponent;
  let fixture: ComponentFixture<RecentMatchesSidebarContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RecentMatchesSidebarContentComponent,
        SharedPlayerMatchSearchComponent
      ],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [HttpClientTestingModule, MatSnackBarModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentMatchesSidebarContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
