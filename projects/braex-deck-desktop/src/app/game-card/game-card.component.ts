import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
  ViewChild
} from "@angular/core";
import { environment } from "../../environments/environment";
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";
import { Router } from "@angular/router";
import { EagerGame } from "../../../../braex-deck-shared/src/lib/ds/game/eager-game";

class ViewContext {
  dateTimeFormat = environment.dateTimeFormat;
}

@Component({
  selector: "app-game-card",
  templateUrl: "./game-card.component.html",
  styleUrls: [
    "./game-card.component.scss",
    "../../../../styles/shared-game.scss"
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger("detailsButton", [
      state(
        "visible",
        style({ height: "*", display: "*", padding: "*", opacity: 1 })
      ),
      state(
        "hidden",
        style({ height: 0, display: "none", padding: 0, opacity: 0 })
      ),
      transition("visible => hidden", animate("200ms ease-out")),
      transition("hidden => visible", animate("170ms ease-out"))
    ])
  ]
})
export class GameCardComponent implements OnInit {
  readonly viewContext = new ViewContext();
  @Input()
  game: EagerGame;
  @ViewChild("overlay")
  private _overlay: ElementRef<HTMLDivElement>;
  private _hovered = false;

  constructor(
    private _renderer: Renderer2,
    private _changeDetector: ChangeDetectorRef,
    private _router: Router
  ) {}

  ngOnInit() {
    this._renderer.listen(this._overlay.nativeElement, "mouseenter", () => {
      this._hovered = true;
      this._changeDetector.detectChanges();
    });
    this._renderer.listen(this._overlay.nativeElement, "mouseleave", () => {
      this._hovered = false;
      this._changeDetector.detectChanges();
    });
  }

  actionOpenDetails() {
    this._router.navigate(["/gameDetails", this.game.info.gameId]);
  }

  get hovered() {
    return this._hovered;
  }
}
