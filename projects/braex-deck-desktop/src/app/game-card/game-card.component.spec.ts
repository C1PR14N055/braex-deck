import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { GameCardComponent } from "./game-card.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { MapThumbnailModule } from "../../../../braex-deck-shared/src/lib/map-thumbnail/map-thumbnail.module";
import { GameModeNameModule } from "../../../../braex-deck-shared/src/lib/game-mode-name/game-mode-name.module";
import { MapNameModule } from "../../../../braex-deck-shared/src/lib/map-name/map-name.module";
import { RouterTestingModule } from "@angular/router/testing";
import { LazyGame } from "../../../../braex-deck-shared/src/lib/ds/game/lazy-game";
import * as Moment from "moment";
import { PubgGameMode } from "../../../../braex-deck-shared/src/lib/ds/game/pubg-game-mode.enum";
import { PubgMap } from "../../../../braex-deck-shared/src/lib/ds/game/pubg-map.enum";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { EagerGame } from "../../../../braex-deck-shared/src/lib/ds/game/eager-game";

describe("GameCardComponent", () => {
  let component: GameCardComponent;
  let fixture: ComponentFixture<GameCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameCardComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        MapThumbnailModule,
        GameModeNameModule,
        MapNameModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameCardComponent);
    component = fixture.componentInstance;
    component.game = new EagerGame(
      new LazyGame(
        1,
        "3c99daee-2f0f-4568-bc4c-6f971124a5c3",
        Moment.unix(1556713444),
        1488,
        PubgGameMode.ESPORTS_SQUAD_FPP,
        PubgMap.ERANGEL_MAIN,
        true
      )
    );
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
