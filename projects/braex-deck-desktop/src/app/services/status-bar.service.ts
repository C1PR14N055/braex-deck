import { Injectable } from "@angular/core";
import { Observable, Observer } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class StatusBarService {
  private _message = "";
  private _messageChangedObservers: Observer<string>[] = [];

  private _triggerMessageChanged(message: string) {
    this._messageChangedObservers.forEach(x => {
      x.next(message);
    });
  }

  messageChanged(): Observable<string> {
    return Observable.create(observer => {
      this._messageChangedObservers.push(observer);
    });
  }

  clearCurrentMessage() {
    this.message = "";
  }

  get message() {
    return this._message;
  }

  set message(value: string) {
    const changed = this._message !== value;
    this._message = value;

    if (changed) {
      this._triggerMessageChanged(value);
    }
  }
}
