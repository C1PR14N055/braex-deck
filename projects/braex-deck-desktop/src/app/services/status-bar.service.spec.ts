import { fakeAsync, TestBed, tick } from "@angular/core/testing";

import { StatusBarService } from "./status-bar.service";
import { Subscription } from "rxjs";

describe("StatusBarService", () => {
  let service: StatusBarService;
  let subs: Subscription[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StatusBarService]
    });

    service = TestBed.get(StatusBarService);
    subs = [];
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should set the current message", () => {
    const message = "some message";
    service.message = message;

    expect(service.message).toEqual(message);
  });

  it("should clear the message", () => {
    service.message = "some test message";
    service.clearCurrentMessage();
    expect(service.message).toEqual("");
  });

  it("should notify all observers when the current message changes", fakeAsync(() => {
    let receivedMessage: string;
    const sub = service.messageChanged().subscribe(m => {
      receivedMessage = m;
    });
    subs.push(sub);

    const message = "another test message";
    service.message = message;
    tick(300);
    expect(receivedMessage).toEqual(message);
  }));

  it("should notify all observers when clearing the message", fakeAsync(() => {
    let receivedMessage: string;
    const sub = service.messageChanged().subscribe(m => {
      receivedMessage = m;
    });
    subs.push(sub);

    service.message = "some test message";
    tick(300);

    service.clearCurrentMessage();
    tick(300);
    expect(receivedMessage).toEqual("");
  }));

  afterEach(() => {
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
