import { TestBed } from "@angular/core/testing";

import { ControlsHelpService } from "./controls-help.service";
import { OverlayModule } from "@angular/cdk/overlay";

describe("ControlsHelpService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [OverlayModule],
      providers: [ControlsHelpService]
    })
  );

  it("should be created", () => {
    const service: ControlsHelpService = TestBed.get(ControlsHelpService);
    expect(service).toBeTruthy();
  });
});
