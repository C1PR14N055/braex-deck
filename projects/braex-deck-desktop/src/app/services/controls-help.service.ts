import { ComponentRef, Injectable } from "@angular/core";
import { Overlay, OverlayRef } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";
import { ControlsHelpComponent } from "../controls-help/controls-help.component";

@Injectable({
  providedIn: "root"
})
export class ControlsHelpService {
  private _overlayRef: OverlayRef;
  private _portal: ComponentPortal<ControlsHelpComponent> | null = null;
  private _componentRef: ComponentRef<ControlsHelpComponent> | null = null;

  constructor(private _overlay: Overlay) {
    this._overlayRef = this._overlay.create();
  }

  open() {
    if (this._componentRef) {
      return;
    }

    this._portal = new ComponentPortal(ControlsHelpComponent);
    this._componentRef = this._overlayRef.attach(this._portal);
    this._componentRef.instance.open();
    this._componentRef.instance.close.subscribe(() => {
      this._overlayRef.detach();
      this._portal = null;
      this._componentRef = null;
    });
  }

  close() {
    if (!this._componentRef) {
      return;
    }

    this._componentRef.instance.actionClose();
  }
}
