import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { DefaultModeOverlayComponent } from "./default-mode-overlay.component";

describe("DefaultModeOverlayComponent", () => {
  let component: DefaultModeOverlayComponent;
  let fixture: ComponentFixture<DefaultModeOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DefaultModeOverlayComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultModeOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
