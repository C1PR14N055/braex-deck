import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
// tslint:disable-next-line:max-line-length
import { PlayerMovementCanvasModule } from "../../../../braex-deck-shared/src/lib/map-canvas/canvas-modules/modules/player-movement.canvas-module";
// tslint:disable-next-line:max-line-length max-line-length
import { RtLocalPlayerMovementService } from "../../../../braex-deck-shared/src/lib/map-canvas/services/rt-local-player-movement.service";
// tslint:disable-next-line:max-line-length
import { RtTeamMovementService } from "../../../../braex-deck-shared/src/lib/map-canvas/services/rt-team-movement.service";
import { MapCanvasComponent } from "../../../../braex-deck-shared/src/lib/map-canvas/map-canvas/map-canvas.component";
import { OverwolfGameEventsService } from "../../../../braex-deck-shared/src/lib/overwolf/overwolf-game-events.service";
import { OverwolfCoreService } from "../../../../braex-deck-shared/src/lib/overwolf/overwolf-core.service";
import { Subscription } from "rxjs";
// tslint:disable-next-line:max-line-length
import { MapBackgroundCanvasModule } from "../../../../braex-deck-shared/src/lib/map-canvas/canvas-modules/modules/map-background.canvas-module";
import { UNSUPPORTED_MAPS } from "../shared/unsupported-maps";

@Component({
  selector: "app-real-time-map-view",
  templateUrl: "./real-time-map-view.component.html",
  styleUrls: [
    "./real-time-map-view.component.scss",
    "../../styles/content-container.scss"
  ],
  host: { class: "h-100" }
})
export class RealTimeMapViewComponent
  implements OnInit, OnDestroy, AfterViewInit {
  private _subs: Subscription[] = [];
  @ViewChild(MapCanvasComponent)
  private _mapCanvas: MapCanvasComponent;

  // the unused services are actually necessary so that an instance of the service is created and it's not tree shaked
  constructor(
    private _playerMovementMod: PlayerMovementCanvasModule,
    private _rtLocalPlayerMovement: RtLocalPlayerMovementService,
    private _rtTeamMovement: RtTeamMovementService,
    private _gameEvents: OverwolfGameEventsService,
    private _core: OverwolfCoreService,
    private _mapBackground: MapBackgroundCanvasModule
  ) {}

  ngOnInit() {
    let sub = this._gameEvents.currentMap().subscribe(pubgMap => {
      if (UNSUPPORTED_MAPS.includes(pubgMap)) {
        console.warn(`The map \`${pubgMap}\` is not supported`);
      } else {
        this._mapBackground.currentMap = pubgMap;
      }
    });
    this._subs.push(sub);

    sub = this._gameEvents.onMatchLeft().subscribe(() => {
      this._mapBackground.currentMap = null;
    });
    this._subs.push(sub);

    sub = this._core.onPubgTerminated().subscribe(() => {
      this._mapBackground.currentMap = null;
    });
    this._subs.push(sub);
  }

  ngAfterViewInit() {
    this._mapCanvas.start(this._playerMovementMod);
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }
}
