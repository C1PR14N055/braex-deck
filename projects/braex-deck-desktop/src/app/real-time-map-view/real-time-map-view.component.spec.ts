import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from "@angular/core/testing";

import { RealTimeMapViewComponent } from "./real-time-map-view.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { MapCanvasModule } from "../../../../braex-deck-shared/src/lib/map-canvas/map-canvas.module";
// tslint:disable-next-line:max-line-length
import { MapBackgroundCanvasModule } from "../../../../braex-deck-shared/src/lib/map-canvas/canvas-modules/modules/map-background.canvas-module";
// tslint:disable-next-line:max-line-length
import { FakeOverwolfGameEventsService } from "../../../../braex-deck-shared/src/lib/overwolf/testing/fake-overwolf-game-events.service";
// tslint:disable-next-line:max-line-length
import { FakeOverwolfCoreService } from "../../../../braex-deck-shared/src/lib/overwolf/testing/fake-overwolf-core.service";
import { OverwolfGameEventsService } from "../../../../braex-deck-shared/src/lib/overwolf/overwolf-game-events.service";
import { OverwolfCoreService } from "../../../../braex-deck-shared/src/lib/overwolf/overwolf-core.service";
import { PubgMap } from "../../../../braex-deck-shared/src/lib/ds/game/pubg-map.enum";

describe("RealTimeMapViewComponent", () => {
  let component: RealTimeMapViewComponent;
  let fixture: ComponentFixture<RealTimeMapViewComponent>;
  let mapBackground: MapBackgroundCanvasModule;
  let fakeGameEvents: FakeOverwolfGameEventsService;
  let fakeCore: FakeOverwolfCoreService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RealTimeMapViewComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [MapCanvasModule],
      providers: [
        MapBackgroundCanvasModule,
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        },
        { provide: OverwolfCoreService, useClass: FakeOverwolfCoreService }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealTimeMapViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    mapBackground = TestBed.get(MapBackgroundCanvasModule);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    fakeCore = TestBed.get(OverwolfCoreService);
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should set the map when receiving it from overwolf", fakeAsync(() => {
    expect(mapBackground.currentMap).toBeNull();
    const pubgMap = PubgMap.DIHOROTOK_MAIN;
    fakeGameEvents.triggerCurrentMap(pubgMap);
    tick(200);

    expect(mapBackground.currentMap).toEqual(pubgMap);
  }));

  it("should reset the map when the match is left", fakeAsync(() => {
    fakeGameEvents.triggerCurrentMap(PubgMap.SAVAGE_MAIN);
    tick(200);

    expect(mapBackground.currentMap).toBeTruthy();
    fakeGameEvents.triggerMatchLeft();
    tick(200);
    expect(mapBackground.currentMap).toBeNull();
  }));

  it("should reset the map when pubg is terminated", fakeAsync(() => {
    fakeGameEvents.triggerCurrentMap(PubgMap.ERANGEL_MAIN);
    tick(200);

    expect(mapBackground.currentMap).toBeTruthy();
    fakeCore.triggerPubgTerminated();
    tick(200);
    expect(mapBackground.currentMap).toBeNull();
  }));
});
