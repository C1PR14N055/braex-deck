import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ThicknessDropdownComponent } from "./thickness-dropdown.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("ThicknessDropdownComponent", () => {
  let component: ThicknessDropdownComponent;
  let fixture: ComponentFixture<ThicknessDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThicknessDropdownComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThicknessDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
