import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from "@angular/core";
import { DEFAULT_BRUSH_THICKNESS } from "../../../../../braex-deck-shared/src/lib/map-canvas/ds/default-drawing-config";
import { BrushConfigService } from "../../../../../braex-deck-shared/src/lib/map-canvas/services/brush-config.service";

export const MIN_THICKNESS = 1;
export const MAX_THICKNESS = 15;

class ViewContext {
  minThickness = MIN_THICKNESS;
  maxThickness = MAX_THICKNESS;
}

@Component({
  selector: "app-thickness-dropdown",
  templateUrl: "./thickness-dropdown.component.html",
  styleUrls: ["./thickness-dropdown.component.scss"]
})
export class ThicknessDropdownComponent implements AfterViewInit, OnInit {
  viewContext = new ViewContext();
  @ViewChild("thicknessInput")
  private _thicknessInput: ElementRef<HTMLInputElement>;
  private _thickness = DEFAULT_BRUSH_THICKNESS;

  constructor(private _drawingConfigService: BrushConfigService) {}

  ngOnInit() {
    this._thickness = this._drawingConfigService.thickness;
  }

  ngAfterViewInit() {
    setTimeout(() => this._thicknessInput.nativeElement.focus(), 0);
  }

  actionThicknessInputBlur() {
    if (this.thickness > MAX_THICKNESS) {
      this.thickness = MAX_THICKNESS;
    } else if (this.thickness < MIN_THICKNESS) {
      this.thickness = MIN_THICKNESS;
    }
  }

  get thickness() {
    return this._thickness;
  }

  set thickness(value: number) {
    this._thickness = value;
    this._drawingConfigService.thickness = value;
  }
}
