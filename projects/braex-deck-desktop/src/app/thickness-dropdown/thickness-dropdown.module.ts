import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ThicknessDropdownComponent } from "./thickness-dropdown/thickness-dropdown.component";
import { MatSliderModule } from "@angular/material";
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [ThicknessDropdownComponent],
  imports: [CommonModule, MatSliderModule, FormsModule],
  exports: [ThicknessDropdownComponent],
  entryComponents: [ThicknessDropdownComponent]
})
export class ThicknessDropdownModule {}
