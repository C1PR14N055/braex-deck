export const environment = {
  production: true,
  apiUrl: "http://localhost:5000",
  apiEndpoints: {
    getPlayerSeasonStats: "/playerSeasonStats/{playerName}"
  },
  dateTimeFormat: "MMM dd, yyyy HH:mm",
  webSockets: {
    reviewSync: {
      brokerUrl: "ws://team.cgs.gg/teamComm/websocket",
      pushPath: "/topic/{sessionId}/replay",
      pullPath: "/topic/{sessionId}/replay"
    }
  }
};
