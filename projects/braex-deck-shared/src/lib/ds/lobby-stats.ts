export class LobbyStats {
  private constructor(
    public kdRatio = 0,
    public winRatio = 0,
    public headshotRatio = 0
  ) {}

  static create(kdRatio = 0, winRatio = 0, headshotRatio = 0) {
    return Object.freeze(new LobbyStats(kdRatio, winRatio, headshotRatio));
  }
}
