import { MappingError } from "../../../error/mapping.error";

export enum ExtPUBGMap {
  ERANGEL_MAIN = "ERANGEL_MAIN",
  DESERT_MAIN = "DESERT_MAIN",
  SAVAGE_MAIN = "SAVAGE_MAIN",
  RANGE_MAIN = "RANGE_MAIN",
  DIHOROTOK_MAIN = "DIHOROTOK_MAIN"
}

export function reverseExtPUBGMapMapping(value: string) {
  switch (value) {
    case "ERANGEL_MAIN":
      return ExtPUBGMap.ERANGEL_MAIN;
    case "DESERT_MAIN":
      return ExtPUBGMap.DESERT_MAIN;
    case "SAVAGE_MAIN":
      return ExtPUBGMap.SAVAGE_MAIN;
    case "RANGE_MAIN":
      return ExtPUBGMap.RANGE_MAIN;
    case "DIHOROTOK_MAIN":
      return ExtPUBGMap.DIHOROTOK_MAIN;
  }

  throw new MappingError(`No mapping found for the ExtPUBGMap ${value}`);
}
