import { MappingError } from "../../../error/mapping.error";

export enum ExtGameMode {
  DUO = "DUO",
  DUO_FPP = "DUO_FPP",
  SOLO = "SOLO",
  SOLO_FPP = "SOLO_FPP",
  SQUAD = "SQUAD",
  SQUAD_FPP = "SQUAD_FPP",
  WAR = "WAR",
  WAR_SOLO = "WAR_SOLO",
  WAR_SOLO_FPP = "WAR_SOLO_FPP",
  WAR_DUO = "WAR_DUO",
  WAR_DUO_FPP = "WAR_DUO_FPP",
  WAR_SQUAD = "WAR_SQUAD",
  WAR_SQUAD_FPP = "WAR_SQUAD_FPP",
  ZOMBIE = "ZOMBIE",
  ZOMBIE_SOLO = "ZOMBIE_SOLO",
  ZOMBIE_SOLO_FPP = "ZOMBIE_SOLO_FPP",
  ZOMBIE_DUO = "ZOMBIE_DUO",
  ZOMBIE_DUO_FPP = "ZOMBIE_DUO_FPP",
  ZOMBIE_SQUAD = "ZOMBIE_SQUAD",
  ZOMBIE_SQUAD_FPP = "ZOMBIE_SQUAD_FPP",
  CONQUEST = "CONQUEST",
  CONQUEST_SOLO = "CONQUEST_SOLO",
  CONQUEST_SOLO_FPP = "CONQUEST_SOLO_FPP",
  CONQUEST_DUO = "CONQUEST_DUO",
  CONQUEST_DUO_FPP = "CONQUEST_DUO_FPP",
  CONQUEST_SQUAD = "CONQUEST_SQUAD",
  CONQUEST_SQUAD_FPP = "CONQUEST_SQUAD_FPP",
  ESPORTS = "ESPORTS",
  ESPORTS_SOLO = "ESPORTS_SOLO",
  ESPORTS_SOLO_FPP = "ESPORTS_SOLO_FPP",
  ESPORTS_DUO = "ESPORTS_DUO",
  ESPORTS_DUO_FPP = "ESPORTS_DUO_FPP",
  ESPORTS_SQUAD = "ESPORTS_SQUAD",
  ESPORTS_SQUAD_FPP = "ESPORTS_SQUAD_FPP",
  NORMAL = "NORMAL",
  NORMAL_DUO = "NORMAL_DUO",
  NORMAL_DUO_FPP = "NORMAL_DUO_FPP",
  NORMAL_SOLO = "NORMAL_SOLO",
  NORMAL_SOLO_FPP = "NORMAL_SOLO_FPP",
  NORMAL_SQUAD = "NORMAL_SQUAD",
  NORMAL_SQUAD_FPP = "NORMAL_SQUAD_FPP"
}

export function reverseExtGameModeMapping(value: string): ExtGameMode {
  switch (value) {
    case "SOLO":
      return ExtGameMode.SOLO;
    case "SOLO_FPP":
      return ExtGameMode.SOLO_FPP;
    case "DUO":
      return ExtGameMode.DUO;
    case "DUO_FPP":
      return ExtGameMode.DUO_FPP;
    case "SQUAD":
      return ExtGameMode.SQUAD;
    case "SQUAD_FPP":
      return ExtGameMode.SQUAD_FPP;
    case "NORMAL":
      return ExtGameMode.NORMAL;
    case "NORMAL_DUO":
      return ExtGameMode.NORMAL_DUO;
    case "NORMAL_DUO_FPP":
      return ExtGameMode.NORMAL_DUO_FPP;
    case "NORMAL_SOLO":
      return ExtGameMode.NORMAL_SOLO;
    case "NORMAL_SOLO_FPP":
      return ExtGameMode.NORMAL_SOLO_FPP;
    case "NORMAL_SQUAD":
      return ExtGameMode.NORMAL_SQUAD;
    case "NORMAL_SQUAD_FPP":
      return ExtGameMode.NORMAL_SQUAD_FPP;
    case "WAR":
      return ExtGameMode.WAR;
    case "WAR_SOLO":
      return ExtGameMode.WAR_SOLO;
    case "WAR_SOLO_FPP":
      return ExtGameMode.WAR_SOLO_FPP;
    case "WAR_DUO":
      return ExtGameMode.WAR_DUO;
    case "WAR_DUO_FPP":
      return ExtGameMode.WAR_DUO_FPP;
    case "WAR_SQUAD":
      return ExtGameMode.WAR_SQUAD;
    case "WAR_SQUAD_FPP":
      return ExtGameMode.WAR_SQUAD_FPP;
    case "ZOMBIE":
      return ExtGameMode.ZOMBIE;
    case "ZOMBIE_SOLO":
      return ExtGameMode.ZOMBIE_SOLO;
    case "ZOMBIE_SOLO_FPP":
      return ExtGameMode.ZOMBIE_SOLO_FPP;
    case "ZOMBIE_DUO":
      return ExtGameMode.ZOMBIE_DUO;
    case "ZOMBIE_DUO_FPP":
      return ExtGameMode.ZOMBIE_DUO_FPP;
    case "ZOMBIE_SQUAD":
      return ExtGameMode.ZOMBIE_SQUAD;
    case "ZOMBIE_SQUAD_FPP":
      return ExtGameMode.ZOMBIE_SQUAD_FPP;
    case "CONQUEST":
      return ExtGameMode.CONQUEST;
    case "CONQUEST_SOLO":
      return ExtGameMode.CONQUEST_SOLO;
    case "CONQUEST_SOLO_FPP":
      return ExtGameMode.CONQUEST_SOLO_FPP;
    case "CONQUEST_DUO":
      return ExtGameMode.CONQUEST_DUO;
    case "CONQUEST_DUO_FPP":
      return ExtGameMode.CONQUEST_DUO_FPP;
    case "CONQUEST_SQUAD":
      return ExtGameMode.CONQUEST_SQUAD;
    case "CONQUEST_SQUAD_FPP":
      return ExtGameMode.CONQUEST_SQUAD_FPP;
    case "ESPORTS":
      return ExtGameMode.ESPORTS;
    case "ESPORTS_SOLO":
      return ExtGameMode.ESPORTS_SOLO;
    case "ESPORTS_SOLO_FPP":
      return ExtGameMode.ESPORTS_SOLO_FPP;
    case "ESPORTS_DUO":
      return ExtGameMode.ESPORTS_DUO;
    case "ESPORTS_DUO_FPP":
      return ExtGameMode.ESPORTS_DUO_FPP;
    case "ESPORTS_SQUAD":
      return ExtGameMode.ESPORTS_SQUAD;
    case "ESPORTS_SQUAD_FPP":
      return ExtGameMode.ESPORTS_SQUAD_FPP;
  }

  throw new MappingError(`No mapping found for the ExtGameMode ${value}`);
}
