export class ExtGameParticipant {
  id: number;
  created: number;
  updated: number;
  dbnos: number;
  assists: number;
  boosts: number;
  damageDealt: number;
  deathType: string;
  // noinspection SpellCheckingInspection
  headshotkills: number;
  heals: number;
  killPlace: number;
  killStreaks: number;
  kills: number;
  lastKillPoints: number;
  lastWinPoints: number;
  longestKill: number;
  mostDamage: number;
  name: string;
  playerId: string;
  rankPoints: number;
  revives: number;
  rideDistance: number;
  roadKills: number;
  swimDistance: number;
  teamKills: number;
  timeSurvived: number;
  vehicleDestroys: number;
  walkDistance: number;
  weaponsAcquired: number;
  winPlace: number;
}
