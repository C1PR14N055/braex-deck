import { MappingError } from "../../../error/mapping.error";

export enum ExtPlatformRegion {
  STEAM = "STEAM",
  KAKAO = "KAKAO",
  XBOX = "XBOX",
  TOURNAMENT = "TOURNAMENT",
  XBOX_AS = "XBOX_AS",
  XBOX_EU = "XBOX_EU",
  XBOX_NA = "XBOX_NA",
  XBOX_OC = "XBOX_OC",
  PC_KRJP = "PC_KRJP",
  PC_JP = "PC_JP",
  PC_NA = "PC_NA",
  PC_EU = "PC_EU",
  PC_OC = "PC_OC",
  PC_KAKAO = "PC_KAKAO",
  PC_SEA = "PC_SEA",
  PC_SA = "PC_SA",
  PC_AS = "PC_AS"
}

export function reverseExtPlatformRegionMapping(
  value: string
): ExtPlatformRegion {
  switch (value) {
    case "STEAM":
      return ExtPlatformRegion.STEAM;
    case "KAKAO":
      return ExtPlatformRegion.KAKAO;
    case "XBOX":
      return ExtPlatformRegion.XBOX;
    case "TOURNAMENT":
      return ExtPlatformRegion.TOURNAMENT;
    case "XBOX_AS":
      return ExtPlatformRegion.XBOX_AS;
    case "XBOX_EU":
      return ExtPlatformRegion.XBOX_EU;
    case "XBOX_NA":
      return ExtPlatformRegion.XBOX_NA;
    case "XBOX_OC":
      return ExtPlatformRegion.XBOX_OC;
    case "PC_KRJP":
      return ExtPlatformRegion.PC_KRJP;
    case "PC_JP":
      return ExtPlatformRegion.PC_JP;
    case "PC_NA":
      return ExtPlatformRegion.PC_NA;
    case "PC_EU":
      return ExtPlatformRegion.PC_EU;
    case "PC_OC":
      return ExtPlatformRegion.PC_OC;
    case "PC_KAKAO":
      return ExtPlatformRegion.PC_KAKAO;
    case "PC_SEA":
      return ExtPlatformRegion.PC_SEA;
    case "PC_SA":
      return ExtPlatformRegion.PC_SA;
    case "PC_AS":
      return ExtPlatformRegion.PC_AS;
  }

  throw new MappingError(`No mapping found for the ExtPlatformRegion ${value}`);
}
