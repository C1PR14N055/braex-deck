import { ExtGameMode } from "./ext-game-mode.enum";
import { ExtPlatformRegion } from "./ext-platform-region.enum";
import { ExtPUBGMap } from "./ext-pubg-map.enum";

export interface ExtLazyGame {
  id: number;
  created: number;
  updated: number;
  type: string;
  matchId: string;
  createdAt: number;
  duration: number;
  gameMode: ExtGameMode;
  patchVersion: string;
  seasonState: string;
  shardId: ExtPlatformRegion;
  titleId: string;
  mapName: ExtPUBGMap;
  isCustomMatch: boolean;
  telemetryUrl: string;
}
