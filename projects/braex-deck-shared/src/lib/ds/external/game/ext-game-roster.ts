import { ExtGameParticipant } from "./ext-game-participant";

export interface ExtGameRoster {
  id: number;
  rank: number;
  teamId: number;
  won: boolean;
  participants: ExtGameParticipant[];
}
