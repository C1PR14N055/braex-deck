export interface ExtPageable {
  sort: {
    sorted: boolean;
    unsorted: boolean;
  };
  pageSize: number;
  pageNumber: number;
  offset: number;
  // noinspection SpellCheckingInspection
  unpaged: boolean;
  paged: boolean;
}
