import { ExtPageable } from "./ext-pageable";

export interface ExtPageableResponse<T> {
  content: T[];
  pageable: ExtPageable;
  last: boolean;
  totalPages: number;
  totalElements: number;
  first: boolean;
  sort: {
    sorted: boolean;
    unsorted: boolean;
  };
  numberOfElements: number;
  size: number;
  number: number;
}
