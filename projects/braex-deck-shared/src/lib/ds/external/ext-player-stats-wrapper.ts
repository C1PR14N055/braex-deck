import { ExtPlayerStats } from "./ext-player-stats";

export interface ExtPlayerStatsWrapper {
  season: string;
  playerName: string;
  gameMode: string;
  stats: ExtPlayerStats;
}
