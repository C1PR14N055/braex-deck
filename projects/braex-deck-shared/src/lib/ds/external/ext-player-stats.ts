export interface ExtPlayerStats {
  avgKills: number;
  top10Rate: number;
  avgDmg: number;
  longestKill: number;
  avgSurvivedTime: number;
  mostKills: number;
  matches: number;
  headshotRate: number;
  kdRate: number;
  winRate: number;
  rankPointsTitle: string;
  rankPoints: number;
}
