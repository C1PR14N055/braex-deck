/**
 * Contains the names of the hotkey actions which are defined in the overwolf manifest.json.
 * If you want to add a new hotkey it needs to be done in the overwolf manifest.
 */
export enum HotkeyActions {
  /**
   * Toggle the in-game map.
   */
  IN_GAME_MAP = "braex_in_game_map"
}
