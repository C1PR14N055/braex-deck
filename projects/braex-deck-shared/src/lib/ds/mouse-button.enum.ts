export enum MouseButton {
  LMB = 0,
  MMB = 1,
  RMB = 2
}
