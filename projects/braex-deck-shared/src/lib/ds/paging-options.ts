export const DEFAULT_PAGE_NUMBER = 0;
export const DEFAULT_PAGE_SIZE = 15;

export interface PagingOptions {
  /**
   * The index of the page that wants to be retrieved (0..N).
   * Default: {@link DEFAULT_PAGE_NUMBER}
   */
  page?: number;
  /**
   * The number of records per page.
   * Default: {@link DEFAULT_PAGE_SIZE}
   */
  size?: number;
}
