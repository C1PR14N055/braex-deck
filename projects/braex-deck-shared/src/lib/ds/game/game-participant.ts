import * as Moment from "moment";

export class GameParticipant {
  constructor(
    public id?: number,
    public created?: Moment.Moment,
    public updated?: Moment.Moment,
    public dbnos?: number,
    public assists?: number,
    public boosts?: number,
    public damageDealt?: number,
    public deathType?: string,
    public headshotKills?: number,
    public heals?: number,
    public killPlace?: number,
    public killStreaks?: number,
    public kills?: number,
    public lastKillPoints?: number,
    public lastWinPoints?: number,
    public longestKill?: number,
    public mostDamage?: number,
    public name?: string,
    public playerId?: string,
    public rankPoints?: number,
    public revives?: number,
    public rideDistance?: number,
    public roadKills?: number,
    public swimDistance?: number,
    public teamKills?: number,
    public timeSurvived?: number,
    public vehiclesDestroyed?: number,
    public walkDistance?: number,
    public weaponsAcquired?: number,
    public winPlace?: number
  ) {}
}
