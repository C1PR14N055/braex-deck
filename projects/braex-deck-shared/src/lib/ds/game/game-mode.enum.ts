export enum GameMode {
  SOLO = "solo",
  DUO = "duo",
  SQUAD = "squad"
}
