export enum PubgMap {
  DESERT_MAIN = "desert-main",
  ERANGEL_MAIN = "erangel-main",
  SAVAGE_MAIN = "savage-main",
  DIHOROTOK_MAIN = "dihorotok-main",
  RANGE_MAIN = "range-main"
}
