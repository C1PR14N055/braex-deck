export enum PlatformRegion {
  STEAM = "steam",
  KAKAO = "kakao",
  XBOX = "xbox",
  TOURNAMENT = "tournament"
}

export function reversePlatformRegionMapping(
  value: string
): PlatformRegion | null {
  switch (value) {
    case "steam":
      return PlatformRegion.STEAM;
    case "kakao":
      return PlatformRegion.KAKAO;
    case "xbox":
      return PlatformRegion.XBOX;
    case "tournament":
      return PlatformRegion.TOURNAMENT;
  }

  return null;
}
