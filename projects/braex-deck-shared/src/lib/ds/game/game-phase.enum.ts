export enum GamePhase {
  LOBBY = "lobby",
  LOADING_SCREEN = "loading-screen",
  AIRFIELD = "airfield",
  AIRCRAFT = "aircraft",
  FREE_FLY = "free-fly",
  LANDED = "landed"
}
