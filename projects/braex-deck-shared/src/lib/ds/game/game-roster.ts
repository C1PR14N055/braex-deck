import { List } from "immutable";
import { GameParticipant } from "./game-participant";

export class GameRoster {
  constructor(
    public id?: number,
    public rank?: number,
    public teamId?: number,
    public won?: boolean,
    public participants = List<GameParticipant>()
  ) {}
}
