import * as Moment from "moment";
import { PubgMap } from "./pubg-map.enum";
import { PubgGameMode } from "./pubg-game-mode.enum";

export class LazyGame {
  constructor(
    public id?: number,
    public gameId?: string,
    public createdAt?: Moment.Moment,
    public duration?: number,
    public gameMode?: PubgGameMode,
    public map?: PubgMap,
    public isCustomGame = false,
    public telemetryUrl?: string
  ) {}
}
