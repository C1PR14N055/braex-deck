import { LazyGame } from "./lazy-game";
import { GameRoster } from "./game-roster";
import { List } from "immutable";

export class EagerGame {
  constructor(
    public info?: LazyGame,
    public stats?: List<GameRoster>,
    public totalTeams?: number,
    public placement?: number,
    public kills = 0,
    public headshots = 0
  ) {}
}
