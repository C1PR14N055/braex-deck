import { List } from "immutable";

export class Pageable<T> {
  content = List<T>();
  pageSize = 0;
  pageNumber = 0;
  offset = 0;
  last = true;
  totalPages = 0;
  totalElements = 0;
  numberOfElements = 0;
}
