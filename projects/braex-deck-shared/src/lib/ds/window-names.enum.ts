/**
 * Contains the names of the windows which exist in the application.
 * Note that new windows need to be registered in the overwolf manifest
 * to be actually created/considered.
 */
export enum WindowNames {
  MAIN_WINDOW = "desktop_only",
  IN_GAME = "in_game"
}
