export class PlayerStats {
  constructor(
    public nickname?: string,
    public kdRatio = 0,
    public winRatio = 0,
    public headshotRatio = 0,
    public averageKills = 0,
    public top10Ratio = 0,
    public averageDamage = 0,
    public longestKill = 0,
    public averageSurvivedTime = 0,
    public mostKills = 0,
    public playedMatches = 0,
    public rankTitle?: string,
    public rankPoints = 0
  ) {}
}
