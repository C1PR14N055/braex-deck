import { PlayerStats } from "./player-stats";

export class Participant {
  constructor(public friendly = false, public stats: PlayerStats) {}
}
