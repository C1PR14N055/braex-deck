import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "enumeration"
})
export class EnumerationPipe implements PipeTransform {
  transform(value: number, args?: any) {
    if (!value) {
      return value.toString();
    }

    return value.toString() + this._getExtension(value);
  }

  private _getExtension(value: number) {
    if ((value > 0 && value < 10) || value > 20) {
      const valueString: string = value.toString();
      const lastDigit: number = Number(valueString[valueString.length - 1]);

      if (lastDigit === 1) {
        return "st";
      } else if (lastDigit === 2) {
        return "nd";
      } else if (lastDigit === 3) {
        return "rd";
      }
    }

    return "th";
  }
}
