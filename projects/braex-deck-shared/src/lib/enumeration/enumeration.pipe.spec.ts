import { EnumerationPipe } from "./enumeration.pipe";

describe("EnumerationPipe", () => {
  let pipe: EnumerationPipe;

  beforeEach(() => {
    pipe = new EnumerationPipe();
  });

  it("create an instance", () => {
    expect(pipe).toBeTruthy();
  });

  it("should correctly transform 1", () => {
    expect(pipe.transform(1)).toEqual("1st");
  });

  it("should correctly transform 2", () => {
    expect(pipe.transform(2)).toEqual("2nd");
  });

  it("should correctly transform 3", () => {
    expect(pipe.transform(3)).toEqual("3rd");
  });

  it("should correctly transform 4", () => {
    expect(pipe.transform(4)).toEqual("4th");
  });

  it("should correctly transform 10", () => {
    expect(pipe.transform(10)).toEqual("10th");
  });

  it("should correctly transform 11", () => {
    expect(pipe.transform(11)).toEqual("11th");
  });

  it("should correctly transform 12", () => {
    expect(pipe.transform(12)).toEqual("12th");
  });

  it("should correctly transform 13", () => {
    expect(pipe.transform(13)).toEqual("13th");
  });

  it("should correctly transform 21", () => {
    expect(pipe.transform(21)).toEqual("21st");
  });

  it("should correctly transform 22", () => {
    expect(pipe.transform(22)).toEqual("22nd");
  });

  it("should correctly transform 23", () => {
    expect(pipe.transform(23)).toEqual("23rd");
  });

  it("should correctly transform 24", () => {
    expect(pipe.transform(24)).toEqual("24th");
  });
});
