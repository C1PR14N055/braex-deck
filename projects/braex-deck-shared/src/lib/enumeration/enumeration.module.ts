import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EnumerationPipe } from "./enumeration.pipe";

@NgModule({
  declarations: [EnumerationPipe],
  imports: [CommonModule],
  exports: [EnumerationPipe]
})
export class EnumerationModule {}
