import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from "@angular/core/testing";

import { ZoomIndicatorComponent } from "./zoom-indicator.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import BigNumber from "bignumber.js";
import { Subscription } from "rxjs";
import {
  DEFAULT_ZOOM,
  MAX_ZOOM,
  MIN_ZOOM,
  ZOOM_STEP_SIZE
} from "../rendering/camera-2d";

describe("ZoomIndicatorComponent", () => {
  let component: ZoomIndicatorComponent;
  let fixture: ComponentFixture<ZoomIndicatorComponent>;
  const defaultZoomOffset = Object.freeze(new BigNumber("100"));
  let subs: Subscription[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ZoomIndicatorComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomIndicatorComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
    subs = [];
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should compute the slider knob offset correctly when the zoom is MIN_ZOOM", () => {
    expect(component.zoom.isEqualTo(DEFAULT_ZOOM)).toBeTruthy();
    expect(component.sliderKnobOffset.toPrecision(4)).toBe(
      defaultZoomOffset.toPrecision(4)
    );

    component.zoom = new BigNumber(MIN_ZOOM);
    expect(component.sliderKnobOffset.toPrecision(4)).toBe(
      new BigNumber(100).toPrecision(4)
    );
  });

  it("should compute the slider knob offset correctly when then zoom is MAX_ZOOM", () => {
    expect(component.zoom.isEqualTo(DEFAULT_ZOOM)).toBeTruthy();
    expect(component.sliderKnobOffset.toPrecision(4)).toBe(
      defaultZoomOffset.toPrecision(4)
    );

    component.zoom = new BigNumber(MAX_ZOOM);
    expect(component.sliderKnobOffset.toPrecision(4)).toBe(
      new BigNumber(0).toPrecision(4)
    );
  });

  it("should compute the slider knob offset correctly when zooming in", () => {
    expect(component.zoom.isEqualTo(DEFAULT_ZOOM)).toBeTruthy();
    expect(component.sliderKnobOffset.toPrecision(4)).toBe(
      defaultZoomOffset.toPrecision(4)
    );

    component.zoom = component.zoom.plus(ZOOM_STEP_SIZE);
    expect(component.sliderKnobOffset.toPrecision(4)).toBe(
      new BigNumber("94.12").toPrecision(4)
    );
  });

  it("should compute the slider knob offset correctly when zooming out", () => {
    component.zoom = DEFAULT_ZOOM.plus(ZOOM_STEP_SIZE);

    expect(
      component.zoom.isEqualTo(DEFAULT_ZOOM.plus(ZOOM_STEP_SIZE))
    ).toBeTruthy();
    expect(component.sliderKnobOffset.toPrecision(4)).toBe(
      new BigNumber("94.12").toPrecision(4)
    );

    component.zoom = component.zoom.minus(ZOOM_STEP_SIZE);
    expect(component.sliderKnobOffset.toPrecision(4)).toBe(
      defaultZoomOffset.toPrecision(4)
    );
  });

  it("should trigger the zoomedIn event when #actionZoomIn is called", fakeAsync(() => {
    let called = false;
    const sub = component.zoomedIn.subscribe(() => {
      called = true;
    });
    subs.push(sub);

    component.actionZoomIn();
    tick(300);
    expect(called).toBeTruthy();
  }));

  it("should trigger the zoomOut event when #actionZoomOut is called", fakeAsync(() => {
    let called = false;
    const sub = component.zoomedOut.subscribe(() => {
      called = true;
    });
    subs.push(sub);

    component.actionZoomOut();
    tick(300);
    expect(called).toBeTruthy();
  }));

  afterEach(() => {
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
