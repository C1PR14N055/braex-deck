import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";
import BigNumber from "bignumber.js";
import { DEFAULT_ZOOM, MAX_ZOOM, MIN_ZOOM } from "../rendering/camera-2d";

const HUNDRED = Object.freeze(new BigNumber(100));

class ViewContext {
  faPlus = faPlus;
  faMinus = faMinus;
  maxZoom = MAX_ZOOM;
  minZoom = MIN_ZOOM;
}

@Component({
  selector: "lib-zoom-indicator",
  templateUrl: "./zoom-indicator.component.html",
  styleUrls: ["./zoom-indicator.component.scss"]
})
export class ZoomIndicatorComponent implements OnInit {
  readonly viewContext = new ViewContext();
  @Output()
  zoomedIn = new EventEmitter<void>();
  @Output()
  zoomedOut = new EventEmitter<void>();
  private _zoom = Object.freeze(new BigNumber(DEFAULT_ZOOM));
  private _sliderKnobOffset = Object.freeze(new BigNumber(0));

  ngOnInit() {
    this._computeSliderKnobOffset();
  }

  actionZoomIn() {
    this.zoomedIn.emit(null);
  }

  actionZoomOut() {
    this.zoomedOut.emit(null);
  }

  private _computeSliderKnobOffset() {
    this._sliderKnobOffset = Object.freeze(
      HUNDRED.minus(
        this._zoom
          .minus(MIN_ZOOM)
          .multipliedBy(HUNDRED)
          .dividedBy(MAX_ZOOM.minus(MIN_ZOOM))
      )
    );
  }

  get sliderKnobOffset() {
    return this._sliderKnobOffset;
  }

  get zoom() {
    return this._zoom;
  }

  @Input()
  set zoom(value: Readonly<BigNumber>) {
    const changed = !this._zoom.isEqualTo(value);
    if (changed) {
      this._zoom = Object.freeze(new BigNumber(value));
      this._computeSliderKnobOffset();
    }
  }
}
