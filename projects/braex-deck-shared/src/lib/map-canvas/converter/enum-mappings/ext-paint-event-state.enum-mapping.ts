import { ExtPaintEventState } from "../../ds/drawing-sync/external/ext-paint-event-state.enum";
import { PaintEventState } from "../../ds/drawing-sync/paint-event-state.enum";

export const EXT_PAINT_EVENT_STATE_MAPPING = new Map<
  ExtPaintEventState,
  PaintEventState
>([
  [ExtPaintEventState.START, PaintEventState.START],
  [ExtPaintEventState.MIDDLE, PaintEventState.MIDDLE],
  [ExtPaintEventState.END, PaintEventState.END]
]);
