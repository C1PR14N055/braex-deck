import { MapMarkerType } from "../../ds/map-marker-type.enum";
import { ExtMapMarkerType } from "../../ds/external/ext-map-marker-type.enum";

export const EXT_MAP_MARKER_TYPE_MAPPING = new Map<
  ExtMapMarkerType,
  MapMarkerType
>([
  [ExtMapMarkerType.DEFAULT, MapMarkerType.DEFAULT],
  [ExtMapMarkerType.WEAPON, MapMarkerType.WEAPON],
  [ExtMapMarkerType.CAR, MapMarkerType.CAR],
  [ExtMapMarkerType.ITEM, MapMarkerType.ITEM]
]);
