import { ExtChangeEventType } from "../../ds/external/ext-change-event-type.enum";
import { ChangeEventType } from "../../ds/change-event-type.enum";

export const EXT_CHANGE_EVENT_TYPE_MAPPING = new Map([
  [ExtChangeEventType.CREATE, ChangeEventType.CREATE],
  [ExtChangeEventType.REMOVE, ChangeEventType.REMOVE]
]);
