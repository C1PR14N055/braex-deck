import { ChangeActionType } from "../../ds/drawing-sync/change-action-type.enum";
import { ExtChangeActionType } from "../../ds/external/ext-change-action-type.enum";

export const EXT_CHANGE_ACTION_TYPE_MAPPING = new Map([
  [ExtChangeActionType.LINE, ChangeActionType.LINE],
  [ExtChangeActionType.MARKER, ChangeActionType.MARKER]
]);
