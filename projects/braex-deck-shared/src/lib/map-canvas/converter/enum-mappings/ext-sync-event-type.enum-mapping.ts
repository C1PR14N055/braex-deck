import { ExtDrawingSyncEventType } from "../../ds/drawing-sync/external/ext-drawing-sync-event-type";
import { DrawingSyncEventType } from "../../ds/drawing-sync/drawing-sync-event-type";

export const EXT_SYNC_EVENT_TYPE_MAPPING = new Map<
  ExtDrawingSyncEventType,
  DrawingSyncEventType
>([
  [ExtDrawingSyncEventType.PAINT, DrawingSyncEventType.PAINT],
  [ExtDrawingSyncEventType.CLEAR_PAINT, DrawingSyncEventType.CLEAR_PAINT],
  [ExtDrawingSyncEventType.ERASE_PAINT, DrawingSyncEventType.ERASE_PAINT],
  [ExtDrawingSyncEventType.OTHER, DrawingSyncEventType.OTHER],
  [ExtDrawingSyncEventType.UNDO_PAINT, DrawingSyncEventType.UNDO_PAINT],
  [ExtDrawingSyncEventType.REDO_PAINT, DrawingSyncEventType.REDO_PAINT],
  [ExtDrawingSyncEventType.MARKER, DrawingSyncEventType.MARKER],
  [ExtDrawingSyncEventType.CLEAR_MARKER, DrawingSyncEventType.CLEAR_MARKER]
]);
