import { ChangeEventType } from "../../ds/change-event-type.enum";
import { ExtChangeEventType } from "../../ds/external/ext-change-event-type.enum";
import { Converter } from "../../../converters/converter";
import { Injectable } from "@angular/core";
import { EXT_CHANGE_EVENT_TYPE_MAPPING } from "../enum-mappings/ext-change-event-type.enum-mapping";
import { MappingError } from "../../../error/mapping.error";

@Injectable({
  providedIn: "root"
})
export class ChangeEventTypeConverter
  implements Converter<ChangeEventType, ExtChangeEventType> {
  convert(source: ChangeEventType) {
    for (const key of Array.from(EXT_CHANGE_EVENT_TYPE_MAPPING.keys())) {
      if (EXT_CHANGE_EVENT_TYPE_MAPPING.get(key) === source) {
        return key;
      }
    }

    throw new MappingError(
      `No mapping found for the ChangeEventType \`${source}\``
    );
  }
}
