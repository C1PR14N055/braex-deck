import { Converter } from "../../../converters/converter";
import { Injectable } from "@angular/core";
import { MappingError } from "../../../error/mapping.error";
import { PaintSyncBodyConverter } from "./paint-sync-body.converter";
import { ChangeHistorySyncBodyConverter } from "./change-history-sync-body.converter";
import { MarkerSyncBodyConverter } from "./marker-sync-body.converter";
import { ExtDrawingSyncEvent } from "../../ds/drawing-sync/external/ext-drawing-sync-event";
import { DrawingSyncEventTypeConverter } from "./drawing-sync-event-type.converter";
import { PaintSyncBody } from "../../ds/drawing-sync/paint-sync-body";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { DrawingSyncEventType } from "../../ds/drawing-sync/drawing-sync-event-type";
import { ChangeHistorySyncBody } from "../../ds/drawing-sync/change-history-sync-body";
import { MarkerSyncBody } from "../../ds/drawing-sync/marker-sync-body";

@Injectable({
  providedIn: "root"
})
export class DrawingSyncEventConverter
  implements Converter<DrawingSyncEvent<any>, ExtDrawingSyncEvent> {
  constructor(
    private _syncEventType: DrawingSyncEventTypeConverter,
    private _paintSyncBody: PaintSyncBodyConverter,
    private _changeHistorySyncBody: ChangeHistorySyncBodyConverter,
    private _markerSyncBody: MarkerSyncBodyConverter
  ) {}

  convert(source: DrawingSyncEvent<any>): ExtDrawingSyncEvent {
    let details: string | null;

    switch (source.type) {
      case DrawingSyncEventType.PAINT:
        details = JSON.stringify(
          this._paintSyncBody.convert(source.body as PaintSyncBody)
        );
        break;
      case DrawingSyncEventType.UNDO_PAINT:
      case DrawingSyncEventType.REDO_PAINT:
        details = JSON.stringify(
          this._changeHistorySyncBody.convert(
            source.body as ChangeHistorySyncBody
          )
        );
        break;
      case DrawingSyncEventType.CLEAR_PAINT:
      case DrawingSyncEventType.CLEAR_MARKER:
        details = null;
        break;
      case DrawingSyncEventType.MARKER:
        details = JSON.stringify(
          this._markerSyncBody.convert(source.body as MarkerSyncBody)
        );
        break;
      default:
        throw new MappingError(
          `Unknown DrawingSyncEventType \`${source.type}\``
        );
    }

    return {
      owner: source.owner,
      type: this._syncEventType.convert(source.type),
      details: details
    };
  }
}
