import { TestBed } from "@angular/core/testing";
import { Vector2Converter } from "./vector2.converter";
import { PubgMap } from "../../../ds/game/pubg-map.enum";
import { Vector2 } from "../../ds/vector2";
import { MapBackgroundCanvasModule } from "../../canvas-modules/modules/map-background.canvas-module";

describe("Vector2Converter", () => {
  let converter: Vector2Converter;
  let fakeMapBackground: any;

  beforeEach(() => {
    fakeMapBackground = {
      currentMap: PubgMap.SAVAGE_MAIN,
      mapSize: Vector2.create(900, 900)
    };

    TestBed.configureTestingModule({
      providers: [
        Vector2Converter,
        { provide: MapBackgroundCanvasModule, useValue: fakeMapBackground }
      ]
    });

    converter = TestBed.get(Vector2Converter);
  });

  it("should divide the vector by the size of the map", () => {
    const expected = Vector2.create(24, 40.22222222);
    const actual = converter.convert(Vector2.create(216, 362));

    expect(actual.x).toBeCloseTo(expected.x);
    expect(actual.y).toBeCloseTo(expected.y);
  });
});
