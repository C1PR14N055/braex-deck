import { Converter } from "../../../converters/converter";
import { Vector3 } from "../../ds/vector3";
import { ExtVector3 } from "../../ds/external/ext-vector-3";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class Vector3Converter implements Converter<Vector3, ExtVector3> {
  convert(source: Vector3): ExtVector3 {
    return {
      x: source.x,
      y: source.y,
      z: source.z
    };
  }
}
