import { Converter } from "../../../converters/converter";
import { ExtMarkerSyncBody } from "../../ds/drawing-sync/external/ext-marker-sync-body";
import { Injectable } from "@angular/core";
import { Vector2Converter } from "./vector2.converter";
import { MarkerSyncBody } from "../../ds/drawing-sync/marker-sync-body";
import { MarkerConfigConverter } from "./marker-config.converter";

@Injectable({
  providedIn: "root"
})
export class MarkerSyncBodyConverter
  implements Converter<MarkerSyncBody, ExtMarkerSyncBody> {
  constructor(
    private _vector: Vector2Converter,
    private _markerConfig: MarkerConfigConverter
  ) {}

  convert(source: MarkerSyncBody): ExtMarkerSyncBody {
    return {
      position: this._vector.convert(source.position),
      config: this._markerConfig.convert(source.config)
    };
  }
}
