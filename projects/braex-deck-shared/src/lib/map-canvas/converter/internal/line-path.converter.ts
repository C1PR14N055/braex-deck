import { Converter } from "../../../converters/converter";
import { LinePath } from "../../ds/line-path";
import { ExtLinePath } from "../../ds/drawing-sync/external/ext-line-path";
import { Injectable } from "@angular/core";
import { BrushConfigConverter } from "./brush-config.converter";
import { Vector2Converter } from "./vector2.converter";

@Injectable({
  providedIn: "root"
})
export class LinePathConverter implements Converter<LinePath, ExtLinePath> {
  constructor(
    private _brushConfig: BrushConfigConverter,
    private _vector: Vector2Converter
  ) {}

  convert(source: LinePath): ExtLinePath {
    return {
      brushConfig: this._brushConfig.convert(source.brushConfig),
      points: source.points.toArray().map(x => this._vector.convert(x))
    };
  }
}
