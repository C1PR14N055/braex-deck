import { Converter } from "../../../converters/converter";
import { ExtPaintSyncBody } from "../../ds/drawing-sync/external/ext-paint-sync-body";
import { Injectable } from "@angular/core";
import { BrushConfigConverter } from "./brush-config.converter";
import { Vector2Converter } from "./vector2.converter";
import { PaintEventStateConverter } from "./paint-event-state.converter";
import { PaintSyncBody } from "../../ds/drawing-sync/paint-sync-body";

@Injectable({
  providedIn: "root"
})
export class PaintSyncBodyConverter
  implements Converter<PaintSyncBody, ExtPaintSyncBody> {
  constructor(
    private _vector: Vector2Converter,
    private _brushConfig: BrushConfigConverter,
    private _paintEventState: PaintEventStateConverter
  ) {}

  convert(source: PaintSyncBody): ExtPaintSyncBody {
    return {
      state: this._paintEventState.convert(source.state),
      position: source.position
        ? this._vector.convert(source.position)
        : undefined,
      config: source.config
        ? this._brushConfig.convert(source.config)
        : undefined
    };
  }
}
