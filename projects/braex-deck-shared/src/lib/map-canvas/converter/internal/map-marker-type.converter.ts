import { Converter } from "../../../converters/converter";
import { MapMarkerType } from "../../ds/map-marker-type.enum";
import { ExtMapMarkerType } from "../../ds/external/ext-map-marker-type.enum";
import { Injectable } from "@angular/core";
import { MappingError } from "../../../error/mapping.error";
import { EXT_MAP_MARKER_TYPE_MAPPING } from "../enum-mappings/ext-map-marker-type.enum-mapping";

@Injectable({
  providedIn: "root"
})
export class MapMarkerTypeConverter
  implements Converter<MapMarkerType, ExtMapMarkerType> {
  convert(source: MapMarkerType): ExtMapMarkerType {
    for (const key of Array.from(EXT_MAP_MARKER_TYPE_MAPPING.keys())) {
      if (EXT_MAP_MARKER_TYPE_MAPPING.get(key) === source) {
        return key;
      }
    }

    throw new MappingError(
      `No mapping found for the MapMarkerType \`${source}\``
    );
  }
}
