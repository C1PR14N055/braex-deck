import { Converter } from "../../../converters/converter";
import { ExtChangeHistorySyncBody } from "../../ds/drawing-sync/external/ext-change-history-sync-body";
import { Injectable } from "@angular/core";
import { ChangeHistorySyncBody } from "../../ds/drawing-sync/change-history-sync-body";
import { ChangeHistoryEventConverter } from "./change-history-event.converter";
import { ChangeActionTypeConverter } from "./change-action-type.converter";

@Injectable({
  providedIn: "root"
})
export class ChangeHistorySyncBodyConverter
  implements Converter<ChangeHistorySyncBody, ExtChangeHistorySyncBody> {
  constructor(
    private _changeHistoryEvent: ChangeHistoryEventConverter,
    private _changeActionType: ChangeActionTypeConverter
  ) {}

  convert(source: ChangeHistorySyncBody): ExtChangeHistorySyncBody {
    return {
      event: this._changeHistoryEvent.convert(source.event),
      type: this._changeActionType.convert(source.type)
    };
  }
}
