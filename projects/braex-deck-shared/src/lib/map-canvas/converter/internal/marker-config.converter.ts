import { MarkerConfig } from "../../ds/marker-config";
import { ExtMarkerConfig } from "../../ds/external/ext-marker-config";
import { Converter } from "../../../converters/converter";
import { Injectable } from "@angular/core";
import { MapMarkerTypeConverter } from "./map-marker-type.converter";

@Injectable({
  providedIn: "root"
})
export class MarkerConfigConverter
  implements Converter<MarkerConfig, ExtMarkerConfig> {
  constructor(private _mapMarkerType: MapMarkerTypeConverter) {}

  convert(source: MarkerConfig): ExtMarkerConfig {
    return {
      color: source.color,
      type: this._mapMarkerType.convert(source.type)
    };
  }
}
