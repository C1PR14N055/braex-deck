import { Converter } from "../../../converters/converter";
import { TeamPositionSync } from "../../ds/position-sync/team-position-sync";
import { ExtTeamPositionSync } from "../../ds/position-sync/external/ext-team-position-sync";
import { Injectable } from "@angular/core";
import { Vector3Converter } from "./vector3.converter";

@Injectable({
  providedIn: "root"
})
export class TeamPositionSyncConverter
  implements Converter<TeamPositionSync, ExtTeamPositionSync> {
  constructor(private _vector3: Vector3Converter) {}

  convert(source: TeamPositionSync): ExtTeamPositionSync {
    return {
      position: this._vector3.convert(source.position),
      owner: source.owner,
      alive: source.alive
    };
  }
}
