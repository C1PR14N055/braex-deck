import { Converter } from "../../../converters/converter";
import { ChangeActionType } from "../../ds/drawing-sync/change-action-type.enum";
import { ExtChangeActionType } from "../../ds/external/ext-change-action-type.enum";
import { EXT_CHANGE_ACTION_TYPE_MAPPING } from "../enum-mappings/ext-change-action-type.enum-mapping";
import { MappingError } from "../../../error/mapping.error";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ChangeActionTypeConverter
  implements Converter<ChangeActionType, ExtChangeActionType> {
  convert(source: ChangeActionType) {
    for (const key of Array.from(EXT_CHANGE_ACTION_TYPE_MAPPING.keys())) {
      if (EXT_CHANGE_ACTION_TYPE_MAPPING.get(key) === source) {
        return key;
      }
    }

    throw new MappingError(
      `No mapping found for the ChangeActionType \`${source}\``
    );
  }
}
