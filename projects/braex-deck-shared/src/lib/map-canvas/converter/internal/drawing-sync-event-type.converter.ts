import { ExtDrawingSyncEventType } from "../../ds/drawing-sync/external/ext-drawing-sync-event-type";
import { Converter } from "../../../converters/converter";
import { Injectable } from "@angular/core";
import { EXT_SYNC_EVENT_TYPE_MAPPING } from "../enum-mappings/ext-sync-event-type.enum-mapping";
import { MappingError } from "../../../error/mapping.error";
import { DrawingSyncEventType } from "../../ds/drawing-sync/drawing-sync-event-type";

@Injectable({
  providedIn: "root"
})
export class DrawingSyncEventTypeConverter
  implements Converter<DrawingSyncEventType, ExtDrawingSyncEventType> {
  convert(source: DrawingSyncEventType) {
    for (const key of Array.from(EXT_SYNC_EVENT_TYPE_MAPPING.keys())) {
      if (EXT_SYNC_EVENT_TYPE_MAPPING.get(key) === source) {
        return key;
      }
    }

    throw new MappingError(
      `No mapping for the SyncEventType \`${source}\` has been found`
    );
  }
}
