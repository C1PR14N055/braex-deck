import { MapMarker } from "../../ds/map-marker";
import { ExtMapMarker } from "../../ds/external/ext-map-marker";
import { Converter } from "../../../converters/converter";
import { MarkerConfigConverter } from "./marker-config.converter";
import { Vector2Converter } from "./vector2.converter";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class MapMarkerConverter implements Converter<MapMarker, ExtMapMarker> {
  constructor(
    private _markerConfig: MarkerConfigConverter,
    private _vector2: Vector2Converter
  ) {}

  convert(source: MapMarker): ExtMapMarker {
    return {
      config: this._markerConfig.convert(source.config),
      position: this._vector2.convert(source.position)
    };
  }
}
