import { Converter } from "../../../converters/converter";
import { BrushConfig } from "../../ds/brush-config";
import { ExtBrushConfig } from "../../ds/drawing-sync/external/ext-brush-config";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class BrushConfigConverter
  implements Converter<BrushConfig, ExtBrushConfig> {
  convert(source: BrushConfig): ExtBrushConfig {
    return {
      color: source.color,
      thickness: source.thickness
    };
  }
}
