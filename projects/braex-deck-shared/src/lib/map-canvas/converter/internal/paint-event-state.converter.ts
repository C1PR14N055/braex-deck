import { Converter } from "../../../converters/converter";
import { ExtPaintEventState } from "../../ds/drawing-sync/external/ext-paint-event-state.enum";
import { MappingError } from "../../../error/mapping.error";
import { EXT_PAINT_EVENT_STATE_MAPPING } from "../enum-mappings/ext-paint-event-state.enum-mapping";
import { Injectable } from "@angular/core";
import { PaintEventState } from "../../ds/drawing-sync/paint-event-state.enum";

@Injectable({
  providedIn: "root"
})
export class PaintEventStateConverter
  implements Converter<PaintEventState, ExtPaintEventState> {
  convert(source: PaintEventState) {
    for (const key of Array.from(EXT_PAINT_EVENT_STATE_MAPPING.keys())) {
      if (EXT_PAINT_EVENT_STATE_MAPPING.get(key) === source) {
        return key;
      }
    }

    throw new MappingError(
      `No mapping found for the PaintEventState \`${source}\``
    );
  }
}
