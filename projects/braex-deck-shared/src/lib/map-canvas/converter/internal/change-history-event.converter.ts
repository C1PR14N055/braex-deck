import { Converter } from "../../../converters/converter";
import { ChangeHistoryEvent } from "../../ds/change-history-event";
import { ExtChangeHistoryEvent } from "../../ds/external/ext-change-history-event";
import { Injectable } from "@angular/core";
import { MappingError } from "../../../error/mapping.error";
import { LinePath } from "../../ds/line-path";
import { MapMarker } from "../../ds/map-marker";
import { ChangeEventTypeConverter } from "./change-event-type.converter";
import { LinePathConverter } from "./line-path.converter";
import { MapMarkerConverter } from "./map-marker.converter";

@Injectable({
  providedIn: "root"
})
export class ChangeHistoryEventConverter
  implements Converter<ChangeHistoryEvent<any>, ExtChangeHistoryEvent> {
  constructor(
    private _changeEventType: ChangeEventTypeConverter,
    private _linePath: LinePathConverter,
    private _mapMarker: MapMarkerConverter
  ) {}

  convert(source: ChangeHistoryEvent<any>): ExtChangeHistoryEvent {
    let body: string;
    if (source.body instanceof LinePath) {
      body = JSON.stringify(this._linePath.convert(source.body));
    } else if (source.body instanceof MapMarker) {
      body = JSON.stringify(this._mapMarker.convert(source.body));
    } else {
      throw new MappingError(`No mapping for the change history event body`);
    }

    return {
      type: this._changeEventType.convert(source.type),
      body: body
    };
  }
}
