import { Converter } from "../../../converters/converter";
import { Vector2 } from "../../ds/vector2";
import { ExtVector2 } from "../../ds/external/ext-vector2";
import { Injectable } from "@angular/core";
import { MapBackgroundCanvasModule } from "../../canvas-modules/modules/map-background.canvas-module";
import { MAP_SCALE_FACTOR } from "../map-translation";

@Injectable({
  providedIn: "root"
})
export class Vector2Converter implements Converter<Vector2, ExtVector2> {
  constructor(private _mapBackground: MapBackgroundCanvasModule) {}

  convert(source: Vector2): ExtVector2 {
    // divide the vector by the size of the map so that the partner can compute the point on his own map
    let x = source.x;
    let y = source.y;
    if (this._mapBackground.mapSize) {
      x /= this._mapBackground.mapSize.x / MAP_SCALE_FACTOR;
      y /= this._mapBackground.mapSize.y / MAP_SCALE_FACTOR;
    }

    return {
      x: x,
      y: y
    };
  }
}
