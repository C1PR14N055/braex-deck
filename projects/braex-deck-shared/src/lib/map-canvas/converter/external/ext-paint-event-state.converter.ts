import { ExtPaintEventState } from "../../ds/drawing-sync/external/ext-paint-event-state.enum";
import { Converter } from "../../../converters/converter";
import { EXT_PAINT_EVENT_STATE_MAPPING } from "../enum-mappings/ext-paint-event-state.enum-mapping";
import { MappingError } from "../../../error/mapping.error";
import { Injectable } from "@angular/core";
import { PaintEventState } from "../../ds/drawing-sync/paint-event-state.enum";

@Injectable({
  providedIn: "root"
})
export class ExtPaintEventStateConverter
  implements Converter<ExtPaintEventState, PaintEventState> {
  convert(source: ExtPaintEventState) {
    if (!EXT_PAINT_EVENT_STATE_MAPPING.has(source)) {
      throw new MappingError(
        `No mapping found for the ExtPaintEventState \`${source}\``
      );
    }

    return EXT_PAINT_EVENT_STATE_MAPPING.get(source);
  }
}
