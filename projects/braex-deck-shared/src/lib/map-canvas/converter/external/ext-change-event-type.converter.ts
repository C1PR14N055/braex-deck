import { Converter } from "../../../converters/converter";
import { ExtChangeEventType } from "../../ds/external/ext-change-event-type.enum";
import { ChangeEventType } from "../../ds/change-event-type.enum";
import { Injectable } from "@angular/core";
import { EXT_CHANGE_EVENT_TYPE_MAPPING } from "../enum-mappings/ext-change-event-type.enum-mapping";
import { MappingError } from "../../../error/mapping.error";

@Injectable({
  providedIn: "root"
})
export class ExtChangeEventTypeConverter
  implements Converter<ExtChangeEventType, ChangeEventType> {
  convert(source: ExtChangeEventType) {
    if (!EXT_CHANGE_EVENT_TYPE_MAPPING.has(source)) {
      throw new MappingError(
        `No mapping found for the ExtChangeEventType \`${source}\``
      );
    }

    return EXT_CHANGE_EVENT_TYPE_MAPPING.get(source);
  }
}
