import { Converter } from "../../../converters/converter";
import { ExtVector3 } from "../../ds/external/ext-vector-3";
import { Vector3 } from "../../ds/vector3";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ExtVector3Converter implements Converter<ExtVector3, Vector3> {
  convert(source: ExtVector3): Vector3 {
    return Vector3.create(source.x, source.y, source.z);
  }
}
