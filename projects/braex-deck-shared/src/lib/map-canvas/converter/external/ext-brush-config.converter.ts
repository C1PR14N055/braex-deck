import { Converter } from "../../../converters/converter";
import { ExtBrushConfig } from "../../ds/drawing-sync/external/ext-brush-config";
import { BrushConfig } from "../../ds/brush-config";
import { Injectable } from "@angular/core";
import { Color } from "../../ds/color.enum";

@Injectable({
  providedIn: "root"
})
export class ExtBrushConfigConverter
  implements Converter<ExtBrushConfig, BrushConfig> {
  convert(source: ExtBrushConfig): BrushConfig {
    return new BrushConfig(source.thickness, source.color as Color);
  }
}
