import { Converter } from "../../../converters/converter";
import { Injectable } from "@angular/core";
import { ExtChangeHistoryEvent } from "../../ds/external/ext-change-history-event";
import { ChangeHistoryEvent } from "../../ds/change-history-event";
import { ExtChangeEventTypeConverter } from "./ext-change-event-type.converter";
import { ExtLinePath } from "../../ds/drawing-sync/external/ext-line-path";
import { ExtLinePathConverter } from "./ext-line-path.converter";
import { ExtMapMarker } from "../../ds/external/ext-map-marker";
import { ExtMapMarkerConverter } from "./ext-map-marker.converter";
import { MappingError } from "../../../error/mapping.error";

@Injectable({
  providedIn: "root"
})
export class ExtChangeHistoryEventConverter
  implements Converter<ExtChangeHistoryEvent, ChangeHistoryEvent<any>> {
  constructor(
    private _changeEventType: ExtChangeEventTypeConverter,
    private _extLinePath: ExtLinePathConverter,
    private _extMapMarker: ExtMapMarkerConverter
  ) {}

  convert(source: ExtChangeHistoryEvent): ChangeHistoryEvent<any> {
    const body = JSON.parse(source.body);
    if (
      (body as ExtLinePath).points !== undefined &&
      (body as ExtLinePath).brushConfig !== undefined
    ) {
      return new ChangeHistoryEvent(
        this._changeEventType.convert(source.type),
        this._extLinePath.convert(body as ExtLinePath)
      );
    } else if (
      (body as ExtMapMarker).config !== undefined &&
      (body as ExtMapMarker).position !== undefined
    ) {
      return new ChangeHistoryEvent(
        this._changeEventType.convert(source.type),
        this._extMapMarker.convert(body)
      );
    }

    throw new MappingError(`No mapping found ExtChangeHistoryEvent body`);
  }
}
