import { Converter } from "../../../converters/converter";
import { ExtTeamPositionSync } from "../../ds/position-sync/external/ext-team-position-sync";
import { TeamPositionSync } from "../../ds/position-sync/team-position-sync";
import { Injectable } from "@angular/core";
import { ExtVector3Converter } from "./ext-vector3.converter";

@Injectable({
  providedIn: "root"
})
export class ExtTeamPositionSyncConverter
  implements Converter<ExtTeamPositionSync, TeamPositionSync> {
  constructor(private _extVector3: ExtVector3Converter) {}

  convert(source: ExtTeamPositionSync) {
    return new TeamPositionSync(
      source.owner,
      this._extVector3.convert(source.position),
      source.alive
    );
  }
}
