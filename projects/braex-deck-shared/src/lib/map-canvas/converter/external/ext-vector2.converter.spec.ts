import { TestBed } from "@angular/core/testing";
import { ExtVector2Converter } from "./ext-vector2.converter";
import { PubgMap } from "../../../ds/game/pubg-map.enum";
import { Vector2 } from "../../ds/vector2";
import { MapBackgroundCanvasModule } from "../../canvas-modules/modules/map-background.canvas-module";
import { ExtVector2 } from "../../ds/external/ext-vector2";

describe("ExtVector2Converter", () => {
  let converter: ExtVector2Converter;
  let fakeMapBackground: any;

  beforeEach(() => {
    fakeMapBackground = {
      currentMap: PubgMap.ERANGEL_MAIN,
      mapSize: Vector2.create(600, 600)
    };

    TestBed.configureTestingModule({
      providers: [
        ExtVector2Converter,
        { provide: MapBackgroundCanvasModule, useValue: fakeMapBackground }
      ]
    });

    converter = TestBed.get(ExtVector2Converter);
  });

  it("should translate the vector from the 3rd party space to the own space", () => {
    const extVector: ExtVector2 = {
      x: 24,
      y: 40.2222222222
    };
    const actual = converter.convert(extVector);

    expect(actual.x).toBeCloseTo(144);
    expect(actual.y).toBeCloseTo(241.3333333333);
  });
});
