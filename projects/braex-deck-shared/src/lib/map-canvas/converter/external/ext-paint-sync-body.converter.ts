import { Converter } from "../../../converters/converter";
import { ExtPaintSyncBody } from "../../ds/drawing-sync/external/ext-paint-sync-body";
import { ExtPaintEventStateConverter } from "./ext-paint-event-state.converter";
import { ExtVector2Converter } from "./ext-vector2.converter";
import { ExtBrushConfigConverter } from "./ext-brush-config.converter";
import { Injectable } from "@angular/core";
import { PaintSyncBody } from "../../ds/drawing-sync/paint-sync-body";

@Injectable({
  providedIn: "root"
})
export class ExtPaintSyncBodyConverter
  implements Converter<ExtPaintSyncBody, PaintSyncBody> {
  constructor(
    private _extPaintEventState: ExtPaintEventStateConverter,
    private _extVector: ExtVector2Converter,
    private _extBrushConfig: ExtBrushConfigConverter
  ) {}

  convert(source: ExtPaintSyncBody) {
    return new PaintSyncBody(
      this._extPaintEventState.convert(source.state),
      source.position ? this._extVector.convert(source.position) : undefined,
      source.config ? this._extBrushConfig.convert(source.config) : undefined
    );
  }
}
