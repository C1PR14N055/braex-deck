import { Converter } from "../../../converters/converter";
import { ExtMarkerConfig } from "../../ds/external/ext-marker-config";
import { MarkerConfig } from "../../ds/marker-config";
import { Injectable } from "@angular/core";
import { ExtMapMarkerTypeConverter } from "./ext-map-marker-type.converter";
import { Color } from "../../ds/color.enum";

@Injectable({
  providedIn: "root"
})
export class ExtMarkerConfigConverter
  implements Converter<ExtMarkerConfig, MarkerConfig> {
  constructor(private _extMapMarkerType: ExtMapMarkerTypeConverter) {}

  convert(source: ExtMarkerConfig) {
    return new MarkerConfig(
      this._extMapMarkerType.convert(source.type),
      source.color as Color
    );
  }
}
