import { ExtMapMarker } from "../../ds/external/ext-map-marker";
import { MapMarker } from "../../ds/map-marker";
import { Converter } from "../../../converters/converter";
import { ExtMarkerConfigConverter } from "./ext-marker-config.converter";
import { ExtVector2Converter } from "./ext-vector2.converter";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ExtMapMarkerConverter
  implements Converter<ExtMapMarker, MapMarker> {
  constructor(
    private _extMarkerConfig: ExtMarkerConfigConverter,
    private _extVector2: ExtVector2Converter
  ) {}

  convert(source: ExtMapMarker) {
    return MapMarker.create(
      this._extVector2.convert(source.position),
      this._extMarkerConfig.convert(source.config)
    );
  }
}
