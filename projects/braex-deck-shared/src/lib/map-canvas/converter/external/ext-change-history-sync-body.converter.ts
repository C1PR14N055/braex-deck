import { ExtChangeHistorySyncBody } from "../../ds/drawing-sync/external/ext-change-history-sync-body";
import { Converter } from "../../../converters/converter";
import { Injectable } from "@angular/core";
import { ChangeHistorySyncBody } from "../../ds/drawing-sync/change-history-sync-body";
import { ExtChangeActionTypeConverter } from "./ext-change-action-type.converter";
import { ExtChangeHistoryEventConverter } from "./ext-change-history-event.converter";

@Injectable({
  providedIn: "root"
})
export class ExtChangeHistorySyncBodyConverter
  implements Converter<ExtChangeHistorySyncBody, ChangeHistorySyncBody> {
  constructor(
    private _extChangeHistoryEvent: ExtChangeHistoryEventConverter,
    private _extChangeActionType: ExtChangeActionTypeConverter
  ) {}

  convert(source: ExtChangeHistorySyncBody) {
    return new ChangeHistorySyncBody(
      this._extChangeHistoryEvent.convert(source.event),
      this._extChangeActionType.convert(source.type)
    );
  }
}
