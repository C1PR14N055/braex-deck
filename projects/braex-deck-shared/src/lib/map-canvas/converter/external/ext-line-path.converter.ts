import { ExtLinePath } from "../../ds/drawing-sync/external/ext-line-path";
import { LinePath } from "../../ds/line-path";
import { Converter } from "../../../converters/converter";
import { ExtBrushConfigConverter } from "./ext-brush-config.converter";
import { Vector2 } from "../../ds/vector2";
import { List } from "immutable";
import { ExtVector2Converter } from "./ext-vector2.converter";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ExtLinePathConverter implements Converter<ExtLinePath, LinePath> {
  constructor(
    private _extBrushConfig: ExtBrushConfigConverter,
    private _extVector: ExtVector2Converter
  ) {}

  convert(source: ExtLinePath) {
    return LinePath.create(
      List<Vector2>(source.points.map(x => this._extVector.convert(x))),
      this._extBrushConfig.convert(source.brushConfig)
    );
  }
}
