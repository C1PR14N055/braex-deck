import { Converter } from "../../../converters/converter";
import { ExtMapMarkerType } from "../../ds/external/ext-map-marker-type.enum";
import { MapMarkerType } from "../../ds/map-marker-type.enum";
import { Injectable } from "@angular/core";
import { EXT_MAP_MARKER_TYPE_MAPPING } from "../enum-mappings/ext-map-marker-type.enum-mapping";
import { MappingError } from "../../../error/mapping.error";

@Injectable({
  providedIn: "root"
})
export class ExtMapMarkerTypeConverter
  implements Converter<ExtMapMarkerType, MapMarkerType> {
  convert(source: ExtMapMarkerType) {
    if (!EXT_MAP_MARKER_TYPE_MAPPING.has(source)) {
      throw new MappingError(
        `No mapping found for the ExtMapMarkerType \`${source}\``
      );
    }

    return EXT_MAP_MARKER_TYPE_MAPPING.get(source);
  }
}
