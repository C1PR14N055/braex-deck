import { Converter } from "../../../converters/converter";
import { ExtDrawingSyncEventType } from "../../ds/drawing-sync/external/ext-drawing-sync-event-type";
import { Injectable } from "@angular/core";
import { EXT_SYNC_EVENT_TYPE_MAPPING } from "../enum-mappings/ext-sync-event-type.enum-mapping";
import { MappingError } from "../../../error/mapping.error";
import { DrawingSyncEventType } from "../../ds/drawing-sync/drawing-sync-event-type";

@Injectable({
  providedIn: "root"
})
export class ExtDrawingSyncEventTypeConverter
  implements Converter<ExtDrawingSyncEventType, DrawingSyncEventType> {
  convert(source: ExtDrawingSyncEventType) {
    if (!EXT_SYNC_EVENT_TYPE_MAPPING.has(source)) {
      throw new MappingError(
        `No mapping for the ExtSyncEventType \`${source}\` has been found`
      );
    }

    return EXT_SYNC_EVENT_TYPE_MAPPING.get(source);
  }
}
