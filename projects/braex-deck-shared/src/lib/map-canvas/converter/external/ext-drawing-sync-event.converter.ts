import { ExtDrawingSyncEvent } from "../../ds/drawing-sync/external/ext-drawing-sync-event";
import { Converter } from "../../../converters/converter";
import { Injectable } from "@angular/core";
import { ExtDrawingSyncEventTypeConverter } from "./ext-drawing-sync-event-type.converter";
import { ExtPaintSyncBody } from "../../ds/drawing-sync/external/ext-paint-sync-body";
import { ExtPaintEventStateConverter } from "./ext-paint-event-state.converter";
import { ExtVector2Converter } from "./ext-vector2.converter";
import { ExtBrushConfigConverter } from "./ext-brush-config.converter";
import { ExtChangeHistorySyncBody } from "../../ds/drawing-sync/external/ext-change-history-sync-body";
import { ExtChangeHistorySyncBodyConverter } from "./ext-change-history-sync-body.converter";
import { ExtPaintSyncBodyConverter } from "./ext-paint-sync-body.converter";
import { MappingError } from "../../../error/mapping.error";
import { ExtMarkerSyncBody } from "../../ds/drawing-sync/external/ext-marker-sync-body";
import { ExtMarkerSyncBodyConverter } from "./ext-marker-sync-body.converter";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { PaintSyncBody } from "../../ds/drawing-sync/paint-sync-body";
import { ChangeHistorySyncBody } from "../../ds/drawing-sync/change-history-sync-body";
import { MarkerSyncBody } from "../../ds/drawing-sync/marker-sync-body";
import { DrawingSyncEventType } from "../../ds/drawing-sync/drawing-sync-event-type";

@Injectable({
  providedIn: "root"
})
export class ExtDrawingSyncEventConverter
  implements Converter<ExtDrawingSyncEvent, DrawingSyncEvent<any>> {
  constructor(
    private _extDrawingSyncEventType: ExtDrawingSyncEventTypeConverter,
    private _extPaintEventState: ExtPaintEventStateConverter,
    private _extVector: ExtVector2Converter,
    private _extBrushConfig: ExtBrushConfigConverter,
    private _extChangeHistorySyncBody: ExtChangeHistorySyncBodyConverter,
    private _extPaintSyncBody: ExtPaintSyncBodyConverter,
    private _extMarkerSyncBody: ExtMarkerSyncBodyConverter
  ) {}

  convert(source: ExtDrawingSyncEvent): DrawingSyncEvent<any> {
    const eventType = this._extDrawingSyncEventType.convert(source.type);

    switch (eventType) {
      case DrawingSyncEventType.PAINT:
        const paintBody = JSON.parse(source.details) as ExtPaintSyncBody;

        return new DrawingSyncEvent<PaintSyncBody>(
          eventType,
          this._extPaintSyncBody.convert(paintBody),
          source.owner
        );
      case DrawingSyncEventType.UNDO_PAINT:
      case DrawingSyncEventType.REDO_PAINT:
        const changeBody = JSON.parse(
          source.details
        ) as ExtChangeHistorySyncBody;

        return new DrawingSyncEvent<ChangeHistorySyncBody>(
          eventType,
          this._extChangeHistorySyncBody.convert(changeBody),
          source.owner
        );
      case DrawingSyncEventType.CLEAR_PAINT:
      case DrawingSyncEventType.CLEAR_MARKER:
        return new DrawingSyncEvent<void>(eventType, null, source.owner);
      case DrawingSyncEventType.MARKER:
        const markerBody = JSON.parse(source.details) as ExtMarkerSyncBody;

        return new DrawingSyncEvent<MarkerSyncBody>(
          eventType,
          this._extMarkerSyncBody.convert(markerBody),
          source.owner
        );
      default:
        throw new MappingError(
          `No mapping found for the DrawingSyncEventType \`${eventType}\``
        );
    }
  }
}
