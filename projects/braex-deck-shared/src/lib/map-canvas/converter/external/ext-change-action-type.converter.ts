import { Converter } from "../../../converters/converter";
import { ExtChangeActionType } from "../../ds/external/ext-change-action-type.enum";
import { ChangeActionType } from "../../ds/drawing-sync/change-action-type.enum";
import { EXT_CHANGE_ACTION_TYPE_MAPPING } from "../enum-mappings/ext-change-action-type.enum-mapping";
import { MappingError } from "../../../error/mapping.error";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ExtChangeActionTypeConverter
  implements Converter<ExtChangeActionType, ChangeActionType> {
  convert(source: ExtChangeActionType) {
    if (!EXT_CHANGE_ACTION_TYPE_MAPPING.has(source)) {
      throw new MappingError(
        `No mapping found for the ExtChangeActionType \`${source}\``
      );
    }

    return EXT_CHANGE_ACTION_TYPE_MAPPING.get(source);
  }
}
