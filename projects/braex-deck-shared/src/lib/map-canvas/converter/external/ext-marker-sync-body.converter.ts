import { Converter } from "../../../converters/converter";
import { ExtMarkerSyncBody } from "../../ds/drawing-sync/external/ext-marker-sync-body";
import { Injectable } from "@angular/core";
import { ExtVector2Converter } from "./ext-vector2.converter";
import { MarkerSyncBody } from "../../ds/drawing-sync/marker-sync-body";
import { ExtMarkerConfigConverter } from "./ext-marker-config.converter";

@Injectable({
  providedIn: "root"
})
export class ExtMarkerSyncBodyConverter
  implements Converter<ExtMarkerSyncBody, MarkerSyncBody> {
  constructor(
    private _extVector: ExtVector2Converter,
    private _extMarkerConfig: ExtMarkerConfigConverter
  ) {}

  convert(source: ExtMarkerSyncBody) {
    return new MarkerSyncBody(
      this._extVector.convert(source.position),
      this._extMarkerConfig.convert(source.config)
    );
  }
}
