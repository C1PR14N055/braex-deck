import { Converter } from "../../../converters/converter";
import { ExtVector2 } from "../../ds/external/ext-vector2";
import { Vector2 } from "../../ds/vector2";
import { Injectable } from "@angular/core";
import { MapBackgroundCanvasModule } from "../../canvas-modules/modules/map-background.canvas-module";
import { MAP_SCALE_FACTOR } from "../map-translation";

@Injectable({
  providedIn: "root"
})
export class ExtVector2Converter implements Converter<ExtVector2, Vector2> {
  constructor(private _mapBackground: MapBackgroundCanvasModule) {}

  convert(source: ExtVector2): Vector2 {
    // multiply the received vector by the size of the map background so that
    // we have the exact same point as the received one but on our map
    let x = source.x;
    let y = source.y;
    if (this._mapBackground.mapSize) {
      x *= this._mapBackground.mapSize.x / MAP_SCALE_FACTOR;
      y *= this._mapBackground.mapSize.y / MAP_SCALE_FACTOR;
    }

    return Vector2.create(x, y);
  }
}
