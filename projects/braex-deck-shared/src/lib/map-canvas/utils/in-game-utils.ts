import { MappingError } from "../../error/mapping.error";
import { PubgMap } from "../../ds/game/pubg-map.enum";
import { Vector2 } from "../ds/vector2";
import { Vector3 } from "../ds/vector3";

export abstract class InGameUtils {
  static getTelemetryMapToPubgMap(map: string): PubgMap {
    switch (map) {
      case "Desert_Main":
        return PubgMap.DESERT_MAIN;
      case "DihorOtok_Main":
        return PubgMap.DIHOROTOK_MAIN;
      case "Erangel_Main":
        return PubgMap.ERANGEL_MAIN;
      case "Range_Main":
        return PubgMap.RANGE_MAIN;
      case "Savage_Main":
        return PubgMap.SAVAGE_MAIN;
    }

    throw new MappingError(`Unknown Telemetry \`${map}\``);
  }

  static getPubgMapSizeFromTelemetry(map: PubgMap) {
    switch (map) {
      // 8x8 maps
      case PubgMap.ERANGEL_MAIN:
      case PubgMap.DESERT_MAIN:
        return Vector2.create(816000, 816000);
      // 6x6 maps
      case PubgMap.DIHOROTOK_MAIN:
        return Vector2.create(612000, 612000);
      // 4x4 maps
      case PubgMap.SAVAGE_MAIN:
        return Vector2.create(408000, 408000);
    }

    throw new MappingError(`Unknown PubgMap \`${map}\``);
  }

  static getPubgMapSizeFromOverwolf(map: PubgMap) {
    switch (map) {
      case PubgMap.ERANGEL_MAIN:
      case PubgMap.DESERT_MAIN:
        return Vector2.create(8000, 8000);
      case PubgMap.DIHOROTOK_MAIN:
        return Vector2.create(6000, 6000);
      case PubgMap.SAVAGE_MAIN:
        return Vector2.create(4000, 4000);
    }

    throw new MappingError(`Unknown PubgMap \`${map}\``);
  }

  /**
   * Transform a location of the pubg world space **coming from overwolf** to a location on the map canvas.
   * @param pubgMap The {@link PubgMap} of which the size should be used to
   * compute the location on the canvas.
   * @param canvasMapSize The size of the map on the canvas.
   * @param location The {@link Vector3} with the location from the pubg world space
   */
  static overwolfMapLocationToCanvasSpace(
    pubgMap: PubgMap,
    canvasMapSize: Vector2,
    location: Vector3
  ) {
    const pubgMapSize = this.getPubgMapSizeFromOverwolf(pubgMap);

    return Vector2.create(location.x, location.y)
      .divide(pubgMapSize)
      .multiply(canvasMapSize);
  }

  static telemetryMapLocationToCanvasSpace(
    pubgMap: PubgMap,
    canvasMapSize: Vector2,
    location: Vector3
  ) {
    const pubgMapSize = this.getPubgMapSizeFromTelemetry(pubgMap);

    return Vector2.create(location.x, location.y)
      .divide(pubgMapSize)
      .multiply(canvasMapSize);
  }

  /**
   * Normalize a vector so that a consumer can compute the exact same point based on their map.
   * @param vector The vector which should be normalized.
   * @param mapSize The map size of the own map.
   */
  static normalizeVector(vector: Vector2, mapSize: Vector2) {
    return vector.divide(mapSize);
  }

  /**
   * Transform a normalized vector onto the exact same location on the own map.
   * @param vector The vector which should be denormalized.
   * @param mapSize The map size of the own map.
   */
  static denormalizeVector(vector: Vector2, mapSize: Vector2) {
    return vector.multiply(mapSize);
  }
}
