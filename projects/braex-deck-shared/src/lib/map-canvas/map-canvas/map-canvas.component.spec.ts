import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { MapCanvasComponent } from "./map-canvas.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { BrushCanvasModule } from "../canvas-modules/modules/brush.canvas-module";
import { OverwolfCoreService } from "../../overwolf/overwolf-core.service";
import { BrushChangeHistoryCanvasModule } from "../canvas-modules/modules/brush-change-history.canvas-module";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { MapMarkerCanvasModule } from "../canvas-modules/modules/map-marker.canvas-module";
import { ResizeObserverService } from "../../services/resize-observer.service";
import { BrushDotCanvasModule } from "../canvas-modules/modules/brush-dot.canvas-module";
import { InputControllerService } from "../services/input-controller.service";
import { FakeInputControllerService } from "../services/testing/fake-input-controller.service";
import { CanvasEngine } from "../core/canvas-engine";
import { BrushLinesCanvasModule } from "../canvas-modules/modules/brush-lines.canvas-module";
import { MapNameModule } from "../../map-name/map-name.module";

describe("MapCanvasComponent", () => {
  let component: MapCanvasComponent;
  let fixture: ComponentFixture<MapCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MapCanvasComponent],
      imports: [HttpClientTestingModule, MapNameModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        BrushCanvasModule,
        OverwolfCoreService,
        BrushChangeHistoryCanvasModule,
        { provide: MapMarkerCanvasModule, useValue: {} },
        ResizeObserverService,
        BrushDotCanvasModule,
        {
          provide: InputControllerService,
          useClass: FakeInputControllerService
        },
        CanvasEngine,
        BrushLinesCanvasModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should call #zoomIn on the camera when calling #actionZoomIn", () => {
    component.start();
    fixture.detectChanges();
    const spy = spyOn(component.engine.camera, "zoomIn");
    component.actionZoomIn();
    expect(spy).toHaveBeenCalled();
  });

  it("should call #zoomOut on the camera when calling #actionZoomOut", () => {
    component.start();
    fixture.detectChanges();
    const spy = spyOn(component.engine.camera, "zoomOut");
    component.actionZoomOut();
    expect(spy).toHaveBeenCalled();
  });

  it("should return the zoom of the camera", () => {
    component.start();
    fixture.detectChanges();
    expect(component.zoom.isEqualTo(component.engine.camera.zoom)).toBeTruthy();
    component.actionZoomIn();
    expect(component.zoom.isEqualTo(component.engine.camera.zoom)).toBeTruthy();
    component.actionZoomOut();
    expect(component.zoom.isEqualTo(component.engine.camera.zoom)).toBeTruthy();
  });
});
