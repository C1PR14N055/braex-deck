import "reflect-metadata";
import {
  Component,
  ElementRef,
  OnDestroy,
  Renderer2,
  ViewChild
} from "@angular/core";
import { BrushConfigService } from "../services/brush-config.service";
import { BrushCanvasModule } from "../canvas-modules/modules/brush.canvas-module";
import { MapMarkerCanvasModule } from "../canvas-modules/modules/map-marker.canvas-module";
import { ResizeObserverService } from "../../services/resize-observer.service";
import { ResourceLoadingService } from "../services/resource-loading.service";
import { BrushDotCanvasModule } from "../canvas-modules/modules/brush-dot.canvas-module";
import { MapToolService } from "../services/map-tool.service";
import { TeamBrushSyncCanvasModule } from "../canvas-modules/modules/team-brush-sync.canvas-module";
import { OverwolfGameEventsService } from "../../overwolf/overwolf-game-events.service";
import { CanvasEngine } from "../core/canvas-engine";
import { CanvasModuleRegistryFactory } from "../canvas-modules/canvas-module-registry-factory";
import { BrushLinesCanvasModule } from "../canvas-modules/modules/brush-lines.canvas-module";
import { MapBackgroundCanvasModule } from "../canvas-modules/modules/map-background.canvas-module";
import BigNumber from "bignumber.js";
import { DEFAULT_ZOOM } from "../rendering/camera-2d";
import { TeamLinesSyncCanvasModule } from "../canvas-modules/modules/team-lines-sync.canvas-module";
import { EraserCanvasModule } from "../canvas-modules/modules/eraser.canvas-module";
import { TeamMarkerSyncCanvasModule } from "../canvas-modules/modules/team-marker-sync.canvas-module";
import { MapHotkeysService } from "../../services/map-hotkeys.service";
import { UNSUPPORTED_MAPS } from "../../../../../braex-deck-desktop/src/app/shared/unsupported-maps";
import { CircleZoneCanvasModule } from "../canvas-modules/modules/circle-zone.canvas-module";
import { CanvasModuleRegistry } from "../canvas-modules/canvas-module-registry";
import { ChangeHistoryService } from "../services/change-history.service";

class ViewContext {
  unsupportedMaps = UNSUPPORTED_MAPS;
}

@Component({
  selector: "lib-map-canvas",
  templateUrl: "./map-canvas.component.html",
  styleUrls: [
    "./map-canvas.component.scss",
    "../../../../../styles/theme/messages.scss"
  ],
  host: { class: "d-flex flex-column" }
})
export class MapCanvasComponent implements OnDestroy {
  readonly viewContext = new ViewContext();
  @ViewChild("canvasWrapper")
  private _canvasWrapper: ElementRef<HTMLDivElement>;
  @ViewChild("engineHolder")
  private _engineHolder: ElementRef<HTMLDivElement>;
  private _moduleRegistry: CanvasModuleRegistry;

  constructor(
    private _brushMod: BrushCanvasModule,
    private _mapMarkerMod: MapMarkerCanvasModule,
    private _brushDotMod: BrushDotCanvasModule,
    private _teamBrushSyncMod: TeamBrushSyncCanvasModule,
    private _brushLinesMod: BrushLinesCanvasModule,
    private _teamLinesSyncMod: TeamLinesSyncCanvasModule,
    private _teamMarkerSyncMod: TeamMarkerSyncCanvasModule,
    private _mapBgMod: MapBackgroundCanvasModule,
    private _renderer: Renderer2,
    private _drawingConfigService: BrushConfigService,
    private _resizeObserverService: ResizeObserverService,
    private _resourceLoadingService: ResourceLoadingService,
    private _toolService: MapToolService,
    private _gameEvents: OverwolfGameEventsService,
    private _engine: CanvasEngine,
    private _mapHotkeys: MapHotkeysService,
    private _eraserMod: EraserCanvasModule,
    private _circleZoneMod: CircleZoneCanvasModule,
    private _changeHistory: ChangeHistoryService
  ) {}

  start(...modules: any[]) {
    this._moduleRegistry = CanvasModuleRegistryFactory.create();
    this._moduleRegistry.registerModules(
      ...modules,
      this._brushMod,
      this._mapMarkerMod,
      this._brushDotMod,
      this._teamBrushSyncMod,
      this._brushLinesMod,
      this._teamLinesSyncMod,
      this._teamMarkerSyncMod,
      this._mapBgMod,
      this._eraserMod,
      this._circleZoneMod
    );
    this._engine.attach(this._engineHolder.nativeElement);
    this._engine.start();
  }

  actionZoomIn() {
    this._engine.camera.zoomIn();
  }

  actionZoomOut() {
    this._engine.camera.zoomOut();
  }

  ngOnDestroy() {
    this._changeHistory.clear();
    this._moduleRegistry.unregisterAllModules();
    if (this._engine) {
      this._engine.destroy();
    }
  }

  get zoom() {
    return this._engine.camera
      ? this._engine.camera.zoom
      : Object.freeze(new BigNumber(DEFAULT_ZOOM));
  }

  get engine() {
    return this._engine;
  }

  get map() {
    return this._mapBgMod.currentMap;
  }
}
