import { InputController } from "./input-controller";
import { MouseZoomEvent } from "../ds/events/mouse-zoom-event";
import { Observable, Observer } from "rxjs";
import { MouseClickEvent } from "../ds/events/mouse-click-event";
import { Vector2 } from "../ds/vector2";
import { MouseButton } from "../../ds/mouse-button.enum";
import { MouseWheel } from "../ds/events/mouse-wheel.enum";

/**
 * This {@link InputController} returns the position in the space of the document.
 */
export class BaseInputController implements InputController {
  protected _mouseClickObservers: Observer<MouseClickEvent>[] = [];
  protected _mouseDownObservers: Observer<MouseClickEvent>[] = [];
  protected _mouseMoveObservers: Observer<Vector2>[] = [];
  protected _mouseUpObservers: Observer<MouseClickEvent>[] = [];
  protected _mouseWheelObservers: Observer<MouseZoomEvent>[] = [];
  protected _mousePosition = Vector2.create();

  constructor(protected _target: HTMLElement) {
    this._target.style.userSelect = "none";
    document.addEventListener("mouseup", ev => this._handleMouseUp(ev));
    document.addEventListener("mousemove", ev => this._handleMouseMove(ev));
    this._target.addEventListener("mousewheel", ev =>
      this._handleMouseWheel(ev as WheelEvent)
    );
    this._target.addEventListener("mousedown", ev => this._handleMouseDown(ev));
    this._target.addEventListener("click", ev => this._handleMouseClick(ev));
  }

  private _handleMouseMove(event: MouseEvent) {
    this._mousePosition = Vector2.create(event.clientX, event.clientY);
    this._triggerMouseMove(this._mousePosition);
  }

  private _handleMouseUp(event: MouseEvent) {
    this._triggerMouseUp({
      mouseButton: event.button as MouseButton,
      position: this._mousePosition
    });
  }

  private _handleMouseWheel(event: WheelEvent) {
    let wheel = MouseWheel.ZOOM_IN;
    if (event.deltaY > 0) {
      wheel = MouseWheel.ZOOM_OUT;
    }

    this._triggerMouseWheel({
      position: this._mousePosition,
      wheel: wheel
    });
  }

  private _handleMouseDown(event: MouseEvent) {
    this._triggerMouseDown({
      mouseButton: event.button as MouseButton,
      position: this._mousePosition
    });
  }

  private _handleMouseClick(event: MouseEvent) {
    this._triggerMouseClick({
      mouseButton: event.button as MouseButton,
      position: this._mousePosition
    });
  }

  private _triggerMouseClick(event: MouseClickEvent) {
    this._mouseClickObservers.forEach(x => {
      x.next(event);
    });
  }

  mouseClick(): Observable<MouseClickEvent> {
    return Observable.create(observer => {
      this._mouseClickObservers.push(observer);
    });
  }

  private _triggerMouseDown(event: MouseClickEvent) {
    this._mouseDownObservers.forEach(x => {
      x.next(event);
    });
  }

  mouseDown(): Observable<MouseClickEvent> {
    return Observable.create(observer => {
      this._mouseDownObservers.push(observer);
    });
  }

  private _triggerMouseMove(event: Vector2) {
    this._mouseMoveObservers.forEach(x => {
      x.next(event);
    });
  }

  mouseMove(): Observable<Vector2> {
    return Observable.create(observer => {
      this._mouseMoveObservers.push(observer);
    });
  }

  private _triggerMouseUp(event: MouseClickEvent) {
    this._mouseUpObservers.forEach(x => {
      x.next(event);
    });
  }

  mouseUp(): Observable<MouseClickEvent> {
    return Observable.create(observer => {
      this._mouseUpObservers.push(observer);
    });
  }

  private _triggerMouseWheel(event: MouseZoomEvent) {
    this._mouseWheelObservers.forEach(x => {
      x.next(event);
    });
  }

  mouseWheel(): Observable<MouseZoomEvent> {
    return Observable.create(observer => {
      this._mouseWheelObservers.push(observer);
    });
  }

  destroy() {
    this._mouseClickObservers.forEach(x => {
      x.complete();
    });
    this._mouseClickObservers = [];

    this._mouseDownObservers.forEach(x => {
      x.complete();
    });
    this._mouseDownObservers = [];

    this._mouseMoveObservers.forEach(x => {
      x.complete();
    });
    this._mouseMoveObservers = [];

    this._mouseUpObservers.forEach(x => {
      x.complete();
    });
    this._mouseUpObservers = [];

    this._mouseWheelObservers.forEach(x => {
      x.complete();
    });
    this._mouseWheelObservers = [];
  }
}
