import { MouseZoomEvent } from "../ds/events/mouse-zoom-event";
import { Observable } from "rxjs";
import { MouseClickEvent } from "../ds/events/mouse-click-event";
import { Vector2 } from "../ds/vector2";
import { Camera2D } from "../rendering/camera-2d";
import { BaseInputController } from "./base-input-controller";
import { map } from "rxjs/operators";
import { InputController } from "./input-controller";

/**
 * This {@link InputController} returns the positions of the mouse events in the space of the given {@link Camera2D}.
 */
export class CanvasInputController extends BaseInputController {
  constructor(target: HTMLElement, private _camera: Camera2D) {
    super(target);
  }

  mouseClick(): Observable<MouseClickEvent> {
    return super.mouseClick().pipe(map(x => this._toCameraSpaceClickEvent(x)));
  }

  private _toCameraSpaceClickEvent(event: MouseClickEvent): MouseClickEvent {
    return {
      mouseButton: event.mouseButton,
      position: this._camera.documentToWorldPosition(event.position)
    };
  }

  mouseDown(): Observable<MouseClickEvent> {
    return super.mouseDown().pipe(map(x => this._toCameraSpaceClickEvent(x)));
  }

  mouseMove(): Observable<Vector2> {
    return super
      .mouseMove()
      .pipe(map(x => this._camera.documentToWorldPosition(x)));
  }

  mouseUp(): Observable<MouseClickEvent> {
    return super.mouseUp().pipe(map(x => this._toCameraSpaceClickEvent(x)));
  }

  mouseWheel(): Observable<MouseZoomEvent> {
    return super.mouseWheel().pipe(
      map(x => {
        return {
          wheel: x.wheel,
          position: this._camera.documentToWorldPosition(x.position)
        };
      })
    );
  }
}
