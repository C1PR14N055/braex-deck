import { BaseInputController } from "./base-input-controller";

export abstract class BaseInputControllerFactory {
  static create(target: HTMLElement) {
    return new BaseInputController(target);
  }
}
