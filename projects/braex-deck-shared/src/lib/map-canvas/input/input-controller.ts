import { Observable } from "rxjs";
import { MouseClickEvent } from "../ds/events/mouse-click-event";
import { Vector2 } from "../ds/vector2";
import { MouseZoomEvent } from "../ds/events/mouse-zoom-event";

export interface InputController {
  mouseClick(): Observable<MouseClickEvent>;

  mouseDown(): Observable<MouseClickEvent>;

  mouseMove(): Observable<Vector2>;

  mouseUp(): Observable<MouseClickEvent>;

  mouseWheel(): Observable<MouseZoomEvent>;
}
