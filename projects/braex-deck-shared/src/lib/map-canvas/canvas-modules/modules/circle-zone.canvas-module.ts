import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit
} from "../canvas-module-hooks";
import { CanvasModule } from "../canvas-module.decorator";
import { Vector2 } from "../../ds/vector2";
import { OverwolfGameEventsService } from "../../../overwolf/overwolf-game-events.service";
import { GamePhase } from "../../../ds/game/game-phase.enum";
import { Subscription } from "rxjs";

// FIXME: ZONE NAMES ARE REVERSED (SAFE -> BLUE
export const BLUE_ZONE_COLOR = "#eeeeee22";
export const BLUE_ZONE_STROKE_COLOR = "#eeeeee";
export const RED_ZONE_COLOR = "#d91e1822";
export const RED_ZONE_STROKE_COLOR = "#d91e18";
export const SAFE_ZONE_COLOR = "#2c82c922";
export const SAFE_ZONE_STROKE_COLOR = "#2c82c9";

export class CircleZone {
  position: Vector2;
  radius: number;

  constructor(position, radius) {
    this.position = position;
    this.radius = radius;
  }
}

@CanvasModule({
  zIndex: 500
})
export class CircleZoneCanvasModule
  implements CanvasModuleInit, CanvasModuleDestroy, CanvasModuleDraw {
  private _subs: Subscription[] = [];
  private _blueZone: CircleZone;
  private _redZone: CircleZone;
  private _safeZone: CircleZone;
  private _redraw = false;

  constructor(private _gameEvents: OverwolfGameEventsService) {}

  onInit() {
    const sub = this._gameEvents.onPhaseChanged().subscribe(phase => {
      if (phase === GamePhase.LOBBY) {
        this.clear();
      }
    });
    this._subs.push(sub);
  }

  onDestroy(): void {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  setBlueZone(blueZone: any) {
    this._blueZone = blueZone;
    this._redraw = true;
  }

  setRedZone(redZone: any) {
    this._redZone = redZone;
    this._redraw = true;
  }

  setSafeZone(safeZone: any) {
    this._safeZone = safeZone;
    this._redraw = true;
  }

  onDraw(canvasContext: CanvasRenderingContext2D) {
    const transform = canvasContext.getTransform().a;

    if (this._blueZone) {
      canvasContext.beginPath();
      canvasContext.arc(
        this._blueZone.position.x,
        this._blueZone.position.y,
        this._blueZone.radius,
        0,
        2 * Math.PI
      );
      canvasContext.lineWidth = 2 / transform;
      canvasContext.strokeStyle = BLUE_ZONE_STROKE_COLOR;
      canvasContext.stroke();
    }
    if (this._redZone) {
      canvasContext.beginPath();
      canvasContext.arc(
        this._redZone.position.x,
        this._redZone.position.y,
        this._redZone.radius,
        0,
        2 * Math.PI
      );
      canvasContext.fillStyle = RED_ZONE_COLOR;
      canvasContext.fill();
      canvasContext.lineWidth = 2 / transform;
      canvasContext.strokeStyle = RED_ZONE_STROKE_COLOR;
      canvasContext.stroke();
    }
    if (this._safeZone) {
      canvasContext.beginPath();
      canvasContext.arc(
        this._safeZone.position.x,
        this._safeZone.position.y,
        this._safeZone.radius,
        0,
        2 * Math.PI
      );
      canvasContext.lineWidth = 2 / transform;
      canvasContext.strokeStyle = SAFE_ZONE_STROKE_COLOR;
      canvasContext.stroke();
    }

    this._redraw = false;
  }

  clear() {
    this._blueZone = null;
    this._redZone = null;
    this._safeZone = null;
    this._redraw = true;
  }

  get redraw() {
    return this._redraw;
  }
}
