import { async, fakeAsync, TestBed, tick } from "@angular/core/testing";
import { MapMarkerCanvasModule } from "./map-marker.canvas-module";
import { Vector2 } from "../../ds/vector2";
import { MapCanvasTool } from "../../ds/map-canvas-tool.enum";
import { MapToolService } from "../../services/map-tool.service";
import { TOOL_MOUSE_BUTTON } from "../../ds/tool-config";
import { FakeInputControllerService } from "../../services/testing/fake-input-controller.service";
import { InputControllerService } from "../../services/input-controller.service";
import { FakeDrawingSyncService } from "../../services/testing/fake-drawing-sync.service";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { FakeLocalPlayerInfoService } from "../../../services/testing/fake-local-player-info.service";
import { LocalPlayerInfoService } from "../../../services/local-player-info.service";
import { MarkerSyncBody } from "../../ds/drawing-sync/marker-sync-body";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { MarkerConfigService } from "../../services/marker-config.service";
import { OverwolfGameEventsService } from "../../../overwolf/overwolf-game-events.service";
import { FakeOverwolfGameEventsService } from "../../../overwolf/testing/fake-overwolf-game-events.service";
import { GamePhase } from "../../../ds/game/game-phase.enum";

describe("MapMarkerCanvasModule", () => {
  let module: MapMarkerCanvasModule;
  let fakeInputController: FakeInputControllerService;
  let toolService: MapToolService;
  let fakeDrawingSync: FakeDrawingSyncService;
  let fakeLocalPlayer: FakeLocalPlayerInfoService;
  let markerConfig: MarkerConfigService;
  let fakeGameEvents: FakeOverwolfGameEventsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        MapMarkerCanvasModule,
        MapToolService,
        {
          provide: InputControllerService,
          useClass: FakeInputControllerService
        },
        {
          provide: DrawingSyncService,
          useClass: FakeDrawingSyncService
        },
        {
          provide: LocalPlayerInfoService,
          useClass: FakeLocalPlayerInfoService
        },
        MarkerConfigService,
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        }
      ]
    });
    module = TestBed.get(MapMarkerCanvasModule);
    fakeInputController = TestBed.get(InputControllerService);
    fakeDrawingSync = TestBed.get(DrawingSyncService);
    fakeLocalPlayer = TestBed.get(LocalPlayerInfoService);
    toolService = TestBed.get(MapToolService);
    markerConfig = TestBed.get(MarkerConfigService);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    module.onInit();
  }));

  it("should create", () => {
    expect(module).toBeTruthy();
  });

  it("should place a marker when the active tool is MARKER and the tool button is clicked", fakeAsync(() => {
    toolService.activeTool = MapCanvasTool.MARKER;
    const vector = Vector2.create(90, 120);

    expect(module.placedMarkers.size).toBeFalsy();
    fakeInputController.triggerMouseUp({
      position: vector,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(300);

    expect(module.placedMarkers.size).toBeTruthy();
  }));

  it("should NOT place marker when the active tool is NOT MARKER and lmb is clicked", fakeAsync(() => {
    toolService.activeTool = MapCanvasTool.SELECTION;
    const vector = Vector2.create(39, 28);

    expect(module.placedMarkers.size).toBeFalsy();
    fakeInputController.triggerMouseUp({
      position: vector,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(300);

    expect(module.placedMarkers.size).toBeFalsy();
  }));

  it("should place the marker at the position of the mouse", fakeAsync(() => {
    toolService.activeTool = MapCanvasTool.MARKER;
    const vector = Vector2.create(900, 650);
    expect(module.placedMarkers.size).toBeFalsy();
    fakeInputController.triggerMouseUp({
      position: vector,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(300);

    expect(module.placedMarkers.size).toBeTruthy();
    const marker = module.placedMarkers.get(0);
    expect(marker.position.x).toBeCloseTo(vector.x);
    expect(marker.position.y).toBeCloseTo(vector.y);
    expect(
      marker.config.equals(markerConfig.createMarkerConfig())
    ).toBeTruthy();
  }));

  it("should send the marker event when a marker is placed", fakeAsync(() => {
    fakeLocalPlayer.triggerLocalPlayerNickname("TestPlayer");
    tick(500);

    toolService.activeTool = MapCanvasTool.MARKER;
    const vector = Vector2.create(900, 1231);
    let received: DrawingSyncEvent<MarkerSyncBody>;
    const spy = spyOn(fakeDrawingSync, "sendEvent").and.callFake(x => {
      received = x;
    });

    fakeInputController.triggerMouseUp({
      position: vector,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(300);

    expect(spy).toHaveBeenCalled();
    expect(received.body.position.x).toBeCloseTo(vector.x);
    expect(received.body.position.y).toBeCloseTo(vector.y);
  }));

  it("should remove a placed marker", () => {
    const markerPositions = [
      Vector2.create(32, 51),
      Vector2.create(24, 23),
      Vector2.create(59, 22)
    ];
    toolService.activeTool = MapCanvasTool.MARKER;
    markerPositions.forEach(mp => module.addMarker(mp));
    expect(module.placedMarkers.size).toEqual(3);
    module.removeMarker(module.placedMarkers.get(0));
    expect(module.placedMarkers.size).toEqual(2);
  });

  it("should clear the markers when the phase changes to LOBBY and it has been different before", fakeAsync(() => {
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(20);

    module.addMarker(Vector2.create(129, 123));
    expect(module.placedMarkers.isEmpty()).toBeFalsy();

    fakeGameEvents.triggerPhaseChanged(GamePhase.LOBBY);
    tick(20);
    expect(module.placedMarkers.isEmpty()).toBeTruthy();
  }));

  it("should mark for redraw when the phase change to LOBBY  and it has been different before", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(20);
    module.addMarker(Vector2.create(1, 10));
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
    fakeGameEvents.triggerPhaseChanged(GamePhase.LOBBY);
    tick(20);
    expect(module.redraw).toBeTruthy();
  }));

  it("should not cleat the markers when randomly receiving the phase LOBBY", fakeAsync(() => {
    module.addMarker(Vector2.create(420, 420));

    expect(module.placedMarkers.isEmpty()).toBeFalsy();
    fakeGameEvents.triggerPhaseChanged(GamePhase.LOBBY);
    expect(module.placedMarkers.isEmpty()).toBeFalsy();
  }));

  afterEach(() => {
    module.onDestroy();
  });
});
