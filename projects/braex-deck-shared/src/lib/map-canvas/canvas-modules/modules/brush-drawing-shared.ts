import { Vector2 } from "../../ds/vector2";
import { LinePath } from "../../ds/line-path";
import { List } from "immutable";

export abstract class BrushDrawingShared {
  static drawTempBrushPoints(
    context: CanvasRenderingContext2D,
    points: Vector2[]
  ) {
    if (!points.length) {
      return;
    }

    context.beginPath();
    const firstPoint = points[0];
    context.moveTo(firstPoint.x, firstPoint.y);

    if (points.length === 1) {
      context.lineTo(firstPoint.x, firstPoint.y);
    } else if (points.length > 1) {
      for (let i = 1; i < points.length; i++) {
        const point = points[i];
        context.lineTo(point.x, point.y);
      }
    }

    context.stroke();
    context.closePath();
  }

  static drawLinePaths(
    context: CanvasRenderingContext2D,
    linePaths: List<LinePath>
  ) {
    linePaths.forEach(linePath => {
      linePath.brushConfig.apply(context);

      context.beginPath();
      const firstPoint = linePath.points.get(0);
      context.moveTo(firstPoint.x, firstPoint.y);

      if (linePath.points.size === 1) {
        context.lineTo(firstPoint.x, firstPoint.y);
      } else if (linePath.points.size > 1) {
        for (let i = 1; i < linePath.points.size; i++) {
          const point = linePath.points.get(i);
          context.lineTo(point.x, point.y);
        }
      }

      context.stroke();
      context.closePath();
    });
  }
}
