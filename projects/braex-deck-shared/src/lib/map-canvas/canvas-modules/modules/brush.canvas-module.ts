import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit,
  CanvasModuleUpdate
} from "../canvas-module-hooks";
import { Subscription } from "rxjs";
import { LinePath } from "../../ds/line-path";
import { BrushConfigService } from "../../services/brush-config.service";
import { BrushConfig } from "../../ds/brush-config";
import { Vector2 } from "../../ds/vector2";
import { List } from "immutable";
import { CanvasModule } from "../canvas-module.decorator";
import { MapToolService } from "../../services/map-tool.service";
import { MapCanvasTool } from "../../ds/map-canvas-tool.enum";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { MapTool } from "../../core/map-tool.decorator";
import { TOOL_MOUSE_BUTTON } from "../../ds/tool-config";
import { LocalPlayerInfoService } from "../../../services/local-player-info.service";
import { BrushDrawingShared } from "./brush-drawing-shared";
import { InputControllerService } from "../../services/input-controller.service";
import { BrushLinesCanvasModule } from "./brush-lines.canvas-module";
import { faPaintBrush } from "@fortawesome/free-solid-svg-icons";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { PaintEventState } from "../../ds/drawing-sync/paint-event-state.enum";
import { PaintSyncBody } from "../../ds/drawing-sync/paint-sync-body";
import { DrawingSyncEventType } from "../../ds/drawing-sync/drawing-sync-event-type";

@CanvasModule({
  zIndex: 20
})
@MapTool({
  tool: MapCanvasTool.BRUSH,
  cursor: "none",
  icon: faPaintBrush
})
export class BrushCanvasModule
  implements
    CanvasModuleInit,
    CanvasModuleDestroy,
    CanvasModuleUpdate,
    CanvasModuleDraw {
  private _active = false;
  private _subs: Subscription[] = [];
  private _tempBrushPoints: Vector2[] = [];
  private _localPlayerNickname: string | null = null;
  private _lastSyncedPosition: Vector2 | null = null;
  private _redraw = false;

  constructor(
    private _inputController: InputControllerService,
    private _brushConfigService: BrushConfigService,
    private _mapToolService: MapToolService,
    private _drawingSyncService: DrawingSyncService,
    private _localPlayer: LocalPlayerInfoService,
    private _brushLines: BrushLinesCanvasModule
  ) {}

  onInit() {
    this._initBrushPointSyncing();
    this._initCanvasMouseEvents();
  }

  private _initBrushPointSyncing() {
    const sub = this._localPlayer.localPlayerNickname().subscribe(nickname => {
      this._localPlayerNickname = nickname;
    });
    this._subs.push(sub);
  }

  private _initCanvasMouseEvents() {
    let sub = this._inputController.mouseDown().subscribe(event => {
      if (this._mapToolService.activeTool !== MapCanvasTool.BRUSH) {
        return;
      }

      if (event.mouseButton === TOOL_MOUSE_BUTTON) {
        this._active = true;
      }
    });
    this._subs.push(sub);

    sub = this._inputController.mouseUp().subscribe(event => {
      if (!this._active) {
        return;
      }

      if (event.mouseButton === TOOL_MOUSE_BUTTON) {
        this._active = false;
        this._drawingSyncService.sendEvent(
          new DrawingSyncEvent(
            DrawingSyncEventType.PAINT,
            new PaintSyncBody(PaintEventState.END, null),
            this._localPlayerNickname
          )
        );
        this._lastSyncedPosition = null;
      }

      if (this._tempBrushPoints.length) {
        const linePath = LinePath.create(
          List(this._tempBrushPoints),
          this._brushConfigService.createBrushConfig()
        );
        this._brushLines.addLinePath(linePath);
        this._redraw = true;
        this._tempBrushPoints.length = 0;
      }
    });
    this._subs.push(sub);
  }

  onDestroy() {
    this._subs.forEach(x => x.unsubscribe());
  }

  onUpdate(mousePosition: Vector2) {
    if (!this._active) {
      return;
    }

    if (!this._tempBrushPoints.length) {
      this._lastSyncedPosition = mousePosition;
      this._drawingSyncService.sendEvent(
        new DrawingSyncEvent(
          DrawingSyncEventType.PAINT,
          new PaintSyncBody(
            PaintEventState.START,
            mousePosition,
            this._brushConfigService.createBrushConfig()
          ),
          this._localPlayerNickname
        )
      );
    } else {
      if (!mousePosition.equals(this._lastSyncedPosition)) {
        this._lastSyncedPosition = mousePosition;
        this._drawingSyncService.sendEvent(
          new DrawingSyncEvent(
            DrawingSyncEventType.PAINT,
            new PaintSyncBody(PaintEventState.MIDDLE, mousePosition),
            this._localPlayerNickname
          )
        );
      }
    }

    if (
      !this._tempBrushPoints.length ||
      !this._tempBrushPoints[this._tempBrushPoints.length - 1].equals(
        mousePosition
      )
    ) {
      this._tempBrushPoints.push(mousePosition);
      this._redraw = true;
    }
  }

  onDraw(canvasContext: CanvasRenderingContext2D) {
    this._applyLineStyles(
      canvasContext,
      this._brushConfigService.createBrushConfig()
    );

    if (this._tempBrushPoints.length) {
      BrushDrawingShared.drawTempBrushPoints(
        canvasContext,
        this._tempBrushPoints
      );
    }
    this._redraw = false;
  }

  private _applyLineStyles(
    canvasContext: CanvasRenderingContext2D,
    brushConfig: BrushConfig
  ) {
    canvasContext.lineWidth = brushConfig.thickness;
    canvasContext.lineCap = "round";
    canvasContext.lineJoin = "round";
    canvasContext.strokeStyle = brushConfig.color;
  }

  get active() {
    return this._active;
  }

  get redraw() {
    return this._redraw;
  }
}
