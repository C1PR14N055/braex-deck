import { List } from "immutable";
import { MapPlayer } from "../../ds/map-player";
import { InternalError } from "../../../error/internal.error";
import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit
} from "../canvas-module-hooks";
import { CanvasResource } from "../../ds/canvas-resource";
import { ResourceLoadingService } from "../../services/resource-loading.service";
import { Vector2 } from "../../ds/vector2";
import { Color } from "../../ds/color.enum";
import { CanvasModule } from "../canvas-module.decorator";
import { OverwolfCoreService } from "../../../overwolf/overwolf-core.service";
import { OverwolfGameEventsService } from "../../../overwolf/overwolf-game-events.service";
import { Subscription } from "rxjs";
import { GamePhase } from "../../../ds/game/game-phase.enum";

export const PLAYER_SIZE = Vector2.create(10, 10);
export const PLAYER_DEATH_SIZE = Vector2.create(6, 8);
/**
 * The zoom after which the names of the players start to be displayed
 */
export const NICKNAME_ZOOM_THRESHOLD = 1.8;
export const PLAYER_COLOR = "#2c82c9";
export const PLAYER_DEATH_COLOR = "#d91e18";
/**
 * The y offset of the box with the nickname of the player to the figure of the player.
 */
export const NICKNAME_BOX_OFFSET_Y = 9;
export const NICKNAME_FONT_SIZE = 11;
export const NICKNAME_BOX_PADDING = Vector2.create(5, 1);

@CanvasModule({
  zIndex: 400
})
export class PlayerMovementCanvasModule
  implements CanvasModuleInit, CanvasModuleDestroy, CanvasModuleDraw {
  private _subs: Subscription[] = [];
  private _redraw = false;
  private _playerImage: CanvasResource;
  private _playerDeathImage: CanvasResource;
  private _players = List<MapPlayer>();

  constructor(
    private _resourceLoading: ResourceLoadingService,
    private _overwolfCore: OverwolfCoreService,
    private _gameEvents: OverwolfGameEventsService
  ) {}

  onInit() {
    this._initResources();
    this._initAutoClearing();
    const sub = this._gameEvents.onPhaseChanged().subscribe(phase => {
      if (phase === GamePhase.LOBBY) {
        this.clear();
      }
    });
    this._subs.push(sub);
  }

  private _initResources() {
    this._resourceLoading
      .loadImage("/assets/img/player/map-player.png")
      .subscribe(resource => {
        this._playerImage = resource;
      });
    this._resourceLoading
      .loadImage("/assets/img/player/death.png")
      .subscribe(resource => {
        this._playerDeathImage = resource;
      });
  }

  private _initAutoClearing() {
    let sub = this._gameEvents.onMatchLeft().subscribe(() => {
      this._players = this._players.clear();
      this._redraw = true;
    });
    this._subs.push(sub);

    sub = this._overwolfCore.onPubgTerminated().subscribe(() => {
      this._players = this._players.clear();
      this._redraw = true;
    });
    this._subs.push(sub);
  }

  addPlayer(...players: MapPlayer[]) {
    players.forEach(player => {
      const matching = this._players.filter(
        x => x.nickname === player.nickname
      );
      if (matching.size) {
        throw new InternalError(
          `Tried to add the player \`${player.nickname}\` twice`
        );
      }
    });
    this._players = this._players.push(...players);
    this._redraw = true;
  }

  updatePlayer(...players: MapPlayer[]) {
    players.forEach(player => {
      const matching = this._players.filter(
        x => x.nickname === player.nickname
      );
      // FIXME(pmo): comment this in again when telemetry handles it differently
      // if (!matching.size) {
      //   throw new InternalError(
      //     `Could not find the player \`${
      //       player.nickname
      //     }\`... Did you forget to add it?`
      //   );
      // }

      const index = this._players.indexOf(matching.get(0));
      this._players = this._players.splice(index, 1, player);
    });
    this._redraw = true;
  }

  removePlayer(...nicknames: string[]) {
    nicknames.forEach(nickname => {
      const matching = this._players.filter(x => x.nickname === nickname);
      // FIXME(pmo): comment this in again when telemetry handles it differently
      // if (!matching.size) {
      //   throw new InternalError(
      //     `Could not find the player \`${nickname}\`... Did you forget to add it?`
      //   );
      // }

      const index = this._players.indexOf(matching.get(0));
      this._players = this._players.splice(index, 1);
    });
    this._redraw = true;
  }

  onDraw(canvasContext: CanvasRenderingContext2D) {
    if (!this._playerImage) {
      return;
    }

    const transform = canvasContext.getTransform();
    this._players.forEach(player => {
      canvasContext.beginPath();

      const playerIconPosition = player.position.subtract(
        PLAYER_SIZE.divide(2).divide(transform.a)
      );
      let playerSize: Vector2;

      if (player.alive) {
        playerSize = PLAYER_SIZE.divide(transform.a);
        canvasContext.drawImage(
          this._playerImage.image,
          playerIconPosition.x,
          playerIconPosition.y,
          playerSize.x,
          playerSize.y
        );
        canvasContext.globalCompositeOperation = "source-atop";
        canvasContext.fillStyle = PLAYER_COLOR;
        canvasContext.rect(
          playerIconPosition.x,
          playerIconPosition.y,
          playerSize.x,
          playerSize.y
        );
        canvasContext.fill();
        canvasContext.globalCompositeOperation = "source-over";
      } else {
        playerSize = PLAYER_DEATH_SIZE.divide(transform.a);
        canvasContext.drawImage(
          this._playerDeathImage.image,
          playerIconPosition.x,
          playerIconPosition.y,
          playerSize.x,
          playerSize.y
        );
        canvasContext.globalCompositeOperation = "source-atop";
        canvasContext.fillStyle = PLAYER_DEATH_COLOR;
        canvasContext.rect(
          playerIconPosition.x,
          playerIconPosition.y,
          playerSize.x,
          playerSize.y
        );
        canvasContext.fill();
        canvasContext.globalCompositeOperation = "source-over";
      }

      // draw the name of the player when we have zoomed in to the point
      // where it should be displayed
      if (canvasContext.getTransform().a > NICKNAME_ZOOM_THRESHOLD) {
        const fontSize = NICKNAME_FONT_SIZE / transform.a;
        canvasContext.font = `400 ${fontSize}px arial`;
        const measure = canvasContext.measureText(player.nickname);
        const nicknameOffsetY = NICKNAME_BOX_OFFSET_Y / transform.a;

        // draw the box around the nickname
        canvasContext.beginPath();
        if (player.alive) {
          canvasContext.fillStyle = PLAYER_COLOR;
        } else {
          canvasContext.fillStyle = PLAYER_DEATH_COLOR;
        }
        canvasContext.rect(
          player.position.x -
            measure.width / 2 -
            playerSize.x +
            NICKNAME_BOX_PADDING.x / transform.a,
          player.position.y + nicknameOffsetY,
          measure.width + (NICKNAME_BOX_PADDING.x / transform.a) * 2,
          fontSize + (NICKNAME_BOX_PADDING.y / transform.a) * 2
        );
        canvasContext.fill();
        canvasContext.closePath();

        // draw nickname
        canvasContext.beginPath();
        canvasContext.fillStyle = Color.WHITE;
        canvasContext.fillText(
          player.nickname,
          player.position.x - measure.width / 2,
          player.position.y + nicknameOffsetY + fontSize
        );
        canvasContext.closePath();
      }
    });
    this._redraw = false;
  }

  onDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  clear() {
    this._players = this._players.clear();
    this._redraw = true;
  }

  get players() {
    return this._players;
  }

  get redraw() {
    return this._redraw;
  }
}
