import { TeamLinesSyncCanvasModule } from "./team-lines-sync.canvas-module";
import { fakeAsync, TestBed, tick } from "@angular/core/testing";
import { List } from "immutable";
import { LinePath } from "../../ds/line-path";
import { Vector2 } from "../../ds/vector2";
import { BrushConfig } from "../../ds/brush-config";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { FakeDrawingSyncService } from "../../services/testing/fake-drawing-sync.service";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { ChangeHistorySyncBody } from "../../ds/drawing-sync/change-history-sync-body";
import { DrawingSyncEventType } from "../../ds/drawing-sync/drawing-sync-event-type";
import { FakeOverwolfGameEventsService } from "../../../overwolf/testing/fake-overwolf-game-events.service";
import { OverwolfGameEventsService } from "../../../overwolf/overwolf-game-events.service";
import { GamePhase } from "../../../ds/game/game-phase.enum";

describe("TeamLinesSyncCanvasModule", () => {
  let module: TeamLinesSyncCanvasModule;
  let fakeDrawSync: FakeDrawingSyncService;
  let fakeGameEvents: FakeOverwolfGameEventsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TeamLinesSyncCanvasModule,
        { provide: DrawingSyncService, useClass: FakeDrawingSyncService },
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        }
      ]
    });

    module = TestBed.get(TeamLinesSyncCanvasModule);
    fakeDrawSync = TestBed.get(DrawingSyncService);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    module.onInit();
  });

  it("should be created", () => {
    expect(module).toBeTruthy();
  });

  it("should add new owners to the team mate brushes to sync", () => {
    expect(module.teamBrushes.isEmpty()).toBeTruthy();
    const playerNickname = "TestPlayer3";
    const line = LinePath.create(
      List<Vector2>([
        Vector2.create(54, 39),
        Vector2.create(89, 123),
        Vector2.create(12, 49),
        Vector2.create(90, 42)
      ]),
      new BrushConfig()
    );
    module.addTeamBrush(playerNickname, line);

    expect(module.teamBrushes.has(playerNickname)).toBeTruthy();
    const list = module.teamBrushes.get(playerNickname);
    expect(list.size).toBe(1);
    expect(list).toContain(line);
  });

  it("should mark the redraw as dirty when a new team mate is added", () => {
    expect(module.teamBrushes.isEmpty()).toBeTruthy();
    expect(module.redraw).toBeFalsy();

    const playerName = "TestPlayer2";
    const line = LinePath.create(
      List<Vector2>([
        Vector2.create(90, 23),
        Vector2.create(19, 78),
        Vector2.create(34, 42),
        Vector2.create(7, 98)
      ])
    );
    module.addTeamBrush(playerName, line);
    expect(module.redraw).toBeTruthy();
  });

  it("should mark the redraw as dirty when an existing team mate is added", () => {
    expect(module.teamBrushes.isEmpty()).toBeTruthy();
    expect(module.redraw).toBeFalsy();

    const playerName = "TestPlayer4";
    const line1 = LinePath.create(
      List<Vector2>([
        Vector2.create(94, 42),
        Vector2.create(42, 42),
        Vector2.create(19, 12),
        Vector2.create(73, 89)
      ])
    );
    module.addTeamBrush(playerName, line1);
    expect(module.redraw).toBeTruthy();

    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    module.onDraw(context);
    expect(module.redraw).toBeFalsy();

    const line2 = LinePath.create(
      List<Vector2>([
        Vector2.create(16, 12),
        Vector2.create(16, 397),
        Vector2.create(90, 123)
      ])
    );
    module.addTeamBrush(playerName, line2);
  });

  it("should mark the redraw as NOT dirty after the changes have been drawn", () => {
    expect(module.teamBrushes.isEmpty()).toBeTruthy();
    expect(module.redraw).toBeFalsy();

    const playerName = "SomePlayer";
    const line = LinePath.create(
      List<Vector2>([
        Vector2.create(16, 42),
        Vector2.create(90, 71),
        Vector2.create(87, 67),
        Vector2.create(14, 79)
      ])
    );
    module.addTeamBrush(playerName, line);

    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
  });

  it("should clear all lines of a team mate when receiving the clear event", fakeAsync(() => {
    const playerName = "Uwewewewe";
    const line1 = LinePath.create(
      List<Vector2>([
        Vector2.create(16, 42),
        Vector2.create(90, 71),
        Vector2.create(87, 67),
        Vector2.create(14, 79)
      ])
    );
    const line2 = LinePath.create(
      List<Vector2>([
        Vector2.create(10, 1337),
        Vector2.create(123, 12),
        Vector2.create(346, 653),
        Vector2.create(4, 573)
      ])
    );
    const line3 = LinePath.create(
      List<Vector2>([
        Vector2.create(23, 53),
        Vector2.create(53, 23),
        Vector2.create(12, 75)
      ])
    );
    module.addTeamBrush(playerName, line1);
    module.addTeamBrush(playerName, line2);
    module.addTeamBrush(playerName, line3);
    expect(module.teamBrushes.isEmpty()).toBeFalsy();
    let brushes = module.teamBrushes.get(playerName);
    expect(brushes.size).toBe(3);

    fakeDrawSync.triggerPaintClearEvent(
      new DrawingSyncEvent<void>(
        DrawingSyncEventType.CLEAR_PAINT,
        null,
        playerName
      )
    );
    tick(400);
    expect(module.teamBrushes.isEmpty()).toBeFalsy();
    brushes = module.teamBrushes.get(playerName);
    expect(brushes.isEmpty()).toBeTruthy();
  }));

  it("should undo a line of a team mate when receiving the undo event", fakeAsync(() => {
    const playerName = "LickName";
    const line1 = LinePath.create(
      List<Vector2>([
        Vector2.create(16, 42),
        Vector2.create(90, 71),
        Vector2.create(87, 67),
        Vector2.create(14, 79)
      ])
    );
    const line2 = LinePath.create(
      List<Vector2>([
        Vector2.create(10, 1337),
        Vector2.create(123, 12),
        Vector2.create(346, 653),
        Vector2.create(4, 573)
      ])
    );
    module.addTeamBrush(playerName, line1);
    module.addTeamBrush(playerName, line2);
    expect(module.teamBrushes.isEmpty()).toBeFalsy();
    let brushes = module.teamBrushes.get(playerName);
    expect(brushes.size).toBe(2);

    fakeDrawSync.triggerPaintUndoEvent(
      new DrawingSyncEvent<ChangeHistorySyncBody>(
        DrawingSyncEventType.UNDO_PAINT,
        new ChangeHistorySyncBody(line2),
        playerName
      )
    );
    tick(400);
    brushes = module.teamBrushes.get(playerName);
    expect(brushes.size).toBe(1);
    expect(brushes.get(0)).toBe(line1);
  }));

  it("should redo a line of a team mate when receiving the redo event", fakeAsync(() => {
    const playerName = "GitHub";
    const line1 = LinePath.create(
      List<Vector2>([
        Vector2.create(16, 42),
        Vector2.create(90, 71),
        Vector2.create(87, 67),
        Vector2.create(14, 79)
      ])
    );
    module.addTeamBrush(playerName, line1);
    expect(module.teamBrushes.isEmpty()).toBeFalsy();
    let brushes = module.teamBrushes.get(playerName);
    expect(brushes.size).toBe(1);

    const line2 = LinePath.create(
      List<Vector2>([
        Vector2.create(10, 1337),
        Vector2.create(123, 12),
        Vector2.create(346, 653),
        Vector2.create(4, 573)
      ])
    );
    fakeDrawSync.triggerPaintRedoEvent(
      new DrawingSyncEvent<ChangeHistorySyncBody>(
        DrawingSyncEventType.REDO_PAINT,
        new ChangeHistorySyncBody(line2),
        playerName
      )
    );
    tick(400);
    brushes = module.teamBrushes.get(playerName);
    expect(brushes.size).toBe(2);
    expect(brushes).toContain(line1);
    expect(brushes).toContain(line2);
  }));

  it("should mark the redraw as dirty when receiving the clear event", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    const playerName = "Uwewewewe";
    const line1 = LinePath.create(
      List<Vector2>([
        Vector2.create(16, 42),
        Vector2.create(90, 71),
        Vector2.create(87, 67),
        Vector2.create(14, 79)
      ])
    );
    module.addTeamBrush(playerName, line1);
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
    fakeDrawSync.triggerPaintClearEvent(
      new DrawingSyncEvent<void>(
        DrawingSyncEventType.CLEAR_PAINT,
        null,
        playerName
      )
    );
    tick(400);
    expect(module.redraw).toBeTruthy();
  }));

  it("should mark the redraw as dirty when receiving the undo event", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    const playerName = "LickName";
    const line1 = LinePath.create(
      List<Vector2>([
        Vector2.create(16, 42),
        Vector2.create(90, 71),
        Vector2.create(87, 67),
        Vector2.create(14, 79)
      ])
    );
    const line2 = LinePath.create(
      List<Vector2>([
        Vector2.create(10, 1337),
        Vector2.create(123, 12),
        Vector2.create(346, 653),
        Vector2.create(4, 573)
      ])
    );
    module.addTeamBrush(playerName, line1);
    module.addTeamBrush(playerName, line2);
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
    fakeDrawSync.triggerPaintUndoEvent(
      new DrawingSyncEvent<ChangeHistorySyncBody>(
        DrawingSyncEventType.UNDO_PAINT,
        new ChangeHistorySyncBody(line2),
        playerName
      )
    );
    tick(400);
    expect(module.redraw).toBeTruthy();
  }));

  it("should mark the redraw as dirty when receiving the redo event", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    const playerName = "GitHub";
    const line1 = LinePath.create(
      List<Vector2>([
        Vector2.create(16, 42),
        Vector2.create(90, 71),
        Vector2.create(87, 67),
        Vector2.create(14, 79)
      ])
    );
    module.addTeamBrush(playerName, line1);
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
    const line2 = LinePath.create(
      List<Vector2>([
        Vector2.create(10, 1337),
        Vector2.create(123, 12),
        Vector2.create(346, 653),
        Vector2.create(4, 573)
      ])
    );
    fakeDrawSync.triggerPaintRedoEvent(
      new DrawingSyncEvent<ChangeHistorySyncBody>(
        DrawingSyncEventType.REDO_PAINT,
        new ChangeHistorySyncBody(line2),
        playerName
      )
    );
    tick(400);
    expect(module.redraw).toBeTruthy();
  }));

  it("should clear all lines when the phase changes to LOBBY and it has been different before", fakeAsync(() => {
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(20);

    const playerName = "TestPlayer2";
    const line = LinePath.create(
      List<Vector2>([
        Vector2.create(90, 23),
        Vector2.create(19, 78),
        Vector2.create(34, 42),
        Vector2.create(7, 98)
      ])
    );
    module.addTeamBrush(playerName, line);
    expect(module.teamBrushes.isEmpty()).toBeFalsy();

    fakeGameEvents.triggerPhaseChanged(GamePhase.LOBBY);
    tick(20);
    expect(module.teamBrushes.isEmpty()).toBeTruthy();
  }));

  it("should mark for redraw when the phase changes to LOBBY and it has been different before", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(20);
    const playerName = "TestPlayer2";
    const line = LinePath.create(
      List<Vector2>([
        Vector2.create(90, 23),
        Vector2.create(19, 78),
        Vector2.create(34, 42),
        Vector2.create(7, 98)
      ])
    );
    module.addTeamBrush(playerName, line);
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
    fakeGameEvents.triggerPhaseChanged(GamePhase.LOBBY);
    tick(20);
    expect(module.redraw).toBeTruthy();
  }));

  it("should NOT clear the lines when randomly receiving the phase LOBBY", fakeAsync(() => {
    const playerName = "TestPlayer2";
    const line = LinePath.create(
      List<Vector2>([
        Vector2.create(90, 23),
        Vector2.create(19, 78),
        Vector2.create(34, 42),
        Vector2.create(7, 98)
      ])
    );
    module.addTeamBrush(playerName, line);

    expect(module.teamBrushes.isEmpty()).toBeFalsy();
    fakeGameEvents.triggerPhaseChanged(GamePhase.LOBBY);
    tick(20);
    expect(module.teamBrushes.isEmpty()).toBeFalsy();
  }));

  afterEach(() => {
    module.onDestroy();
  });
});
