import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit
} from "../canvas-module-hooks";
import { CanvasModule } from "../canvas-module.decorator";
import { LocalPlayerInfoService } from "../../../services/local-player-info.service";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { Subscription } from "rxjs";
import { TeamBrushHolder } from "../ds/team-brush-holder";
import { List } from "immutable";
import { LinePath } from "../../ds/line-path";
import { BrushDrawingShared } from "./brush-drawing-shared";
import { TeamLinesSyncCanvasModule } from "./team-lines-sync.canvas-module";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { PaintSyncBody } from "../../ds/drawing-sync/paint-sync-body";
import { PaintEventState } from "../../ds/drawing-sync/paint-event-state.enum";

@CanvasModule({
  zIndex: 10
})
export class TeamBrushSyncCanvasModule
  implements CanvasModuleInit, CanvasModuleDraw, CanvasModuleDestroy {
  private _subs: Subscription[] = [];
  private _teamBrushHolders = List<TeamBrushHolder>();
  private _localPlayerNickname: string | null = null;
  private _redraw = false;

  constructor(
    private _localPlayer: LocalPlayerInfoService,
    private _drawingSync: DrawingSyncService,
    private _teamLines: TeamLinesSyncCanvasModule
  ) {}

  onInit() {
    let sub = this._localPlayer.localPlayerNickname().subscribe(nickname => {
      this._localPlayerNickname = nickname;
    });
    this._subs.push(sub);

    sub = this._drawingSync.paintEvents().subscribe(event => {
      this._processPaintEvent(event);
    });
    this._subs.push(sub);
  }

  private _processPaintEvent(event: DrawingSyncEvent<PaintSyncBody>) {
    let holder: TeamBrushHolder;
    const matchingHolders = this._teamBrushHolders.filter(
      x => x.playerNickname === event.owner
    );
    if (!matchingHolders.isEmpty()) {
      holder = matchingHolders.get(0);
    }

    if (
      matchingHolders.isEmpty() &&
      event.body.state === PaintEventState.START
    ) {
      holder = new TeamBrushHolder(event.owner);
      this._teamBrushHolders = this._teamBrushHolders.push(holder);
    }

    if (event.body.position) {
      holder.tempBrushPoints.push(event.body.position);
    }

    if (event.body.state === PaintEventState.START) {
      holder.activeBrushConfig = event.body.config;
    } else if (event.body.state === PaintEventState.END) {
      this._teamLines.addTeamBrush(
        holder.playerNickname,
        LinePath.create(List(holder.tempBrushPoints), holder.activeBrushConfig)
      );
      holder.tempBrushPoints.length = 0;
    }
    this._redraw = true;
  }

  onDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  onDraw(canvasContext: CanvasRenderingContext2D) {
    this._teamBrushHolders.forEach(holder => {
      holder.activeBrushConfig.apply(canvasContext);
      BrushDrawingShared.drawTempBrushPoints(
        canvasContext,
        holder.tempBrushPoints
      );
    });
    this._redraw = false;
  }

  get teamBrushHolders() {
    return this._teamBrushHolders;
  }

  get redraw() {
    return this._redraw;
  }
}
