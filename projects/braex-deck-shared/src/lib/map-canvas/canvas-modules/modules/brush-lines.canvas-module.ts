import { CanvasModule } from "../canvas-module.decorator";
import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit
} from "../canvas-module-hooks";
import { LinePath } from "../../ds/line-path";
import { List } from "immutable";
import { Subscription } from "rxjs";
import { BrushDrawingShared } from "./brush-drawing-shared";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { LocalPlayerInfoService } from "../../../services/local-player-info.service";
import { OverwolfCoreService } from "../../../overwolf/overwolf-core.service";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { DrawingSyncEventType } from "../../ds/drawing-sync/drawing-sync-event-type";
import { Vector2 } from "../../ds/vector2";
import { OverwolfGameEventsService } from "../../../overwolf/overwolf-game-events.service";
import { WindowDataExchangeService } from "../../../services/window-data-exchange.service";
import { WindowCommunicator } from "../../../services/window-communicator";
import { ChangeHistoryService } from "../../services/change-history.service";
import { ChangeEventType } from "../../ds/change-event-type.enum";
import { ChangeHistoryEvent } from "../../ds/change-history-event";
import { InGameUtils } from "../../utils/in-game-utils";
import { MapBackgroundCanvasModule } from "./map-background.canvas-module";
import { ChangeHistorySyncBody } from "../../ds/drawing-sync/change-history-sync-body";

@CanvasModule({
  zIndex: 19
})
export class BrushLinesCanvasModule
  implements CanvasModuleInit, CanvasModuleDestroy, CanvasModuleDraw {
  private _redraw = false;
  private _subs: Subscription[] = [];
  private _localPlayerNickname: string | null = null;
  private _linePaths = List<LinePath>();
  private _windowComm: WindowCommunicator;

  constructor(
    private _drawingSync: DrawingSyncService,
    private _localPlayer: LocalPlayerInfoService,
    private _overwolfCore: OverwolfCoreService,
    private _gameEvents: OverwolfGameEventsService,
    private _windowData: WindowDataExchangeService,
    private _changeHistory: ChangeHistoryService,
    private _mapBackground: MapBackgroundCanvasModule
  ) {}

  onInit() {
    this._initAutoClearing();
    this._initPlayerInfo();
    this._initChangeHistoryEvents();
    this._initWindowCommunication();
  }

  private _initAutoClearing() {
    let sub = this._gameEvents.onMatchLeft().subscribe(() => {
      this._linePaths = this._linePaths.clear();
      this._redraw = true;
    });
    this._subs.push(sub);

    sub = this._overwolfCore.onPubgTerminated().subscribe(() => {
      this._linePaths = this._linePaths.clear();
      this._redraw = true;
    });
    this._subs.push(sub);
  }

  private _initPlayerInfo() {
    const sub = this._localPlayer.localPlayerNickname().subscribe(nickname => {
      this._localPlayerNickname = nickname;
    });
    this._subs.push(sub);
  }

  private _initChangeHistoryEvents() {
    let sub = this._changeHistory.undoEvent().subscribe(event => {
      if (event.body instanceof LinePath) {
        if (event.type === ChangeEventType.CREATE) {
          this._linePaths = this._linePaths.delete(
            this._linePaths.indexOf(event.body)
          );
        } else if (event.type === ChangeEventType.REMOVE) {
          this._linePaths = this._linePaths.push(event.body);
        }
      }
      this._redraw = true;
    });
    this._subs.push(sub);

    sub = this._changeHistory.redoEvent().subscribe(event => {
      if (event.body instanceof LinePath) {
        if (event.type === ChangeEventType.CREATE) {
          this._linePaths = this._linePaths.push(event.body);
        } else {
          this._linePaths = this._linePaths.delete(
            this._linePaths.indexOf(event.body)
          );
        }
        this._redraw = true;
      }
    });
    this._subs.push(sub);
  }

  /**
   * Initialize the window communication between the desktop window and the in-game window
   */
  private _initWindowCommunication() {
    this._windowComm = this._windowData.getWindowCommunicator();
    if (!this._windowComm) {
      return;
    }

    let sub = this._windowComm.brushLineAdded().subscribe(line => {
      const de = this._denormalizeLinePath(line);
      const matching = this.linePaths.filter(x => x.equals(de));
      if (!matching.isEmpty()) {
        return;
      }

      this._addLine(de);
    });
    this._subs.push(sub);

    sub = this._windowComm.brushLineRemoved().subscribe(line => {
      const de = this._denormalizeLinePath(line);
      const matching = this.linePaths.filter(x => x.equals(de));
      if (matching.isEmpty()) {
        return;
      }

      this._removeLine(de);
    });
    this._subs.push(sub);

    sub = this._windowComm.brushLinesCleared().subscribe(() => {
      this._linePaths = this._linePaths.clear();
    });
    this._subs.push(sub);
  }

  private _denormalizeLinePath(line: LinePath) {
    return LinePath.create(
      line.points.map(x =>
        InGameUtils.denormalizeVector(x, this._mapBackground.mapSize)
      ),
      line.brushConfig
    );
  }

  addLinePath(linePath: LinePath) {
    this._addLine(linePath);
    if (this._windowComm) {
      this._windowComm.triggerBrushLineAdded(this._normalizeLinePath(linePath));
    }
  }

  private _normalizeLinePath(line: LinePath) {
    return LinePath.create(
      line.points.map(x =>
        InGameUtils.normalizeVector(x, this._mapBackground.mapSize)
      ),
      line.brushConfig
    );
  }

  private _addLine(line: LinePath) {
    this._linePaths = this._linePaths.push(line);
    this._changeHistory.add(
      new ChangeHistoryEvent(ChangeEventType.CREATE, line)
    );
    this._redraw = true;
  }

  removeLinePath(linePath: LinePath) {
    this._removeLine(linePath);
    if (this._windowComm) {
      this._windowComm.triggerBrushLineRemoved(
        this._normalizeLinePath(linePath)
      );
    }
  }

  private _removeLine(line: LinePath) {
    this._linePaths = this._linePaths.remove(this._linePaths.indexOf(line));
    this._changeHistory.add(
      new ChangeHistoryEvent(ChangeEventType.REMOVE, line)
    );
    this._redraw = true;
  }

  closestLinePath(position: Vector2): LinePath | null {
    if (this._linePaths.size <= 0) {
      return null;
    }

    return this._linePaths.reduce((reduction: LinePath, value: LinePath) => {
      return value.distanceToPosition(position) <
        reduction.distanceToPosition(position)
        ? value
        : reduction;
    });
  }

  clear() {
    if (this._linePaths.isEmpty()) {
      return;
    }

    this._linePaths = this._linePaths.clear();
    this._drawingSync.sendEvent(
      new DrawingSyncEvent(
        DrawingSyncEventType.CLEAR_PAINT,
        null,
        this._localPlayerNickname
      )
    );
    if (this._windowComm) {
      this._windowComm.triggerBrushLinesCleared();
    }
    this._redraw = true;
  }

  clearContext() {
    this.clear();
    this._subs.forEach(x => {
      x.unsubscribe();
    });
    this._subs = [];
  }

  onDraw(canvasContext: CanvasRenderingContext2D) {
    BrushDrawingShared.drawLinePaths(canvasContext, this._linePaths);
    this._redraw = false;
  }

  onDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  get redraw() {
    return this._redraw;
  }

  get linePaths() {
    return this._linePaths;
  }

  get canUndo() {
    return this._changeHistory.canUndo;
  }

  get canRedo() {
    return this._changeHistory.canRedo;
  }
}
