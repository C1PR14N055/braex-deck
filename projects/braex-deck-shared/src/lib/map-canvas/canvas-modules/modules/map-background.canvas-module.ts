import { CanvasModuleDraw } from "../canvas-module-hooks";
import { CanvasModule } from "../canvas-module.decorator";
import { ResourceLoadingService } from "../../services/resource-loading.service";
import { PubgMap } from "../../../ds/game/pubg-map.enum";
import { CanvasResource } from "../../ds/canvas-resource";
import { MapImageNamePipe } from "../../pipes/map-image-name.pipe";
import { Vector2 } from "../../ds/vector2";
import { UNSUPPORTED_MAPS } from "../../../../../../braex-deck-desktop/src/app/shared/unsupported-maps";

@CanvasModule({
  zIndex: 1
})
export class MapBackgroundCanvasModule implements CanvasModuleDraw {
  private _redraw = false;
  private _currentMap: PubgMap | null = null;
  private _mapImage: CanvasResource | null = null;
  private _mapSize: Vector2 | null = null;
  private _mapImageName = new MapImageNamePipe();

  constructor(private _resource: ResourceLoadingService) {}

  onDraw(canvasContext: CanvasRenderingContext2D) {
    if (!this._mapImage) {
      return;
    }

    // noinspection JSSuspiciousNameCombination
    this._mapSize = Vector2.create(
      canvasContext.canvas.height,
      canvasContext.canvas.height
    );
    canvasContext.beginPath();
    canvasContext.drawImage(
      this._mapImage.image,
      0,
      0,
      this._mapSize.x,
      this._mapSize.y
    );
    this._redraw = false;
  }

  get redraw() {
    return this._redraw;
  }

  get currentMap() {
    return this._currentMap;
  }

  set currentMap(map: PubgMap) {
    if (UNSUPPORTED_MAPS.includes(map)) {
      throw new Error(`Unsupported map \`${map}\``);
    }

    const changed = this._currentMap !== map;
    this._currentMap = map;

    if (changed) {
      this._mapImage = null;
      this._mapSize = null;
      this._redraw = true;
      if (map) {
        this._resource
          .loadImage(`/assets/img/maps/${this._mapImageName.transform(map)}`)
          .subscribe(
            resource => {
              this._mapImage = resource;
            },
            error => {
              console.error(error);
            }
          );
      }
    }
  }

  get mapSize() {
    return this._mapSize;
  }
}
