import { async, TestBed } from "@angular/core/testing";
import { PlayerMovementCanvasModule } from "./player-movement.canvas-module";
import { MapPlayer } from "../../ds/map-player";
import { ResourceLoadingService } from "../../services/resource-loading.service";
import { PubgMap } from "../../../ds/game/pubg-map.enum";
import { MapBackgroundCanvasModule } from "./map-background.canvas-module";
import { Vector2 } from "../../ds/vector2";

describe("PlayerMovementCanvasModule", () => {
  let module: PlayerMovementCanvasModule;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [PlayerMovementCanvasModule, ResourceLoadingService]
    });
    module = TestBed.get(PlayerMovementCanvasModule);
    module.onInit();
  }));

  it("should create", () => {
    expect(module).toBeTruthy();
  });

  it("should add a player", () => {
    expect(module.players.isEmpty()).toBeTruthy();
    module.addPlayer(MapPlayer.create("Deck"));
    expect(module.players.size).toEqual(1);
  });

  it("should throw when a player with the same nickname is added twice", () => {
    expect(module.players.size).toBeFalsy();
    const player = MapPlayer.create("TestPlayer");
    module.addPlayer(player);

    expect(module.players.size).toBe(1);
    expect(module.players).toContain(player);

    expect(() => {
      module.addPlayer(player);
    }).toThrow();
  });

  it("should throw when updating a player which has not been added before", () => {
    expect(module.players.size).toBeFalsy();
    const player = MapPlayer.create("TestPlayer");

    expect(() => {
      module.updatePlayer(player);
    }).toThrow();
  });

  it("should update a player", () => {
    expect(module.players.size).toBeFalsy();
    let player = MapPlayer.create("TestPlayer", Vector2.create(1124, 231));

    module.addPlayer(player);
    let added = module.players.get(0);
    expect(added.position.x).toBeCloseTo(player.position.x);
    expect(added.position.y).toBeCloseTo(player.position.y);

    player = MapPlayer.create(player.nickname, Vector2.create(139, 1123));
    module.updatePlayer(player);
    added = module.players.get(0);
    expect(added.position.x).toBeCloseTo(player.position.x);
    expect(added.position.y).toBeCloseTo(player.position.y);

    player = MapPlayer.create(
      player.nickname,
      Vector2.create(3455, 6343),
      false
    );
    module.updatePlayer(player);
    added = module.players.get(0);
    expect(added.position.x).toBeCloseTo(player.position.x);
    expect(added.position.y).toBeCloseTo(player.position.y);
    expect(added.alive).toBe(player.alive);
  });

  it("should mark for redraw when adding a new player", () => {
    expect(module.redraw).toBeFalsy();

    module.addPlayer(MapPlayer.create("Some Nickname"));
    expect(module.redraw).toBeTruthy();
  });

  it("should reset the redraw after drawing", () => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    module.addPlayer(MapPlayer.create("Test"));

    expect(module.redraw).toBeTruthy();
    module.onDraw(context);
    expect(module.redraw).toBeFalsy();
  });

  it("should mark for redraw after updating a player", () => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    module.addPlayer(MapPlayer.create("GitHub"));
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
    module.updatePlayer(MapPlayer.create("GitHub", Vector2.create(230, 9340)));
    expect(module.redraw).toBeTruthy();
  });

  it("should remove a player", () => {
    const nickname = "Git";
    module.addPlayer(MapPlayer.create(nickname));
    module.addPlayer(MapPlayer.create("Another player"));

    expect(module.players.size).toEqual(2);
    expect(module.players.filter(x => x.nickname === nickname).size).toEqual(1);

    module.removePlayer(nickname);
    expect(module.players.size).toEqual(1);
    expect(
      module.players.filter(x => x.nickname === nickname).isEmpty()
    ).toBeTruthy();
  });

  it("should throw when removing a player which doesn't exist", () => {
    expect(() => {
      module.removePlayer("APlayerWhichDoesNotExist");
    }).toThrow();
  });

  it("should mark for redraw when removing a player", () => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    const nickname = "PR";
    module.addPlayer(MapPlayer.create(nickname));
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
    module.removePlayer(nickname);
    expect(module.redraw).toBeTruthy();
  });
});
