import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit,
  CanvasModuleUpdate
} from "../canvas-module-hooks";
import { CanvasModule } from "../canvas-module.decorator";
import { MapTool } from "../../core/map-tool.decorator";
import { MapCanvasTool } from "../../ds/map-canvas-tool.enum";
import { faEraser } from "@fortawesome/free-solid-svg-icons";
import { InputControllerService } from "../../services/input-controller.service";
import { ResourceLoadingService } from "../../services/resource-loading.service";
import { CanvasResource } from "../../ds/canvas-resource";
import { Subscription } from "rxjs";
import { MapToolService } from "../../services/map-tool.service";
import { TOOL_MOUSE_BUTTON } from "../../ds/tool-config";
import { BrushLinesCanvasModule } from "./brush-lines.canvas-module";
import { Vector2 } from "../../ds/vector2";
import {
  ERASER_DEFAULT_ICON_COLOR,
  ERASER_ENABLE_ICON_FEEDBACK,
  ERASER_FEEDBACK_FALSE_ICON_COLOR,
  ERASER_FEEDBACK_TRUE_ICON_COLOR,
  ERASER_ICON_MAX_SIZE,
  ERASER_ICON_OFFSET,
  ERASER_ICON_SIZE,
  ERASER_NEAR_LINE_ERROR_MARGIN
} from "../../ds/default-eraser-config";
import { MapMarkerCanvasModule } from "./map-marker.canvas-module";

@CanvasModule({
  zIndex: 1000
})
@MapTool({
  cursor: "none",
  tool: MapCanvasTool.ERASER,
  icon: faEraser
})
export class EraserCanvasModule
  implements
    CanvasModuleInit,
    CanvasModuleUpdate,
    CanvasModuleDraw,
    CanvasModuleDestroy {
  private _subs: Subscription[] = [];
  private _redraw = false;
  private _eraserResource: CanvasResource;
  private _active = false;
  private _position: Vector2 = Vector2.create();
  private _canvasTransformScale = 1;
  private _isNearALine = false;
  private _isNearAMarker = false;

  constructor(
    private _inputControllerService: InputControllerService,
    private _resourceLoadingService: ResourceLoadingService,
    private _mapToolService: MapToolService,
    private _brushLinesCanvasModule: BrushLinesCanvasModule,
    private _mapMarkerCanvasModule: MapMarkerCanvasModule
  ) {}

  onInit(): void {
    this._active = this._mapToolService.activeTool === MapCanvasTool.ERASER;
    const toolSub = this._mapToolService.activeToolChanged().subscribe(tool => {
      this._redraw = this._active || tool === MapCanvasTool.ERASER;
      this._active = tool === MapCanvasTool.ERASER;
    });
    this._subs.push(toolSub);

    this._resourceLoadingService
      .loadImage("/assets/img/map-tool-icons/times.svg")
      .subscribe(resource => {
        this._eraserResource = resource;
      });

    const inputSub = this._inputControllerService.mouseUp().subscribe(event => {
      if (!this._active) {
        return;
      }

      if (event.mouseButton === TOOL_MOUSE_BUTTON) {
        // prioritize markers as they have a higher index
        if (this._isNearAMarker) {
          this._deleteNearestMarker();
        } else if (this._isNearALine) {
          this._deleteNearestLine();
        }
      }
    });
    this._subs.push(inputSub);
  }

  onUpdate(mousePosition: Vector2): void {
    if (!this._active) {
      return;
    }
    this._redraw = !this._position.equals(mousePosition);
    this._position = mousePosition;

    // compute only once per update
    this._isNearALine = this._checkIfNearALine();
    this._isNearAMarker = this._checkIfNearAMarker();
  }

  onDraw(canvasContext: CanvasRenderingContext2D): void {
    if (!this._active) {
      return;
    }

    this._canvasTransformScale = canvasContext.getTransform().a;
    const size = this._calculateSize();
    const position = this._calculatePosition();

    canvasContext.drawImage(
      this._eraserResource.image,
      position.x,
      position.y,
      size.x,
      size.y
    );
    canvasContext.globalCompositeOperation = "source-in";
    if (ERASER_ENABLE_ICON_FEEDBACK) {
      canvasContext.fillStyle =
        this._isNearALine || this._isNearAMarker
          ? ERASER_FEEDBACK_TRUE_ICON_COLOR
          : ERASER_FEEDBACK_FALSE_ICON_COLOR;
    } else {
      canvasContext.fillStyle = ERASER_DEFAULT_ICON_COLOR;
    }
    canvasContext.fillRect(position.x, position.y, size.x, size.y);
    canvasContext.globalCompositeOperation = "source-over";
    this._redraw = false;
  }

  private _calculateSize() {
    let size = Vector2.create(
      ERASER_ICON_SIZE.x / this._canvasTransformScale,
      ERASER_ICON_SIZE.y / this._canvasTransformScale
    );
    if (size.x > ERASER_ICON_MAX_SIZE.x) {
      size = Vector2.create(ERASER_ICON_MAX_SIZE.x, size.y);
    }
    if (size.y > ERASER_ICON_MAX_SIZE.y) {
      size = Vector2.create(size.x, ERASER_ICON_MAX_SIZE.y);
    }

    return size;
  }

  private _calculatePosition() {
    return this._position.subtract(
      ERASER_ICON_OFFSET.divide(this._canvasTransformScale)
    );
  }

  private _checkIfNearALine() {
    const closestLinePath = this._brushLinesCanvasModule.closestLinePath(
      this._position
    );
    if (!closestLinePath) {
      return false;
    }

    return (
      closestLinePath &&
      closestLinePath.distanceToPosition(this._position) <=
        ERASER_NEAR_LINE_ERROR_MARGIN / this._canvasTransformScale
    );
  }

  private _checkIfNearAMarker() {
    const closestMarker = this._mapMarkerCanvasModule.closestMarker(
      this._position
    );
    if (!closestMarker) {
      return false;
    }

    return (
      closestMarker &&
      closestMarker.position.distance(this._position) <=
        ERASER_NEAR_LINE_ERROR_MARGIN / this._canvasTransformScale
    );
  }

  private _deleteNearestLine() {
    if (this._isNearALine) {
      this._brushLinesCanvasModule.removeLinePath(
        this._brushLinesCanvasModule.closestLinePath(this._position)
      );
    }
  }

  private _deleteNearestMarker() {
    if (this._isNearAMarker) {
      this._mapMarkerCanvasModule.removeMarker(
        this._mapMarkerCanvasModule.closestMarker(this._position)
      );
    }
  }

  onDestroy() {
    this._subs.forEach(s => {
      s.unsubscribe();
    });
  }

  get active() {
    return this._active;
  }

  get redraw() {
    return this._redraw;
  }

  get isNearALine() {
    return this._isNearALine;
  }

  get isNearAMarker() {
    return this._isNearAMarker;
  }
}
