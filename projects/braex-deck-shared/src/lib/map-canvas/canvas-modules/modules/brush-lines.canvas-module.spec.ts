import { BrushLinesCanvasModule } from "./brush-lines.canvas-module";
import { fakeAsync, TestBed, tick } from "@angular/core/testing";
import { LinePath } from "../../ds/line-path";
import { List } from "immutable";
import { Vector2 } from "../../ds/vector2";
import { Subscription } from "rxjs";
import { BrushChangeHistoryCanvasModule } from "./brush-change-history.canvas-module";
import { FakeDrawingSyncService } from "../../services/testing/fake-drawing-sync.service";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { LocalPlayerInfoService } from "../../../services/local-player-info.service";
import { FakeLocalPlayerInfoService } from "../../../services/testing/fake-local-player-info.service";
import { OverwolfCoreService } from "../../../overwolf/overwolf-core.service";
import { FakeOverwolfCoreService } from "../../../overwolf/testing/fake-overwolf-core.service";
import { HotkeyActions } from "../../../ds/hotkey-actions.enum";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { ChangeHistorySyncBody } from "../../ds/drawing-sync/change-history-sync-body";
import { GamePhase } from "../../../ds/game/game-phase.enum";
import { FakeOverwolfGameEventsService } from "../../../overwolf/testing/fake-overwolf-game-events.service";
import { OverwolfGameEventsService } from "../../../overwolf/overwolf-game-events.service";

describe("BrushLinesCanvasModule", () => {
  let module: BrushLinesCanvasModule;
  let subs: Subscription[];
  let fakeDrawSync: FakeDrawingSyncService;
  let fakeLocalPlayer: FakeLocalPlayerInfoService;
  let localPlayerNickname: string;
  let fakeOverwolfCore: FakeOverwolfCoreService;
  let fakeGameEvents: FakeOverwolfGameEventsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BrushLinesCanvasModule,
        BrushChangeHistoryCanvasModule,
        { provide: DrawingSyncService, useClass: FakeDrawingSyncService },
        {
          provide: LocalPlayerInfoService,
          useClass: FakeLocalPlayerInfoService
        },
        { provide: OverwolfCoreService, useClass: FakeOverwolfCoreService },
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        }
      ]
    });

    localPlayerNickname = "CGS";
    module = TestBed.get(BrushLinesCanvasModule);
    fakeDrawSync = TestBed.get(DrawingSyncService);
    fakeLocalPlayer = TestBed.get(LocalPlayerInfoService);
    fakeOverwolfCore = TestBed.get(OverwolfCoreService);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);

    module.onInit();
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayerNickname);
    subs = [];
  });

  it("should be created", () => {
    expect(module).toBeTruthy();
  });

  it("should add a line path", () => {
    expect(module.linePaths.isEmpty()).toBeTruthy();

    const linePath = LinePath.create(
      List<Vector2>([
        Vector2.create(19, 42),
        Vector2.create(36, 99),
        Vector2.create(124, 393)
      ])
    );
    module.addLinePath(linePath);

    expect(module.linePaths.size).toBe(1);
    expect(module.linePaths).toContain(linePath);
  });

  it("should redraw when a new line path has been added", () => {
    expect(module.redraw).toBeFalsy();

    const linePath = LinePath.create(
      List<Vector2>([Vector2.create(123, 634), Vector2.create(90, 12)])
    );
    module.addLinePath(linePath);
    expect(module.redraw).toBeTruthy();
  });

  it("should not redraw when the line paths stay the same", () => {
    expect(module.redraw).toBeFalsy();

    const linePath = LinePath.create(
      List<Vector2>([Vector2.create(324, 352), Vector2.create(8, 23)])
    );
    module.addLinePath(linePath);
    expect(module.redraw).toBeTruthy();

    const canvas = document.createElement("canvas");
    canvas.width = 300;
    canvas.height = 400;
    const context = canvas.getContext("2d");

    module.onDraw(context);
    expect(module.redraw).toBeFalsy();
  });

  it("should clear the brush lines when calling #clear", () => {
    module.addLinePath(
      LinePath.create(
        List<Vector2>([
          Vector2.create(49, 72),
          Vector2.create(15, 16),
          Vector2.create(1563, 486)
        ])
      )
    );

    expect(module.linePaths.size).toBeTruthy();
    module.clear();
    expect(module.linePaths.isEmpty()).toBeTruthy();
  });

  it("should clear the change history when clearing the brush lines", () => {
    module.addLinePath(
      LinePath.create(
        List<Vector2>([
          Vector2.create(94, 35),
          Vector2.create(78, 31),
          Vector2.create(59, 452)
        ])
      )
    );

    expect(module.linePaths.size).toBeTruthy();
    module.clear();
    expect(module.linePaths.isEmpty()).toBeTruthy();

    module.undo();
    expect(module.linePaths.isEmpty()).toBeTruthy();

    module.redo();
    expect(module.linePaths.isEmpty()).toBeTruthy();
  });

  it("should undo the last line when calling #undo", () => {
    module.addLinePath(
      LinePath.create(
        List<Vector2>([
          Vector2.create(500, 500),
          Vector2.create(510, 500),
          Vector2.create(520, 500),
          Vector2.create(530, 500)
        ])
      )
    );

    expect(module.linePaths.size).toBe(1);
    module.undo();
    expect(module.linePaths.isEmpty()).toBeTruthy();
  });

  it("should redo the last line when calling #redo", () => {
    const linePath = LinePath.create(
      List<Vector2>([
        Vector2.create(500, 500),
        Vector2.create(510, 500),
        Vector2.create(520, 500),
        Vector2.create(530, 500)
      ])
    );
    module.addLinePath(linePath);

    expect(module.linePaths.size).toBe(1);
    module.undo();
    expect(module.linePaths.isEmpty()).toBeTruthy();

    module.redo();
    expect(module.linePaths.size).toBe(1);
    expect(module.linePaths).toContain(linePath);
  });

  it("should undo the last two lines", () => {
    module.addLinePath(
      LinePath.create(
        List<Vector2>([
          Vector2.create(300, 200),
          Vector2.create(300, 210),
          Vector2.create(300, 220),
          Vector2.create(290, 230)
        ])
      )
    );
    module.addLinePath(
      LinePath.create(
        List<Vector2>([
          Vector2.create(100, 0),
          Vector2.create(110, 10),
          Vector2.create(120, 20),
          Vector2.create(130, 30),
          Vector2.create(140, 40)
        ])
      )
    );

    expect(module.linePaths.size).toBe(2);
    const linePath1 = module.linePaths.get(0);

    module.undo();
    expect(module.linePaths.size).toBe(1);
    expect(module.linePaths).toContain(linePath1);

    module.undo();
    expect(module.linePaths.isEmpty()).toBeTruthy();
  });

  it("should mark to redraw when undoing a brush", () => {
    module.addLinePath(
      LinePath.create(
        List([
          Vector2.create(12, 16),
          Vector2.create(90, 14),
          Vector2.create(74, 32)
        ])
      )
    );
    module.addLinePath(
      LinePath.create(
        List([
          Vector2.create(73, 345),
          Vector2.create(42, 78),
          Vector2.create(76, 7)
        ])
      )
    );
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    module.onDraw(context);
    expect(module.redraw).toBeFalsy();

    module.undo();
    expect(module.redraw).toBeTruthy();
  });

  it("should mark to redraw when redoing a brush", () => {
    module.addLinePath(
      LinePath.create(
        List([
          Vector2.create(73, 345),
          Vector2.create(42, 78),
          Vector2.create(76, 7)
        ])
      )
    );
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    module.onDraw(context);

    module.undo();
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
    module.redo();
    expect(module.redraw).toBeTruthy();
  });

  it("should mark to redraw when clearing the brushes", () => {
    module.addLinePath(
      LinePath.create(
        List([
          Vector2.create(73, 345),
          Vector2.create(42, 78),
          Vector2.create(76, 7)
        ])
      )
    );
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
    module.clear();
    expect(module.redraw).toBeTruthy();
  });

  it("should sync a clear event when calling #clear", fakeAsync(() => {
    const spy = spyOn(fakeDrawSync, "sendEvent");
    module.addLinePath(
      LinePath.create(
        List<Vector2>([
          Vector2.create(12, 32),
          Vector2.create(69, 59),
          Vector2.create(420, 1337)
        ])
      )
    );
    module.clear();
    tick(100);
    expect(spy).toHaveBeenCalled();
  }));

  it("should not sync a clear when there is nothing to clear", fakeAsync(() => {
    const spy = spyOn(fakeDrawSync, "sendEvent");
    module.clear();
    tick(200);

    expect(spy).not.toHaveBeenCalled();
  }));

  it("should sync an undo event when calling #undo", fakeAsync(() => {
    let received: LinePath;
    spyOn(fakeDrawSync, "sendEvent").and.callFake(
      (event: DrawingSyncEvent<ChangeHistorySyncBody>) => {
        received = event.body.linePath;
      }
    );
    const line = LinePath.create(
      List<Vector2>([Vector2.create(12, 53), Vector2.create(5, 0)])
    );
    module.addLinePath(line);

    module.undo();
    tick(200);
    expect(received.equals(line)).toBeTruthy();
  }));

  it("should not sync an undo event when there is nothing to undo", fakeAsync(() => {
    const spy = spyOn(fakeDrawSync, "sendEvent");
    module.undo();
    tick(200);
    expect(spy).not.toHaveBeenCalled();
  }));

  it("should sync a redo event when calling #redo", fakeAsync(() => {
    const line1 = LinePath.create(
      List<Vector2>([
        Vector2.create(300, 200),
        Vector2.create(300, 210),
        Vector2.create(300, 220),
        Vector2.create(290, 230)
      ])
    );
    module.addLinePath(line1);
    const line2 = LinePath.create(
      List<Vector2>([
        Vector2.create(100, 0),
        Vector2.create(110, 10),
        Vector2.create(120, 20),
        Vector2.create(130, 30),
        Vector2.create(140, 40)
      ])
    );
    module.addLinePath(line2);
    module.undo();
    tick(200);

    let received: LinePath;
    spyOn(fakeDrawSync, "sendEvent").and.callFake(
      (event: DrawingSyncEvent<ChangeHistorySyncBody>) => {
        received = event.body.linePath;
      }
    );
    module.redo();
    tick(200);

    expect(received.equals(line2)).toBeTruthy();
  }));

  it("should not sync a redo event when there is nothing to redo", fakeAsync(() => {
    const spy = spyOn(fakeDrawSync, "sendEvent");
    module.redo();
    tick(200);

    expect(spy).not.toHaveBeenCalled();
  }));

  it("should clear the lines when the phase changes to LOBBY and it has been different before", fakeAsync(() => {
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(20);

    module.addLinePath(
      LinePath.create(
        List([
          Vector2.create(10, 123),
          Vector2.create(13, 1),
          Vector2.create(9, 111)
        ])
      )
    );
    expect(module.linePaths.isEmpty()).toBeFalsy();

    fakeGameEvents.triggerPhaseChanged(GamePhase.LOBBY);
    tick(20);
    expect(module.linePaths.isEmpty()).toBeTruthy();
  }));

  it("should mark for redraw when the phase changes to LOBBY and it has been different before", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(20);

    module.addLinePath(LinePath.create(List([Vector2.create(1337, 1337)])));
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
    fakeGameEvents.triggerPhaseChanged(GamePhase.LOBBY);
    tick(20);
    expect(module.redraw).toBeTruthy();
  }));

  it("should NOT clear the lines when randomly receiving the phase LOBBY", fakeAsync(() => {
    module.addLinePath(
      LinePath.create(
        List([Vector2.create(15, 345), Vector2.create(1234, 142)])
      )
    );
    expect(module.linePaths.isEmpty()).toBeFalsy();

    fakeGameEvents.triggerPhaseChanged(GamePhase.LOBBY);
    tick(20);
    expect(module.linePaths.isEmpty()).toBeFalsy();
  }));

  afterEach(() => {
    module.onDestroy();
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
