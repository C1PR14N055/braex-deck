import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit
} from "../canvas-module-hooks";
import { CanvasModule } from "../canvas-module.decorator";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { List, Map as ImmMap } from "immutable";
import { MapMarker } from "../../ds/map-marker";
import { Subscription } from "rxjs";
import { MarkerDrawingShared } from "./marker-drawing-shared";
import { ResourceLoadingService } from "../../services/resource-loading.service";
import { MapMarkerType } from "../../ds/map-marker-type.enum";
import { CanvasResource } from "../../ds/canvas-resource";
import { MapMarkerIconPipe } from "../../../map-marker-icon/map-marker-icon.pipe";
import { OverwolfGameEventsService } from "../../../overwolf/overwolf-game-events.service";
import { ChangeActionType } from "../../ds/drawing-sync/change-action-type.enum";
import { ChangeEventType } from "../../ds/change-event-type.enum";

@CanvasModule({
  zIndex: 199
})
export class TeamMarkerSyncCanvasModule
  implements CanvasModuleInit, CanvasModuleDestroy, CanvasModuleDraw {
  private _redraw = false;
  private _subs: Subscription[] = [];
  private _teamMarkers = ImmMap<string, List<MapMarker>>();
  private _markerResourceMapping = new Map<MapMarkerType, CanvasResource>();

  constructor(
    private _drawingSync: DrawingSyncService,
    private _resourceLoadingService: ResourceLoadingService,
    private _gameEvents: OverwolfGameEventsService
  ) {}

  onInit() {
    this._initResources();
    this._initSyncEvents();
    this._initTeamMarkerEvents();
    this._initAutoClearing();
  }

  private _initResources() {
    const pipe = new MapMarkerIconPipe();
    Object.keys(MapMarkerType).forEach(key => {
      const sub = this._resourceLoadingService
        .loadImage(pipe.transform(MapMarkerType[key]))
        .subscribe(resource => {
          this._markerResourceMapping.set(MapMarkerType[key], resource);
        });
      this._subs.push(sub);
    });
  }

  private _initSyncEvents() {
    let sub = this._drawingSync.undoEvents().subscribe(event => {
      if (event.body.type !== ChangeActionType.MARKER) {
        return;
      }

      if (this._teamMarkers.has(event.owner)) {
        let markers = this._teamMarkers.get(event.owner);
        if (event.body.event.type === ChangeEventType.CREATE) {
          const matching = markers.filter(x => x.equals(event.body.event.body));
          if (!matching.isEmpty()) {
            markers = markers.delete(markers.indexOf(matching.get(0)));
          }
        } else if (event.body.event.type === ChangeEventType.REMOVE) {
          markers = markers.push(event.body.event.body);
        }

        this._teamMarkers = this._teamMarkers.set(event.owner, markers);
        this._redraw = true;
      }
    });
    this._subs.push(sub);

    sub = this._drawingSync.redoEvents().subscribe(event => {
      if (event.body.type !== ChangeActionType.MARKER) {
        return;
      }

      if (this._teamMarkers.has(event.owner)) {
        let markers = this._teamMarkers.get(event.owner);
        if (event.body.event.type === ChangeEventType.CREATE) {
          markers = markers.push(event.body.event.body);
        } else if (event.body.event.type === ChangeEventType.REMOVE) {
          const matching = markers.filter(x => x.equals(event.body.event.body));
          if (!matching.isEmpty()) {
            markers = markers.delete(markers.indexOf(matching.get(0)));
          }
        }

        this._teamMarkers = this._teamMarkers.set(event.owner, markers);
        this._redraw = true;
      }
    });
    this._subs.push(sub);
  }

  private _initTeamMarkerEvents() {
    const sub = this._drawingSync.markerEvents().subscribe(event => {
      const marker = MapMarker.create(event.body.position, event.body.config);
      this._addTeamMarker(event.owner, marker);
    });
    this._subs.push(sub);
  }

  private _initAutoClearing() {
    const sub = this._gameEvents.onMatchLeft().subscribe(() => {
      this._teamMarkers = this._teamMarkers.clear();
      this._redraw = true;
    });
    this._subs.push(sub);
  }

  private _addTeamMarker(nickname: string, marker: MapMarker) {
    if (this._teamMarkers.has(nickname)) {
      let markers = this._teamMarkers.get(nickname);
      markers = markers.push(marker);
      this._teamMarkers = this._teamMarkers.set(nickname, markers);
      this._redraw = true;

      return;
    }

    this._teamMarkers = this._teamMarkers.set(
      nickname,
      List<MapMarker>([marker])
    );
    this._redraw = true;
  }

  onDraw(canvasContext: CanvasRenderingContext2D) {
    this._teamMarkers.forEach(markers => {
      MarkerDrawingShared.drawMarkers(
        canvasContext,
        markers,
        this._markerResourceMapping
      );
    });
    this._redraw = false;
  }

  onDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  get redraw() {
    return this._redraw;
  }

  /**
   * Map with the markers of all team mates.
   * Key: nickname of the team mate
   * Value: List of the {@link MapMarker} of the team mate.
   */
  get teamMarkers() {
    return this._teamMarkers;
  }
}
