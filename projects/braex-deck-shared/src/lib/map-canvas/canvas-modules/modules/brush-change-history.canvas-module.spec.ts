import {
  BrushChangeHistoryCanvasModule,
  MAX_CHANGE_HISTORY_SIZE
} from "./brush-change-history.canvas-module";
import { LinePath } from "../../ds/line-path";
import { List } from "immutable";
import { Vector2 } from "../../ds/vector2";
import { async, TestBed } from "@angular/core/testing";
import { BrushCanvasModule } from "./brush.canvas-module";

describe("BrushChangeHistoryCanvasModule", () => {
  let changeHistory: BrushChangeHistoryCanvasModule;
  let linePath1: LinePath;
  let linePath2: LinePath;
  let linePath3: LinePath;
  let linePath4: LinePath;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [BrushChangeHistoryCanvasModule, BrushCanvasModule]
    });
    changeHistory = TestBed.get(BrushChangeHistoryCanvasModule);
    linePath1 = LinePath.create(
      List([Vector2.create(10, 5), Vector2.create(20, 9), Vector2.create(1, 2)])
    );
    linePath2 = LinePath.create(
      List([Vector2.create(), Vector2.create(10, 10)])
    );
    linePath3 = LinePath.create(
      List([
        Vector2.create(90, 42),
        Vector2.create(100, 42),
        Vector2.create(110, 42)
      ])
    );
    linePath4 = LinePath.create(
      List([
        Vector2.create(56, 165),
        Vector2.create(1564, 452),
        Vector2.create(56, 18)
      ])
    );
  }));

  it("should create", () => {
    expect(changeHistory).toBeTruthy();
  });

  it("should add a change to the history when calling #add", () => {
    changeHistory.add(linePath1);

    expect(changeHistory.history.size).toBe(1);
    expect(changeHistory.history).toContain(linePath1);
  });

  it("should remove the oldest change when the max history size is reached", () => {
    for (let i = 0; i < MAX_CHANGE_HISTORY_SIZE; i++) {
      const linePath = LinePath.create(
        List([Vector2.create(10, 10), Vector2.create(20, 20)])
      );
      changeHistory.add(linePath);
    }

    expect(changeHistory.history.size).toBe(MAX_CHANGE_HISTORY_SIZE);
    const first = changeHistory.history.get(0);

    changeHistory.add(LinePath.create(List([Vector2.create()])));
    expect(changeHistory.history.size).toBe(MAX_CHANGE_HISTORY_SIZE);
    expect(changeHistory.history).not.toContain(first);
  });

  it("should emit null when there are no changes to undo when calling #undo", () => {
    expect(changeHistory.history.size).toBeFalsy();
    expect(changeHistory.undo()).toBeNull();
  });

  it("should emit null when there are no changes to redo when calling #redo", () => {
    expect(changeHistory.history.size).toBeFalsy();
    expect(changeHistory.redo()).toBeNull();
  });

  it("should emit the latest change when calling #undo", () => {
    changeHistory.add(linePath1);
    changeHistory.add(linePath2);

    expect(changeHistory.undo()).toBe(linePath2);
  });

  it("should emit the changes in reversed order when calling #undo", () => {
    addTwoAndUndoBoth();
  });

  function addTwoAndUndoBoth() {
    changeHistory.add(linePath1);
    changeHistory.add(linePath2);

    expect(changeHistory.undo()).toBe(linePath2);
    expect(changeHistory.undo()).toBe(linePath1);
  }

  it("should emit the changes in the order they occurred when calling #redo", () => {
    addTwoAndUndoBoth();
    expect(changeHistory.redo()).toBe(linePath2);
    expect(changeHistory.redo()).toBe(linePath1);
  });

  it("should emit null when no more changes can be undone when calling #undo", () => {
    changeHistory.add(linePath1);
    changeHistory.add(linePath2);

    changeHistory.undo();
    changeHistory.undo();
    expect(changeHistory.undo()).toBeNull();
  });

  it("should emit the last change which was undone when calling #redo", () => {
    changeHistory.add(linePath1);
    changeHistory.add(linePath2);

    expect(changeHistory.undo()).toBe(linePath2);
    expect(changeHistory.redo()).toBe(linePath2);
  });

  it("should emit null when no more changes can be redone when calling #redo", () => {
    changeHistory.add(linePath1);
    changeHistory.add(linePath2);

    changeHistory.undo();
    changeHistory.redo();
    expect(changeHistory.redo()).toBeNull();
  });

  it("should clear the history when calling #clear", () => {
    changeHistory.add(linePath1);
    changeHistory.add(linePath2);
    expect(changeHistory.history).toContain(linePath1);
    expect(changeHistory.history).toContain(linePath2);

    changeHistory.clear();
    expect(changeHistory.history.size).toBeFalsy();
  });

  it("should reverse the history after undoing all changes", () => {
    changeHistory.add(linePath1);
    changeHistory.add(linePath2);
    changeHistory.add(linePath3);
    expect(changeHistory.history.size).toBe(3);
    expect(changeHistory.history.indexOf(linePath1)).toBe(0);

    changeHistory.undo();
    changeHistory.undo();
    changeHistory.undo();
    expect(changeHistory.history.indexOf(linePath1)).toBe(2);
    expect(changeHistory.redo()).toBe(linePath3);
  });

  it("should indicate that it's possible to undo", () => {
    expect(changeHistory.canUndo).toBeFalsy();
    changeHistory.add(linePath1);
    expect(changeHistory.canUndo).toBeTruthy();
  });

  it("should indicate that it's possible to redo", () => {
    changeHistory.add(linePath1);
    expect(changeHistory.canRedo).toBeFalsy();
    changeHistory.undo();
    expect(changeHistory.canRedo).toBeTruthy();
  });

  it("should indicate that it's NOT possible to undo after all undo's", () => {
    expect(changeHistory.canUndo).toBeFalsy();
    changeHistory.add(linePath1);
    expect(changeHistory.canUndo).toBeTruthy();
    changeHistory.undo();
    expect(changeHistory.canUndo).toBeFalsy();
  });

  it("should indicate that it's NOT possible to redo after all redo's", () => {
    changeHistory.add(linePath1);
    expect(changeHistory.canRedo).toBeFalsy();
    changeHistory.undo();
    expect(changeHistory.canRedo).toBeTruthy();
    changeHistory.redo();
    expect(changeHistory.canRedo).toBeFalsy();
  });
});
