import { async, TestBed } from "@angular/core/testing";
import { MapBackgroundCanvasModule } from "./map-background.canvas-module";
import { OverwolfGameEventsService } from "../../../overwolf/overwolf-game-events.service";
import { FakeOverwolfGameEventsService } from "../../../overwolf/testing/fake-overwolf-game-events.service";
import { PubgMap } from "../../../ds/game/pubg-map.enum";
import { ResourceLoadingService } from "../../services/resource-loading.service";
import { MapImageNamePipe } from "../../pipes/map-image-name.pipe";

describe("MapBackgroundCanvasModule", () => {
  let module: MapBackgroundCanvasModule;
  let fakeGameEvents: FakeOverwolfGameEventsService;
  let resourceService: ResourceLoadingService;

  beforeEach(done => {
    TestBed.configureTestingModule({
      providers: [
        MapBackgroundCanvasModule,
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        },
        ResourceLoadingService
      ]
    });

    module = TestBed.get(MapBackgroundCanvasModule);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    resourceService = TestBed.get(ResourceLoadingService);

    const mapImageName = new MapImageNamePipe();
    resourceService
      .loadImage(
        `/assets/img/maps/${mapImageName.transform(PubgMap.ERANGEL_MAIN)}`
      )
      .subscribe(() => {
        done();
      });
  });

  it("should be created", () => {
    expect(module).toBeTruthy();
  });

  it("should throw when setting a map which is not supported", () => {
    expect(() => {
      module.currentMap = PubgMap.RANGE_MAIN;
    }).toThrow();
  });

  it("should mark for redraw when changing the map", () => {
    module.currentMap = PubgMap.DIHOROTOK_MAIN;
    expect(module.redraw).toBeTruthy();
  });

  it("should reset redraw after the new map has been drawn", () => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    module.currentMap = PubgMap.ERANGEL_MAIN;
    expect(module.redraw).toBeTruthy();
    module.onDraw(context);
    expect(module.redraw).toBeFalsy();
  });

  it("should reset when setting the map to null", () => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    module.currentMap = PubgMap.ERANGEL_MAIN;
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
    module.currentMap = null;
    expect(module.mapSize).toBeNull();
    expect(module.redraw).toBeTruthy();
  });
});
