import { MapToolService } from "../../services/map-tool.service";
import { Vector2 } from "../../ds/vector2";
import { FakeDrawingSyncService } from "../../services/testing/fake-drawing-sync.service";
import { FakeLocalPlayerInfoService } from "../../../services/testing/fake-local-player-info.service";
import { FakeInputControllerService } from "../../services/testing/fake-input-controller.service";
import { BrushLinesCanvasModule } from "./brush-lines.canvas-module";
import { async, fakeAsync, TestBed, tick } from "@angular/core/testing";
import { BrushConfigService } from "../../services/brush-config.service";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { LocalPlayerInfoService } from "../../../services/local-player-info.service";
import { InputControllerService } from "../../services/input-controller.service";
import { EraserCanvasModule } from "./eraser.canvas-module";
import { MapCanvasTool } from "../../ds/map-canvas-tool.enum";
import { LinePath } from "../../ds/line-path";
import { List } from "immutable";
import { ERASER_NEAR_LINE_ERROR_MARGIN } from "../../ds/default-eraser-config";
import { BrushConfig } from "../../ds/brush-config";
import { TOOL_MOUSE_BUTTON } from "../../ds/tool-config";
import { MapMarkerCanvasModule } from "./map-marker.canvas-module";
import { ResourceLoadingService } from "../../services/resource-loading.service";

describe("EraserCanvasModule", () => {
  let fakeInputController: FakeInputControllerService;
  let eraserModule: EraserCanvasModule;
  let mapToolService: MapToolService;
  let brushLines: BrushLinesCanvasModule;
  let mapMarkerCanvasModule: MapMarkerCanvasModule;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        EraserCanvasModule,
        BrushConfigService,
        MapToolService,
        { provide: DrawingSyncService, useClass: FakeDrawingSyncService },
        {
          provide: LocalPlayerInfoService,
          useClass: FakeLocalPlayerInfoService
        },
        {
          provide: InputControllerService,
          useClass: FakeInputControllerService
        },
        BrushLinesCanvasModule,
        MapMarkerCanvasModule,
        ResourceLoadingService
      ]
    }).compileComponents();

    fakeInputController = TestBed.get(InputControllerService);
    eraserModule = TestBed.get(EraserCanvasModule);
    mapToolService = TestBed.get(MapToolService);
    brushLines = TestBed.get(BrushLinesCanvasModule);
    mapMarkerCanvasModule = TestBed.get(MapMarkerCanvasModule);
    eraserModule.onInit();
  }));

  it("should create", () => {
    expect(eraserModule).toBeTruthy();
  });

  it("should activate when the currently active tool is the ERASER", () => {
    expect(eraserModule.active).toBeFalsy();
    mapToolService.activeTool = MapCanvasTool.ERASER;
    expect(eraserModule.active).toBeTruthy();
  });

  it("should not activate when the currently active tool is not the ERASE", () => {
    mapToolService.activeTool = MapCanvasTool.BRUSH;
    expect(eraserModule.active).toBeFalsy();
  });

  it("should NOT be near a line", fakeAsync(() => {
    const position = Vector2.create(20, 50);
    const line = LinePath.create(
      List<Vector2>([Vector2.create(200, 234), Vector2.create(123, 87)]),
      new BrushConfig()
    );
    brushLines.addLinePath(line);
    mapToolService.activeTool = MapCanvasTool.ERASER;
    eraserModule.onUpdate(position);
    tick(200);
    expect(eraserModule.isNearALine).toBeFalsy();
  }));

  it("should BE near a dot", fakeAsync(() => {
    const position = Vector2.create(200, 200);
    const line = LinePath.create(
      List<Vector2>([Vector2.create(position.x, position.y)]),
      new BrushConfig()
    );
    brushLines.addLinePath(line);
    mapToolService.activeTool = MapCanvasTool.ERASER;
    eraserModule.onUpdate(position);
    tick(200);
    expect(eraserModule.isNearALine).toBeTruthy();
  }));

  it("should BE near a line", fakeAsync(() => {
    const position = Vector2.create(200, 200);
    const line = LinePath.create(
      List<Vector2>([
        Vector2.create(position.x - ERASER_NEAR_LINE_ERROR_MARGIN, position.y),
        Vector2.create(position.x + ERASER_NEAR_LINE_ERROR_MARGIN, position.y)
      ]),
      new BrushConfig()
    );
    brushLines.addLinePath(line);
    mapToolService.activeTool = MapCanvasTool.ERASER;
    eraserModule.onUpdate(position);
    tick(200);
    expect(eraserModule.isNearALine).toBeTruthy();
  }));

  it("should DELETE the nearest line", fakeAsync(() => {
    const position = Vector2.create(200, 200);
    const lines = [
      LinePath.create(
        List<Vector2>([
          Vector2.create(
            position.x - ERASER_NEAR_LINE_ERROR_MARGIN,
            position.y
          ),
          Vector2.create(position.x + ERASER_NEAR_LINE_ERROR_MARGIN, position.y)
        ]),
        new BrushConfig()
      ),
      LinePath.create(
        List<Vector2>([
          Vector2.create(position.x + 200, position.y + 200),
          Vector2.create(position.x + 300, position.y + 300)
        ]),
        new BrushConfig()
      )
    ];
    lines.forEach(line => brushLines.addLinePath(line));
    mapToolService.activeTool = MapCanvasTool.ERASER;
    eraserModule.onUpdate(position);
    fakeInputController.triggerMouseUp({
      position: position,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);
    expect(brushLines.linePaths.size).toEqual(1);
  }));

  it("should NOT BE near a marker", () => {
    const position = Vector2.create(420, 420); // ahemm
    const mapMarkerPos = Vector2.create(69, 69);
    mapMarkerCanvasModule.addMarker(mapMarkerPos);
    mapToolService.activeTool = MapCanvasTool.ERASER;
    eraserModule.onUpdate(position);
    expect(eraserModule.isNearAMarker).toBeFalsy();
  });

  it("should BE near a marker", () => {
    const position = Vector2.create(123, 210);
    const mapMarkerPos = Vector2.create(
      position.x - ERASER_NEAR_LINE_ERROR_MARGIN / 2,
      position.y
    );
    mapMarkerCanvasModule.addMarker(mapMarkerPos);
    mapToolService.activeTool = MapCanvasTool.ERASER;
    eraserModule.onUpdate(position);
    expect(eraserModule.isNearAMarker).toBeTruthy();
  });

  it("should DELETE a marker", fakeAsync(() => {
    const position = Vector2.create(123, 210);
    const mapMarkersPos = [
      Vector2.create(
        position.x - ERASER_NEAR_LINE_ERROR_MARGIN / 2,
        position.y
      ),
      Vector2.create(304, 742)
    ];
    mapMarkersPos.forEach(mp => mapMarkerCanvasModule.addMarker(mp));
    mapToolService.activeTool = MapCanvasTool.ERASER;
    eraserModule.onUpdate(position);
    fakeInputController.triggerMouseUp({
      position: position,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);
    expect(mapMarkerCanvasModule.placedMarkers.size).toEqual(1);
  }));

  it("should mark for redraw when selecting the eraser", fakeAsync(() => {
    mapToolService.activeTool = MapCanvasTool.ERASER;
    tick(200);
    expect(eraserModule.redraw).toBeTruthy();
  }));

  it("should reset the redraw when drawing", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    mapToolService.activeTool = MapCanvasTool.ERASER;
    tick(200);

    expect(eraserModule.redraw).toBeTruthy();
    eraserModule.onDraw(context);
    expect(eraserModule.redraw).toBeFalsy();
  }));

  it("should mark for redraw when switching to another too from the eraser tool", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    mapToolService.activeTool = MapCanvasTool.ERASER;
    tick(200);
    eraserModule.onDraw(context);

    mapToolService.activeTool = MapCanvasTool.MARKER;
    tick(200);
    expect(eraserModule.redraw).toBeTruthy();
  }));

  afterEach(() => {
    eraserModule.onDestroy();
  });
});
