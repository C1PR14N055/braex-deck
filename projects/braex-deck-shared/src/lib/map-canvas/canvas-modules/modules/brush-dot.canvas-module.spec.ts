import { async, fakeAsync, TestBed, tick } from "@angular/core/testing";
import { BrushDotCanvasModule } from "./brush-dot.canvas-module";
import { BrushConfigService } from "../../services/brush-config.service";
import { MapToolService } from "../../services/map-tool.service";
import { MapCanvasTool } from "../../ds/map-canvas-tool.enum";
import { Vector2 } from "../../ds/vector2";

describe("BrushDotCanvasModule", () => {
  let dotModule: BrushDotCanvasModule;
  let brushConfigService: BrushConfigService;
  let toolService: MapToolService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [BrushDotCanvasModule, BrushConfigService, MapToolService]
    });

    dotModule = TestBed.get(BrushDotCanvasModule);
    brushConfigService = TestBed.get(BrushConfigService);
    toolService = TestBed.get(MapToolService);
  }));

  beforeEach(() => {
    dotModule.onInit();
  });

  it("should create", () => {
    expect(dotModule).toBeTruthy();
  });

  it("should activate the brush dot when the BRUSH is active", fakeAsync(() => {
    expect(dotModule.active).toBeFalsy();
    toolService.activeTool = MapCanvasTool.BRUSH;
    tick(300);
    expect(dotModule.active).toBeTruthy();
  }));

  it("should deactivate the brush dot when the BRUSH is NOT active anymore", fakeAsync(() => {
    expect(dotModule.active).toBeFalsy();
    toolService.activeTool = MapCanvasTool.BRUSH;
    tick(300);
    expect(dotModule.active).toBeTruthy();

    toolService.activeTool = MapCanvasTool.SELECTION;
    tick(300);
    expect(dotModule.active).toBeFalsy();
  }));

  it("should place the brush dot at the mouse position on each update", fakeAsync(() => {
    expect(dotModule.active).toBeFalsy();
    toolService.activeTool = MapCanvasTool.BRUSH;
    tick(300);
    expect(dotModule.active).toBeTruthy();

    const vectors = [
      Vector2.create(300, 240),
      Vector2.create(290, 230),
      Vector2.create(320, 260),
      Vector2.create(320, 260),
      Vector2.create(100, 290)
    ];
    vectors.forEach(vector => {
      dotModule.onUpdate(vector);
      tick(200);
      expect(dotModule.position.x).toBeCloseTo(vector.x);
      expect(dotModule.position.y).toBeCloseTo(vector.y);
    });
  }));

  it("should mark for redraw when selecting the BRUSH tool", fakeAsync(() => {
    expect(dotModule.redraw).toBeFalsy();
    toolService.activeTool = MapCanvasTool.BRUSH;
    tick(300);
    expect(dotModule.redraw).toBeTruthy();
  }));

  it("should reset the redraw mark when drawing", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    expect(dotModule.redraw).toBeFalsy();
    toolService.activeTool = MapCanvasTool.BRUSH;
    tick(300);
    expect(dotModule.redraw).toBeTruthy();

    dotModule.onDraw(context);
    expect(dotModule.redraw).toBeFalsy();
  }));

  it("should mark for redraw when the current dot position !== the previous one", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    toolService.activeTool = MapCanvasTool.BRUSH;
    tick(300);
    dotModule.onDraw(context);

    expect(dotModule.redraw).toBeFalsy();
    dotModule.onUpdate(dotModule.position.add(12));
    expect(dotModule.redraw).toBeTruthy();
  }));

  it("should mark for redraw when the active tool is switched from the BRUSH tool to another", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    toolService.activeTool = MapCanvasTool.BRUSH;
    tick(300);
    expect(dotModule.active).toBeTruthy();
    dotModule.onDraw(context);

    const spy = spyOn(context, "beginPath");
    expect(dotModule.redraw).toBeFalsy();
    toolService.activeTool = MapCanvasTool.SELECTION;
    tick(300);
    expect(dotModule.redraw).toBeTruthy();

    dotModule.onDraw(context);
    expect(spy).not.toHaveBeenCalled();
  }));

  afterEach(() => {
    dotModule.onDestroy();
  });
});
