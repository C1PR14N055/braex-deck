import { async, fakeAsync, TestBed, tick } from "@angular/core/testing";
import { BrushCanvasModule } from "./brush.canvas-module";
import { BrushConfigService } from "../../services/brush-config.service";
import { Vector2 } from "../../ds/vector2";
import { MapToolService } from "../../services/map-tool.service";
import { MapCanvasTool } from "../../ds/map-canvas-tool.enum";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { TOOL_MOUSE_BUTTON } from "../../ds/tool-config";
import { LocalPlayerInfoService } from "../../../services/local-player-info.service";
import { FakeLocalPlayerInfoService } from "../../../services/testing/fake-local-player-info.service";
import { FakeInputControllerService } from "../../services/testing/fake-input-controller.service";
import { InputControllerService } from "../../services/input-controller.service";
import { BrushLinesCanvasModule } from "./brush-lines.canvas-module";
import { FakeDrawingSyncService } from "../../services/testing/fake-drawing-sync.service";
import {
  DEFAULT_BRUSH_THICKNESS,
  DEFAULT_COLOR
} from "../../ds/default-drawing-config";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { PaintSyncBody } from "../../ds/drawing-sync/paint-sync-body";
import { PaintEventState } from "../../ds/drawing-sync/paint-event-state.enum";

describe("BrushCanvasModule", () => {
  let brushModule: BrushCanvasModule;
  let mapToolService: MapToolService;
  const points = [
    Vector2.create(500, 500),
    Vector2.create(510, 500),
    Vector2.create(520, 500),
    Vector2.create(530, 500)
  ];
  let fakeDrawingSync: FakeDrawingSyncService;
  let fakeLocalPlayer: FakeLocalPlayerInfoService;
  let fakeInputController: FakeInputControllerService;
  let brushLines: BrushLinesCanvasModule;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        BrushCanvasModule,
        BrushConfigService,
        MapToolService,
        { provide: DrawingSyncService, useClass: FakeDrawingSyncService },
        {
          provide: LocalPlayerInfoService,
          useClass: FakeLocalPlayerInfoService
        },
        {
          provide: InputControllerService,
          useClass: FakeInputControllerService
        },
        BrushLinesCanvasModule
      ]
    }).compileComponents();

    brushModule = TestBed.get(BrushCanvasModule);
    mapToolService = TestBed.get(MapToolService);
    fakeLocalPlayer = TestBed.get(LocalPlayerInfoService);
    fakeInputController = TestBed.get(InputControllerService);
    fakeDrawingSync = TestBed.get(DrawingSyncService);
    brushLines = TestBed.get(BrushLinesCanvasModule);
  }));

  beforeEach(() => {
    brushModule.onInit();
  });

  it("should create", () => {
    expect(brushModule).toBeTruthy();
  });

  it("should activate when the currently active tool is the BRUSH tool and LMB is down", fakeAsync(() => {
    mapToolService.activeTool = MapCanvasTool.BRUSH;
    activateBrush(points[0]);
  }));

  function activateBrush(position: Vector2) {
    expect(brushModule.active).toBeFalsy();
    fakeInputController.triggerMouseDown({
      position: position,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);
    expect(brushModule.active).toBeTruthy();
  }

  it("should not activate when the currently active tool is not the BRUSH tool", fakeAsync(() => {
    mapToolService.activeTool = MapCanvasTool.SELECTION;
    expect(brushModule.active).toBeFalsy();
    fakeInputController.triggerMouseDown({
      position: Vector2.create(200, 200),
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);

    expect(brushModule.active).toBeFalsy();
  }));

  it("should deactivate the brush when LMB is released", fakeAsync(() => {
    fakeLocalPlayer.triggerLocalPlayerNickname("TestPlayer");
    tick(500);

    mapToolService.activeTool = MapCanvasTool.BRUSH;
    activateBrush(points[0]);

    fakeInputController.triggerMouseUp({
      position: points[0],
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);
    expect(brushModule.active).toBeFalsy();
  }));

  it("should draw a line", fakeAsync(() => {
    mapToolService.activeTool = MapCanvasTool.BRUSH;
    drawLine(points);
    expectOneLine();
  }));

  function drawLine(path: Vector2[]) {
    const first = path[0];
    activateBrush(first);
    path.forEach(point => {
      brushModule.onUpdate(point);
      tick(50);
    });

    const last = path[path.length - 1];
    fakeInputController.triggerMouseUp({
      position: last,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);
  }

  function expectOneLine() {
    expect(brushLines.linePaths.size).toBe(1);
    const linePath = brushLines.linePaths.get(0);
    expect(linePath.points.size).toBe(points.length);
    points.forEach((point, i) => {
      expect(linePath.points.get(i)).toBe(point);
    });
  }

  it("should start syncing the brush point when lmb is down and the BRUSH tool is active", fakeAsync(() => {
    fakeLocalPlayer.triggerLocalPlayerNickname("TestPlayer");
    tick(500);

    const vector = Vector2.create(20, 390);
    let received: DrawingSyncEvent<PaintSyncBody>;
    const spy = spyOn(fakeDrawingSync, "sendEvent").and.callFake(syncPoint => {
      received = syncPoint;
    });

    mapToolService.activeTool = MapCanvasTool.BRUSH;
    expect(brushModule.active).toBeFalsy();
    fakeInputController.triggerMouseDown({
      position: vector,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);

    expect(brushModule.active).toBeTruthy();

    brushModule.onUpdate(vector);
    tick(300);
    expect(spy).toHaveBeenCalled();
    expect(received.body.position.x).toBeCloseTo(vector.x);
    expect(received.body.position.y).toBeCloseTo(vector.y);
    expect(received.body.state).toBe(PaintEventState.START);
    expect(received.body.config).toBeTruthy();
    expect(received.body.config.color).toBe(DEFAULT_COLOR);
    expect(received.body.config.thickness).toBe(DEFAULT_BRUSH_THICKNESS);
  }));

  it("should send a brush end when the lmb is up and the brush was active", fakeAsync(() => {
    fakeLocalPlayer.triggerLocalPlayerNickname("TestPlayer");
    tick(500);

    const startVector = Vector2.create(220, 802);
    const endVector = Vector2.create(90, 37);

    let received: DrawingSyncEvent<PaintSyncBody>;
    const spy = spyOn(fakeDrawingSync, "sendEvent").and.callFake(point => {
      received = point;
    });

    mapToolService.activeTool = MapCanvasTool.BRUSH;
    expect(brushModule.active).toBeFalsy();
    fakeInputController.triggerMouseDown({
      position: startVector,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);

    expect(brushModule.active).toBeTruthy();
    brushModule.onUpdate(startVector);
    tick(300);
    expect(spy).toHaveBeenCalledTimes(1);

    brushModule.onUpdate(endVector);
    tick(300);
    expect(spy).toHaveBeenCalledTimes(2);

    fakeInputController.triggerMouseUp({
      position: endVector,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);
    expect(spy).toHaveBeenCalledTimes(3);
    expect(received.body.state).toBe(PaintEventState.END);
    expect(received.body.config).toBeFalsy();
  }));

  it("should send brush updates while the brush is drawing", fakeAsync(() => {
    fakeLocalPlayer.triggerLocalPlayerNickname("TestPlayer");
    tick(500);

    let received: DrawingSyncEvent<PaintSyncBody>;
    const spy = spyOn(fakeDrawingSync, "sendEvent").and.callFake(point => {
      received = point;
    });

    const vectors = [
      Vector2.create(50, 50),
      Vector2.create(50, 60),
      Vector2.create(50, 70),
      Vector2.create(50, 80),
      Vector2.create(50, 90),
      Vector2.create(50, 100)
    ];
    const first = vectors[0];

    mapToolService.activeTool = MapCanvasTool.BRUSH;
    expect(brushModule.active).toBeFalsy();
    fakeInputController.triggerMouseDown({
      position: first,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);

    vectors.forEach((vector, i) => {
      brushModule.onUpdate(vector);
      tick(600);
      expect(spy).toHaveBeenCalledTimes(i + 1);

      let expectedState: PaintEventState;
      if (vector === first) {
        expectedState = PaintEventState.START;
      } else {
        expectedState = PaintEventState.MIDDLE;
      }

      expect(received.body.position.x).toBeCloseTo(vector.x);
      expect(received.body.position.y).toBeCloseTo(vector.y);
      expect(received.body.state).toBe(expectedState);

      if (vector === first) {
        expect(received.body.config).toBeTruthy();
      } else {
        expect(received.body.config).toBeFalsy();
      }
    });
  }));

  it("should not add new points to the line path while the mouse position remains the same", fakeAsync(() => {
    mapToolService.activeTool = MapCanvasTool.BRUSH;

    expect(brushLines.linePaths.size).toBeFalsy();
    expect(brushModule.active).toBeFalsy();

    const vector = Vector2.create(200, 200);
    fakeInputController.triggerMouseDown({
      position: vector,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);

    const updates = 10;
    for (let i = 0; i < updates; i++) {
      brushModule.onUpdate(vector);
      tick(200);
    }

    fakeInputController.triggerMouseUp({
      position: vector,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    expect(brushLines.linePaths.size).toBe(1);
    const linePath = brushLines.linePaths.get(0);
    expect(linePath.points.size).toBe(1);
  }));

  it("should add the brush line to the brush lines module when the line is finished", fakeAsync(() => {
    mapToolService.activeTool = MapCanvasTool.BRUSH;
    const spy = spyOn(brushLines, "addLinePath");
    const vectors = [
      Vector2.create(90, 42),
      Vector2.create(16, 29),
      Vector2.create(1337, 652),
      Vector2.create(900, 630)
    ];
    drawLine(vectors);

    expect(spy).toHaveBeenCalled();
  }));

  it("should not sync the same point as the START and as the MIDDLE", fakeAsync(() => {
    fakeLocalPlayer.triggerLocalPlayerNickname("TestPlayer");
    tick(500);

    mapToolService.activeTool = MapCanvasTool.BRUSH;
    let received: DrawingSyncEvent<PaintSyncBody>;
    const spy = spyOn(fakeDrawingSync, "sendEvent").and.callFake(
      (event: DrawingSyncEvent<PaintSyncBody>) => {
        received = event;
      }
    );

    const vector = Vector2.create(34, 56);
    fakeInputController.triggerMouseDown({
      position: vector,
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);
    brushModule.onUpdate(vector);
    brushModule.onUpdate(vector);
    expect(spy).toHaveBeenCalledTimes(1);
    expect(received.body.state).toEqual(PaintEventState.START);
  }));

  it("should not sync the same point in multiple updates", fakeAsync(() => {
    fakeLocalPlayer.triggerLocalPlayerNickname("TestPlayer");
    tick(500);
    let received: DrawingSyncEvent<PaintSyncBody>;
    const spy = spyOn(fakeDrawingSync, "sendEvent").and.callFake(
      (event: DrawingSyncEvent<PaintSyncBody>) => {
        received = event;
      }
    );

    const vectors = [Vector2.create(3, 565), Vector2.create(44, 78)];
    mapToolService.activeTool = MapCanvasTool.BRUSH;
    fakeInputController.triggerMouseDown({
      position: vectors[0],
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);

    brushModule.onUpdate(vectors[0]);
    brushModule.onUpdate(vectors[1]);
    brushModule.onUpdate(vectors[1]);
    brushModule.onUpdate(vectors[1]);
    brushModule.onUpdate(vectors[1]);

    expect(spy).toHaveBeenCalledTimes(2);
    expect(received.body.state).toEqual(PaintEventState.MIDDLE);
    expect(received.body.position.equals(vectors[1])).toBeTruthy();
  }));

  it("should send an empty position on the END when it's the same as the last synced position", fakeAsync(() => {
    fakeLocalPlayer.triggerLocalPlayerNickname("TestPlayer");
    tick(500);
    let received: DrawingSyncEvent<PaintSyncBody>;
    const spy = spyOn(fakeDrawingSync, "sendEvent").and.callFake(
      (event: DrawingSyncEvent<PaintSyncBody>) => {
        received = event;
      }
    );

    const vectors = [
      Vector2.create(3, 565),
      Vector2.create(44, 78),
      Vector2.create(99, 234)
    ];
    mapToolService.activeTool = MapCanvasTool.BRUSH;
    fakeInputController.triggerMouseDown({
      position: vectors[0],
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);

    brushModule.onUpdate(vectors[0]);
    brushModule.onUpdate(vectors[1]);
    brushModule.onUpdate(vectors[2]);

    fakeInputController.triggerMouseUp({
      position: vectors[2],
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);

    expect(spy).toHaveBeenCalledTimes(4);
    expect(received.body.state).toEqual(PaintEventState.END);
    expect(received.body.position).toBeNull();
  }));

  it("should mark for redraw after adding a line to the brush lines module", fakeAsync(() => {
    mapToolService.activeTool = MapCanvasTool.BRUSH;
    const vectors = [
      Vector2.create(90, 42),
      Vector2.create(16, 29),
      Vector2.create(1337, 652),
      Vector2.create(900, 630)
    ];
    drawLine(vectors);

    expect(brushModule.redraw).toBeTruthy();
  }));

  it("should not send a sync event when on mouse up when the brush is NOT active", fakeAsync(() => {
    fakeLocalPlayer.triggerLocalPlayerNickname("TestPlayer");
    tick(500);
    const spy = spyOn(fakeDrawingSync, "sendEvent");
    mapToolService.activeTool = MapCanvasTool.MARKER;
    expect(brushModule.active).toBeFalsy();

    fakeInputController.triggerMouseDown({
      position: Vector2.create(),
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);

    fakeInputController.triggerMouseUp({
      position: Vector2.create(),
      mouseButton: TOOL_MOUSE_BUTTON
    });
    tick(200);

    expect(spy).not.toHaveBeenCalled();
  }));

  afterEach(() => {
    brushModule.onDestroy();
  });
});
