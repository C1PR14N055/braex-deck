import { fakeAsync, TestBed, tick } from "@angular/core/testing";
import { TeamBrushSyncCanvasModule } from "./team-brush-sync.canvas-module";
import { LocalPlayerInfoService } from "../../../services/local-player-info.service";
import { FakeLocalPlayerInfoService } from "../../../services/testing/fake-local-player-info.service";
import { OverwolfGameEventsService } from "../../../overwolf/overwolf-game-events.service";
import { FakeOverwolfGameEventsService } from "../../../overwolf/testing/fake-overwolf-game-events.service";
import { BrushConfig } from "../../ds/brush-config";
import { Vector2 } from "../../ds/vector2";
import { Color } from "../../ds/color.enum";
import { TeamLinesSyncCanvasModule } from "./team-lines-sync.canvas-module";
import { FakeDrawingSyncService } from "../../services/testing/fake-drawing-sync.service";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { PaintSyncBody } from "../../ds/drawing-sync/paint-sync-body";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { PaintEventState } from "../../ds/drawing-sync/paint-event-state.enum";
import { DrawingSyncEventType } from "../../ds/drawing-sync/drawing-sync-event-type";

describe("TeamBrushSyncCanvasModule", () => {
  let teamSyncModule: TeamBrushSyncCanvasModule;
  let fakeLocalPlayer: FakeLocalPlayerInfoService;
  let fakeGameEvents: FakeOverwolfGameEventsService;
  let fakeDrawingSync: FakeDrawingSyncService;
  let teamLines: TeamLinesSyncCanvasModule;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TeamBrushSyncCanvasModule,
        {
          provide: LocalPlayerInfoService,
          useClass: FakeLocalPlayerInfoService
        },
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        },
        {
          provide: DrawingSyncService,
          useClass: FakeDrawingSyncService
        },
        TeamLinesSyncCanvasModule
      ]
    });

    teamSyncModule = TestBed.get(TeamBrushSyncCanvasModule);
    fakeLocalPlayer = TestBed.get(LocalPlayerInfoService);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    fakeDrawingSync = TestBed.get(DrawingSyncService);
    teamSyncModule.onInit();
    teamLines = TestBed.get(TeamLinesSyncCanvasModule);
  });

  it("should create", () => {
    expect(teamSyncModule).toBeTruthy();
  });

  it("should add new owners to the team mate brushes to sync", fakeAsync(() => {
    const localPlayer = "TestLocalPlayer";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(400);

    const teamMate = "UnknownPlayer";
    fakeDrawingSync.sendEvent(
      new DrawingSyncEvent<PaintSyncBody>(
        DrawingSyncEventType.PAINT,
        new PaintSyncBody(
          PaintEventState.START,
          Vector2.create(),
          new BrushConfig()
        ),
        teamMate
      )
    );
    tick(500);

    expect(teamSyncModule.teamBrushHolders.size).toBe(1);
    const mate = teamSyncModule.teamBrushHolders.get(0);
    expect(mate.playerNickname).toBe(teamMate);
  }));

  it("should draw the brush points of the team mates when received", fakeAsync(() => {
    const localPlayer = "TestLocalPlayer";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(400);

    const teamMate = "TestTeamMate";
    const positions = [
      Vector2.create(99, 46),
      Vector2.create(120, 50),
      Vector2.create(140, 60),
      Vector2.create(150, 90)
    ];
    const firstPosition = positions[0];
    fakeDrawingSync.triggerPaintEvent(
      new DrawingSyncEvent<PaintSyncBody>(
        DrawingSyncEventType.PAINT,
        new PaintSyncBody(
          PaintEventState.START,
          firstPosition,
          new BrushConfig()
        ),
        teamMate
      )
    );
    tick(500);

    let teamMateHolder = teamSyncModule.teamBrushHolders.get(0);
    expect(teamMateHolder.tempBrushPoints.length).toBe(1);
    expect(
      teamMateHolder.tempBrushPoints[0].equals(firstPosition)
    ).toBeTruthy();

    for (let i = 1; i < positions.length; i++) {
      const position = positions[i];
      fakeDrawingSync.triggerPaintEvent(
        new DrawingSyncEvent<PaintSyncBody>(
          DrawingSyncEventType.PAINT,
          new PaintSyncBody(PaintEventState.MIDDLE, position),
          teamMate
        )
      );
      tick(500);
      teamMateHolder = teamSyncModule.teamBrushHolders.get(0);
      expect(teamMateHolder.tempBrushPoints.length).toBe(i + 1);
      expect(teamMateHolder.tempBrushPoints[i].equals(position)).toBeTruthy();
    }
  }));

  it("should apply the active brush config of the team mate", fakeAsync(() => {
    const localPlayer = "TestLocalPlayer";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(400);

    const teamMate = "TestTeamMate";
    fakeGameEvents.triggerPlayerTeamNicknames([localPlayer, teamMate]);
    tick(400);

    const brushConfig = new BrushConfig(16, Color.CINNABAR);
    const event = new DrawingSyncEvent<PaintSyncBody>(
      DrawingSyncEventType.PAINT,
      new PaintSyncBody(
        PaintEventState.START,
        Vector2.create(49, 42),
        brushConfig
      ),
      teamMate
    );
    fakeDrawingSync.triggerPaintEvent(event);
    tick(400);

    const teamMateHolder = teamSyncModule.teamBrushHolders.get(0);
    expect(teamMateHolder.activeBrushConfig.equals(brushConfig)).toBeTruthy();
  }));

  it("should add a line path of the team mate when his brush ends", fakeAsync(() => {
    const localPlayer = "TestLocalPlayer";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(400);

    const teamMate = "TestTeamMate";
    const brushConfig = new BrushConfig(23, Color.JAFFA);
    const positions = [
      Vector2.create(42, 42),
      Vector2.create(50, 42),
      Vector2.create(42, 60)
    ];

    fakeDrawingSync.triggerPaintEvent(
      new DrawingSyncEvent<PaintSyncBody>(
        DrawingSyncEventType.PAINT,
        new PaintSyncBody(PaintEventState.START, positions[0], brushConfig),
        teamMate
      )
    );
    tick(400);

    fakeDrawingSync.triggerPaintEvent(
      new DrawingSyncEvent<PaintSyncBody>(
        DrawingSyncEventType.PAINT,
        new PaintSyncBody(PaintEventState.MIDDLE, positions[1]),
        teamMate
      )
    );
    tick(400);

    fakeDrawingSync.triggerPaintEvent(
      new DrawingSyncEvent<PaintSyncBody>(
        DrawingSyncEventType.PAINT,
        new PaintSyncBody(PaintEventState.END, positions[2]),
        teamMate
      )
    );
    tick(400);

    expect(teamLines.teamBrushes.get(teamMate).size).toBe(1);
    const linePath = teamLines.teamBrushes.get(teamMate).get(0);
    expect(linePath.brushConfig.equals(brushConfig)).toBeTruthy();
    expect(linePath.points.size).toBe(positions.length);
    positions.forEach(x => {
      expect(linePath.points).toContain(x);
    });
  }));

  it("should add the temp line to the team lines sync module", fakeAsync(() => {
    const spy = spyOn(teamLines, "addTeamBrush");
    const localPlayer = "TheLocalPlayer";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);

    const teamMate = "SomeTeamMate";
    const brushConfig = new BrushConfig(23, Color.JAFFA);
    const positions = [
      Vector2.create(42, 42),
      Vector2.create(50, 42),
      Vector2.create(42, 60)
    ];

    fakeDrawingSync.triggerPaintEvent(
      new DrawingSyncEvent<PaintSyncBody>(
        DrawingSyncEventType.PAINT,
        new PaintSyncBody(PaintEventState.START, positions[0], brushConfig),
        teamMate
      )
    );
    tick(400);

    fakeDrawingSync.triggerPaintEvent(
      new DrawingSyncEvent<PaintSyncBody>(
        DrawingSyncEventType.PAINT,
        new PaintSyncBody(PaintEventState.MIDDLE, positions[1]),
        teamMate
      )
    );
    tick(400);

    fakeDrawingSync.triggerPaintEvent(
      new DrawingSyncEvent<PaintSyncBody>(
        DrawingSyncEventType.PAINT,
        new PaintSyncBody(PaintEventState.END, positions[2]),
        teamMate
      )
    );
    tick(400);

    expect(spy).toHaveBeenCalled();
  }));

  afterEach(() => {
    teamSyncModule.onDestroy();
  });
});
