import { CanvasModule } from "../canvas-module.decorator";
import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit,
  CanvasModuleUpdate
} from "../canvas-module-hooks";
import { Vector2 } from "../../ds/vector2";
import { MapToolService } from "../../services/map-tool.service";
import { MapCanvasTool } from "../../ds/map-canvas-tool.enum";
import { Subscription } from "rxjs";
import { BrushConfigService } from "../../services/brush-config.service";

@CanvasModule({
  zIndex: 999
})
export class BrushDotCanvasModule
  implements
    CanvasModuleInit,
    CanvasModuleUpdate,
    CanvasModuleDraw,
    CanvasModuleDestroy {
  private _active = false;
  private _position = Vector2.create();
  private _previousPosition: Vector2 | null = null;
  private _subs: Subscription[] = [];
  private _redraw = false;

  constructor(
    private _brushConfig: BrushConfigService,
    private _toolService: MapToolService
  ) {}

  onInit() {
    const sub = this._toolService.activeToolChanged().subscribe(tool => {
      this._redraw = this._active || tool === MapCanvasTool.BRUSH;
      this._active = tool === MapCanvasTool.BRUSH;
    });
    this._subs.push(sub);
  }

  onDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  onUpdate(mousePosition: Vector2) {
    if (!this._active) {
      return;
    }

    this._previousPosition = this._position;
    this._position = mousePosition;

    if (!this._previousPosition.equals(this._position)) {
      this._redraw = true;
    }
  }

  onDraw(canvasContext: CanvasRenderingContext2D) {
    if (!this._active) {
      return;
    }

    canvasContext.beginPath();
    canvasContext.ellipse(
      this._position.x,
      this._position.y,
      this._brushConfig.thickness / 2,
      this._brushConfig.thickness / 2,
      Math.PI,
      0,
      2 * Math.PI
    );
    canvasContext.lineWidth = 0.5;
    canvasContext.strokeStyle = "#ffffff";
    canvasContext.setLineDash([1, 1]);
    canvasContext.stroke();
    canvasContext.closePath();
    this._redraw = false;
  }

  get active() {
    return this._active;
  }

  get position() {
    return this._position;
  }

  get redraw() {
    return this._redraw;
  }
}
