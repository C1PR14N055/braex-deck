import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit
} from "../canvas-module-hooks";
import { CanvasModule } from "../canvas-module.decorator";
import { LinePath } from "../../ds/line-path";
import { List, Map } from "immutable";
import { BrushDrawingShared } from "./brush-drawing-shared";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { Subscription } from "rxjs";
import { OverwolfGameEventsService } from "../../../overwolf/overwolf-game-events.service";
import { GamePhase } from "../../../ds/game/game-phase.enum";
import { ChangeActionType } from "../../ds/drawing-sync/change-action-type.enum";
import { ChangeEventType } from "../../ds/change-event-type.enum";

@CanvasModule({
  zIndex: 9
})
export class TeamLinesSyncCanvasModule
  implements CanvasModuleInit, CanvasModuleDestroy, CanvasModuleDraw {
  private _redraw = false;
  private _subs: Subscription[] = [];
  private _teamBrushes = Map<string, List<LinePath>>();
  private _inGame = false;

  constructor(
    private _drawingSync: DrawingSyncService,
    private _gameEvents: OverwolfGameEventsService
  ) {}

  onInit() {
    this._initSyncEvents();
    this._initAutoClearing();
  }

  private _initSyncEvents() {
    let sub = this._drawingSync.paintClearEvents().subscribe(event => {
      if (this._teamBrushes.has(event.owner)) {
        this._teamBrushes = this._teamBrushes.set(
          event.owner,
          List<LinePath>()
        );
        this._redraw = true;
      }
    });
    this._subs.push(sub);

    sub = this._drawingSync.undoEvents().subscribe(event => {
      if (event.body.type !== ChangeActionType.LINE) {
        return;
      }

      if (this._teamBrushes.has(event.owner)) {
        let brushes = this._teamBrushes.get(event.owner);
        if (event.body.event.type === ChangeEventType.CREATE) {
          const matching = brushes.filter(x => x.equals(event.body.event.body));
          if (!matching.isEmpty()) {
            brushes = brushes.delete(brushes.indexOf(matching.get(0)));
          }
        } else if (event.body.event.type === ChangeEventType.REMOVE) {
          brushes = brushes.push(event.body.event.body);
        }

        this._teamBrushes = this._teamBrushes.set(event.owner, brushes);
        this._redraw = true;
      }
    });
    this._subs.push(sub);

    sub = this._drawingSync.redoEvents().subscribe(event => {
      if (event.body.type !== ChangeActionType.LINE) {
        return;
      }

      if (this._teamBrushes.has(event.owner)) {
        let brushes = this._teamBrushes.get(event.owner);
        if (event.body.event.type === ChangeEventType.CREATE) {
          brushes = brushes.push(event.body.event.body);
        } else if (event.body.event.type === ChangeEventType.REMOVE) {
          const matching = brushes.filter(x => x.equals(event.body.event.body));
          if (!matching.isEmpty()) {
            brushes = brushes.delete(brushes.indexOf(matching.get(0)));
          }
        }

        this._teamBrushes = this._teamBrushes.set(event.owner, brushes);
        this._redraw = true;
      }
    });
    this._subs.push(sub);
  }

  private _initAutoClearing() {
    const sub = this._gameEvents.onPhaseChanged().subscribe(phase => {
      if (phase === GamePhase.LOBBY) {
        if (this._inGame) {
          this._teamBrushes = this._teamBrushes.clear();
          this._redraw = true;
          this._inGame = false;
        }
      } else {
        this._inGame = true;
      }
    });
    this._subs.push(sub);
  }

  addTeamBrush(nickname: string, line: LinePath) {
    if (this._teamBrushes.has(nickname)) {
      let lines = this._teamBrushes.get(nickname);
      lines = lines.push(line);
      this._teamBrushes = this._teamBrushes.set(nickname, lines);
      this._redraw = true;

      return;
    }

    this._teamBrushes = this._teamBrushes.set(nickname, List<LinePath>([line]));
    this._redraw = true;
  }

  onDraw(canvasContext: CanvasRenderingContext2D) {
    this._teamBrushes.forEach(lines => {
      BrushDrawingShared.drawLinePaths(canvasContext, lines);
    });
    this._redraw = false;
  }

  onDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  get redraw() {
    return this._redraw;
  }

  get teamBrushes() {
    return this._teamBrushes;
  }
}
