import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit
} from "../canvas-module-hooks";
import { CanvasModule } from "../canvas-module.decorator";
import { Subscription } from "rxjs";
import { MapToolService } from "../../services/map-tool.service";
import { MapCanvasTool } from "../../ds/map-canvas-tool.enum";
import { Vector2 } from "../../ds/vector2";
import { MapMarker } from "../../ds/map-marker";
import { List } from "immutable";
import { MapMarkerType } from "../../ds/map-marker-type.enum";
import { ResourceLoadingService } from "../../services/resource-loading.service";
import { CanvasResource } from "../../ds/canvas-resource";
import { MapTool } from "../../core/map-tool.decorator";
import { TOOL_MOUSE_BUTTON } from "../../ds/tool-config";
import { InputControllerService } from "../../services/input-controller.service";
import { faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { LocalPlayerInfoService } from "../../../services/local-player-info.service";
import { MarkerDrawingShared } from "./marker-drawing-shared";
import { MarkerSyncBody } from "../../ds/drawing-sync/marker-sync-body";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { DrawingSyncEventType } from "../../ds/drawing-sync/drawing-sync-event-type";
import { MarkerConfigService } from "../../services/marker-config.service";
import { MapMarkerIconPipe } from "../../../map-marker-icon/map-marker-icon.pipe";
import { OverwolfGameEventsService } from "../../../overwolf/overwolf-game-events.service";
import { WindowDataExchangeService } from "../../../services/window-data-exchange.service";
import { WindowCommunicator } from "../../../services/window-communicator";
import { ChangeHistoryService } from "../../services/change-history.service";
import { ChangeEventType } from "../../ds/change-event-type.enum";
import { ChangeHistoryEvent } from "../../ds/change-history-event";
import { MapBackgroundCanvasModule } from "./map-background.canvas-module";
import { InGameUtils } from "../../utils/in-game-utils";
import { OverwolfCoreService } from "../../../overwolf/overwolf-core.service";

@CanvasModule({
  zIndex: 200
})
@MapTool({
  tool: MapCanvasTool.MARKER,
  cursor: "crosshair",
  icon: faMapMarkerAlt
})
export class MapMarkerCanvasModule
  implements CanvasModuleInit, CanvasModuleDestroy, CanvasModuleDraw {
  private _redraw = false;
  private _subs: Subscription[] = [];
  private _localPlayerNickname: string | null = null;
  private _placedMarkers = List<MapMarker>();
  private _markerResourceMapping = new Map<MapMarkerType, CanvasResource>();
  private _windowComm: WindowCommunicator;

  constructor(
    private _inputController: InputControllerService,
    private _mapToolService: MapToolService,
    private _resourceLoadingService: ResourceLoadingService,
    private _drawingSync: DrawingSyncService,
    private _localPlayer: LocalPlayerInfoService,
    private _markerConfig: MarkerConfigService,
    private _gameEvents: OverwolfGameEventsService,
    private _windowData: WindowDataExchangeService,
    private _changeHistory: ChangeHistoryService,
    private _mapBackground: MapBackgroundCanvasModule,
    private _overwolfCore: OverwolfCoreService
  ) {}

  onInit() {
    this._initResources();
    this._initLocalPlayerInfo();
    this._initMarkerPlacement();
    this._initAutoClearing();
    this._initChangeHistoryEvents();
    this._initWindowCommunication();
  }

  private _initResources() {
    const pipe = new MapMarkerIconPipe();
    Object.keys(MapMarkerType).forEach(key => {
      const sub = this._resourceLoadingService
        .loadImage(pipe.transform(MapMarkerType[key]))
        .subscribe(resource => {
          this._markerResourceMapping.set(MapMarkerType[key], resource);
        });
      this._subs.push(sub);
    });
  }

  private _initLocalPlayerInfo() {
    const sub = this._localPlayer.localPlayerNickname().subscribe(nickname => {
      this._localPlayerNickname = nickname;
    });
    this._subs.push(sub);
  }

  private _initMarkerPlacement() {
    const sub = this._inputController.mouseUp().subscribe(event => {
      if (this._mapToolService.activeTool !== MapCanvasTool.MARKER) {
        return;
      }

      if (event.mouseButton === TOOL_MOUSE_BUTTON) {
        this.addMarker(event.position);
      }
    });
    this._subs.push(sub);
  }

  private _initAutoClearing() {
    let sub = this._gameEvents.onMatchLeft().subscribe(() => {
      this.clear();
    });
    this._subs.push(sub);

    sub = this._overwolfCore.onPubgTerminated().subscribe(() => {
      this.clear();
    });
    this._subs.push(sub);
  }

  private _initChangeHistoryEvents() {
    let sub = this._changeHistory.undoEvent().subscribe(event => {
      if (event.body instanceof MapMarker) {
        if (event.type === ChangeEventType.CREATE) {
          this._placedMarkers = this._placedMarkers.delete(
            this._placedMarkers.indexOf(event.body)
          );
        } else if (event.type === ChangeEventType.REMOVE) {
          this._placedMarkers = this._placedMarkers.push(event.body);
        }
        this._redraw = true;
      }
    });
    this._subs.push(sub);

    sub = this._changeHistory.redoEvent().subscribe(event => {
      if (event.body instanceof MapMarker) {
        if (event.type === ChangeEventType.CREATE) {
          this._placedMarkers = this._placedMarkers.push(event.body);
        } else if (event.type === ChangeEventType.REMOVE) {
          this._placedMarkers = this._placedMarkers.delete(
            this._placedMarkers.indexOf(event.body)
          );
        }
        this._redraw = true;
      }
    });
    this._subs.push(sub);
  }

  private _initWindowCommunication() {
    this._windowComm = this._windowData.getWindowCommunicator();
    if (!this._windowComm) {
      return;
    }

    let sub = this._windowComm.markerAdded().subscribe(marker => {
      const de = this._denormalizeMapMarker(marker);
      const matching = this._placedMarkers.filter(x => x.equals(de));
      if (!matching.isEmpty()) {
        return;
      }

      this._addMarker(de);
    });
    this._subs.push(sub);

    sub = this._windowComm.markerRemoved().subscribe(marker => {
      const de = this._denormalizeMapMarker(marker);
      const matching = this._placedMarkers.filter(x => x.equals(de));
      if (matching.isEmpty()) {
        return;
      }

      this._placedMarkers = this._placedMarkers.remove(
        this._placedMarkers.indexOf(matching.get(0))
      );
      this._redraw = true;
    });
    this._subs.push(sub);
  }

  private _denormalizeMapMarker(marker: MapMarker) {
    return MapMarker.create(
      InGameUtils.denormalizeVector(
        marker.position,
        this._mapBackground.mapSize
      ),
      marker.config
    );
  }

  addMarker(position: Vector2) {
    let mapMarker: MapMarker;
    const config = this._markerConfig.createMarkerConfig();
    mapMarker = MapMarker.create(position, config);

    this._addMarker(mapMarker);
    this._drawingSync.sendEvent(
      new DrawingSyncEvent<MarkerSyncBody>(
        DrawingSyncEventType.MARKER,
        new MarkerSyncBody(mapMarker.position, config),
        this._localPlayerNickname
      )
    );
    if (this._windowComm) {
      this._windowComm.triggerMarkerAdded(this._normalizeMapMarker(mapMarker));
    }
  }

  private _normalizeMapMarker(marker: MapMarker) {
    return MapMarker.create(
      InGameUtils.normalizeVector(marker.position, this._mapBackground.mapSize),
      marker.config
    );
  }

  private _addMarker(marker: MapMarker) {
    const matchingMarkers = this._placedMarkers.filter(x => x.equals(marker));
    if (!matchingMarkers.isEmpty()) {
      return;
    }

    this._placedMarkers = this._placedMarkers.push(marker);
    this._changeHistory.add(
      new ChangeHistoryEvent(ChangeEventType.CREATE, marker)
    );
    this._redraw = true;
  }

  removeMarker(mapMarker: MapMarker) {
    this._removeMarker(mapMarker);
    if (this._windowComm) {
      this._windowComm.triggerMarkerRemoved(
        this._normalizeMapMarker(mapMarker)
      );
    }
    // TODO: implement change observers for markers when they are done
  }

  private _removeMarker(marker: MapMarker) {
    this._placedMarkers = this._placedMarkers.remove(
      this._placedMarkers.indexOf(marker)
    );
    this._changeHistory.add(
      new ChangeHistoryEvent(ChangeEventType.REMOVE, marker)
    );
    this._redraw = true;
  }

  closestMarker(position: Vector2): MapMarker | null {
    if (this._placedMarkers.size <= 0) {
      return null;
    }

    return this._placedMarkers.reduce(
      (reduction: MapMarker, value: MapMarker) => {
        return value.position.distance(position) <
          reduction.position.distance(position)
          ? value
          : reduction;
      }
    );
  }

  clear() {
    this._placedMarkers = this._placedMarkers.clear();
    this._redraw = true;
  }

  clearContext() {
    this.clear();
    this._subs.forEach(x => {
      x.unsubscribe();
    });
    this._subs = [];
  }

  onDraw(canvasContext: CanvasRenderingContext2D) {
    MarkerDrawingShared.drawMarkers(
      canvasContext,
      this._placedMarkers,
      this._markerResourceMapping
    );
    this._redraw = false;
  }

  onDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  get placedMarkers() {
    return this._placedMarkers;
  }

  get redraw() {
    return this._redraw;
  }
}
