import { LinePath } from "../../ds/line-path";
import { List } from "immutable";
import { CanvasModule } from "../canvas-module.decorator";

export const MAX_CHANGE_HISTORY_SIZE = 10;

@CanvasModule()
export class BrushChangeHistoryCanvasModule {
  private _history = List<LinePath>();
  /**
   * The pointer points on the last change that has been made.
   * Example:
   * - When a new line has been added it will point on that line thus the change pointer will be ``0``.
   * - When we had multiple lines and all have been undone then the pointer will be ``null`` cause
   *   there are no more changes after that.
   * - When we have two lines and {@link undo} has been called then the change pointer will point to the
   *   first line.
   */

  // TODO: custom undo redo for all event types, ie eraser deletes a line | markerd added | eraser deletes a marker etc
  private _changePointer: number | null = null;

  add(path: LinePath) {
    this._history = this._history.push(path);
    if (this._history.size > MAX_CHANGE_HISTORY_SIZE) {
      this._history = this._history.remove(0);
    }

    this._changePointer = this._history.size - 1;
  }

  undo(): LinePath | null {
    if (this._changePointer === null) {
      return null;
    }

    const result = this._history.get(this._changePointer);
    if (this._changePointer > 0) {
      this._changePointer--;
    } else {
      this._changePointer = null;
      this._history = this._history.reverse();
    }

    return result;
  }

  redo(): LinePath | null {
    if (
      !this._history.size ||
      (this._changePointer && this._changePointer + 1 === this._history.size)
    ) {
      return null;
    }
    if (this._changePointer === null) {
      this._changePointer = 0;
    } else {
      this._changePointer++;
    }

    return this._history.get(this._changePointer);
  }

  clear() {
    this._history = this._history.clear();
    this._changePointer = null;
  }

  get history() {
    return this._history;
  }

  get canRedo() {
    return (
      !this._history.isEmpty() &&
      (this._changePointer < this._history.size - 1 ||
        this._changePointer === null)
    );
  }

  get canUndo() {
    return !this._history.isEmpty() && this._changePointer !== null;
  }
}
