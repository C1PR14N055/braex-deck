import { MapMarker } from "../../ds/map-marker";
import { List } from "immutable";
import { Vector2 } from "../../ds/vector2";
import { MapMarkerType } from "../../ds/map-marker-type.enum";
import { CanvasResource } from "../../ds/canvas-resource";

export const MARKER_OFFSET = Vector2.create(0.2, 0.2);
export const MARKER_SIZE = Vector2.create(16, 20);
export const MAX_MARKER_SIZE = Vector2.create(36, 40);

export abstract class MarkerDrawingShared {
  static drawMarkers(
    context: CanvasRenderingContext2D,
    markers: List<MapMarker>,
    markerMapping: Map<MapMarkerType, CanvasResource>
  ) {
    const transform = context.getTransform();
    let size = Vector2.create(
      MARKER_SIZE.x / transform.a,
      MARKER_SIZE.y / transform.a
    );
    if (size.x > MAX_MARKER_SIZE.x) {
      size = Vector2.create(MAX_MARKER_SIZE.x, size.y);
    }
    if (size.y > MAX_MARKER_SIZE.y) {
      size = Vector2.create(size.x, MAX_MARKER_SIZE.y);
    }

    markers.forEach(marker => {
      context.beginPath();
      const position = marker.position
        .subtract(MARKER_OFFSET)
        .subtract(Vector2.create(size.x / 2, size.y));

      context.drawImage(
        markerMapping.get(marker.config.type).image,
        position.x,
        position.y,
        size.x,
        size.y
      );

      context.globalCompositeOperation = "source-atop";
      context.beginPath();
      context.fillStyle = marker.config.color;
      context.rect(position.x - 1, position.y - 1, size.x + 1, size.y + 1);
      context.fill();
      context.closePath();
      context.globalCompositeOperation = "source-over";
    });
  }
}
