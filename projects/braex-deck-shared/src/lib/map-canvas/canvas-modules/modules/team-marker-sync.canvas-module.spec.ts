import { async, fakeAsync, TestBed, tick } from "@angular/core/testing";
import { TeamMarkerSyncCanvasModule } from "./team-marker-sync.canvas-module";
import { DrawingSyncService } from "../../services/drawing-sync.service";
import { FakeDrawingSyncService } from "../../services/testing/fake-drawing-sync.service";
import { Vector2 } from "../../ds/vector2";
import { MarkerSyncBody } from "../../ds/drawing-sync/marker-sync-body";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { DrawingSyncEventType } from "../../ds/drawing-sync/drawing-sync-event-type";
import { MarkerConfig } from "../../ds/marker-config";
import { MapMarkerType } from "../../ds/map-marker-type.enum";
import { Color } from "../../ds/color.enum";
import { FakeOverwolfGameEventsService } from "../../../overwolf/testing/fake-overwolf-game-events.service";
import { OverwolfGameEventsService } from "../../../overwolf/overwolf-game-events.service";
import { GamePhase } from "../../../ds/game/game-phase.enum";

describe("TeamMarkerSyncCanvasModule", () => {
  let module: TeamMarkerSyncCanvasModule;
  let fakeDrawingSync: FakeDrawingSyncService;
  let fakeGameEvents: FakeOverwolfGameEventsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        TeamMarkerSyncCanvasModule,
        { provide: DrawingSyncService, useClass: FakeDrawingSyncService },
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        }
      ]
    });

    module = TestBed.get(TeamMarkerSyncCanvasModule);
    fakeDrawingSync = TestBed.get(DrawingSyncService);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    module.onInit();
  }));

  it("should be created", () => {
    expect(module).toBeTruthy();
  });

  it("should add a marker of a mate when receiving the marker event", fakeAsync(() => {
    expect(module.teamMarkers.isEmpty()).toBeTruthy();

    const teamMate = "Python";
    const event = new DrawingSyncEvent<MarkerSyncBody>(
      DrawingSyncEventType.MARKER,
      new MarkerSyncBody(Vector2.create(23, 534), new MarkerConfig()),
      teamMate
    );
    fakeDrawingSync.triggerMarkerEvent(event);
    tick(400);

    expect(module.teamMarkers.has(teamMate)).toBeTruthy();
  }));

  it("should mark for redraw after receiving a new marker", fakeAsync(() => {
    expect(module.redraw).toBeFalsy();

    const teamMate = "Flutter";
    const event = new DrawingSyncEvent<MarkerSyncBody>(
      DrawingSyncEventType.MARKER,
      new MarkerSyncBody(Vector2.create(95, 43), new MarkerConfig()),
      teamMate
    );
    fakeDrawingSync.triggerMarkerEvent(event);
    tick(400);

    expect(module.redraw).toBeTruthy();
  }));

  it("should reset the redraw after drawing", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");

    const teamMate = "Dart";
    const event = new DrawingSyncEvent<MarkerSyncBody>(
      DrawingSyncEventType.MARKER,
      new MarkerSyncBody(Vector2.create(12, 6), new MarkerConfig()),
      teamMate
    );
    fakeDrawingSync.triggerMarkerEvent(event);
    tick(400);

    expect(module.redraw).toBeTruthy();
    module.onDraw(context);
    expect(module.redraw).toBeFalsy();
  }));

  it("should add use the config which is received from the sync event", fakeAsync(() => {
    const teamMate = "beer";
    const config = new MarkerConfig(MapMarkerType.WEAPON, Color.CERULEAN_BLUE);
    const event = new DrawingSyncEvent<MarkerSyncBody>(
      DrawingSyncEventType.MARKER,
      new MarkerSyncBody(Vector2.create(432), config),
      teamMate
    );
    fakeDrawingSync.triggerMarkerEvent(event);
    tick(400);

    expect(module.teamMarkers.has(teamMate)).toBeTruthy();
    const markers = module.teamMarkers.get(teamMate);
    expect(markers.get(0).config.equals(config)).toBeTruthy();
  }));

  it("should clear the markers when the phase changes to LOBBY and it has been different before", fakeAsync(() => {
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(20);

    const teamMate = "Blegh";
    const event = new DrawingSyncEvent<MarkerSyncBody>(
      DrawingSyncEventType.MARKER,
      new MarkerSyncBody(Vector2.create(23, 534), new MarkerConfig()),
      teamMate
    );
    fakeDrawingSync.triggerMarkerEvent(event);
    tick(400);

    expect(module.teamMarkers.isEmpty()).toBeFalsy();

    fakeGameEvents.triggerPhaseChanged(GamePhase.LOBBY);
    tick(20);

    expect(module.teamMarkers.isEmpty()).toBeTruthy();
  }));

  it("should mark for redraw when the phase changes to LOBBY and it has been different before", fakeAsync(() => {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(20);

    const teamMate = "The Seventh Circle";
    const event = new DrawingSyncEvent<MarkerSyncBody>(
      DrawingSyncEventType.MARKER,
      new MarkerSyncBody(Vector2.create(23, 534), new MarkerConfig()),
      teamMate
    );
    fakeDrawingSync.triggerMarkerEvent(event);
    tick(400);
    module.onDraw(context);

    expect(module.redraw).toBeFalsy();
    fakeGameEvents.triggerPhaseChanged(GamePhase.LOBBY);
    tick(20);
    expect(module.redraw).toBeTruthy();
  }));

  it("should NOT clear the markers when randomly receiving the phase LOBBY", fakeAsync(() => {
    const teamMate = "A match made in heaven";
    const event = new DrawingSyncEvent<MarkerSyncBody>(
      DrawingSyncEventType.MARKER,
      new MarkerSyncBody(Vector2.create(23, 534), new MarkerConfig()),
      teamMate
    );
    fakeDrawingSync.triggerMarkerEvent(event);
    tick(400);

    expect(module.teamMarkers.isEmpty()).toBeFalsy();
    fakeGameEvents.triggerPhaseChanged(GamePhase.LOBBY);
    tick(20);
    expect(module.teamMarkers.isEmpty()).toBeFalsy();
  }));

  afterEach(() => {
    module.onDestroy();
  });
});
