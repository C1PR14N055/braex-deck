import "reflect-metadata";
import { CanvasModuleDraw } from "./canvas-module-hooks";
import { CanvasModule, META_MODULE_Z_INDEX } from "./canvas-module.decorator";
import { TestBed } from "@angular/core/testing";

@CanvasModule({
  zIndex: 1
})
class FakeCanvasModule implements CanvasModuleDraw {
  onDraw(canvasContext: CanvasRenderingContext2D): void {}

  get redraw() {
    return false;
  }
}

@CanvasModule()
class FakeCanvasModule2 implements CanvasModuleDraw {
  onDraw(canvasContext: CanvasRenderingContext2D): void {}

  get redraw() {
    return false;
  }
}

describe("CanvasModule decorator", () => {
  it("should set the z-index on the module when it's defined in the config", () => {
    const module = TestBed.get(FakeCanvasModule);
    expect(Reflect.getMetadata(META_MODULE_Z_INDEX, module.constructor)).toBe(
      1
    );
  });

  it("should NOT set the z-index on the module when it's NOT defined in the config", () => {
    const module = TestBed.get(FakeCanvasModule2);
    expect(
      Reflect.getMetadata(META_MODULE_Z_INDEX, module.constructor)
    ).toBeFalsy();
  });
});
