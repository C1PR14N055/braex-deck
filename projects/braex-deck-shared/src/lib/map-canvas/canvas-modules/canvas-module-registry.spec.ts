import { CanvasModuleRegistry } from "./canvas-module-registry";
import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit,
  CanvasModuleUpdate
} from "./canvas-module-hooks";
import { CanvasModule } from "./canvas-module.decorator";
import { Vector2 } from "../ds/vector2";
import { fakeAsync, TestBed, tick } from "@angular/core/testing";
import { Subscription } from "rxjs";

@CanvasModule()
class FakeInitCanvasModule implements CanvasModuleInit {
  onInit() {}
}

@CanvasModule()
class FakeDestroyCanvasModule implements CanvasModuleDestroy {
  onDestroy() {}
}

@CanvasModule()
class FakeUpdateCanvasModule implements CanvasModuleUpdate {
  onUpdate(mousePosition: Vector2) {}
}

@CanvasModule()
class FakeDrawCanvasModule implements CanvasModuleDraw {
  onDraw(canvasContext: CanvasRenderingContext2D) {}

  get redraw() {
    return true;
  }
}

@CanvasModule()
class FakeCanvasModule {}

describe("CanvasModuleRegistry", () => {
  let registry: CanvasModuleRegistry;
  let subs: Subscription[];

  beforeEach(() => {
    subs = [];
    registry = new CanvasModuleRegistry();
  });

  it("should categorize modules which implement the init hook", () => {
    const module = TestBed.get(FakeInitCanvasModule);
    registry.registerModules(module);
    expect(registry.initializableModules).toContain(module);
  });

  it("should categorize modules which implement the destroy hook", () => {
    const module = TestBed.get(FakeDestroyCanvasModule);
    registry.registerModules(module);
    expect(registry.destroyableModules).toContain(module);
  });

  it("should categorize modules which implement the update hook", () => {
    const module = TestBed.get(FakeUpdateCanvasModule);
    registry.registerModules(module);
    expect(registry.updatableModules).toContain(module);
  });

  it("should categorize modules which implement the draw hook", () => {
    const module = TestBed.get(FakeDrawCanvasModule);
    registry.registerModules(module);
    expect(registry.drawableModules).toContain(module);
  });

  it("should return all modules", () => {
    const modules = [
      TestBed.get(FakeInitCanvasModule),
      TestBed.get(FakeDestroyCanvasModule),
      TestBed.get(FakeUpdateCanvasModule),
      TestBed.get(FakeDrawCanvasModule),
      TestBed.get(FakeCanvasModule)
    ];
    registry.registerModules(...modules);
    modules.forEach(x => {
      expect(registry.allModules).toContain(x);
    });
  });

  it("should notify all observers when a module is registered", fakeAsync(() => {
    let receivedModule: any;
    const sub = registry.moduleRegistered().subscribe(m => {
      receivedModule = m;
    });
    subs.push(sub);

    const module = TestBed.get(FakeCanvasModule);
    registry.registerModules(module);
    tick(500);

    // noinspection JSUnusedAssignment
    expect(receivedModule).toBe(module);
  }));

  afterEach(() => {
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
