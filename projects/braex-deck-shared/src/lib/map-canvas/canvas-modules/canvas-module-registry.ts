import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit,
  CanvasModuleUpdate
} from "./canvas-module-hooks";
import { Set } from "immutable";
import { Observable, Observer } from "rxjs";

export class CanvasModuleRegistry {
  private _allModules = Set();
  private _initializableModules = Set<CanvasModuleInit>();
  private _destroyableModules = Set<CanvasModuleDestroy>();
  private _updatableModules = Set<CanvasModuleUpdate>();
  private _drawableModules = Set<CanvasModuleDraw>();
  private _moduleRegisteredObservers: Observer<any>[] = [];
  private _moduleUnregisteredObservers: Observer<any>[] = [];

  registerModules(...modules: any[]) {
    modules.forEach(module => {
      if ((module as CanvasModuleInit).onInit !== undefined) {
        this._initializableModules = this._initializableModules.add(
          module as CanvasModuleInit
        );
      }
      if ((module as CanvasModuleDestroy).onDestroy !== undefined) {
        this._destroyableModules = this._destroyableModules.add(
          module as CanvasModuleDestroy
        );
      }
      if ((module as CanvasModuleUpdate).onUpdate !== undefined) {
        this._updatableModules = this._updatableModules.add(
          module as CanvasModuleUpdate
        );
      }
      if (
        (module as CanvasModuleDraw).onDraw !== undefined &&
        (module as CanvasModuleDraw).redraw !== undefined
      ) {
        this._drawableModules = this._drawableModules.add(
          module as CanvasModuleDraw
        );
      }

      this._allModules = this._allModules.add(module);
      this._triggerModuleRegistered(module);
    });
  }

  private _triggerModuleRegistered(module: any) {
    this._moduleRegisteredObservers.forEach(x => {
      x.next(module);
    });
  }

  moduleRegistered(): Observable<any> {
    return Observable.create(observer => {
      this._moduleRegisteredObservers.push(observer);
    });
  }

  unregisterModules(...modules: any[]) {
    modules.forEach(module => {
      if ((module as CanvasModuleInit).onInit !== undefined) {
        this._initializableModules = this._initializableModules.remove(
          module as CanvasModuleInit
        );
      }
      if ((module as CanvasModuleDestroy).onDestroy !== undefined) {
        this._destroyableModules = this._destroyableModules.remove(
          module as CanvasModuleDestroy
        );
      }
      if ((module as CanvasModuleUpdate).onUpdate !== undefined) {
        this._updatableModules = this._updatableModules.remove(
          module as CanvasModuleUpdate
        );
      }
      if (
        (module as CanvasModuleDraw).onDraw !== undefined &&
        (module as CanvasModuleDraw).redraw !== undefined
      ) {
        this._drawableModules = this._drawableModules.remove(
          module as CanvasModuleDraw
        );
      }

      this._allModules = this._allModules.remove(module);
    });
  }

  private _triggerModuleUnregistered(module: any) {
    this._moduleUnregisteredObservers.forEach(x => {
      x.next(module);
    });
  }

  moduleUnregistered(): Observable<any> {
    return Observable.create(observer => {
      this._moduleUnregisteredObservers.push(observer);
    });
  }

  unregisterAllModules() {
    this.unregisterModules(...this._allModules.toArray());
  }

  get initializableModules() {
    return this._initializableModules;
  }

  get destroyableModules() {
    return this._destroyableModules;
  }

  get updatableModules() {
    return this._updatableModules;
  }

  get drawableModules() {
    return this._drawableModules;
  }

  get allModules() {
    return this._allModules;
  }
}
