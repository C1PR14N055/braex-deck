export enum ZoomMode {
  NONE = "none",
  ZOOM_IN = "zoom-in",
  ZOOM_OUT = "zoom-out"
}
