import { BrushConfig } from "../../ds/brush-config";
import { Vector2 } from "../../ds/vector2";

export class TeamBrushHolder {
  constructor(
    public playerNickname: string,
    public activeBrushConfig = new BrushConfig(),
    public tempBrushPoints: Vector2[] = []
  ) {}
}
