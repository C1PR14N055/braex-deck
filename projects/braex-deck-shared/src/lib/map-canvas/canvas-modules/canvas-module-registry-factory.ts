import { CanvasModuleRegistry } from "./canvas-module-registry";

export abstract class CanvasModuleRegistryFactory {
  private static _registry: CanvasModuleRegistry;

  static create() {
    if (!this._registry) {
      this._registry = new CanvasModuleRegistry();
    }

    return this._registry;
  }
}
