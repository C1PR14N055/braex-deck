import "reflect-metadata";
import { Injectable } from "@angular/core";

export const META_MODULE_Z_INDEX = "moduleZIndex";

interface CanvasModuleConfig {
  zIndex: number;
}

type Constructor<T = {}> = new (...args: any[]) => T;

export function CanvasModule(config?: CanvasModuleConfig) {
  return function<T extends Constructor>(C: T) {
    @Injectable({ providedIn: "root" })
    class CanvasModule extends C {}

    if (config && config.zIndex !== undefined) {
      Reflect.defineMetadata(META_MODULE_Z_INDEX, config.zIndex, CanvasModule);
    }

    return CanvasModule;
  };
}
