export class CanvasModuleConfigError extends Error {
  constructor(message?: string) {
    super(message);
    Object.setPrototypeOf(this, CanvasModuleConfigError.prototype);
  }
}
