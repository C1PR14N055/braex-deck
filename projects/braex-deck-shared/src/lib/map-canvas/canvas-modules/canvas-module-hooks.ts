import { Vector2 } from "../ds/vector2";

export interface CanvasModuleInit {
  onInit(): void;
}

export interface CanvasModuleDestroy {
  onDestroy(): void;
}

export interface CanvasModuleUpdate {
  onUpdate(mousePosition: Vector2): void;
}

export interface CanvasModuleDraw {
  /**
   * Whether the draw function should be called on the
   * next iteration / changes need to be redrawn.
   *
   * IMPORTANT:
   * This needs to be manually managed by the module so that means
   * after the draw function has been called this variable needs
   * to be reset in order for the draw controller not to
   * unnecessarily redraw the module when it's not needed.
   */
  readonly redraw: boolean;

  onDraw(canvasContext: CanvasRenderingContext2D): void;
}
