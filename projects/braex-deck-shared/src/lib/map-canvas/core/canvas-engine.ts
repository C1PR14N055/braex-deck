import "reflect-metadata";
import { Camera2D } from "../rendering/camera-2d";
import { CanvasRenderController } from "../rendering/canvas-render-controller";
import { CanvasInputController } from "../input/canvas-input-controller";
import { CanvasModuleRegistry } from "../canvas-modules/canvas-module-registry";
import { CanvasModuleRegistryFactory } from "../canvas-modules/canvas-module-registry-factory";
import { Injectable } from "@angular/core";
import { InputControllerService } from "../services/input-controller.service";
import { Vector2 } from "../ds/vector2";
import { Subscription } from "rxjs";
import { MapToolService } from "../services/map-tool.service";

export const INPUT_OVERLAY_CLASS = "ce-input-overlay";

@Injectable({
  providedIn: "root"
})
export class CanvasEngine {
  private _engineHolder: HTMLDivElement;
  private _moduleRegistry: CanvasModuleRegistry;
  private _renderController: CanvasRenderController;
  private _camera: Camera2D;
  private _inputController: CanvasInputController;
  private _inputOverlay: HTMLDivElement;
  private _mousePosition = Vector2.create();
  private _subs: Subscription[] = [];

  constructor(
    private _inputControllerService: InputControllerService,
    private _mapToolService: MapToolService
  ) {}

  /**
   * Attach this engine to an element.
   * @param engineHolder The div element to which this engine should be attached to.
   */
  attach(engineHolder: HTMLDivElement) {
    this._engineHolder = engineHolder;
    this._moduleRegistry = CanvasModuleRegistryFactory.create();
    this._prepareEngineHolder();
    this._initInputOverlay();

    this._camera = new Camera2D(this._inputOverlay);
    this._inputController = new CanvasInputController(
      this._inputOverlay,
      this._camera
    );
    this._initInputEvents();
    this._inputControllerService.bindSourceController(this._inputController);
    this._renderController = new CanvasRenderController(
      this._engineHolder,
      this._camera
    );

    this._initMapToolEvents();
  }

  private _prepareEngineHolder() {
    this._engineHolder.style.position = "relative";
  }

  private _initInputOverlay() {
    this._inputOverlay = document.createElement("div");
    this._inputOverlay.className = INPUT_OVERLAY_CLASS;
    this._inputOverlay.style.position = "absolute";
    this._inputOverlay.style.top = "0";
    this._inputOverlay.style.left = "0";
    this._inputOverlay.style.width = "100%";
    this._inputOverlay.style.height = "100%";
    this._inputOverlay.style.zIndex = "400";
    this._inputOverlay.style.cursor = "default";
    this._engineHolder.appendChild(this._inputOverlay);

    this._inputOverlay.addEventListener("contextmenu", event => {
      event.preventDefault();
    });
  }

  private _initInputEvents() {
    const sub = this._inputController.mouseMove().subscribe(position => {
      this._mousePosition = position;
    });
    this._subs.push(sub);
  }

  private _initMapToolEvents() {
    const sub = this._mapToolService.activeToolChanged().subscribe(() => {
      this._setCurrentCursor();
    });
    this._subs.push(sub);
  }

  private _setCurrentCursor() {
    const toolConfig = this._mapToolService.getToolConfig(
      this._mapToolService.activeTool
    );
    if (toolConfig) {
      this._inputOverlay.style.cursor = toolConfig.cursor || "default";
    } else {
      this._inputOverlay.style.cursor = "default";
    }
  }

  start() {
    this._moduleRegistry.initializableModules.forEach(module => {
      module.onInit();
    });
    this._update();
  }

  private _update() {
    this._camera.update(this._mousePosition);
    this._moduleRegistry.updatableModules.forEach(module => {
      module.onUpdate(this._mousePosition);
    });
    this._renderController.draw();

    requestAnimationFrame(() => this._update());
  }

  destroy() {
    this._inputControllerService.unbindSourceController();
    this._subs.forEach(x => {
      x.unsubscribe();
    });
    this._moduleRegistry.destroyableModules.forEach(module => {
      module.onDestroy();
    });
    this._renderController.destroy();
    this._camera.destroy();
    this._engineHolder.removeChild(this._inputOverlay);
  }

  get camera() {
    return this._camera;
  }
}
