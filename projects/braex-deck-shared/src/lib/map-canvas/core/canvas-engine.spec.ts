import "reflect-metadata";
import { CanvasEngine, INPUT_OVERLAY_CLASS } from "./canvas-engine";
import {
  CanvasModuleDestroy,
  CanvasModuleDraw,
  CanvasModuleInit,
  CanvasModuleUpdate
} from "../canvas-modules/canvas-module-hooks";
import { CanvasModule } from "../canvas-modules/canvas-module.decorator";
import { CanvasModuleRegistryFactory } from "../canvas-modules/canvas-module-registry-factory";
import { CanvasModuleRegistry } from "../canvas-modules/canvas-module-registry";
import { fakeAsync, TestBed, tick } from "@angular/core/testing";
import { Vector2 } from "../ds/vector2";
import { MapToolService } from "../services/map-tool.service";
import { MapCanvasTool } from "../ds/map-canvas-tool.enum";

@CanvasModule()
class FakeModule1 implements CanvasModuleDraw {
  onDraw(canvasContext: CanvasRenderingContext2D): void {}

  get redraw() {
    return true;
  }
}

@CanvasModule()
class FakeModule2 implements CanvasModuleDraw {
  onDraw(canvasContext: CanvasRenderingContext2D): void {}

  get redraw() {
    return true;
  }
}

@CanvasModule()
class FakeModule3 implements CanvasModuleDestroy {
  onDestroy() {}
}

@CanvasModule()
class FakeModule4 implements CanvasModuleInit {
  onInit() {}
}

@CanvasModule()
class FakeModule5 implements CanvasModuleUpdate {
  onUpdate(mousePosition: Vector2) {}
}

describe("CanvasEngine", () => {
  let engineHolder: HTMLDivElement;
  let engine: CanvasEngine;
  const engineHolderId = "test-engine-holder";
  let registry: CanvasModuleRegistry;
  let registryFactoryCreate: jasmine.Spy;
  let mapTool: MapToolService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanvasEngine, MapToolService]
    });

    mapTool = TestBed.get(MapToolService);
    engineHolder = document.createElement("div");
    engineHolder.id = engineHolderId;
    document.body.appendChild(engineHolder);

    registry = new CanvasModuleRegistry();
    registry.registerModules(
      TestBed.get(FakeModule1),
      TestBed.get(FakeModule2),
      TestBed.get(FakeModule3),
      TestBed.get(FakeModule4),
      TestBed.get(FakeModule5)
    );
    registryFactoryCreate = spyOn(
      CanvasModuleRegistryFactory,
      "create"
    ).and.returnValue(registry);

    engine = TestBed.get(CanvasEngine);
    engine.attach(engineHolder);
  });

  it("should be created", () => {
    expect(engine).toBeTruthy();
  });

  it("should create the input overlay", () => {
    const el = engineHolder.querySelector(`.${INPUT_OVERLAY_CLASS}`);
    expect(el).toBeTruthy();
  });

  it("should call the init hook when starting the engine", () => {
    const spy = spyOn(TestBed.get(FakeModule4), "onInit");
    engine.start();
    expect(spy).toHaveBeenCalled();
  });

  it("should remove the input overlay when destroying the engine", () => {
    expect(engineHolder.querySelector(`.${INPUT_OVERLAY_CLASS}`)).toBeTruthy();
    engine.destroy();
    expect(engineHolder.querySelector(`.${INPUT_OVERLAY_CLASS}`)).toBeFalsy();
  });

  it("should call the destroy hook on each module when destroying the engine", () => {
    const spy = spyOn(TestBed.get(FakeModule3), "onDestroy");
    engine.destroy();
    expect(spy).toHaveBeenCalled();
  });

  it("should start the update loop when starting the engine", () => {
    const spy = spyOn(TestBed.get(FakeModule5), "onUpdate");
    engine.start();
    expect(spy).toHaveBeenCalled();
  });

  it("should set the cursor of the input overlay to the one of the active tool", fakeAsync(() => {
    const el = engineHolder.querySelector(
      `.${INPUT_OVERLAY_CLASS}`
    ) as HTMLElement;
    expect(el.style.cursor).toEqual("default");

    mapTool.activeTool = MapCanvasTool.MARKER;
    tick(300);
    expect(el.style.cursor).toEqual("crosshair");
  }));

  it("should set the cursor to default when switching for the MARKER tool to the SELECTION tool", fakeAsync(() => {
    const el = engineHolder.querySelector(
      `.${INPUT_OVERLAY_CLASS}`
    ) as HTMLElement;
    mapTool.activeTool = MapCanvasTool.MARKER;
    tick(300);
    expect(el.style.cursor).toEqual("crosshair");

    mapTool.activeTool = MapCanvasTool.SELECTION;
    tick(300);
    expect(el.style.cursor).toEqual("default");
  }));
});
