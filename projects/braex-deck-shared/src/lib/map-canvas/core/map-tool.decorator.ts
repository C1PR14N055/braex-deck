import { ToolConfig } from "../ds/tool-config";
import { ToolRegistryService } from "../services/tool-registry.service";

export function MapTool(config: ToolConfig) {
  return function(target) {
    ToolRegistryService.registerTool(config);
  };
}
