import { ToolRegistryService } from "../services/tool-registry.service";
import { MapCanvasTool } from "../ds/map-canvas-tool.enum";

describe("MapToolDecorator", () => {
  it("should register a BRUSH tool to the registry", () => {
    const config = ToolRegistryService.getToolConfig(MapCanvasTool.BRUSH);
    expect(config).toBeTruthy();
    expect(config.tool).toBe(MapCanvasTool.BRUSH);
  });

  it("should return null when a tool is not in the registry", () => {
    const config = ToolRegistryService.getToolConfig(MapCanvasTool.SELECTION);
    expect(config).toBeNull();
  });
});
