import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MapCanvasComponent } from "./map-canvas/map-canvas.component";
import { OverwolfCoreService } from "../overwolf/overwolf-core.service";
import { HttpClientModule } from "@angular/common/http";
import { ZoomIndicatorComponent } from "./zoom-indicator/zoom-indicator.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { MatRippleModule } from "@angular/material";
import { ResizeObserverService } from "../services/resize-observer.service";
import { MapImageNamePipe } from "./pipes/map-image-name.pipe";
import { MapNameModule } from "../map-name/map-name.module";
import { MapMarkerIconModule } from "../map-marker-icon/map-marker-icon.module";

@NgModule({
  declarations: [MapCanvasComponent, ZoomIndicatorComponent, MapImageNamePipe],
  imports: [
    CommonModule,
    HttpClientModule,
    FontAwesomeModule,
    MatRippleModule,
    MapNameModule,
    MapMarkerIconModule
  ],
  exports: [MapCanvasComponent],
  providers: [OverwolfCoreService, ResizeObserverService]
})
export class MapCanvasModule {}
