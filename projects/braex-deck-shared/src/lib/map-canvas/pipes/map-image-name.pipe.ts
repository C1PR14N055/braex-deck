import { Pipe, PipeTransform } from "@angular/core";
import { PubgMap } from "../../ds/game/pubg-map.enum";
import { MappingError } from "../../error/mapping.error";

@Pipe({
  name: "mapImageName"
})
export class MapImageNamePipe implements PipeTransform {
  transform(value: PubgMap, args?: any) {
    switch (value) {
      case PubgMap.DESERT_MAIN:
        return "desert-main.jpg";
      case PubgMap.DIHOROTOK_MAIN:
        return "dihorotok-main.jpg";
      case PubgMap.ERANGEL_MAIN:
        return "erangel-main.jpg";
      case PubgMap.RANGE_MAIN:
        return "range-main.jpg";
      case PubgMap.SAVAGE_MAIN:
        return "savage-main.jpg";
    }

    throw new MappingError(`Unknown map ${value}`);
  }
}
