import { fakeAsync, tick } from "@angular/core/testing";
import {
  Camera2D,
  DEFAULT_ZOOM,
  DRAG_MOUSE_BUTTON,
  MAX_ZOOM,
  MIN_ZOOM,
  ZOOM_STEP_SIZE
} from "./camera-2d";
import { Matrix2D } from "../ds/matrix-2d";
import { Subscription } from "rxjs";
import { FakeInputController } from "../input/testing/fake-input-controller";
import { BaseInputControllerFactory } from "../input/base-input-controller-factory";
import { Vector2 } from "../ds/vector2";
import { MouseWheel } from "../ds/events/mouse-wheel.enum";
import { ZoomMode } from "../canvas-modules/ds/zoom-mode.enum";

describe("Camera2D", () => {
  let camera: Camera2D;
  let cameraOverlay: HTMLDivElement;
  let fakeInputController: FakeInputController;
  let subs: Subscription[];

  beforeEach(() => {
    subs = [];
    cameraOverlay = document.createElement("div");
    cameraOverlay.style.width = "900px";
    cameraOverlay.style.height = "900px";
    cameraOverlay.style.position = "absolute";
    document.body.appendChild(cameraOverlay);

    fakeInputController = new FakeInputController();
    spyOn(BaseInputControllerFactory, "create").and.returnValue(
      fakeInputController
    );

    camera = new Camera2D(cameraOverlay);
  });

  it("should notify all observers when the transform of the camera changes", fakeAsync(() => {
    const transform = Matrix2D.create(10, 20, 9, 4, 6, 2);

    let receivedTransform: Matrix2D;
    const sub = camera.transformChanged().subscribe(t => {
      receivedTransform = t;
    });
    subs.push(sub);

    camera.transform = transform;
    tick(300);
    expect(receivedTransform.equals(transform)).toBeTruthy();
  }));

  it("should zoom in when scrolling forward", fakeAsync(() => {
    expect(camera.zoom.isEqualTo(DEFAULT_ZOOM)).toBeTruthy();
    fakeInputController.triggerMouseWheel({
      position: Vector2.create(),
      wheel: MouseWheel.ZOOM_IN
    });
    tick(200);
    camera.update(Vector2.create());

    expect(
      camera.zoom.isEqualTo(DEFAULT_ZOOM.plus(ZOOM_STEP_SIZE))
    ).toBeTruthy();
  }));

  it("should zoom out when scrolling backward", fakeAsync(() => {
    expect(camera.zoom.isEqualTo(DEFAULT_ZOOM)).toBeTruthy();

    camera.zoomIn(Vector2.create());
    camera.update(Vector2.create());
    expect(
      camera.zoom.isEqualTo(DEFAULT_ZOOM.plus(ZOOM_STEP_SIZE))
    ).toBeTruthy();

    fakeInputController.triggerMouseWheel({
      position: Vector2.create(),
      wheel: MouseWheel.ZOOM_OUT
    });
    tick(200);
    camera.update(Vector2.create());
    expect(camera.zoom.isEqualTo(DEFAULT_ZOOM)).toBeTruthy();
  }));

  it("should not be possible to zoom out under the MIN zoom", fakeAsync(() => {
    expect(camera.zoom.isEqualTo(DEFAULT_ZOOM)).toBeTruthy();
    const loopSize =
      (DEFAULT_ZOOM.toNumber() - MIN_ZOOM.toNumber()) *
      ZOOM_STEP_SIZE.toNumber() *
      100;
    for (let i = 0; i < loopSize; i++) {
      fakeInputController.triggerMouseWheel({
        position: Vector2.create(),
        wheel: MouseWheel.ZOOM_OUT
      });
      tick(200);
      camera.update(Vector2.create());
    }

    expect(camera.zoom.isEqualTo(MIN_ZOOM)).toBeTruthy();
    fakeInputController.triggerMouseWheel({
      position: Vector2.create(),
      wheel: MouseWheel.ZOOM_OUT
    });
    tick(200);
    camera.update(Vector2.create());
    expect(camera.zoom.isEqualTo(MIN_ZOOM)).toBeTruthy();
  }));

  it("should not be possible to zoom in over the MAX zoom", fakeAsync(() => {
    expect(camera.zoom.isEqualTo(DEFAULT_ZOOM)).toBeTruthy();
    const loopSize =
      (MAX_ZOOM.toNumber() - DEFAULT_ZOOM.toNumber()) *
      ZOOM_STEP_SIZE.toNumber() *
      100;
    for (let i = 0; i < loopSize; i++) {
      fakeInputController.triggerMouseWheel({
        position: Vector2.create(),
        wheel: MouseWheel.ZOOM_IN
      });
      tick(200);
      camera.update(Vector2.create());
    }

    expect(camera.zoom.isEqualTo(MAX_ZOOM)).toBeTruthy();
    fakeInputController.triggerMouseWheel({
      position: Vector2.create(),
      wheel: MouseWheel.ZOOM_IN
    });
    tick(200);
    camera.update(Vector2.create());
    expect(camera.zoom.isEqualTo(MAX_ZOOM)).toBeTruthy();
  }));

  it("should set the zoom target to the mouse position when zooming in", fakeAsync(() => {
    const position = Vector2.create(200, 300);
    expect(camera.zoomTarget).toBeNull();

    fakeInputController.triggerMouseWheel({
      position: position,
      wheel: MouseWheel.ZOOM_IN
    });
    tick(200);
    camera.update(position);

    expect(camera.zoomTarget.equals(position)).toBeTruthy();
  }));

  it("should set the zoom target to the mouse position when zooming out", fakeAsync(() => {
    const position = Vector2.create(900, 500);
    expect(camera.zoomTarget).toBeNull();
    camera.zoomIn(Vector2.create());
    camera.update(Vector2.create());
    expect(
      camera.zoom.isEqualTo(DEFAULT_ZOOM.plus(ZOOM_STEP_SIZE))
    ).toBeTruthy();

    fakeInputController.triggerMouseWheel({
      position: position,
      wheel: MouseWheel.ZOOM_OUT
    });
    tick(200);
    camera.update(position);

    expect(camera.zoom.isEqualTo(DEFAULT_ZOOM)).toBeTruthy();
    expect(camera.zoomTarget.equals(position)).toBeTruthy();
  }));

  it("should compute the zoom offset correctly when the mouse stays at the same position while zooming in", fakeAsync(() => {
    const position = Vector2.create(500, 300);
    expect(camera.zoomTarget).toBeNull();
    fakeInputController.triggerMouseWheel({
      position: position,
      wheel: MouseWheel.ZOOM_IN
    });
    tick(200);
    camera.update(position);

    let expectedOffset = Vector2.create(-200, -120);
    expect(camera.zoomTarget.equals(position)).toBeTruthy();
    expect(camera.offset.x).toBeCloseTo(expectedOffset.x);
    expect(camera.offset.y).toBeCloseTo(expectedOffset.y);
    fakeInputController.triggerMouseWheel({
      position: position,
      wheel: MouseWheel.ZOOM_IN
    });
    tick(200);
    camera.update(position);

    expectedOffset = Vector2.create(-400, -240);
    expect(camera.zoomTarget.equals(position)).toBeTruthy();
    expect(camera.offset.x).toBeCloseTo(expectedOffset.x);
    expect(camera.offset.y).toBeCloseTo(expectedOffset.y);
  }));

  it("should compute the zoom offset correctly when the mouse stays at the same position while zooming out", fakeAsync(() => {
    for (let i = 0; i < 5; i++) {
      camera.zoomIn(Vector2.create());
      camera.update(Vector2.create());
    }

    const position = Vector2.create(200, 900);
    expect(camera.zoomTarget.equals(Vector2.create())).toBeTruthy();
    fakeInputController.triggerMouseWheel({
      position: position,
      wheel: MouseWheel.ZOOM_OUT
    });
    tick(200);
    camera.update(position);

    let expectedOffset = Vector2.create(80, 360);
    expect(camera.zoomTarget.equals(position)).toBeTruthy();
    expect(camera.offset.x).toBeCloseTo(expectedOffset.x);
    expect(camera.offset.y).toBeCloseTo(expectedOffset.y);

    fakeInputController.triggerMouseWheel({
      position: position,
      wheel: MouseWheel.ZOOM_OUT
    });
    tick(200);
    camera.update(position);

    expectedOffset = Vector2.create(160, 720);
    expect(camera.zoomTarget.equals(position)).toBeTruthy();
    expect(camera.offset.x).toBeCloseTo(expectedOffset.x);
    expect(camera.offset.y).toBeCloseTo(expectedOffset.y);
  }));

  it("should compute the zoom offset correctly when changing the mouse position on each zoom in", fakeAsync(() => {
    const points = [
      Vector2.create(200, 300),
      Vector2.create(150, 275),
      Vector2.create(290, 400)
    ];
    expect(camera.zoom.isEqualTo(DEFAULT_ZOOM)).toBeTruthy();

    const expectedOffsets = [
      Vector2.create(-80, -120),
      Vector2.create(-140, -230),
      Vector2.create(-256, -390)
    ];
    points.forEach((point, i) => {
      fakeInputController.triggerMouseWheel({
        position: point,
        wheel: MouseWheel.ZOOM_IN
      });
      tick(200);
      camera.update(point);

      const expected = expectedOffsets[i];
      expect(camera.offset.x).toBeCloseTo(expected.x);
      expect(camera.offset.y).toBeCloseTo(expected.y);
    });
  }));

  it("should compute the zoom offset correctly when changing the mouse position on each zoom out", fakeAsync(() => {
    for (let i = 0; i < 5; i++) {
      camera.zoomIn(Vector2.create());
      camera.update(Vector2.create());
    }

    const points = [
      Vector2.create(456, 856),
      Vector2.create(46, 163),
      Vector2.create(931, 124)
    ];
    expect(
      camera.zoom.isEqualTo(DEFAULT_ZOOM.plus(ZOOM_STEP_SIZE.multipliedBy(5)))
    ).toBeTruthy();

    const expectedOffsets = [
      Vector2.create(182.4, 342.4),
      Vector2.create(200.8, 407.6),
      Vector2.create(573.2, 457.2)
    ];
    points.forEach((point, i) => {
      fakeInputController.triggerMouseWheel({
        position: point,
        wheel: MouseWheel.ZOOM_OUT
      });
      tick(200);
      camera.update(point);

      const expected = expectedOffsets[i];
      expect(camera.offset.x).toBeCloseTo(expected.x);
      expect(camera.offset.y).toBeCloseTo(expected.y);
    });
  }));

  it("should set the zoom target to the given position when calling #zoomIn", () => {
    const position = Vector2.create(42, 90);
    expect(camera.zoomTarget).toBeNull();
    camera.zoomIn(position);

    expect(camera.zoomTarget.equals(position)).toBeTruthy();
  });

  it("should set the zoom target to the given position when calling #zoomOut", () => {
    camera.zoomIn(Vector2.create());
    camera.update(Vector2.create());

    const position = Vector2.create(42, 563);
    expect(camera.zoomTarget.equals(Vector2.create())).toBeTruthy();
    camera.zoomOut(position);

    expect(camera.zoomTarget.equals(position)).toBeTruthy();
  });

  it("should zoom in when calling #zoomIn", fakeAsync(() => {
    const position = Vector2.create(93, 496);
    expect(camera.zoom.isEqualTo(DEFAULT_ZOOM)).toBeTruthy();

    camera.zoomIn(position);
    camera.update(position);

    expect(
      camera.zoom.isEqualTo(DEFAULT_ZOOM.plus(ZOOM_STEP_SIZE))
    ).toBeTruthy();
  }));

  it("should zoom out when calling #zoomOut", fakeAsync(() => {
    for (let i = 0; i < 5; i++) {
      camera.zoomIn(Vector2.create());
      camera.update(Vector2.create());
    }

    const position = Vector2.create(596, 453);
    expect(
      camera.zoom.isEqualTo(DEFAULT_ZOOM.plus(ZOOM_STEP_SIZE.multipliedBy(5)))
    ).toBeTruthy();

    camera.zoomOut(position);
    camera.update(position);
    expect(
      camera.zoom.isEqualTo(DEFAULT_ZOOM.plus(ZOOM_STEP_SIZE.multipliedBy(4)))
    ).toBeTruthy();
  }));

  it("should activate the dragging when pressing the DRAG_MOUSE_BUTTON", fakeAsync(() => {
    expect(camera.dragging).toBeFalsy();
    fakeInputController.triggerMouseDown({
      position: Vector2.create(500, 500),
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(200);
    expect(camera.dragging).toBeTruthy();
  }));

  it("should deactivate the dragging when releasing the DRAG_MOUSE_BUTTON", fakeAsync(() => {
    expect(camera.dragging).toBeFalsy();
    fakeInputController.triggerMouseDown({
      position: Vector2.create(500, 500),
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(200);
    expect(camera.dragging).toBeTruthy();

    fakeInputController.triggerMouseUp({
      position: Vector2.create(500, 500),
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(200);
    expect(camera.dragging).toBeFalsy();
  }));

  it("should offset the canvas to the left", fakeAsync(() => {
    const points = [
      Vector2.create(500, 500),
      Vector2.create(510, 500),
      Vector2.create(520, 500),
      Vector2.create(530, 500)
    ];

    fakeInputController.triggerMouseDown({
      position: points[0],
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(200);
    expect(camera.offset.equals(Vector2.create())).toBeTruthy();
    _offsetCameraOnX(points);
  }));

  function _offsetCameraOnX(points: Vector2[]) {
    const first = points[0];
    let base = camera.offset;
    points.forEach(point => {
      camera.update(point);
      tick(200);

      const expected = base.add(Vector2.create(first.diff(point).x, 0));
      expect(camera.offset.x).toBeCloseTo(expected.x);
      expect(camera.offset.y).toBeCloseTo(expected.y);
      base = camera.offset;
    });
  }

  it("should offset the canvas to the right", fakeAsync(() => {
    const points = [
      Vector2.create(500, 500),
      Vector2.create(490, 500),
      Vector2.create(480, 500),
      Vector2.create(470, 500)
    ];
    fakeInputController.triggerMouseDown({
      mouseButton: DRAG_MOUSE_BUTTON,
      position: points[0]
    });
    tick(200);
    expect(camera.offset.equals(Vector2.create())).toBeTruthy();
    _offsetCameraOnX(points);
  }));

  it("should offset the canvas to the top", fakeAsync(() => {
    const points = [
      Vector2.create(500, 500),
      Vector2.create(500, 490),
      Vector2.create(500, 480),
      Vector2.create(500, 470)
    ];
    fakeInputController.triggerMouseDown({
      position: points[0],
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(200);
    expect(camera.offset.equals(Vector2.create())).toBeTruthy();
    _offsetCameraOnY(points);
  }));

  function _offsetCameraOnY(points: Vector2[]) {
    const first = points[0];
    let base = camera.offset;
    points.forEach(point => {
      camera.update(point);
      tick(200);

      const expected = base.add(Vector2.create(0, first.diff(point).y));
      expect(camera.offset.x).toBeCloseTo(expected.x);
      expect(camera.offset.y).toBeCloseTo(expected.y);
      base = camera.offset;
    });
  }

  it("should offset the canvas to the bottom", fakeAsync(() => {
    const points = [
      Vector2.create(500, 500),
      Vector2.create(500, 510),
      Vector2.create(500, 520),
      Vector2.create(500, 530)
    ];
    fakeInputController.triggerMouseDown({
      position: points[0],
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(200);
    expect(camera.offset.equals(Vector2.create())).toBeTruthy();
    _offsetCameraOnY(points);
  }));

  it("should continue the dragging on the previous offset", fakeAsync(() => {
    const point1 = Vector2.create(500, 500);
    const point2 = Vector2.create(540, 510);
    const point3 = Vector2.create(420, 310);

    fakeInputController.triggerMouseDown({
      position: point1,
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(200);
    camera.update(point1);
    expect(camera.dragging).toBeTruthy();

    camera.update(point2);
    tick(200);
    let expected = point2.subtract(point1);
    expect(camera.offset.x).toBeCloseTo(expected.x);
    expect(camera.offset.y).toBeCloseTo(expected.y);

    fakeInputController.triggerMouseUp({
      position: point2,
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(200);
    expect(camera.dragging).toBeFalsy();

    fakeInputController.triggerMouseDown({
      position: point3,
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(200);
    camera.update(point3);
    tick(200);
    expect(camera.dragging).toBeTruthy();

    expected = point2.subtract(point1);
    expect(camera.offset.x).toBeCloseTo(expected.x);
    expect(camera.offset.y).toBeCloseTo(expected.y);
  }));

  it("should set the zoom mode to ZOOM IN when ZOOMING IN", fakeAsync(() => {
    expect(camera.zoomMode).toEqual(ZoomMode.NONE);
    fakeInputController.triggerMouseWheel({
      position: Vector2.create(),
      wheel: MouseWheel.ZOOM_IN
    });
    tick(200);
    expect(camera.zoomMode).toEqual(ZoomMode.ZOOM_IN);
  }));

  it("should set to zoom mode to NONE after the ZOOM IN occurred", fakeAsync(() => {
    expect(camera.zoomMode).toEqual(ZoomMode.NONE);
    fakeInputController.triggerMouseWheel({
      position: Vector2.create(),
      wheel: MouseWheel.ZOOM_IN
    });
    tick(200);
    camera.update(Vector2.create());
    expect(camera.zoomMode).toEqual(ZoomMode.NONE);
  }));

  it("should set the zoom mode to ZOOM OUT when ZOOMING OUT", fakeAsync(() => {
    expect(camera.zoomMode).toEqual(ZoomMode.NONE);
    fakeInputController.triggerMouseWheel({
      position: Vector2.create(),
      wheel: MouseWheel.ZOOM_OUT
    });
    tick(200);
    expect(camera.zoomMode).toEqual(ZoomMode.ZOOM_OUT);
  }));

  it("should set the zoom mode to NONE after the ZOOM OUT occurred", fakeAsync(() => {
    expect(camera.zoomMode).toEqual(ZoomMode.NONE);
    fakeInputController.triggerMouseWheel({
      position: Vector2.create(),
      wheel: MouseWheel.ZOOM_OUT
    });
    tick(200);
    camera.update(Vector2.create());
    expect(camera.zoomMode).toEqual(ZoomMode.NONE);
  }));

  it("should only recompute the camera transform once when calling #zoomIn and dragging the camera", fakeAsync(() => {
    let called = 0;
    const sub = camera.transformChanged().subscribe(() => {
      called++;
    });
    subs.push(sub);

    const position = Vector2.create(42, 69);
    camera.zoomIn(position);
    fakeInputController.triggerMouseDown({
      position: position,
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(300);

    expect(called).toEqual(0);
    camera.update(position);
    expect(called).toEqual(1);
  }));

  it("should only recompute the camera transform once when calling #zoomOut and dragging the camera", fakeAsync(() => {
    camera.zoomIn(Vector2.create());
    camera.update(Vector2.create());

    let called = 0;
    const sub = camera.transformChanged().subscribe(() => {
      called++;
    });
    subs.push(sub);

    const position = Vector2.create(420, 4);
    camera.zoomOut(position);
    fakeInputController.triggerMouseDown({
      position: position,
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(300);

    expect(called).toEqual(0);
    camera.update(position);
    expect(called).toEqual(1);
  }));

  it("should only recompute the camera transform once when zooming in and dragging the camera", fakeAsync(() => {
    let called = 0;
    const sub = camera.transformChanged().subscribe(() => {
      called++;
    });
    subs.push(sub);

    const position = Vector2.create(12, 54);
    fakeInputController.triggerMouseWheel({
      position: position,
      wheel: MouseWheel.ZOOM_IN
    });
    tick(100);

    fakeInputController.triggerMouseDown({
      position: position,
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(300);

    expect(called).toEqual(0);
    camera.update(position);
    expect(called).toEqual(1);
  }));

  it("should only recompute the camera transform once when zooming out and dragging the camera", fakeAsync(() => {
    camera.zoomIn(Vector2.create());
    camera.update(Vector2.create());

    let called = 0;
    const sub = camera.transformChanged().subscribe(() => {
      called++;
    });
    subs.push(sub);

    const position = Vector2.create(73, 325);
    fakeInputController.triggerMouseWheel({
      position: position,
      wheel: MouseWheel.ZOOM_OUT
    });
    tick(100);

    fakeInputController.triggerMouseDown({
      position: position,
      mouseButton: DRAG_MOUSE_BUTTON
    });
    tick(300);

    expect(called).toEqual(0);
    camera.update(position);
    expect(called).toEqual(1);
  }));

  it("should return the size of it's target", () => {
    expect(camera.width).toEqual(cameraOverlay.offsetWidth);
    expect(camera.height).toEqual(cameraOverlay.offsetHeight);
  });

  it("should zoom to the center of the camera when calling #zoomIn without a zoom target", () => {
    const rect = cameraOverlay.getBoundingClientRect();

    camera.zoomIn();
    camera.update(Vector2.create());

    const center = camera.documentToWorldPosition(
      Vector2.create(rect.left + camera.width / 2, rect.top + camera.height / 2)
    );
    expect(camera.zoomTarget.x).toBeCloseTo(center.x);
    expect(camera.zoomTarget.y).toBeCloseTo(center.y);
  });

  it("should zoom of of the center of the camera when calling #zoomOut without a zoom target", () => {
    const rect = cameraOverlay.getBoundingClientRect();

    const zoomInVector = Vector2.create(123, 12342);
    camera.zoomIn(zoomInVector);
    camera.update(zoomInVector);

    camera.zoomOut();
    camera.update(Vector2.create());
    const center = camera.documentToWorldPosition(
      Vector2.create(rect.left + camera.width / 2, rect.top + camera.height / 2)
    );
    expect(camera.zoomTarget.x).toBeCloseTo(center.x);
    expect(camera.zoomTarget.y).toBeCloseTo(center.y);
  });

  it("should focus the given position which is in the cameras viewport", () => {
    expect(camera.offset.equals(Vector2.create())).toBeTruthy();
    const position = Vector2.create(100, 100);
    camera.focusPosition(position);

    const expectedOffset = Vector2.create(350, 350);
    expect(camera.offset.x).toBeCloseTo(expectedOffset.x);
    expect(camera.offset.y).toBeCloseTo(expectedOffset.y);
  });

  it("should focus on the given position which is larger than the cameras viewport", () => {
    expect(camera.offset.equals(Vector2.create())).toBeTruthy();
    const position = Vector2.create(camera.width + 200, camera.height + 200);
    camera.focusPosition(position);

    const expectedOffset = Vector2.create(-650, -650);
    expect(camera.offset.x).toBeCloseTo(expectedOffset.x);
    expect(camera.offset.y).toBeCloseTo(expectedOffset.y);
  });

  it("should stay in the same position when focusing on the middle of the camera", () => {
    expect(camera.offset.equals(Vector2.create())).toBeTruthy();
    camera.focusPosition(Vector2.create(camera.width / 2, camera.height / 2));

    expect(camera.offset.x).toBeCloseTo(0);
    expect(camera.offset.y).toBeCloseTo(0);
  });

  it("should recompute the transform after focusing a point", fakeAsync(() => {
    let called = 0;
    const sub = camera.transformChanged().subscribe(() => {
      called++;
    });
    subs.push(sub);

    camera.update(Vector2.create());
    expect(called).toEqual(0);

    camera.focusPosition(Vector2.create(200, 900));
    camera.update(Vector2.create());
    expect(called).toEqual(1);
  }));

  afterEach(() => {
    subs.forEach(x => {
      x.unsubscribe();
    });
    camera.destroy();
  });
});
