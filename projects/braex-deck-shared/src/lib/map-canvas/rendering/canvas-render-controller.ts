import { Camera2D } from "./camera-2d";
import { CanvasModuleRegistry } from "../canvas-modules/canvas-module-registry";
import { CanvasModuleRegistryFactory } from "../canvas-modules/canvas-module-registry-factory";
import { CanvasModuleDraw } from "../canvas-modules/canvas-module-hooks";
import { orderBy } from "lodash";
import { META_MODULE_Z_INDEX } from "../canvas-modules/canvas-module.decorator";
import { Subscription } from "rxjs";
import * as ElementResizeFactory from "element-resize-detector";

export const MODULE_CANVAS_CLASS = "ce-module-canvas";
export const TEMP_CANVAS_CLASS = "ce-temp-canvas";
export const META_MODULE_CANVAS = "moduleCanvas";
export const META_MODULE_CANVAS_CTX = "moduleCanvasContext";
export const ATTR_MODULE_NAME = "ce-module-name";

export class CanvasRenderController {
  private _moduleRegistry: CanvasModuleRegistry;
  private _tempCanvas: HTMLCanvasElement;
  private _tempCanvasContext: CanvasRenderingContext2D;
  /**
   * A list of {@link CanvasModuleDraw} which are going to be rendered sorted by the z-index
   * defined in the canvas module config.
   */
  private _drawableModules: CanvasModuleDraw[] = [];
  private _subs: Subscription[] = [];
  private _forceRedraw = false;

  constructor(
    private _canvasesHolder: HTMLDivElement,
    private _camera: Camera2D
  ) {
    this._moduleRegistry = CanvasModuleRegistryFactory.create();
    this._initDummyCanvas();
    this._initTempCanvas();
    this._initCanvasModules();
    this._initCameraEvents();
    this._initCanvasResizeObservers();
  }

  /**
   * Init a canvas which is kind of not necessary but without
   * it the canvas context of the other modules will have
   * some weird behavior e.g. right after setting the transform
   * of the canvas it's not the one that has been set.
   */
  private _initDummyCanvas() {
    const el = document.createElement("canvas");
    this._canvasesHolder.appendChild(el);
  }

  private _initTempCanvas() {
    this._tempCanvas = document.createElement("canvas");
    this._tempCanvas.className = TEMP_CANVAS_CLASS;
    this._tempCanvas.style.position = "absolute";
    this._tempCanvas.style.top = "0";
    this._tempCanvas.style.left = "0";
    this._tempCanvas.style.zIndex = "-1";
    this._tempCanvas.style.visibility = "hidden";
    this._resizeCanvasToEngineHolder(this._tempCanvas);
    this._canvasesHolder.appendChild(this._tempCanvas);
    this._tempCanvasContext = this._tempCanvas.getContext("2d");
  }

  private _resizeCanvasToEngineHolder(canvas: HTMLCanvasElement) {
    canvas.width = this._canvasesHolder.offsetWidth;
    canvas.height = this._canvasesHolder.offsetHeight;
    canvas.style.width = `${this._canvasesHolder.offsetWidth}px`;
    canvas.style.height = `${this._canvasesHolder.offsetHeight}px`;
  }

  private _initCanvasModules() {
    const modules = orderBy(
      this._moduleRegistry.drawableModules.toArray(),
      module => Reflect.getMetadata(META_MODULE_Z_INDEX, module.constructor),
      "asc"
    ) as CanvasModuleDraw[];
    modules.forEach((module, i) => {
      this._initDrawableCanvasModule(module, i + 1);
      this._drawableModules.push(module);
    });

    const sub = this._moduleRegistry.moduleUnregistered().subscribe(module => {
      const index = this._drawableModules.indexOf(module);
      if (index >= 0) {
        this._drawableModules.splice(index, 1);
      }
    });
    this._subs.push(sub);
  }

  private _initDrawableCanvasModule(
    module: CanvasModuleDraw,
    canvasZIndex: number
  ) {
    const moduleName = this._getModuleName(module);
    const canvas = document.createElement("canvas");
    canvas.className = MODULE_CANVAS_CLASS;
    canvas.style.position = "absolute";
    canvas.style.top = "0";
    canvas.style.left = "0";
    canvas.style.zIndex = canvasZIndex.toString();
    canvas.setAttribute(ATTR_MODULE_NAME, moduleName);
    this._resizeCanvasToEngineHolder(canvas);
    this._canvasesHolder.appendChild(canvas);

    Reflect.defineMetadata(META_MODULE_CANVAS, canvas, module);

    const context = canvas.getContext("2d");
    context.setTransform(this._camera.transform.toDOMMatrix());
    Reflect.defineMetadata(META_MODULE_CANVAS_CTX, context, module);
  }

  private _getModuleName(module: any) {
    const prototype = Object.getPrototypeOf(module);

    if (
      prototype.constructor &&
      prototype.constructor.name !== "CanvasModule"
    ) {
      return prototype.constructor.name;
    } else {
      return this._getModuleName(prototype);
    }
  }

  private _initCameraEvents() {
    const sub = this._camera.transformChanged().subscribe(() => {
      this._forceRedraw = true;
    });
    this._subs.push(sub);
  }

  private _initCanvasResizeObservers() {
    const factory = ElementResizeFactory();
    factory.listenTo(this._canvasesHolder, () => {
      this._resizeCanvasToEngineHolder(this._tempCanvas);
      this._drawableModules.forEach(module => {
        const canvas = Reflect.getMetadata(META_MODULE_CANVAS, module);
        if (canvas) {
          this._resizeCanvasToEngineHolder(canvas);
        }
      });

      // FIXME(pmo): when the if statement is missing some tests will randomly fail because the context is undefined
      if (
        this._drawableModules.filter(x =>
          Reflect.getMetadata(META_MODULE_CANVAS_CTX, x)
        ).length === this._drawableModules.length
      ) {
        this._forceRedraw = true;
      }
    });
  }

  draw() {
    this._drawableModules.forEach(module => {
      if (module.redraw || this._forceRedraw) {
        const context = Reflect.getMetadata(META_MODULE_CANVAS_CTX, module);
        if (context) {
          this._clearCanvas(context);
          module.onDraw(context);
        }
      }
    });

    this._forceRedraw = false;
  }

  private _clearCanvas(context: CanvasRenderingContext2D) {
    context.setTransform(1, 0, 0, 1, 0, 0);
    context.clearRect(0, 0, context.canvas.width, context.canvas.height);
    context.scale(this._camera.transform.a, this._camera.transform.d);
    context.setTransform(
      this._camera.transform.a,
      this._camera.transform.b,
      this._camera.transform.c,
      this._camera.transform.d,
      this._camera.transform.e,
      this._camera.transform.f
    );
  }

  destroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });

    // TODO: remove the resize listener of the canvases holder
    this._drawableModules.forEach(module => {
      const canvas = Reflect.getMetadata(META_MODULE_CANVAS, module);
      try {
        this._canvasesHolder.removeChild(canvas);
      } catch (e) {}

      Reflect.deleteMetadata(META_MODULE_CANVAS, module);
      Reflect.deleteMetadata(META_MODULE_CANVAS_CTX, module);
    });
  }
}
