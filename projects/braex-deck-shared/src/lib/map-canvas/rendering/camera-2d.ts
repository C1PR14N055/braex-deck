import { Matrix2D } from "../ds/matrix-2d";
import { Observable, Observer, Subscription } from "rxjs";
import { Vector2 } from "../ds/vector2";
import * as ElementResizeFactory from "element-resize-detector";
import { BaseInputController } from "../input/base-input-controller";
import { BaseInputControllerFactory } from "../input/base-input-controller-factory";
import { MouseButton } from "../../ds/mouse-button.enum";
import BigNumber from "bignumber.js";
import { MouseWheel } from "../ds/events/mouse-wheel.enum";
import { ZoomMode } from "../canvas-modules/ds/zoom-mode.enum";

export const DRAG_MOUSE_BUTTON = MouseButton.LMB;
export const DEFAULT_ZOOM = Object.freeze(new BigNumber(1));
export const ZOOM_STEP_SIZE = Object.freeze(new BigNumber(0.4));
export const MAX_ZOOM = Object.freeze(new BigNumber(7.8));
export const MIN_ZOOM = Object.freeze(new BigNumber(1));

export class Camera2D {
  private _targetRect: ClientRect;
  private _transform = Matrix2D.create();
  private _cameraTransformObservers: Observer<Matrix2D>[] = [];
  private readonly _inputController: BaseInputController;
  private _subs: Subscription[] = [];
  private _offset = Vector2.create();
  private _dragging = false;
  private _dragStart: Vector2 | null = null;
  private _previousMousePosition: Vector2 | null = null;
  private _zoomMode = ZoomMode.NONE;
  private _zoom = Object.freeze(new BigNumber(DEFAULT_ZOOM));
  private _zoomTarget: Vector2 | null = null;
  private _transformDirty = false;

  constructor(private _target: HTMLDivElement) {
    this._targetRect = this._target.getBoundingClientRect();
    this._inputController = BaseInputControllerFactory.create(this._target);
    this._initInputEvents();
    this._initTargetResizeObserver();
  }

  private _initInputEvents() {
    let sub = this._inputController.mouseDown().subscribe(e => {
      if (e.mouseButton === DRAG_MOUSE_BUTTON) {
        this._dragging = true;
      }
    });
    this._subs.push(sub);

    sub = this._inputController.mouseUp().subscribe(() => {
      if (!this._dragging) {
        return;
      }

      this._dragging = false;
      this._dragStart = null;
    });
    this._subs.push(sub);

    sub = this._inputController.mouseWheel().subscribe(e => {
      if (e.wheel === MouseWheel.ZOOM_IN) {
        this._zoomMode = ZoomMode.ZOOM_IN;
      } else if (e.wheel === MouseWheel.ZOOM_OUT) {
        this._zoomMode = ZoomMode.ZOOM_OUT;
      }
    });
    this._subs.push(sub);
  }

  private _initTargetResizeObserver() {
    const factory = ElementResizeFactory();
    factory.listenTo(this._target, () => {
      this._targetRect = this._target.getBoundingClientRect();
    });
  }

  private _triggerCameraTransformChanged(transform: Matrix2D) {
    this._cameraTransformObservers.forEach(x => {
      x.next(transform);
    });
  }

  transformChanged(): Observable<Matrix2D> {
    return Observable.create(observer => {
      this._cameraTransformObservers.push(observer);
    });
  }

  /**
   * Transform a position from the document i.e. the current web page to the space of the camera (world).
   * @param position The {@link Vector2} which should be transformed to the space of the camera.
   */
  documentToWorldPosition(position: Vector2) {
    const x =
      (position.x - this._targetRect.left - this._transform.e) /
      this._transform.a;
    const y =
      (position.y - this._targetRect.top - this._transform.f) /
      this._transform.d;

    return Vector2.create(x, y);
  }

  update(mousePosition: Vector2) {
    this._zoomCamera(mousePosition);
    this._dragCamera(mousePosition);

    if (this._transformDirty) {
      this._recomputeTransform();
    }

    this._previousMousePosition = mousePosition;
  }

  private _dragCamera(mousePosition: Vector2) {
    if (!this._dragging) {
      return;
    }
    if (!this._dragStart) {
      this._dragStart = mousePosition;
    }

    if (
      !this._previousMousePosition ||
      !this._previousMousePosition.equals(mousePosition)
    ) {
      this._offset = this._offset.add(this._dragStart.diff(mousePosition));
      this._transformDirty = true;
    }
  }

  private _recomputeTransform() {
    this.transform = Matrix2D.create(
      this._zoom.toNumber(),
      0,
      0,
      this._zoom.toNumber(),
      this._offset.x,
      this._offset.y
    );
  }

  private _zoomCamera(mousePosition: Vector2) {
    if (this._zoomMode === ZoomMode.NONE) {
      return;
    }

    if (this._zoomMode === ZoomMode.ZOOM_IN) {
      this.zoomIn(mousePosition);
      this._zoomMode = ZoomMode.NONE;
    } else if (this._zoomMode === ZoomMode.ZOOM_OUT) {
      this.zoomOut(mousePosition);
      this._zoomMode = ZoomMode.NONE;
    }
  }

  /**
   * Zoom in by one step.
   * @param target (optional) point on which the camera should focus when zooming.
   * When no point is given it will use the center point.
   */
  zoomIn(target?: Vector2) {
    if (!target) {
      target = this._getCenter();
    }

    const newZoom = this._zoom.plus(ZOOM_STEP_SIZE);
    if (newZoom.isLessThanOrEqualTo(MAX_ZOOM)) {
      this._zoom = Object.freeze(newZoom);
      this._zoomTarget = target;
      this._computeZoomOffset(ZoomMode.ZOOM_IN);
      this._transformDirty = true;
    }
  }

  private _computeZoomOffset(mode: ZoomMode) {
    const zoomTarget = this._zoomTarget || Vector2.create();

    const offset = Vector2.create()
      .diff(zoomTarget)
      .multiply(ZOOM_STEP_SIZE.toNumber());
    if (mode === ZoomMode.ZOOM_IN) {
      this._offset = this._offset.subtract(offset);
    } else if (mode === ZoomMode.ZOOM_OUT) {
      this._offset = this._offset.add(offset);
    }
  }

  /**
   * The center point of the camera in the current context.
   */
  private _getCenter() {
    const rect = this._target.getBoundingClientRect();

    return this.documentToWorldPosition(
      Vector2.create(rect.left + this.width / 2, rect.top + this.height / 2)
    );
  }

  /**
   * Zoom out by one step.
   * @param target (optional) point on which the camera should focus when zooming.
   * When no point is given it will use the center point.
   */
  zoomOut(target?: Vector2) {
    if (!target) {
      target = this._getCenter();
    }

    const newZoom = this._zoom.minus(ZOOM_STEP_SIZE);
    if (newZoom.isGreaterThanOrEqualTo(MIN_ZOOM)) {
      this._zoom = Object.freeze(newZoom);
      this._zoomTarget = target;
      this._computeZoomOffset(ZoomMode.ZOOM_OUT);
      this._transformDirty = true;
    }
  }

  /**
   * Focus a position in the camera space.
   * (The position will be in the center of the camera).
   * @param position The position which should be focused (it needs to be in the space of the camera).
   */
  focusPosition(position: Vector2) {
    this._offset = Vector2.create().add(
      Vector2.create(this.width, this.height)
        .divide(2)
        .subtract(position)
    );
    this._transformDirty = true;
  }

  destroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  get transform() {
    return this._transform;
  }

  set transform(value: Matrix2D) {
    const changed = !this._transform.equals(value);
    this._transform = value;

    if (changed) {
      this._triggerCameraTransformChanged(value);
    }
  }

  get dragging() {
    return this._dragging;
  }

  get zoom() {
    return this._zoom;
  }

  get zoomTarget() {
    return this._zoomTarget;
  }

  get zoomMode() {
    return this._zoomMode;
  }

  get width() {
    return this._target.offsetWidth;
  }

  get height() {
    return this._target.offsetHeight;
  }

  get offset() {
    return this._offset;
  }
}
