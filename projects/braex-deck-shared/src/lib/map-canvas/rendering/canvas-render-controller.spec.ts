import "reflect-metadata";
import {
  ATTR_MODULE_NAME,
  CanvasRenderController,
  META_MODULE_CANVAS,
  META_MODULE_CANVAS_CTX,
  MODULE_CANVAS_CLASS,
  TEMP_CANVAS_CLASS
} from "./canvas-render-controller";
import { Camera2D } from "./camera-2d";
import { CanvasModuleRegistry } from "../canvas-modules/canvas-module-registry";
import { CanvasModuleRegistryFactory } from "../canvas-modules/canvas-module-registry-factory";
import {
  CanvasModuleDraw,
  CanvasModuleUpdate
} from "../canvas-modules/canvas-module-hooks";
import { CanvasModule } from "../canvas-modules/canvas-module.decorator";
import { fakeAsync, TestBed, tick } from "@angular/core/testing";
import { Vector2 } from "../ds/vector2";
import { Matrix2D } from "../ds/matrix-2d";

@CanvasModule({
  zIndex: 1
})
class FakeDrawableModule1 implements CanvasModuleDraw, CanvasModuleUpdate {
  private _redraw = false;

  onUpdate(mousePosition: Vector2) {}

  onDraw(canvasContext: CanvasRenderingContext2D) {
    this._redraw = false;
  }

  get redraw() {
    return this._redraw;
  }

  set redraw(value: boolean) {
    this._redraw = value;
  }
}

@CanvasModule({
  zIndex: 2
})
class FakeDrawableModule2 implements CanvasModuleDraw, CanvasModuleUpdate {
  private _redraw = false;

  onUpdate(mousePosition: Vector2) {}

  onDraw(canvasContext: CanvasRenderingContext2D) {
    this._redraw = false;
  }

  get redraw() {
    return this._redraw;
  }

  set redraw(value: boolean) {
    this._redraw = value;
  }
}

@CanvasModule()
class FakeDrawableModule3 implements CanvasModuleDraw {
  private _redraw = false;

  onDraw(canvasContext: CanvasRenderingContext2D) {}

  get redraw() {
    return this._redraw;
  }

  set redraw(value: boolean) {
    this._redraw = value;
  }
}

describe("CanvasRenderController", () => {
  let moduleRegistry: CanvasModuleRegistry;
  let controller: CanvasRenderController;
  let canvasesHolder: HTMLDivElement;
  let cameraOverlay: HTMLDivElement;
  let camera: Camera2D;

  beforeEach(() => {
    moduleRegistry = new CanvasModuleRegistry();
    spyOn(CanvasModuleRegistryFactory, "create").and.returnValue(
      moduleRegistry
    );

    moduleRegistry.registerModules(
      TestBed.get(FakeDrawableModule3),
      TestBed.get(FakeDrawableModule1),
      TestBed.get(FakeDrawableModule2)
    );

    canvasesHolder = document.createElement("div");
    document.body.appendChild(canvasesHolder);

    cameraOverlay = document.createElement("div");
    canvasesHolder.appendChild(cameraOverlay);
    camera = new Camera2D(cameraOverlay);
    camera.transform = Matrix2D.create(12, 4, 3, 9, 1, 6);

    controller = new CanvasRenderController(canvasesHolder, camera);
  });

  it("should be created", () => {
    expect(controller).toBeTruthy();
  });

  it("should create a canvas for each drawable module when creating the controller", () => {
    const canvases = canvasesHolder.querySelectorAll(`.${MODULE_CANVAS_CLASS}`);
    expect(canvases.length).toBe(moduleRegistry.drawableModules.size);
  });

  it("should attach the canvas of a drawable module to the module itself", () => {
    const canvas = Reflect.getMetadata(
      META_MODULE_CANVAS,
      TestBed.get(FakeDrawableModule1)
    );

    expect(canvas).toBeTruthy();
    expect((canvas as HTMLElement).tagName.toLowerCase()).toBe("canvas");
    expect(
      canvasesHolder.querySelectorAll(`.${MODULE_CANVAS_CLASS}`)
    ).toContain(canvas);
  });

  it("should attach the context of the canvas to the drawable module itself", () => {
    const context = Reflect.getMetadata(
      META_MODULE_CANVAS_CTX,
      TestBed.get(FakeDrawableModule1)
    );

    expect(context).toBeTruthy();
    expect(context instanceof CanvasRenderingContext2D).toBeTruthy();
    expect(context.canvas).toBe(
      Reflect.getMetadata(META_MODULE_CANVAS, TestBed.get(FakeDrawableModule1))
    );
  });

  it("should redraw only the modules which need to be redrawn", () => {
    const module1: FakeDrawableModule1 = TestBed.get(FakeDrawableModule1);
    module1.redraw = true;
    const module2: FakeDrawableModule2 = TestBed.get(FakeDrawableModule2);
    module2.redraw = false;

    const spy1 = spyOn(module1, "onDraw");
    const spy2 = spyOn(module2, "onDraw");

    controller.draw();
    expect(spy1).toHaveBeenCalled();
    expect(spy2).not.toHaveBeenCalled();
  });

  it("should remove the canvases of each drawable module when destroying the controller", () => {
    expect(
      canvasesHolder.querySelectorAll(`.${MODULE_CANVAS_CLASS}`).length
    ).toBe(moduleRegistry.drawableModules.size);
    controller.destroy();
    expect(
      canvasesHolder.querySelectorAll(`.${MODULE_CANVAS_CLASS}`).length
    ).toBeFalsy();
  });

  it("should remove the canvas from the drawable module when destroying the controller", () => {
    moduleRegistry.drawableModules.forEach(module => {
      expect(Reflect.getMetadata(META_MODULE_CANVAS, module)).toBeTruthy();
    });
    controller.destroy();

    moduleRegistry.drawableModules.forEach(module => {
      expect(Reflect.getMetadata(META_MODULE_CANVAS, module)).toBeFalsy();
    });
  });

  it("should remove the context of the canvas from the drawable module when destroying the controller", () => {
    moduleRegistry.drawableModules.forEach(module => {
      expect(Reflect.getMetadata(META_MODULE_CANVAS_CTX, module)).toBeTruthy();
    });
    controller.destroy();

    moduleRegistry.drawableModules.forEach(module => {
      expect(Reflect.getMetadata(META_MODULE_CANVAS_CTX, module)).toBeFalsy();
    });
  });

  it("should set the context transform of a module to the camera transform after creating the controller", () => {
    moduleRegistry.drawableModules.forEach(module => {
      const context: CanvasRenderingContext2D = Reflect.getMetadata(
        META_MODULE_CANVAS_CTX,
        module
      );

      expect(
        Matrix2D.create(context.getTransform()).equals(camera.transform)
      ).toBeTruthy();
    });
  });

  it("should order the canvases in the defined z-index order on the canvas modules", () => {
    const module1Canvas = Reflect.getMetadata(
      META_MODULE_CANVAS,
      TestBed.get(FakeDrawableModule1)
    );
    const module2Canvas = Reflect.getMetadata(
      META_MODULE_CANVAS,
      TestBed.get(FakeDrawableModule2)
    );
    const module3Canvas = Reflect.getMetadata(
      META_MODULE_CANVAS,
      TestBed.get(FakeDrawableModule3)
    );

    const canvases = canvasesHolder.querySelectorAll(`.${MODULE_CANVAS_CLASS}`);
    expect(canvases[0]).toBe(module1Canvas);
    expect(canvases[1]).toBe(module2Canvas);
    expect(canvases[2]).toBe(module3Canvas);
  });

  it("should place the html canvas elements on top of each other", () => {
    const module1Canvas: HTMLCanvasElement = Reflect.getMetadata(
      META_MODULE_CANVAS,
      TestBed.get(FakeDrawableModule1)
    );
    const module2Canvas: HTMLCanvasElement = Reflect.getMetadata(
      META_MODULE_CANVAS,
      TestBed.get(FakeDrawableModule2)
    );
    const module3Canvas: HTMLCanvasElement = Reflect.getMetadata(
      META_MODULE_CANVAS,
      TestBed.get(FakeDrawableModule3)
    );

    expect(module1Canvas.style.zIndex).toBe("1");
    expect(module2Canvas.style.zIndex).toBe("2");
    expect(module3Canvas.style.zIndex).toBe("3");
  });

  it("should attach the name of the module as an attribute to the canvas", () => {
    const module1Canvas: HTMLCanvasElement = Reflect.getMetadata(
      META_MODULE_CANVAS,
      TestBed.get(FakeDrawableModule1)
    );
    expect(module1Canvas.hasAttribute(ATTR_MODULE_NAME)).toBeTruthy();
    expect(module1Canvas.getAttribute(ATTR_MODULE_NAME)).toBe(
      "FakeDrawableModule1"
    );
  });

  it("should apply the camera transform to the canvases when it changes", fakeAsync(() => {
    moduleRegistry.drawableModules.forEach(module => {
      const context = Reflect.getMetadata(META_MODULE_CANVAS_CTX, module);
      expect(
        Matrix2D.create(context.getTransform()).equals(camera.transform)
      ).toBeTruthy();
    });

    camera.transform = Matrix2D.create(42, 16, 12, 42, 39, 4);
    tick(400);

    controller.draw();
    moduleRegistry.drawableModules.forEach(module => {
      const context = Reflect.getMetadata(META_MODULE_CANVAS_CTX, module);
      expect(
        Matrix2D.create(context.getTransform()).equals(camera.transform)
      ).toBeTruthy();
    });
  }));

  it("should create a temp canvas when creating the controller", () => {
    const canvas = canvasesHolder.querySelector(`.${TEMP_CANVAS_CLASS}`);
    expect(canvas).toBeTruthy();
    expect((canvas as HTMLElement).tagName.toLowerCase()).toBe("canvas");
  });

  it("should force redraw the modules when the transform of the camera changes", fakeAsync(() => {
    const spies: jasmine.Spy[] = [];
    moduleRegistry.drawableModules.forEach(module => {
      const spy = spyOn(module, "onDraw");
      spies.push(spy);
    });

    controller.draw();
    spies.forEach(spy => {
      expect(spy).not.toHaveBeenCalled();
    });

    camera.transform = Matrix2D.create(43, 234, 75, 5, 23, 3);
    tick(400);
    controller.draw();

    spies.forEach(spy => {
      expect(spy).toHaveBeenCalled();
    });
  }));

  it("should stop the force redraw after redrawing", fakeAsync(() => {
    const spies: jasmine.Spy[] = [];
    moduleRegistry.drawableModules.forEach(module => {
      const spy = spyOn(module, "onDraw");
      spies.push(spy);
    });

    controller.draw();
    spies.forEach(spy => {
      expect(spy).not.toHaveBeenCalled();
    });

    camera.transform = Matrix2D.create(43, 234, 75, 5, 23, 3);
    tick(400);
    controller.draw();
    controller.draw();
    controller.draw();
    controller.draw();
    controller.draw();

    spies.forEach(spy => {
      expect(spy).toHaveBeenCalledTimes(1);
    });
  }));
});
