import { ExtVector3 } from "../../external/ext-vector-3";

export interface ExtTeamPositionSync {
  position: ExtVector3;
  owner: string;
  alive: boolean;
}
