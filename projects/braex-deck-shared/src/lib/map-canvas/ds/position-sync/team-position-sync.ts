import { Vector3 } from "../vector3";

export class TeamPositionSync {
  constructor(
    public owner: string,
    public position: Vector3,
    public alive = true
  ) {}
}
