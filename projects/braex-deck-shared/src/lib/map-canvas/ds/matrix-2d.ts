export class Matrix2D {
  a = 1;

  protected constructor(
    a?: number | DOMMatrix | number[] | Matrix2D,
    public b = 0,
    public c = 0,
    public d = 1,
    public e = 0,
    public f = 0
  ) {
    if (a && typeof a !== "number") {
      if ((a as number[]).length !== undefined) {
        if ((a as number[]).length !== 6) {
          throw new Error(
            "The length of the array to create the matrix must be 6"
          );
        }

        this.a = a[0];
        this.b = a[1];
        this.c = a[2];
        this.d = a[3];
        this.e = a[4];
        this.f = a[5];
      } else if (a instanceof DOMMatrix || a instanceof Matrix2D) {
        this.a = a.a;
        this.b = a.b;
        this.c = a.c;
        this.d = a.d;
        this.e = a.e;
        this.f = a.f;
      }
    } else if (typeof a === "number") {
      this.a = a;
    }
  }

  static create(
    a?: number | DOMMatrix | number[] | Matrix2D,
    b = 0,
    c = 0,
    d = 1,
    e = 0,
    f = 0
  ) {
    return Object.freeze(new Matrix2D(a, b, c, d, e, f));
  }

  equals(other: Matrix2D) {
    return (
      this.a === other.a &&
      this.b === other.b &&
      this.c === other.c &&
      this.d === other.d &&
      this.e === other.e &&
      this.f === other.f
    );
  }

  toDOMMatrix() {
    return new DOMMatrix(this.toArray());
  }

  toArray() {
    return [this.a, this.b, this.c, this.d, this.e, this.f];
  }
}
