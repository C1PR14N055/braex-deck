import { Vector2 } from "./vector2";

export class Line {
  constructor(private _start: Vector2, private _end: Vector2) {}

  get start() {
    return this._start;
  }

  get end() {
    return this._end;
  }

  length() {
    return this._start.distance(this._end);
  }

  /**
   * Calculates the distance between a line and a vector position
   * If the triangle is *obtuse* in the angle between A & C return A
   * If the triangle is *obtuse* in the angle between B & C return B
   * Else return the perpedicular imaginary line distance to this line
   * @param position The position from which the distance is calculated to this line
   * @return number
   */
  distanceToPosition(position: Vector2): number {
    const triangleSideA = new Line(this._start, position).length();
    const triangleSideB = new Line(position, this._end).length();
    const triangleSideC = new Line(this._end, this._start).length();

    if (triangleSideB ** 2 > triangleSideA ** 2 + triangleSideC ** 2) {
      return triangleSideA;
    } else if (triangleSideA ** 2 > triangleSideB ** 2 + triangleSideC ** 2) {
      return triangleSideB;
    } else {
      const triangleHalfPerimeter =
        (triangleSideA + triangleSideB + triangleSideC) / 2;
      const triangleArea = Math.sqrt(
        triangleHalfPerimeter *
          (triangleHalfPerimeter - triangleSideA) *
          (triangleHalfPerimeter - triangleSideB) *
          (triangleHalfPerimeter - triangleSideC)
      );

      return (2 / triangleSideC) * triangleArea;
    }
  }
}
