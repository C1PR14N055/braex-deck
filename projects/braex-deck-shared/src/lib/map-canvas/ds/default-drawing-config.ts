import { Color } from "./color.enum";

export const DEFAULT_BRUSH_THICKNESS = 3;
export const DEFAULT_COLOR = Color.CHATEAU_GREEN;
