import { MapCanvasTool } from "./map-canvas-tool.enum";
import { MouseButton } from "../../ds/mouse-button.enum";
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";

export const TOOL_MOUSE_BUTTON = MouseButton.RMB;

export interface ToolConfig {
  tool: MapCanvasTool;
  cursor?: string;
  icon: IconDefinition;
}
