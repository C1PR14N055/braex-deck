import { TelemetryLocation } from "./telemetry-location";

export class TelemetryCharacter {
  name: string;
  teamId: number;
  health: number;
  location: TelemetryLocation;
  ranking: number;
  accountId: string;
  isInBlueZone: boolean;
  isInRedZone: boolean;
  zone: string[];

  constructor({
    name,
    teamId,
    health,
    location,
    ranking,
    accountId,
    isInBlueZone,
    isInRedZone,
    zone
  }) {
    this.name = name;
    this.teamId = teamId;
    this.health = health;
    this.location = new TelemetryLocation(location);
    this.ranking = ranking;
    this.accountId = accountId;
    this.isInBlueZone = isInBlueZone;
    this.isInRedZone = isInRedZone;
    this.zone = zone;
  }
}
