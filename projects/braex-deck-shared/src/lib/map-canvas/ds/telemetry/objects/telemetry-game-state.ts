import { TelemetryLocation } from "./telemetry-location";

export class TelemetryGameState {
  elapsedTime: number;
  numAliveTeams: number;
  numJoinPlayers: number;
  numStartPlayers: number;
  numAlivePlayers: number;
  safetyZonePosition: TelemetryLocation;
  safetyZoneRadius: number;
  poisonGasWarningPosition: TelemetryLocation;
  poisonGasWarningRadius: number;
  redZonePosition: TelemetryLocation;
  redZoneRadius: number;

  constructor({
    elapsedTime,
    numAliveTeams,
    numJoinPlayers,
    numStartPlayers,
    numAlivePlayers,
    safetyZonePosition,
    safetyZoneRadius,
    poisonGasWarningPosition,
    poisonGasWarningRadius,
    redZonePosition,
    redZoneRadius
  }) {
    this.elapsedTime = elapsedTime;
    this.numAliveTeams = numAliveTeams;
    this.numJoinPlayers = numJoinPlayers;
    this.numStartPlayers = numStartPlayers;
    this.numAlivePlayers = numAlivePlayers;
    this.safetyZonePosition = new TelemetryLocation(safetyZonePosition);
    this.safetyZoneRadius = safetyZoneRadius;
    this.poisonGasWarningPosition = new TelemetryLocation(
      poisonGasWarningPosition
    );
    this.poisonGasWarningRadius = poisonGasWarningRadius;
    this.redZonePosition = new TelemetryLocation(redZonePosition);
    this.redZoneRadius = redZoneRadius;
  }
}
