export class TelemetryVehicle {
  vehicleType: string;
  vehicleId: string;
  healthPercent: number;
  feulPercent: number; // yes its mispelled

  constructor({ vehicleType, vehicleId, healthPercent, feulPercent }) {
    this.vehicleType = vehicleType;
    this.vehicleId = vehicleId;
    this.healthPercent = healthPercent;
    this.feulPercent = feulPercent;
  }
}
