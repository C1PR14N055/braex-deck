import { TelemetryStats } from "./telemetry-stats";

export class TelemetryGameResult {
  rank: number;
  gameResult: string;
  teamId: number;
  stats: any;
  accountId: string;

  constructor({ rank, gameResult, teamId, stats, accountId }) {
    this.rank = rank;
    this.gameResult = gameResult;
    this.teamId = teamId;
    this.stats = new TelemetryStats(stats);
    this.accountId = accountId;
  }
}
