export class TelemetryStats {
  killCount: number;
  distanceOnFoot: number;
  distanceOnSwim: number;
  distanceOnVehicle: number;
  distanceOnParachute: number;
  distanceOnFreefall: number;

  constructor({
    killCount,
    distanceOnFoot,
    distanceOnSwim,
    distanceOnVehicle,
    distanceOnParachute,
    distanceOnFreefall
  }) {
    this.killCount = killCount;
    this.distanceOnFoot = distanceOnFoot;
    this.distanceOnSwim = distanceOnSwim;
    this.distanceOnVehicle = distanceOnVehicle;
    this.distanceOnParachute = distanceOnParachute;
    this.distanceOnFreefall = distanceOnFreefall;
  }
}
