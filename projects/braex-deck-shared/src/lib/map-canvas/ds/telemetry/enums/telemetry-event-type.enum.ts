export enum TelemetryEventType {
  // TODO: add all eventsByTimestamp
  LOG_MATCH_DEFINITION = "LogMatchDefinition",
  LOG_MATCH_START = "LogMatchStart",
  LOG_MATCH_END = "LogMatchEnd",
  LOG_PLAYER_LOGIN = "LogPlayerLogin",
  LOG_PLAYER_LOGOUT = "LogPlayerLogout",
  LOG_PLAYER_CREATE = "LogPlayerCreate",
  LOG_PLAYER_POSITION = "LogPlayerPosition",
  LOG_PLAYER_KILL = "LogPlayerKill",
  LOG_PLAYER_ATTACK = "LogPlayerAttack",
  LOG_PLAYER_REVIVE = "LogPlayerRevive",
  LOG_GAME_STATE_PERIODIC = "LogGameStatePeriodic"
}
