import { TelemetryCharacter } from "../objects/telemetry-character";

export class TelemetryEventPlayerCreated {
  character: TelemetryCharacter;

  constructor(character: TelemetryCharacter) {
    this.character = character;
  }
}
