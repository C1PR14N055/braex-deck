export class TelemetryEventPlayerLogout {
  accountId: string;

  constructor({ accountId }) {
    this.accountId = accountId;
  }
}
