import { TelemetryGameState } from "../objects/telemetry-game-state";

export class TelemetryEventGameStatePeriodic {
  gameState: TelemetryGameState;

  constructor(gameState) {
    this.gameState = gameState;
  }
}
