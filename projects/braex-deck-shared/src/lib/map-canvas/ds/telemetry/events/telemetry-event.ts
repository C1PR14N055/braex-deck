import { TelemetryEventPlayerPosition } from "./telemetry-event-player-position";
import { TelemetryEventPlayerCreated } from "./telemetry-event-player-created";
import { TelemetryEventPlayerLogin } from "./telemetry-event-player-login";
import { TelemetryEventPlayerLogout } from "./telemetry-event-player-logout";
import { TelemetryEventPlayerKill } from "./telemetry-event-player-kill";
import { TelemetryEventGameStatePeriodic } from "./telemetry-event-game-state-periodic";

export type TelemetryEvent =
  | TelemetryEventPlayerLogin
  | TelemetryEventPlayerLogout
  | TelemetryEventPlayerCreated
  | TelemetryEventPlayerPosition
  | TelemetryEventPlayerKill
  | TelemetryEventGameStatePeriodic;
