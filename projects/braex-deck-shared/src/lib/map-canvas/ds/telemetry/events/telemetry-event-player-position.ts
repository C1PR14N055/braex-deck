import { TelemetryCharacter } from "../objects/telemetry-character";
import { TelemetryVehicle } from "../objects/telemetry-vehicle";

export class TelemetryEventPlayerPosition {
  character: TelemetryCharacter;
  vehicle: TelemetryVehicle;
  elapsedTime: number;
  numAlivePlayer: number;

  constructor({ character, vehicle, elapsedTime, numAlivePlayer }) {
    this.character = new TelemetryCharacter(character);
    this.vehicle = new TelemetryVehicle(vehicle);
    this.elapsedTime = elapsedTime;
    this.numAlivePlayer = numAlivePlayer;
  }
}
