import { TelemetryCharacter } from "../objects/telemetry-character";
import { TelemetryGameResult } from "../objects/telemetry-game-result";

export class TelemetryEventPlayerKill {
  attackId: number;
  killer: TelemetryCharacter;
  victim: TelemetryCharacter;
  assistant: TelemetryCharacter;
  dBNOId: number;
  damageTypeCategory: string;
  damageCauserName: string;
  damageCauserAdditionalInfo: string[];
  damageReason: string;
  distance: number;
  victimGameResult: TelemetryGameResult;

  constructor({
    attackId,
    killer,
    victim,
    assistant,
    dBNOId,
    damageTypeCategory,
    damageCauserName,
    damageCauserAdditionalInfo,
    damageReason,
    distance,
    victimGameResult
  }) {
    this.attackId = attackId;
    this.killer = new TelemetryCharacter(killer);
    this.victim = new TelemetryCharacter(victim);
    this.assistant = new TelemetryCharacter(assistant);
    this.dBNOId = dBNOId;
    this.damageTypeCategory = damageTypeCategory;
    this.damageCauserName = damageCauserName;
    this.damageCauserAdditionalInfo = damageCauserAdditionalInfo;
    this.damageReason = damageReason;
    this.distance = distance;
    this.victimGameResult = new TelemetryGameResult(victimGameResult);
  }
}
