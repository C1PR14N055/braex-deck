export class TelemetryEventPlayerLogin {
  accountId: string;

  constructor({ accountId }) {
    this.accountId = accountId;
  }
}
