import { TelemetryCharacter } from "../objects/telemetry-character";

export class TelemetryEventMatchStart {
  mapName: string;
  weatherId: string;
  characters: TelemetryCharacter[];
  cameraViewBehaviour: string;
  teamSize: number;
  isCustomGame: boolean;
  isEventMode: boolean;
  blueZoneCustomOptions: string;

  constructor({
    mapName,
    weatherId,
    characters,
    cameraViewBehaviour,
    teamSize,
    isCustomGame,
    isEventMode,
    blueZoneCustomOptions
  }) {
    this.mapName = mapName;
    this.weatherId = weatherId;
    this.characters = characters.map(
      character => new TelemetryCharacter(character)
    );
    this.cameraViewBehaviour = cameraViewBehaviour;
    this.teamSize = teamSize;
    this.isCustomGame = isCustomGame;
    this.isEventMode = isEventMode;
    this.blueZoneCustomOptions = blueZoneCustomOptions;
  }
}
