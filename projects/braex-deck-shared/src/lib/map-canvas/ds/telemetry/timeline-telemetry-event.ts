import { TelemetryEventMatchStart } from "./events/telemetry-event-match-start";
import { TelemetryCharacter } from "./objects/telemetry-character";
import { TelemetryEventGameStatePeriodic } from "./events/telemetry-event-game-state-periodic";

export class TimelineTelemetryEvents {
  matchDuration: number;
  matchStartEvent: TelemetryEventMatchStart = null;
  // Map of seconds elapsed to chars
  charactersAtMoment: Map<number, TelemetryCharacter[]> = new Map<
    number,
    TelemetryCharacter[]
  >();
  // Map of seconds elapsed to gameState
  gameStateAtMoment: Map<number, TelemetryEventGameStatePeriodic> = new Map<
    number,
    TelemetryEventGameStatePeriodic
  >();
}
