import { Vector2 } from "./vector2";

describe("Vector2", () => {
  it("should be equal", () => {
    const a = Vector2.create(10, 10);
    const b = Vector2.create(10, 10);
    expect(a.equals(b)).toBeTruthy();
  });

  it("should not be equal", () => {
    const a = Vector2.create(10, 15);
    const b = Vector2.create(4, 9);
    expect(a.equals(b)).toBeFalsy();
  });

  it("should add the other vector to the current one", () => {
    const a = Vector2.create(10, 10);
    const b = Vector2.create(5, 3);
    expect(a.add(b)).toEqual(Vector2.create(a.x + b.x, a.y + b.y));
  });

  it("should add a number to the current vector", () => {
    const a = Vector2.create(10, 3);
    const b = 5;
    expect(a.add(b)).toEqual(Vector2.create(a.x + b, a.y + b));
  });

  it("should subtract the other vector from the current one", () => {
    const a = Vector2.create(4, 3);
    const b = Vector2.create(2, 1);
    expect(a.subtract(b)).toEqual(Vector2.create(a.x - b.x, a.y - b.y));
  });

  it("should subtract a number from the current vector", () => {
    const a = Vector2.create(12, 6);
    const b = 4;
    expect(a.subtract(b)).toEqual(Vector2.create(a.x - b, a.y - b));
  });

  it("should multiply the other vector with the current one", () => {
    const a = Vector2.create(9, 7);
    const b = Vector2.create(6, 4);
    expect(a.multiply(b)).toEqual(Vector2.create(a.x * b.x, a.y * b.y));
  });

  it("should multiply a number with the current vector", () => {
    const a = Vector2.create(17, 12);
    const b = 6;
    expect(a.multiply(b)).toEqual(Vector2.create(a.x * b, a.y * b));
  });

  it("should give a positive difference when the comparing vector is larger than the current one", () => {
    const a = Vector2.create(19, 25);
    const b = Vector2.create(52, 39);
    expect(a.diff(b)).toEqual(Vector2.create(b.x - a.x, b.y - a.y));
  });

  it("should give a negative difference when the comparing vector is smaller than the current one", () => {
    const a = Vector2.create(90, 57);
    const b = Vector2.create(16, 23);
    expect(a.diff(b)).toEqual(Vector2.create(b.x - a.x, b.y - a.y));
  });

  it("should divide the current vector by the given one", () => {
    const a = Vector2.create(12, 16);
    const b = Vector2.create(7, 3);
    const expected = Vector2.create(a.x / b.x, a.y / b.y);
    const actual = a.divide(b);
    expect(actual.x).toBeCloseTo(expected.x);
    expect(actual.y).toBeCloseTo(expected.y);
  });

  it("should divide the current vector by a number", () => {
    const a = Vector2.create(59, 45);
    const b = 7;
    const expected = Vector2.create(a.x / b, a.y / b);
    const actual = a.divide(b);
    expect(actual.x).toBeCloseTo(expected.x);
    expect(actual.y).toBeCloseTo(expected.y);
  });

  it("should return the magnitude of the negative vector when calling #abs", () => {
    const a = Vector2.create(-12, -137);
    // noinspection JSSuspiciousNameCombination
    expect(a.abs()).toEqual(Vector2.create(Math.abs(a.x), Math.abs(a.y)));
  });

  it("should return the magnitude of the positive vector when calling #abs", () => {
    const a = Vector2.create(39, 241);
    expect(a.abs()).toEqual(Vector2.create(a.x, a.y));
  });

  it("should return the distance betwen two vectors", () => {
    const a = Vector2.create(20, 40);
    const b = Vector2.create(12, 44);

    const expected = Math.hypot(a.x - b.x, a.y - b.y);
    const actual = a.distance(b);

    expect(actual).toBeCloseTo(expected);
  });
});
