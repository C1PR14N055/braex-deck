import {
  DEFAULT_BRUSH_THICKNESS,
  DEFAULT_COLOR
} from "./default-drawing-config";

export class BrushConfig {
  constructor(
    public thickness = DEFAULT_BRUSH_THICKNESS,
    public color = DEFAULT_COLOR
  ) {}

  apply(canvasContext: CanvasRenderingContext2D) {
    canvasContext.lineCap = "round";
    canvasContext.lineJoin = "round";
    canvasContext.lineWidth = this.thickness;
    canvasContext.strokeStyle = this.color;
  }

  equals(other: BrushConfig) {
    return this.thickness === other.thickness && this.color === other.color;
  }
}
