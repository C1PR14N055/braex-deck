import { Vector2 } from "./vector2";

export class MapPlayer {
  /**
   * The position of the player in the transform to the canvas world space.
   */
  position: Vector2;

  protected constructor(
    public nickname: string,
    position = Vector2.create(),
    public alive = true
  ) {
    this.position = position;
  }

  static create(nickname: string, position = Vector2.create(), alive = true) {
    if (alive === null || alive === undefined) {
      alive = true;
    }

    return Object.freeze(new MapPlayer(nickname, position, alive));
  }
}
