import { MapMarkerType } from "./map-marker-type.enum";
import { Color } from "./color.enum";

export const DEFAULT_MARKER_COLOR = Color.LIGHTNING_YELLOW;

export class MarkerConfig {
  constructor(
    public type = MapMarkerType.DEFAULT,
    public color = DEFAULT_MARKER_COLOR
  ) {}

  equals(other: MarkerConfig) {
    return this.type === other.type && this.color === other.color;
  }
}
