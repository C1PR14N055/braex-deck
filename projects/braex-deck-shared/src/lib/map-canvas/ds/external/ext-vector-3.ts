import { ExtVector2 } from "./ext-vector2";

export interface ExtVector3 extends ExtVector2 {
  z: number;
}
