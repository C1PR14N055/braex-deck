export enum ExtMapMarkerType {
  DEFAULT = "DEFAULT",
  CAR = "CAR",
  ITEM = "ITEM",
  WEAPON = "WEAPON"
}
