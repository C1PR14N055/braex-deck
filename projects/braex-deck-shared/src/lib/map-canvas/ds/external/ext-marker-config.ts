import { ExtMapMarkerType } from "./ext-map-marker-type.enum";

export interface ExtMarkerConfig {
  color: string;
  type: ExtMapMarkerType;
}
