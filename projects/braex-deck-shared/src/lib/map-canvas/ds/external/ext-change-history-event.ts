import { ExtChangeEventType } from "./ext-change-event-type.enum";

export interface ExtChangeHistoryEvent {
  type: ExtChangeEventType;
  body: string;
}
