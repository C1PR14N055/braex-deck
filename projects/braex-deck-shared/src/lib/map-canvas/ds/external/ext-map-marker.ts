import { ExtMarkerConfig } from "./ext-marker-config";
import { ExtVector2 } from "./ext-vector2";

export interface ExtMapMarker {
  position: ExtVector2;
  config: ExtMarkerConfig;
}
