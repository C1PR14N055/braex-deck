export interface ExtVector2 {
  x: number;
  y: number;
}
