import { Vector2 } from "./vector2";
import { Color } from "./color.enum";

export const ERASER_ICON_SIZE = Vector2.create(18, 26);
export const ERASER_ICON_MAX_SIZE = Vector2.create(36, 52);
export const ERASER_ICON_OFFSET = Vector2.create(
  ERASER_ICON_SIZE.x / 2,
  ERASER_ICON_SIZE.y / 2
);
export const ERASER_NEAR_LINE_ERROR_MARGIN = 12;
export const ERASER_ENABLE_ICON_FEEDBACK = true;
export const ERASER_DEFAULT_ICON_COLOR = Color.WHITE;
export const ERASER_FEEDBACK_TRUE_ICON_COLOR = Color.FERN;
export const ERASER_FEEDBACK_FALSE_ICON_COLOR = Color.WELL_READ;
// TODO: add custom eraser error margins for the markers, something more similar to their shape
