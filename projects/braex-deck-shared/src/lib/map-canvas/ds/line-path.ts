import { BrushConfig } from "./brush-config";
import { Vector2 } from "./vector2";
import { List } from "immutable";
import { Line } from "./line";

export class LinePath {
  protected constructor(
    public points = List<Vector2>(),
    public brushConfig?: BrushConfig
  ) {}

  static create(points = List<Vector2>(), brushConfig = new BrushConfig()) {
    return Object.freeze(new LinePath(points, brushConfig));
  }

  equals(other: LinePath) {
    return (
      other.points.size === this.points.size &&
      this.points.every((point, i) => other.points.get(i).equals(point)) &&
      this.brushConfig.equals(other.brushConfig)
    );
  }

  distanceToPosition(position: Vector2) {
    // has some stuff in it
    if (this.points.size === 0) {
      return Infinity;
    }

    // only has one point in it
    if (this.points.size === 1) {
      return this.points.get(0).distance(position);
    } else {
      // has more points in it
      let closest = Infinity;
      for (let i = this.points.size - 1; i > 0; i--) {
        const line = new Line(this.points.get(i), this.points.get(i - 1));
        const distance =
          line.distanceToPosition(position) -
          (this.brushConfig ? this.brushConfig.thickness / 2 : 1);
        if (distance < closest) {
          closest = distance;
        }
      }

      return closest;
    }
  }
}
