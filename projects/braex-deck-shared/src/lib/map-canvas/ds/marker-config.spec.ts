import { MarkerConfig } from "./marker-config";
import { MapMarkerType } from "./map-marker-type.enum";
import { Color } from "./color.enum";

describe("MarkerConfig", () => {
  it("should return that the configs are equal", () => {
    const a = new MarkerConfig();
    const b = new MarkerConfig();
    expect(a.equals(b)).toBeTruthy();
  });

  it("should return that the configs are not equal", () => {
    const a = new MarkerConfig(MapMarkerType.CAR, Color.BURNT_SIENNA);
    const b = new MarkerConfig(MapMarkerType.WEAPON, Color.AMERICANO);
    expect(a.equals(b)).toBeFalsy();
  });
});
