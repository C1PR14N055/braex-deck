import { Vector2 } from "./vector2";
import { MarkerConfig } from "./marker-config";

export class MapMarker {
  protected constructor(
    public position = Vector2.create(),
    public config = new MarkerConfig()
  ) {}

  static create(position = Vector2.create(), config = new MarkerConfig()) {
    if (!config) {
      config = new MarkerConfig();
    }

    return Object.freeze(new MapMarker(position, config));
  }

  equals(other: MapMarker) {
    return (
      this.position.equals(other.position) && this.config.equals(other.config)
    );
  }
}
