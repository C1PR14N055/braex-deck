export class CanvasResource {
  protected constructor(
    public path?: string,
    public image?: HTMLImageElement
  ) {}

  static create(path?: string, image?: HTMLImageElement) {
    return Object.freeze(new CanvasResource(path, image));
  }
}
