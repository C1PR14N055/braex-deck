export class Vector2 {
  protected constructor(public x = 0, public y = 0) {}

  static create(x = 0, y = 0) {
    return Object.freeze(new Vector2(x, y));
  }

  /**
   * Check whether two vectors are equal.
   * @param other The other {@link Vector2} which should be compared to this one.
   */
  equals(other: Vector2) {
    return other.x === this.x && other.y === this.y;
  }

  /**
   * Add another vector or number to this vector.
   * @param other The {@link Vector2} or number which should be added to the current vector.
   */
  add(other: Vector2 | number) {
    if (typeof other === "number") {
      return Vector2.create(this.x + other, this.y + other);
    } else {
      return Vector2.create(this.x + other.x, this.y + other.y);
    }
  }

  /**
   * Subtract another vector or a number from this vector.
   * @param other The {@link Vector2} or number which should be subtracted from the current vector.
   */
  subtract(other: Vector2 | number) {
    if (typeof other === "number") {
      return Vector2.create(this.x - other, this.y - other);
    } else {
      return Vector2.create(this.x - other.x, this.y - other.y);
    }
  }

  /**
   * Multiply another vector or a number with the this vector.
   * @param other The {@link Vector2} or number which should be multiplied with the current vector.
   */
  multiply(other: Vector2 | number) {
    if (typeof other === "number") {
      return Vector2.create(this.x * other, this.y * other);
    } else {
      return Vector2.create(this.x * other.x, this.y * other.y);
    }
  }

  /**
   * Divide this vector by a number or another vector.
   * @param other The {@link Vector2} or number with with this vector should be divided by
   */
  divide(other: Vector2 | number) {
    if (typeof other === "number") {
      return Vector2.create(this.x / other, this.y / other);
    } else {
      return Vector2.create(this.x / other.x, this.y / other.y);
    }
  }

  /**
   * Floor x and y of this vector2
   */
  floor() {
    // noinspection JSSuspiciousNameCombination
    return Vector2.create(Math.floor(this.x), Math.floor(this.y));
  }

  /**
   * Linearly interpolates this vector's coordinates to the other's vector coordinates
   * @param other The other {@link Vector2} to interpolate to
   * @param finesse A number between 0 and 1 where:
   * 0 - it will never change
   * 1 - it will jump right away to the other vector coordinates
   */
  lerp(other, finesse) {
    return Vector2.create(
      (1 - finesse) * this.x + finesse * other.x,
      (1 - finesse) * this.y + finesse * other.y
    );
  }

  /**
   * The difference between a vector and the current one.
   * Will be positive when the comparing vector is larger than the current one or negative if it's smaller.
   * @param other The {@link Vector2} of which the difference to this vector is desired.
   */
  diff(other: Vector2) {
    return Vector2.create(other.x - this.x, other.y - this.y);
  }

  abs() {
    // noinspection JSSuspiciousNameCombination
    return Vector2.create(Math.abs(this.x), Math.abs(this.y));
  }

  /**
   * The distance between two vectors
   * @param other The {@link Vector2} of which the distance to this vector is wanted.
   */
  distance(other: Vector2) {
    return Math.hypot(this.x - other.x, this.y - other.y);
  }
}
