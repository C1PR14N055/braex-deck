import { Matrix2D } from "./matrix-2d";

describe("Matrix", () => {
  it("should correctly construct an empty matrix", () => {
    const matrix = Matrix2D.create();
    expect(matrix.a).toBe(1);
    expect(matrix.b).toBe(0);
    expect(matrix.c).toBe(0);
    expect(matrix.d).toBe(1);
    expect(matrix.e).toBe(0);
    expect(matrix.f).toBe(0);
  });

  it("should correctly construct a matrix when using a number", () => {
    const a = 90;
    const matrix = Matrix2D.create(a);
    expect(matrix.a).toBe(a);
    expect(matrix.b).toBe(0);
    expect(matrix.c).toBe(0);
    expect(matrix.d).toBe(1);
    expect(matrix.e).toBe(0);
    expect(matrix.f).toBe(0);
  });

  it("should correctly construct a matrix when using a DOMMatrix", () => {
    const domMatrix = new DOMMatrix([2, 3, 4, 5, 6, 7]);
    const matrix = Matrix2D.create(domMatrix);
    expect(matrix.a).toBe(domMatrix.a);
    expect(matrix.b).toBe(domMatrix.b);
    expect(matrix.c).toBe(domMatrix.c);
    expect(matrix.d).toBe(domMatrix.d);
    expect(matrix.e).toBe(domMatrix.e);
    expect(matrix.f).toBe(domMatrix.f);
  });

  it("should correctly construct a matrix when using an array of numbers", () => {
    const numbers = [9, 12, 3, 6, 2, 1];
    const matrix = Matrix2D.create(numbers);
    expect(matrix.a).toBe(numbers[0]);
    expect(matrix.b).toBe(numbers[1]);
    expect(matrix.c).toBe(numbers[2]);
    expect(matrix.d).toBe(numbers[3]);
    expect(matrix.e).toBe(numbers[4]);
    expect(matrix.f).toBe(numbers[5]);
  });

  it("should correctly construct a matrix from another Matrix2D", () => {
    const other = Matrix2D.create(9, 2, 1, 9, 2, 1);
    const matrix = Matrix2D.create(other);
    expect(matrix.a).toBe(other.a);
    expect(matrix.b).toBe(other.b);
    expect(matrix.c).toBe(other.c);
    expect(matrix.d).toBe(other.d);
    expect(matrix.e).toBe(other.e);
    expect(matrix.f).toBe(other.f);
  });

  it("should throw when constructing with an array of numbers with a smaller length than 6", () => {
    expect(() => {
      Matrix2D.create([1]);
    }).toThrow();
  });

  it("should be equal", () => {
    const a = Matrix2D.create(9, 0, 1, 3, 4, 1);
    const b = Matrix2D.create(9, 0, 1, 3, 4, 1);
    expect(a.equals(b)).toBeTruthy();
  });

  it("should not be equal", () => {
    const a = Matrix2D.create();
    const b = Matrix2D.create(9, 12, 0, 2, 0, 0);
    expect(a.equals(b)).toBeFalsy();
  });

  it("should convert the Matrix2D to a DOMMatrix", () => {
    const matrix = Matrix2D.create(9, 12, 3, 6, 4, 9);
    const actual = matrix.toDOMMatrix();
    expect(actual.a).toBe(9);
    expect(actual.b).toBe(12);
    expect(actual.c).toBe(3);
    expect(actual.d).toBe(6);
    expect(actual.e).toBe(4);
    expect(actual.f).toBe(9);
  });

  it("should convert the Matrix2D to an array of numbers", () => {
    const matrix = Matrix2D.create(17, 9, 12, 3, 4, 1);
    const actual = matrix.toArray();
    expect(actual[0]).toBe(matrix.a);
    expect(actual[1]).toBe(matrix.b);
    expect(actual[2]).toBe(matrix.c);
    expect(actual[3]).toBe(matrix.d);
    expect(actual[4]).toBe(matrix.e);
    expect(actual[5]).toBe(matrix.f);
  });
});
