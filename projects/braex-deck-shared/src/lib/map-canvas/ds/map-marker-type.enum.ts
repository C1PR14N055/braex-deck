export enum MapMarkerType {
  DEFAULT = "default",
  CAR = "car",
  ITEM = "item",
  WEAPON = "weapon"
}
