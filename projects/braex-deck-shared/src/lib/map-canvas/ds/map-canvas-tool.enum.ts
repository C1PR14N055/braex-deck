export enum MapCanvasTool {
  SELECTION = "selection",
  BRUSH = "brush",
  MARKER = "marker",
  ERASER = "eraser"
}
