import { LinePath } from "./line-path";
import { Vector2 } from "./vector2";
import { List } from "immutable";
import { BrushConfig } from "./brush-config";
import { Color } from "./color.enum";

describe("LinePath", () => {
  it("should equal", () => {
    const line1 = LinePath.create(
      List<Vector2>([
        Vector2.create(23, 53),
        Vector2.create(64, 5),
        Vector2.create(8, 52)
      ]),
      new BrushConfig()
    );
    const line2 = LinePath.create(
      List<Vector2>([
        Vector2.create(23, 53),
        Vector2.create(64, 5),
        Vector2.create(8, 52)
      ]),
      new BrushConfig()
    );
    expect(line1.equals(line2)).toBeTruthy();
  });

  it("should not equal when the config is different", () => {
    const line1 = LinePath.create(
      List<Vector2>([
        Vector2.create(23, 53),
        Vector2.create(64, 5),
        Vector2.create(8, 52)
      ]),
      new BrushConfig()
    );
    const line2 = LinePath.create(
      List<Vector2>([
        Vector2.create(23, 53),
        Vector2.create(64, 5),
        Vector2.create(8, 52)
      ]),
      new BrushConfig(63, Color.CINNABAR)
    );
    expect(line1.equals(line2)).toBeFalsy();
  });

  it("should not equal when the points are different", () => {
    const line1 = LinePath.create(
      List<Vector2>([
        Vector2.create(23, 53),
        Vector2.create(64, 5),
        Vector2.create(8, 52)
      ]),
      new BrushConfig()
    );
    const line2 = LinePath.create(
      List<Vector2>([
        Vector2.create(23, 64),
        Vector2.create(4, 2123),
        Vector2.create(0, 23)
      ]),
      new BrushConfig()
    );
    expect(line1.equals(line2)).toBeFalsy();
  });

  it("should return Infinity when linepath is empty", () => {
    const line = LinePath.create();
    const point = Vector2.create(12, 34);

    expect(line.distanceToPosition(point)).toEqual(Infinity);
  });

  it("should assert the distance from a point to a sigle point line is as expected", () => {
    const line = LinePath.create(List<Vector2>([Vector2.create(0, 0)]));
    const point = Vector2.create(0, 42);
    expect(line.distanceToPosition(point)).toEqual(42);
  });

  it("should assert the distance from a point to a multi point line is as expected (acute triangle)", () => {
    const thickness = 1;
    const line = LinePath.create(
      List<Vector2>([Vector2.create(0, 0), Vector2.create(0, 42)]),
      new BrushConfig(thickness)
    );
    const point = Vector2.create(42, 21);
    const expected = 42 - thickness / 2;
    expect(line.distanceToPosition(point)).toBeCloseTo(expected);
  });

  it("should assert the distance from a point to a multi point line is as expected (obtuse triangle 1/2)", () => {
    const thickness = 1;
    const line = LinePath.create(
      List<Vector2>([Vector2.create(40, 10), Vector2.create(10, 10)]),
      new BrushConfig(thickness)
    );
    const point = Vector2.create(60, 30);
    const expected = 28.28 - thickness / 2;

    expect(line.distanceToPosition(point)).toBeCloseTo(expected);
  });

  it("should assert the distance from a point to a multi point line is as expected (obtuse triangle 2/2)", () => {
    const thickness = 1;
    const line = LinePath.create(
      List<Vector2>([Vector2.create(80, 50), Vector2.create(60, 70)]),
      new BrushConfig(thickness)
    );
    const point = Vector2.create(30, 50);
    const expected = 36.06 - thickness / 2;

    expect(line.distanceToPosition(point)).toBeCloseTo(expected);
  });
});
