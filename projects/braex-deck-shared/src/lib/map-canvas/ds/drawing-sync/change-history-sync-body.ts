import { ChangeHistoryEvent } from "../change-history-event";
import { ChangeActionType } from "./change-action-type.enum";

/**
 * The event body of a {@link DrawingSyncEventType.UNDO_PAINT} and {@link DrawingSyncEventType.REDO_PAINT} event.
 */
export class ChangeHistorySyncBody {
  constructor(
    public event: ChangeHistoryEvent<any>,
    public type: ChangeActionType
  ) {}
}
