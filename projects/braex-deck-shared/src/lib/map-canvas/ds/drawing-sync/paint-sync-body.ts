import { Vector2 } from "../vector2";
import { PaintEventState } from "./paint-event-state.enum";
import { BrushConfig } from "../brush-config";

export class PaintSyncBody {
  constructor(
    public state: PaintEventState,
    public position: Vector2,
    public config?: BrushConfig
  ) {}
}
