export interface ExtBrushConfig {
  thickness: number;
  color: string;
}
