import { ExtChangeHistoryEvent } from "../../external/ext-change-history-event";
import { ExtChangeActionType } from "../../external/ext-change-action-type.enum";

export interface ExtChangeHistorySyncBody {
  event: ExtChangeHistoryEvent;
  type: ExtChangeActionType;
}
