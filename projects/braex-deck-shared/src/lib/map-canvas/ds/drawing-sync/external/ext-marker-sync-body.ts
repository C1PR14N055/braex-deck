import { ExtVector2 } from "../../external/ext-vector2";
import { ExtMarkerConfig } from "../../external/ext-marker-config";

export interface ExtMarkerSyncBody {
  position: ExtVector2;
  config: ExtMarkerConfig;
}
