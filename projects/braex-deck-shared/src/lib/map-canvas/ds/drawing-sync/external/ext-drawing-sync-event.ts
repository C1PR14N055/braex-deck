import { ExtDrawingSyncEventType } from "./ext-drawing-sync-event-type";

export interface ExtDrawingSyncEvent {
  type: ExtDrawingSyncEventType;
  /**
   * A string with the json serialized event body.
   */
  details: string;
  /**
   * The PUBG nickname of the owner of this sync event.
   */
  owner: string;
}
