import { ExtPaintEventState } from "./ext-paint-event-state.enum";
import { ExtVector2 } from "../../external/ext-vector2";
import { ExtBrushConfig } from "./ext-brush-config";

export interface ExtPaintSyncBody {
  state: ExtPaintEventState;
  position: ExtVector2;
  config: ExtBrushConfig;
}
