export enum ExtPaintEventState {
  START = "START",
  MIDDLE = "MIDDLE",
  END = "END"
}
