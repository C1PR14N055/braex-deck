import { ExtBrushConfig } from "./ext-brush-config";
import { ExtVector2 } from "../../external/ext-vector2";

export interface ExtLinePath {
  points: ExtVector2[];
  brushConfig: ExtBrushConfig;
}
