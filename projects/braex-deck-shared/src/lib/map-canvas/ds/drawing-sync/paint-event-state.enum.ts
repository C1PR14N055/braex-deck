export enum PaintEventState {
  START = "start",
  MIDDLE = "middle",
  END = "end"
}
