export enum ChangeActionType {
  LINE = "line",
  MARKER = "marker"
}
