import { DrawingSyncEventType } from "./drawing-sync-event-type";

/**
 * A sync event which can be send through a web socket.
 *
 * @param T The type of the event body.
 */
export class DrawingSyncEvent<T> {
  /**
   * @param type The {@link DrawingSyncEventType} of this event.
   * @param body The event body of type {@link T}
   * @param owner The nickname of the owner of this event.
   */
  constructor(
    public type: DrawingSyncEventType,
    public body: T,
    public owner: string
  ) {}
}
