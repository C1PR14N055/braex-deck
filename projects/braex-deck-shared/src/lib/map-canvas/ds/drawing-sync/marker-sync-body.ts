import { Vector2 } from "../vector2";
import { MarkerConfig } from "../marker-config";

export class MarkerSyncBody {
  constructor(public position: Vector2, public config: MarkerConfig) {}
}
