export enum DrawingSyncEventType {
  PAINT = "paint",
  REDO_PAINT = "redo-paint",
  UNDO_PAINT = "undo-paint",
  CLEAR_PAINT = "clear-paint",
  ERASE_PAINT = "erase-paint",
  MARKER = "marker",
  CLEAR_MARKER = "clear-marker",
  OTHER = "other"
}
