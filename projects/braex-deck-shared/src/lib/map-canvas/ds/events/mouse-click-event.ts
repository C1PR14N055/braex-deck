import { MouseButton } from "../../../ds/mouse-button.enum";
import { Vector2 } from "../vector2";

export interface MouseClickEvent {
  position: Vector2;
  mouseButton: MouseButton;
}
