import { MouseWheel } from "./mouse-wheel.enum";
import { Vector2 } from "../vector2";

export interface MouseZoomEvent {
  position: Vector2;
  wheel: MouseWheel;
}
