export enum MouseWheel {
  ZOOM_IN = "zoom-in",
  ZOOM_OUT = "zoom-out"
}
