import { Vector2 } from "./vector2";

export class Vector3 extends Vector2 {
  protected constructor(public x = 0, public y = 0, public z = 0) {
    super(x, y);
  }

  static create(x = 0, y = 0, z = 0) {
    return new Vector3(x, y, z);
  }

  equals(other: Vector3) {
    return super.equals(other) && this.z === other.z;
  }

  add(other: Vector3 | number) {
    const base = super.add(other);
    const result = new Vector3();
    result.x = base.x;
    result.y = base.y;
    if (typeof other === "number") {
      result.z = this.z + other;
    } else {
      result.z = this.z + other.z;
    }

    return Object.freeze(result);
  }

  subtract(other: Vector3 | number) {
    const base = super.subtract(other);
    const result = new Vector3();
    result.x = base.x;
    result.y = base.y;
    if (typeof other === "number") {
      result.z = this.z - other;
    } else {
      result.z = this.z - other.z;
    }

    return Object.freeze(result);
  }

  multiply(other: Vector3 | number) {
    const base = super.multiply(other);
    const result = new Vector3();
    result.x = base.x;
    result.y = base.y;
    if (typeof other === "number") {
      result.z = this.z * other;
    } else {
      result.z = this.z * other.z;
    }

    return Object.freeze(result);
  }

  divide(other: Vector3 | number) {
    const result = new Vector3();
    const base = super.divide(other);
    result.x = base.x;
    result.y = base.y;
    if (typeof other === "number") {
      result.z = this.z / other;
    } else {
      result.z = this.z / other.z;
    }

    return Object.freeze(result);
  }

  diff(other: Vector3) {
    const result = new Vector3();
    const base = super.diff(other);
    result.x = base.x;
    result.y = base.y;
    result.z = other.z - this.z;

    return Object.freeze(result);
  }

  abs() {
    const base = super.abs();

    return Vector3.create(base.x, base.y, Math.abs(this.z));
  }

  distance(other: Vector3): number {
    throw new Error("distance is not implemented on the Vector3");
  }
}
