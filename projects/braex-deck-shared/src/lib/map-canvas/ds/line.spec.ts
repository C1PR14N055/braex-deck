import { Line } from "./line";
import { Vector2 } from "./vector2";

describe("Line", () => {
  it("should return the distance to a line", () => {
    const line = new Line(Vector2.create(0, 0), Vector2.create(0, 500));
    const position = Vector2.create(250, 250);

    expect(line.distanceToPosition(position)).toBeCloseTo(250);
  });
});
