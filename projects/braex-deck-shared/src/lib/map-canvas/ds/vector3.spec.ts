import { Vector3 } from "./vector3";

describe("Vector3", () => {
  it("should be equal", () => {
    const a = Vector3.create(10, 10, 20);
    const b = Vector3.create(10, 10, 20);
    expect(a.equals(b)).toBeTruthy();
  });

  it("should not be equal", () => {
    const a = Vector3.create(10, 5, 20);
    const b = Vector3.create(10, 10, 10);
    expect(a.equals(b)).toBeFalsy();
  });

  it("should add the other point to the current one", () => {
    const a = Vector3.create(10, 5, 15);
    const b = Vector3.create(5, 10, 10);
    expect(a.add(b)).toEqual(Vector3.create(15, 15, 25));
  });

  it("should add a number to the current point", () => {
    const a = Vector3.create(9, 12, 30);
    const b = 4;
    expect(a.add(b)).toEqual(Vector3.create(a.x + b, a.y + b, a.z + b));
  });

  it("should subtract the other point from the current one", () => {
    const a = Vector3.create(20, 10, 5);
    const b = Vector3.create(5, 9, 3);
    expect(a.subtract(b)).toEqual(Vector3.create(15, 1, 2));
  });

  it("should subtract a number from the current point", () => {
    const a = Vector3.create(12, 3, 17);
    const b = 7;
    expect(a.subtract(b)).toEqual(Vector3.create(a.x - b, a.y - b, a.z - b));
  });

  it("should multiply the other point with the current one", () => {
    const a = Vector3.create(16, 9, 21);
    const b = Vector3.create(6, 0, 12);
    expect(a.multiply(b)).toEqual(
      Vector3.create(a.x * b.x, a.y * b.y, a.z * b.z)
    );
  });

  it("should multiply a number with the current point", () => {
    const a = Vector3.create(9, 9, 9);
    const b = 3;
    expect(a.multiply(b)).toEqual(Vector3.create(a.x * b, a.y * b, a.z * b));
  });

  it("should give a positive difference when the comparing point is larger than the current one", () => {
    const a = Vector3.create(39, 39, 104);
    const b = Vector3.create(119, 217, 301);
    expect(a.diff(b)).toEqual(Vector3.create(b.x - a.x, b.y - a.y, b.z - a.z));
  });

  it("should give a negative difference when the comparing point is smaller than the current one", () => {
    const a = Vector3.create(132, 89, 237);
    const b = Vector3.create(16, 23, 42);
    expect(a.diff(b)).toEqual(Vector3.create(b.x - a.x, b.y - a.y, b.z - a.z));
  });

  it("should divide the current point by the given one", () => {
    const a = Vector3.create(16, 19, 15);
    const b = Vector3.create(5, 12, 39);
    const expected = Vector3.create(a.x / b.x, a.y / b.y, a.z / b.z);
    const actual = a.divide(b);
    expect(actual.x).toBeCloseTo(expected.x);
    expect(actual.y).toBeCloseTo(expected.y);
    expect(actual.z).toBeCloseTo(expected.z);
  });

  it("should divide the current point by a number", () => {
    const a = Vector3.create(127, 36, 27);
    const b = 89;
    const expected = Vector3.create(a.x / b, a.y / b, a.z / b);
    const actual = a.divide(b);
    expect(actual.x).toBeCloseTo(expected.x);
    expect(actual.y).toBeCloseTo(expected.y);
    expect(actual.z).toBeCloseTo(expected.z);
  });

  it("should return the magnitude of the negative Points when calling #abs", () => {
    const a = Vector3.create(-123, -56, -41563);
    // noinspection JSSuspiciousNameCombination
    expect(a.abs()).toEqual(
      Vector3.create(Math.abs(a.x), Math.abs(a.y), Math.abs(a.z))
    );
  });

  it("should return the magnitude of the positive Points when calling #abs", () => {
    const a = Vector3.create(315, 8961, 431);
    expect(a.abs()).toEqual(Vector3.create(a.x, a.y, a.z));
  });
});
