import { ChangeEventType } from "./change-event-type.enum";

export class ChangeHistoryEvent<T> {
  constructor(public type: ChangeEventType, public body: T) {}
}
