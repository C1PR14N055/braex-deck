import { fakeAsync, TestBed, tick } from "@angular/core/testing";

import { DrawingSyncService } from "./drawing-sync.service";
import { WebSocketBrokerService } from "../../websockets/web-socket-broker.service";
import { LocalPlayerInfoService } from "../../services/local-player-info.service";
import { FakeLocalPlayerInfoService } from "../../services/testing/fake-local-player-info.service";
import { Subscription } from "rxjs";
import { FakeOverwolfGameEventsService } from "../../overwolf/testing/fake-overwolf-game-events.service";
import { OverwolfGameEventsService } from "../../overwolf/overwolf-game-events.service";
import { PubgMap } from "../../ds/game/pubg-map.enum";
import { OverwolfCoreService } from "../../overwolf/overwolf-core.service";
import { FakeOverwolfCoreService } from "../../overwolf/testing/fake-overwolf-core.service";
import { Vector2 } from "../ds/vector2";
import { BrushConfig } from "../ds/brush-config";
import { LinePath } from "../ds/line-path";
import { List } from "immutable";
import { DrawingSyncEventConverter } from "../converter/internal/drawing-sync-event.converter";
import { PaintSyncBody } from "../ds/drawing-sync/paint-sync-body";
import { DrawingSyncEvent } from "../ds/drawing-sync/drawing-sync-event";
import { PaintEventState } from "../ds/drawing-sync/paint-event-state.enum";
import { ChangeHistorySyncBody } from "../ds/drawing-sync/change-history-sync-body";
import { DrawingSyncEventType } from "../ds/drawing-sync/drawing-sync-event-type";
import { MarkerSyncBody } from "../ds/drawing-sync/marker-sync-body";
import { FakeWebSocketBrokerService } from "../../websockets/testing/fake-web-socket-broker.service";
import { TeamSessionService } from "./team-session.service";
import { MarkerConfig } from "../ds/marker-config";
import { GameMode } from "../../ds/game/game-mode.enum";

describe("DrawingSyncService", () => {
  let service: DrawingSyncService;
  let fakeWebSocket: FakeWebSocketBrokerService;
  let fakeLocalPlayer: FakeLocalPlayerInfoService;
  let fakeGameEvents: FakeOverwolfGameEventsService;
  let subs: Subscription[];
  let fakeCoreService: FakeOverwolfCoreService;
  let syncConverter: DrawingSyncEventConverter;
  let teamSession: TeamSessionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DrawingSyncService,
        {
          provide: WebSocketBrokerService,
          useClass: FakeWebSocketBrokerService
        },
        {
          provide: LocalPlayerInfoService,
          useClass: FakeLocalPlayerInfoService
        },
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        },
        {
          provide: OverwolfCoreService,
          useClass: FakeOverwolfCoreService
        },
        TeamSessionService
      ]
    });
    service = TestBed.get(DrawingSyncService);
    fakeLocalPlayer = TestBed.get(LocalPlayerInfoService);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    fakeCoreService = TestBed.get(OverwolfCoreService);
    fakeWebSocket = TestBed.get(WebSocketBrokerService);
    syncConverter = TestBed.get(DrawingSyncEventConverter);
    teamSession = TestBed.get(TeamSessionService);
    subs = [];
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  function _startSession(localPlayer: string, ...teamNicknames: string[]) {
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(500);

    const team = [localPlayer].concat(
      teamNicknames.filter(x => x !== localPlayer)
    );
    fakeGameEvents.triggerPlayerTeamNicknames(team);
    tick(500);

    const map = PubgMap.DIHOROTOK_MAIN;
    fakeGameEvents.triggerCurrentMap(map);
    tick(400);

    fakeGameEvents.triggerCurrentGameMode(GameMode.DUO);
    tick(500);
  }

  it("should not emit paint events from the local player", fakeAsync(() => {
    const localPlayer = "NicknameOfTheLocalPlayer";
    _startSession(localPlayer, "Config");

    let received: DrawingSyncEvent<PaintSyncBody>;
    const sub = service.paintEvents().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const event = new DrawingSyncEvent<PaintSyncBody>(
      DrawingSyncEventType.PAINT,
      new PaintSyncBody(
        PaintEventState.START,
        Vector2.create(),
        new BrushConfig()
      ),
      localPlayer
    );

    service.sendEvent(event);
    tick(400);
    expect(received).toBeUndefined();
  }));

  it("should not emit paint undo events from the local player", fakeAsync(() => {
    const localPlayer = "LocalPlayerNickname";
    _startSession(localPlayer, "Karma");

    let received: DrawingSyncEvent<ChangeHistorySyncBody>;
    const sub = service.undoEvents().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const event = new DrawingSyncEvent<ChangeHistorySyncBody>(
      DrawingSyncEventType.UNDO_PAINT,
      new ChangeHistorySyncBody(
        LinePath.create(
          List<Vector2>([
            Vector2.create(12, 5),
            Vector2.create(42, 512),
            Vector2.create(798, 31)
          ])
        )
      ),
      localPlayer
    );

    service.sendEvent(event);
    tick(400);
    expect(received).toBeUndefined();
  }));

  it("should not emit paint redo events from the local player", fakeAsync(() => {
    const localPlayer = "TotallyNotTheLocalPlayer";
    _startSession(localPlayer, "Week");

    let received: DrawingSyncEvent<ChangeHistorySyncBody>;
    const sub = service.redoEvents().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const event = new DrawingSyncEvent<ChangeHistorySyncBody>(
      DrawingSyncEventType.REDO_PAINT,
      new ChangeHistorySyncBody(
        LinePath.create(
          List<Vector2>([
            Vector2.create(12, 5),
            Vector2.create(42, 512),
            Vector2.create(798, 31)
          ])
        )
      ),
      localPlayer
    );

    service.sendEvent(event);
    tick(400);
    expect(received).toBeUndefined();
  }));

  it("should not emit paint clear events from the local player", fakeAsync(() => {
    const localPlayer = "LocalPlayer";
    _startSession(localPlayer, "Database");

    let received: DrawingSyncEvent<void>;
    const sub = service.paintClearEvents().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const event = new DrawingSyncEvent<void>(
      DrawingSyncEventType.CLEAR_PAINT,
      null,
      localPlayer
    );
    service.sendEvent(event);
    tick(400);
    expect(received).toBeUndefined();
  }));

  it("should not emit marker events from the local player", fakeAsync(() => {
    const localPlayer = "R8 M8";
    _startSession(localPlayer, "Maven");

    let received: DrawingSyncEvent<MarkerSyncBody>;
    const sub = service.markerEvents().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const event = new DrawingSyncEvent<MarkerSyncBody>(
      DrawingSyncEventType.MARKER,
      new MarkerSyncBody(Vector2.create(123, 45), new MarkerConfig()),
      localPlayer
    );
    service.sendEvent(event);
    tick(400);
    expect(received).toBeUndefined();
  }));

  it("should not emit clear marker events from the local player", fakeAsync(() => {
    const localPlayer = "Kenny";
    _startSession(localPlayer, "Run");

    let received: DrawingSyncEvent<MarkerSyncBody>;
    const sub = service.markerEvents().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const event = new DrawingSyncEvent<void>(
      DrawingSyncEventType.CLEAR_MARKER,
      null,
      localPlayer
    );
    service.sendEvent(event);
    tick(400);
    expect(received).toBeUndefined();
  }));

  it("should emit the paint event of another player", fakeAsync(() => {
    const localPlayer = "TheLocalPlayer";
    const teamMember = "SomeOtherPlayer";
    _startSession(localPlayer, teamMember);

    let received: DrawingSyncEvent<PaintSyncBody>;
    const sub = service.paintEvents().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const event = new DrawingSyncEvent<PaintSyncBody>(
      DrawingSyncEventType.PAINT,
      new PaintSyncBody(
        PaintEventState.START,
        Vector2.create(12, 39),
        new BrushConfig()
      ),
      teamMember
    );

    const broker = teamSession.getDrawingSyncBroker();
    broker.push(syncConverter.convert(event));
    tick(400);

    expect(received).toBeTruthy();
  }));

  it("should emit the paint undo event of another player", fakeAsync(() => {
    const localPlayer = "TheLocalPlayerIGuess?";
    const teamMember = "The team member?? Maybe...";
    _startSession(localPlayer, teamMember);

    let received: DrawingSyncEvent<ChangeHistorySyncBody>;
    const sub = service.undoEvents().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const event = new DrawingSyncEvent<ChangeHistorySyncBody>(
      DrawingSyncEventType.UNDO_PAINT,
      new ChangeHistorySyncBody(
        LinePath.create(
          List<Vector2>([
            Vector2.create(14, 235),
            Vector2.create(73, 123),
            Vector2.create(243, 23),
            Vector2.create(53, 12)
          ])
        )
      ),
      teamMember
    );

    const broker = teamSession.getDrawingSyncBroker();
    broker.push(syncConverter.convert(event));
    tick(400);

    expect(received).toBeTruthy();
  }));

  it("should emit the paint redo event of another player", fakeAsync(() => {
    const localPlayer = "DolphinMan";
    const teamMember = "FishGuy";
    _startSession(localPlayer, teamMember);

    let received: DrawingSyncEvent<ChangeHistorySyncBody>;
    const sub = service.redoEvents().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const event = new DrawingSyncEvent<ChangeHistorySyncBody>(
      DrawingSyncEventType.REDO_PAINT,
      new ChangeHistorySyncBody(
        LinePath.create(
          List<Vector2>([
            Vector2.create(21, 42),
            Vector2.create(0, 420),
            Vector2.create(69, 1337)
          ])
        )
      ),
      teamMember
    );

    const broker = teamSession.getDrawingSyncBroker();
    broker.push(syncConverter.convert(event));
    tick(400);

    expect(received).toBeTruthy();
  }));

  it("should emit the paint clear event of another player", fakeAsync(() => {
    const localPlayer = "HHKB";
    const teamMember = "Pro 2";
    _startSession(localPlayer, teamMember);

    let received: DrawingSyncEvent<void>;
    const sub = service.paintClearEvents().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const event = new DrawingSyncEvent<void>(
      DrawingSyncEventType.CLEAR_PAINT,
      null,
      teamMember
    );

    const broker = teamSession.getDrawingSyncBroker();
    broker.push(syncConverter.convert(event));
    tick(400);

    expect(received).toBeTruthy();
  }));

  it("should emit the marker event of another player", fakeAsync(() => {
    const localPlayer = "Cartman";
    const teamMember = "Stan";
    _startSession(localPlayer, teamMember);

    let received: DrawingSyncEvent<MarkerSyncBody>;
    const sub = service.markerEvents().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const event = new DrawingSyncEvent<MarkerSyncBody>(
      DrawingSyncEventType.MARKER,
      new MarkerSyncBody(Vector2.create(12, 53), new MarkerConfig()),
      teamMember
    );

    const broker = teamSession.getDrawingSyncBroker();
    broker.push(syncConverter.convert(event));
    tick(400);

    expect(received).toBeTruthy();
  }));

  it("should emit the clear marker events of another player", fakeAsync(() => {
    const localPlayer = "Dennis";
    const teamMember = "Ritchie";
    _startSession(localPlayer, teamMember);

    let received: DrawingSyncEvent<void>;
    const sub = service.markerClearEvents().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const event = new DrawingSyncEvent<void>(
      DrawingSyncEventType.CLEAR_MARKER,
      null,
      teamMember
    );

    const broker = teamSession.getDrawingSyncBroker();
    broker.push(syncConverter.convert(event));
    tick(400);

    expect(received).toBeTruthy();
  }));

  it("should send the paint events to the web socket broker", fakeAsync(() => {
    const localPlayer = "Razer";
    const teamMember = "Roccat";
    _startSession(localPlayer, teamMember);

    const broker = teamSession.getDrawingSyncBroker();
    const spy = spyOn(broker, "push");
    const event = new DrawingSyncEvent<PaintSyncBody>(
      DrawingSyncEventType.PAINT,
      new PaintSyncBody(
        PaintEventState.START,
        Vector2.create(12, 39),
        new BrushConfig()
      ),
      localPlayer
    );
    service.sendEvent(event);
    tick(400);

    expect(spy).toHaveBeenCalled();
  }));

  it("should send the paint undo events to the web socket broker", fakeAsync(() => {
    const localPlayer = "BenQ";
    const teamMember = "Huawei";
    _startSession(localPlayer, teamMember);

    const broker = teamSession.getDrawingSyncBroker();
    const spy = spyOn(broker, "push");
    const event = new DrawingSyncEvent<ChangeHistorySyncBody>(
      DrawingSyncEventType.UNDO_PAINT,
      new ChangeHistorySyncBody(
        LinePath.create(
          List<Vector2>([
            Vector2.create(14, 235),
            Vector2.create(73, 123),
            Vector2.create(243, 23),
            Vector2.create(53, 12)
          ])
        )
      ),
      localPlayer
    );
    service.sendEvent(event);
    tick(400);

    expect(spy).toHaveBeenCalled();
  }));

  it("should send the paint redo events to the web socket broker", fakeAsync(() => {
    const localPlayer = "TypeScript";
    const teamMember = "Angular";
    _startSession(localPlayer, teamMember);

    const broker = teamSession.getDrawingSyncBroker();
    const spy = spyOn(broker, "push");
    const event = new DrawingSyncEvent<ChangeHistorySyncBody>(
      DrawingSyncEventType.REDO_PAINT,
      new ChangeHistorySyncBody(
        LinePath.create(
          List<Vector2>([
            Vector2.create(14, 235),
            Vector2.create(73, 123),
            Vector2.create(243, 23),
            Vector2.create(53, 12)
          ])
        )
      ),
      localPlayer
    );
    service.sendEvent(event);
    tick(400);

    expect(spy).toHaveBeenCalled();
  }));

  it("should send the paint clear events to the web socket broker", fakeAsync(() => {
    const localPlayer = "Whiteboard";
    const teamMember = "Dark IDE Theme";
    _startSession(localPlayer, teamMember);

    const broker = teamSession.getDrawingSyncBroker();
    const spy = spyOn(broker, "push");
    const event = new DrawingSyncEvent<void>(
      DrawingSyncEventType.CLEAR_PAINT,
      null,
      localPlayer
    );
    service.sendEvent(event);
    tick(400);

    expect(spy).toHaveBeenCalled();
  }));

  it("should send the marker events to the web socket broker", fakeAsync(() => {
    const localPlayer = "Ubuntu";
    const teamMember = "Manjaro";
    _startSession(localPlayer, teamMember);

    const broker = teamSession.getDrawingSyncBroker();
    const spy = spyOn(broker, "push");
    const event = new DrawingSyncEvent<MarkerSyncBody>(
      DrawingSyncEventType.MARKER,
      new MarkerSyncBody(Vector2.create(42, 534), new MarkerConfig()),
      localPlayer
    );
    service.sendEvent(event);
    tick(400);

    expect(spy).toHaveBeenCalled();
  }));

  it("should send the marker clear events to the web socket broker", fakeAsync(() => {
    const localPlayer = "Google";
    const teamMember = "Amazon";
    _startSession(localPlayer, teamMember);

    const broker = teamSession.getDrawingSyncBroker();
    const spy = spyOn(broker, "push");
    const event = new DrawingSyncEvent<void>(
      DrawingSyncEventType.CLEAR_MARKER,
      null,
      localPlayer
    );
    service.sendEvent(event);
    tick(400);

    expect(spy).toHaveBeenCalled();
  }));

  afterEach(() => {
    subs.forEach(x => {
      x.unsubscribe();
    });
    service.ngOnDestroy();
  });
});
