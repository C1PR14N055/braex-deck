import { TestBed } from "@angular/core/testing";

import { TelemetryParserService } from "./telemetry-parser.service";

describe("TelemetryParserService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: TelemetryParserService = TestBed.get(TelemetryParserService);
    expect(service).toBeTruthy();
  });
});
