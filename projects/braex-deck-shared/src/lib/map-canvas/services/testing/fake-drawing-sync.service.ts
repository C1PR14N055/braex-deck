import { Observable, Observer } from "rxjs";
import { PaintSyncBody } from "../../ds/drawing-sync/paint-sync-body";
import { DrawingSyncEvent } from "../../ds/drawing-sync/drawing-sync-event";
import { ChangeHistorySyncBody } from "../../ds/drawing-sync/change-history-sync-body";
import { MarkerSyncBody } from "../../ds/drawing-sync/marker-sync-body";
import { DrawingSyncEventType } from "../../ds/drawing-sync/drawing-sync-event-type";

export class FakeDrawingSyncService {
  // noinspection JSUnusedGlobalSymbols
  syncActive = false;
  private _paintEventObservers: Observer<
    DrawingSyncEvent<PaintSyncBody>
  >[] = [];
  private _paintUndoEventObservers: Observer<
    DrawingSyncEvent<ChangeHistorySyncBody>
  >[] = [];
  private _paintRedoEventObservers: Observer<
    DrawingSyncEvent<ChangeHistorySyncBody>
  >[] = [];
  private _paintClearEventObservers: Observer<DrawingSyncEvent<void>>[] = [];
  private _markerEventObservers: Observer<
    DrawingSyncEvent<MarkerSyncBody>
  >[] = [];
  private _markerClearEventObservers: Observer<DrawingSyncEvent<void>>[] = [];

  // noinspection JSUnusedGlobalSymbols
  async sendEvent<T>(event: DrawingSyncEvent<T>) {
    switch (event.type) {
      case DrawingSyncEventType.PAINT:
        this.triggerPaintEvent((event as unknown) as DrawingSyncEvent<
          PaintSyncBody
        >);
        break;
      case DrawingSyncEventType.UNDO_PAINT:
        this.triggerPaintUndoEvent((event as unknown) as DrawingSyncEvent<
          ChangeHistorySyncBody
        >);
        break;
      case DrawingSyncEventType.REDO_PAINT:
        this.triggerPaintRedoEvent((event as unknown) as DrawingSyncEvent<
          ChangeHistorySyncBody
        >);
        break;
      case DrawingSyncEventType.CLEAR_PAINT:
        this.triggerPaintClearEvent((event as unknown) as DrawingSyncEvent<
          void
        >);
        break;
      case DrawingSyncEventType.CLEAR_MARKER:
        this.triggerMarkerClearEvent((event as unknown) as DrawingSyncEvent<
          void
        >);
        break;
      case DrawingSyncEventType.MARKER:
        this.triggerMarkerEvent((event as unknown) as DrawingSyncEvent<
          MarkerSyncBody
        >);
        break;
      default:
        throw new Error(
          `Couldn't find an appropriate function for the event type \`${
            event.type
          }\`. Did you forget to add it to the FakeDrawingSyncService?`
        );
    }
  }

  triggerPaintEvent(event: DrawingSyncEvent<PaintSyncBody>) {
    this._paintEventObservers.forEach(x => {
      x.next(event);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  paintEvents(): Observable<DrawingSyncEvent<PaintSyncBody>> {
    return Observable.create(observer => {
      this._paintEventObservers.push(observer);
    });
  }

  triggerPaintUndoEvent(event: DrawingSyncEvent<ChangeHistorySyncBody>) {
    this._paintUndoEventObservers.forEach(x => {
      x.next(event);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  paintUndoEvents(): Observable<DrawingSyncEvent<ChangeHistorySyncBody>> {
    return Observable.create(observer => {
      this._paintUndoEventObservers.push(observer);
    });
  }

  triggerPaintRedoEvent(event: DrawingSyncEvent<ChangeHistorySyncBody>) {
    this._paintRedoEventObservers.forEach(x => {
      x.next(event);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  paintRedoEvents(): Observable<DrawingSyncEvent<ChangeHistorySyncBody>> {
    return Observable.create(observer => {
      this._paintRedoEventObservers.push(observer);
    });
  }

  triggerPaintClearEvent(event: DrawingSyncEvent<void>) {
    this._paintClearEventObservers.forEach(x => {
      x.next(event);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  paintClearEvents(): Observable<DrawingSyncEvent<void>> {
    return Observable.create(observer => {
      this._paintClearEventObservers.push(observer);
    });
  }

  triggerMarkerEvent(event: DrawingSyncEvent<MarkerSyncBody>) {
    this._markerEventObservers.forEach(x => {
      x.next(event);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  markerEvents(): Observable<DrawingSyncEvent<MarkerSyncBody>> {
    return Observable.create(observer => {
      this._markerEventObservers.push(observer);
    });
  }

  triggerMarkerClearEvent(event: DrawingSyncEvent<void>) {
    this._markerClearEventObservers.forEach(x => {
      x.next(event);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  markerClearEvents(): Observable<DrawingSyncEvent<void>> {
    return Observable.create(observer => {
      this._markerClearEventObservers.push(observer);
    });
  }
}
