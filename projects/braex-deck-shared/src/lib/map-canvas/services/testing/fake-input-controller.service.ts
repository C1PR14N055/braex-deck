import { Observable, Observer } from "rxjs";
import { MouseClickEvent } from "../../ds/events/mouse-click-event";
import { Vector2 } from "../../ds/vector2";
import { MouseZoomEvent } from "../../ds/events/mouse-zoom-event";
import { CanvasInputController } from "../../input/canvas-input-controller";

export class FakeInputControllerService {
  private _mouseClickObservers: Observer<MouseClickEvent>[] = [];
  private _mouseDownObservers: Observer<MouseClickEvent>[] = [];
  private _mouseMoveObservers: Observer<Vector2>[] = [];
  private _mouseUpObservers: Observer<MouseClickEvent>[] = [];
  private _mouseWheelObservers: Observer<MouseZoomEvent>[] = [];
  private _sourceController: CanvasInputController;

  // noinspection JSUnusedGlobalSymbols
  bindSourceController(controller: CanvasInputController) {
    this._sourceController = controller;
  }

  triggerMouseClick(event: MouseClickEvent) {
    this._mouseClickObservers.forEach(x => {
      x.next(event);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  mouseClick(): Observable<MouseClickEvent> {
    return Observable.create(observer => {
      this._mouseClickObservers.push(observer);
    });
  }

  triggerMouseDown(event: MouseClickEvent) {
    this._mouseDownObservers.forEach(x => {
      x.next(event);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  mouseDown(): Observable<MouseClickEvent> {
    return Observable.create(observer => {
      this._mouseDownObservers.push(observer);
    });
  }

  triggerMouseMove(position: Vector2) {
    this._mouseMoveObservers.forEach(x => {
      x.next(position);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  mouseMove(): Observable<Vector2> {
    return Observable.create(observer => {
      this._mouseMoveObservers.push(observer);
    });
  }

  triggerMouseUp(event: MouseClickEvent) {
    this._mouseUpObservers.forEach(x => {
      x.next(event);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  mouseUp(): Observable<MouseClickEvent> {
    return Observable.create(observer => {
      this._mouseUpObservers.push(observer);
    });
  }

  triggerMouseWheel(event: MouseZoomEvent) {
    this._mouseWheelObservers.forEach(x => {
      x.next(event);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  mouseWheel(): Observable<MouseZoomEvent> {
    return Observable.create(observer => {
      this._mouseWheelObservers.push(observer);
    });
  }
}
