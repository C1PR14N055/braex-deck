import { Injectable, OnDestroy } from "@angular/core";
import { Observable, Observer, Subscription } from "rxjs";
import { LocalPlayerInfoService } from "../../services/local-player-info.service";
import { WebSocketBroker } from "../../websockets/web-socket-broker";
import { ExtDrawingSyncEvent } from "../ds/drawing-sync/external/ext-drawing-sync-event";
import { ExtDrawingSyncEventConverter } from "../converter/external/ext-drawing-sync-event.converter";
import { InternalError } from "../../error/internal.error";
import { DrawingSyncEventConverter } from "../converter/internal/drawing-sync-event.converter";
import { DrawingSyncEvent } from "../ds/drawing-sync/drawing-sync-event";
import { PaintSyncBody } from "../ds/drawing-sync/paint-sync-body";
import { ChangeHistorySyncBody } from "../ds/drawing-sync/change-history-sync-body";
import { MarkerSyncBody } from "../ds/drawing-sync/marker-sync-body";
import { DrawingSyncEventType } from "../ds/drawing-sync/drawing-sync-event-type";
import { TeamSessionService } from "./team-session.service";

@Injectable({
  providedIn: "root"
})
export class DrawingSyncService implements OnDestroy {
  private _subs: Subscription[] = [];
  private _localPlayerNickname: string | null = null;
  private _broker: WebSocketBroker<ExtDrawingSyncEvent> | null = null;
  private _paintEventObservers: Observer<
    DrawingSyncEvent<PaintSyncBody>
  >[] = [];
  private _undoPaintEventObservers: Observer<
    DrawingSyncEvent<ChangeHistorySyncBody>
  >[] = [];
  private _redoPaintEventObservers: Observer<
    DrawingSyncEvent<ChangeHistorySyncBody>
  >[] = [];
  private _clearPaintEventObservers: Observer<DrawingSyncEvent<void>>[] = [];
  private _markerEventObservers: Observer<
    DrawingSyncEvent<MarkerSyncBody>
  >[] = [];
  private _markerClearEventObservers: Observer<DrawingSyncEvent<void>>[] = [];
  private _brokerStream: Subscription | null = null;

  constructor(
    private _localPlayer: LocalPlayerInfoService,
    private _extSyncConverter: ExtDrawingSyncEventConverter,
    private _syncConverter: DrawingSyncEventConverter,
    private _teamSession: TeamSessionService
  ) {
    let sub = this._localPlayer.localPlayerNickname().subscribe(nickname => {
      this._localPlayerNickname = nickname;
    });
    this._subs.push(sub);

    sub = this._teamSession.connected().subscribe(() => {
      this._broker = this._teamSession.getDrawingSyncBroker();
      this._brokerStream = this._broker.pull().subscribe(event => {
        if (event.owner === this._localPlayerNickname) {
          return;
        }

        this._processIncomingEvent(event);
      });
    });
    this._subs.push(sub);

    sub = this._teamSession.disconnected().subscribe(() => {
      this._broker = null;
      this._brokerStream.unsubscribe();
      this._brokerStream = null;
    });
    this._subs.push(sub);
  }

  ngOnDestroy() {
    if (this._brokerStream) {
      this._brokerStream.unsubscribe();
    }

    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  async sendEvent<T>(event: DrawingSyncEvent<T>) {
    if (!this._teamSession.active) {
      return;
    }

    this._broker.push(this._syncConverter.convert(event));
  }

  private _processIncomingEvent(event: ExtDrawingSyncEvent) {
    const syncEvent = this._extSyncConverter.convert(event);
    switch (syncEvent.type) {
      case DrawingSyncEventType.PAINT:
        this._triggerPaintEvent(syncEvent as DrawingSyncEvent<PaintSyncBody>);
        break;
      case DrawingSyncEventType.UNDO_PAINT:
        this._triggerUndoEvent(syncEvent as DrawingSyncEvent<
          ChangeHistorySyncBody
        >);
        break;
      case DrawingSyncEventType.REDO_PAINT:
        this._triggerRedoEvent(syncEvent as DrawingSyncEvent<
          ChangeHistorySyncBody
        >);
        break;
      case DrawingSyncEventType.CLEAR_PAINT:
        this._triggerPaintClearEvent(syncEvent as DrawingSyncEvent<void>);
        break;
      case DrawingSyncEventType.MARKER:
        this._triggerMarkerEvent(syncEvent as DrawingSyncEvent<MarkerSyncBody>);
        break;
      case DrawingSyncEventType.CLEAR_MARKER:
        this._triggerMarkerClearEvent(syncEvent as DrawingSyncEvent<void>);
        break;
      default:
        throw new InternalError(
          `Can't process the incoming sync event with the event type \`${
            syncEvent.type
          }\`. Did you forget to add i to the DrawingSyncService?`
        );
    }
  }

  private _triggerPaintEvent(event: DrawingSyncEvent<PaintSyncBody>) {
    this._paintEventObservers.forEach(x => {
      x.next(event);
    });
  }

  paintEvents(): Observable<DrawingSyncEvent<PaintSyncBody>> {
    return Observable.create(observer => {
      this._paintEventObservers.push(observer);
    });
  }

  private _triggerUndoEvent(event: DrawingSyncEvent<ChangeHistorySyncBody>) {
    this._undoPaintEventObservers.forEach(x => {
      x.next(event);
    });
  }

  undoEvents(): Observable<DrawingSyncEvent<ChangeHistorySyncBody>> {
    return Observable.create(observer => {
      this._undoPaintEventObservers.push(observer);
    });
  }

  private _triggerRedoEvent(event: DrawingSyncEvent<ChangeHistorySyncBody>) {
    this._redoPaintEventObservers.forEach(x => {
      x.next(event);
    });
  }

  redoEvents(): Observable<DrawingSyncEvent<ChangeHistorySyncBody>> {
    return Observable.create(observer => {
      this._redoPaintEventObservers.push(observer);
    });
  }

  private _triggerPaintClearEvent(event: DrawingSyncEvent<void>) {
    this._clearPaintEventObservers.forEach(x => {
      x.next(event);
    });
  }

  paintClearEvents(): Observable<DrawingSyncEvent<void>> {
    return Observable.create(observer => {
      this._clearPaintEventObservers.push(observer);
    });
  }

  private _triggerMarkerEvent(event: DrawingSyncEvent<MarkerSyncBody>) {
    this._markerEventObservers.forEach(x => {
      x.next(event);
    });
  }

  markerEvents(): Observable<DrawingSyncEvent<MarkerSyncBody>> {
    return Observable.create(observer => {
      this._markerEventObservers.push(observer);
    });
  }

  private _triggerMarkerClearEvent(event: DrawingSyncEvent<void>) {
    this._markerClearEventObservers.forEach(x => {
      x.next(event);
    });
  }

  markerClearEvents(): Observable<DrawingSyncEvent<void>> {
    return Observable.create(observer => {
      this._markerClearEventObservers.push(observer);
    });
  }
}
