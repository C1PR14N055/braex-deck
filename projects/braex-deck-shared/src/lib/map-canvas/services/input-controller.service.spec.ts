import { fakeAsync, TestBed, tick } from "@angular/core/testing";

import { InputControllerService } from "./input-controller.service";
import { FakeInputController } from "../input/testing/fake-input-controller";
import { Subscription } from "rxjs";
import { MouseClickEvent } from "../ds/events/mouse-click-event";
import { MouseButton } from "../../ds/mouse-button.enum";
import { Vector2 } from "../ds/vector2";
import { MouseZoomEvent } from "../ds/events/mouse-zoom-event";
import { MouseWheel } from "../ds/events/mouse-wheel.enum";

describe("InputControllerService", () => {
  let service: InputControllerService;
  let fakeInputController: FakeInputController;
  let subs: Subscription[];

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [InputControllerService] });
    service = TestBed.get(InputControllerService);

    fakeInputController = new FakeInputController();
    service.bindSourceController(fakeInputController as any);

    subs = [];
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should emit a MOUSE CLICK when the bound controller does", fakeAsync(() => {
    let receivedEvent: MouseClickEvent;
    const sub = service.mouseClick().subscribe(e => {
      receivedEvent = e;
    });
    subs.push(sub);

    const event: MouseClickEvent = {
      mouseButton: MouseButton.LMB,
      position: Vector2.create(25, 49)
    };
    fakeInputController.triggerMouseClick(event);
    tick(400);

    expect(receivedEvent).toBe(event);
  }));

  it("should emit a MOUSE DOWN when the bound controller does", fakeAsync(() => {
    let receivedEvent: MouseClickEvent;
    const sub = service.mouseDown().subscribe(e => {
      receivedEvent = e;
    });
    subs.push(sub);

    const event: MouseClickEvent = {
      position: Vector2.create(49, 42),
      mouseButton: MouseButton.MMB
    };
    fakeInputController.triggerMouseDown(event);
    tick(400);

    expect(receivedEvent).toBe(event);
  }));

  it("should emit a MOUSE MOVE when the bound controller does", fakeAsync(() => {
    let receivedPosition: Vector2;
    const sub = service.mouseMove().subscribe(p => {
      receivedPosition = p;
    });
    subs.push(sub);

    const position = Vector2.create(25, 1563);
    fakeInputController.triggerMouseMove(position);
    tick(400);

    expect(receivedPosition).toBe(position);
  }));

  it("should emit a MOUSE UP when the bound controller does", fakeAsync(() => {
    let receivedEvent: MouseClickEvent;
    const sub = service.mouseUp().subscribe(e => {
      receivedEvent = e;
    });
    subs.push(sub);

    const event: MouseClickEvent = {
      mouseButton: MouseButton.LMB,
      position: Vector2.create(416, 15)
    };
    fakeInputController.triggerMouseUp(event);
    tick(400);

    expect(receivedEvent).toBe(event);
  }));

  it("should emit a MOUSE WHEEL when the bond controller does", fakeAsync(() => {
    let receivedEvent: MouseZoomEvent;
    const sub = service.mouseWheel().subscribe(e => {
      receivedEvent = e;
    });
    subs.push(sub);

    const event: MouseZoomEvent = {
      position: Vector2.create(7456, 45),
      wheel: MouseWheel.ZOOM_IN
    };
    fakeInputController.triggerMouseWheel(event);
    tick(400);

    expect(receivedEvent).toBe(event);
  }));

  afterEach(() => {
    subs.forEach(x => {
      x.unsubscribe();
    });
    service.ngOnDestroy();
  });
});
