import { async, fakeAsync, TestBed, tick } from "@angular/core/testing";

import { RtLocalPlayerMovementService } from "./rt-local-player-movement.service";
import { OverwolfGameEventsService } from "../../overwolf/overwolf-game-events.service";
import { FakeOverwolfGameEventsService } from "../../overwolf/testing/fake-overwolf-game-events.service";
import { PlayerMovementCanvasModule } from "../canvas-modules/modules/player-movement.canvas-module";
import { LocalPlayerInfoService } from "../../services/local-player-info.service";
import { FakeLocalPlayerInfoService } from "../../services/testing/fake-local-player-info.service";
import { Vector3 } from "../ds/vector3";
import { MapBackgroundCanvasModule } from "../canvas-modules/modules/map-background.canvas-module";
import { PubgMap } from "../../ds/game/pubg-map.enum";
import { Vector2 } from "../ds/vector2";
import { MapPlayer } from "../ds/map-player";

describe("RtLocalPlayerMovementService", () => {
  let service: RtLocalPlayerMovementService;
  let fakeGameEvents: FakeOverwolfGameEventsService;
  let fakeLocalPlayer: FakeLocalPlayerInfoService;
  let playerMovementModule: PlayerMovementCanvasModule;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        RtLocalPlayerMovementService,
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        },
        PlayerMovementCanvasModule,
        {
          provide: LocalPlayerInfoService,
          useClass: FakeLocalPlayerInfoService
        },
        {
          provide: MapBackgroundCanvasModule,
          useValue: {
            currentMap: PubgMap.ERANGEL_MAIN,
            mapSize: Vector2.create(900, 900)
          }
        }
      ]
    });

    service = TestBed.get(RtLocalPlayerMovementService);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    fakeLocalPlayer = TestBed.get(LocalPlayerInfoService);
    playerMovementModule = TestBed.get(PlayerMovementCanvasModule);
  }));

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should add the local player", fakeAsync(() => {
    const localPlayer = "TechLead";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(200);
    expect(playerMovementModule.players.isEmpty()).toBeTruthy();

    fakeGameEvents.triggerMatchStart();
    tick(200);
    expect(playerMovementModule.players.isEmpty()).toBeTruthy();

    const location = Vector3.create(3003, 2987, 1080);
    fakeGameEvents.triggerLocalPlayerLocationChanged(location);
    tick(200);

    expect(playerMovementModule.players.size).toEqual(1);
    const player = playerMovementModule.players.get(0);
    expect(player.nickname).toEqual(localPlayer);
  }));

  it("should update the local player when it's position changes", fakeAsync(() => {
    const localPlayer = "Beer";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(200);

    fakeGameEvents.triggerMatchStart();
    tick(200);

    const location1 = Vector3.create(5986, 8994, 1235);
    fakeGameEvents.triggerLocalPlayerLocationChanged(location1);
    tick(200);

    const spy = spyOn(playerMovementModule, "updatePlayer");
    const location2 = Vector3.create(1235, 2535, 803);
    fakeGameEvents.triggerLocalPlayerLocationChanged(location2);
    tick(200);

    expect(spy).toHaveBeenCalled();
  }));

  it("should update the local player when he dies", fakeAsync(() => {
    const localPlayer = "Desktop";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(200);

    fakeGameEvents.triggerMatchStart();
    tick(200);

    const location = Vector3.create(1293, 1239, 5932);
    fakeGameEvents.triggerLocalPlayerLocationChanged(location);
    tick(200);

    fakeGameEvents.triggerLocalPlayerDied();
    tick(200);

    const player = playerMovementModule.players.get(0);
    expect(player.alive).toBeFalsy();
  }));

  it("should transform the coordinates of the player when adding him", fakeAsync(() => {
    const localPlayer = "1Play3r";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(200);

    fakeGameEvents.triggerMatchStart();
    tick(200);

    let sendPlayer: MapPlayer;
    spyOn(playerMovementModule, "addPlayer").and.callFake(player => {
      sendPlayer = player;
    });
    const location = Vector3.create(1888, 2389, 1542);
    fakeGameEvents.triggerLocalPlayerLocationChanged(location);
    tick(200);

    expect(sendPlayer.position.x).toBeCloseTo(212.39999999999998);
    expect(sendPlayer.position.y).toBeCloseTo(268.7625);
  }));

  it("should transform the coordinates of the player when updating him", fakeAsync(() => {
    const localPlayer = "Analytics";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(200);

    fakeGameEvents.triggerMatchStart();
    tick(200);

    fakeGameEvents.triggerLocalPlayerLocationChanged(
      Vector3.create(2783, 2934, 2223)
    );
    tick(200);

    let sendPlayer: MapPlayer;
    spyOn(playerMovementModule, "updatePlayer").and.callFake(player => {
      sendPlayer = player;
    });
    fakeGameEvents.triggerLocalPlayerLocationChanged(
      Vector3.create(2750, 2900, 1500)
    );
    tick(200);

    expect(sendPlayer.position.x).toBeCloseTo(309.375);
    expect(sendPlayer.position.y).toBeCloseTo(326.25);
  }));

  it("should remove the local player from the movement when the match ends", fakeAsync(() => {
    const localPlayer = "Medium";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(200);

    fakeGameEvents.triggerMatchStart();
    tick(200);

    fakeGameEvents.triggerLocalPlayerLocationChanged(
      Vector3.create(2783, 2934, 2223)
    );
    tick(200);

    expect(playerMovementModule.players.size).toEqual(1);
    fakeGameEvents.triggerMatchCompleted();
    tick(200);
    expect(playerMovementModule.players.isEmpty()).toBeTruthy();
  }));
});
