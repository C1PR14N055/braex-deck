import { TestBed } from "@angular/core/testing";

import { ChangeHistoryService } from "./change-history.service";

describe("ChangeHistoryService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: ChangeHistoryService = TestBed.get(ChangeHistoryService);
    expect(service).toBeTruthy();
  });
});
