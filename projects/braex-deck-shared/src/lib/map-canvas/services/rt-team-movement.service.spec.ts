import { async, fakeAsync, TestBed, tick } from "@angular/core/testing";
import { RtTeamMovementService } from "./rt-team-movement.service";
import { PlayerMovementCanvasModule } from "../canvas-modules/modules/player-movement.canvas-module";
import { LocalPlayerInfoService } from "../../services/local-player-info.service";
import { FakeLocalPlayerInfoService } from "../../services/testing/fake-local-player-info.service";
import { OverwolfGameEventsService } from "../../overwolf/overwolf-game-events.service";
import { FakeOverwolfGameEventsService } from "../../overwolf/testing/fake-overwolf-game-events.service";
import { PubgMap } from "../../ds/game/pubg-map.enum";
import { Vector3 } from "../ds/vector3";
import { TeamSessionService } from "./team-session.service";
import { WebSocketBrokerService } from "../../websockets/web-socket-broker.service";
import { FakeWebSocketBrokerService } from "../../websockets/testing/fake-web-socket-broker.service";
import { MapBackgroundCanvasModule } from "../canvas-modules/modules/map-background.canvas-module";
import { Vector2 } from "../ds/vector2";
import { GameMode } from "../../ds/game/game-mode.enum";

describe("RtTeamMovementService", () => {
  const localPlayer = "TestLocalPlayer";
  const teamMate = "Other Player";
  let service: RtTeamMovementService;
  let playerMovement: PlayerMovementCanvasModule;
  let fakeLocalPlayer: FakeLocalPlayerInfoService;
  let fakeGameEvents: FakeOverwolfGameEventsService;
  let teamSession: TeamSessionService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        RtTeamMovementService,
        PlayerMovementCanvasModule,
        {
          provide: LocalPlayerInfoService,
          useClass: FakeLocalPlayerInfoService
        },
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        },
        {
          provide: WebSocketBrokerService,
          useClass: FakeWebSocketBrokerService
        },
        TeamSessionService,
        {
          provide: MapBackgroundCanvasModule,
          useValue: {
            mapSize: Vector2.create(900, 900),
            currentMap: PubgMap.SAVAGE_MAIN
          }
        }
      ]
    });

    service = TestBed.get(RtTeamMovementService);
    playerMovement = TestBed.get(PlayerMovementCanvasModule);
    fakeLocalPlayer = TestBed.get(LocalPlayerInfoService);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    teamSession = TestBed.get(TeamSessionService);
  }));

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  function _startSession() {
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(200);

    fakeGameEvents.triggerPlayerTeamNicknames([localPlayer, teamMate]);
    tick(200);

    fakeGameEvents.triggerCurrentMap(PubgMap.DESERT_MAIN);
    tick(500);

    fakeGameEvents.triggerCurrentGameMode(GameMode.DUO);
    tick(500);
  }

  it("should ignore position events of the local player from overwolf", fakeAsync(() => {
    _startSession();

    expect(playerMovement.players.isEmpty()).toBeTruthy();
    fakeGameEvents.triggerLocalPlayerLocationChanged(
      Vector3.create(1234, 3465, 34521)
    );
    tick(500);
    expect(playerMovement.players.isEmpty()).toBeTruthy();
  }));

  it("should ignore death events of the local player from overwolf", fakeAsync(() => {
    _startSession();

    expect(playerMovement.players.isEmpty()).toBeTruthy();
    fakeGameEvents.triggerLocalPlayerLocationChanged(
      Vector3.create(283, 42523, 234)
    );
    tick(200);

    fakeGameEvents.triggerLocalPlayerDied();
    tick(500);
    expect(playerMovement.players.isEmpty()).toBeTruthy();
  }));

  it("should ignore position events of the local player from the web socket", fakeAsync(() => {
    _startSession();

    expect(playerMovement.players.isEmpty()).toBeTruthy();
    const broker = teamSession.getPositionSyncBroker();
    broker.push({
      position: {
        x: 565,
        y: 70853,
        z: 7948
      },
      owner: localPlayer,
      alive: true
    });
    tick(500);

    expect(playerMovement.players.isEmpty()).toBeTruthy();
  }));

  it("should ignore death events of the local player from the web socket", fakeAsync(() => {
    _startSession();

    expect(playerMovement.players.isEmpty()).toBeTruthy();
    const broker = teamSession.getPositionSyncBroker();
    broker.push({
      position: {
        x: 565,
        y: 70853,
        z: 7948
      },
      owner: localPlayer,
      alive: false
    });
    tick(500);

    expect(playerMovement.players.isEmpty()).toBeTruthy();
  }));

  it("should add a team mate", fakeAsync(() => {
    _startSession();

    expect(playerMovement.players.isEmpty()).toBeTruthy();
    const broker = teamSession.getPositionSyncBroker();
    const alive = true;
    broker.push({
      position: {
        x: 2534,
        y: 3524,
        z: 3245
      },
      owner: teamMate,
      alive: alive
    });
    tick(500);

    expect(playerMovement.players.size).toEqual(1);
    const player = playerMovement.players.get(0);
    expect(player.nickname).toEqual(teamMate);
    expect(player.alive).toEqual(alive);
  }));

  it("should update a team mate when his position changes", fakeAsync(() => {
    _startSession();

    expect(playerMovement.players.isEmpty()).toBeTruthy();
    const broker = teamSession.getPositionSyncBroker();
    broker.push({
      position: {
        x: 843,
        y: 9384,
        z: 1256
      },
      owner: teamMate,
      alive: true
    });
    tick(500);

    const spy = spyOn(playerMovement, "updatePlayer");
    broker.push({
      position: {
        x: 983,
        y: 9303,
        z: 2343
      },
      owner: teamMate,
      alive: true
    });
    tick(500);

    expect(spy).toHaveBeenCalled();
  }));

  it("should update a team mate when he dies", fakeAsync(() => {
    _startSession();

    expect(playerMovement.players.isEmpty()).toBeTruthy();
    const broker = teamSession.getPositionSyncBroker();
    broker.push({
      position: {
        x: 8049,
        y: 9384,
        z: 2839
      },
      owner: teamMate,
      alive: true
    });
    tick(500);

    const spy = spyOn(playerMovement, "updatePlayer");
    broker.push({
      position: {
        x: 8049,
        y: 9384,
        z: 2839
      },
      owner: teamMate,
      alive: false
    });
    tick(500);

    expect(spy).toHaveBeenCalled();
  }));

  it("should send the position of the local player", fakeAsync(() => {
    _startSession();

    const broker = teamSession.getPositionSyncBroker();
    const spy = spyOn(broker, "push");

    fakeGameEvents.triggerLocalPlayerLocationChanged(
      Vector3.create(2327, 7543, 4112)
    );
    tick(200);

    expect(spy).toHaveBeenCalled();
  }));

  it("should send the death of the local player", fakeAsync(() => {
    _startSession();

    const broker = teamSession.getPositionSyncBroker();

    fakeGameEvents.triggerLocalPlayerLocationChanged(
      Vector3.create(193, 123, 828)
    );
    tick(200);

    const spy = spyOn(broker, "push");
    fakeGameEvents.triggerLocalPlayerDied();
    tick(200);

    expect(spy).toHaveBeenCalled();
  }));
});
