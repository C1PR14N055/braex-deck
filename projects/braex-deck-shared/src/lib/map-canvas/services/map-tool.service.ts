import { Injectable } from "@angular/core";
import { MapCanvasTool } from "../ds/map-canvas-tool.enum";
import { Observable, Observer } from "rxjs";
import { ToolRegistryService } from "./tool-registry.service";
import { ToolConfig } from "../ds/tool-config";

export const DEFAULT_TOOL = MapCanvasTool.SELECTION;

@Injectable({
  providedIn: "root"
})
export class MapToolService {
  private _activeTool = DEFAULT_TOOL;
  private _activeToolChangedObservers: Observer<MapCanvasTool>[] = [];

  private _triggerActiveToolChanged(tool: MapCanvasTool) {
    this._activeToolChangedObservers.forEach(x => {
      x.next(tool);
    });
  }

  activeToolChanged(): Observable<MapCanvasTool> {
    return Observable.create(observer => {
      this._activeToolChangedObservers.push(observer);
    });
  }

  getToolConfig(tool: MapCanvasTool): ToolConfig | null {
    return ToolRegistryService.getToolConfig(tool);
  }

  get activeTool() {
    return this._activeTool;
  }

  set activeTool(value: MapCanvasTool) {
    const notifyObservers = this._activeTool !== value;
    this._activeTool = value;

    if (notifyObservers) {
      this._triggerActiveToolChanged(this._activeTool);
    }
  }
}
