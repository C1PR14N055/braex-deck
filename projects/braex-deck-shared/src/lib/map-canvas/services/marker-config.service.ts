import { Injectable } from "@angular/core";
import { DEFAULT_MARKER_COLOR, MarkerConfig } from "../ds/marker-config";
import { MapMarkerType } from "../ds/map-marker-type.enum";
import { Color } from "../ds/color.enum";
import { Observable, Observer } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class MarkerConfigService {
  private _color = DEFAULT_MARKER_COLOR;
  private _type = MapMarkerType.DEFAULT;
  private _colorObservers: Observer<Color>[] = [];
  private _typeObservers: Observer<MapMarkerType>[] = [];

  private _triggerColorChanged(color: Color) {
    this._colorObservers.forEach(x => {
      x.next(color);
    });
  }

  colorChanged(): Observable<Color> {
    return Observable.create(observer => {
      this._colorObservers.push(observer);
    });
  }

  private _triggerTypeChanged(type: MapMarkerType) {
    this._typeObservers.forEach(x => {
      x.next(type);
    });
  }

  typeChanged(): Observable<MapMarkerType> {
    return Observable.create(observer => {
      this._typeObservers.push(observer);
    });
  }

  createMarkerConfig() {
    return new MarkerConfig(this._type, this._color);
  }

  get color() {
    return this._color;
  }
  set color(value: Color) {
    const changed = this._color !== value;
    this._color = value;

    if (changed) {
      this._triggerColorChanged(value);
    }
  }

  get type() {
    return this._type;
  }

  set type(value: MapMarkerType) {
    const changed = this._type !== value;
    this._type = value;

    if (changed) {
      this._triggerTypeChanged(value);
    }
  }
}
