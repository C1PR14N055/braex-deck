import { Injectable, OnDestroy } from "@angular/core";
import { CanvasInputController } from "../input/canvas-input-controller";
import { Observable, Observer, Subscription } from "rxjs";
import { MouseClickEvent } from "../ds/events/mouse-click-event";
import { Vector2 } from "../ds/vector2";
import { MouseZoomEvent } from "../ds/events/mouse-zoom-event";

/**
 * Provides the interface of a {@link CanvasInputController} but in the
 * injectable angular context.
 */
@Injectable({
  providedIn: "root"
})
export class InputControllerService implements OnDestroy {
  private _sourceController: CanvasInputController | null = null;
  private _mouseClickObservers: Observer<MouseClickEvent>[] = [];
  private _mouseDownObservers: Observer<MouseClickEvent>[] = [];
  private _mouseMoveObservers: Observer<Vector2>[] = [];
  private _mouseUpObservers: Observer<MouseClickEvent>[] = [];
  private _mouseWheelObservers: Observer<MouseZoomEvent>[] = [];
  private _subs: Subscription[] = [];

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  bindSourceController(controller: CanvasInputController) {
    this._sourceController = controller;
    let sub = this._sourceController.mouseClick().subscribe(e => {
      this._triggerMouseClick(e);
    });
    this._subs.push(sub);

    sub = this._sourceController.mouseDown().subscribe(e => {
      this._triggerMouseDown(e);
    });
    this._subs.push(sub);

    sub = this._sourceController.mouseMove().subscribe(p => {
      this._triggerMouseMove(p);
    });
    this._subs.push(sub);

    sub = this._sourceController.mouseUp().subscribe(e => {
      this._triggerMouseUp(e);
    });
    this._subs.push(sub);

    sub = this._sourceController.mouseWheel().subscribe(e => {
      this._triggerMouseWheel(e);
    });
    this._subs.push(sub);
  }

  unbindSourceController() {
    this._sourceController.destroy();
    this._sourceController = null;
  }

  private _triggerMouseClick(event: MouseClickEvent) {
    this._mouseClickObservers.forEach(x => {
      x.next(event);
    });
  }

  mouseClick(): Observable<MouseClickEvent> {
    return Observable.create(observer => {
      this._mouseClickObservers.push(observer);
    });
  }

  private _triggerMouseDown(event: MouseClickEvent) {
    this._mouseDownObservers.forEach(x => {
      x.next(event);
    });
  }

  mouseDown(): Observable<MouseClickEvent> {
    return Observable.create(observer => {
      this._mouseDownObservers.push(observer);
    });
  }

  private _triggerMouseMove(position: Vector2) {
    this._mouseMoveObservers.forEach(x => {
      x.next(position);
    });
  }

  mouseMove(): Observable<Vector2> {
    return Observable.create(observer => {
      this._mouseMoveObservers.push(observer);
    });
  }

  private _triggerMouseUp(event: MouseClickEvent) {
    this._mouseUpObservers.forEach(x => {
      x.next(event);
    });
  }

  mouseUp(): Observable<MouseClickEvent> {
    return Observable.create(observer => {
      this._mouseUpObservers.push(observer);
    });
  }

  private _triggerMouseWheel(event: MouseZoomEvent) {
    this._mouseWheelObservers.forEach(x => {
      x.next(event);
    });
  }

  mouseWheel(): Observable<MouseZoomEvent> {
    return Observable.create(observer => {
      this._mouseWheelObservers.push(observer);
    });
  }
}
