import { Injectable, OnDestroy } from "@angular/core";
import { Observable, Observer, Subscription, zip } from "rxjs";
import { WebSocketBroker } from "../../websockets/web-socket-broker";
import { LocalPlayerInfoService } from "../../services/local-player-info.service";
import { PubgMap } from "../../ds/game/pubg-map.enum";
import { List } from "immutable";
import * as uuidv5 from "uuid/v5";
import { OverwolfGameEventsService } from "../../overwolf/overwolf-game-events.service";
import { WebSocketBrokerService } from "../../websockets/web-socket-broker.service";
import { OverwolfCoreService } from "../../overwolf/overwolf-core.service";
import { environment } from "../../../environments/environment";
import * as format from "string-format";
import { InternalError } from "../../error/internal.error";
import { ExtDrawingSyncEvent } from "../ds/drawing-sync/external/ext-drawing-sync-event";
import { ExtTeamPositionSync } from "../ds/position-sync/external/ext-team-position-sync";
import { GameMode } from "../../ds/game/game-mode.enum";
import { sortBy } from "lodash";

export const SESSION_ID_NAMESPACE = "f85a8b58-e04d-41d0-891b-abad85f2c435";
export const SEPARATOR = "&&";

@Injectable({
  providedIn: "root"
})
export class TeamSessionService implements OnDestroy {
  private _subs: Subscription[] = [];
  private _active = false;
  private _sessionId: string | null = null;
  private _localPlayerNickname: string | null = null;
  private _currentMap: PubgMap | null = null;
  private _team = List<string>();
  private _currentGameMode: GameMode | null = null;
  private _positionSyncBroker: WebSocketBroker<
    ExtTeamPositionSync
  > | null = null;
  private _drawingSyncBroker: WebSocketBroker<
    ExtDrawingSyncEvent
  > | null = null;
  private _connectedObservers: Observer<string>[] = [];
  private _disconnectedObservers: Observer<string>[] = [];

  constructor(
    private _localPlayer: LocalPlayerInfoService,
    private _gameEvents: OverwolfGameEventsService,
    private _webSocket: WebSocketBrokerService,
    private _overwolfCore: OverwolfCoreService
  ) {
    let sub = this._localPlayer.localPlayerNickname().subscribe(nickname => {
      this._localPlayerNickname = nickname;
      this._tryBuildingConnection();
    });
    this._subs.push(sub);

    sub = this._gameEvents.currentMap().subscribe(m => {
      this._currentMap = m;
      this._tryBuildingConnection();
    });
    this._subs.push(sub);

    sub = this._gameEvents.playerTeamNicknames().subscribe(team => {
      this._team = this._team.push(...team.toArray());
      this._tryBuildingConnection();
    });
    this._subs.push(sub);

    sub = this._gameEvents.currentGameMode().subscribe(gameMode => {
      this._currentGameMode = gameMode;
      this._tryBuildingConnection();
    });
    this._subs.push(sub);

    sub = this._gameEvents.onMatchLeft().subscribe(() => {
      if (this._active) {
        this.disconnect();
      }
    });
    this._subs.push(sub);

    sub = this._overwolfCore.onPubgTerminated().subscribe(() => {
      if (this._active) {
        this.disconnect();
      }
    });
    this._subs.push(sub);
  }

  private _tryBuildingConnection() {
    console.log("Trying to create a team session");
    if (this._active) {
      console.warn(
        "A session is currently active so a new one won't be started automatically"
      );

      return;
    }
    if (!this._localPlayerNickname) {
      console.warn(
        "Nickname of the local player is missing... Session won't be started"
      );

      return;
    }
    if (!this._currentMap) {
      console.warn("Current map is missing... Session won't be started");

      return;
    }
    if (this._currentGameMode === GameMode.SOLO) {
      console.warn("GameMode -> SOLO... No Team mates expected");
    } else if (!this._currentGameMode) {
      console.warn("GameMode missing... Session won't be started");

      return;
    } else {
      console.warn(
        `GameMode -> ${this._currentGameMode.toUpperCase()}... Team mates required`
      );
      if (this._team.isEmpty()) {
        console.warn(
          "The team of the local player is missing... Session won't be started"
        );

        return;
      }
    }

    console.log("Creating a team session");
    this.connect(this._computeSessionId());
  }

  private _computeSessionId() {
    if (this._currentGameMode === GameMode.SOLO) {
      console.log(
        `Computing session from the following values: ${JSON.stringify({
          map: this._currentMap,
          gameMode: this._currentGameMode,
          localPlayer: this._localPlayerNickname
        })}`
      );

      return uuidv5(
        [
          this._currentMap,
          this._currentGameMode,
          this._localPlayerNickname
        ].join(SEPARATOR),
        SESSION_ID_NAMESPACE
      );
    }
    console.log(
      `Computing session from the following values: ${JSON.stringify({
        map: this._currentMap,
        gameMode: this._currentGameMode,
        team: this._team.toArray()
      })}`
    );

    return uuidv5(
      [
        this._currentMap,
        this._currentGameMode,
        ...sortBy(this._team.toArray())
      ].join(SEPARATOR),
      SESSION_ID_NAMESPACE
    );
  }

  getPositionSyncBroker(): WebSocketBroker<ExtTeamPositionSync> | null {
    return this._positionSyncBroker;
  }

  getDrawingSyncBroker(): WebSocketBroker<ExtDrawingSyncEvent> | null {
    return this._drawingSyncBroker;
  }

  connect(sessionId: string) {
    if (this._active) {
      this.disconnect();
    }

    this._drawingSyncBroker = this._webSocket.createBroker({
      brokerUrl: environment.webSockets.drawingSync.brokerUrl,
      pushPath: format(environment.webSockets.drawingSync.pushPath, {
        sessionId: sessionId
      }),
      pullPath: format(environment.webSockets.drawingSync.pullPath, {
        sessionId: sessionId
      })
    });
    this._positionSyncBroker = this._webSocket.createBroker({
      brokerUrl: environment.webSockets.positionSync.brokerUrl,
      pushPath: format(environment.webSockets.positionSync.pushPath, {
        sessionId: sessionId
      }),
      pullPath: format(environment.webSockets.positionSync.pullPath, {
        sessionId: sessionId
      })
    });
    zip(
      this._drawingSyncBroker.connected(),
      this._positionSyncBroker.connected()
    ).subscribe(
      () => {
        this._active = true;
        this._sessionId = sessionId;
        this._triggerConnected();
        console.log(`Connected to session: ${this._sessionId}`);
      },
      error => {
        console.error(error);
        throw new InternalError(`Failed to connect to a broker`);
      }
    );
  }

  private _triggerConnected() {
    this._connectedObservers.forEach(x => {
      x.next(this._sessionId);
    });
  }

  connected(): Observable<string> {
    return Observable.create(observer => {
      this._connectedObservers.push(observer);
    });
  }

  disconnect() {
    if (!this._active) {
      return;
    }

    const oldSessionId = this._sessionId;
    this._drawingSyncBroker.destroy();
    this._drawingSyncBroker = null;
    this._positionSyncBroker.destroy();
    this._positionSyncBroker = null;
    this._team = this._team.clear();
    this._currentMap = null;
    this._currentGameMode = null;
    this._active = false;
    this._sessionId = null;
    this._triggerDisconnected(oldSessionId);
  }

  private _triggerDisconnected(sessionId: string) {
    this._disconnectedObservers.forEach(x => {
      x.next(sessionId);
    });
  }

  disconnected(): Observable<string> {
    return Observable.create(observer => {
      this._disconnectedObservers.push(observer);
    });
  }

  ngOnDestroy() {
    if (this._active) {
      this.disconnect();
    }

    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  /**
   * Whether the connection to a session is currently active.
   */
  get active() {
    return this._active;
  }

  /**
   * The id of the active session.
   */
  get sessionId() {
    return this._sessionId;
  }
}
