import { TestBed } from "@angular/core/testing";

import { BrushConfigService } from "./brush-config.service";

describe("BrushConfigService", () => {
  let service: BrushConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [BrushConfigService] });
    service = TestBed.get(BrushConfigService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
