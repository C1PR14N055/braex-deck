import { Injectable } from "@angular/core";
import { CanvasResource } from "../ds/canvas-resource";
import { Observable } from "rxjs";
import { ResourceNotFoundError } from "../../error/resource-not-found.error";
import { ResourceLoadError } from "../../error/resource-load.error";

@Injectable({
  providedIn: "root"
})
export class ResourceLoadingService {
  private _loadedImages: CanvasResource[] = [];

  loadImage(path: string): Observable<CanvasResource> {
    return Observable.create(observer => {
      try {
        observer.next(this.getImage(path));
      } catch (e) {
        if (e instanceof ResourceNotFoundError) {
          const image = new Image();
          image.onload = () => {
            const resource = CanvasResource.create(path, image);
            this._loadedImages.push(resource);
            observer.next(resource);
          };
          image.onerror = () => {
            observer.error(
              new ResourceLoadError(
                `Failed to load the resource under the path \`${path}\``
              )
            );
          };
          image.src = path;
        }
      }
    });
  }

  getImage(path: string) {
    const matchingResource = this._loadedImages.filter(x => x.path === path);
    if (matchingResource.length) {
      return matchingResource[0];
    }

    throw new ResourceNotFoundError(
      `No resource was found with the path \`${path}\`... Did you forget to load it?`
    );
  }
}
