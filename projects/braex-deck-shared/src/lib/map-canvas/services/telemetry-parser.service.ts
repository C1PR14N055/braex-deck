import { Injectable } from "@angular/core";
import { HttpClient, HttpEventType, HttpRequest } from "@angular/common/http";
import { TelemetryEventType } from "../ds/telemetry/enums/telemetry-event-type.enum";
import { TimelineTelemetryEvents } from "../ds/telemetry/timeline-telemetry-event";
import { Observable } from "rxjs";
import { TelemetryEventMatchStart } from "../ds/telemetry/events/telemetry-event-match-start";
import { TelemetryEventGameStatePeriodic } from "../ds/telemetry/events/telemetry-event-game-state-periodic";
import { TelemetryCharacter } from "../ds/telemetry/objects/telemetry-character";
export const TIMESTAMP_FORMAT = "HHmmss";

export enum TelemetryParserState {
  DOWNLOADING = "downloading",
  PARSING = "parsing",
  DONE = "done"
}

@Injectable({
  providedIn: "root"
})
export class TelemetryParserService {
  private _timelineTelemetryEvents = new TimelineTelemetryEvents();
  private _lastTelemetryCharacters: TelemetryCharacter[] = [];

  // represents the number of seconds the match started at
  // ie: 19:36:39 => 70599
  private _originalMatchStartSeconds = 0;

  constructor(private _http: HttpClient) {}

  downloadAndParseTelemetryData(
    telemetryUrl: string
  ): Observable<TelemetryParserState | TimelineTelemetryEvents> {
    return Observable.create(observer => {
      const req = new HttpRequest("GET", telemetryUrl, {
        reportProgress: true,
        observe: "events"
      });
      this._http.request<any[]>(req).subscribe(event => {
        if (
          event.type === HttpEventType.Sent ||
          event.type === HttpEventType.DownloadProgress
        ) {
          observer.next(TelemetryParserState.DOWNLOADING);
        } else if (event.type === HttpEventType.Response) {
          observer.next(TelemetryParserState.PARSING);

          for (let i = 0; i < event.body.length; i++) {
            const source = event.body[i];

            // skip all loby events as we dont need them
            // unless they are player create events
            if (
              this._timelineTelemetryEvents.matchStartEvent === null &&
              source._T !== TelemetryEventType.LOG_MATCH_START
            ) {
              continue;
            }

            switch (source._T) {
              case TelemetryEventType.LOG_MATCH_DEFINITION: {
                // TODO:
                break;
              }
              case TelemetryEventType.LOG_MATCH_START: {
                this._timelineTelemetryEvents.matchStartEvent = new TelemetryEventMatchStart(
                  source
                );
                this._originalMatchStartSeconds = this._timestampToSeconds(
                  source._D
                );
                break;
              }
              case TelemetryEventType.LOG_MATCH_END: {
                this._timelineTelemetryEvents.matchDuration = this._timestampToSeconds(
                  source._D
                );
                break;
              }
              case TelemetryEventType.LOG_PLAYER_LOGIN: {
                // TODO:
                break;
              }
              case TelemetryEventType.LOG_PLAYER_LOGOUT: {
                this._deleteCharacter(source.accountID);
                break;
              }
              case TelemetryEventType.LOG_PLAYER_CREATE:
              case TelemetryEventType.LOG_PLAYER_POSITION: {
                this._setCharacterAtMoment(
                  source._D,
                  new TelemetryCharacter(source.character)
                );
                break;
              }
              case TelemetryEventType.LOG_PLAYER_KILL: {
                this._setCharacterAtMoment(
                  source._D,
                  new TelemetryCharacter(source.victim)
                );
                break;
              }
              case TelemetryEventType.LOG_PLAYER_ATTACK: {
                // TODO:
                break;
              }
              case TelemetryEventType.LOG_PLAYER_REVIVE: {
                // TODO:
                break;
              }
              case TelemetryEventType.LOG_GAME_STATE_PERIODIC: {
                this._setGameStateAtMoment(source._D, source.gameState);

                break;
              }
            }
          }

          observer.next(TelemetryParserState.DONE);
          observer.next(this._timelineTelemetryEvents);
          observer.complete();
        }
      });
    });
  }

  // create get prev next time with events
  private _setCharacterAtMoment(
    timestamp: string,
    character: TelemetryCharacter
  ) {
    const charIndex = this._lastTelemetryCharacters.findIndex(
      ch => character.accountId === ch.accountId
    );
    if (charIndex >= 0) {
      const newTelemCh = [...this._lastTelemetryCharacters];
      newTelemCh[charIndex] = character;
      this._timelineTelemetryEvents.charactersAtMoment.set(
        this._timestampToSeconds(timestamp),
        newTelemCh
      );
      this._lastTelemetryCharacters = newTelemCh;
    } else {
      this._lastTelemetryCharacters.push(character);
      this._timelineTelemetryEvents.charactersAtMoment.set(
        this._timestampToSeconds(timestamp),
        this._lastTelemetryCharacters
      );
    }
  }

  private _deleteCharacter(accountId: string) {
    const charIndex = this._lastTelemetryCharacters.findIndex(
      ch => accountId === ch.accountId
    );
    if (charIndex >= 0) {
      this._lastTelemetryCharacters.splice(charIndex, 1);
    }
  }

  private _setGameStateAtMoment(timestamp: string, gameState: {}) {
    this._timelineTelemetryEvents.gameStateAtMoment.set(
      this._timestampToSeconds(timestamp),
      new TelemetryEventGameStatePeriodic(gameState)
    );
  }

  /**
   * Receives a time of 2019-05-15T06:42:47.997Z format
   * Transforms it to a time of 22:30:15 format,
   * Splits each value by the : separator
   * Adds them together to returns time in seconds.
   * The reducer would also work with MM:SS formats as:
   * 1 H = 60 M and 1 M = 60 sec
   * @param timestamp 22:30:15 | 32:10 formatted strings
   * @return total number of seconds in time adjusted to start from 0
   */
  private _timestampToSeconds(timestamp: string): number {
    timestamp = timestamp.substr(11, 8); // 11, 12 for millis too

    return (
      // @ts-ignore
      // +time converts time ie: "05" to number ie: 5 and the linter complains about string + number
      timestamp.split(":").reduce((acc, time) => 60 * acc + +time) -
      this._originalMatchStartSeconds
    );
  }
}
