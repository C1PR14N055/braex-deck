import { TestBed } from "@angular/core/testing";

import { TelemetryActiveFeedService } from "./telemetry-active-feed.service";

describe("TelemetryActiveFeedService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: TelemetryActiveFeedService = TestBed.get(
      TelemetryActiveFeedService
    );
    expect(service).toBeTruthy();
  });
});
