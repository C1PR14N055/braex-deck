import { fakeAsync, TestBed, tick } from "@angular/core/testing";

import { MarkerConfigService } from "./marker-config.service";
import { Color } from "../ds/color.enum";
import { Subscription } from "rxjs";
import { MapMarkerType } from "../ds/map-marker-type.enum";

describe("MarkerConfigService", () => {
  let service: MarkerConfigService;
  let subs: Subscription[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MarkerConfigService]
    });

    service = TestBed.get(MarkerConfigService);
    subs = [];
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should notify all observers when the color changes", fakeAsync(() => {
    let received: Color;
    const sub = service.colorChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const color = Color.CINNABAR;
    service.color = color;
    tick(200);
    expect(received).toEqual(color);
  }));

  it("should notify all observers when the type changes", fakeAsync(() => {
    let received: MapMarkerType;
    const sub = service.typeChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const type = MapMarkerType.WEAPON;
    service.type = type;
    tick(200);
    expect(received).toEqual(type);
  }));

  it("should create the marker config", () => {
    service.type = MapMarkerType.ITEM;
    service.color = Color.JAFFA;

    const actual = service.createMarkerConfig();
    expect(actual.color).toEqual(service.color);
    expect(actual.type).toEqual(service.type);
  });

  afterEach(() => {
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
