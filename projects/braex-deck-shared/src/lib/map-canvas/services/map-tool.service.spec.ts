import { fakeAsync, TestBed, tick } from "@angular/core/testing";

import { MapToolService } from "./map-tool.service";
import { Subscription } from "rxjs";
import { MapCanvasTool } from "../ds/map-canvas-tool.enum";
import { OverwolfCoreService } from "../../overwolf/overwolf-core.service";
import { FakeOverwolfCoreService } from "../../overwolf/testing/fake-overwolf-core.service";

describe("MapToolService", () => {
  let service: MapToolService;
  let subs: Subscription[] = [];
  let fakeOverwolfCore: FakeOverwolfCoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MapToolService,
        { provide: OverwolfCoreService, useClass: FakeOverwolfCoreService }
      ]
    });
    service = TestBed.get(MapToolService);
    fakeOverwolfCore = TestBed.get(OverwolfCoreService);
    subs = [];
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should notify all observers when the active tool changes", fakeAsync(() => {
    let receivedTool: MapCanvasTool | null = null;
    const sub = service.activeToolChanged().subscribe(t => {
      receivedTool = t;
    });
    subs.push(sub);

    expect(service.activeTool).toBe(MapCanvasTool.SELECTION);
    service.activeTool = MapCanvasTool.BRUSH;
    tick(300);
    expect(receivedTool).toBe(service.activeTool);
  }));

  afterEach(() => {
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
