import { TestBed } from "@angular/core/testing";

import { ResourceLoadingService } from "./resource-loading.service";
import { ResourceLoadError } from "../../error/resource-load.error";

describe("ResourceLoadingService", () => {
  let service: ResourceLoadingService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [ResourceLoadingService] });
    service = TestBed.get(ResourceLoadingService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should load an image", (done: DoneFn) => {
    const path = "/assets/img/map-markers/map-marker.png";
    service.loadImage(path).subscribe(res => {
      expect(res.path).toBe(path);
      expect(res.image).toBeTruthy();
      done();
    });
  });

  it("should NOT load the image since the path is wrong", (done: DoneFn) => {
    service
      .loadImage("/assets/some/path/which/does/not/exist/test.png")
      .subscribe(
        () => {},
        error1 => {
          expect(error1 instanceof ResourceLoadError).toBeTruthy();
          done();
        }
      );
  });

  it("should get a resource which has the same path when calling #getImage", (done: DoneFn) => {
    const path = "/assets/img/map-markers/map-marker.png";
    service.loadImage(path).subscribe(() => {
      const image = service.getImage(path);
      expect(image.path).toBe(path);
      expect(image.image).toBeTruthy();
      done();
    });
  });

  it("should throw an error when no loaded image has the same path when calling #getImage", () => {
    expect(() => {
      service.getImage("/assets/some/path/that/does/not/exist/test.png");
    }).toThrow();
  });
});
