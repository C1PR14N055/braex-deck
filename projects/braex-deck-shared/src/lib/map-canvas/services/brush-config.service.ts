import { Injectable } from "@angular/core";
import { Color } from "../ds/color.enum";
import { BrushConfig } from "../ds/brush-config";

@Injectable({
  providedIn: "root"
})
export class BrushConfigService {
  private _brushConfig = new BrushConfig();

  createBrushConfig(): BrushConfig {
    return new BrushConfig(
      this._brushConfig.thickness,
      this._brushConfig.color
    );
  }

  get thickness() {
    return this._brushConfig.thickness;
  }

  set thickness(value: number) {
    this._brushConfig.thickness = value;
  }

  get color() {
    return this._brushConfig.color;
  }

  set color(value: Color) {
    this._brushConfig.color = value;
  }
}
