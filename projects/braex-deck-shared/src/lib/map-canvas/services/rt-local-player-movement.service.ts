import { Injectable, OnDestroy } from "@angular/core";
import { PlayerMovementCanvasModule } from "../canvas-modules/modules/player-movement.canvas-module";
import { Subscription } from "rxjs";
import { LocalPlayerInfoService } from "../../services/local-player-info.service";
import { OverwolfGameEventsService } from "../../overwolf/overwolf-game-events.service";
import { MapPlayer } from "../ds/map-player";
import { Vector2 } from "../ds/vector2";
import { InGameUtils } from "../utils/in-game-utils";
import { MapBackgroundCanvasModule } from "../canvas-modules/modules/map-background.canvas-module";
import { OverwolfCoreService } from "../../overwolf/overwolf-core.service";

@Injectable({
  providedIn: "root"
})
export class RtLocalPlayerMovementService implements OnDestroy {
  private _subs: Subscription[] = [];
  private _localPlayerNickname: string | null = null;
  private _matchStarted = false;
  private _addedLocalPlayer = false;
  private _lastPlayerLocation: Vector2 | null = null;

  constructor(
    private _movementCanvasModule: PlayerMovementCanvasModule,
    private _localPlayer: LocalPlayerInfoService,
    private _gameEvents: OverwolfGameEventsService,
    private _mapBg: MapBackgroundCanvasModule,
    private _overwolfCore: OverwolfCoreService
  ) {
    let sub = this._localPlayer.localPlayerNickname().subscribe(nickname => {
      this._localPlayerNickname = nickname;
    });
    this._subs.push(sub);

    sub = this._gameEvents.onMatchStart().subscribe(() => {
      this._matchStarted = true;
    });
    this._subs.push(sub);

    // TODO(pmo): adjust the test from onMatchCompleted to onMatchLeft
    sub = this._gameEvents.onMatchLeft().subscribe(() => {
      this._clearLocalPlayer();
    });
    this._subs.push(sub);

    // TODO(pmo): add a test to see if the player is cleared when pubg is terminated
    sub = this._overwolfCore.onPubgTerminated().subscribe(() => {
      this._clearLocalPlayer();
    });
    this._subs.push(sub);

    sub = this._gameEvents
      .onLocalPlayerLocationChanged()
      .subscribe(location => {
        const transformedLocation = InGameUtils.overwolfMapLocationToCanvasSpace(
          this._mapBg.currentMap,
          this._mapBg.mapSize,
          location
        );

        if (this._addedLocalPlayer) {
          this._movementCanvasModule.updatePlayer(
            MapPlayer.create(this._localPlayerNickname, transformedLocation)
          );
        } else {
          this._movementCanvasModule.addPlayer(
            MapPlayer.create(this._localPlayerNickname, transformedLocation)
          );
          this._addedLocalPlayer = true;
        }

        this._lastPlayerLocation = transformedLocation;
      });
    this._subs.push(sub);

    sub = this._gameEvents.onLocalPlayerDied().subscribe(() => {
      this._movementCanvasModule.updatePlayer(
        MapPlayer.create(
          this._localPlayerNickname,
          this._lastPlayerLocation,
          false
        )
      );
    });
    this._subs.push(sub);
  }

  private _clearLocalPlayer() {
    this._matchStarted = false;
    this._addedLocalPlayer = false;
    this._movementCanvasModule.removePlayer(this._localPlayerNickname);
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }
}
