import { Injectable, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { TeamSessionService } from "./team-session.service";
import { WebSocketBroker } from "../../websockets/web-socket-broker";
import { ExtTeamPositionSync } from "../ds/position-sync/external/ext-team-position-sync";
import { PlayerMovementCanvasModule } from "../canvas-modules/modules/player-movement.canvas-module";
import { LocalPlayerInfoService } from "../../services/local-player-info.service";
import { Vector3 } from "../ds/vector3";
import { OverwolfGameEventsService } from "../../overwolf/overwolf-game-events.service";
import { TeamPositionSync } from "../ds/position-sync/team-position-sync";
import { TeamPositionSyncConverter } from "../converter/internal/team-position-sync.converter";
import { List } from "immutable";
import { ExtTeamPositionSyncConverter } from "../converter/external/ext-team-position-sync.converter";
import { MapPlayer } from "../ds/map-player";
import { MapBackgroundCanvasModule } from "../canvas-modules/modules/map-background.canvas-module";
import { InGameUtils } from "../utils/in-game-utils";

@Injectable({
  providedIn: "root"
})
export class RtTeamMovementService implements OnDestroy {
  private _subs: Subscription[] = [];
  private _broker: WebSocketBroker<ExtTeamPositionSync> | null = null;
  private _brokerStream: Subscription | null = null;
  private _localPlayerNickname: string | null = null;
  private _addedMateNicknames = List<string>();
  private _lastPosition: Vector3 | null = null;

  constructor(
    private _teamSession: TeamSessionService,
    private _playerMovement: PlayerMovementCanvasModule,
    private _localPlayer: LocalPlayerInfoService,
    private _gameEvents: OverwolfGameEventsService,
    private _teamSyncConverter: TeamPositionSyncConverter,
    private _extTeamSyncConverter: ExtTeamPositionSyncConverter,
    private _mapBg: MapBackgroundCanvasModule
  ) {
    let sub = this._localPlayer.localPlayerNickname().subscribe(nickname => {
      this._localPlayerNickname = nickname;
    });
    this._subs.push(sub);

    sub = this._teamSession.connected().subscribe(() => {
      this._broker = this._teamSession.getPositionSyncBroker();
      this._brokerStream = this._broker.pull().subscribe(event => {
        if (event.owner === this._localPlayerNickname) {
          return;
        }

        this._processIncomingEvents(event);
      });
    });
    this._subs.push(sub);

    sub = this._teamSession.disconnected().subscribe(() => {
      this._broker = null;
      this._brokerStream.unsubscribe();
      this._brokerStream = null;
      this._lastPosition = null;
    });
    this._subs.push(sub);

    sub = this._gameEvents
      .onLocalPlayerLocationChanged()
      .subscribe(location => {
        this._lastPosition = location;
        if (!this._teamSession.active) {
          return;
        }

        this._sendPosition(location);
      });
    this._subs.push(sub);

    sub = this._gameEvents.onLocalPlayerDied().subscribe(() => {
      if (!this._teamSession.active) {
        return;
      }

      this._sendDeath();
    });
    this._subs.push(sub);
  }

  private _processIncomingEvents(event: ExtTeamPositionSync) {
    const syncEvent = this._extTeamSyncConverter.convert(event);
    const player = MapPlayer.create(
      syncEvent.owner,
      InGameUtils.overwolfMapLocationToCanvasSpace(
        this._mapBg.currentMap,
        this._mapBg.mapSize,
        syncEvent.position
      ),
      syncEvent.alive
    );

    if (this._addedMateNicknames.includes(syncEvent.owner)) {
      this._playerMovement.updatePlayer(player);
    } else {
      this._playerMovement.addPlayer(player);
      this._addedMateNicknames = this._addedMateNicknames.push(syncEvent.owner);
    }
  }

  private _sendPosition(position: Vector3) {
    const event = new TeamPositionSync(this._localPlayerNickname, position);
    this._broker.push(this._teamSyncConverter.convert(event));
  }

  private _sendDeath() {
    const event = new TeamPositionSync(
      this._localPlayerNickname,
      this._lastPosition,
      false
    );
    this._broker.push(this._teamSyncConverter.convert(event));
  }

  ngOnDestroy() {
    if (this._brokerStream) {
      this._brokerStream.unsubscribe();
    }

    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }
}
