import { fakeAsync, TestBed, tick } from "@angular/core/testing";

import { TeamSessionService } from "./team-session.service";
import { LocalPlayerInfoService } from "../../services/local-player-info.service";
import { FakeLocalPlayerInfoService } from "../../services/testing/fake-local-player-info.service";
import { OverwolfGameEventsService } from "../../overwolf/overwolf-game-events.service";
import { FakeOverwolfGameEventsService } from "../../overwolf/testing/fake-overwolf-game-events.service";
import { PubgMap } from "../../ds/game/pubg-map.enum";
import { Subscription } from "rxjs";
import { WebSocketBrokerService } from "../../websockets/web-socket-broker.service";
import { FakeOverwolfCoreService } from "../../overwolf/testing/fake-overwolf-core.service";
import { OverwolfCoreService } from "../../overwolf/overwolf-core.service";
import { environment } from "../../../environments/environment";
import * as format from "string-format";
import { FakeWebSocketBrokerService } from "../../websockets/testing/fake-web-socket-broker.service";
import { GameMode } from "../../ds/game/game-mode.enum";

describe("TeamSessionService", () => {
  let service: TeamSessionService;
  let fakeLocalPlayer: FakeLocalPlayerInfoService;
  let fakeGameEvents: FakeOverwolfGameEventsService;
  let fakeCoreEvents: FakeOverwolfCoreService;
  let subs: Subscription[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TeamSessionService,
        {
          provide: WebSocketBrokerService,
          useClass: FakeWebSocketBrokerService
        },
        {
          provide: LocalPlayerInfoService,
          useClass: FakeLocalPlayerInfoService
        },
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        },
        {
          provide: OverwolfCoreService,
          useClass: FakeOverwolfCoreService
        }
      ]
    });

    service = TestBed.get(TeamSessionService);
    fakeLocalPlayer = TestBed.get(LocalPlayerInfoService);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    fakeCoreEvents = TestBed.get(OverwolfCoreService);
    subs = [];
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should compute the session id", fakeAsync(() => {
    const localPlayer = "TheLocalPlayer";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(200);

    const pubgMap = PubgMap.SAVAGE_MAIN;
    fakeGameEvents.triggerCurrentMap(pubgMap);
    tick(200);

    const team = [localPlayer, "AnotherTeamMember"];
    fakeGameEvents.triggerPlayerTeamNicknames(team);
    tick(500);

    const gameMode = GameMode.DUO;
    fakeGameEvents.triggerCurrentGameMode(gameMode);
    tick(500);

    expect(service.sessionId).toEqual("67282d82-9508-5a2b-a959-c2a6635e4739");
  }));

  it("should connect to the session when all information is available", fakeAsync(() => {
    expect(service.active).toBeFalsy();

    const localPlayer = "Volume";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(200);
    expect(service.active).toBeFalsy();

    fakeGameEvents.triggerPlayerTeamNicknames([localPlayer, "LF"]);
    tick(200);
    expect(service.active).toBeFalsy();

    fakeGameEvents.triggerCurrentMap(PubgMap.DESERT_MAIN);
    tick(500);
    expect(service.active).toBeFalsy();

    fakeGameEvents.triggerCurrentGameMode(GameMode.DUO);
    tick(500);
    expect(service.active).toBeTruthy();
  }));

  it("should connect to the session when the game mode is solo", fakeAsync(() => {
    expect(service.active).toBeFalsy();

    const localPlayer = "To do";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(200);
    expect(service.active).toBeFalsy();

    fakeGameEvents.triggerCurrentMap(PubgMap.ERANGEL_MAIN);
    tick(500);
    expect(service.active).toBeFalsy();

    fakeGameEvents.triggerCurrentGameMode(GameMode.SOLO);
    tick(500);
    expect(service.active).toBeTruthy();
  }));

  it("should disconnect from the session when in lobby again", fakeAsync(() => {
    _connectToSession();

    fakeGameEvents.triggerMatchLeft();
    tick(500);

    expect(service.active).toBeFalsy();
    expect(service.sessionId).toBeNull();
  }));

  it("should notify all observers that the connection has been established", fakeAsync(() => {
    let receivedId: string;
    const sub = service.connected().subscribe(x => {
      receivedId = x;
    });
    subs.push(sub);

    const sessionId = _connectToSession();
    expect(receivedId).toEqual(sessionId);
  }));

  it("should notify all observers that the session was left", fakeAsync(() => {
    let receivedId: string;
    const sub = service.disconnected().subscribe(x => {
      receivedId = x;
    });
    subs.push(sub);

    const sessionId = _connectToSession();
    fakeGameEvents.triggerMatchLeft();
    tick(500);

    expect(receivedId).toEqual(sessionId);
  }));

  it("should connect to a new session when calling #connect", () => {
    expect(service.active).toBeFalsy();
    expect(service.sessionId).toBeNull();

    const sessionId = "SomeSessionIdForTestPurposes";
    service.connect(sessionId);

    expect(service.active).toBeTruthy();
    expect(service.sessionId).toEqual(sessionId);
    expect(service.getDrawingSyncBroker()).toBeTruthy();
    expect(service.getPositionSyncBroker()).toBeTruthy();
  });

  it("should disconnect from the active session when calling #disconnect", () => {
    expect(service.active).toBeFalsy();
    expect(service.sessionId).toBeNull();

    const sessionId = "scroll";
    service.connect(sessionId);

    service.disconnect();
    expect(service.active).toBeFalsy();
    expect(service.sessionId).toBeNull();
    expect(service.getPositionSyncBroker()).toBeNull();
    expect(service.getDrawingSyncBroker()).toBeNull();
  });

  function _connectToSession() {
    const localPlayer = "Git: master";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(200);

    const team = [localPlayer, "UTF-8"];
    fakeGameEvents.triggerPlayerTeamNicknames(team);
    tick(200);

    const map = PubgMap.DESERT_MAIN;
    fakeGameEvents.triggerCurrentMap(map);
    tick(500);

    const gameMode = GameMode.DUO;
    fakeGameEvents.triggerCurrentGameMode(gameMode);
    tick(500);
    expect(service.active).toBeTruthy();

    return "55b3285b-4d05-5f33-a92c-051270a6b0ad";
  }

  it("should deactivate the session when pubg has been terminated", fakeAsync(() => {
    _connectToSession();

    fakeCoreEvents.triggerPubgTerminated();
    tick(500);
    expect(service.active).toBeFalsy();
  }));

  it("should return the web socket broker for the drawing events", () => {
    const sessionId = "ThisIsASessionId...IThink";
    service.connect(sessionId);

    const drawingBroker = service.getDrawingSyncBroker();
    expect(drawingBroker.config.brokerUrl).toEqual(
      format(environment.webSockets.drawingSync.brokerUrl, {
        sessionId: sessionId
      })
    );
    expect(drawingBroker.config.pullPath).toEqual(
      format(environment.webSockets.drawingSync.pullPath, {
        sessionId: sessionId
      })
    );
    expect(drawingBroker.config.pushPath).toEqual(
      format(environment.webSockets.drawingSync.pushPath, {
        sessionId: sessionId
      })
    );
  });

  it("should return the web socket broker for the position events", () => {
    const sessionId = "SDD";
    service.connect(sessionId);

    const positionBroker = service.getPositionSyncBroker();
    expect(positionBroker.config.brokerUrl).toEqual(
      format(environment.webSockets.positionSync.brokerUrl, {
        sessionId: sessionId
      })
    );
    expect(positionBroker.config.pullPath).toEqual(
      format(environment.webSockets.positionSync.pullPath, {
        sessionId: sessionId
      })
    );
    expect(positionBroker.config.pushPath).toEqual(
      format(environment.webSockets.positionSync.pushPath, {
        sessionId: sessionId
      })
    );
  });

  it("should disconnect when calling #connect while having an active connection", fakeAsync(() => {
    service.connect("to some session");

    const spy = spyOn(service, "disconnect");
    service.connect("to another session");

    expect(spy).toHaveBeenCalled();
  }));

  it("should compute the same session id for different team members", fakeAsync(() => {
    const gameMode = GameMode.DUO;
    const pubgMap = PubgMap.DIHOROTOK_MAIN;
    const localPlayer1 = "Next";
    const localPlayer2 = "Private";
    const team1 = [localPlayer1, "Private"];
    const team2 = [localPlayer2, localPlayer1];

    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer1);
    tick(100);

    fakeGameEvents.triggerCurrentMap(pubgMap);
    tick(100);

    fakeGameEvents.triggerPlayerTeamNicknames(team1);
    tick(100);

    fakeGameEvents.triggerCurrentGameMode(gameMode);
    tick(100);

    const sessionId1 = service.sessionId;
    fakeGameEvents.triggerMatchLeft();
    tick(200);
    expect(service.sessionId).toBeNull();

    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer2);
    tick(100);

    fakeGameEvents.triggerCurrentGameMode(gameMode);
    tick(100);

    fakeGameEvents.triggerCurrentMap(pubgMap);
    tick(200);

    fakeGameEvents.triggerPlayerTeamNicknames(team2);
    tick(100);

    const sessionId2 = service.sessionId;

    expect(sessionId1).toBeTruthy();
    expect(sessionId2).toBeTruthy();
    expect(sessionId1).toEqual(sessionId2);
  }));

  afterEach(() => {
    service.ngOnDestroy();
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
