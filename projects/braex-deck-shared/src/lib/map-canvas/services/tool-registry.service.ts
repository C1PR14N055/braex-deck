import { ToolConfig } from "../ds/tool-config";
import { MapCanvasTool } from "../ds/map-canvas-tool.enum";

export class ToolRegistryService {
  private static _registeredTools = new Map<MapCanvasTool, ToolConfig>();

  static registerTool(config?: ToolConfig) {
    this._registeredTools.set(config.tool, config);
  }

  static getToolConfig(tool: MapCanvasTool): ToolConfig | null {
    if (this._registeredTools.has(tool)) {
      return this._registeredTools.get(tool);
    }

    return null;
  }
}
