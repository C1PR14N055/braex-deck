import { Injectable } from "@angular/core";
import { TelemetryCharacter } from "../ds/telemetry/objects/telemetry-character";
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class TelemetryActiveFeedService {
  private static _subject = new Subject<TelemetryCharacter[]>();

  static sendCharacters(characters: TelemetryCharacter[]) {
    TelemetryActiveFeedService._subject.next(characters);
  }

  static getCharacters(): Observable<any> {
    return TelemetryActiveFeedService._subject.asObservable();
  }
}
