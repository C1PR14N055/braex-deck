import { Injectable, OnDestroy } from "@angular/core";
import { Stack } from "immutable";
import { ChangeHistoryEvent } from "../ds/change-history-event";
import { Observable, Observer, Subscription } from "rxjs";
import { WindowDataExchangeService } from "../../services/window-data-exchange.service";
import { WindowCommunicator } from "../../services/window-communicator";
import { DrawingSyncService } from "./drawing-sync.service";
import { DrawingSyncEvent } from "../ds/drawing-sync/drawing-sync-event";
import { DrawingSyncEventType } from "../ds/drawing-sync/drawing-sync-event-type";
import { LocalPlayerInfoService } from "../../services/local-player-info.service";
import { ChangeHistorySyncBody } from "../ds/drawing-sync/change-history-sync-body";
import { ChangeActionType } from "../ds/drawing-sync/change-action-type.enum";
import { LinePath } from "../ds/line-path";

@Injectable({
  providedIn: "root"
})
export class ChangeHistoryService implements OnDestroy {
  private _subs: Subscription[] = [];
  private _changeHistory = Stack<ChangeHistoryEvent<any>>();
  private _undoneChanges = Stack<ChangeHistoryEvent<any>>();
  private _undoEventObservers: Observer<ChangeHistoryEvent<any>>[] = [];
  private _redoEventObservers: Observer<ChangeHistoryEvent<any>>[] = [];
  private _windowComm: WindowCommunicator;
  private _localPlayerNickname: string | null = null;

  constructor(
    private _windowData: WindowDataExchangeService,
    private _drawingSync: DrawingSyncService,
    private _localPlayer: LocalPlayerInfoService
  ) {
    this._initLocalPlayerInfo();
    this._initWindowCommunication();
  }

  private _initLocalPlayerInfo() {
    const sub = this._localPlayer.localPlayerNickname().subscribe(nickname => {
      this._localPlayerNickname = nickname;
    });
    this._subs.push(sub);
  }

  private _initWindowCommunication() {
    this._windowComm = this._windowData.getWindowCommunicator();
    if (!this._windowComm) {
      return;
    }

    let sub = this._windowComm.undoEvent().subscribe(event => {
      if (this._undoneChanges.indexOf(event) >= 0) {
        return;
      }

      this._undoneChanges = this._undoneChanges.push(event);
      this._triggerUndo(event);
    });
    this._subs.push(sub);

    sub = this._windowComm.redoEvent().subscribe(event => {
      if (this._changeHistory.indexOf(event) >= 0) {
        return;
      }

      this._changeHistory = this._changeHistory.push(event);
      this._triggerRedo(event);
    });
    this._subs.push(sub);
  }

  add<T>(change: ChangeHistoryEvent<T>) {
    this._changeHistory = this._changeHistory.push(change);
  }

  undo() {
    if (this._changeHistory.isEmpty()) {
      return;
    }

    const event: ChangeHistoryEvent<any> = this._changeHistory.first();
    this._undoneChanges = this._undoneChanges.push(event);
    this._changeHistory = this._changeHistory.pop();
    this._triggerUndo(event);

    // TODO(pmo): This doesn't belong here
    this._drawingSync.sendEvent(
      new DrawingSyncEvent(
        DrawingSyncEventType.UNDO_PAINT,
        new ChangeHistorySyncBody(event, this._actionTypeByBody(event.body)),
        this._localPlayerNickname
      )
    );
  }

  private _actionTypeByBody(body: any) {
    if (body instanceof LinePath) {
      return ChangeActionType.LINE;
    }

    return ChangeActionType.MARKER;
  }

  private _triggerUndo(event: ChangeHistoryEvent<any>) {
    this._undoEventObservers.forEach(x => {
      x.next(event);
    });
    if (this._windowComm) {
      this._windowComm.triggerUndo(event);
    }
  }

  undoEvent(): Observable<ChangeHistoryEvent<any>> {
    return Observable.create(observer => {
      this._undoEventObservers.push(observer);
    });
  }

  redo() {
    if (this._undoneChanges.isEmpty()) {
      return;
    }

    const event: ChangeHistoryEvent<any> = this._undoneChanges.first();
    this._changeHistory = this._changeHistory.push(event);
    this._undoneChanges = this._undoneChanges.pop();
    this._triggerRedo(event);

    // TODO(pmo): This doesn't belong here
    this._drawingSync.sendEvent(
      new DrawingSyncEvent(
        DrawingSyncEventType.REDO_PAINT,
        new ChangeHistorySyncBody(event, this._actionTypeByBody(event.body)),
        this._localPlayerNickname
      )
    );
  }

  private _triggerRedo(event: ChangeHistoryEvent<any>) {
    this._redoEventObservers.forEach(x => {
      x.next(event);
    });
    if (this._windowComm) {
      this._windowComm.triggerRedo(event);
    }
  }

  redoEvent(): Observable<ChangeHistoryEvent<any>> {
    return Observable.create(observer => {
      this._redoEventObservers.push(observer);
    });
  }

  clear() {
    this._changeHistory = this._changeHistory.clear();
    this._undoneChanges = this._undoneChanges.clear();
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  get canUndo() {
    return !this._changeHistory.isEmpty();
  }

  get canRedo() {
    return !this._undoneChanges.isEmpty();
  }
}
