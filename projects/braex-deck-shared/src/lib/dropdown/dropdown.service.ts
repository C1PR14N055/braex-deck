import { Injectable } from "@angular/core";
import { ComponentPortal, ComponentType } from "@angular/cdk/portal";
import { Overlay } from "@angular/cdk/overlay";
import { DropdownComponent } from "./dropdown/dropdown.component";
import { DropdownRef } from "./dropdown-ref";

export const OFFSET_TO_HOST_LEFT = -172;
export const OFFSET_TO_HOST_TOP = 12;

@Injectable()
export class DropdownService {
  constructor(private _overlay: Overlay) {}

  show<T>(
    component: ComponentType<T>,
    host: HTMLElement,
    label?: string
  ): DropdownRef<T> {
    const overlayRef = this._overlay.create();
    const componentRef = overlayRef.attach(
      new ComponentPortal(DropdownComponent)
    );
    componentRef.instance.afterClosed.subscribe(() => {
      overlayRef.detach();
      overlayRef.dispose();
    });
    if (label) {
      componentRef.instance.label = label;
    }

    const clientRect = host.getBoundingClientRect();
    const x = clientRect.left + host.clientWidth / 2 + OFFSET_TO_HOST_LEFT;
    const y = clientRect.top + host.clientHeight + OFFSET_TO_HOST_TOP;
    componentRef.instance.show(component, x, y);

    return new DropdownRef<T>(componentRef.instance as DropdownComponent<T>);
  }
}
