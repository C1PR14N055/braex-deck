import { DropdownComponent } from "./dropdown/dropdown.component";
import { Observable } from "rxjs";

export class DropdownRef<T> {
  constructor(private _dropdown: DropdownComponent<T>) {}

  afterClosed(): Observable<any> {
    return Observable.create(observer => {
      this._dropdown.afterClosed.subscribe(result => {
        observer.next(result);
      });
    });
  }

  hide() {
    this._dropdown.hide();
  }

  get visible() {
    return this._dropdown.visible;
  }

  get componentInstance() {
    return this._dropdown.componentInstance;
  }
}
