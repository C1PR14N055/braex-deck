import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DropdownComponent } from "./dropdown/dropdown.component";
import { DropdownService } from "./dropdown.service";
import { OverlayModule } from "@angular/cdk/overlay";
import { PortalModule } from "@angular/cdk/portal";

@NgModule({
  declarations: [DropdownComponent],
  imports: [CommonModule, OverlayModule, PortalModule],
  providers: [DropdownService],
  entryComponents: [DropdownComponent]
})
export class DropdownModule {}
