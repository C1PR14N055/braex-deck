import { TestBed } from "@angular/core/testing";

import { DropdownService } from "./dropdown.service";
import { OverlayModule } from "@angular/cdk/overlay";

describe("DropdownService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [DropdownService],
      imports: [OverlayModule]
    })
  );

  it("should be created", () => {
    const service: DropdownService = TestBed.get(DropdownService);
    expect(service).toBeTruthy();
  });
});
