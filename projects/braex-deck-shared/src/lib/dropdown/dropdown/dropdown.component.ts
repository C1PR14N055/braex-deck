import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Renderer2,
  ViewChild
} from "@angular/core";
import {
  CdkPortalOutlet,
  ComponentPortal,
  ComponentType
} from "@angular/cdk/portal";
import { ComponentAlreadyAttachedError } from "../../error/component-already-attached.error";
import {
  animate,
  keyframes,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";

export const OUT_ANIMATION_DURATION = 160;

@Component({
  selector: "lib-dropdown",
  templateUrl: "./dropdown.component.html",
  styleUrls: ["./dropdown.component.scss"],
  animations: [
    trigger("visibility", [
      state("visible", style({ opacity: 1 })),
      state("hidden", style({ opacity: 0 })),
      transition(
        "hidden => visible",
        animate(
          120,
          keyframes([
            style({ opacity: 0, transform: "translateY(10%)" }),
            style({ opacity: 1, transform: "translateY(0)" })
          ])
        )
      ),
      transition(
        "visible => hidden",
        animate(
          OUT_ANIMATION_DURATION,
          keyframes([
            style({ opacity: 1, height: "*" }),
            style({
              transform: "translateY(25%) scale(0.96)",
              opacity: 0,
              height: "*"
            })
          ])
        )
      )
    ])
  ]
})
export class DropdownComponent<T> implements OnInit {
  afterClosed = new EventEmitter<void>();
  @ViewChild(CdkPortalOutlet)
  private _portalOutlet: CdkPortalOutlet;
  private _visible = false;
  @ViewChild("dropdownWrapper")
  private _dropdownWrapper: ElementRef<HTMLDivElement>;
  @Input()
  label = "";
  private _componentInstance: T | null = null;

  constructor(private _renderer: Renderer2) {}

  ngOnInit() {
    this._renderer.listen("document", "click", event => {
      if (this._visible && this._portalOutlet.hasAttached() && event.target) {
        const target = event.target as HTMLElement;
        if (!this._dropdownWrapper.nativeElement.contains(target)) {
          this.hide();
        }
      }
    });
  }

  show(content: ComponentType<T>, x: number, y: number) {
    if (this._portalOutlet.hasAttached()) {
      throw new ComponentAlreadyAttachedError(
        `This dropdown already has a component attached to it`
      );
    }

    this._componentInstance = this._portalOutlet.attachComponentPortal(
      new ComponentPortal(content)
    ).instance;
    this._renderer.setStyle(
      this._dropdownWrapper.nativeElement,
      "left",
      `${x}px`
    );
    this._renderer.setStyle(
      this._dropdownWrapper.nativeElement,
      "top",
      `${y}px`
    );
    setTimeout(() => (this._visible = true), 0);
  }

  hide() {
    this._visible = false;
    this._componentInstance = null;
    setTimeout(() => {
      this.afterClosed.emit(null);
    }, OUT_ANIMATION_DURATION);
  }

  get visible() {
    return this._visible;
  }

  get componentInstance() {
    return this._componentInstance;
  }
}
