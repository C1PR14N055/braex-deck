export class MappingError extends Error {
  constructor(message?: string) {
    super(message);
    Object.setPrototypeOf(this, MappingError.prototype);
  }
}
