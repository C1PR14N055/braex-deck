export class PlayerNotInDbError extends Error {
  constructor(message?: string) {
    super(message);
    Object.setPrototypeOf(this, PlayerNotInDbError.prototype);
  }
}
