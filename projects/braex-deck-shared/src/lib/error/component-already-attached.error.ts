export class ComponentAlreadyAttachedError extends Error {
  constructor(message?: string) {
    super(message);
    Object.setPrototypeOf(this, ComponentAlreadyAttachedError.prototype);
  }
}
