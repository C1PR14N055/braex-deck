export class OverwolfError extends Error {
  constructor(message?: string) {
    super(message);
    Object.setPrototypeOf(this, OverwolfError.prototype);
  }
}
