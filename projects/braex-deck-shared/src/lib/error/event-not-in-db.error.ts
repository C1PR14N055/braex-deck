export class EventNotInDbError extends Error {
  constructor(message?: string) {
    super(message);
    Object.setPrototypeOf(this, EventNotInDbError.prototype);
  }
}
