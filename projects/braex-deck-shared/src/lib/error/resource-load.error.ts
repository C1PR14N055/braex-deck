export class ResourceLoadError extends Error {
  constructor(message?: string) {
    super(message);
    Object.setPrototypeOf(this, ResourceLoadError.prototype);
  }
}
