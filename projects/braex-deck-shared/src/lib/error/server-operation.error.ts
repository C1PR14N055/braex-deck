export class ServerOperationError extends Error {
  constructor(message?: string) {
    super(message);
    Object.setPrototypeOf(this, ServerOperationError.prototype);
  }
}
