import {
  AfterContentInit,
  Component,
  ContentChildren,
  OnDestroy,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { SidebarToggleButtonComponent } from "../sidebar-toggle-button/sidebar-toggle-button.component";
import { Subscription } from "rxjs";
import { CdkPortalOutlet, Portal, TemplatePortal } from "@angular/cdk/portal";
import {
  animate,
  keyframes,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";

/**
 * The time before the content of the sidebar is visible when the sidebar is being toggled.
 */
export const SIDEBAR_CONTENT_TIMEOUT = 150;
export const SIDEBAR_HIDE_TIMEOUT = 100;

@Component({
  selector: "lib-sidebar-toggle",
  templateUrl: "./sidebar-toggle.component.html",
  styleUrls: ["./sidebar-toggle.component.scss"],
  host: { class: "h-100" },
  animations: [
    trigger("sidebar", [
      state("visible", style({ width: "*" })),
      state("hidden", style({ width: 0 })),
      transition("visible <=> hidden", animate(200))
    ]),
    trigger("sidebarContent", [
      transition(
        ":enter",
        animate(150, keyframes([style({ opacity: 0 }), style({ opacity: 1 })]))
      ),
      transition(
        ":leave",
        animate(150, keyframes([style({ opacity: 1 }), style({ opacity: 0 })]))
      )
    ])
  ]
})
export class SidebarToggleComponent implements AfterContentInit, OnDestroy {
  @ContentChildren(SidebarToggleButtonComponent)
  private _buttons: QueryList<SidebarToggleButtonComponent>;
  private _subs: Subscription[] = [];
  private _activePortal: Portal<any> | null;
  @ViewChild(CdkPortalOutlet)
  private _portalOutlet: CdkPortalOutlet;
  private _portals = new Map<TemplateRef<any>, Portal<TemplateRef<any>>>();
  private _showSidebar = false;
  private _showSidebarContent = false;

  constructor(private _viewContainerRef: ViewContainerRef) {}

  ngAfterContentInit() {
    this._buttons.forEach(button => {
      const sub = button.activated.subscribe(sidebar => {
        if (!sidebar) {
          this._activePortal = null;
          setTimeout(() => (this._showSidebar = false), SIDEBAR_HIDE_TIMEOUT);

          return;
        }

        if (!this._activePortal) {
          this._showSidebarContent = false;
        }

        if (!this._portals.has(sidebar)) {
          this._portals.set(
            sidebar,
            new TemplatePortal(sidebar, this._viewContainerRef)
          );
        }

        this._activePortal = this._portals.get(sidebar);
        this._showSidebar = true;
        this._buttons.forEach(x => {
          if (x !== button) {
            x.active = false;
          }
        });
        setTimeout(
          () => (this._showSidebarContent = true),
          SIDEBAR_CONTENT_TIMEOUT
        );
      });
      this._subs.push(sub);
    });
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  get activePortal() {
    return this._activePortal;
  }

  get showSidebar() {
    return this._showSidebar;
  }

  get showSidebarContent() {
    return this._showSidebarContent;
  }
}
