import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SidebarToggleComponent } from "./sidebar-toggle.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { PortalModule } from "@angular/cdk/portal";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

describe("SidebarToggleComponent", () => {
  let component: SidebarToggleComponent;
  let fixture: ComponentFixture<SidebarToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SidebarToggleComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [PortalModule, BrowserAnimationsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
