import { Component } from "@angular/core";

@Component({
  selector: "lib-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"],
  host: { class: "flex-grow-1" }
})
export class SidebarComponent {}
