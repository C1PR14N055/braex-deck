import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SidebarToggleComponent } from "./sidebar-toggle/sidebar-toggle.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { SidebarToggleButtonComponent } from "./sidebar-toggle-button/sidebar-toggle-button.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { PortalModule } from "@angular/cdk/portal";
import { SidebarDirective } from "./directives/sidebar.directive";
import { SidebarHeaderComponent } from "./sidebar-header/sidebar-header.component";
import { SidebarContentComponent } from "./sidebar-content/sidebar-content.component";

@NgModule({
  declarations: [
    SidebarToggleComponent,
    SidebarComponent,
    SidebarToggleButtonComponent,
    SidebarDirective,
    SidebarHeaderComponent,
    SidebarContentComponent
  ],
  imports: [CommonModule, FontAwesomeModule, PortalModule],
  exports: [
    SidebarToggleButtonComponent,
    SidebarToggleComponent,
    SidebarComponent,
    SidebarDirective,
    SidebarHeaderComponent,
    SidebarContentComponent
  ]
})
export class SidebarModule {}
