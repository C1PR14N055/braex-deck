import { Component } from "@angular/core";

@Component({
  selector: "lib-sidebar-content",
  templateUrl: "./sidebar-content.component.html",
  styleUrls: ["./sidebar-content.component.scss"],
  host: { class: "flex-grow-1" }
})
export class SidebarContentComponent {}
