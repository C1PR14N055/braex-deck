import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SidebarToggleButtonComponent } from "./sidebar-toggle-button.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("SidebarToggleButtonComponent", () => {
  let component: SidebarToggleButtonComponent;
  let fixture: ComponentFixture<SidebarToggleButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SidebarToggleButtonComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarToggleButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
