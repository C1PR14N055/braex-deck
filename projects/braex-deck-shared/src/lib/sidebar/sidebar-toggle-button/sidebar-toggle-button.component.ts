import {
  AfterContentInit,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  TemplateRef
} from "@angular/core";
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";

@Component({
  selector: "lib-sidebar-toggle-button",
  templateUrl: "./sidebar-toggle-button.component.html",
  styleUrls: [
    "./sidebar-toggle-button.component.scss",
    "../../../../../styles/utils.scss"
  ]
})
export class SidebarToggleButtonComponent implements AfterContentInit {
  @Input()
  label = "";
  @Input()
  icon: IconDefinition;
  @Input()
  disabled = false;
  activated = new EventEmitter<TemplateRef<any> | null>();
  private _active = false;
  @ContentChild(TemplateRef)
  private _sidebar: TemplateRef<any>;

  ngAfterContentInit() {
    setTimeout(() => {
      if (this._active) {
        this.activated.emit(this._sidebar);
      }
    }, 0);
  }

  actionToggleActive() {
    let toEmit: TemplateRef<any> | null = null;

    this._active = !this._active;
    if (this._active) {
      toEmit = this._sidebar;
    }

    this.activated.emit(toEmit);
  }

  get active() {
    return this._active;
  }

  @Input()
  set active(value: boolean) {
    this._active = value;
  }
}
