import { Directive, TemplateRef } from "@angular/core";

@Directive({
  selector: "[appSidebar]"
})
export class SidebarDirective {
  constructor(private _template: TemplateRef<any>) {}

  get template() {
    return this._template;
  }
}
