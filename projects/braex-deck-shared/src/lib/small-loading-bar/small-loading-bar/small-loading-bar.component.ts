import { Component } from "@angular/core";

@Component({
  selector: "lib-small-loading-bar",
  templateUrl: "./small-loading-bar.component.html",
  styleUrls: ["./small-loading-bar.component.scss"]
})
export class SmallLoadingBarComponent {}
