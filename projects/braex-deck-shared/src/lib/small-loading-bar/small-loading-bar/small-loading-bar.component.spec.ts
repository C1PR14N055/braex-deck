import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SmallLoadingBarComponent } from "./small-loading-bar.component";

describe("SmallLoadingBarComponent", () => {
  let component: SmallLoadingBarComponent;
  let fixture: ComponentFixture<SmallLoadingBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SmallLoadingBarComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallLoadingBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
