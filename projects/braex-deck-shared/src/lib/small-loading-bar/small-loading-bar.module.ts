import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SmallLoadingBarComponent } from "./small-loading-bar/small-loading-bar.component";

@NgModule({
  declarations: [SmallLoadingBarComponent],
  imports: [CommonModule],
  exports: [SmallLoadingBarComponent]
})
export class SmallLoadingBarModule {}
