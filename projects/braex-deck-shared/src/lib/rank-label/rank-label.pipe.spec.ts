import { RankLabelPipe } from "./rank-label.pipe";

describe("RankLabelPipe", () => {
  let pipe: RankLabelPipe;

  beforeEach(() => {
    pipe = new RankLabelPipe();
  });

  it("should be created", () => {
    expect(pipe).toBeTruthy();
  });

  it("should return the unknown label when passing undefined", () => {
    expect(pipe.transform(undefined)).toEqual("Unknown");
  });

  it("should return the unknown label when passing null", () => {
    expect(pipe.transform(null)).toEqual("Unknown");
  });

  it("should return the unknown label when the rankTitle cannot be identified", () => {
    expect(pipe.transform("This is a short sentence.")).toEqual("Unknown");
  });

  it("should return the unknown label when the rankTitle cannot be identified 2", () => {
    expect(pipe.transform("9-9")).toEqual("Unknown");
  });

  it("should return the unknown label", () => {
    expect(pipe.transform("0-0")).toEqual("Unknown");
  });

  it("should return the beginner 1 label", () => {
    expect(pipe.transform("1-1")).toEqual("Beginner I");
  });

  it("should return the beginner 2 label", () => {
    expect(pipe.transform("1-2")).toEqual("Beginner II");
  });

  it("should return the beginner 3 label", () => {
    expect(pipe.transform("1-3")).toEqual("Beginner III");
  });

  it("should return the beginner 4 label", () => {
    expect(pipe.transform("1-4")).toEqual("Beginner IV");
  });

  it("should return the beginner 5 label", () => {
    expect(pipe.transform("1-5")).toEqual("Beginner V");
  });

  it("should return the novice 1 label", () => {
    expect(pipe.transform("2-1")).toEqual("Novice I");
  });

  it("should return the novice 2 label", () => {
    expect(pipe.transform("2-2")).toEqual("Novice II");
  });

  it("should return the novice 3 label", () => {
    expect(pipe.transform("2-3")).toEqual("Novice III");
  });

  it("should return the novice 4 label", () => {
    expect(pipe.transform("2-4")).toEqual("Novice IV");
  });

  it("should return the novice 5 label", () => {
    expect(pipe.transform("2-5")).toEqual("Novice V");
  });

  it("should return the experienced 1 label", () => {
    expect(pipe.transform("3-1")).toEqual("Experienced I");
  });

  it("should return the experienced 2 label", () => {
    expect(pipe.transform("3-2")).toEqual("Experienced II");
  });

  it("should return the experienced 3 label", () => {
    expect(pipe.transform("3-3")).toEqual("Experienced III");
  });

  it("should return the experienced 4 label", () => {
    expect(pipe.transform("3-4")).toEqual("Experienced IV");
  });

  it("should return the experienced 5 label", () => {
    expect(pipe.transform("3-5")).toEqual("Experienced V");
  });

  it("should return the skilled 1 label", () => {
    expect(pipe.transform("4-1")).toEqual("Skilled I");
  });

  it("should return the skilled 2 label", () => {
    expect(pipe.transform("4-2")).toEqual("Skilled II");
  });

  it("should return the skilled 3 label", () => {
    expect(pipe.transform("4-3")).toEqual("Skilled III");
  });

  it("should return the skilled 4 label", () => {
    expect(pipe.transform("4-4")).toEqual("Skilled IV");
  });

  it("should return the skilled 5 label", () => {
    expect(pipe.transform("4-5")).toEqual("Skilled V");
  });

  it("should return the specialist 1 label", () => {
    expect(pipe.transform("5-1")).toEqual("Specialist I");
  });

  it("should return the specialist 2 label", () => {
    expect(pipe.transform("5-2")).toEqual("Specialist II");
  });

  it("should return the specialist 3 label", () => {
    expect(pipe.transform("5-3")).toEqual("Specialist III");
  });

  it("should return the specialist 4 label", () => {
    expect(pipe.transform("5-4")).toEqual("Specialist IV");
  });

  it("should return the specialist 5 label", () => {
    expect(pipe.transform("5-5")).toEqual("Specialist V");
  });

  it("should return the expert label", () => {
    expect(pipe.transform("6-0")).toEqual("Expert");
  });

  it("should return the survivor label", () => {
    expect(pipe.transform("7-0")).toEqual("Survivor");
  });
});
