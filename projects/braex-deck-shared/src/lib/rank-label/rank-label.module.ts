import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RankLabelPipe } from "./rank-label.pipe";

@NgModule({
  declarations: [RankLabelPipe],
  imports: [CommonModule],
  exports: [RankLabelPipe]
})
export class RankLabelModule {}
