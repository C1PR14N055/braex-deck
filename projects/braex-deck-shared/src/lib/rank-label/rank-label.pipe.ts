import { Pipe, PipeTransform } from "@angular/core";
import { RANK_TITLE_PATTERN } from "../shared/rank-shared";
import { InternalError } from "../error/internal.error";

@Pipe({
  name: "rankLabel"
})
export class RankLabelPipe implements PipeTransform {
  /**
   * A more detailed explanation on what the value should be can be found on {@link RankIconPipe}.
   */
  transform(value: string, args?: any) {
    if (value) {
      const groups = RANK_TITLE_PATTERN.exec(value);
      if (groups && groups.length === 3) {
        switch (groups[1]) {
          case "1":
            return `Beginner ${this._numberToRomanNumeral(Number(groups[2]))}`;
          case "2":
            return `Novice ${this._numberToRomanNumeral(Number(groups[2]))}`;
          case "3":
            return `Experienced ${this._numberToRomanNumeral(
              Number(groups[2])
            )}`;
          case "4":
            return `Skilled ${this._numberToRomanNumeral(Number(groups[2]))}`;
          case "5":
            return `Specialist ${this._numberToRomanNumeral(
              Number(groups[2])
            )}`;
          case "6":
            return "Expert";
          case "7":
            return "Survivor";
        }
      }
    }

    return "Unknown";
  }

  private _numberToRomanNumeral(n: number) {
    switch (n) {
      case 1:
        return "I";
      case 2:
        return "II";
      case 3:
        return "III";
      case 4:
        return "IV";
      case 5:
        return "V";
    }

    // FIXME(don't throw cuz it may cause a weird issue for now):
    // throw new InternalError(
    //   `Only number up to 5 can be converted... received ${n}`
    // );
    return "";
  }
}
