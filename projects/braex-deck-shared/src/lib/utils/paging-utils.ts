import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE,
  PagingOptions
} from "../ds/paging-options";

export abstract class PagingUtils {
  public static buildPagingParams(
    options?: PagingOptions
  ): { [param: string]: string } {
    let page = DEFAULT_PAGE_NUMBER;
    if (options && options.page) {
      page = options.page;
    }

    let size = DEFAULT_PAGE_SIZE;
    if (options && options.size) {
      size = options.size;
    }

    return {
      page: page.toString(),
      size: size.toString()
    };
  }
}
