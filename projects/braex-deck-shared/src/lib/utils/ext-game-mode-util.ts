import { Injectable } from "@angular/core";
import { Perspective } from "../ds/game/perspective.enum";
import { GameMode } from "../ds/game/game-mode.enum";
import { reverseExtGameModeMapping } from "../ds/external/game/ext-game-mode.enum";

@Injectable({
  providedIn: "root"
})
export class ExtGameModeUtil {
  buildExtGameMode(gameMode: GameMode, gameModeType: Perspective) {
    const result: string[] = [];
    if (gameMode === GameMode.SOLO) {
      result.push("SOLO");
    } else if (gameMode === GameMode.DUO) {
      result.push("DUO");
    } else if (gameMode === GameMode.SQUAD) {
      result.push("SQUAD");
    }

    if (gameModeType === Perspective.FPP) {
      result.push("FPP");
    }

    return reverseExtGameModeMapping(result.join("_"));
  }
}
