import { PubgGameMode } from "../ds/game/pubg-game-mode.enum";

export abstract class GameModeUtils {
  static isFpp(gameMode: PubgGameMode) {
    switch (gameMode) {
      case PubgGameMode.SOLO_FPP:
      case PubgGameMode.DUO_FPP:
      case PubgGameMode.SQUAD_FPP:
      case PubgGameMode.NORMAL:
      case PubgGameMode.WAR_SOLO_FPP:
      case PubgGameMode.WAR_DUO_FPP:
      case PubgGameMode.WAR_SQUAD_FPP:
      case PubgGameMode.ZOMBIE_SOLO_FPP:
      case PubgGameMode.ZOMBIE_DUO_FPP:
      case PubgGameMode.ZOMBIE_SQUAD_FPP:
      case PubgGameMode.CONQUEST_SOLO_FPP:
      case PubgGameMode.CONQUEST_DUO_FPP:
      case PubgGameMode.CONQUEST_SQUAD_FPP:
      case PubgGameMode.ESPORTS_SOLO_FPP:
      case PubgGameMode.ESPORTS_DUO_FPP:
      case PubgGameMode.ESPORTS_SQUAD_FPP:
        return true;
    }

    return false;
  }

  static areCompatible(a: PubgGameMode, b: PubgGameMode) {
    switch (a) {
      case PubgGameMode.SOLO:
      case PubgGameMode.SOLO_FPP:
        if (b === PubgGameMode.SOLO || b === PubgGameMode.SOLO_FPP) {
          return true;
        }
        break;
      case PubgGameMode.DUO:
      case PubgGameMode.DUO_FPP:
        if (b === PubgGameMode.DUO || b === PubgGameMode.DUO_FPP) {
          return true;
        }
        break;
      case PubgGameMode.SQUAD:
      case PubgGameMode.SQUAD_FPP:
        if (b === PubgGameMode.SQUAD || b === PubgGameMode.SQUAD_FPP) {
          return true;
        }
        break;
      case PubgGameMode.WAR_SOLO:
      case PubgGameMode.WAR_SOLO_FPP:
        if (b === PubgGameMode.WAR_SOLO || PubgGameMode.WAR_SOLO_FPP) {
          return true;
        }
        break;
      case PubgGameMode.WAR_DUO:
      case PubgGameMode.WAR_DUO_FPP:
        if (b === PubgGameMode.WAR_DUO || b === PubgGameMode.WAR_DUO_FPP) {
          return true;
        }
        break;
      case PubgGameMode.WAR_SQUAD:
      case PubgGameMode.WAR_SQUAD_FPP:
        if (b === PubgGameMode.WAR_SQUAD || b === PubgGameMode.WAR_SQUAD_FPP) {
          return true;
        }
        break;
      case PubgGameMode.ZOMBIE_SOLO:
      case PubgGameMode.ZOMBIE_SOLO_FPP:
        if (
          b === PubgGameMode.ZOMBIE_SOLO ||
          b === PubgGameMode.ZOMBIE_SOLO_FPP
        ) {
          return true;
        }
        break;
      case PubgGameMode.ZOMBIE_DUO:
      case PubgGameMode.ZOMBIE_DUO_FPP:
        if (
          b === PubgGameMode.ZOMBIE_DUO ||
          b === PubgGameMode.ZOMBIE_DUO_FPP
        ) {
          return true;
        }
        break;
      case PubgGameMode.ZOMBIE_SQUAD:
      case PubgGameMode.ZOMBIE_SQUAD_FPP:
        if (
          b === PubgGameMode.ZOMBIE_SQUAD ||
          b === PubgGameMode.ZOMBIE_SQUAD_FPP
        ) {
          return true;
        }
        break;
      case PubgGameMode.CONQUEST_SOLO:
      case PubgGameMode.CONQUEST_SOLO_FPP:
        if (
          b === PubgGameMode.CONQUEST_SOLO ||
          b === PubgGameMode.CONQUEST_SOLO_FPP
        ) {
          return true;
        }
        break;
      case PubgGameMode.CONQUEST_DUO:
      case PubgGameMode.CONQUEST_DUO_FPP:
        if (
          b === PubgGameMode.CONQUEST_DUO ||
          b === PubgGameMode.CONQUEST_DUO_FPP
        ) {
          return true;
        }
        break;
      case PubgGameMode.CONQUEST_SQUAD:
      case PubgGameMode.CONQUEST_SQUAD_FPP:
        if (
          b === PubgGameMode.CONQUEST_SQUAD ||
          b === PubgGameMode.CONQUEST_SQUAD_FPP
        ) {
          return true;
        }
        break;
      case PubgGameMode.ESPORTS_SOLO:
      case PubgGameMode.ESPORTS_SOLO_FPP:
        if (
          b === PubgGameMode.ESPORTS_SOLO ||
          b === PubgGameMode.ESPORTS_SOLO_FPP
        ) {
          return true;
        }
        break;
      case PubgGameMode.ESPORTS_DUO:
      case PubgGameMode.ESPORTS_DUO_FPP:
        if (
          b === PubgGameMode.ESPORTS_DUO ||
          b === PubgGameMode.ESPORTS_DUO_FPP
        ) {
          return true;
        }
        break;
      case PubgGameMode.ESPORTS_SQUAD:
      case PubgGameMode.ESPORTS_SQUAD_FPP:
        if (
          b === PubgGameMode.ESPORTS_SQUAD ||
          b === PubgGameMode.ESPORTS_SQUAD_FPP
        ) {
          return true;
        }
        break;
    }

    return false;
  }

  static teamSize(gameMode: PubgGameMode): number | null {
    switch (gameMode) {
      case PubgGameMode.SOLO:
      case PubgGameMode.SOLO_FPP:
      case PubgGameMode.WAR_SOLO:
      case PubgGameMode.WAR_SOLO_FPP:
      case PubgGameMode.ZOMBIE_SOLO:
      case PubgGameMode.ZOMBIE_SOLO_FPP:
      case PubgGameMode.CONQUEST_SOLO:
      case PubgGameMode.CONQUEST_SOLO_FPP:
      case PubgGameMode.ESPORTS_SOLO:
      case PubgGameMode.ESPORTS_SOLO_FPP:
        return 1;
      case PubgGameMode.DUO:
      case PubgGameMode.DUO_FPP:
      case PubgGameMode.WAR_DUO:
      case PubgGameMode.WAR_DUO_FPP:
      case PubgGameMode.ZOMBIE_DUO:
      case PubgGameMode.ZOMBIE_DUO_FPP:
      case PubgGameMode.CONQUEST_DUO:
      case PubgGameMode.CONQUEST_DUO_FPP:
      case PubgGameMode.ESPORTS_DUO:
      case PubgGameMode.ESPORTS_DUO_FPP:
        return 2;
      case PubgGameMode.SQUAD:
      case PubgGameMode.SQUAD_FPP:
      case PubgGameMode.WAR_SQUAD:
      case PubgGameMode.WAR_SQUAD_FPP:
      case PubgGameMode.ZOMBIE_SQUAD:
      case PubgGameMode.ZOMBIE_SQUAD_FPP:
      case PubgGameMode.CONQUEST_SQUAD:
      case PubgGameMode.CONQUEST_SQUAD_FPP:
      case PubgGameMode.ESPORTS_SQUAD:
      case PubgGameMode.ESPORTS_SQUAD_FPP:
        return 4;
    }

    return null;
  }
}
