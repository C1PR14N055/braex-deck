import { PagingUtils } from "./paging-utils";
import { DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE } from "../ds/paging-options";

describe("PagingUtils", () => {
  it("should return the default options when passing no options", () => {
    const actual: any = PagingUtils.buildPagingParams();

    expect(actual).toBeTruthy();
    expect(actual.page).toEqual(DEFAULT_PAGE_NUMBER.toString());
    expect(actual.size).toEqual(DEFAULT_PAGE_SIZE.toString());
  });

  it("should return the default options when passing an empty object", () => {
    const actual: any = PagingUtils.buildPagingParams({});

    expect(actual).toBeTruthy();
    expect(actual.page).toEqual(DEFAULT_PAGE_NUMBER.toString());
    expect(actual.size).toEqual(DEFAULT_PAGE_SIZE.toString());
  });

  it("should only set the page", () => {
    const page = 12;
    const actual: any = PagingUtils.buildPagingParams({ page: page });

    expect(actual).toBeTruthy();
    expect(actual.page).toEqual(page.toString());
    expect(actual.size).toEqual(DEFAULT_PAGE_SIZE.toString());
  });

  it("should only set the size", () => {
    const size = 90;
    const actual: any = PagingUtils.buildPagingParams({ size: size });

    expect(actual).toBeTruthy();
    expect(actual.page).toEqual(DEFAULT_PAGE_NUMBER.toString());
    expect(actual.size).toEqual(size.toString());
  });

  it("should set the page and size of the page", () => {
    const size = 90;
    const page = 4;
    const actual: any = PagingUtils.buildPagingParams({
      size: size,
      page: page
    });

    expect(actual).toBeTruthy();
    expect(actual.page).toEqual(page.toString());
    expect(actual.size).toEqual(size.toString());
  });
});
