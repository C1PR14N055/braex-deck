import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input
} from "@angular/core";
import {
  animate,
  keyframes,
  style,
  transition,
  trigger
} from "@angular/animations";

@Component({
  selector: "lib-loading-overlay",
  templateUrl: "./loading-overlay.component.html",
  styleUrls: ["./loading-overlay.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger("fade", [
      transition(
        ":enter",
        animate(
          "120ms ease-in",
          keyframes([
            style({ display: "none", opacity: 0 }),
            style({ display: "*", opacity: 1 })
          ])
        )
      ),
      transition(
        ":leave",
        animate(
          "550ms ease-out",
          keyframes([
            style({ display: "*", opacity: 1 }),
            style({ display: "none", opacity: 0 })
          ])
        )
      )
    ])
  ]
})
export class LoadingOverlayComponent {
  private _loading = false;

  constructor(private _changeDetector: ChangeDetectorRef) {}

  get loading() {
    return this._loading;
  }

  @Input()
  set loading(value: boolean) {
    const changed = this._loading !== value;
    this._loading = value;

    if (changed) {
      this._changeDetector.detectChanges();
    }
  }
}
