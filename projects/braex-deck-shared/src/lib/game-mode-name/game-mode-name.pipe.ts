import { Pipe, PipeTransform } from "@angular/core";
import { MappingError } from "../error/mapping.error";
import { PubgGameMode } from "../ds/game/pubg-game-mode.enum";

@Pipe({
  name: "gameModeName"
})
export class GameModeNamePipe implements PipeTransform {
  transform(value: PubgGameMode, args?: any) {
    switch (value) {
      case PubgGameMode.SOLO:
      case PubgGameMode.WAR_SOLO:
      case PubgGameMode.ZOMBIE_SOLO:
      case PubgGameMode.CONQUEST_SOLO:
      case PubgGameMode.ESPORTS_SOLO:
        return "Solo";
      case PubgGameMode.SOLO_FPP:
      case PubgGameMode.WAR_SOLO_FPP:
      case PubgGameMode.ZOMBIE_SOLO_FPP:
      case PubgGameMode.CONQUEST_SOLO_FPP:
      case PubgGameMode.ESPORTS_SOLO_FPP:
        return "Solo Fpp";
      case PubgGameMode.DUO:
      case PubgGameMode.WAR_DUO:
      case PubgGameMode.ZOMBIE_DUO:
      case PubgGameMode.CONQUEST_DUO:
      case PubgGameMode.ESPORTS_DUO:
        return "Duo";
      case PubgGameMode.DUO_FPP:
      case PubgGameMode.WAR_DUO_FPP:
      case PubgGameMode.ZOMBIE_DUO_FPP:
      case PubgGameMode.CONQUEST_DUO_FPP:
      case PubgGameMode.ESPORTS_DUO_FPP:
        return "Duo Fpp";
      case PubgGameMode.SQUAD:
      case PubgGameMode.WAR_SQUAD:
      case PubgGameMode.ZOMBIE_SQUAD:
      case PubgGameMode.CONQUEST_SQUAD:
      case PubgGameMode.ESPORTS_SQUAD:
        return "Squad";
      case PubgGameMode.SQUAD_FPP:
      case PubgGameMode.WAR_SQUAD_FPP:
      case PubgGameMode.ZOMBIE_SQUAD_FPP:
      case PubgGameMode.CONQUEST_SQUAD_FPP:
      case PubgGameMode.ESPORTS_SQUAD_FPP:
        return "Squad Fpp";
      case PubgGameMode.WAR:
        return "War";
      case PubgGameMode.ZOMBIE:
        return "Zombie";
      case PubgGameMode.CONQUEST:
        return "Conquest";
      case PubgGameMode.ESPORTS:
        return "Esports";
      case PubgGameMode.NORMAL:
        return "Normal";
    }

    throw new MappingError(`No name for the mode ${value} found`);
  }
}
