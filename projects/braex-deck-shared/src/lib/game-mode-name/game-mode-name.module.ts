import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GameModeNamePipe } from "./game-mode-name.pipe";

@NgModule({
  declarations: [GameModeNamePipe],
  exports: [GameModeNamePipe],
  imports: [CommonModule]
})
export class GameModeNameModule {}
