import { Pipe, PipeTransform } from "@angular/core";
import { PubgMap } from "../ds/game/pubg-map.enum";

@Pipe({
  name: "mapName"
})
export class MapNamePipe implements PipeTransform {
  transform(value: PubgMap, args?: any) {
    switch (value) {
      case PubgMap.ERANGEL_MAIN:
        return "Erangel";
      case PubgMap.DESERT_MAIN:
        return "Miramar";
      case PubgMap.SAVAGE_MAIN:
        return "Sanhok";
      case PubgMap.DIHOROTOK_MAIN:
        return "Vikendi";
      case PubgMap.RANGE_MAIN:
        return "Range";
    }

    return value;
  }
}
