import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MapNamePipe } from "./map-name.pipe";

@NgModule({
  declarations: [MapNamePipe],
  imports: [CommonModule],
  exports: [MapNamePipe]
})
export class MapNameModule {}
