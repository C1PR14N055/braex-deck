import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { GameModeBadgeComponent } from "./game-mode-badge.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { SharedPipesModule } from "../../shared-pipes/shared-pipes.module";

describe("GameModeBadgeComponent", () => {
  let component: GameModeBadgeComponent;
  let fixture: ComponentFixture<GameModeBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameModeBadgeComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [SharedPipesModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameModeBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
