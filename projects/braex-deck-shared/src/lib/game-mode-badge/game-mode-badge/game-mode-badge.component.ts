import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit
} from "@angular/core";
import { PubgGameMode } from "../../ds/game/pubg-game-mode.enum";
import { GameModeUtils } from "../../utils/game-mode-utils";
import { Perspective } from "../../ds/game/perspective.enum";

@Component({
  selector: "lib-game-mode-badge",
  templateUrl: "./game-mode-badge.component.html",
  styleUrls: ["./game-mode-badge.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameModeBadgeComponent implements OnInit {
  @Input()
  gameMode: PubgGameMode;
  private _perspective = Perspective.TPP;

  ngOnInit() {
    if (GameModeUtils.isFpp(this.gameMode)) {
      this._perspective = Perspective.FPP;
    }
  }

  get perspective() {
    return this._perspective;
  }
}
