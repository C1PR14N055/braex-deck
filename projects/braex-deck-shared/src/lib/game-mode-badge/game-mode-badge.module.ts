import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GameModeBadgeComponent } from "./game-mode-badge/game-mode-badge.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { GameModeIconModule } from "../game-mode-icon/game-mode-icon.module";
import { SharedPipesModule } from "../shared-pipes/shared-pipes.module";

@NgModule({
  declarations: [GameModeBadgeComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    GameModeIconModule,
    SharedPipesModule
  ],
  exports: [GameModeBadgeComponent]
})
export class GameModeBadgeModule {}
