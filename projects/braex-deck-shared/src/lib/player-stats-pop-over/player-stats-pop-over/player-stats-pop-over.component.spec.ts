import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PlayerStatsPopOverComponent } from "./player-stats-pop-over.component";
import { PlayerStats } from "../../ds/player/player-stats";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { GameTrackerService } from "../../services/game-tracker.service";
import { FakeGameTrackerService } from "../../services/testing/fake-game-tracker.service";
import { LobbyStats } from "../../ds/lobby-stats";
import { RankIconModule } from "../../rank-icon/rank-icon.module";
import { RankLabelModule } from "../../rank-label/rank-label.module";

describe("PlayerStatsPopOverComponent", () => {
  let component: PlayerStatsPopOverComponent;
  let fixture: ComponentFixture<PlayerStatsPopOverComponent>;
  let fakeTrackerService: FakeGameTrackerService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RankIconModule, RankLabelModule],
      declarations: [PlayerStatsPopOverComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: GameTrackerService, useClass: FakeGameTrackerService }
      ]
    }).compileComponents();

    fakeTrackerService = TestBed.get(
      GameTrackerService
    ) as FakeGameTrackerService;
  }));

  beforeEach(() => {
    fakeTrackerService.averageLobbyStats = LobbyStats.create(
      1.2855941004184042,
      2.360938596491219,
      23.147180020496688
    );
    fakeTrackerService.minLobbyStats = LobbyStats.create(0.5, 0.155, 1.7931);
    fakeTrackerService.maxLobbyStats = LobbyStats.create(
      4.55882,
      15.584000000000001,
      46.875073242301944
    );

    fixture = TestBed.createComponent(PlayerStatsPopOverComponent);
    component = fixture.componentInstance;
    component.playerStats = new PlayerStats(
      "TestPlayer",
      0.8209,
      3.0482,
      3.7931,
      0.8209,
      0.13433,
      115.49009,
      251.592029999999994060999597422778606414794921875,
      326.46099,
      7,
      134
    );
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should compute the kd percentage compared to the lobby", () => {
    component.ngOnInit();
    expect(component.lobbyKdRatioPercentage).toBeCloseTo(
      7.90623875904819627379386127
    );
  });

  it("should compute the win percentage compared to the lobby", () => {
    component.ngOnInit();
    expect(component.lobbyWinRatioPercentage).toBeCloseTo(
      18.75170134162939796800172781
    );
  });

  it("should compute the headshot percentage compared to the lobby", () => {
    component.ngOnInit();
    expect(component.lobbyHsRatioPercentage).toBeCloseTo(
      4.436363043051833831203105728
    );
  });
});
