import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from "@angular/core";
import { PlayerStats } from "../../ds/player/player-stats";
import { GameTrackerService } from "../../services/game-tracker.service";

@Component({
  selector: "lib-player-stats-pop-over",
  templateUrl: "./player-stats-pop-over.component.html",
  styleUrls: ["./player-stats-pop-over.component.scss"]
})
export class PlayerStatsPopOverComponent implements OnInit, AfterViewInit {
  @Input()
  playerStats: PlayerStats;
  @Output()
  closePopover = new EventEmitter<void>();
  ready = false;
  @ViewChild("wrapper")
  private _wrapper: ElementRef<HTMLDivElement>;
  private _localPlayerStats: PlayerStats;
  private _lobbyKdRatioPercentage = 0;
  private _lobbyWinRatioPercentage = 0;
  private _lobbyHsRatioPercentage = 0;

  constructor(
    private _renderer: Renderer2,
    private _gameTrackerService: GameTrackerService
  ) {}

  ngOnInit() {
    this._localPlayerStats = this._gameTrackerService.localPlayerStats;
    this._computeLobbyPercentages();
  }
  private _computeLobbyPercentages() {
    this._lobbyKdRatioPercentage = this._computePercentageBetweenBounds(
      this.playerStats.kdRatio,
      this._gameTrackerService.minLobbyStats.kdRatio,
      this._gameTrackerService.maxLobbyStats.kdRatio
    );
    this._lobbyWinRatioPercentage = this._computePercentageBetweenBounds(
      this.playerStats.winRatio,
      this._gameTrackerService.minLobbyStats.winRatio,
      this._gameTrackerService.maxLobbyStats.winRatio
    );
    this._lobbyHsRatioPercentage = this._computePercentageBetweenBounds(
      this.playerStats.headshotRatio,
      this._gameTrackerService.minLobbyStats.headshotRatio,
      this._gameTrackerService.maxLobbyStats.headshotRatio
    );
  }

  private _computePercentageBetweenBounds(
    value: number,
    min: number,
    max: number
  ) {
    return ((value - min) * 100) / (max - min);
  }

  ngAfterViewInit() {
    this._renderer.listen("document", "click", event => {
      if (event.target) {
        const target = event.target as HTMLElement;
        if (
          this.ready &&
          target !== this._wrapper.nativeElement &&
          !this._wrapper.nativeElement.contains(target)
        ) {
          this._triggerClose();
        }
      }
    });
  }

  private _triggerClose() {
    this.closePopover.next(null);
  }

  get lobbyKdRatioPercentage() {
    return this._lobbyKdRatioPercentage;
  }

  get lobbyWinRatioPercentage() {
    return this._lobbyWinRatioPercentage;
  }

  get lobbyHsRatioPercentage() {
    return this._lobbyHsRatioPercentage;
  }

  get localPlayerStats() {
    return this._localPlayerStats;
  }
}
