import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PlayerStatsPopOverComponent } from "./player-stats-pop-over/player-stats-pop-over.component";
import { OverlayModule } from "@angular/cdk/overlay";
import { RankIconModule } from "../rank-icon/rank-icon.module";
import { TooltipModule } from "../tooltip/tooltip.module";
import { RankLabelModule } from "../rank-label/rank-label.module";

@NgModule({
  declarations: [PlayerStatsPopOverComponent],
  imports: [
    CommonModule,
    OverlayModule,
    RankIconModule,
    TooltipModule,
    RankLabelModule
  ],
  exports: [PlayerStatsPopOverComponent],
  entryComponents: [PlayerStatsPopOverComponent]
})
export class PlayerStatsPopOverModule {}
