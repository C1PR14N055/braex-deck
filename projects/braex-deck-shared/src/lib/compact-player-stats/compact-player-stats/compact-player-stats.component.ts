import { Component, Input, OnDestroy, ViewChild } from "@angular/core";
import { PlayerStats } from "../../ds/player/player-stats";
import { faAngleDoubleRight } from "@fortawesome/free-solid-svg-icons";
import { SatPopover } from "@ncstate/sat-popover";
import { PlayerStatsPopOverComponent } from "../../player-stats-pop-over/player-stats-pop-over/player-stats-pop-over.component";
import { Subscription } from "rxjs";

class ViewContext {
  faAngleDoubleRight = faAngleDoubleRight;
}

@Component({
  selector: "lib-compact-player-stats",
  templateUrl: "./compact-player-stats.component.html",
  styleUrls: ["./compact-player-stats.component.scss"]
})
export class CompactPlayerStatsComponent implements OnDestroy {
  readonly viewContext = new ViewContext();
  @Input()
  playerStats: PlayerStats;
  @Input()
  friendly = false;
  @ViewChild("playerStatsPopover")
  private _playerStatsPopover: SatPopover;
  @ViewChild(PlayerStatsPopOverComponent)
  private _popOver: PlayerStatsPopOverComponent;
  private _subs: Subscription[] = [];

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  actionOpenStatsPopOver() {
    const sub = this._playerStatsPopover.afterOpen.subscribe(() => {
      this._popOver.ready = true;
    });
    this._subs.push(sub);
    this._playerStatsPopover.open();
  }

  actionCloseStatsPopOver() {
    const sub = this._playerStatsPopover.afterClose.subscribe(() => {
      this._popOver.ready = false;
    });
    this._subs.push(sub);
    this._playerStatsPopover.close();
  }
}
