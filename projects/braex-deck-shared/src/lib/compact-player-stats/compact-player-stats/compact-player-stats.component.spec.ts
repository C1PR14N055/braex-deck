import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CompactPlayerStatsComponent } from "./compact-player-stats.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { PlayerStats } from "../../ds/player/player-stats";
import { PlayerStatsPopOverModule } from "../../player-stats-pop-over/player-stats-pop-over.module";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { RankIconModule } from "../../rank-icon/rank-icon.module";
import { RankLabelModule } from "../../rank-label/rank-label.module";

describe("CompactPlayerStatsComponent", () => {
  let component: CompactPlayerStatsComponent;
  let fixture: ComponentFixture<CompactPlayerStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        PlayerStatsPopOverModule,
        HttpClientTestingModule,
        RankIconModule,
        RankLabelModule
      ],
      declarations: [CompactPlayerStatsComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompactPlayerStatsComponent);
    component = fixture.componentInstance;
    component.playerStats = new PlayerStats("TestPlayer", 0.7, 16);
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
