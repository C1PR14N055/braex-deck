import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CompactPlayerStatsComponent } from "./compact-player-stats/compact-player-stats.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { MatRippleModule } from "@angular/material";
import { PlayerStatsPopOverModule } from "../player-stats-pop-over/player-stats-pop-over.module";
import { TooltipModule } from "../tooltip/tooltip.module";
import { SatPopoverModule } from "@ncstate/sat-popover";
import { RankIconModule } from "../rank-icon/rank-icon.module";
import { RankLabelModule } from "../rank-label/rank-label.module";

@NgModule({
  declarations: [CompactPlayerStatsComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    MatRippleModule,
    PlayerStatsPopOverModule,
    TooltipModule,
    SatPopoverModule,
    RankIconModule,
    RankLabelModule
  ],
  exports: [CompactPlayerStatsComponent]
})
export class CompactPlayerStatsModule {}
