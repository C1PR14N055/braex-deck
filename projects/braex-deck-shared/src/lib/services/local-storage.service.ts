import { Injectable } from "@angular/core";
import * as store from "store2";

@Injectable({
  providedIn: "root"
})
export class LocalStorageService {
  set(key: string, data: string) {
    store.set(key, data, true);
  }

  has(key: string) {
    return store.has(key);
  }

  get(key: string): string | null {
    return store.get(key) as string;
  }

  delete(key: string) {
    store.remove(key);
  }
}
