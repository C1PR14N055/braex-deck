/**
 * A service to communicate between the in-game and the desktop window in overwolf
 */
import { Observable, Observer } from "rxjs";
import { LinePath } from "../map-canvas/ds/line-path";
import { MapMarker } from "../map-canvas/ds/map-marker";
import { ChangeHistoryEvent } from "../map-canvas/ds/change-history-event";

export class WindowCommunicator {
  private _brushLineAddedObservers: Observer<LinePath>[] = [];
  private _brushLineRemovedObservers: Observer<LinePath>[] = [];
  private _brushLinesClearedObservers: Observer<void>[] = [];
  private _markerAddedObservers: Observer<MapMarker>[] = [];
  private _markerRemovedObservers: Observer<MapMarker>[] = [];
  private _undoEventObservers: Observer<ChangeHistoryEvent<any>>[] = [];
  private _redoEventObservers: Observer<ChangeHistoryEvent<any>>[] = [];

  triggerBrushLineAdded(line: LinePath) {
    this._brushLineAddedObservers.forEach(x => {
      x.next(line);
    });
  }

  brushLineAdded(): Observable<LinePath> {
    return Observable.create(observer => {
      this._brushLineAddedObservers.push(observer);
    });
  }

  triggerBrushLineRemoved(line: LinePath) {
    this._brushLineRemovedObservers.forEach(x => {
      x.next(line);
    });
  }

  brushLineRemoved(): Observable<LinePath> {
    return Observable.create(observer => {
      this._brushLineRemovedObservers.push(observer);
    });
  }

  triggerBrushLinesCleared() {
    this._brushLinesClearedObservers.forEach(x => {
      x.next(null);
    });
  }

  brushLinesCleared(): Observable<void> {
    return Observable.create(observer => {
      this._brushLinesClearedObservers.push(observer);
    });
  }

  triggerMarkerAdded(marker: MapMarker) {
    this._markerAddedObservers.forEach(x => {
      x.next(marker);
    });
  }

  markerAdded(): Observable<MapMarker> {
    return Observable.create(observer => {
      this._markerAddedObservers.push(observer);
    });
  }

  triggerMarkerRemoved(marker: MapMarker) {
    this._markerRemovedObservers.forEach(x => {
      x.next(marker);
    });
  }

  markerRemoved(): Observable<MapMarker> {
    return Observable.create(observer => {
      this._markerRemovedObservers.push(observer);
    });
  }

  triggerUndo(event: ChangeHistoryEvent<any>) {
    this._undoEventObservers.forEach(x => {
      x.next(event);
    });
  }

  undoEvent(): Observable<ChangeHistoryEvent<any>> {
    return Observable.create(observer => {
      this._undoEventObservers.push(observer);
    });
  }

  triggerRedo(event: ChangeHistoryEvent<any>) {
    this._redoEventObservers.forEach(x => {
      x.next(event);
    });
  }

  redoEvent(): Observable<ChangeHistoryEvent<any>> {
    return Observable.create(observer => {
      this._redoEventObservers.push(observer);
    });
  }
}
