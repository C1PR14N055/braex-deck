import { Injectable } from "@angular/core";
import * as ComboKeys from "combokeys";

export const HOTKEY_SEPARATOR = "+";

@Injectable({
  providedIn: "root"
})
export class HotkeyService {
  private _documentComboKeys: any;

  constructor() {
    this._documentComboKeys = new ComboKeys(document.documentElement);
  }

  registerHotkey(keyCombination: string, handler: () => void) {
    this._documentComboKeys.bind(keyCombination, handler);
  }

  unregisterHotkey(keyCombination: string) {
    this._documentComboKeys.unbind(keyCombination);
  }
}
