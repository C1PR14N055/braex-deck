import { Injectable } from "@angular/core";
import { OverwolfCoreService } from "../overwolf/overwolf-core.service";
import { WindowCommunicator } from "./window-communicator";

export const COMMUNICATION_INTERFACE_NAME = "__braex_data_exchange";

@Injectable({
  providedIn: "root"
})
export class WindowDataExchangeService {
  private _mainWindow: Window | null = null;

  constructor(private _overwolfCore: OverwolfCoreService) {
    this._defineCommunicationInterface();
  }

  private _defineCommunicationInterface() {
    this._mainWindow = this._overwolfCore.getMainWindow();
    // check if the communication interface is already defined
    if (
      !this._mainWindow ||
      (this._mainWindow &&
        typeof this._mainWindow[COMMUNICATION_INTERFACE_NAME] !== "undefined")
    ) {
      return;
    }
    this._mainWindow[COMMUNICATION_INTERFACE_NAME] = new WindowCommunicator();
  }

  getWindowCommunicator(): WindowCommunicator | null {
    if (!this._mainWindow) {
      return null;
    }

    return this._mainWindow[COMMUNICATION_INTERFACE_NAME];
  }
}
