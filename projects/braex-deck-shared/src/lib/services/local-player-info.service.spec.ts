import { fakeAsync, TestBed, tick } from "@angular/core/testing";

import { LocalPlayerInfoService } from "./local-player-info.service";
import { Subscription } from "rxjs";
import { OverwolfGameEventsService } from "../overwolf/overwolf-game-events.service";
import { FakeOverwolfGameEventsService } from "../overwolf/testing/fake-overwolf-game-events.service";
import { AppSettingsService } from "./app-settings.service";
import { FakeAppSettingsService } from "./testing/fake-app-settings.service";

describe("LocalPlayerInfoService", () => {
  let service: LocalPlayerInfoService;
  let fakeGameEvents: FakeOverwolfGameEventsService;
  let fakeAppSettings: FakeAppSettingsService;
  let subs: Subscription[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        LocalPlayerInfoService,
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        },
        { provide: AppSettingsService, useClass: FakeAppSettingsService }
      ]
    });

    service = TestBed.get(LocalPlayerInfoService);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    fakeAppSettings = TestBed.get(AppSettingsService);
    subs = [];
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should emit the name of the player when it's available", fakeAsync(() => {
    let receivedName: string = undefined;
    const sub = service.localPlayerNickname().subscribe(nn => {
      receivedName = nn;
    });
    subs.push(sub);

    const name = "TestPlayer";
    fakeGameEvents.triggerLocalPlayerName(name);
    tick(500);
    expect(receivedName).toBe(name);
  }));

  it("should buffer the name of the player once it has been emitted", fakeAsync(() => {
    const name = "TestPlayer";
    fakeGameEvents.triggerLocalPlayerName(name);
    tick(500);

    let receivedName: string = undefined;
    const sub = service.localPlayerNickname().subscribe(nn => {
      receivedName = nn;
    });
    subs.push(sub);
    tick(500);

    expect(receivedName).toBe(name);
  }));

  it("should save the name of the player in the app settings after it has been retrieved once", fakeAsync(() => {
    fakeAppSettings.localPlayerNickname = null;
    const name = "TestPlayer";
    fakeGameEvents.triggerLocalPlayerName(name);
    tick(500);

    expect(fakeAppSettings.localPlayerNickname).toBe(name);
  }));

  it("should emit the name from the app settings when it's available", fakeAsync(() => {
    const name = "TestPlayer";
    fakeAppSettings.localPlayerNickname = name;

    const newService = new LocalPlayerInfoService(
      TestBed.get(OverwolfGameEventsService),
      TestBed.get(AppSettingsService)
    );

    let receivedName: string = undefined;
    const sub = newService.localPlayerNickname().subscribe(nn => {
      receivedName = nn;
    });
    subs.push(sub);
    tick(500);

    expect(receivedName).toBe(name);
  }));

  it("should update the name of the local player in the app settings when it changes", fakeAsync(() => {
    const savedName = "TestPlayer";
    fakeAppSettings.localPlayerNickname = savedName;

    const newService = new LocalPlayerInfoService(
      TestBed.get(OverwolfGameEventsService),
      TestBed.get(AppSettingsService)
    );

    let receivedName: string = undefined;
    const sub = newService.localPlayerNickname().subscribe(nn => {
      receivedName = nn;
    });
    subs.push(sub);
    tick(500);

    expect(receivedName).toBe(savedName);
    const updatedName = "NotTestPlayer";
    fakeGameEvents.triggerLocalPlayerName(updatedName);
    tick(500);

    expect(receivedName).toBe(updatedName);
    expect(fakeAppSettings.localPlayerNickname).toBe(updatedName);
  }));

  it("should use the name from the app settings when it changes", fakeAsync(() => {
    let receivedName: string = undefined;
    const sub = service.localPlayerNickname().subscribe(n => {
      receivedName = n;
    });
    subs.push(sub);
    tick(500);

    expect(receivedName).toBeFalsy();
    const nickname = "TestPlayer";
    fakeAppSettings.localPlayerNickname = nickname;
    tick(500);

    expect(receivedName).toBe(nickname);
  }));

  afterEach(() => {
    service.ngOnDestroy();
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
