import { fakeAsync, TestBed, tick } from "@angular/core/testing";

import { GameTrackerService } from "./game-tracker.service";
import { OverwolfGameEventsService } from "../overwolf/overwolf-game-events.service";
import { FakeOverwolfGameEventsService } from "../overwolf/testing/fake-overwolf-game-events.service";
import { PlayerStatsService } from "./player-stats.service";
import { GameMode } from "../ds/game/game-mode.enum";
import { of, Subscription } from "rxjs";
import { PlayerStats } from "../ds/player/player-stats";
import { List } from "immutable";
import { FakePlayerStatsService } from "./testing/fake-player-stats.service";
import { GamePhase } from "../ds/game/game-phase.enum";
import { FakeOverwolfCoreService } from "../overwolf/testing/fake-overwolf-core.service";
import { OverwolfCoreService } from "../overwolf/overwolf-core.service";
import { Participant } from "../ds/player/participant";
import { LocalStorageService } from "./local-storage.service";
import { FakeLocalStorageService } from "./testing/fake-local-storage.service";
import {
  AppSettingsService,
  LAST_PLAYED_GAME_MODE
} from "./app-settings.service";
import { LocalPlayerInfoService } from "./local-player-info.service";
import { FakeLocalPlayerInfoService } from "./testing/fake-local-player-info.service";

describe("GameTrackerService", () => {
  let service: GameTrackerService;
  let fakeGameEvents: FakeOverwolfGameEventsService;
  let fakePlayerStats: FakePlayerStatsService;
  let fakeCoreService: FakeOverwolfCoreService;
  let fakeStorage: FakeLocalStorageService;
  let appSettings: AppSettingsService;
  let fakeLocalPlayer: FakeLocalPlayerInfoService;
  let subs: Subscription[];
  let lastMode: GameMode;

  beforeEach(() => {
    lastMode = GameMode.SOLO;
    fakeStorage = new FakeLocalStorageService();
    fakeStorage.set(LAST_PLAYED_GAME_MODE, GameMode.SOLO);

    TestBed.configureTestingModule({
      providers: [
        GameTrackerService,
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        },
        { provide: PlayerStatsService, useClass: FakePlayerStatsService },
        { provide: OverwolfCoreService, useClass: FakeOverwolfCoreService },
        { provide: LocalStorageService, useValue: fakeStorage },
        AppSettingsService,
        {
          provide: LocalPlayerInfoService,
          useClass: FakeLocalPlayerInfoService
        }
      ]
    });
    service = TestBed.get(GameTrackerService);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    fakePlayerStats = TestBed.get(PlayerStatsService);
    fakeCoreService = TestBed.get(OverwolfCoreService);
    appSettings = TestBed.get(AppSettingsService);
    fakeLocalPlayer = TestBed.get(LocalPlayerInfoService);
  });

  beforeEach(() => {
    subs = [];
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should notify all observers that the players changed when a player joins the game", fakeAsync(() => {
    let receivedPlayers: List<Participant>;
    const sub = service.playersChanged().subscribe(game => {
      receivedPlayers = game;
    });
    subs.push(sub);

    const player = "TestPlayer1";
    fakePlayerStats.playerStatsToEmit = List<PlayerStats>([
      _buildFakePlayerStats(player)
    ]);

    const gameMode = GameMode.SOLO;
    fakeGameEvents.triggerCurrentGameMode(gameMode);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);
    fakeGameEvents.triggerPlayerJoin(player);
    tick(10050);
    fakeGameEvents.triggerMatchStart();
    tick(2500);

    expect(receivedPlayers.size).toBe(1);
    const receivedNames = receivedPlayers.map(x => x.stats.nickname);
    expect(receivedNames).toContain(player);
  }));

  function _buildFakePlayerStats(
    nickname: string,
    stats?: { kdRatio?: number; winRatio?: number; headshotRatio?: number }
  ) {
    return new PlayerStats(
      nickname,
      stats && stats.kdRatio ? stats.kdRatio : 0.8209,
      stats && stats.winRatio ? stats.winRatio : 3.0482,
      stats && stats.headshotRatio ? stats.headshotRatio : 3.7931,
      0.8209,
      0.13433,
      115.49009,
      251.592029999999994060999597422778606414794921875,
      326.46099,
      7,
      134
    );
  }

  it("should combine the players to a list when many players join at once", fakeAsync(() => {
    const players = [
      "TestPlayer1",
      "TestPlayer2",
      "TestPlayer3",
      "TestPlayer4"
    ];
    const spy = spyOn(fakePlayerStats, "playerStats").and.returnValue(
      of(List<PlayerStats>(players.map(x => _buildFakePlayerStats(x))))
    );

    const gameMode = GameMode.SOLO;
    fakeGameEvents.triggerCurrentGameMode(gameMode);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);
    players.forEach(nickname => {
      fakeGameEvents.triggerPlayerJoin(nickname);
    });
    tick(10050);
    fakeGameEvents.triggerMatchStart();
    tick(2500);
    expect(spy).toHaveBeenCalledWith(
      gameMode,
      appSettings.preferredPerspective,
      ...players
    );
  }));

  it("should clear the game info when in the lobby and reset the game mode to the last played one", fakeAsync(() => {
    fakeStorage.delete(LAST_PLAYED_GAME_MODE);
    expect(fakeStorage.has(LAST_PLAYED_GAME_MODE)).toBeFalsy();

    const gameMode = GameMode.SOLO;
    fakeGameEvents.triggerCurrentGameMode(gameMode);
    expect(service.searchGameMode).toBe(gameMode);

    const player = "TestPlayer1";
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    fakePlayerStats.playerStatsToEmit = List([_buildFakePlayerStats(player)]);
    tick(6050);
    fakeGameEvents.triggerPlayerJoin(player);
    tick(10050);
    fakeGameEvents.triggerMatchStart();
    tick(2500);
    expect(service.currentPlayers.size).toBe(1);

    fakeGameEvents.triggerMatchLeft();
    tick(2000);
    expect(service.currentPlayers.size).toBeFalsy();
    expect(service.searchGameMode).toBe(gameMode);
    expect(fakeStorage.has(LAST_PLAYED_GAME_MODE)).toBeTruthy();
    expect(fakeStorage.get(LAST_PLAYED_GAME_MODE)).toBe(gameMode as string);
  }));

  it("should notify all observers when in the lobby and the info has been cleared", fakeAsync(() => {
    let receivedPlayers: List<Participant>;
    const sub = service.playersChanged().subscribe(game => {
      receivedPlayers = game;
    });
    subs.push(sub);

    const gameMode = GameMode.SOLO;
    fakeGameEvents.triggerCurrentGameMode(gameMode);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);

    const player1 = "TestPlayer1";
    const player2 = "TestPlayer2";
    fakeGameEvents.triggerPlayerTeamNicknames([player1, player2]);
    fakePlayerStats.playerStatsToEmit = List([
      _buildFakePlayerStats(player1),
      _buildFakePlayerStats(player2)
    ]);
    tick(50);
    fakeGameEvents.triggerPlayerJoin(player1);
    tick(100);
    fakeGameEvents.triggerPlayerJoin(player2);
    tick(10050);
    fakeGameEvents.triggerMatchStart();
    tick(3000);
    expect(receivedPlayers.size).toBe(2);

    fakeGameEvents.triggerMatchLeft();
    tick(2000);
    expect(receivedPlayers.size).toBeFalsy();
  }));

  it("should remove the player immediately when the match has started and he leaves", fakeAsync(() => {
    const player1 = "TestPlayer1";
    const player2 = "TestPlayer2";
    let receivedPlayers: List<Participant>;
    const sub = service.playersChanged().subscribe(players => {
      receivedPlayers = players;
    });
    subs.push(sub);

    const gameMode = GameMode.SOLO;
    fakeGameEvents.triggerCurrentGameMode(gameMode);
    fakeGameEvents.triggerPlayerTeamNicknames([player1, player2]);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);

    fakePlayerStats.playerStatsToEmit = List([_buildFakePlayerStats(player1)]);
    tick(3000);
    fakeGameEvents.triggerPlayerJoin(player1);
    tick(3000);
    fakePlayerStats.playerStatsToEmit = List([_buildFakePlayerStats(player2)]);
    fakeGameEvents.triggerPlayerJoin(player2);

    tick(10050);
    fakeGameEvents.triggerMatchStart();
    tick(10000);
    expect(service.fetchingPlayerStats).toBeFalsy();
    expect(receivedPlayers.size).toBe(2);

    tick(3000);
    fakeGameEvents.triggerPlayerLeft(player1);
    tick(3000);
    expect(receivedPlayers.size).toBe(1);
    expect(receivedPlayers.get(0).stats.nickname).toBe(player2);
  }));

  it("should not request the player stats when the player joins and leaves immediately and the request has not been sent yet", fakeAsync(() => {
    const player1 = "TestPlayer1";
    const player2 = "TestPlayer2";
    const spy = spyOn(fakePlayerStats, "playerStats").and.returnValue(
      of(List([_buildFakePlayerStats(player1)]))
    );
    let receivedPlayers: List<Participant>;
    const sub = service.playersChanged().subscribe(players => {
      receivedPlayers = players;
    });
    subs.push(sub);

    const gameMode = GameMode.SOLO;
    fakeGameEvents.triggerCurrentGameMode(gameMode);
    fakeGameEvents.triggerPlayerTeamNicknames([player1, player2]);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);

    fakeGameEvents.triggerPlayerJoin(player1);
    tick(50);
    fakeGameEvents.triggerPlayerJoin(player2);
    tick(50);
    fakeGameEvents.triggerPlayerLeft(player2);
    tick(10050);
    fakeGameEvents.triggerMatchStart();
    tick(3000);
    expect(spy).toHaveBeenCalledWith(
      gameMode,
      appSettings.preferredPerspective,
      player1
    );
    expect(receivedPlayers.size).toBe(1);
    expect(receivedPlayers.get(0).stats.nickname).toBe(player1);
  }));

  it("should mark friendly players", fakeAsync(() => {
    const players = [
      {
        name: "TestPlayer1",
        friendly: true
      },
      {
        name: "TestPlayer2",
        friendly: false
      },
      {
        name: "TestPlayer3",
        friendly: true
      },
      {
        name: "TestPlayer4",
        friendly: false
      }
    ];
    const friendlyPlayers = players.filter(x => x.friendly).map(x => x.name);
    let receivedPlayers: List<Participant>;
    const sub = service.playersChanged().subscribe(p => {
      receivedPlayers = p;
    });
    subs.push(sub);

    fakeGameEvents.triggerPlayerTeamNicknames(friendlyPlayers);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);

    fakePlayerStats.playerStatsToEmit = List(
      players.map(x => _buildFakePlayerStats(x.name))
    );
    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x.name);
      tick(10);
    });
    tick(10050);
    fakeGameEvents.triggerMatchStart();
    tick(3000);
    expect(receivedPlayers.size).toBe(players.length);
    receivedPlayers.forEach(x => {
      if (friendlyPlayers.indexOf(x.stats.nickname) >= 0) {
        expect(x.friendly).toBeTruthy();
      } else {
        expect(x.friendly).toBeFalsy();
      }
    });
  }));

  it("should check if the stats for all requested players have been returned", fakeAsync(() => {
    const players = [
      "TestPlayer1",
      "TestPlayer2",
      "TestPlayer3",
      "TestPlayer4",
      "TestPlayer5"
    ];
    const spy = spyOn(fakePlayerStats, "playerStats").and.returnValues(
      of(
        List([
          _buildFakePlayerStats(players[0]),
          _buildFakePlayerStats(players[1]),
          _buildFakePlayerStats(players[2])
        ])
      ),
      of(
        List([
          _buildFakePlayerStats(players[3]),
          _buildFakePlayerStats(players[4])
        ])
      )
    );

    const gameMode = GameMode.SOLO;
    let receivedPlayers: List<Participant>;
    const sub = service.playersChanged().subscribe(p => {
      receivedPlayers = p;
    });
    subs.push(sub);

    fakeGameEvents.triggerCurrentGameMode(gameMode);
    tick(50);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(50);

    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(10);
    });
    tick(1940);
    expect(spy).toHaveBeenCalledWith(
      gameMode,
      appSettings.preferredPerspective,
      ...players
    );
    expect(receivedPlayers.size).toBe(3);
    let nicknames = receivedPlayers.map(x => x.stats.nickname);
    expect(nicknames).toContain(players[0]);
    expect(nicknames).toContain(players[1]);
    expect(nicknames).toContain(players[2]);
    tick(2050);

    expect(spy).toHaveBeenCalledWith(
      gameMode,
      appSettings.preferredPerspective,
      players[3],
      players[4]
    );
    expect(receivedPlayers.size).toBe(5);
    tick(3000);

    fakeGameEvents.triggerMatchStart();
    tick(3000);

    nicknames = receivedPlayers.map(x => x.stats.nickname);
    players.forEach(x => {
      expect(nicknames).toContain(x);
    });
  }));

  it("should compute the average kd of the lobby", fakeAsync(() => {
    const players = ["TestPlayer1", "TestPlayer2", "TestPlayer3"];
    fakePlayerStats.playerStatsToEmit = List([
      _buildFakePlayerStats(players[0]),
      _buildFakePlayerStats(players[1], { kdRatio: 0.9483 }),
      _buildFakePlayerStats(players[2], { kdRatio: 4.5682 })
    ]);

    fakeGameEvents.triggerCurrentGameMode(GameMode.SOLO);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);

    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(10);
    });
    tick(2000);
    fakeGameEvents.triggerMatchStart();
    tick(3000);

    expect(service.averageLobbyStats.kdRatio).toBeCloseTo(
      2.112466666666666666666666667
    );
  }));

  it("should compute the average win ratio of the lobby", fakeAsync(() => {
    const players = ["TestPlayer1", "TestPlayer2", "TestPlayer2"];
    fakePlayerStats.playerStatsToEmit = List([
      _buildFakePlayerStats(players[0]),
      _buildFakePlayerStats(players[1], { winRatio: 12.0429 }),
      _buildFakePlayerStats(players[2], { winRatio: 21.834 })
    ]);
    fakeGameEvents.triggerCurrentGameMode(GameMode.SOLO);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);

    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(10);
    });

    fakeGameEvents.triggerMatchStart();
    tick(3000);

    expect(service.averageLobbyStats.winRatio).toBeCloseTo(
      12.30836666666666666666666667
    );
  }));

  it("should compute the average headshot ratio of the lobby", fakeAsync(() => {
    const players = ["TestPlayer1", "TestPlayer2", "TestPlayer3"];
    fakePlayerStats.playerStatsToEmit = List([
      _buildFakePlayerStats(players[0]),
      _buildFakePlayerStats(players[1], { headshotRatio: 16.2486 }),
      _buildFakePlayerStats(players[2], { headshotRatio: 23.4113 })
    ]);
    fakeGameEvents.triggerCurrentGameMode(GameMode.SOLO);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);

    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(10);
    });

    fakeGameEvents.triggerMatchStart();
    tick(3000);

    expect(service.averageLobbyStats.headshotRatio).toBeCloseTo(14.484333333);
  }));

  it("should compute the min kd ratio of the lobby", fakeAsync(() => {
    const players = ["TestPlayer1", "TestPlayer2", "TestPlayer3"];
    fakePlayerStats.playerStatsToEmit = List([
      _buildFakePlayerStats(players[0]),
      _buildFakePlayerStats(players[1], { kdRatio: 0.4023 }),
      _buildFakePlayerStats(players[2], { kdRatio: 2.5793 })
    ]);
    fakeGameEvents.triggerCurrentGameMode(GameMode.SOLO);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);

    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(10);
    });

    fakeGameEvents.triggerMatchStart();
    tick(3000);

    expect(service.minLobbyStats.kdRatio).toBeCloseTo(0.4023);
  }));

  it("should compute the min win ratio of the lobby", fakeAsync(() => {
    const players = ["TestPlayer1", "TestPlayer2", "TestPlayer3"];
    fakePlayerStats.playerStatsToEmit = List([
      _buildFakePlayerStats(players[0]),
      _buildFakePlayerStats(players[1], { winRatio: 6.2035 }),
      _buildFakePlayerStats(players[2], { winRatio: 3.5333 })
    ]);
    fakeGameEvents.triggerCurrentGameMode(GameMode.SOLO);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);
    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(10);
    });

    fakeGameEvents.triggerMatchStart();
    tick(3000);

    expect(service.minLobbyStats.winRatio).toBeCloseTo(3.0482);
  }));

  it("should compute the min headshot ratio of the lobby", fakeAsync(() => {
    const players = ["TestPlayer1", "TestPlayer2", "TestPlayer3"];
    fakePlayerStats.playerStatsToEmit = List([
      _buildFakePlayerStats(players[0]),
      _buildFakePlayerStats(players[1], { headshotRatio: 26.1256 }),
      _buildFakePlayerStats(players[2], { headshotRatio: 15.1658 })
    ]);
    fakeGameEvents.triggerCurrentGameMode(GameMode.SOLO);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);
    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(10);
    });

    fakeGameEvents.triggerMatchStart();
    tick(3000);

    expect(service.minLobbyStats.headshotRatio).toBeCloseTo(3.7931);
  }));

  it("should compute the max kd ratio of the lobby", fakeAsync(() => {
    const players = ["TestPlayer1", "TestPlayer2", "TestPlayer3"];
    fakePlayerStats.playerStatsToEmit = List([
      _buildFakePlayerStats(players[0]),
      _buildFakePlayerStats(players[1], { kdRatio: 3.3931 }),
      _buildFakePlayerStats(players[2], { kdRatio: 12.1234 })
    ]);
    fakeGameEvents.triggerCurrentGameMode(GameMode.SOLO);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);
    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(10);
    });

    fakeGameEvents.triggerMatchStart();
    tick(3000);

    expect(service.maxLobbyStats.kdRatio).toBeCloseTo(12.1234);
  }));

  it("should compute the max win ratio of the lobby", fakeAsync(() => {
    const players = ["TestPlayer1", "TestPlayer2", "TestPlayer3"];
    fakePlayerStats.playerStatsToEmit = List([
      _buildFakePlayerStats(players[0]),
      _buildFakePlayerStats(players[1], { winRatio: 12.4152 }),
      _buildFakePlayerStats(players[2], { winRatio: 15.1542 })
    ]);
    fakeGameEvents.triggerCurrentGameMode(GameMode.SOLO);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);
    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(10);
    });

    fakeGameEvents.triggerMatchStart();
    tick(3000);

    expect(service.maxLobbyStats.winRatio).toBeCloseTo(15.1542);
  }));

  it("should compute the max headshot ratio of the lobby", fakeAsync(() => {
    const players = ["TestPlayer1", "TestPlayer2", "TestPlayer3"];
    fakePlayerStats.playerStatsToEmit = List([
      _buildFakePlayerStats(players[0]),
      _buildFakePlayerStats(players[1], { headshotRatio: 19.2687 }),
      _buildFakePlayerStats(players[2], { headshotRatio: 53.1851 })
    ]);
    fakeGameEvents.triggerCurrentGameMode(GameMode.SOLO);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);
    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(10);
    });

    fakeGameEvents.triggerMatchStart();
    tick(3000);

    expect(service.maxLobbyStats.headshotRatio).toBeCloseTo(53.1851);
  }));

  it("should start fetching the player stats in the AIRFIELD phase", fakeAsync(() => {
    expect(service.fetchingPlayerStats).toBeFalsy();
    fakeGameEvents.triggerCurrentGameMode(GameMode.SOLO);
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(6050);
    expect(service.fetchingPlayerStats).toBeTruthy();

    fakeGameEvents.triggerMatchStart();
    tick(3000);
  }));

  it("should stop fetching the player stats when pubg is terminated", fakeAsync(() => {
    expect(service.fetchingPlayerStats).toBeFalsy();
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);

    let receivedPlayers: List<Participant>;
    const sub = service.playersChanged().subscribe(p => {
      receivedPlayers = p;
    });
    subs.push(sub);

    const players = ["TestPlayer1", "TestPlayer2"];
    fakePlayerStats.playerStatsToEmit = List([
      _buildFakePlayerStats(players[0]),
      _buildFakePlayerStats(players[1])
    ]);
    tick(50);
    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
    });
    tick(6002);
    fakeGameEvents.triggerMatchStart();
    tick(10050);
    expect(receivedPlayers.size).toBe(2);
    tick(400);
    fakeCoreService.triggerPubgTerminated();
    tick(500);
    expect(service.currentPlayers.size).toBeFalsy();
    expect(service.fetchingPlayerStats).toBeFalsy();
  }));

  it("should save the game mode of the current game as the LAST_PLAYED_GAME_MODE", fakeAsync(() => {
    fakeStorage.delete(LAST_PLAYED_GAME_MODE);
    expect(fakeStorage.has(LAST_PLAYED_GAME_MODE)).toBeFalsy();
    const gameMode = GameMode.DUO;
    fakeGameEvents.triggerCurrentGameMode(gameMode);
    tick(1000);

    expect(fakeStorage.has(LAST_PLAYED_GAME_MODE)).toBeTruthy();
    expect(fakeStorage.get(LAST_PLAYED_GAME_MODE)).toBe(gameMode as string);

    fakeCoreService.triggerPubgTerminated();
    tick(1000);
  }));

  it("should re-search all players with the newly received game mode when it's different than the last one", fakeAsync(() => {
    expect(service.fetchingPlayerStats).toBeFalsy();
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(50);
    expect(service.searchGameMode).toBe(lastMode);
    tick(50);
    expect(service.fetchingPlayerStats).toBeTruthy();

    let receivedPlayers: List<Participant>;
    const sub = service.playersChanged().subscribe(p => {
      receivedPlayers = p;
    });
    subs.push(sub);

    const players = [
      "TestPlayer1",
      "TestPlayer2",
      "TestPlayer3",
      "TestPlayer4"
    ];
    fakePlayerStats.playerStatsToEmit = List(
      players.map(x => _buildFakePlayerStats(x))
    );
    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(10);
    });
    tick(1810);
    fakeGameEvents.triggerMatchStart();
    tick(50);
    expect(service.fetchingPlayerStats).toBeFalsy();
    expect(receivedPlayers.size).toBe(players.length);

    const gameMode = GameMode.SQUAD;
    spyOn(fakePlayerStats, "playerStats").and.returnValues(
      of(
        List([
          _buildFakePlayerStats(players[0]),
          _buildFakePlayerStats(players[1])
        ])
      ),
      of(
        List([
          _buildFakePlayerStats(players[2]),
          _buildFakePlayerStats(players[3])
        ])
      )
    );

    fakeGameEvents.triggerCurrentGameMode(gameMode);
    tick(50);
    expect(service.fetchingPlayerStats).toBeTruthy();
    expect(service.searchGameMode).toBe(gameMode);
    expect(receivedPlayers.size).toBe(2);
    tick(6050);
    expect(receivedPlayers.size).toBe(4);
    tick(10005);
    expect(service.fetchingPlayerStats).toBeFalsy();
  }));

  it("should use the DEFAULT_SEARCH_GAME_MODE as the current game mode when the LAST_PLAYED_GAME_MODE is not available", () => {
    appSettings.lastPlayedGameMode = null;
    const gameMode = GameMode.SQUAD;
    appSettings.defaultSearchGameMode = gameMode;

    const newService = new GameTrackerService(
      TestBed.get(OverwolfGameEventsService),
      TestBed.get(PlayerStatsService),
      TestBed.get(OverwolfCoreService),
      TestBed.get(AppSettingsService),
      TestBed.get(LocalPlayerInfoService)
    );
    expect(newService.searchGameMode).toBe(gameMode);
  });

  it("should use the LAST_PLAYED_GAME_MODE as the current game mode when it's available", () => {
    appSettings.defaultSearchGameMode = GameMode.SQUAD;
    const lastGameMode = GameMode.DUO;
    appSettings.lastPlayedGameMode = lastGameMode;

    const newService = new GameTrackerService(
      TestBed.get(OverwolfGameEventsService),
      TestBed.get(PlayerStatsService),
      TestBed.get(OverwolfCoreService),
      TestBed.get(AppSettingsService),
      TestBed.get(LocalPlayerInfoService)
    );
    expect(newService.searchGameMode).toBe(lastGameMode);
  });

  it("should set the current game mode the the default game mode after it has been set", fakeAsync(() => {
    appSettings.defaultSearchGameMode = null;
    appSettings.lastPlayedGameMode = null;
    appSettings.preferredPerspective = null;

    const newService = new GameTrackerService(
      TestBed.get(OverwolfGameEventsService),
      TestBed.get(PlayerStatsService),
      TestBed.get(OverwolfCoreService),
      TestBed.get(AppSettingsService),
      TestBed.get(LocalPlayerInfoService)
    );
    expect(newService.searchGameMode).toBeFalsy();
    appSettings.defaultSearchGameMode = GameMode.DUO;
    tick(500);
    expect(newService.searchGameMode).toBe(appSettings.defaultSearchGameMode);
  }));

  it("should mark the local player as friendly", fakeAsync(() => {
    const localPlayer = "TestLocalPlayer";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);

    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(500);

    let receivedPlayers: List<Participant>;
    const sub = service.playersChanged().subscribe(p => {
      receivedPlayers = p;
    });
    subs.push(sub);

    const players = ["TestPlayer1", "TestPlayer2", localPlayer, "TestPlayer3"];
    fakePlayerStats.playerStatsToEmit = List(
      players.map(x => _buildFakePlayerStats(x))
    );
    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(20);
    });
    tick(6050);

    expect(receivedPlayers.size).toBe(players.length);
    const matchingPlayers = receivedPlayers.filter(
      x => x.stats.nickname === localPlayer
    );
    const localPlayerStats = matchingPlayers.get(0);
    expect(localPlayerStats.friendly).toBeTruthy();

    fakeCoreService.triggerPubgTerminated();
    tick(3000);
  }));

  it("should mark the local player as friendly when his name is received after all participants", fakeAsync(() => {
    const oldLocalPlayer = "OldLocalPlayerName";
    appSettings.localPlayerNickname = oldLocalPlayer;
    fakeLocalPlayer.triggerLocalPlayerNickname(oldLocalPlayer);

    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(500);

    let receivedPlayers: List<Participant>;
    const sub = service.playersChanged().subscribe(p => {
      receivedPlayers = p;
    });
    subs.push(sub);

    const newLocalPlayer = "NewLocalPlayerName";
    const players = [
      "TestPlayer1",
      "TestPlayer2",
      newLocalPlayer,
      "TestPlayer3",
      "TestPlayer4"
    ];
    fakePlayerStats.playerStatsToEmit = List(
      players.map(x => _buildFakePlayerStats(x))
    );
    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(15);
    });
    tick(6050);
    expect(receivedPlayers.size).toBe(players.length);
    let matchingPlayers = receivedPlayers.filter(
      x => x.stats.nickname === newLocalPlayer
    );
    let localPlayer = matchingPlayers.get(0);
    expect(localPlayer.friendly).toBeFalsy();

    fakeLocalPlayer.triggerLocalPlayerNickname(newLocalPlayer);
    tick(500);
    expect(receivedPlayers.size).toBe(players.length);
    matchingPlayers = receivedPlayers.filter(
      x => x.stats.nickname === newLocalPlayer
    );
    localPlayer = matchingPlayers.get(0);
    expect(localPlayer.friendly).toBeTruthy();

    fakeCoreService.triggerPubgTerminated();
    tick(3000);
  }));

  it("should return only the current game mode not one from the settings or the last one", fakeAsync(() => {
    expect(service.currentGameMode).toBeNull();

    const gameMode = GameMode.SOLO;
    fakeGameEvents.triggerCurrentGameMode(gameMode);
    tick(50);
    expect(service.currentGameMode).toEqual(gameMode);
  }));

  it("should NOT request the missing players when the local player has left the match", fakeAsync(() => {
    expect(service.fetchingPlayerStats).toBeFalsy();
    fakeGameEvents.triggerPhaseChanged(GamePhase.AIRFIELD);
    tick(50);

    const players = [
      "TestPlayer1",
      "TestPlayer2",
      "TestPlayer3",
      "TestPlayer4"
    ];
    fakePlayerStats.playerStatsToEmit = List(
      players.map(x => _buildFakePlayerStats(x))
    );
    players.forEach(x => {
      fakeGameEvents.triggerPlayerJoin(x);
      tick(10);
    });
    tick(1810);
    fakeGameEvents.triggerMatchStart();
    tick(50);
    expect(service.fetchingPlayerStats).toBeTruthy();
    tick(6050);
    fakeGameEvents.triggerMatchLeft();
    tick(500);
    expect(service.fetchingPlayerStats).toBeFalsy();
  }));

  afterEach(() => {
    service.ngOnDestroy();
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
