import { Injectable, OnDestroy } from "@angular/core";
import { OverwolfGameEventsService } from "../overwolf/overwolf-game-events.service";
import { Observable, Observer, Subscription } from "rxjs";
import { GameMode } from "../ds/game/game-mode.enum";
import { List } from "immutable";
import { PlayerStatsService } from "./player-stats.service";
import { orderBy } from "lodash";
import { Participant } from "../ds/player/participant";
import { PlayerStats } from "../ds/player/player-stats";
import { GamePhase } from "../ds/game/game-phase.enum";
import { OverwolfCoreService } from "../overwolf/overwolf-core.service";
import { LobbyStats } from "../ds/lobby-stats";
import { AppSettingsService } from "./app-settings.service";
import { LocalPlayerInfoService } from "./local-player-info.service";

@Injectable({
  providedIn: "root"
})
export class GameTrackerService implements OnDestroy {
  private _subs: Subscription[] = [];
  private _searchGameMode: GameMode | null = null;
  private _currentPlayers = List<Participant>();
  private _currentGameMode: GameMode | null = null;
  private _matchStarted = false;
  private _playersChangedObservers: Observer<List<Participant>>[] = [];
  private _playersToRemoveQueue = List<string>();
  private _playerFetchQueue = List<string>();
  private _teamMates = List<string>();
  private _fetchingPlayerStats = false;
  private _averageLobbyStats = LobbyStats.create();
  private _minLobbyStats = LobbyStats.create();
  private _maxLobbyStats = LobbyStats.create();
  private _localPlayerStats = new PlayerStats();
  private _localPlayerNickname: string;

  constructor(
    private _gameEventsService: OverwolfGameEventsService,
    private _playerStatsService: PlayerStatsService,
    private _coreService: OverwolfCoreService,
    private _appSettings: AppSettingsService,
    private _localPlayerInfo: LocalPlayerInfoService
  ) {
    this._initCurrentGameMode();
    this._initEvents();
  }

  private _initCurrentGameMode() {
    const lastPlayedGameMode = this._appSettings.lastPlayedGameMode;
    const defaultSearchGameMode = this._appSettings.defaultSearchGameMode;
    this._searchGameMode = lastPlayedGameMode || defaultSearchGameMode;

    const sub = this._appSettings
      .defaultSearchGameModeChanged()
      .subscribe(mode => {
        if (!this._searchGameMode) {
          this._searchGameMode = mode;
        }
      });
    this._subs.push(sub);
  }

  private _initEvents() {
    let sub = this._localPlayerInfo
      .localPlayerNickname()
      .subscribe(nickname => {
        this._localPlayerNickname = nickname;
        this._markLocalPlayerAsFriendly();
      });
    this._subs.push(sub);

    sub = this._gameEventsService.onPhaseChanged().subscribe(phase => {
      if (phase === GamePhase.AIRFIELD) {
        this._startPlayerFetchLoop();
      }
    });
    this._subs.push(sub);

    sub = this._gameEventsService.onMatchLeft().subscribe(() => {
      this._killPlayerFetchQueue();
      this._clearCurrentGameInfo();
    });
    this._subs.push(sub);

    sub = this._gameEventsService.currentGameMode().subscribe(gameMode => {
      const oldGameMode = this._searchGameMode;
      this._appSettings.lastPlayedGameMode = gameMode;
      this._searchGameMode = gameMode;
      this._currentGameMode = gameMode;

      if (oldGameMode !== gameMode) {
        this._playerFetchQueue = this._playerFetchQueue.push(
          ...this._currentPlayers.map(x => x.stats.nickname).toArray()
        );
        this._currentPlayers = this._currentPlayers.clear();
        this._triggerPlayersChanged();
        if (!this._fetchingPlayerStats) {
          this._startPlayerFetchLoop();
        }
      }
    });
    this._subs.push(sub);

    sub = this._gameEventsService.playerTeamNicknames().subscribe(team => {
      this._teamMates = this._teamMates.push(...team.toArray());
    });
    this._subs.push(sub);

    sub = this._gameEventsService.onPlayerJoin().subscribe(nickname => {
      this._playerFetchQueue = this._playerFetchQueue.push(nickname);
    });
    this._subs.push(sub);

    sub = this._gameEventsService.onPlayerLeft().subscribe(nickname => {
      if (this._playerFetchQueue.contains(nickname)) {
        const index = this._playerFetchQueue.indexOf(nickname);
        this._playerFetchQueue = this._playerFetchQueue.delete(index);

        return;
      }

      if (this._matchStarted) {
        const matchingPlayers = this._currentPlayers.filter(
          x => x.stats.nickname === nickname
        );
        if (matchingPlayers.size) {
          const index = this._currentPlayers.indexOf(matchingPlayers.get(0));
          this._currentPlayers = this._currentPlayers.delete(index);
          this._triggerPlayersChanged();
        }
      } else {
        this._playersToRemoveQueue = this._playersToRemoveQueue.push(nickname);
      }
    });
    this._subs.push(sub);

    sub = this._gameEventsService.onMatchStart().subscribe(() => {
      this._matchStarted = true;
      this._stopIntervalWhenReady();
    });
    this._subs.push(sub);

    sub = this._coreService.onPubgTerminated().subscribe(() => {
      this._fetchingPlayerStats = false;
      this._clearCurrentGameInfo();
    });
    this._subs.push(sub);
  }

  private _markLocalPlayerAsFriendly() {
    if (!this._currentPlayers.isEmpty()) {
      const matchingPlayers = this._currentPlayers.filter(
        x => x.stats.nickname === this._localPlayerNickname
      );
      if (!matchingPlayers.isEmpty()) {
        const localPlayer = matchingPlayers.get(0);
        localPlayer.friendly = true;
      }
    }
  }

  private _startPlayerFetchLoop() {
    this._fetchingPlayerStats = true;
    this._playerFetchLoop();
  }

  /**
   * We use this recursive function which calls itself after a timeout
   * instead of an interval because it's less painful to test than an interval.
   */
  private _playerFetchLoop() {
    if (this._searchGameMode && this._playerFetchQueue.size) {
      this._requestPlayerStats();
    }

    if (this._fetchingPlayerStats) {
      setTimeout(() => {
        this._playerFetchLoop();
      }, 2000);
    }
  }

  private async _requestPlayerStats() {
    const players = this._playerFetchQueue.toArray();
    this._playerFetchQueue = this._playerFetchQueue.clear();
    this._playerStatsService
      .playerStats(
        this._searchGameMode,
        this._appSettings.preferredPerspective,
        ...players
      )
      .subscribe(p => {
        this._requeueMissingPlayers(players, p.map(x => x.nickname).toArray());
        this._addPlayers(p);
        this._computeStats();
        this._triggerPlayersChanged();
        this._removeQueuedPlayers();
        this._stopIntervalWhenReady();
      });
  }

  private _stopIntervalWhenReady() {
    if (this._matchStarted && !this._playerFetchQueue.size) {
      this._fetchingPlayerStats = false;
    }
  }

  private _addPlayers(players: List<PlayerStats>) {
    this._currentPlayers = List(
      orderBy(
        this._currentPlayers
          .push(
            ...players
              .toArray()
              .map(
                stats =>
                  new Participant(
                    this._teamMates.contains(stats.nickname) ||
                      stats.nickname === this._localPlayerNickname,
                    stats
                  )
              )
          )
          .toArray(),
        ["stats.kdRatio", "stats.winRatio"],
        "desc"
      )
    );
  }

  private _computeStats() {
    let kdRatioSum = 0;
    let winRatioSum = 0;
    let headshotRatioSum = 0;

    let minKdRatio = 9999;
    let minWinRatio = 9999;
    let minHeadshotRatio = 9999;

    let maxKdRatio = 0;
    let maxWinRatio = 0;
    let maxHeadshotRatio = 0;

    let playersWithKdRatio = 0;
    let playersWithWinRatio = 0;
    let playersWithHeadshotRatio = 0;
    this._currentPlayers.forEach(x => {
      if (x.stats.nickname === this._localPlayerNickname) {
        this._localPlayerStats = x.stats;
      }

      if (x.stats.kdRatio) {
        playersWithKdRatio++;
        kdRatioSum += x.stats.kdRatio;

        if (x.stats.kdRatio < minKdRatio) {
          minKdRatio = x.stats.kdRatio;
        }

        if (x.stats.kdRatio > maxKdRatio) {
          maxKdRatio = x.stats.kdRatio;
        }
      }

      if (x.stats.winRatio) {
        playersWithWinRatio++;
        winRatioSum += x.stats.winRatio;

        if (x.stats.winRatio < minWinRatio) {
          minWinRatio = x.stats.winRatio;
        }

        if (x.stats.winRatio > maxWinRatio) {
          maxWinRatio = x.stats.winRatio;
        }
      }

      if (x.stats.headshotRatio) {
        playersWithHeadshotRatio++;
        headshotRatioSum += x.stats.headshotRatio;

        if (x.stats.headshotRatio < minHeadshotRatio) {
          minHeadshotRatio = x.stats.headshotRatio;
        }

        if (x.stats.headshotRatio > maxHeadshotRatio) {
          maxHeadshotRatio = x.stats.headshotRatio;
        }
      }
    });

    this._averageLobbyStats = LobbyStats.create(
      kdRatioSum / playersWithKdRatio,
      winRatioSum / playersWithWinRatio,
      headshotRatioSum / playersWithHeadshotRatio
    );
    this._minLobbyStats = LobbyStats.create(
      minKdRatio,
      minWinRatio,
      minHeadshotRatio
    );
    this._maxLobbyStats = LobbyStats.create(
      maxKdRatio,
      maxWinRatio,
      maxHeadshotRatio
    );
  }

  private _requeueMissingPlayers(requested: string[], gotten: string[]) {
    requested.forEach(nickname => {
      if (gotten.indexOf(nickname) < 0) {
        this._playerFetchQueue = this._playerFetchQueue.push(nickname);
      }
    });
  }

  private async _removeQueuedPlayers() {
    this._playersToRemoveQueue.forEach(nickname => {
      const matchingPlayers = this._currentPlayers.filter(
        x => x.stats.nickname === nickname
      );
      if (matchingPlayers.size) {
        const index = this._currentPlayers.indexOf(matchingPlayers.get(0));
        this._currentPlayers = this._currentPlayers.delete(index);
        this._triggerPlayersChanged();
      }
    });
  }

  private _clearCurrentGameInfo() {
    this._currentPlayers = this._currentPlayers.clear();
    this._triggerPlayersChanged();
  }

  private _killPlayerFetchQueue() {
    this._fetchingPlayerStats = false;
    this._playerFetchQueue = this._playerFetchQueue.clear();
  }

  private _triggerPlayersChanged() {
    this._playersChangedObservers.forEach(x => {
      x.next(this._currentPlayers);
    });
  }

  playersChanged(): Observable<List<Participant>> {
    return Observable.create(observer => {
      this._playersChangedObservers.push(observer);
    });
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  get currentPlayers() {
    return this._currentPlayers;
  }

  get currentGameMode() {
    return this._currentGameMode;
  }

  /**
   * The game mode which has been used to search for the stats of the players.
   * This may differ from the current game mode e.g. because we receive the
   * current game mode from overwolf only AFTER the game has started and since
   * we want the stats of the players as soon as possible we use either the
   * default game mode which has been defined by the user in the settings or
   * the last game mode which the user has played when it's available.
   */
  get searchGameMode() {
    return this._searchGameMode;
  }

  get fetchingPlayerStats() {
    return this._fetchingPlayerStats;
  }

  get averageLobbyStats() {
    return this._averageLobbyStats;
  }

  get minLobbyStats() {
    return this._minLobbyStats;
  }

  get maxLobbyStats() {
    return this._maxLobbyStats;
  }

  get localPlayerStats() {
    return this._localPlayerStats;
  }
}
