import { Injectable } from "@angular/core";
import * as ElementResizeDetectorFactory from "element-resize-detector";
import { Observable } from "rxjs";

@Injectable()
export class ResizeObserverService {
  private _resizeDetector: ElementResizeDetectorFactory.Erd;

  constructor() {
    this._resizeDetector = ElementResizeDetectorFactory();
  }

  observe(el: HTMLElement): Observable<HTMLElement> {
    return Observable.create(observer => {
      this._resizeDetector.listenTo(el, event => {
        observer.next(event);
      });
    });
  }
}
