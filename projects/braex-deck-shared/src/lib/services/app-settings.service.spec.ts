import { fakeAsync, TestBed, tick } from "@angular/core/testing";

import {
  AppSettingsService,
  BRUSH_TOOL_COMBO,
  BRUSH_TOOL_HOTKEY,
  CAR_MAP_MARKER_COMBO,
  CAR_MARKER_HOTKEY,
  DEFAULT_SEARCH_GAME_MODE,
  ERASER_TOOL_COMBO,
  ERASER_TOOL_HOTKEY,
  FAST_BACKWARD_COMBO,
  FAST_BACKWARD_HOTKEY,
  FAST_FORWARD_COMBO,
  FAST_FORWARD_HOTKEY,
  FOCUS_PLAYER_COMBO,
  FOCUS_PLAYER_HOTKEY,
  GENERIC_MAP_MARKER_COMBO,
  GENERIC_MARKER_HOTKEY,
  ITEM_MAP_MARKER_COMBO,
  ITEM_MARKER_HOTKEY,
  LAST_PLAYED_GAME_MODE,
  LOCAL_PLAYER_NICKNAME,
  MAP_MARKER_TOOL_COMBO,
  MAP_MARKER_TOOL_HOTKEY,
  PLAY_PAUSE_COMBO,
  PLAY_PAUSE_HOTKEY,
  PREFERRED_PERSPECTIVE,
  REDO_COMBO,
  REDO_HOTKEY,
  UNDO_COMBO,
  UNDO_HOTKEY,
  WEAPON_MAP_MARKER_COMBO,
  WEAPON_MARKER_HOTKEY
} from "./app-settings.service";
import { LocalStorageService } from "./local-storage.service";
import { FakeLocalStorageService } from "./testing/fake-local-storage.service";
import { Subscription } from "rxjs";
import { GameMode } from "../ds/game/game-mode.enum";
import { Perspective } from "../ds/game/perspective.enum";

describe("AppSettingsService", () => {
  let service: AppSettingsService;
  let fakeStorage: FakeLocalStorageService;
  let subs: Subscription[];
  let playerNickname: string;

  beforeEach(() => {
    playerNickname = "TestPlayer";
    fakeStorage = new FakeLocalStorageService();
    fakeStorage.set(DEFAULT_SEARCH_GAME_MODE, GameMode.SOLO);
    fakeStorage.set(PREFERRED_PERSPECTIVE, Perspective.FPP);
    fakeStorage.set(LAST_PLAYED_GAME_MODE, GameMode.DUO);
    fakeStorage.set(LOCAL_PLAYER_NICKNAME, playerNickname);
    fakeStorage.set(BRUSH_TOOL_HOTKEY, BRUSH_TOOL_COMBO);
    fakeStorage.set(MAP_MARKER_TOOL_HOTKEY, MAP_MARKER_TOOL_COMBO);
    fakeStorage.set(ERASER_TOOL_HOTKEY, ERASER_TOOL_COMBO);
    fakeStorage.set(GENERIC_MARKER_HOTKEY, GENERIC_MAP_MARKER_COMBO);
    fakeStorage.set(CAR_MARKER_HOTKEY, CAR_MAP_MARKER_COMBO);
    fakeStorage.set(WEAPON_MARKER_HOTKEY, WEAPON_MAP_MARKER_COMBO);
    fakeStorage.set(ITEM_MARKER_HOTKEY, ITEM_MAP_MARKER_COMBO);
    fakeStorage.set(PLAY_PAUSE_HOTKEY, PLAY_PAUSE_COMBO);
    fakeStorage.set(FAST_FORWARD_HOTKEY, FAST_FORWARD_COMBO);
    fakeStorage.set(FAST_BACKWARD_HOTKEY, FAST_BACKWARD_COMBO);
    fakeStorage.set(UNDO_HOTKEY, UNDO_COMBO);
    fakeStorage.set(REDO_HOTKEY, REDO_COMBO);
    fakeStorage.set(FOCUS_PLAYER_HOTKEY, FOCUS_PLAYER_COMBO);

    TestBed.configureTestingModule({
      providers: [
        AppSettingsService,
        { provide: LocalStorageService, useValue: fakeStorage }
      ]
    });
    service = TestBed.get(AppSettingsService);
  });

  beforeEach(() => {
    subs = [];
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should load the values from the local storage when instantiated", () => {
    expect(service.preferredPerspective).toEqual(
      fakeStorage.get(PREFERRED_PERSPECTIVE)
    );
    expect(service.lastPlayedGameMode).toEqual(
      fakeStorage.get(LAST_PLAYED_GAME_MODE)
    );
    expect(service.defaultSearchGameMode).toEqual(
      fakeStorage.get(DEFAULT_SEARCH_GAME_MODE)
    );
    expect(service.localPlayerNickname).toEqual(
      fakeStorage.get(LOCAL_PLAYER_NICKNAME)
    );
    expect(service.brushToolHotkey).toEqual(fakeStorage.get(BRUSH_TOOL_HOTKEY));
    expect(service.mapMarkerToolHotkey).toEqual(
      fakeStorage.get(MAP_MARKER_TOOL_HOTKEY)
    );
    expect(service.eraserToolHotkey).toEqual(
      fakeStorage.get(ERASER_TOOL_HOTKEY)
    );
    expect(service.genericMarkerHotkey).toEqual(
      fakeStorage.get(GENERIC_MARKER_HOTKEY)
    );
    expect(service.carMarkerHotkey).toEqual(fakeStorage.get(CAR_MARKER_HOTKEY));
    expect(service.weaponMarkerHotkey).toEqual(
      fakeStorage.get(WEAPON_MARKER_HOTKEY)
    );
    expect(service.itemMarkerHotkey).toEqual(
      fakeStorage.get(ITEM_MARKER_HOTKEY)
    );
    expect(service.playPauseHotkey).toEqual(fakeStorage.get(PLAY_PAUSE_HOTKEY));
    expect(service.fastForwardHotkey).toEqual(
      fakeStorage.get(FAST_FORWARD_HOTKEY)
    );
    expect(service.fastBackwardHotkey).toEqual(
      fakeStorage.get(FAST_BACKWARD_HOTKEY)
    );
    expect(service.undoHotkey).toEqual(fakeStorage.get(UNDO_HOTKEY));
    expect(service.redoHotkey).toEqual(fakeStorage.get(REDO_HOTKEY));
    expect(service.focusPlayerHotkey).toEqual(
      fakeStorage.get(FOCUS_PLAYER_HOTKEY)
    );
  });

  it("should notify all observers when the default search game mode changes", fakeAsync(() => {
    let receivedMode: GameMode;
    const sub = service.defaultSearchGameModeChanged().subscribe(m => {
      receivedMode = m;
    });
    subs.push(sub);

    const gameMode = GameMode.SQUAD;
    service.defaultSearchGameMode = gameMode;
    tick(500);
    expect(receivedMode).toBe(gameMode);
    expect(fakeStorage.get(DEFAULT_SEARCH_GAME_MODE)).toBe(gameMode as string);
  }));

  it("should notify all observers when the last played game mode changes", fakeAsync(() => {
    let receivedMode: GameMode;
    const sub = service.lastPlayedGameModeChanged().subscribe(m => {
      receivedMode = m;
    });
    subs.push(sub);

    const gameMode = GameMode.SQUAD;
    service.lastPlayedGameMode = gameMode;
    tick(500);
    expect(receivedMode).toBe(gameMode);
  }));

  it("should notify all observers when the preferred mode type changes", fakeAsync(() => {
    let receivedType: Perspective;
    const sub = service.preferredPerspectiveChanged().subscribe(t => {
      receivedType = t;
    });
    subs.push(sub);

    const gameModeType = Perspective.TPP;
    service.preferredPerspective = gameModeType;
    tick(500);
    expect(receivedType).toBe(gameModeType);
  }));

  it("should notify all observers when the player nickname changes", fakeAsync(() => {
    let receivedNickname: string;
    const sub = service.localPlayerNicknameChanged().subscribe(n => {
      receivedNickname = n;
    });
    subs.push(sub);

    const nickname = "SomeNicknameJustForTheTest";
    service.localPlayerNickname = nickname;
    tick(500);
    expect(receivedNickname).toBe(nickname);
  }));

  it("should notify all observers when the brush tool hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.brushToolHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "ctrl+1+2+3+4";
    service.brushToolHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should notify all observers when the map marker tool hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.mapMarkerToolHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "alt+f4";
    service.mapMarkerToolHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should notify all observers when the eraser tool hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.eraserToolHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "ctrl+alt+shift";
    service.eraserToolHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should notify all observer when the generic marker hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.genericMarkerHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "a+b+c+d";
    service.genericMarkerHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should notify all observers when the car marker hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.carMarkerHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "a+c+b+v+e+f+g";
    service.carMarkerHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should notify all observers when the weapon marker hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.weaponMarkerHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "ctrl+1+a+v";
    service.weaponMarkerHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should notify all observers when the item marker hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.itemMarkerHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "p+e+t+3+r";
    service.itemMarkerHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should notify all observers when the play pause hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.playPauseHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "g+i+t";
    service.playPauseHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should notify all observers when the fast forward hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.fastForwardHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "a+b+c+d+e+f";
    service.fastForwardHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should notify all observers when the fast backward hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.fastBackwardHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "j+e+s";
    service.fastBackwardHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should notify all observers when the undo hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.undoHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "1+2+3+4+5+6+7+8+9";
    service.undoHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should notify all observers when the redo hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.redoHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "ctrl+0+k+u";
    service.redoHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should notify all observers when the focus player hotkey changes", fakeAsync(() => {
    let received: string;
    const sub = service.focusPlayerHotkeyChanged().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    const hotkey = "ctr+1+2+3+4";
    service.focusPlayerHotkey = hotkey;
    tick(500);
    expect(received).toEqual(hotkey);
  }));

  it("should save the default search game mode in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.defaultSearchGameMode = GameMode.SQUAD;
    expect(spy).toHaveBeenCalled();
  });

  it("should save the preferred perspective in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.preferredPerspective = Perspective.TPP;
    expect(spy).toHaveBeenCalled();
  });

  it("should save the local player nickname in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.localPlayerNickname = "pEtTEeR1337";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the brush tool hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.brushToolHotkey = "peter";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the map marker tool hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.mapMarkerToolHotkey = "in";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the eraser tool hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.eraserToolHotkey = "link";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the generic marker hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.genericMarkerHotkey = "app";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the car marker hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.carMarkerHotkey = "car";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the weapon marker hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.weaponMarkerHotkey = "weapon";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the item marker hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.itemMarkerHotkey = "item";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the play pause hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.playPauseHotkey = "play";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the fast forward hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.fastForwardHotkey = "fast forward";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the fast backward hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.fastBackwardHotkey = "fast backward";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the undo hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.undoHotkey = "undo";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the redo hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.redoHotkey = "redo";
    expect(spy).toHaveBeenCalled();
  });

  it("should save the focus player hotkey in the local storage", () => {
    const spy = spyOn(fakeStorage, "set");
    service.focusPlayerHotkey = "focus";
    expect(spy).toHaveBeenCalled();
  });

  it("should set the brush tool hotkey when it's missing", () => {
    fakeStorage.delete(BRUSH_TOOL_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(BRUSH_TOOL_HOTKEY, BRUSH_TOOL_COMBO);
  });

  it("should set the map marker tool hotkey when it's missing", () => {
    fakeStorage.delete(MAP_MARKER_TOOL_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(
      MAP_MARKER_TOOL_HOTKEY,
      MAP_MARKER_TOOL_COMBO
    );
  });

  it("should set the eraser tool hotkey when it's missing", () => {
    fakeStorage.delete(ERASER_TOOL_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(ERASER_TOOL_HOTKEY, ERASER_TOOL_COMBO);
  });

  it("should set the generic marker hotkey when it's missing", () => {
    fakeStorage.delete(GENERIC_MARKER_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(
      GENERIC_MARKER_HOTKEY,
      GENERIC_MAP_MARKER_COMBO
    );
  });

  it("should set the car marker hotkey when it's missing", () => {
    fakeStorage.delete(CAR_MARKER_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(CAR_MARKER_HOTKEY, CAR_MAP_MARKER_COMBO);
  });

  it("should set the weapon marker hotkey when it's missing", () => {
    fakeStorage.delete(WEAPON_MARKER_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(
      WEAPON_MARKER_HOTKEY,
      WEAPON_MAP_MARKER_COMBO
    );
  });

  it("should set the item marker hotkey when it's missing", () => {
    fakeStorage.delete(ITEM_MARKER_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(ITEM_MARKER_HOTKEY, ITEM_MAP_MARKER_COMBO);
  });

  it("should set the play pause hotkey when it's missing", () => {
    fakeStorage.delete(PLAY_PAUSE_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(PLAY_PAUSE_HOTKEY, PLAY_PAUSE_COMBO);
  });

  it("should set the fast forward hotkey when it's missing", () => {
    fakeStorage.delete(FAST_FORWARD_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(FAST_FORWARD_HOTKEY, FAST_FORWARD_COMBO);
  });

  it("should set the fast backward hotkey when it's missing", () => {
    fakeStorage.delete(FAST_BACKWARD_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(FAST_BACKWARD_HOTKEY, FAST_BACKWARD_COMBO);
  });

  it("should set the undo hotkey when it's missing", () => {
    fakeStorage.delete(UNDO_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(UNDO_HOTKEY, UNDO_COMBO);
  });

  it("should set the redo hotkey when it's missing", () => {
    fakeStorage.delete(REDO_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(REDO_HOTKEY, REDO_COMBO);
  });

  it("should set the focus player hotkey when it's missing", () => {
    fakeStorage.delete(FOCUS_PLAYER_HOTKEY);
    const spy = spyOn(fakeStorage, "set");
    const x = new AppSettingsService(fakeStorage);
    expect(spy).toHaveBeenCalledWith(FOCUS_PLAYER_HOTKEY, FOCUS_PLAYER_COMBO);
  });

  afterEach(() => {
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
