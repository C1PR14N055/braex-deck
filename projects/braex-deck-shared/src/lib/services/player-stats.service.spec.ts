import { async, fakeAsync, TestBed, tick } from "@angular/core/testing";
import * as format from "string-format";

import { PlayerStatsService } from "./player-stats.service";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { GameMode } from "../ds/game/game-mode.enum";
import { PlayerStats } from "../ds/player/player-stats";
import { List } from "immutable";
import { environment } from "../../environments/environment";
import { ServerOperationError } from "../error/server-operation.error";
import { ExtGameModeUtil } from "../utils/ext-game-mode-util";
import { ExtPlayerStatsConverter } from "../converters/player/ext-player-stats.converter";
import { Perspective } from "../ds/game/perspective.enum";
import { LocalPlayerInfoService } from "./local-player-info.service";
import { FakeLocalPlayerInfoService } from "./testing/fake-local-player-info.service";
import { ExtGameMode } from "../ds/external/game/ext-game-mode.enum";
import { ExtPlatformRegion } from "../ds/external/game/ext-platform-region.enum";

describe("PlayerStatsService", () => {
  let service: PlayerStatsService;
  let httpController: HttpTestingController;
  let fakeLocalPlayer: FakeLocalPlayerInfoService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        PlayerStatsService,
        ExtGameModeUtil,
        ExtPlayerStatsConverter,
        {
          provide: LocalPlayerInfoService,
          useClass: FakeLocalPlayerInfoService
        }
      ]
    });
    service = TestBed.get(PlayerStatsService);
    fakeLocalPlayer = TestBed.get(LocalPlayerInfoService);
    httpController = TestBed.get(HttpTestingController);
  }));

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should request the stats for multiple players", fakeAsync(() => {
    const players = [
      "TestPlayer1",
      "TestPlayer2",
      "TestPlayer3",
      "TestPlayer4"
    ];
    let receivedStats: List<PlayerStats>;
    service
      .playerStats(GameMode.DUO, Perspective.FPP, ...players)
      .subscribe(stats => {
        receivedStats = stats;
      });
    tick(200);

    const req = httpController.expectOne(
      environment.apiUrl +
        format(environment.apiEndpoints.multiplePlayerStats, {
          platform: ExtPlatformRegion.STEAM,
          gameMode: ExtGameMode.DUO_FPP
        })
    );
    expect(req.request.body).toBeTruthy();
    expect(req.request.method).toEqual("POST");
    const requestBody = <string[]>req.request.body;
    expect(requestBody.length).toBe(4);
    players.forEach(x => {
      expect(requestBody).toContain(x);
    });
    req.flush([
      {
        season: "division.bro.official.pc-2018-02",
        playerName: "TestPlayer1",
        gameMode: "DUO_FPP",
        stats: {
          avgKills: 0.8209,
          top10Rate: 0.13433,
          avgDmg: 115.49009,
          longestKill: 251.592029999999994060999597422778606414794921875,
          avgSurvivedTime: 326.46099,
          mostKills: 7,
          matches: 134,
          headshotRate: 3.7931,
          kdRate: 0.8209,
          winRate: 0.02985,
          rankPointsTitle: "3-3",
          rankPoints: 2529.8823000000002139131538569927215576171875
        }
      },
      {
        season: "division.bro.official.pc-2018-02",
        playerName: "TestPlayer2",
        gameMode: "DUO_FPP",
        stats: {
          avgKills: 0.52174,
          top10Rate: 0.125,
          avgDmg: 79.90536,
          longestKill: 133.9796000000000049112713895738124847412109375,
          avgSurvivedTime: 394.55125,
          mostKills: 7,
          matches: 184,
          headshotRate: 3.31034,
          kdRate: 0.52174,
          winRate: 0.01087,
          rankPointsTitle: "3-3",
          rankPoints: 2529.8823000000002139131538569927215576171875
        }
      },
      {
        season: "division.bro.official.pc-2018-02",
        playerName: "TestPlayer3",
        gameMode: "DUO_FPP",
        stats: {
          avgKills: 0.99717,
          top10Rate: 0.19263,
          avgDmg: 143.41795,
          longestKill: 213.48259999999999081410351209342479705810546875,
          avgSurvivedTime: 447.9768,
          mostKills: 8,
          matches: 353,
          headshotRate: 3.95506,
          kdRate: 0.99717,
          winRate: 0.01983,
          rankPointsTitle: "3-3",
          rankPoints: 2529.8823000000002139131538569927215576171875
        }
      },
      {
        season: "division.bro.official.pc-2018-02",
        playerName: "TestPlayer4",
        gameMode: "DUO_FPP",
        stats: {
          avgKills: 1.73961,
          top10Rate: 0.07202,
          avgDmg: 207.58385,
          longestKill: 300.9631299999999782812665216624736785888671875,
          avgSurvivedTime: 275.06061,
          mostKills: 16,
          matches: 361,
          headshotRate: 3.97468,
          kdRate: 1.73961,
          winRate: 0.00831,
          rankPointsTitle: "3-3",
          rankPoints: 2529.8823000000002139131538569927215576171875
        }
      }
    ]);
    tick(600);

    expect(receivedStats).toBeTruthy();
    expect(receivedStats.size).toBe(4);
  }));

  it("should emit an error when the server returns HTTP 500", fakeAsync(() => {
    const players = ["TestPlayer1", "TestPlayer2"];
    let e: any;
    service.playerStats(GameMode.SOLO, Perspective.TPP, ...players).subscribe(
      () => {},
      error1 => {
        e = error1;
      }
    );

    const req = httpController.expectOne(
      environment.apiUrl +
        format(environment.apiEndpoints.multiplePlayerStats, {
          platform: ExtPlatformRegion.STEAM,
          gameMode: ExtGameMode.SOLO
        })
    );
    expect(req.request.body).toBeTruthy();
    expect(req.request.method).toEqual("POST");
    req.error(null, { status: 500 });

    tick(600);
    expect(e).toBeTruthy();
    expect(e instanceof ServerOperationError).toBeTruthy();
  }));

  it("should cache the stats of the local player", fakeAsync(() => {
    const localPlayer = "SomeNickname";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(400);

    service
      .playerStats(GameMode.SOLO, Perspective.TPP, localPlayer)
      .subscribe(() => {});

    const req = httpController.expectOne(
      environment.apiUrl +
        format(environment.apiEndpoints.multiplePlayerStats, {
          platform: ExtPlatformRegion.STEAM,
          gameMode: ExtGameMode.SOLO
        })
    );
    req.flush([
      {
        season: "division.bro.official.pc-2018-02",
        playerName: localPlayer,
        gameMode: "SOLO",
        stats: {
          avgKills: 1.73961,
          top10Rate: 0.07202,
          avgDmg: 207.58385,
          longestKill: 300.9631299999999782812665216624736785888671875,
          avgSurvivedTime: 275.06061,
          mostKills: 16,
          matches: 361,
          headshotRate: 3.97468,
          kdRate: 1.73961,
          winRate: 0.00831,
          rankPointsTitle: "3-3",
          rankPoints: 2529.8823000000002139131538569927215576171875
        }
      }
    ]);

    httpController.verify();

    service
      .playerStats(GameMode.SOLO, Perspective.TPP, localPlayer)
      .subscribe(() => {});
    tick(400);

    httpController.expectNone(
      environment.apiUrl +
        format(environment.apiEndpoints.multiplePlayerStats, {
          platform: ExtPlatformRegion.STEAM,
          gameMode: ExtGameMode.SOLO
        })
    );
  }));

  it("should NOT use the cache when the game mode is different than the one in the cache", fakeAsync(() => {
    const localPlayer = "NicknameOfTheLocalPlayer";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(400);

    service
      .playerStats(GameMode.DUO, Perspective.FPP, localPlayer)
      .subscribe(() => {});
    tick(400);

    const req = httpController.expectOne(
      environment.apiUrl +
        format(environment.apiEndpoints.multiplePlayerStats, {
          platform: ExtPlatformRegion.STEAM,
          gameMode: ExtGameMode.DUO_FPP
        })
    );
    req.flush([
      {
        season: "division.bro.official.pc-2018-02",
        playerName: localPlayer,
        gameMode: ExtGameMode.DUO_FPP,
        stats: {
          avgKills: 1.73961,
          top10Rate: 0.07202,
          avgDmg: 207.58385,
          longestKill: 300.9631299999999782812665216624736785888671875,
          avgSurvivedTime: 275.06061,
          mostKills: 16,
          matches: 361,
          headshotRate: 3.97468,
          kdRate: 1.73961,
          winRate: 0.00831,
          rankPointsTitle: "3-3",
          rankPoints: 2529.8823000000002139131538569927215576171875
        }
      }
    ]);
    httpController.verify();

    service
      .playerStats(GameMode.SQUAD, Perspective.TPP, localPlayer)
      .subscribe(() => {});
    tick(400);

    httpController.expectOne(
      environment.apiUrl +
        format(environment.apiEndpoints.multiplePlayerStats, {
          platform: ExtPlatformRegion.STEAM,
          gameMode: ExtGameMode.SQUAD
        })
    );
  }));

  it("should NOT use the cache when the name of the local player changed", fakeAsync(() => {
    let localPlayer = "TheFirstNicknameOfTheLocalPlayer";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(400);

    service
      .playerStats(GameMode.SOLO, Perspective.TPP, localPlayer)
      .subscribe(() => {});

    const req = httpController.expectOne(
      environment.apiUrl +
        format(environment.apiEndpoints.multiplePlayerStats, {
          platform: ExtPlatformRegion.STEAM,
          gameMode: ExtGameMode.SOLO
        })
    );
    req.flush([
      {
        season: "division.bro.official.pc-2018-02",
        playerName: localPlayer,
        gameMode: "SOLO",
        stats: {
          avgKills: 1.73961,
          top10Rate: 0.07202,
          avgDmg: 207.58385,
          longestKill: 300.9631299999999782812665216624736785888671875,
          avgSurvivedTime: 275.06061,
          mostKills: 16,
          matches: 361,
          headshotRate: 3.97468,
          kdRate: 1.73961,
          winRate: 0.00831,
          rankPointsTitle: "3-3",
          rankPoints: 2529.8823000000002139131538569927215576171875
        }
      }
    ]);
    httpController.verify();

    localPlayer = "AnotherNicknameOfTheLocalPlayer";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(400);

    service
      .playerStats(GameMode.SOLO, Perspective.TPP, localPlayer)
      .subscribe(() => {});

    httpController.expectOne(
      environment.apiUrl +
        format(environment.apiEndpoints.multiplePlayerStats, {
          platform: ExtPlatformRegion.STEAM,
          gameMode: ExtGameMode.SOLO
        })
    );
  }));

  it("should net return the players which don't have a season", fakeAsync(() => {
    let localPlayer = "TheFirstNicknameOfTheLocalPlayer";
    fakeLocalPlayer.triggerLocalPlayerNickname(localPlayer);
    tick(400);

    const playerNickname = "SomePlayer";
    let received: List<PlayerStats>;
    service
      .playerStats(GameMode.SOLO, Perspective.TPP, playerNickname)
      .subscribe(x => {
        received = x;
      });

    const req = httpController.expectOne(
      environment.apiUrl +
        format(environment.apiEndpoints.multiplePlayerStats, {
          platform: ExtPlatformRegion.STEAM,
          gameMode: ExtGameMode.SOLO
        })
    );
    req.flush([
      {
        season: null,
        playerName: playerNickname,
        gameMode: null,
        stats: null
      }
    ]);
    httpController.verify();

    expect(received.isEmpty()).toBeTruthy();
  }));
});
