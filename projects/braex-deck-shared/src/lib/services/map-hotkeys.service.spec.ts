import { fakeAsync, TestBed, tick } from "@angular/core/testing";
import { HotkeyService } from "./hotkey.service";
import { FakeHotkeyService } from "./testing/fake-hotkey.service";
import { MapToolService } from "../map-canvas/services/map-tool.service";
import { BrushLinesCanvasModule } from "../map-canvas/canvas-modules/modules/brush-lines.canvas-module";
import { MapCanvasTool } from "../map-canvas/ds/map-canvas-tool.enum";
import { CanvasEngine } from "../map-canvas/core/canvas-engine";
import { OverwolfGameEventsService } from "../overwolf/overwolf-game-events.service";
import { FakeOverwolfGameEventsService } from "../overwolf/testing/fake-overwolf-game-events.service";
import { CanvasModuleRegistryFactory } from "../map-canvas/canvas-modules/canvas-module-registry-factory";
import { BrushCanvasModule } from "../map-canvas/canvas-modules/modules/brush.canvas-module";
import { Vector3 } from "../map-canvas/ds/vector3";
import { InGameUtils } from "../map-canvas/utils/in-game-utils";
import { PubgMap } from "../ds/game/pubg-map.enum";
import { MapBackgroundCanvasModule } from "../map-canvas/canvas-modules/modules/map-background.canvas-module";
import { Vector2 } from "../map-canvas/ds/vector2";
import { MarkerConfigService } from "../map-canvas/services/marker-config.service";
import { MapMarkerType } from "../map-canvas/ds/map-marker-type.enum";
import { MapHotkeysService } from "./map-hotkeys.service";
import {
  AppSettingsService,
  BRUSH_TOOL_COMBO,
  CAR_MAP_MARKER_COMBO,
  FOCUS_PLAYER_COMBO,
  ERASER_TOOL_COMBO,
  GENERIC_MAP_MARKER_COMBO,
  ITEM_MAP_MARKER_COMBO,
  MAP_MARKER_TOOL_COMBO,
  REDO_COMBO,
  UNDO_COMBO,
  WEAPON_MAP_MARKER_COMBO
} from "./app-settings.service";
import { FakeAppSettingsService } from "./testing/fake-app-settings.service";

describe("MapHotkeysService", () => {
  let service: MapHotkeysService;
  let fakeHotkey: FakeHotkeyService;
  let mapTool: MapToolService;
  let brushLines: BrushLinesCanvasModule;
  let fakeGameEvents: FakeOverwolfGameEventsService;
  let canvasEngine: CanvasEngine;
  let fakeMapBg: any;
  let markerConfig: MarkerConfigService;
  let appSettings: FakeAppSettingsService;

  beforeEach(() => {
    fakeMapBg = {
      mapSize: Vector2.create(900, 900),
      currentMap: PubgMap.DIHOROTOK_MAIN
    };
    TestBed.configureTestingModule({
      providers: [
        MapHotkeysService,
        { provide: HotkeyService, useClass: FakeHotkeyService },
        MapToolService,
        BrushLinesCanvasModule,
        CanvasEngine,
        {
          provide: OverwolfGameEventsService,
          useClass: FakeOverwolfGameEventsService
        },
        BrushCanvasModule,
        { provide: MapBackgroundCanvasModule, useValue: fakeMapBg },
        MarkerConfigService,
        { provide: AppSettingsService, useClass: FakeAppSettingsService }
      ]
    });

    service = TestBed.get(MapHotkeysService);
    fakeHotkey = TestBed.get(HotkeyService);
    mapTool = TestBed.get(MapToolService);
    brushLines = TestBed.get(BrushLinesCanvasModule);
    fakeGameEvents = TestBed.get(OverwolfGameEventsService);
    markerConfig = TestBed.get(MarkerConfigService);
    appSettings = TestBed.get(AppSettingsService);
    canvasEngine = TestBed.get(CanvasEngine);

    const engineHolder = document.createElement("div");
    engineHolder.style.width = "100px";
    engineHolder.style.height = "100px";
    canvasEngine.attach(engineHolder);

    const moduleRegistry = CanvasModuleRegistryFactory.create();
    moduleRegistry.registerModules(BrushCanvasModule, BrushLinesCanvasModule);
    canvasEngine.start();
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should register the undo hotkey", () => {
    expect(fakeHotkey.hasHotkey(UNDO_COMBO)).toBeTruthy();
  });

  it("should register the redo hotkey", () => {
    expect(fakeHotkey.hasHotkey(REDO_COMBO)).toBeTruthy();
  });

  it("should register the brush tool hotkey", () => {
    expect(fakeHotkey.hasHotkey(BRUSH_TOOL_COMBO)).toBeTruthy();
  });

  it("should register the map marker tool hotkey", () => {
    expect(fakeHotkey.hasHotkey(MAP_MARKER_TOOL_COMBO)).toBeTruthy();
  });

  it("should register the center on player hotkey", () => {
    expect(fakeHotkey.hasHotkey(FOCUS_PLAYER_COMBO)).toBeTruthy();
  });

  it("should register the default map marker hotkey", () => {
    expect(fakeHotkey.hasHotkey(GENERIC_MAP_MARKER_COMBO)).toBeTruthy();
  });

  it("should register the car map marker hotkey", () => {
    expect(fakeHotkey.hasHotkey(CAR_MAP_MARKER_COMBO)).toBeTruthy();
  });

  it("should register the weapon marker hotkey", () => {
    expect(fakeHotkey.hasHotkey(WEAPON_MAP_MARKER_COMBO)).toBeTruthy();
  });

  it("should register the item marker hotkey", () => {
    expect(fakeHotkey.hasHotkey(ITEM_MAP_MARKER_COMBO)).toBeTruthy();
  });

  it("should register the eraser tool hotkey", () => {
    expect(fakeHotkey.hasHotkey(ERASER_TOOL_COMBO)).toBeTruthy();
  });

  it("should trigger the undo on the brush lines module", () => {
    const spy = spyOn(brushLines, "undo");
    fakeHotkey.triggerHotkey(UNDO_COMBO);
    expect(spy).toHaveBeenCalled();
  });

  it("should trigger the redo on the brush lines module", () => {
    const spy = spyOn(brushLines, "redo");
    fakeHotkey.triggerHotkey(REDO_COMBO);
    expect(spy).toHaveBeenCalled();
  });

  it("should set the active tool to the brush tool", () => {
    mapTool.activeTool = MapCanvasTool.SELECTION;
    fakeHotkey.triggerHotkey(BRUSH_TOOL_COMBO);
    expect(mapTool.activeTool).toEqual(MapCanvasTool.BRUSH);
  });

  it("should set the active tool to the map marker tool", () => {
    mapTool.activeTool = MapCanvasTool.SELECTION;
    fakeHotkey.triggerHotkey(MAP_MARKER_TOOL_COMBO);
    expect(mapTool.activeTool).toEqual(MapCanvasTool.MARKER);
  });

  it("should center on the player", fakeAsync(() => {
    const playerLocation = Vector3.create(1234, 2342, 2111);
    const expected = InGameUtils.overwolfMapLocationToCanvasSpace(
      fakeMapBg.currentMap,
      fakeMapBg.mapSize,
      playerLocation
    );
    const spy = spyOn(canvasEngine.camera, "focusPosition").and.callFake(
      position => {
        expect(position.x).toBeCloseTo(expected.x);
        expect(position.y).toBeCloseTo(expected.y);
      }
    );
    fakeGameEvents.triggerLocalPlayerLocationChanged(playerLocation);
    tick(200);

    fakeHotkey.triggerHotkey(FOCUS_PLAYER_COMBO);
    expect(spy).toHaveBeenCalled();
  }));

  it("should set the map marker type to default", () => {
    fakeHotkey.triggerHotkey(GENERIC_MAP_MARKER_COMBO);
    expect(markerConfig.type).toEqual(MapMarkerType.DEFAULT);
  });

  it("should set the map marker type to car", () => {
    fakeHotkey.triggerHotkey(CAR_MAP_MARKER_COMBO);
    expect(markerConfig.type).toEqual(MapMarkerType.CAR);
  });

  it("should set the map marker type to weapon", () => {
    fakeHotkey.triggerHotkey(WEAPON_MAP_MARKER_COMBO);
    expect(markerConfig.type).toEqual(MapMarkerType.WEAPON);
  });

  it("should set the map marker type to item", () => {
    fakeHotkey.triggerHotkey(ITEM_MAP_MARKER_COMBO);
    expect(markerConfig.type).toEqual(MapMarkerType.ITEM);
  });

  it("should set the active tool to the eraser", () => {
    fakeHotkey.triggerHotkey(ERASER_TOOL_COMBO);
    expect(mapTool.activeTool).toEqual(MapCanvasTool.ERASER);
  });

  it("should change the hotkey of the brush tool", fakeAsync(() => {
    const unregisterSpy = spyOn(fakeHotkey, "unregisterHotkey");
    let receivedHotkey: string;
    spyOn(fakeHotkey, "registerHotkey").and.callFake(x => {
      receivedHotkey = x;
    });
    const hotkey = "a+b";
    appSettings.triggerBrushToolHotkeyChanged(hotkey);
    tick(200);

    expect(unregisterSpy).toHaveBeenCalledWith(BRUSH_TOOL_COMBO);
    expect(receivedHotkey).toEqual(hotkey);
  }));

  it("should change the hotkey of the map marker tool", fakeAsync(() => {
    const unregisterSpy = spyOn(fakeHotkey, "unregisterHotkey");
    let receivedHotkey: string;
    spyOn(fakeHotkey, "registerHotkey").and.callFake(x => {
      receivedHotkey = x;
    });
    const hotkey = "p+e+t";
    appSettings.triggerMapMarkerToolHotkeyChanged(hotkey);
    tick(200);

    expect(unregisterSpy).toHaveBeenCalledWith(MAP_MARKER_TOOL_COMBO);
    expect(receivedHotkey).toEqual(hotkey);
  }));

  it("should change the hotkey of the eraser tool", fakeAsync(() => {
    const unregisterSpy = spyOn(fakeHotkey, "unregisterHotkey");
    let receivedHotkey: string;
    spyOn(fakeHotkey, "registerHotkey").and.callFake(x => {
      receivedHotkey = x;
    });
    const hotkey = "8+9+j";
    appSettings.triggerEraserToolHotkeyChanged(hotkey);
    tick(200);

    expect(unregisterSpy).toHaveBeenCalledWith(ERASER_TOOL_COMBO);
    expect(receivedHotkey).toEqual(hotkey);
  }));

  it("should change the hotkey of the generic marker", fakeAsync(() => {
    const unregisterSpy = spyOn(fakeHotkey, "unregisterHotkey");
    let receivedHotkey: string;
    spyOn(fakeHotkey, "registerHotkey").and.callFake(x => {
      receivedHotkey = x;
    });
    const hotkey = "k+f+n";
    appSettings.triggerGenericMarkerHotkeyChanged(hotkey);
    tick(200);

    expect(unregisterSpy).toHaveBeenCalledWith(GENERIC_MAP_MARKER_COMBO);
    expect(receivedHotkey).toEqual(hotkey);
  }));

  it("should change the hotkey of the car marker", fakeAsync(() => {
    const unregisterSpy = spyOn(fakeHotkey, "unregisterHotkey");
    let receivedHotkey: string;
    spyOn(fakeHotkey, "registerHotkey").and.callFake(x => {
      receivedHotkey = x;
    });
    const hotkey = "v+a+x";
    appSettings.triggerCarMarkerHotkeyChanged(hotkey);
    tick(200);

    expect(unregisterSpy).toHaveBeenCalledWith(CAR_MAP_MARKER_COMBO);
    expect(receivedHotkey).toEqual(hotkey);
  }));

  it("should change the hotkey of the weapon marker", fakeAsync(() => {
    const unregisterSpy = spyOn(fakeHotkey, "unregisterHotkey");
    let receivedHotkey: string;
    spyOn(fakeHotkey, "registerHotkey").and.callFake(x => {
      receivedHotkey = x;
    });
    const hotkey = "t+r+u+m+p";
    appSettings.triggerWeaponMarkerHotkeyChanged(hotkey);
    tick(200);

    expect(unregisterSpy).toHaveBeenCalledWith(WEAPON_MAP_MARKER_COMBO);
    expect(receivedHotkey).toEqual(hotkey);
  }));

  it("should change the hotkey of the item marker", fakeAsync(() => {
    const unregisterSpy = spyOn(fakeHotkey, "unregisterHotkey");
    let receivedHotkey: string;
    spyOn(fakeHotkey, "registerHotkey").and.callFake(x => {
      receivedHotkey = x;
    });
    const hotkey = "y+z+l";
    appSettings.triggerItemMarkerHotkeyChanged(hotkey);
    tick(200);

    expect(unregisterSpy).toHaveBeenCalledWith(ITEM_MAP_MARKER_COMBO);
    expect(receivedHotkey).toEqual(hotkey);
  }));
});
