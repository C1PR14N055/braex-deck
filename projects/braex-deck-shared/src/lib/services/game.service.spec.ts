import { fakeAsync, TestBed, tick } from "@angular/core/testing";

import { GameService } from "./game.service";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { environment } from "../../environments/environment";
import * as format from "string-format";
import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE,
  PagingOptions
} from "../ds/paging-options";
import { ExtLazyGame } from "../ds/external/game/ext-lazy-game";
import { ExtPUBGMap } from "../ds/external/game/ext-pubg-map.enum";
import { ExtGameMode } from "../ds/external/game/ext-game-mode.enum";
import { ExtPlatformRegion } from "../ds/external/game/ext-platform-region.enum";
import { LazyGame } from "../ds/game/lazy-game";

describe("GameService", () => {
  let service: GameService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GameService]
    });

    service = TestBed.get(GameService);
    httpController = TestBed.get(HttpTestingController);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should search for the matches of the player", () => {
    const playerName = "TestPlayer";
    service.getGamesOfPlayer(playerName).subscribe(() => {});

    const req = httpController.expectOne(r =>
      r.url.startsWith(
        environment.apiUrl +
          format(environment.apiEndpoints.searchPlayer, {
            nickname: playerName
          })
      )
    );
    expect(req.request.method).toEqual("GET");
  });

  it("should set the paging options correctly when they're missing when searching a player", () => {
    const playerName = "SomePlayerNickname";
    service.getGamesOfPlayer(playerName).subscribe(() => {});

    const req = httpController.expectOne(r =>
      r.url.startsWith(
        environment.apiUrl +
          format(environment.apiEndpoints.searchPlayer, {
            nickname: playerName
          })
      )
    );
    expect(req.request.params.has("page")).toBeTruthy();
    expect(req.request.params.has("size")).toBeTruthy();
    expect(Number(req.request.params.get("page"))).toEqual(DEFAULT_PAGE_NUMBER);
    expect(Number(req.request.params.get("size"))).toEqual(DEFAULT_PAGE_SIZE);
  });

  it("should set the paging options when they're available when searching a player", () => {
    const playerName = "Nickname";
    const pagingOptions: PagingOptions = {
      page: 12,
      size: 100
    };
    service.getGamesOfPlayer(playerName, pagingOptions).subscribe(() => {});

    const req = httpController.expectOne(r =>
      r.url.startsWith(
        environment.apiUrl +
          format(environment.apiEndpoints.searchPlayer, {
            nickname: playerName
          })
      )
    );
    expect(req.request.params.has("page")).toBeTruthy();
    expect(req.request.params.has("size")).toBeTruthy();
    expect(Number(req.request.params.get("page"))).toEqual(pagingOptions.page);
    expect(Number(req.request.params.get("size"))).toEqual(pagingOptions.size);
  });

  it("should get the game by it's id", () => {
    const gameId = "c8d9a700-d9a8-48ad-8af6-c8d1ef50eb43";
    service.getGameById(gameId).subscribe(() => {});

    const req = httpController.expectOne(
      environment.apiUrl +
        format(environment.apiEndpoints.anyGameById, { gameId: gameId })
    );
    expect(req.request.method).toEqual("GET");
  });

  it("should use the cached game instead of calling the service", fakeAsync(() => {
    const gameId = "c8d9a700-d9a8-48ad-8af6-c8d1ef50eb43";
    let receivedGame: LazyGame;
    service.getGameById(gameId).subscribe(g => {
      receivedGame = g;
    });

    const req = httpController.expectOne(
      environment.apiUrl +
        format(environment.apiEndpoints.anyGameById, { gameId: gameId })
    );
    req.flush({
      id: 12,
      mapName: ExtPUBGMap.RANGE_MAIN,
      gameMode: ExtGameMode.DUO,
      created: 1552775296,
      updated: 1552775296,
      createdAt: 1552775296,
      duration: 979,
      isCustomMatch: true,
      matchId: gameId,
      patchVersion: null,
      seasonState: "PROGRESS",
      shardId: ExtPlatformRegion.STEAM,
      titleId: "bluehole-pubg",
      type: null
    } as ExtLazyGame);
    tick(500);
    httpController.verify();
    expect(receivedGame).toBeTruthy();
    expect(receivedGame.gameId).toEqual(gameId);

    receivedGame = null;
    service.getGameById(gameId).subscribe(g => {
      receivedGame = g;
    });
    httpController.expectNone(
      environment.apiUrl +
        format(environment.apiEndpoints.anyGameById, { gameId: gameId })
    );
    tick(500);
    expect(receivedGame.gameId).toEqual(gameId);
  }));

  it("should get the rosters of a game", () => {
    const gameId = "dab868a8-18a1-4826-9933-dd777785144e";
    service.getGameRosters(gameId).subscribe(() => {});

    const req = httpController.expectOne(
      environment.apiUrl +
        format(environment.apiEndpoints.gameRoster, { gameId: gameId })
    );
    expect(req.request.method).toEqual("GET");
  });

  it("should cache the rosters", () => {
    const gameId = "dab868a8-18a1-4826-9933-dd777785144e";
    service.getGameRosters(gameId).subscribe(() => {});

    const req = httpController.expectOne(
      environment.apiUrl +
        format(environment.apiEndpoints.gameRoster, { gameId: gameId })
    );
    req.flush([
      {
        id: 14436984,
        created: 1557337753924,
        updated: 1557337753924,
        type: null,
        shardId: "steam",
        rank: 25,
        teamId: 18,
        won: false,
        participants: [
          {
            id: 25710219,
            created: 1557337753934,
            updated: 1557337753934,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 0,
            assists: 1,
            boosts: 1,
            damageDealt: 23.099998500000001655507730902172625064849853515625,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 0,
            killPlace: 94,
            killStreaks: 0,
            kills: 0,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 0,
            mostDamage: 0,
            name: "ZashkvarniyPan",
            playerId: "account.f364789f0a1c41d98b394aab1bdec0b6",
            rankPoints: 0,
            revives: 0,
            rideDistance: 0,
            roadKills: 0,
            swimDistance: 0,
            teamKills: 0,
            timeSurvived: 222.022999999999996134647517465054988861083984375,
            vehicleDestroys: 0,
            walkDistance: 123.778876999999994268364389427006244659423828125,
            weaponsAcquired: 2,
            winPlace: 28
          },
          {
            id: 25710218,
            created: 1557337753931,
            updated: 1557337753931,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 1,
            assists: 0,
            boosts: 0,
            damageDealt: 100.1199950000000029604052542708814144134521484375,
            deathType: "byplayer",
            headshotKills: 1,
            heals: 0,
            killPlace: 37,
            killStreaks: 1,
            kills: 1,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 4.948199269999999927449607639573514461517333984375,
            mostDamage: 0,
            name: "Hayvandao",
            playerId: "account.5f8e1bb52bd644b1bbc8e439003f0002",
            rankPoints: 0,
            revives: 0,
            rideDistance: 0,
            roadKills: 0,
            swimDistance: 0,
            teamKills: 0,
            timeSurvived: 312.02699999999998681232682429254055023193359375,
            vehicleDestroys: 0,
            walkDistance: 214.908156999999988556737662293016910552978515625,
            weaponsAcquired: 3,
            winPlace: 25
          },
          {
            id: 25710217,
            created: 1557337753928,
            updated: 1557337753928,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 5,
            assists: 0,
            boosts: 0,
            damageDealt: 287.3999999999999772626324556767940521240234375,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 0,
            killPlace: 21,
            killStreaks: 1,
            kills: 2,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 6.36370658999999960769855533726513385772705078125,
            mostDamage: 0,
            name: "ibrahimsezg",
            playerId: "account.20861055d37c4d6db66d8c659f45ee98",
            rankPoints: 0,
            revives: 0,
            rideDistance: 0,
            roadKills: 0,
            swimDistance: 0,
            teamKills: 0,
            timeSurvived: 312.03100000000000591171556152403354644775390625,
            vehicleDestroys: 0,
            walkDistance: 304.44409999999999172359821386635303497314453125,
            weaponsAcquired: 3,
            winPlace: 25
          },
          {
            id: 25710216,
            created: 1557337753925,
            updated: 1557337753925,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 0,
            assists: 0,
            boosts: 0,
            damageDealt: 120.3999940000000066220309236086905002593994140625,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 0,
            killPlace: 81,
            killStreaks: 0,
            kills: 0,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 0,
            mostDamage: 0,
            name: "TheroxDaranian",
            playerId: "account.926080397bdd4ca59e6102d90e6b7fab",
            rankPoints: 0,
            revives: 0,
            rideDistance: 0,
            roadKills: 0,
            swimDistance: 0,
            teamKills: 0,
            timeSurvived: 312.02199999999999135980033315718173980712890625,
            vehicleDestroys: 0,
            walkDistance: 217.33323699999999689680407755076885223388671875,
            weaponsAcquired: 5,
            winPlace: 25
          }
        ]
      },
      {
        id: 14436983,
        created: 1557337753908,
        updated: 1557337753908,
        type: null,
        shardId: "steam",
        rank: 16,
        teamId: 2,
        won: false,
        participants: [
          {
            id: 25710215,
            created: 1557337753921,
            updated: 1557337753921,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 0,
            assists: 1,
            boosts: 1,
            damageDealt: 28.547565500000001037506081047467887401580810546875,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 2,
            killPlace: 79,
            killStreaks: 0,
            kills: 0,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 0,
            mostDamage: 0,
            name: "Silveyz",
            playerId: "account.7cb8f88871704ca686f0c4175d279951",
            rankPoints: 0,
            revives: 1,
            rideDistance: 0,
            roadKills: 0,
            swimDistance: 0,
            teamKills: 0,
            timeSurvived: 303.32400000000001227817847393453121185302734375,
            vehicleDestroys: 0,
            walkDistance: 289.39361600000000862564775161445140838623046875,
            weaponsAcquired: 3,
            winPlace: 24
          },
          {
            id: 25710214,
            created: 1557337753919,
            updated: 1557337753919,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 1,
            assists: 0,
            boosts: 0,
            damageDealt: 126.25,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 0,
            killPlace: 38,
            killStreaks: 1,
            kills: 1,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 10.65297220000000066875145421363413333892822265625,
            mostDamage: 0,
            name: "hrenovo13",
            playerId: "account.85aa4ba33b3e4e22969e35d935175551",
            rankPoints: 0,
            revives: 0,
            rideDistance: 0,
            roadKills: 0,
            swimDistance: 0,
            teamKills: 0,
            timeSurvived: 226.1920000000000072759576141834259033203125,
            vehicleDestroys: 0,
            walkDistance: 76.762280000000004065441316924989223480224609375,
            weaponsAcquired: 2,
            winPlace: 27
          },
          {
            id: 25710213,
            created: 1557337753916,
            updated: 1557337753916,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 0,
            assists: 0,
            boosts: 0,
            damageDealt: 26.041183499999998929297362337820231914520263671875,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 2,
            killPlace: 83,
            killStreaks: 0,
            kills: 0,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 0,
            mostDamage: 0,
            name: "Dverg",
            playerId: "account.8f9969a8647a4ec585d053d0334db238",
            rankPoints: 0,
            revives: 1,
            rideDistance: 0,
            roadKills: 0,
            swimDistance: 0,
            teamKills: 0,
            timeSurvived: 276.8229999999999790816218592226505279541015625,
            vehicleDestroys: 0,
            walkDistance: 81.44787999999999783540260978043079376220703125,
            weaponsAcquired: 4,
            winPlace: 25
          },
          {
            id: 25710212,
            created: 1557337753911,
            updated: 1557337753911,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 2,
            assists: 0,
            boosts: 2,
            damageDealt: 219.161255000000011250449460931122303009033203125,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 1,
            killPlace: 10,
            killStreaks: 3,
            kills: 3,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 3.98367762999999985851218298193998634815216064453125,
            mostDamage: 0,
            name: "Sommewhere",
            playerId: "account.2c50cc6c072246959e7be280192dac94",
            rankPoints: 0,
            revives: 1,
            rideDistance: 0,
            roadKills: 0,
            swimDistance: 0,
            teamKills: 0,
            timeSurvived: 767.892000000000052750692702829837799072265625,
            vehicleDestroys: 0,
            walkDistance: 1648.30736999999999170540831983089447021484375,
            weaponsAcquired: 6,
            winPlace: 16
          }
        ]
      },
      {
        id: 14436982,
        created: 1557337753894,
        updated: 1557337753894,
        type: null,
        shardId: "steam",
        rank: 5,
        teamId: 23,
        won: false,
        participants: [
          {
            id: 25710211,
            created: 1557337753905,
            updated: 1557337753905,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 3,
            assists: 0,
            boosts: 2,
            damageDealt: 293.6043700000000171712599694728851318359375,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 1,
            killPlace: 8,
            killStreaks: 1,
            kills: 3,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 32.39856999999999942474460112862288951873779296875,
            mostDamage: 0,
            name: "NikkoHel",
            playerId: "account.72a36c94ff5846cd9923be6b9a64b6ba",
            rankPoints: 0,
            revives: 0,
            rideDistance: 3567.865719999999782885424792766571044921875,
            roadKills: 0,
            swimDistance: 0,
            teamKills: 0,
            timeSurvived: 1466.999000000000023646862246096134185791015625,
            vehicleDestroys: 0,
            walkDistance: 2127.72499999999990905052982270717620849609375,
            weaponsAcquired: 9,
            winPlace: 5
          },
          {
            id: 25710210,
            created: 1557337753901,
            updated: 1557337753901,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 1,
            assists: 0,
            boosts: 5,
            damageDealt: 98,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 2,
            killPlace: 43,
            killStreaks: 0,
            kills: 0,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 0,
            mostDamage: 0,
            name: "Tozoo35",
            playerId: "account.9b331aeb30044c84a13a198e0caa7fa9",
            rankPoints: 0,
            revives: 1,
            rideDistance: 2670.63916000000017447746358811855316162109375,
            roadKills: 0,
            swimDistance: 0,
            teamKills: 0,
            timeSurvived: 1526.406999999999925421434454619884490966796875,
            vehicleDestroys: 0,
            walkDistance: 3173.81323000000020329025574028491973876953125,
            weaponsAcquired: 3,
            winPlace: 5
          },
          {
            id: 25710209,
            created: 1557337753898,
            updated: 1557337753898,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 0,
            assists: 0,
            boosts: 4,
            damageDealt: 0,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 2,
            killPlace: 42,
            killStreaks: 0,
            kills: 0,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 0,
            mostDamage: 0,
            name: "Sansar_TR1",
            playerId: "account.45b81ffcb50e4101b2266b494679f67e",
            rankPoints: 0,
            revives: 1,
            rideDistance: 3366.063720000000103027559816837310791015625,
            roadKills: 0,
            swimDistance: 0,
            teamKills: 0,
            timeSurvived: 1526.40900000000010550138540565967559814453125,
            vehicleDestroys: 0,
            walkDistance: 3075.0915500000000974978320300579071044921875,
            weaponsAcquired: 7,
            winPlace: 5
          },
          {
            id: 25710208,
            created: 1557337753895,
            updated: 1557337753895,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 1,
            assists: 0,
            boosts: 3,
            damageDealt: 0,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 2,
            killPlace: 44,
            killStreaks: 0,
            kills: 0,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 0,
            mostDamage: 0,
            name: "W1nD---",
            playerId: "account.bdb3dee07e984aed88d35dd9062a4f0d",
            rankPoints: 0,
            revives: 0,
            rideDistance: 2729.8044399999998859129846096038818359375,
            roadKills: 0,
            swimDistance: 0,
            teamKills: 0,
            timeSurvived: 1391.74299999999993815436027944087982177734375,
            vehicleDestroys: 0,
            walkDistance: 2842.91040000000020881998352706432342529296875,
            weaponsAcquired: 5,
            winPlace: 5
          }
        ]
      },
      {
        id: 14436981,
        created: 1557337753883,
        updated: 1557337753883,
        type: null,
        shardId: "steam",
        rank: 15,
        teamId: 24,
        won: false,
        participants: [
          {
            id: 25710207,
            created: 1557337753891,
            updated: 1557337753891,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 0,
            assists: 0,
            boosts: 4,
            damageDealt: 69.090000000000003410605131648480892181396484375,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 5,
            killPlace: 58,
            killStreaks: 0,
            kills: 0,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 0,
            mostDamage: 0,
            name: "GreekCaptain21",
            playerId: "account.3e38cf82db204b90bbcc189ddbb3a738",
            rankPoints: 0,
            revives: 1,
            rideDistance: 4538.5366199999998571001924574375152587890625,
            roadKills: 0,
            swimDistance: 55.8852499999999992041921359486877918243408203125,
            teamKills: 0,
            timeSurvived: 1036.386999999999943611328490078449249267578125,
            vehicleDestroys: 0,
            walkDistance: 1829.3131100000000515137799084186553955078125,
            weaponsAcquired: 4,
            winPlace: 15
          },
          {
            id: 25710206,
            created: 1557337753888,
            updated: 1557337753888,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 0,
            assists: 0,
            boosts: 2,
            damageDealt: 25.800000000000000710542735760100185871124267578125,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 1,
            killPlace: 56,
            killStreaks: 0,
            kills: 0,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 0,
            mostDamage: 0,
            name: "BDRJellYToT",
            playerId: "account.6b7893d1aabc48c6a2a302f90aebcdac",
            rankPoints: 0,
            revives: 0,
            rideDistance: 4139.1796899999999368446879088878631591796875,
            roadKills: 0,
            swimDistance: 29.311084699999998548491930705495178699493408203125,
            teamKills: 0,
            timeSurvived: 1039.96599999999989449861459434032440185546875,
            vehicleDestroys: 0,
            walkDistance: 2019.537229999999908613972365856170654296875,
            weaponsAcquired: 4,
            winPlace: 15
          },
          {
            id: 25710205,
            created: 1557337753884,
            updated: 1557337753884,
            type: null,
            actor: "",
            shardId: "steam",
            dbnos: 0,
            assists: 0,
            boosts: 2,
            damageDealt: 88.8357199999999949113771435804665088653564453125,
            deathType: "byplayer",
            headshotKills: 0,
            heals: 0,
            killPlace: 57,
            killStreaks: 0,
            kills: 0,
            lastKillPoints: 0,
            lastWinPoints: 0,
            longestKill: 0,
            mostDamage: 0,
            name: "Fluffy_Fl",
            playerId: "account.c6c3789527e442ef8a120384166fb24d",
            rankPoints: 0,
            revives: 0,
            rideDistance: 3793.84700000000020736479200422763824462890625,
            roadKills: 0,
            swimDistance: 38.88199999999999789679350215010344982147216796875,
            teamKills: 0,
            timeSurvived: 1039.9639999999999417923390865325927734375,
            vehicleDestroys: 0,
            walkDistance: 1908.89075000000002546585164964199066162109375,
            weaponsAcquired: 5,
            winPlace: 15
          }
        ]
      }
    ]);
    httpController.verify();

    service.getGameRosters(gameId).subscribe(() => {});
    httpController.expectNone(
      environment.apiUrl +
        format(environment.apiEndpoints.gameRoster, { gameId: gameId })
    );
  });
});
