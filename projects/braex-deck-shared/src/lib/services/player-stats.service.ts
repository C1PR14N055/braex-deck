import { Injectable, OnDestroy } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, Subscription } from "rxjs";
import { PlayerStats } from "../ds/player/player-stats";
import { ExtPlayerStatsWrapper } from "../ds/external/ext-player-stats-wrapper";
import { environment } from "../../environments/environment";
import * as format from "string-format";
import { GameMode } from "../ds/game/game-mode.enum";
import { ExtPlayerStatsConverter } from "../converters/player/ext-player-stats.converter";
import { ServerOperationError } from "../error/server-operation.error";
import { List } from "immutable";
import { Perspective } from "../ds/game/perspective.enum";
import { ExtGameModeUtil } from "../utils/ext-game-mode-util";
import { LocalPlayerInfoService } from "./local-player-info.service";
import { PubgGameMode } from "../ds/game/pubg-game-mode.enum";
import { ExtGameModeConverter } from "../converters/game/ext-game-mode.converter";

@Injectable({
  providedIn: "root"
})
export class PlayerStatsService implements OnDestroy {
  private _localPlayerStats = new Map<PubgGameMode, PlayerStats>();
  private _localPlayerNickname: string | null = null;
  private _subs: Subscription[] = [];

  constructor(
    private _http: HttpClient,
    private _extPlayerStatsConverter: ExtPlayerStatsConverter,
    private _extGameModeUtil: ExtGameModeUtil,
    private _localPlayerInfo: LocalPlayerInfoService,
    private _extGameModeConverter: ExtGameModeConverter
  ) {
    const sub = this._localPlayerInfo
      .localPlayerNickname()
      .subscribe(nickname => {
        if (
          this._localPlayerNickname &&
          this._localPlayerNickname !== nickname
        ) {
          this._localPlayerStats.clear();
        }

        this._localPlayerNickname = nickname;
      });
    this._subs.push(sub);
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  playerStats(
    gameMode: GameMode,
    perspective: Perspective,
    ...players: string[]
  ): Observable<List<PlayerStats>> {
    return Observable.create(observer => {
      let resultingStats = List<PlayerStats>();
      const playersToRequest: string[] = [];
      const extGameMode = this._extGameModeUtil.buildExtGameMode(
        gameMode,
        perspective
      );
      const pubgGameMode = this._extGameModeConverter.convert(extGameMode);

      if (players.includes(this._localPlayerNickname)) {
        const cachedStats = this._getCachedLocalPlayerStats(pubgGameMode);
        if (cachedStats) {
          resultingStats = resultingStats.push(cachedStats);
          playersToRequest.push(
            ...players.filter(x => x !== this._localPlayerNickname)
          );

          if (!playersToRequest.length) {
            observer.next(resultingStats);
            observer.complete();

            return;
          }
        }
      }

      if (!playersToRequest.length) {
        playersToRequest.push(...players);
      }

      this._http
        .post<ExtPlayerStatsWrapper[]>(
          environment.apiUrl +
            format(environment.apiEndpoints.multiplePlayerStats, {
              platform: "STEAM",
              gameMode: extGameMode
            }),
          playersToRequest
        )
        .subscribe(
          playerStats => {
            const stats = playerStats.filter(x => x.season);
            if (stats.length) {
              resultingStats = resultingStats.push(
                ...stats.map(x => this._extPlayerStatsConverter.convert(x))
              );
              if (playersToRequest.includes(this._localPlayerNickname)) {
                this._cacheLocalPlayerStats(pubgGameMode, resultingStats);
              }

              observer.next(resultingStats);
            } else {
              observer.next(List());
            }

            observer.complete();
          },
          error1 => {
            if (error1.status === 500) {
              observer.error(
                new ServerOperationError(
                  `Failed to fetch the stats for the players \`${JSON.stringify(
                    players
                  )}\` - gameMode \`${gameMode}\``
                )
              );
            }
          }
        );
    });
  }

  private _getCachedLocalPlayerStats(
    pubgGameMode: PubgGameMode
  ): PlayerStats | null {
    if (this._localPlayerStats.has(pubgGameMode)) {
      return this._localPlayerStats.get(pubgGameMode);
    }

    return null;
  }

  private _cacheLocalPlayerStats(
    pubgGameMode: PubgGameMode,
    stats: List<PlayerStats>
  ) {
    const localPlayerStats = stats.filter(
      x => x.nickname === this._localPlayerNickname
    );
    if (!localPlayerStats.isEmpty()) {
      this._localPlayerStats.set(pubgGameMode, localPlayerStats.get(0));
    }
  }
}
