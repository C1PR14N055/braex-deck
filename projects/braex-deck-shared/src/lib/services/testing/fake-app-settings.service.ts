import { Observable, Observer } from "rxjs";
import { GameMode } from "../../ds/game/game-mode.enum";
import { Perspective } from "../../ds/game/perspective.enum";
import {
  BRUSH_TOOL_COMBO,
  CAR_MAP_MARKER_COMBO,
  ERASER_TOOL_COMBO,
  GENERIC_MAP_MARKER_COMBO,
  ITEM_MAP_MARKER_COMBO,
  MAP_MARKER_TOOL_COMBO,
  WEAPON_MAP_MARKER_COMBO
} from "../app-settings.service";

export class FakeAppSettingsService {
  private _lastPlayedGameMode: GameMode | null = null;
  private _defaultSearchGameMode: GameMode | null = null;
  private _preferredModeType: Perspective | null = null;
  private _localPlayerNickname: string | null = null;
  private _brushToolHotkey = BRUSH_TOOL_COMBO;
  private _mapMarkerToolHotkey = MAP_MARKER_TOOL_COMBO;
  private _eraserToolHotkey = ERASER_TOOL_COMBO;
  private _genericMarkerHotkey = GENERIC_MAP_MARKER_COMBO;
  private _carMarkerHotkey = CAR_MAP_MARKER_COMBO;
  private _weaponMarkerHotkey = WEAPON_MAP_MARKER_COMBO;
  private _itemMarkerHotkey = ITEM_MAP_MARKER_COMBO;
  private _lastPlayedModeObservers: Observer<GameMode>[] = [];
  private _defaultSearchModeObservers: Observer<GameMode>[] = [];
  private _preferredModeTypeObservers: Observer<Perspective>[] = [];
  private _localPlayerNicknameObservers: Observer<string>[] = [];
  private _brushToolHotkeyObservers: Observer<string>[] = [];
  private _mapMarkerToolHotkeyObservers: Observer<string>[] = [];
  private _eraserToolHotkeyObservers: Observer<string>[] = [];
  private _genericMarkerHotkeyObservers: Observer<string>[] = [];
  private _carMarkerHotkeyObservers: Observer<string>[] = [];
  private _weaponMarkerHotkeyObservers: Observer<string>[] = [];
  private _itemMarkerHotkeyObservers: Observer<string>[] = [];

  triggerLastPlayedGameModeChanged(mode: GameMode) {
    this._lastPlayedModeObservers.forEach(x => {
      x.next(mode);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  lastPlayedGameModeChanged(): Observable<GameMode> {
    return Observable.create(observer => {
      this._lastPlayedModeObservers.push(observer);
    });
  }

  triggerDefaultSearchGameModeChanged(mode: GameMode) {
    this._defaultSearchModeObservers.forEach(x => {
      x.next(mode);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  defaultSearchGameModeChanged(): Observable<GameMode> {
    return Observable.create(observer => {
      this._defaultSearchModeObservers.push(observer);
    });
  }

  triggerPreferredModeTypeChanged(type: Perspective) {
    this._preferredModeTypeObservers.forEach(x => {
      x.next(type);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  preferredModeTypeChanged(): Observable<Perspective> {
    return Observable.create(observer => {
      this._preferredModeTypeObservers.push(observer);
    });
  }

  triggerLocalPlayerNicknameChanged(nickname: string) {
    this._localPlayerNicknameObservers.forEach(x => {
      x.next(nickname);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  localPlayerNicknameChanged(): Observable<string> {
    return Observable.create(observer => {
      this._localPlayerNicknameObservers.push(observer);
    });
  }

  triggerBrushToolHotkeyChanged(hotkey: string) {
    this._brushToolHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  brushToolHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._brushToolHotkeyObservers.push(observer);
    });
  }

  triggerMapMarkerToolHotkeyChanged(hotkey: string) {
    this._mapMarkerToolHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  mapMarkerToolHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._mapMarkerToolHotkeyObservers.push(observer);
    });
  }

  triggerEraserToolHotkeyChanged(hotkey: string) {
    this._eraserToolHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  eraserToolHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._eraserToolHotkeyObservers.push(observer);
    });
  }

  triggerGenericMarkerHotkeyChanged(hotkey: string) {
    this._genericMarkerHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  genericMarkerHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._genericMarkerHotkeyObservers.push(observer);
    });
  }

  triggerCarMarkerHotkeyChanged(hotkey: string) {
    this._carMarkerHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  carMarkerHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._carMarkerHotkeyObservers.push(observer);
    });
  }

  triggerWeaponMarkerHotkeyChanged(hotkey: string) {
    this._weaponMarkerHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  weaponMarkerHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._weaponMarkerHotkeyObservers.push(observer);
    });
  }

  triggerItemMarkerHotkeyChanged(hotkey: string) {
    this._itemMarkerHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  itemMarkerHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._itemMarkerHotkeyObservers.push(observer);
    });
  }

  get lastPlayedGameMode() {
    return this._lastPlayedGameMode;
  }

  set lastPlayedGameMode(value: GameMode | null) {
    const notifyObservers = this._lastPlayedGameMode !== value;
    this._lastPlayedGameMode = value;

    if (notifyObservers) {
      this.triggerLastPlayedGameModeChanged(value);
    }
  }

  get defaultSearchGameMode() {
    return this._defaultSearchGameMode;
  }

  set defaultSearchGameMode(value: GameMode | null) {
    const notifyObservers = this._defaultSearchGameMode !== value;
    this._defaultSearchGameMode = value;

    if (notifyObservers) {
      this.triggerDefaultSearchGameModeChanged(value);
    }
  }

  get preferredModeType() {
    return this._preferredModeType;
  }

  set preferredModeType(value: Perspective | null) {
    const notifyObservers = this._preferredModeType !== value;
    this._preferredModeType = value;

    if (notifyObservers) {
      this.triggerPreferredModeTypeChanged(value);
    }
  }

  get localPlayerNickname() {
    return this._localPlayerNickname;
  }

  set localPlayerNickname(value: string | null) {
    const notifyObservers = this._localPlayerNickname !== value;
    this._localPlayerNickname = value;

    if (notifyObservers) {
      this.triggerLocalPlayerNicknameChanged(value);
    }
  }

  get brushToolHotkey() {
    return this._brushToolHotkey;
  }

  set brushToolHotkey(value: string) {
    const changed = this._brushToolHotkey !== value;
    this._brushToolHotkey = value;

    if (changed) {
      this.triggerBrushToolHotkeyChanged(value);
    }
  }

  get mapMarkerToolHotkey() {
    return this._mapMarkerToolHotkey;
  }

  set mapMarkerToolHotkey(value: string) {
    const changed = this._mapMarkerToolHotkey !== value;
    this._mapMarkerToolHotkey = value;

    if (changed) {
      this.triggerMapMarkerToolHotkeyChanged(value);
    }
  }

  get eraserToolHotkey() {
    return this._eraserToolHotkey;
  }

  set eraserToolHotkey(value: string) {
    const changed = this._eraserToolHotkey !== value;
    this._eraserToolHotkey = value;

    if (changed) {
      this.triggerEraserToolHotkeyChanged(value);
    }
  }

  get genericMarkerHotkey() {
    return this._genericMarkerHotkey;
  }

  set genericMarkerHotkey(value: string) {
    const changed = this._genericMarkerHotkey !== value;
    this._genericMarkerHotkey = value;

    if (changed) {
      this.triggerGenericMarkerHotkeyChanged(value);
    }
  }

  get carMarkerHotkey() {
    return this._carMarkerHotkey;
  }

  set carMarkerHotkey(value: string) {
    const changed = this._carMarkerHotkey !== value;
    this._carMarkerHotkey = value;

    if (changed) {
      this.triggerCarMarkerHotkeyChanged(value);
    }
  }

  get weaponMarkerHotkey() {
    return this._weaponMarkerHotkey;
  }

  set weaponMarkerHotkey(value: string) {
    const changed = this._weaponMarkerHotkey !== value;
    this._weaponMarkerHotkey = value;

    if (changed) {
      this.triggerWeaponMarkerHotkeyChanged(value);
    }
  }

  get itemMarkerHotkey() {
    return this._itemMarkerHotkey;
  }

  set itemMarkerHotkey(value: string) {
    const changed = this._itemMarkerHotkey !== value;
    this._itemMarkerHotkey = value;

    if (changed) {
      this.triggerItemMarkerHotkeyChanged(value);
    }
  }
}
