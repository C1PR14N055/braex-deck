import { Observable, Observer } from "rxjs";

export class FakeLocalPlayerInfoService {
  private _localPlayerNicknameObservers: Observer<string>[] = [];

  triggerLocalPlayerNickname(nickname: string) {
    this._localPlayerNicknameObservers.forEach(x => {
      x.next(nickname);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  localPlayerNickname(): Observable<string> {
    return Observable.create(observer => {
      this._localPlayerNicknameObservers.push(observer);
    });
  }
}
