import { LobbyStats } from "../../ds/lobby-stats";
import { Observable, Observer } from "rxjs";
import { List } from "immutable";
import { Participant } from "../../ds/player/participant";

export class FakeGameTrackerService {
  // noinspection JSUnusedGlobalSymbols
  averageLobbyStats = LobbyStats.create();
  // noinspection JSUnusedGlobalSymbols
  minLobbyStats = LobbyStats.create();
  // noinspection JSUnusedGlobalSymbols
  maxLobbyStats = LobbyStats.create();
  private _playersChangedObservers: Observer<List<Participant>>[] = [];

  triggerPlayersChanged(players: List<Participant>) {
    this._playersChangedObservers.forEach(x => {
      x.next(players);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  playersChanged(): Observable<List<Participant>> {
    return Observable.create(observer => {
      this._playersChangedObservers.push(observer);
    });
  }
}
