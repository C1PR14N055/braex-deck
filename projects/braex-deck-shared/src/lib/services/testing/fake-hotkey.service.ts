export class FakeHotkeyService {
  private _registeredHotkeys = new Map<string, () => void>();

  // noinspection JSUnusedGlobalSymbols
  registerHotkey(keyCombination: string, handler: () => void) {
    this._registeredHotkeys.set(keyCombination, handler);
  }

  // noinspection JSUnusedGlobalSymbols
  unregisterHotkey(keyCombination: string) {
    this._registeredHotkeys.delete(keyCombination);
  }

  hasHotkey(keyCombination: string) {
    return this._registeredHotkeys.has(keyCombination);
  }

  triggerHotkey(keyCombination: string) {
    this._registeredHotkeys.get(keyCombination)();
  }
}
