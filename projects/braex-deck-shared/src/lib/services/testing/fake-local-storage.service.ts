export class FakeLocalStorageService {
  private _storage = new Map<string, string>();

  set(key: string, data: string) {
    this._storage.set(key, data);
  }

  has(key: string) {
    return this._storage.has(key);
  }

  get(key: string) {
    return this._storage.get(key);
  }

  delete(key: string) {
    this._storage.delete(key);
  }
}
