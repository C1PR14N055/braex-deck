import { GameMode } from "../../ds/game/game-mode.enum";
import { Observable } from "rxjs";
import { List } from "immutable";
import { PlayerStats } from "../../ds/player/player-stats";

export class FakePlayerStatsService {
  playerStatsToEmit: List<PlayerStats>;

  // noinspection JSUnusedLocalSymbols
  playerStats(
    gameMode: GameMode,
    ...players: string[]
  ): Observable<List<PlayerStats>> {
    return Observable.create(observer => {
      observer.next(this.playerStatsToEmit);
      observer.complete();
    });
  }
}
