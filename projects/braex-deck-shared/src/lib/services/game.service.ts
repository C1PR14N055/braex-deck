import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { List } from "immutable";
import { LazyGame } from "../ds/game/lazy-game";
import { environment } from "../../environments/environment";
import * as format from "string-format";
import { ExtLazyGameConverter } from "../converters/game/ext-lazy-game.converter";
import { ExtLazyGame } from "../ds/external/game/ext-lazy-game";
import { PlayerNotInDbError } from "../error/player-not-in-db.error";
import { ServerOperationError } from "../error/server-operation.error";
import { PagingOptions } from "../ds/paging-options";
import { PagingUtils } from "../utils/paging-utils";
import { ExtPageableResponse } from "../ds/external/ext-pageable-response";
import { Pageable } from "../ds/pageable";
import { ExtPageableConverter } from "../converters/ext-pageable-converter";
import { GameRoster } from "../ds/game/game-roster";
import { ExtGameRosterConverter } from "../converters/game/ext-game-roster.converter";
import { ExtGameRoster } from "../ds/external/game/ext-game-roster";

@Injectable({
  providedIn: "root"
})
export class GameService {
  private _cachedGames = new Map<string, LazyGame>();
  private _cachedRosters = new Map<string, List<GameRoster>>();

  constructor(
    private _http: HttpClient,
    private _extLazyGame: ExtLazyGameConverter,
    private _extPageable: ExtPageableConverter,
    private _extGameRoster: ExtGameRosterConverter
  ) {}

  getGameById(gameId: string): Observable<LazyGame | null> {
    return Observable.create(observer => {
      if (this._cachedGames.has(gameId)) {
        observer.next(this._cachedGames.get(gameId));
        observer.complete();

        return;
      }

      this._http
        .get<ExtLazyGame>(
          environment.apiUrl +
            format(environment.apiEndpoints.anyGameById, { gameId: gameId })
        )
        .subscribe(
          extGame => {
            const game = this._extLazyGame.convert(extGame);
            this._cachedGames.set(game.gameId, game);
            observer.next(game);
            observer.complete();
          },
          error => {
            if (error.status === 404) {
              observer.next(null);
              observer.complete();
            } else if (error.status === 500) {
              observer.error(
                new ServerOperationError(
                  `Failed to fetch the game \`${gameId}\``
                )
              );
            }
          }
        );
    });
  }

  getGameRosters(gameId: string): Observable<List<GameRoster>> {
    return Observable.create(observer => {
      if (this._cachedRosters.has(gameId)) {
        observer.next(this._cachedRosters.get(gameId));
        observer.complete();

        return;
      }

      this._http
        .get<ExtGameRoster[]>(
          environment.apiUrl +
            format(environment.apiEndpoints.gameRoster, { gameId: gameId })
        )
        .subscribe(
          rosters => {
            const result = List<GameRoster>(
              rosters.map(x => this._extGameRoster.convert(x))
            );
            this._cachedRosters.set(gameId, result);
            observer.next(result);
            observer.complete();
          },
          error => {
            if (error.status === 404) {
              observer.next(List<GameRoster>());
              observer.complete();
            } else if (error.status === 500) {
              observer.error(
                new ServerOperationError(
                  `Failed to load the roster for the game \`${gameId}\``
                )
              );
            }
          }
        );
    });
  }

  getGamesOfPlayer(
    playerNickname: string,
    options?: PagingOptions
  ): Observable<Pageable<LazyGame>> {
    return Observable.create(observer => {
      const params = PagingUtils.buildPagingParams(options);
      this._http
        .get<ExtPageableResponse<ExtLazyGame>>(
          environment.apiUrl +
            format(environment.apiEndpoints.searchPlayer, {
              nickname: playerNickname
            }),
          {
            params: params
          }
        )
        .subscribe(
          page => {
            const games = List(
              page.content.map(x => this._extLazyGame.convert(x))
            );
            const result: Pageable<LazyGame> = this._extPageable.convert(page);
            result.content = games;
            observer.next(result);
            observer.complete();
          },
          error => {
            if (error.status === 500) {
              observer.error(
                new ServerOperationError(
                  `Failed to fetch the games of the player \`${playerNickname}\` from the server`
                )
              );
            } else if (error.status === 202) {
              observer.error(
                new PlayerNotInDbError(
                  `No matches have been loaded for the player \`${playerNickname}\` yet`
                )
              );
            } else if (error.status === 404) {
              observer.next(new Pageable<LazyGame>());
              observer.complete();
            }
          }
        );
    });
  }

  getGamesOfEvent(
    eventId: string,
    options?: PagingOptions
  ): Observable<Pageable<LazyGame>> {
    return Observable.create(observer => {
      const params = PagingUtils.buildPagingParams(options);
      this._http
        .get<ExtPageableResponse<ExtLazyGame>>(
          environment.apiUrl +
            format(environment.apiEndpoints.getEventMatches, {
              eventId: eventId
            }),
          {
            params: params
          }
        )
        .subscribe(
          page => {
            const games = List(
              page.content.map(x => this._extLazyGame.convert(x))
            );
            const result: Pageable<LazyGame> = this._extPageable.convert(page);
            result.content = games;
            observer.next(result);
            observer.complete();
          },
          error => {
            if (error.status === 500) {
              observer.error(
                new ServerOperationError(
                  `Failed to fetch the games of the event id \`${eventId}\` from the server`
                )
              );
            } else if (error.status === 202) {
              observer.error(
                new PlayerNotInDbError(
                  `No matches have been loaded for the event id \`${eventId}\` yet`
                )
              );
            } else if (error.status === 404) {
              observer.next(new Pageable<LazyGame>());
              observer.complete();
            }
          }
        );
    });
  }
}
