import { Injectable, OnDestroy } from "@angular/core";
import { HotkeyService } from "./hotkey.service";
import { BrushLinesCanvasModule } from "../map-canvas/canvas-modules/modules/brush-lines.canvas-module";
import { MapToolService } from "../map-canvas/services/map-tool.service";
import { MapCanvasTool } from "../map-canvas/ds/map-canvas-tool.enum";
import { CanvasEngine } from "../map-canvas/core/canvas-engine";
import { OverwolfGameEventsService } from "../overwolf/overwolf-game-events.service";
import { MapBackgroundCanvasModule } from "../map-canvas/canvas-modules/modules/map-background.canvas-module";
import { Vector3 } from "../map-canvas/ds/vector3";
import { Subscription } from "rxjs";
import { InGameUtils } from "../map-canvas/utils/in-game-utils";
import { MarkerConfigService } from "../map-canvas/services/marker-config.service";
import { MapMarkerType } from "../map-canvas/ds/map-marker-type.enum";
import {
  AppSettingsService,
  FOCUS_PLAYER_COMBO,
  REDO_COMBO,
  UNDO_COMBO
} from "./app-settings.service";
import { ChangeHistoryService } from "../map-canvas/services/change-history.service";

@Injectable({
  providedIn: "root"
})
export class MapHotkeysService implements OnDestroy {
  private _playerLocation: Vector3 | null = null;
  private _subs: Subscription[] = [];

  constructor(
    private _hotkey: HotkeyService,
    private _brushLinesModule: BrushLinesCanvasModule,
    private _mapTool: MapToolService,
    private _engine: CanvasEngine,
    private _gameEvents: OverwolfGameEventsService,
    private _mapBackground: MapBackgroundCanvasModule,
    private _markerConfig: MarkerConfigService,
    private _appSettings: AppSettingsService,
    private _changeHistory: ChangeHistoryService
  ) {
    const sub = this._gameEvents
      .onLocalPlayerLocationChanged()
      .subscribe(location => {
        this._playerLocation = location;
      });
    this._subs.push(sub);
    this._initHotkeys();
    this._initHotkeyRebinding();
  }

  private _initHotkeys() {
    this._hotkey.registerHotkey(UNDO_COMBO, () => {
      this._changeHistory.undo();
    });
    this._hotkey.registerHotkey(REDO_COMBO, () => {
      this._changeHistory.redo();
    });
    this._hotkey.registerHotkey(this._appSettings.brushToolHotkey, () =>
      this._brushToolAction()
    );
    this._hotkey.registerHotkey(FOCUS_PLAYER_COMBO, () => {
      this._centerOnPlayer();
    });
    this._hotkey.registerHotkey(this._appSettings.eraserToolHotkey, () =>
      this._eraserToolAction()
    );
    this._hotkey.registerHotkey(this._appSettings.mapMarkerToolHotkey, () =>
      this._mapMarkerToolAction()
    );
    this._hotkey.registerHotkey(this._appSettings.genericMarkerHotkey, () =>
      this._genericMarkerAction()
    );
    this._hotkey.registerHotkey(this._appSettings.carMarkerHotkey, () =>
      this._carMarkerAction()
    );
    this._hotkey.registerHotkey(this._appSettings.weaponMarkerHotkey, () =>
      this._weaponMarkerAction()
    );
    this._hotkey.registerHotkey(this._appSettings.itemMarkerHotkey, () =>
      this._itemMarkerAction()
    );
  }

  private _centerOnPlayer() {
    if (
      !this._engine.camera ||
      !this._mapBackground.currentMap ||
      !this._mapBackground.mapSize ||
      !this._playerLocation
    ) {
      return;
    }

    this._engine.camera.focusPosition(
      InGameUtils.overwolfMapLocationToCanvasSpace(
        this._mapBackground.currentMap,
        this._mapBackground.mapSize,
        this._playerLocation
      )
    );
  }

  private _initHotkeyRebinding() {
    let brushToolHotkey = this._appSettings.brushToolHotkey;
    let sub = this._appSettings.brushToolHotkeyChanged().subscribe(hotkey => {
      this._hotkey.unregisterHotkey(brushToolHotkey);
      this._hotkey.registerHotkey(hotkey, () => this._brushToolAction());
      brushToolHotkey = hotkey;
    });
    this._subs.push(sub);

    let mapMarkerToolHotkey = this._appSettings.mapMarkerToolHotkey;
    sub = this._appSettings.mapMarkerToolHotkeyChanged().subscribe(hotkey => {
      this._hotkey.unregisterHotkey(mapMarkerToolHotkey);
      this._hotkey.registerHotkey(hotkey, () => this._mapMarkerToolAction());
      mapMarkerToolHotkey = hotkey;
    });
    this._subs.push(sub);

    let eraserToolHotkey = this._appSettings.eraserToolHotkey;
    sub = this._appSettings.eraserToolHotkeyChanged().subscribe(hotkey => {
      this._hotkey.unregisterHotkey(eraserToolHotkey);
      this._hotkey.registerHotkey(hotkey, () => this._eraserToolAction());
      eraserToolHotkey = hotkey;
    });
    this._subs.push(sub);

    let genericMarkerHotkey = this._appSettings.genericMarkerHotkey;
    sub = this._appSettings.genericMarkerHotkeyChanged().subscribe(hotkey => {
      this._hotkey.unregisterHotkey(genericMarkerHotkey);
      this._hotkey.registerHotkey(hotkey, () => this._genericMarkerAction());
      genericMarkerHotkey = hotkey;
    });
    this._subs.push(sub);

    let carMarkerHotkey = this._appSettings.carMarkerHotkey;
    sub = this._appSettings.carMarkerHotkeyChanged().subscribe(hotkey => {
      this._hotkey.unregisterHotkey(carMarkerHotkey);
      this._hotkey.registerHotkey(hotkey, () => this._carMarkerAction());
      carMarkerHotkey = hotkey;
    });
    this._subs.push(sub);

    let weaponMarkerHotkey = this._appSettings.weaponMarkerHotkey;
    sub = this._appSettings.weaponMarkerHotkeyChanged().subscribe(hotkey => {
      this._hotkey.unregisterHotkey(weaponMarkerHotkey);
      this._hotkey.registerHotkey(hotkey, () => this._weaponMarkerAction());
      weaponMarkerHotkey = hotkey;
    });
    this._subs.push(sub);

    let itemMarkerHotkey = this._appSettings.itemMarkerHotkey;
    sub = this._appSettings.itemMarkerHotkeyChanged().subscribe(hotkey => {
      this._hotkey.unregisterHotkey(itemMarkerHotkey);
      this._hotkey.registerHotkey(hotkey, () => this._itemMarkerAction());
      itemMarkerHotkey = hotkey;
    });
    this._subs.push(sub);
  }

  private _brushToolAction() {
    this._mapTool.activeTool = MapCanvasTool.BRUSH;
  }

  private _eraserToolAction() {
    this._mapTool.activeTool = MapCanvasTool.ERASER;
  }

  private _mapMarkerToolAction() {
    this._mapTool.activeTool = MapCanvasTool.MARKER;
  }

  private _genericMarkerAction() {
    this._markerConfig.type = MapMarkerType.DEFAULT;
  }

  private _carMarkerAction() {
    this._markerConfig.type = MapMarkerType.CAR;
  }

  private _weaponMarkerAction() {
    this._markerConfig.type = MapMarkerType.WEAPON;
  }

  private _itemMarkerAction() {
    this._markerConfig.type = MapMarkerType.ITEM;
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }
}
