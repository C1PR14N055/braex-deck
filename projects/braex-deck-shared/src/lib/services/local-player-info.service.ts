import { Injectable, OnDestroy } from "@angular/core";
import { OverwolfGameEventsService } from "../overwolf/overwolf-game-events.service";
import { Observable, Observer, Subscription } from "rxjs";
import { AppSettingsService } from "./app-settings.service";

@Injectable({
  providedIn: "root"
})
export class LocalPlayerInfoService implements OnDestroy {
  private _localPlayerNickname: string;
  private _localPlayerNicknameObservers: Observer<string>[] = [];
  private _subs: Subscription[] = [];

  constructor(
    private _gameEvents: OverwolfGameEventsService,
    private _appSettings: AppSettingsService
  ) {
    const nickname = this._appSettings.localPlayerNickname;
    if (nickname) {
      this._localPlayerNickname = nickname;
    }

    let sub = this._gameEvents.localPlayerName().subscribe(nickname => {
      const settingsNickname = this._appSettings.localPlayerNickname;
      if (settingsNickname) {
        if (this._localPlayerNickname !== nickname) {
          this._localPlayerNickname = nickname;
          this._triggerLocalPlayerNickname(nickname);
          this._appSettings.localPlayerNickname = nickname;
        }
      } else {
        this._appSettings.localPlayerNickname = nickname;
        this._localPlayerNickname = nickname;
        this._triggerLocalPlayerNickname(nickname);
      }
    });
    this._subs.push(sub);

    sub = this._appSettings.localPlayerNicknameChanged().subscribe(nickname => {
      if (nickname !== this._localPlayerNickname) {
        this._localPlayerNickname = nickname;
        this._triggerLocalPlayerNickname(nickname);
      }
    });
    this._subs.push(sub);
  }

  ngOnDestroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }

  private _triggerLocalPlayerNickname(nickname: string) {
    this._localPlayerNicknameObservers.forEach(x => {
      x.next(nickname);
    });
  }

  localPlayerNickname(): Observable<string> {
    return Observable.create(observer => {
      this._localPlayerNicknameObservers.push(observer);

      if (this._localPlayerNickname) {
        observer.next(this._localPlayerNickname);
      }
    });
  }
}
