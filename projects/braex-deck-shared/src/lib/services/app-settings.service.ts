import { Injectable } from "@angular/core";
import { GameMode } from "../ds/game/game-mode.enum";
import { Perspective } from "../ds/game/perspective.enum";
import { LocalStorageService } from "./local-storage.service";
import { Observable, Observer } from "rxjs";

// TODO(pmo): move the hotkey definitions to the MapTool annotation
//  (https://github.com/Peter-Morawski/braex-deck/issues/79)
export const UNDO_COMBO = "ctrl+z";
export const REDO_COMBO = "ctrl+y";
export const BRUSH_TOOL_COMBO = "w";
export const ERASER_TOOL_COMBO = "e";
export const MAP_MARKER_TOOL_COMBO = "q";
export const GENERIC_MAP_MARKER_COMBO = `${MAP_MARKER_TOOL_COMBO}+1`;
export const CAR_MAP_MARKER_COMBO = `${MAP_MARKER_TOOL_COMBO}+2`;
export const WEAPON_MAP_MARKER_COMBO = `${MAP_MARKER_TOOL_COMBO}+3`;
export const ITEM_MAP_MARKER_COMBO = `${MAP_MARKER_TOOL_COMBO}+4`;
export const FOCUS_PLAYER_COMBO = "space";
export const PLAY_PAUSE_COMBO = "k";
export const FAST_FORWARD_COMBO = "l";
export const FAST_BACKWARD_COMBO = "j";

/**
 * The {@link GameMode} which should be used to search for the stats of the players
 * when the mode of the last game is not available e.g. because it's the first time
 * the application is being used.
 */
export const DEFAULT_SEARCH_GAME_MODE = "default_game_mode";
/**
 * Whether the stats should be searched for tpp or fpp.
 */
export const PREFERRED_PERSPECTIVE = "preferred_perspective";
/**
 * The {@link GameMode} which the user played recently.
 */
export const LAST_PLAYED_GAME_MODE = "last_played_game_mode";
/**
 * The pubg nickname of the local player.
 */
export const LOCAL_PLAYER_NICKNAME = "local_player_nickname";
export const BRUSH_TOOL_HOTKEY = "brush_tool_hotkey";
export const MAP_MARKER_TOOL_HOTKEY = "map_marker_tool_hotkey";
export const ERASER_TOOL_HOTKEY = "eraser_tool_hotkey";
export const GENERIC_MARKER_HOTKEY = "generic_marker_hotkey";
export const CAR_MARKER_HOTKEY = "car_marker_hotkey";
export const WEAPON_MARKER_HOTKEY = "weapon_marker_hotkey";
export const ITEM_MARKER_HOTKEY = "item_marker_hotkey";
export const PLAY_PAUSE_HOTKEY = "play_pause_hotkey";
export const FAST_FORWARD_HOTKEY = "fast_forward_hotkey";
export const FAST_BACKWARD_HOTKEY = "fast_backward_hotkey";
export const UNDO_HOTKEY = "undo_hotkey";
export const REDO_HOTKEY = "redo_hotkey";
export const FOCUS_PLAYER_HOTKEY = "focus_player_hotkey";

@Injectable({
  providedIn: "root"
})
export class AppSettingsService {
  private _lastPlayedGameMode: GameMode | null = null;
  private _defaultSearchGameMode: GameMode | null = null;
  private _preferredPerspective: Perspective | null = null;
  private _localPlayerNickname: string | null = null;
  private _brushToolHotkey: string | null = null;
  private _mapMarkerToolHotkey: string | null = null;
  private _eraserToolHotkey: string | null = null;
  private _genericMarkerHotkey: string | null = null;
  private _carMarkerHotkey: string | null = null;
  private _weaponMarkerHotkey: string | null = null;
  private _itemMarkerHotkey: string | null = null;
  private _playPauseHotkey: string | null = null;
  private _fastForwardHotkey: string | null = null;
  private _fastBackwardHotkey: string | null = null;
  private _undoHotkey: string | null = null;
  private _redoHotkey: string | null = null;
  private _focusPlayerHotkey: string | null = null;
  private _lastPlayedGameModeObservers: Observer<GameMode>[] = [];
  private _defaultSearchGameModeObservers: Observer<GameMode>[] = [];
  private _preferredModeTypeObservers: Observer<Perspective>[] = [];
  private _localPlayerNicknameObservers: Observer<string>[] = [];
  private _brushToolHotkeyObservers: Observer<string>[] = [];
  private _mapMarkerToolHotkeyObservers: Observer<string>[] = [];
  private _eraserToolHotkeyObservers: Observer<string>[] = [];
  private _genericMarkerHotkeyObservers: Observer<string>[] = [];
  private _carMarkerHotkeyObservers: Observer<string>[] = [];
  private _weaponMarkerHotkeyObservers: Observer<string>[] = [];
  private _itemMarkerHotkeyObservers: Observer<string>[] = [];
  private _playPauseHotkeyObservers: Observer<string>[] = [];
  private _fastForwardHotkeyObservers: Observer<string>[] = [];
  private _fastBackwardHotkeyObservers: Observer<string>[] = [];
  private _undoHotkeyObservers: Observer<string>[] = [];
  private _redoHotkeyObservers: Observer<string>[] = [];
  private _focusPlayerHotkeyObservers: Observer<string>[] = [];

  constructor(private _localStorage: LocalStorageService) {
    this._initValuesFromLocalStorage();
  }

  private _initValuesFromLocalStorage() {
    this._lastPlayedGameMode = this._localStorage.get(
      LAST_PLAYED_GAME_MODE
    ) as GameMode;
    this._defaultSearchGameMode = this._localStorage.get(
      DEFAULT_SEARCH_GAME_MODE
    ) as GameMode;
    this._preferredPerspective = this._localStorage.get(
      PREFERRED_PERSPECTIVE
    ) as Perspective;
    this._localPlayerNickname = this._localStorage.get(LOCAL_PLAYER_NICKNAME);
    const brushToolHotkey = this._localStorage.get(BRUSH_TOOL_HOTKEY);
    if (brushToolHotkey) {
      this._brushToolHotkey = brushToolHotkey;
    } else {
      this.brushToolHotkey = BRUSH_TOOL_COMBO;
    }

    const mapMarkerToolHotkey = this._localStorage.get(MAP_MARKER_TOOL_HOTKEY);
    this._localStorage.has(MAP_MARKER_TOOL_HOTKEY);
    if (mapMarkerToolHotkey) {
      this._mapMarkerToolHotkey = mapMarkerToolHotkey;
    } else {
      this.mapMarkerToolHotkey = MAP_MARKER_TOOL_COMBO;
    }

    const eraserToolHotkey = this._localStorage.get(ERASER_TOOL_HOTKEY);
    if (eraserToolHotkey) {
      this._eraserToolHotkey = eraserToolHotkey;
    } else {
      this.eraserToolHotkey = ERASER_TOOL_COMBO;
    }

    const genericMarkerHotkey = this._localStorage.get(GENERIC_MARKER_HOTKEY);
    if (genericMarkerHotkey) {
      this._genericMarkerHotkey = genericMarkerHotkey;
    } else {
      this.genericMarkerHotkey = GENERIC_MAP_MARKER_COMBO;
    }

    const carMarkerHotkey = this._localStorage.get(CAR_MARKER_HOTKEY);
    if (carMarkerHotkey) {
      this._carMarkerHotkey = carMarkerHotkey;
    } else {
      this.carMarkerHotkey = CAR_MAP_MARKER_COMBO;
    }

    const weaponMarkerHotkey = this._localStorage.get(WEAPON_MARKER_HOTKEY);
    if (weaponMarkerHotkey) {
      this._weaponMarkerHotkey = weaponMarkerHotkey;
    } else {
      this.weaponMarkerHotkey = WEAPON_MAP_MARKER_COMBO;
    }

    const itemMarkerHotkey = this._localStorage.get(ITEM_MARKER_HOTKEY);
    if (itemMarkerHotkey) {
      this._itemMarkerHotkey = itemMarkerHotkey;
    } else {
      this.itemMarkerHotkey = ITEM_MAP_MARKER_COMBO;
    }

    const playPauseHotkey = this._localStorage.get(PLAY_PAUSE_HOTKEY);
    if (playPauseHotkey) {
      this._playPauseHotkey = playPauseHotkey;
    } else {
      this.playPauseHotkey = PLAY_PAUSE_COMBO;
    }

    const fastForwardHotkey = this._localStorage.get(FAST_FORWARD_HOTKEY);
    if (fastForwardHotkey) {
      this._fastForwardHotkey = fastForwardHotkey;
    } else {
      this.fastForwardHotkey = FAST_FORWARD_COMBO;
    }

    const fastBackwardHotkey = this._localStorage.get(FAST_BACKWARD_HOTKEY);
    if (fastBackwardHotkey) {
      this._fastBackwardHotkey = fastBackwardHotkey;
    } else {
      this.fastBackwardHotkey = FAST_BACKWARD_COMBO;
    }

    const undoHotkey = this._localStorage.get(UNDO_HOTKEY);
    if (undoHotkey) {
      this._undoHotkey = undoHotkey;
    } else {
      this.undoHotkey = UNDO_COMBO;
    }

    const redoHotkey = this._localStorage.get(REDO_HOTKEY);
    if (redoHotkey) {
      this._redoHotkey = redoHotkey;
    } else {
      this.redoHotkey = REDO_COMBO;
    }

    const focusPlayerHotkey = this._localStorage.get(FOCUS_PLAYER_HOTKEY);
    if (focusPlayerHotkey) {
      this._focusPlayerHotkey = focusPlayerHotkey;
    } else {
      this.focusPlayerHotkey = FOCUS_PLAYER_COMBO;
    }
  }

  private _triggerLastPlayedGameModeChanged(mode: GameMode) {
    this._lastPlayedGameModeObservers.forEach(x => {
      x.next(mode);
    });
  }

  lastPlayedGameModeChanged(): Observable<GameMode> {
    return Observable.create(observer => {
      this._lastPlayedGameModeObservers.push(observer);
    });
  }

  private _triggerDefaultSearchGameModeChanged(mode: GameMode) {
    this._defaultSearchGameModeObservers.forEach(x => {
      x.next(mode);
    });
  }

  defaultSearchGameModeChanged(): Observable<GameMode> {
    return Observable.create(observer => {
      this._defaultSearchGameModeObservers.push(observer);
    });
  }

  private _triggerPreferredPerspectiveChanged(type: Perspective) {
    this._preferredModeTypeObservers.forEach(x => {
      x.next(type);
    });
  }

  preferredPerspectiveChanged(): Observable<Perspective> {
    return Observable.create(observer => {
      this._preferredModeTypeObservers.push(observer);
    });
  }

  private _triggerLocalPlayerNicknameChanged(nickname: string) {
    this._localPlayerNicknameObservers.forEach(x => {
      x.next(nickname);
    });
  }

  localPlayerNicknameChanged(): Observable<string> {
    return Observable.create(observer => {
      this._localPlayerNicknameObservers.push(observer);
    });
  }

  private _triggerBrushToolHotkeyChanged(hotkey: string) {
    this._brushToolHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  brushToolHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._brushToolHotkeyObservers.push(observer);
    });
  }

  private _triggerMapMarkerToolHotkeyChanged(hotkey: string) {
    this._mapMarkerToolHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  mapMarkerToolHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._mapMarkerToolHotkeyObservers.push(observer);
    });
  }

  private _triggerEraserToolHotkeyChanged(hotkey: string) {
    this._eraserToolHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  eraserToolHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._eraserToolHotkeyObservers.push(observer);
    });
  }

  private _triggerGenericMarkerHotkeyChanged(hotkey: string) {
    this._genericMarkerHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  genericMarkerHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._genericMarkerHotkeyObservers.push(observer);
    });
  }

  private _triggerCarMarkerHotkeyChanged(hotkey: string) {
    this._carMarkerHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  carMarkerHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._carMarkerHotkeyObservers.push(observer);
    });
  }

  private _triggerWeaponMarkerHotkeyChanged(hotkey: string) {
    this._weaponMarkerHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  weaponMarkerHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._weaponMarkerHotkeyObservers.push(observer);
    });
  }

  private _triggerItemMarkerHotkeyChanged(hotkey: string) {
    this._itemMarkerHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  itemMarkerHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._itemMarkerHotkeyObservers.push(observer);
    });
  }

  private _triggerPlayPauseHotkeyChanged(hotkey: string) {
    this._playPauseHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  playPauseHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._playPauseHotkeyObservers.push(observer);
    });
  }

  private _triggerFastForwardHotkeyChanged(hotkey: string) {
    this._fastForwardHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  fastForwardHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._fastForwardHotkeyObservers.push(observer);
    });
  }

  private _triggerFastBackwardHotkeyChanged(hotkey: string) {
    this._fastBackwardHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  fastBackwardHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._fastBackwardHotkeyObservers.push(observer);
    });
  }

  private _triggerUndoHotkeyChanged(hotkey: string) {
    this._undoHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  undoHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._undoHotkeyObservers.push(observer);
    });
  }

  private _triggerRedoHotkeyChanged(hotkey: string) {
    this._redoHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  redoHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._redoHotkeyObservers.push(observer);
    });
  }

  private _triggerFocusPlayerHotkeyChanged(hotkey: string) {
    this._focusPlayerHotkeyObservers.forEach(x => {
      x.next(hotkey);
    });
  }

  focusPlayerHotkeyChanged(): Observable<string> {
    return Observable.create(observer => {
      this._focusPlayerHotkeyObservers.push(observer);
    });
  }

  get lastPlayedGameMode() {
    return this._lastPlayedGameMode;
  }

  set lastPlayedGameMode(value: GameMode) {
    const notifyObservers = this._lastPlayedGameMode !== value;
    this._lastPlayedGameMode = value;
    this._localStorage.set(LAST_PLAYED_GAME_MODE, value);

    if (notifyObservers) {
      this._triggerLastPlayedGameModeChanged(value);
    }
  }

  get defaultSearchGameMode() {
    return this._defaultSearchGameMode;
  }

  set defaultSearchGameMode(value: GameMode) {
    const notifyObservers = this._defaultSearchGameMode !== value;
    this._defaultSearchGameMode = value;
    this._localStorage.set(DEFAULT_SEARCH_GAME_MODE, value);

    if (notifyObservers) {
      this._triggerDefaultSearchGameModeChanged(value);
    }
  }

  get preferredPerspective() {
    return this._preferredPerspective;
  }

  set preferredPerspective(value: Perspective) {
    const notifyObservers = this._preferredPerspective !== value;
    this._preferredPerspective = value;
    this._localStorage.set(PREFERRED_PERSPECTIVE, value);

    if (notifyObservers) {
      this._triggerPreferredPerspectiveChanged(value);
    }
  }

  get localPlayerNickname() {
    return this._localPlayerNickname;
  }

  set localPlayerNickname(value: string) {
    const notifyObservers = this._localPlayerNickname !== value;
    this._localPlayerNickname = value;
    this._localStorage.set(LOCAL_PLAYER_NICKNAME, value);

    if (notifyObservers) {
      this._triggerLocalPlayerNicknameChanged(value);
    }
  }

  get brushToolHotkey() {
    return this._brushToolHotkey;
  }

  set brushToolHotkey(value: string) {
    const changed = this._brushToolHotkey !== value;
    this._brushToolHotkey = value;
    this._localStorage.set(BRUSH_TOOL_HOTKEY, value);

    if (changed) {
      this._triggerBrushToolHotkeyChanged(value);
    }
  }

  get mapMarkerToolHotkey() {
    return this._mapMarkerToolHotkey;
  }

  set mapMarkerToolHotkey(value: string) {
    const changed = this._mapMarkerToolHotkey !== value;
    this._mapMarkerToolHotkey = value;
    this._localStorage.set(MAP_MARKER_TOOL_HOTKEY, value);

    if (changed) {
      this._triggerMapMarkerToolHotkeyChanged(value);
    }
  }

  get eraserToolHotkey() {
    return this._eraserToolHotkey;
  }

  set eraserToolHotkey(value: string) {
    const changed = this._eraserToolHotkey !== value;
    this._eraserToolHotkey = value;
    this._localStorage.set(ERASER_TOOL_HOTKEY, value);

    if (changed) {
      this._triggerEraserToolHotkeyChanged(value);
    }
  }

  get genericMarkerHotkey() {
    return this._genericMarkerHotkey;
  }

  set genericMarkerHotkey(value: string) {
    const changed = this._genericMarkerHotkey !== value;
    this._genericMarkerHotkey = value;
    this._localStorage.set(GENERIC_MARKER_HOTKEY, value);

    if (changed) {
      this._triggerGenericMarkerHotkeyChanged(value);
    }
  }

  get carMarkerHotkey() {
    return this._carMarkerHotkey;
  }

  set carMarkerHotkey(value: string) {
    const changed = this._carMarkerHotkey !== value;
    this._carMarkerHotkey = value;
    this._localStorage.set(CAR_MARKER_HOTKEY, value);

    if (changed) {
      this._triggerCarMarkerHotkeyChanged(value);
    }
  }

  get weaponMarkerHotkey() {
    return this._weaponMarkerHotkey;
  }

  set weaponMarkerHotkey(value: string) {
    const changed = this._weaponMarkerHotkey !== value;
    this._weaponMarkerHotkey = value;
    this._localStorage.set(WEAPON_MARKER_HOTKEY, value);

    if (changed) {
      this._triggerWeaponMarkerHotkeyChanged(value);
    }
  }

  get itemMarkerHotkey() {
    return this._itemMarkerHotkey;
  }

  set itemMarkerHotkey(value: string) {
    const changed = this._itemMarkerHotkey !== value;
    this._itemMarkerHotkey = value;
    this._localStorage.set(ITEM_MARKER_HOTKEY, value);

    if (changed) {
      this._triggerItemMarkerHotkeyChanged(value);
    }
  }

  get playPauseHotkey() {
    return this._playPauseHotkey;
  }

  set playPauseHotkey(value: string) {
    const changed = this._playPauseHotkey !== value;
    this._playPauseHotkey = value;
    this._localStorage.set(PLAY_PAUSE_HOTKEY, value);

    if (changed) {
      this._triggerPlayPauseHotkeyChanged(value);
    }
  }

  get fastForwardHotkey() {
    return this._fastForwardHotkey;
  }

  set fastForwardHotkey(value: string) {
    const changed = this._fastForwardHotkey !== value;
    this._fastForwardHotkey = value;
    this._localStorage.set(FAST_FORWARD_HOTKEY, value);

    if (changed) {
      this._triggerFastForwardHotkeyChanged(value);
    }
  }

  get fastBackwardHotkey() {
    return this._fastBackwardHotkey;
  }

  set fastBackwardHotkey(value: string) {
    const changed = this._fastBackwardHotkey !== value;
    this._fastBackwardHotkey = value;
    this._localStorage.set(FAST_BACKWARD_HOTKEY, value);

    if (changed) {
      this._triggerFastBackwardHotkeyChanged(value);
    }
  }

  get undoHotkey() {
    return this._undoHotkey;
  }

  set undoHotkey(value: string) {
    const changed = this._undoHotkey !== value;
    this._undoHotkey = value;
    this._localStorage.set(UNDO_HOTKEY, value);

    if (changed) {
      this._triggerUndoHotkeyChanged(value);
    }
  }

  get redoHotkey() {
    return this._redoHotkey;
  }

  set redoHotkey(value: string) {
    const changed = this._redoHotkey !== value;
    this._redoHotkey = value;
    this._localStorage.set(REDO_HOTKEY, value);

    if (changed) {
      this._triggerRedoHotkeyChanged(value);
    }
  }

  get focusPlayerHotkey() {
    return this._focusPlayerHotkey;
  }

  set focusPlayerHotkey(value: string) {
    const changed = this._focusPlayerHotkey !== value;
    this._focusPlayerHotkey = value;
    this._localStorage.set(FOCUS_PLAYER_HOTKEY, value);

    if (changed) {
      this._triggerFocusPlayerHotkeyChanged(value);
    }
  }
}
