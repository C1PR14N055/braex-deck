import { Component, Inject } from "@angular/core";
import { faCheckCircle } from "@fortawesome/free-regular-svg-icons";
import { MAT_SNACK_BAR_DATA } from "@angular/material";

class ViewContext {
  public faCheckCircle = faCheckCircle;
}

@Component({
  selector: "app-success-snack-bar",
  templateUrl: "./success-snack-bar.component.html",
  styleUrls: ["./success-snack-bar.component.scss"]
})
export class SuccessSnackBarComponent {
  public viewContext = new ViewContext();

  public constructor(@Inject(MAT_SNACK_BAR_DATA) public message: string) {}
}
