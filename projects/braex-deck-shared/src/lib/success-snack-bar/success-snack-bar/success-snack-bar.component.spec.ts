import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SuccessSnackBarComponent } from "./success-snack-bar.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { MAT_SNACK_BAR_DATA } from "@angular/material";

describe("SuccessSnackBarComponent", () => {
  let component: SuccessSnackBarComponent;
  let fixture: ComponentFixture<SuccessSnackBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SuccessSnackBarComponent],
      providers: [{ provide: MAT_SNACK_BAR_DATA, useValue: [] }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessSnackBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
