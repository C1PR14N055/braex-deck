import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SuccessSnackBarComponent } from "./success-snack-bar/success-snack-bar.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  imports: [CommonModule, FontAwesomeModule],
  declarations: [SuccessSnackBarComponent],
  exports: [SuccessSnackBarComponent],
  entryComponents: [SuccessSnackBarComponent]
})
export class SuccessSnackBarModule {}
