import { PubgFeatures } from "./ds/pubg-features.enum";

export const FEATURES_TO_BE_REGISTERED = [
  PubgFeatures.KILL,
  PubgFeatures.REVIVED,
  PubgFeatures.DEATH,
  PubgFeatures.KILLER,
  PubgFeatures.MATCH,
  PubgFeatures.RANK,
  PubgFeatures.LOCATION,
  PubgFeatures.ME,
  PubgFeatures.TEAM,
  PubgFeatures.PHASE,
  PubgFeatures.MAP,
  PubgFeatures.ROSTER
];

export const ROSTER_KEY_PATTERN = /^roster_[0-9]{1,2}$/;

/**
 * This game id is different than the game id which is used in the manifest.
 * It must be 109061.
 */
export const PUBG_GAME_ID = 109061;
