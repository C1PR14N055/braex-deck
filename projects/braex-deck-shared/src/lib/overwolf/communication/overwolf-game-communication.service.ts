/// <reference path="../../../../../typings/overwolf.d.ts"/>
import { Injectable } from "@angular/core";
import GameInfo = overwolf.games.GameInfo;
import FeatureRegistration = overwolf.games.events.FeatureRegistration;
import InfoUpdate2 = overwolf.games.events.InfoUpdate2;
import GameEvents = overwolf.games.events.GameEvents;
import Info = overwolf.games.events.Info;
import { OverwolfError } from "../../error/overwolf.error";
import GameInfoUpdate = overwolf.games.GameInfoUpdate;

@Injectable({
  providedIn: "root"
})
export class OverwolfGameCommunicationService {
  getRunningGame(callback: (runningGameInfo: GameInfo) => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.getRunningGameInfo(callback);
    }
  }

  addOnGameLaunchedListener(callback: (payload?: GameInfo) => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.onGameLaunched.addListener(callback);
    }
  }

  removeOnGameLaunchedListener(callback: (payload?: GameInfo) => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.onGameLaunched.removeListener(callback);
    }
  }

  addErrorListener(callback: (payload?: any) => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.events.onError.addListener(callback);
    }
  }

  removeErrorListener(callback: (payload?: any) => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.events.onError.removeListener(callback);
    }
  }

  registerRequiredFeatures(
    features: string[],
    callback: (result: FeatureRegistration) => void
  ) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.events.setRequiredFeatures(features, callback);
    }
  }

  addGameInfoUpdates2Listener(callback: (payload?: InfoUpdate2) => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.events.onInfoUpdates2.addListener(callback);
    }
  }

  removeGameInfoUpdates2Listener(callback: (payload?: InfoUpdate2) => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.events.onInfoUpdates2.removeListener(callback);
    }
  }

  addNewEventsListener(callback: (payload?: GameEvents) => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.events.onNewEvents.addListener(callback);
    }
  }

  removeNewEventsListener(callback: (payload?: GameEvents) => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.events.onNewEvents.removeListener(callback);
    }
  }

  addOnGameInfoUpdatedListener(callback: (payload?: GameInfoUpdate) => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.onGameInfoUpdated.addListener(callback);
    }
  }

  removeOnGameInfoUpdatedListener(
    callback: (payload?: GameInfoUpdate) => void
  ) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.onGameInfoUpdated.removeListener(callback);
    }
  }

  getInfo(callback: (payload?: Info) => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.games.events.getInfo(info => {
        if (info.status === "success") {
          callback(info);
        } else {
          throw new OverwolfError("Failed to get the info");
        }
      });
    }
  }
}
