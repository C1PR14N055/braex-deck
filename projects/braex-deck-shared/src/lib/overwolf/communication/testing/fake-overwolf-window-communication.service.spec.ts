import MinimizeMaximizeEvent = overwolf.windows.MinimizeMaximizeEvent;
import WindowStateEvent = overwolf.windows.WindowStateEvent;
import ODKWindow = overwolf.windows.ODKWindow;
import RestoreEvent = overwolf.windows.RestoreEvent;

export class FakeOverwolfWindowCommunicationService {
  minimizeEventResult: MinimizeMaximizeEvent;
  maximizeEventResult: MinimizeMaximizeEvent;
  windowStateResult: WindowStateEvent;
  obtainDeclaredWindowResult: ODKWindow;
  restoreEventResult: RestoreEvent;
  getMainWindow = jasmine.createSpy("getMainWindow");

  close(windowId: string, callback: () => void) {
    setTimeout(() => {
      callback();
    }, 0);
  }

  // noinspection JSUnusedGlobalSymbols
  minimize(
    windowId: string,
    callback: (result: MinimizeMaximizeEvent) => void
  ) {
    setTimeout(() => {
      callback(this.minimizeEventResult);
    }, 0);
  }

  // noinspection JSUnusedGlobalSymbols
  maximize(
    windowId: string,
    callback: (result: MinimizeMaximizeEvent) => void
  ) {
    setTimeout(() => {
      callback(this.maximizeEventResult);
    }, 0);
  }

  // noinspection JSUnusedGlobalSymbols
  getWindowState(
    windowId: string,
    callback: (result: WindowStateEvent) => void
  ) {
    setTimeout(() => {
      callback(this.windowStateResult);
    }, 0);
  }

  // noinspection JSUnusedGlobalSymbols
  obtainDeclaredWindow(
    windowName: string,
    callback: (result: ODKWindow) => void
  ) {
    setTimeout(() => {
      callback(this.obtainDeclaredWindowResult);
    }, 0);
  }

  restore(windowId: string, callback: (result: RestoreEvent) => void) {
    setTimeout(() => {
      callback(this.restoreEventResult);
    }, 0);
  }

  // noinspection JSUnusedGlobalSymbols
  hide(windowId: string, callback: () => void) {
    setTimeout(() => {
      callback();
    }, 0);
  }
}
