/// <reference path="../../../../../../typings/overwolf.d.ts"/>
import GameEvents = overwolf.games.events.GameEvents;
import InfoUpdate2 = overwolf.games.events.InfoUpdate2;
import FeatureRegistration = overwolf.games.events.FeatureRegistration;
import { FEATURES_TO_BE_REGISTERED } from "../../constant";
import GameInfo = overwolf.games.GameInfo;
import { Injectable } from "@angular/core";
import Info = overwolf.games.events.Info;
import GameInfoUpdate = overwolf.games.GameInfoUpdate;

@Injectable()
export class FakeOverwolfGameCommunicationService {
  private _gameInfoUpdates2Callbacks: Array<
    (payload?: InfoUpdate2) => void
  > = [];
  private _newEventsCallbacks: Array<(payload?: GameEvents) => void> = [];
  private readonly _registration: FeatureRegistration = {
    status: "success",
    supportedFeatures: FEATURES_TO_BE_REGISTERED
  };
  private _onGameLaunchedCallbacks: Array<(payload?: GameInfo) => void> = [];
  private _onGameInfoUpdatedCallbacks: Array<
    (payload?: GameInfoUpdate) => void
  > = [];
  private _currentlyRunningGame: GameInfo;
  private _gameInfo: Info;

  // noinspection JSUnusedGlobalSymbols
  getRunningGame(callback: (runningGameInfo: GameInfo) => void) {
    setTimeout(() => {
      callback(this._currentlyRunningGame);
    }, 0);
  }

  // noinspection JSUnusedGlobalSymbols
  addOnGameLaunchedListener(callback: (payload?: GameInfo) => void) {
    this._onGameLaunchedCallbacks.push(callback);
  }

  // noinspection JSUnusedGlobalSymbols
  removeOnGameLaunchedListener(callback: (payload?: GameInfo) => void) {
    const index = this._onGameLaunchedCallbacks.indexOf(callback);
    this._onGameLaunchedCallbacks.splice(index, 0);
  }

  triggerGameLaunched(gameInfo: GameInfo) {
    this._onGameLaunchedCallbacks.forEach(x => {
      x(gameInfo);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  // tslint:disable-next-line:member-ordering
  addErrorListener = jasmine.createSpy("addErrorListener");
  // noinspection JSUnusedGlobalSymbols
  // tslint:disable-next-line:member-ordering
  removeErrorListener = jasmine.createSpy("removeErrorListener");
  // noinspection JSUnusedGlobalSymbols
  registerRequiredFeatures(
    features: string[],
    callback: (result: FeatureRegistration) => void
  ) {
    setTimeout(() => callback(this._registration), 0);
  }

  // noinspection JSUnusedGlobalSymbols
  addGameInfoUpdates2Listener(callback: (payload?: InfoUpdate2) => void) {
    this._gameInfoUpdates2Callbacks.push(callback);
  }

  // noinspection JSUnusedGlobalSymbols
  removeGameInfoUpdates2Listener(callback: (payload?: InfoUpdate2) => void) {
    const index = this._gameInfoUpdates2Callbacks.indexOf(callback);
    this._gameInfoUpdates2Callbacks.splice(index, 1);
  }

  triggerGameInfoUpdate2(info: InfoUpdate2) {
    this._gameInfoUpdates2Callbacks.forEach(x => {
      x(info);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  addNewEventsListener(callback: (payload?: GameEvents) => void) {
    this._newEventsCallbacks.push(callback);
  }

  // noinspection JSUnusedGlobalSymbols
  removeNewEventsListener(callback: (payload?: GameEvents) => void) {
    const index = this._newEventsCallbacks.indexOf(callback);
    this._newEventsCallbacks.splice(index, 1);
  }

  triggerNewEvent(gameEvents: GameEvents) {
    this._newEventsCallbacks.forEach(x => {
      x(gameEvents);
    });
  }

  triggerGameInfoUpdate(info: GameInfoUpdate) {
    this._onGameInfoUpdatedCallbacks.forEach(x => {
      x(info);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  addOnGameInfoUpdatedListener(callback: (payload?: GameInfoUpdate) => void) {
    this._onGameInfoUpdatedCallbacks.push(callback);
  }

  // noinspection JSUnusedGlobalSymbols
  removeOnGameInfoUpdatedListener(
    callback: (payload?: GameInfoUpdate) => void
  ) {
    const index = this._onGameInfoUpdatedCallbacks.indexOf(callback);
    this._onGameInfoUpdatedCallbacks.splice(index, 1);
  }

  // noinspection JSUnusedGlobalSymbols
  getInfo(callback: (payload?: Info) => void) {
    setTimeout(() => {
      callback(this._gameInfo);
    }, 0);
  }

  set currentlyRunningGame(value: GameInfo) {
    this._currentlyRunningGame = value;
  }

  get currentlyRunningGame() {
    return this._currentlyRunningGame;
  }

  set gameInfo(value: Info) {
    this._gameInfo = value;
  }

  get gameInfo() {
    return this._gameInfo;
  }
}
