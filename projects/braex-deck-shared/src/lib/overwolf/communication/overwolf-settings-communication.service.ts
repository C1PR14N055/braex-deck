/// <reference path="../../../../../typings/overwolf.d.ts"/>
import { Injectable } from "@angular/core";
import HotkeyRegistration = overwolf.settings.HotkeyRegistration;
import HotkeyRegistrationFailure = overwolf.settings.HotkeyRegistrationFailure;

@Injectable({ providedIn: "root" })
export class OverwolfSettingsCommunicationService {
  registerHotkey(
    actionId: string,
    callback: (result: HotkeyRegistrationFailure | HotkeyRegistration) => void
  ) {
    if (typeof overwolf !== "undefined") {
      overwolf.settings.registerHotKey(actionId, callback);
    }
  }
}
