/// <reference path="../../../../../typings/overwolf.d.ts"/>

import { Injectable } from "@angular/core";
import MinimizeMaximizeEvent = overwolf.windows.MinimizeMaximizeEvent;
import WindowStateEvent = overwolf.windows.WindowStateEvent;
import ODKWindow = overwolf.windows.ODKWindow;
import RestoreEvent = overwolf.windows.RestoreEvent;
import EventStatus = overwolf.games.EventStatus;
import StatusPayload = overwolf.windows.StatusPayload;

@Injectable({
  providedIn: "root"
})
export class OverwolfWindowCommunicationService {
  /**
   * {@see overwolf.windows#getMainWindow}
   */
  getMainWindow(): Window | null {
    if (typeof overwolf !== "undefined") {
      return overwolf.windows.getMainWindow();
    }

    return null;
  }

  /**
   * {@see overwolf.windows#close}
   */
  close(windowId: string, callback: () => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.windows.close(windowId, callback);
    }
  }

  /**
   * {@see overwolf.windows#minimize}
   */
  minimize(
    windowId: string,
    callback: (result: MinimizeMaximizeEvent) => void
  ) {
    if (typeof overwolf !== "undefined") {
      overwolf.windows.minimize(windowId, callback);
    }
  }

  /**
   * {@see overwolf.windows#maximize}
   */
  maximize(
    windowId: string,
    callback: (result: MinimizeMaximizeEvent) => void
  ) {
    if (typeof overwolf !== "undefined") {
      overwolf.windows.maximize(windowId, callback);
    }
  }

  /**
   * {@see overwolf.windows#getWindowState}
   */
  getWindowState(
    windowId: string,
    callback: (result: WindowStateEvent) => void
  ) {
    if (typeof overwolf !== "undefined") {
      overwolf.windows.getWindowState(windowId, callback);
    }
  }

  /**
   * {@see overwolf.windows#obtainDeclaredWindow}
   */
  obtainDeclaredWindow(
    windowName: string,
    callback: (result: ODKWindow) => void
  ) {
    if (typeof overwolf !== "undefined") {
      overwolf.windows.obtainDeclaredWindow(windowName, callback);
    }
  }

  /**
   * {@see overwolf.windows#restore}
   */
  restore(windowId: string, callback: (result: RestoreEvent) => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.windows.restore(windowId, callback);
    }
  }

  /**
   * {@see overwolf.windows#hide}
   */
  hide(windowId: string, callback: () => void) {
    if (typeof overwolf !== "undefined") {
      overwolf.windows.hide(windowId, callback);
    }
  }

  changePosition(
    windowId: string,
    left: number,
    top: number,
    callback: (payload: StatusPayload) => void
  ) {
    if (typeof overwolf !== "undefined") {
      overwolf.windows.changePosition(windowId, left, top, callback);
    }
  }
}
