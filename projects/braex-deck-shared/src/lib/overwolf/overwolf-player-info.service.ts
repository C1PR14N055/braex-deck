import { Injectable } from "@angular/core";
import { OverwolfGameCommunicationService } from "./communication/overwolf-game-communication.service";

@Injectable({
  providedIn: "root"
})
export class OverwolfPlayerInfoService {
  private _pubgNickname: string | null = null;

  constructor(private _gameCommunication: OverwolfGameCommunicationService) {
    this._gameCommunication.addGameInfoUpdates2Listener(info => {
      if (info.info.feature === "me" && info.info.me) {
        this._pubgNickname = info.info.me.name;
      }
    });
  }

  get pubgNickname() {
    return this._pubgNickname;
  }
}
