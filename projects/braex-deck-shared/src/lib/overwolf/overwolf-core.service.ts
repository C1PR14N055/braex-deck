/// <reference path="../../../../typings/overwolf.d.ts"/>
import { Injectable, OnDestroy } from "@angular/core";
import { Observable, Observer, ReplaySubject } from "rxjs";
import { OverwolfGameCommunicationService } from "./communication/overwolf-game-communication.service";
import { PUBG_GAME_ID } from "./constant";
import { OverwolfSettingsCommunicationService } from "./communication/overwolf-settings-communication.service";
import { OverwolfError } from "../error/overwolf.error";
import { OverwolfWindowCommunicationService } from "./communication/overwolf-window-communication.service";
import { WindowState } from "./ds/window-state.enum";
import GameInfo = overwolf.games.GameInfo;
import GameInfoUpdate = overwolf.games.GameInfoUpdate;
import { MappingError } from "../error/mapping.error";
import OverwolfWindowState = overwolf.windows.WindowState;
import { OverwolfWindow } from "./ds/overwolf-window";
import ODKWindow = overwolf.windows.ODKWindow;

@Injectable({
  providedIn: "root"
})
export class OverwolfCoreService implements OnDestroy {
  private _onPubgLaunchedSubject = new ReplaySubject<GameInfo>(1);
  private _onGameLaunchedCallbackRef: any;
  private _onGameInfoUpdatedCallbackRef: any;
  private _onPubgTerminatedObservers: Observer<void>[] = [];
  private _gameInfoUpdatedObservers: Observer<GameInfoUpdate>[] = [];

  constructor(
    private _gameCommunicationService: OverwolfGameCommunicationService,
    private _settingsCommunicationService: OverwolfSettingsCommunicationService,
    private _windowCommunication: OverwolfWindowCommunicationService
  ) {
    this._initGameRunningHooks();
    this._initGameUpdatedHooks();
    this._checkIfPubgAlreadyRunning();
  }

  ngOnDestroy() {
    this._removeGameRunningHooks();
    this._removeOnGameInfoUpdatedHooks();
  }

  private _initGameRunningHooks() {
    this._onGameLaunchedCallbackRef = this._onGameLaunched.bind(this);
    this._gameCommunicationService.addOnGameLaunchedListener(
      this._onGameLaunchedCallbackRef
    );
  }

  private _onGameLaunched(game?: GameInfo) {
    if (game && game.id === PUBG_GAME_ID) {
      this._onPubgLaunchedSubject.next(game);
    }
  }

  private _initGameUpdatedHooks() {
    this._onGameInfoUpdatedCallbackRef = this._onGameInfoUpdated.bind(this);
    this._gameCommunicationService.addOnGameInfoUpdatedListener(
      this._onGameInfoUpdatedCallbackRef
    );
  }

  private _onGameInfoUpdated(game?: GameInfoUpdate) {
    if (game && game.gameInfo && game.gameInfo.id === PUBG_GAME_ID) {
      this._triggerGameInfoUpdated(game);

      if (game.runningChanged && !game.gameInfo.isRunning) {
        this._triggerPubgTerminated();
      }
    }
  }

  private _checkIfPubgAlreadyRunning() {
    this._gameCommunicationService.getRunningGame(game => {
      if (game && game.id && game.id === PUBG_GAME_ID) {
        this._onPubgLaunchedSubject.next(game);
      }
    });
  }

  private _removeGameRunningHooks() {
    this._gameCommunicationService.removeOnGameLaunchedListener(
      this._onGameLaunchedCallbackRef
    );
  }

  private _removeOnGameInfoUpdatedHooks() {
    this._gameCommunicationService.removeOnGameInfoUpdatedListener(
      this._onGameInfoUpdatedCallbackRef
    );
  }

  /**
   * Check whether pubg has been launched.
   *
   * @returns An observable with the game info of pubg. When the subscriber subscribes
   * AFTER the game has been launched he will be notified with the initial game info ->
   * the game info is buffered.
   */
  onPubgLaunched(): Observable<GameInfo> {
    return this._onPubgLaunchedSubject.asObservable();
  }

  private _triggerGameInfoUpdated(info: GameInfoUpdate) {
    this._gameInfoUpdatedObservers.forEach(x => {
      x.next(info);
    });
  }

  onGameInfoUpdated(): Observable<GameInfoUpdate> {
    return Observable.create(observer => {
      this._gameInfoUpdatedObservers.push(observer);
    });
  }

  private _triggerPubgTerminated() {
    this._onPubgTerminatedObservers.forEach(x => {
      x.next(null);
    });
  }

  onPubgTerminated(): Observable<void> {
    return Observable.create(observer => {
      this._onPubgTerminatedObservers.push(observer);
    });
  }

  registerHotkey(action: string, callback: () => void) {
    this._settingsCommunicationService.registerHotkey(action, result => {
      if (result.status === "error") {
        console.error(
          `Failed to register the hotkey for the action \`${action}\``
        );
        console.error(result.error);
        throw new OverwolfError(`Failed to register the hotkey`);
      } else {
        callback();
      }
    });
  }

  getMainWindow() {
    return this._windowCommunication.getMainWindow();
  }

  /**
   * Close an overwolf window
   * @param win The name (from the overwolf manifest) or the id of the window.
   */
  closeWindow(win: string): Observable<void> {
    return Observable.create(observer => {
      this._windowCommunication.close(win, () => {
        observer.next(null);
      });
    });
  }

  /**
   * Minimize an overwolf window.
   * @param win The name (from the overwolf manifest) or the id of the window.
   */
  minimizeWindow(win: string): Observable<void> {
    return Observable.create(observer => {
      this._windowCommunication.minimize(win, event => {
        if (event.status === "success") {
          observer.next(null);
        } else {
          observer.error(
            new OverwolfError(`Failed to minimize the window \`${win}\``)
          );
        }
      });
    });
  }

  maximizeWindow(win: string): Observable<void> {
    return Observable.create(observer => {
      this._windowCommunication.maximize(win, event => {
        if (event.status === "success") {
          observer.next(null);
        } else {
          observer.error(
            new OverwolfError(`Failed to maximize the window \`${win}\``)
          );
        }
      });
    });
  }

  getWindowState(win: string): Observable<WindowState> {
    return Observable.create(observer => {
      this._windowCommunication.getWindowState(win, event => {
        if (event.status === "success") {
          let state: WindowState;
          try {
            state = this._convertOverwolfState(event.window_state_ex);
          } catch (e) {
            observer.error(e);

            return;
          }

          observer.next(state);
        } else {
          observer.error(
            new OverwolfError(
              `Failed to get the state of the window \`${win}\``
            )
          );
        }
      });
    });
  }

  private _convertOverwolfState(state: OverwolfWindowState) {
    let result: WindowState;
    switch (state) {
      case "normal":
        result = WindowState.NORMAL;
        break;
      case "closed":
        result = WindowState.CLOSED;
        break;
      case "hidden":
        result = WindowState.HIDDEN;
        break;
      case "maximized":
        result = WindowState.MAXIMIZED;
        break;
      case "minimized":
        result = WindowState.MINIMIZED;
        break;
      default:
        throw new MappingError(`Unknown window state \`${state}\``);
    }

    return result;
  }

  createWindow(win: string): Observable<OverwolfWindow> {
    return Observable.create(observer => {
      this._windowCommunication.obtainDeclaredWindow(win, event => {
        if (event.status === "success") {
          let result: OverwolfWindow;
          try {
            result = this._convertOverwolfWindow(event);
          } catch (e) {
            observer.error(e);

            return;
          }

          observer.next(result);
        } else {
          observer.error(
            new OverwolfError(`Failed to open the window \`${win}\``)
          );
        }
      });
    });
  }

  private _convertOverwolfWindow(win: ODKWindow) {
    const result = new OverwolfWindow();
    result.id = win.window.id;
    result.name = win.window.name;
    result.width = win.window.width;
    result.height = win.window.height;
    result.top = win.window.top;
    result.left = win.window.left;
    result.visible = win.window.isVisible;
    result.parent = win.window.parent;
    result.state = this._convertOverwolfState(win.window.stateEx);

    return result;
  }

  showWindow(win: string): Observable<void> {
    return Observable.create(observer => {
      this._windowCommunication.restore(win, event => {
        if (event.status === "success") {
          observer.next(null);
        } else {
          observer.error(
            new OverwolfError(
              `Failed to restore the state of the window \`${win}\``
            )
          );
        }
      });
    });
  }

  hideWindow(win: string): Observable<void> {
    return Observable.create(observer => {
      this._windowCommunication.hide(win, () => {
        observer.next(null);
      });
    });
  }

  changeWindowPosition(win: string, left: number, top: number) {
    this._windowCommunication.changePosition(win, left, top, result => {
      if (result.status === "error") {
        throw new OverwolfError(
          `Failed to change the position of the window \`${win}\``
        );
      }
    });
  }
}
