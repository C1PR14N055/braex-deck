import { Observable, Observer } from "rxjs";
import GameInfo = overwolf.games.GameInfo;
import { WindowState } from "../ds/window-state.enum";
import { OverwolfWindow } from "../ds/overwolf-window";

export class FakeOverwolfCoreService {
  mainWindow: Window;
  windowState: WindowState;
  openOverwolfWindow: OverwolfWindow;
  private _onPubgLaunchedObservers: Observer<GameInfo>[] = [];
  private _onPubgTerminatedObservers: Observer<void>[] = [];
  private _registeredHotkeys = new Map<string, () => void>();

  // noinspection JSUnusedGlobalSymbols
  triggerPubgLaunched(info: GameInfo) {
    this._onPubgLaunchedObservers.forEach(x => {
      x.next(info);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  onPubgLaunched(): Observable<GameInfo> {
    return Observable.create(observer => {
      this._onPubgLaunchedObservers.push(observer);
    });
  }

  triggerPubgTerminated() {
    this._onPubgTerminatedObservers.forEach(x => {
      x.next(null);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  onPubgTerminated(): Observable<void> {
    return Observable.create(observer => {
      this._onPubgTerminatedObservers.push(observer);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  registerHotkey(action: string, callback: () => void) {
    this._registeredHotkeys.set(action, callback);
  }

  hasHotkey(action: string) {
    return this._registeredHotkeys.has(action);
  }

  triggerHotkey(action: string) {
    if (!this._registeredHotkeys.has(action)) {
      throw new Error(`No hotkey for the action \`${action}\` registered`);
    }

    const callback = this._registeredHotkeys.get(action);
    callback();
  }

  // noinspection JSUnusedGlobalSymbols
  getMainWindow() {
    return this.mainWindow;
  }

  // noinspection JSUnusedGlobalSymbols
  closeWindow(win: string): Observable<void> {
    return Observable.create(observer => {
      setTimeout(() => {
        observer.next(null);
      }, 50);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  minimizeWindow(win: string): Observable<void> {
    return Observable.create(observer => {
      setTimeout(() => {
        observer.next(null);
      }, 50);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  maximizeWindow(win: string): Observable<void> {
    return Observable.create(observer => {
      setTimeout(() => {
        observer.next(null);
      }, 50);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  getWindowState(win: string): Observable<WindowState> {
    return Observable.create(observer => {
      setTimeout(() => {
        observer.next(this.windowState);
      }, 50);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  createWindow(win: string): Observable<OverwolfWindow> {
    return Observable.create(observer => {
      setTimeout(() => {
        observer.next(this.openOverwolfWindow);
      }, 50);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  showWindow(win: string): Observable<void> {
    return Observable.create(observer => {
      setTimeout(() => {
        observer.next(null);
      }, 50);
    });
  }

  hideWindow(win: string): Observable<void> {
    return Observable.create(observer => {
      setTimeout(() => {
        observer.next(null);
      }, 50);
    });
  }
}
