import { Observable, Observer } from "rxjs";
import { GameMode } from "../../ds/game/game-mode.enum";
import { List } from "immutable";
import { GamePhase } from "../../ds/game/game-phase.enum";
import { PubgMap } from "../../ds/game/pubg-map.enum";
import { Vector3 } from "../../map-canvas/ds/vector3";

export class FakeOverwolfGameEventsService {
  private _onPlayerJoinObservers: Observer<string>[] = [];
  private _onPlayerLeftObservers: Observer<string>[] = [];
  private _currentGameModeObserver: Observer<GameMode>[] = [];
  private _onMatchStartObservers: Observer<void>[] = [];
  private _onMatchCompletedObservers: Observer<void>[] = [];
  private _onMatchLeftObservers: Observer<void>[] = [];
  private _localPlayerNameObservers: Observer<string>[] = [];
  private _playerTeamNicknamesObservers: Observer<List<string>>[] = [];
  private _onPhaseChangedObservers: Observer<GamePhase>[] = [];
  private _currentMapObservers: Observer<PubgMap>[] = [];
  private _localPlayerLocationObservers: Observer<Vector3>[] = [];
  private _localPlayerDiedObservers: Observer<void>[] = [];

  triggerPhaseChanged(phase: GamePhase) {
    this._onPhaseChangedObservers.forEach(x => {
      x.next(phase);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  onPhaseChanged(): Observable<GamePhase> {
    return Observable.create(observer => {
      this._onPhaseChangedObservers.push(observer);
    });
  }

  triggerPlayerJoin(playerName: string) {
    this._onPlayerJoinObservers.forEach(x => {
      x.next(playerName);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  onPlayerJoin(): Observable<string> {
    return Observable.create(observer => {
      this._onPlayerJoinObservers.push(observer);
    });
  }

  triggerPlayerLeft(playerName: string) {
    this._onPlayerLeftObservers.forEach(x => {
      x.next(playerName);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  onPlayerLeft(): Observable<string> {
    return Observable.create(observer => {
      this._onPlayerLeftObservers.push(observer);
    });
  }

  triggerCurrentGameMode(gameMode: GameMode) {
    this._currentGameModeObserver.forEach(x => {
      x.next(gameMode);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  currentGameMode(): Observable<GameMode> {
    return Observable.create(observer => {
      this._currentGameModeObserver.push(observer);
    });
  }

  triggerMatchStart() {
    this._onMatchStartObservers.forEach(x => {
      x.next(null);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  onMatchStart(): Observable<void> {
    return Observable.create(observer => {
      this._onMatchStartObservers.push(observer);
    });
  }

  triggerMatchCompleted() {
    this._onMatchCompletedObservers.forEach(x => {
      x.next(null);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  onMatchCompleted(): Observable<void> {
    return Observable.create(observer => {
      this._onMatchCompletedObservers.push(observer);
    });
  }

  triggerMatchLeft() {
    this._onMatchLeftObservers.forEach(x => {
      x.next(null);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  onMatchLeft() {
    return Observable.create(observer => {
      this._onMatchLeftObservers.push(observer);
    });
  }

  triggerLocalPlayerName(name: string) {
    this._localPlayerNameObservers.forEach(x => {
      x.next(name);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  localPlayerName(): Observable<string> {
    return Observable.create(observer => {
      this._localPlayerNameObservers.push(observer);
    });
  }

  triggerPlayerTeamNicknames(team: string[]) {
    const nicknames = List(team);
    this._playerTeamNicknamesObservers.forEach(x => {
      x.next(nicknames);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  playerTeamNicknames(): Observable<List<string>> {
    return Observable.create(observer => {
      this._playerTeamNicknamesObservers.push(observer);
    });
  }

  triggerCurrentMap(map: PubgMap) {
    this._currentMapObservers.forEach(x => {
      x.next(map);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  currentMap(): Observable<PubgMap> {
    return Observable.create(observer => {
      this._currentMapObservers.push(observer);
    });
  }

  triggerLocalPlayerLocationChanged(vector: Vector3) {
    this._localPlayerLocationObservers.forEach(x => {
      x.next(vector);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  onLocalPlayerLocationChanged(): Observable<Vector3> {
    return Observable.create(observer => {
      this._localPlayerLocationObservers.push(observer);
    });
  }

  triggerLocalPlayerDied() {
    this._localPlayerDiedObservers.forEach(x => {
      x.next(null);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  onLocalPlayerDied(): Observable<void> {
    return Observable.create(observer => {
      this._localPlayerDiedObservers.push(observer);
    });
  }
}
