/// <reference path="../../../../typings/overwolf.d.ts"/>
import { Injectable, OnDestroy } from "@angular/core";
import { OverwolfCoreService } from "./overwolf-core.service";
import { Observable, Observer, ReplaySubject, Subscription } from "rxjs";
import { Vector3 } from "../map-canvas/ds/vector3";
import { GamePhase } from "../ds/game/game-phase.enum";
import { PubgMap } from "../ds/game/pubg-map.enum";
import { GameMode } from "../ds/game/game-mode.enum";
import { OverwolfGameCommunicationService } from "./communication/overwolf-game-communication.service";
import { MappingError } from "../error/mapping.error";
import { List, Set } from "immutable";
import { FEATURES_TO_BE_REGISTERED, ROSTER_KEY_PATTERN } from "./constant";
import InfoUpdate2 = overwolf.games.events.InfoUpdate2;
import GameInfo = overwolf.games.events.GameInfo;
import MatchInfo = overwolf.games.events.MatchInfo;
import LocationInfo = overwolf.games.events.pubg.info.LocationInfo;
import PhaseInfo = overwolf.games.events.pubg.info.PhaseInfo;
import OverwolfGamePhase = overwolf.games.events.pubg.info.GamePhase;
import GameEvents = overwolf.games.events.GameEvents;
import MapInfo = overwolf.games.events.pubg.info.MapInfo;
import OverwolfPubgMap = overwolf.games.events.pubg.info.Map;
import MatchInfoMatch = overwolf.games.events.pubg.info.MatchInfo;
import OverwolfGameMode = overwolf.games.events.pubg.info.MatchMode;
import RosterPlayerStats = overwolf.games.events.pubg.info.RosterPlayerStats;
import DeathEvent = overwolf.games.events.pubg.event.DeathEvent;
import KillerEvent = overwolf.games.events.pubg.event.KillerEvent;
import MatchEvent = overwolf.games.events.pubg.event.MatchEvent;
import KillEvent = overwolf.games.events.pubg.event.KillEvent;
import RevivedEvent = overwolf.games.events.pubg.event.RevivedEvent;
import TeamInfo = overwolf.games.events.pubg.info.TeamInfo;
import RankInfo = overwolf.games.events.pubg.info.RankInfo;
import MeInfo = overwolf.games.events.MeInfo;
import { GameEventsWrapper } from "./ds/game-events-wrapper";
import { OverwolfError } from "../error/overwolf.error";
import Info = overwolf.games.events.Info;

@Injectable({
  providedIn: "root"
})
export class OverwolfGameEventsService implements OnDestroy {
  private _subs: Subscription[] = [];
  private _onPlayerLocationChangedObservers: Observer<Vector3>[] = [];
  private _onPhaseChangedObservers: Observer<GamePhase>[] = [];
  private _onOtherPlayerKilledObservers: Observer<void>[] = [];
  private _onOtherPlayerKnockedOutObservers: Observer<void>[] = [];
  private _onOtherPlayerHeadshotObservers: Observer<void>[] = [];
  private _onDamageDealtObservers: Observer<number>[] = [];
  private _playerJoinObservers: Observer<string>[] = [];
  private _onLocalPlayerDiedObservers: Observer<void>[] = [];
  private _onLocalPlayerRevivedObservers: Observer<void>[] = [];
  private _onLocalPlayerKnockedOutObservers: Observer<void>[] = [];
  private _onMatchStartObservers: Observer<void>[] = [];
  private _onMatchCompletedObservers: Observer<void>[] = [];
  private _onMatchLeftObservers: Observer<void>[] = [];
  private _currentMapObservers: Observer<PubgMap>[] = [];
  private _currentGameModeObservers: Observer<GameMode>[] = [];
  private _playerKillerObservers: Observer<string>[] = [];
  private _playerTeamNicknamesObservers: Observer<List<string>>[] = [];
  private _totalTeamsObservers: Observer<number>[] = [];
  private _playerLeftObservers: Observer<string>[] = [];
  private _localPlayerNameSubject = new ReplaySubject<string>(1);
  private _eventsErrorCallbackRef: () => void;
  private _gameInfoCallbackRef: () => void;
  private _gameEventCallbackRef: () => void;
  private _cachedGamePhase: GamePhase | null = null;
  private _cachedMap: PubgMap | null = null;
  private _cachedPlayerJoins = Set<string>();
  private _cachedPlayerLeaves = Set<string>();
  private _cachedTotalTeams: number | null = null;
  private _cachedPlayerTeamNicknames: List<string> | null = null;
  private _cachedGameMode: GameMode | null = null;
  private _cachedMatchStart = false;
  private _cachedLocalPlayerDied = false;
  /**
   * Whether the player is currently in an ongoing game.
   */
  private _currentlyInGame = false;

  constructor(
    private _overwolfCoreService: OverwolfCoreService,
    private _gameCommService: OverwolfGameCommunicationService
  ) {
    let sub = this._overwolfCoreService.onPubgLaunched().subscribe(() => {
      this._initGameEventListeners();
    });
    this._subs.push(sub);

    sub = this._overwolfCoreService.onPubgTerminated().subscribe(() => {
      this._clearCache();
    });
    this._subs.push(sub);

    sub = this.onPhaseChanged().subscribe(phase => {
      if (phase === GamePhase.LOBBY) {
        if (this._currentlyInGame) {
          this._currentlyInGame = false;
          this.triggerMatchLeft();
        }
      } else {
        this._currentlyInGame = true;
      }
    });
    this._subs.push(sub);

    sub = this.onMatchLeft().subscribe(() => {
      this._clearCache();
    });
    this._subs.push(sub);

    setTimeout(() => {
      this._triggerCurrentMap(PubgMap.ERANGEL_MAIN);
    }, 1000);
  }

  private _clearCache() {
    this._cachedGamePhase = null;
    this._cachedMap = null;
    this._cachedTotalTeams = null;
    this._cachedPlayerTeamNicknames = null;
    this._cachedGameMode = null;
    this._cachedMatchStart = false;
    this._cachedLocalPlayerDied = false;
    this._cachedPlayerJoins = this._cachedPlayerJoins.clear();
    this._cachedPlayerLeaves = this._cachedPlayerLeaves.clear();
  }

  private _initGameEventListeners() {
    this._registerFeaturesToObserve().subscribe(() => {
      console.log("Registering game events");
      this._registerErrorNotifications();
      this._registerGameInfoObserver();
      this._registerGameEventsObserver();
      this._triggerAlreadyAvailableEvents();
    });
  }

  private _registerErrorNotifications() {
    this._eventsErrorCallbackRef = this._overwolfEventsErrorHandler.bind(this);
    this._gameCommService.addErrorListener(this._eventsErrorCallbackRef);
  }

  private _unregisterErrorNotifications() {
    this._gameCommService.removeErrorListener(this._eventsErrorCallbackRef);
  }

  private _overwolfEventsErrorHandler(error1: any) {
    console.error(
      `Received an error from the overwolf event system -> ${JSON.stringify(
        error1
      )}`
    );
  }

  private _registerFeaturesToObserve(): Observable<void> {
    return Observable.create(observer => {
      this._gameCommService.registerRequiredFeatures(
        FEATURES_TO_BE_REGISTERED,
        registration => {
          if (registration.status === "success") {
            console.log(
              `Registered features successfully -> ${JSON.stringify(
                registration.supportedFeatures
              )}`
            );
            observer.next(null);
            observer.complete();
          } else {
            console.warn(
              "Failed to register features... Trying again in 2 seconds"
            );
            setTimeout(() => {
              const sub = this._registerFeaturesToObserve().subscribe(() => {
                observer.next(null);
                observer.complete();
              });
              this._subs.push(sub);
            }, 2000);
          }
        }
      );
    });
  }

  private _registerGameInfoObserver() {
    this._gameInfoCallbackRef = this._gameInfoHandler.bind(this);
    this._gameCommService.addGameInfoUpdates2Listener(
      this._gameInfoCallbackRef
    );
  }

  private _unregisterGameInfoObserver() {
    this._gameCommService.removeGameInfoUpdates2Listener(
      this._gameInfoCallbackRef
    );
  }

  private _gameInfoHandler(info: InfoUpdate2) {
    if (info && info.info) {
      if ((<GameInfo>info.info).game_info) {
        this._processGameInfo(<GameInfo>info.info);
      } else if ((<MatchInfo>info.info).match_info) {
        this._processMatchInfo(<MatchInfo>info.info);
      } else if ((<MeInfo>info.info).me) {
        this._processMeInfo(<MeInfo>info.info);
      }
    }
  }

  private _processGameInfo(gameInfo: GameInfo) {
    if ((<LocationInfo>gameInfo.game_info).location) {
      const location = JSON.parse((<LocationInfo>gameInfo.game_info).location);
      const point = Vector3.create(location.x, location.y, location.z);
      this._triggerLocalPlayerLocationChanged(point);
    } else if ((<PhaseInfo>gameInfo.game_info).phase) {
      const phase = this._convertOverwolfGamePhase(
        (<PhaseInfo>gameInfo.game_info).phase
      );
      this._triggerPhaseChanged(phase);
    }
  }

  private _convertOverwolfGamePhase(phase: OverwolfGamePhase) {
    switch (phase) {
      case "aircraft":
        return GamePhase.AIRCRAFT;
      case "airfield":
        return GamePhase.AIRFIELD;
      case "freefly":
        return GamePhase.FREE_FLY;
      case "loading_screen":
        return GamePhase.LOADING_SCREEN;
      case "lobby":
        return GamePhase.LOBBY;
      case "landed":
        return GamePhase.LANDED;
    }

    throw new MappingError(
      `No mapping found for the OverwolfGamePhase \`${phase}\``
    );
  }

  private _processMatchInfo(matchInfo: MatchInfo) {
    if ((<MapInfo>matchInfo.match_info).map) {
      const map = this._convertOverwolfMap((<MapInfo>matchInfo.match_info).map);
      this._triggerCurrentMap(map);
    } else if ((<MatchInfoMatch>matchInfo.match_info).mode) {
      const mode = this._convertOverwolfGameMode(
        (<MatchInfoMatch>matchInfo.match_info).mode
      );
      this._triggerCurrentGameMode(mode);
    } else if ((<TeamInfo>matchInfo.match_info).nicknames) {
      const teamInfo = <TeamInfo>matchInfo.match_info;
      if (teamInfo.nicknames) {
        const nicknames = JSON.parse(teamInfo.nicknames);
        if (nicknames.team_members) {
          const teamNicknames = nicknames.team_members.map(x => x.player);
          this._triggerPlayerTeamNicknames(teamNicknames);
        }
      }
    } else if ((<RankInfo>matchInfo.match_info).total_teams) {
      const totalTeams = (<RankInfo>matchInfo.match_info).total_teams;
      this._triggerTotalTeams(totalTeams);
    } else {
      const matchingKeys = Object.keys(matchInfo.match_info).filter(x =>
        ROSTER_KEY_PATTERN.test(x)
      );
      if (matchingKeys.length) {
        matchingKeys.forEach(key => {
          let roster: RosterPlayerStats;
          if (typeof matchInfo.match_info[key] === "string") {
            roster = JSON.parse(matchInfo.match_info[key]) as RosterPlayerStats;
          } else {
            roster = matchInfo.match_info[key] as RosterPlayerStats;
          }

          if (roster && !roster.out && roster.player) {
            this._triggerPlayerJoin(roster.player);
          } else if (roster && roster.out && roster.player) {
            this._triggerPlayerLeft(roster.player);
          }
        });
      }
    }
  }

  private _convertOverwolfMap(map: OverwolfPubgMap) {
    switch (map) {
      case "Desert_Main":
        return PubgMap.DESERT_MAIN;
      case "DihorOtok_Main":
        return PubgMap.DIHOROTOK_MAIN;
      case "Erangel_Main":
        return PubgMap.ERANGEL_MAIN;
      case "Range_Main":
        return PubgMap.RANGE_MAIN;
      case "Savage_Main":
        return PubgMap.SAVAGE_MAIN;
    }

    throw new MappingError(
      `No mapping found for the OverwolfPubgMap \`${map}\``
    );
  }

  private _convertOverwolfGameMode(mode: OverwolfGameMode) {
    switch (mode) {
      case "solo":
        return GameMode.SOLO;
      case "duo":
        return GameMode.DUO;
      case "squad":
        return GameMode.SQUAD;
    }

    throw new MappingError(
      `No mapping found for the OverwolfGameMode \`${mode}\``
    );
  }

  private _processMeInfo(info: MeInfo) {
    if (info.me && info.me.name) {
      this._triggerLocalPlayerName(info.me.name);
    }
  }

  private _registerGameEventsObserver() {
    this._gameEventCallbackRef = this._gameEventHandler.bind(this);
    this._gameCommService.addNewEventsListener(this._gameEventCallbackRef);
  }

  private _unregisterGameEventsObserver() {
    this._gameCommService.removeNewEventsListener(this._gameEventCallbackRef);
  }

  private _triggerAlreadyAvailableEvents() {
    console.log("Triggering already available events");
    this.availableGameEvents().subscribe(events => {
      if (!events) {
        return;
      }

      if (events.totalTeams) {
        this._triggerTotalTeams(events.totalTeams);
      }
      if (events.nickname) {
        this._triggerLocalPlayerName(events.nickname);
      }
      if (events.teamNicknames) {
        this._triggerPlayerTeamNicknames(events.teamNicknames.toArray());
      }
      if (events.mode) {
        this._triggerCurrentGameMode(events.mode);
      }
      if (events.map) {
        this._triggerCurrentMap(events.map);
      }
      if (events.phase) {
        this._triggerPhaseChanged(events.phase);
      }
    });
  }

  private _gameEventHandler(event: GameEvents) {
    if (event && event.events && event.events.length) {
      event.events.forEach(ev => {
        if ((<DeathEvent>ev).death === null) {
          this._triggerLocalPlayerDied();
        } else if ((<DeathEvent>ev).knockedout === null) {
          this._triggerLocalPlayerKnockedOut();
        } else if ((<KillerEvent>ev).killer) {
          this._triggerLocalPlayerKiller((<KillerEvent>ev).killer);
        } else if ((<MatchEvent>ev).name) {
          switch ((<MatchEvent>ev).name) {
            case "matchStart":
              this._triggerMatchStart();
              break;
            case "matchEnd":
              this._triggerMatchCompleted();
              break;
          }
        } else if ((<KillEvent>ev).kill === null) {
          this._triggerOtherPlayerKilled();
        } else if ((<KillEvent>ev).headshot === null) {
          this._triggerOtherPlayerHeadshot();
        } else if ((<KillEvent>ev).knockout === null) {
          this._triggerOtherPlayerKnockedOut();
        } else if ((<KillEvent>ev).damage_dealt) {
          this._triggerDamageDealt((<KillEvent>ev).damage_dealt);
        } else if ((<RevivedEvent>ev).revived === null) {
          this._triggerLocalPlayerRevived();
        }
      });
    }
  }

  private _triggerLocalPlayerLocationChanged(location: Vector3) {
    this._onPlayerLocationChangedObservers.forEach(x => {
      x.next(location);
    });
  }

  onLocalPlayerLocationChanged(): Observable<Vector3> {
    return Observable.create(observer => {
      this._onPlayerLocationChangedObservers.push(observer);
    });
  }

  private _triggerPhaseChanged(phase: GamePhase) {
    this._cachedGamePhase = phase;
    this._onPhaseChangedObservers.forEach(x => {
      x.next(phase);
    });
  }

  onPhaseChanged(): Observable<GamePhase> {
    return Observable.create(observer => {
      this._onPhaseChangedObservers.push(observer);

      if (this._cachedGamePhase) {
        observer.next(this._cachedGamePhase);
      }
    });
  }

  private _triggerOtherPlayerKilled() {
    this._onOtherPlayerKilledObservers.forEach(x => {
      x.next(null);
    });
  }

  onOtherPlayerKilled(): Observable<void> {
    return Observable.create(observer => {
      this._onOtherPlayerKilledObservers.push(observer);
    });
  }

  private _triggerOtherPlayerKnockedOut() {
    this._onOtherPlayerKnockedOutObservers.forEach(x => {
      x.next(null);
    });
  }

  onOtherPlayerKnockedOut(): Observable<void> {
    return Observable.create(observer => {
      this._onOtherPlayerKnockedOutObservers.push(observer);
    });
  }

  private _triggerOtherPlayerHeadshot() {
    this._onOtherPlayerHeadshotObservers.forEach(x => {
      x.next(null);
    });
  }

  onOtherPlayerHeadshot(): Observable<void> {
    return Observable.create(observer => {
      this._onOtherPlayerHeadshotObservers.push(observer);
    });
  }

  private _triggerDamageDealt(damage: number) {
    this._onDamageDealtObservers.forEach(x => {
      x.next(damage);
    });
  }

  /**
   * The player dealt damage to an enemy player or to himself.
   */
  onDamageDealt(): Observable<number> {
    return Observable.create(observer => {
      this._onDamageDealtObservers.push(observer);
    });
  }

  private _triggerPlayerJoin(playerName: string) {
    this._cachedPlayerJoins = this._cachedPlayerJoins.add(playerName);
    this._playerJoinObservers.forEach(x => {
      x.next(playerName);
    });
  }

  onPlayerJoin(): Observable<string> {
    return Observable.create(observer => {
      this._playerJoinObservers.push(observer);

      if (!this._cachedPlayerJoins.isEmpty()) {
        this._cachedPlayerJoins.forEach(nickname => {
          observer.next(nickname);
        });
      }
    });
  }

  private _triggerLocalPlayerDied() {
    this._cachedLocalPlayerDied = true;
    this._onLocalPlayerDiedObservers.forEach(x => {
      x.next(null);
    });
  }

  onLocalPlayerDied(): Observable<void> {
    return Observable.create(observer => {
      this._onLocalPlayerDiedObservers.push(observer);

      if (this._cachedLocalPlayerDied) {
        observer.next(null);
      }
    });
  }

  private _triggerLocalPlayerRevived() {
    this._onLocalPlayerRevivedObservers.forEach(x => {
      x.next(null);
    });
  }

  onLocalPlayerRevived(): Observable<void> {
    return Observable.create(observer => {
      this._onLocalPlayerRevivedObservers.push(observer);
    });
  }

  private _triggerLocalPlayerKnockedOut() {
    this._onLocalPlayerKnockedOutObservers.forEach(x => {
      x.next(null);
    });
  }

  onLocalPlayerKnockedOut(): Observable<void> {
    return Observable.create(observer => {
      this._onLocalPlayerKnockedOutObservers.push(observer);
    });
  }

  private _triggerMatchStart() {
    this._cachedMatchStart = true;
    this._onMatchStartObservers.forEach(x => {
      x.next(null);
    });
  }

  onMatchStart(): Observable<void> {
    return Observable.create(observer => {
      this._onMatchStartObservers.push(observer);

      if (this._cachedMatchStart) {
        observer.next(null);
      }
    });
  }

  private _triggerMatchCompleted() {
    this._onMatchCompletedObservers.forEach(x => {
      x.next(null);
    });
  }

  /**
   * Event emitted when the match has ended i.e.
   * all enemy teams have been eliminated and the end screen was visible.
   */
  onMatchCompleted(): Observable<void> {
    return Observable.create(observer => {
      this._onMatchCompletedObservers.push(observer);
    });
  }

  triggerMatchLeft() {
    this._onMatchLeftObservers.forEach(x => {
      x.next(null);
    });
  }

  /**
   * Event emitted when the player returns from a match to the lobby
   * independent if he left the game or if it ended.
   */
  onMatchLeft(): Observable<void> {
    return Observable.create(observer => {
      this._onMatchLeftObservers.push(observer);
    });
  }

  private _triggerCurrentMap(map: PubgMap) {
    this._cachedMap = map;
    this._currentMapObservers.forEach(x => {
      x.next(map);
    });
  }

  currentMap(): Observable<PubgMap> {
    return Observable.create(observer => {
      this._currentMapObservers.push(observer);

      if (this._cachedMap) {
        observer.next(this._cachedMap);
      }
    });
  }

  private _triggerCurrentGameMode(gameMode: GameMode) {
    this._cachedGameMode = gameMode;
    this._currentGameModeObservers.forEach(x => {
      x.next(gameMode);
    });
  }

  currentGameMode(): Observable<GameMode> {
    return Observable.create(observer => {
      this._currentGameModeObservers.push(observer);

      if (this._cachedGameMode) {
        observer.next(this._cachedGameMode);
      }
    });
  }

  private _triggerLocalPlayerKiller(playerName: string) {
    this._playerKillerObservers.forEach(x => {
      x.next(playerName);
    });
  }

  localPlayerKiller(): Observable<string> {
    return Observable.create(observer => {
      this._playerKillerObservers.push(observer);
    });
  }

  private _triggerPlayerTeamNicknames(nicknames: string[]) {
    const nn = List(nicknames);
    this._cachedPlayerTeamNicknames = nn;
    this._playerTeamNicknamesObservers.forEach(x => {
      x.next(nn);
    });
  }

  /**
   * This event is only fired when the game mode is {@link GameMode.DUO} or {@link GameMode.SQUAD}.
   */
  playerTeamNicknames(): Observable<List<string>> {
    return Observable.create(observer => {
      this._playerTeamNicknamesObservers.push(observer);

      if (this._cachedPlayerTeamNicknames) {
        observer.next(this._cachedPlayerTeamNicknames);
      }
    });
  }

  private _triggerTotalTeams(total: number) {
    this._cachedTotalTeams = total;
    this._totalTeamsObservers.forEach(x => {
      x.next(total);
    });
  }

  /**
   * The total teams count when the match starts.
   */
  totalTeams(): Observable<number> {
    return Observable.create(observer => {
      this._totalTeamsObservers.push(observer);

      if (this._cachedTotalTeams) {
        observer.next(this._cachedTotalTeams);
      }
    });
  }

  private _triggerPlayerLeft(nickname: string) {
    this._cachedPlayerLeaves = this._cachedPlayerLeaves.add(nickname);
    this._playerLeftObservers.forEach(x => {
      x.next(nickname);
    });
  }

  /**
   * This event is fired when a player leaves the game or dies.
   *
   * @returns An observable with the name of the player that has left the game.
   */
  onPlayerLeft(): Observable<string> {
    return Observable.create(observer => {
      this._playerLeftObservers.push(observer);

      if (!this._cachedPlayerLeaves.isEmpty()) {
        this._cachedPlayerLeaves.forEach(nickname => {
          observer.next(nickname);
        });
      }
    });
  }

  private _triggerLocalPlayerName(name: string) {
    this._localPlayerNameSubject.next(name);
  }

  /**
   * Get the name of the local player.
   * IMPORTANT: This event is only received in the {@link GamePhase.AIRCRAFT} phase.
   * @returns An observable with the nickname of the local player. The observable emits the nickname every
   * time it is subscribed to i.e. the nickname is buffered.
   */
  localPlayerName(): Observable<string> {
    return this._localPlayerNameSubject.asObservable();
  }

  /**
   * Get all game events which are available to this time.
   */
  availableGameEvents(): Observable<GameEventsWrapper | null> {
    return Observable.create(observer => {
      this._gameCommService.getInfo(info => {
        if (info.status === "error") {
          observer.error(
            new OverwolfError(`Failed to get the currently available events`)
          );
        } else {
          observer.next(this._convertAvailableEvents(info));
        }
      });
    });
  }

  private _convertAvailableEvents(events: Info) {
    if (
      typeof events === "undefined" ||
      (events && typeof events.res === "undefined")
    ) {
      return;
    }

    const result = new GameEventsWrapper();
    if (
      typeof events.res.game_info !== "undefined" &&
      typeof events.res.game_info.phase !== "undefined" &&
      events.res.game_info.phase
    ) {
      result.phase = this._convertOverwolfGamePhase(events.res.game_info.phase);
    }
    if (
      typeof events.res.me !== "undefined" &&
      typeof events.res.me.name !== "undefined" &&
      events.res.me.name
    ) {
      result.nickname = events.res.me.name;
    }
    if (typeof events.res.match_info !== "undefined") {
      if (
        typeof events.res.match_info.kills !== "undefined" &&
        events.res.match_info.kills
      ) {
        result.kills = Number(events.res.match_info.kills);
      }

      if (
        typeof events.res.match_info.headshots !== "undefined" &&
        events.res.match_info.headshots
      ) {
        result.headshots = Number(events.res.match_info.headshots);
      }

      if (
        typeof events.res.match_info.total_damage_dealt !== "undefined" &&
        events.res.match_info.total_damage_dealt
      ) {
        result.totalDamageDealt = Number(
          events.res.match_info.total_damage_dealt
        );
      }

      if (
        typeof events.res.match_info.max_kill_distance !== "undefined" &&
        events.res.match_info.max_kill_distance
      ) {
        result.maxKillDistance = Number(
          events.res.match_info.max_kill_distance
        );
      }

      if (
        typeof events.res.match_info.total_teams !== "undefined" &&
        events.res.match_info.total_teams
      ) {
        result.totalTeams = Number(events.res.match_info.total_teams);
      }

      if (
        typeof events.res.match_info.me !== "undefined" &&
        events.res.match_info.me
      ) {
        result.localPlayerRank = Number(events.res.match_info.me);
      }

      if (
        typeof events.res.match_info.nicknames !== "undefined" &&
        events.res.match_info.nicknames &&
        events.res.match_info.nicknames.length
      ) {
        result.teamNicknames = List<string>(
          JSON.parse(events.res.match_info.nicknames).team_members
        );
      }

      if (
        typeof events.res.match_info.map !== "undefined" &&
        events.res.match_info.map
      ) {
        result.map = this._convertOverwolfMap(events.res.match_info.map);
      }

      if (
        typeof events.res.match_info.mode !== "undefined" &&
        events.res.match_info.mode
      ) {
        result.mode = this._convertOverwolfGameMode(events.res.match_info.mode);
      }

      if (
        typeof events.res.match_info.match_id !== "undefined" &&
        events.res.match_info.match_id
      ) {
        result.gameId = events.res.match_info.match_id;
      }
    }

    return result;
  }

  ngOnDestroy() {
    this._unregisterErrorNotifications();
    this._unregisterGameInfoObserver();
    this._unregisterGameEventsObserver();
    this._subs.forEach(x => {
      x.unsubscribe();
    });
  }
}
