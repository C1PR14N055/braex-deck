export enum PubgFeatures {
  KILL = "kill",
  REVIVED = "revived",
  DEATH = "death",
  KILLER = "killer",
  MATCH = "match",
  RANK = "rank",
  LOCATION = "location",
  ME = "me",
  TEAM = "team",
  PHASE = "phase",
  MAP = "map",
  ROSTER = "roster"
}
