export enum WindowState {
  CLOSED = "closed",
  MINIMIZED = "minimized",
  HIDDEN = "hidden",
  NORMAL = "normal",
  MAXIMIZED = "maxmimized"
}
