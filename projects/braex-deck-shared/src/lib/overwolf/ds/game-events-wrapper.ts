import { GamePhase } from "../../ds/game/game-phase.enum";
import { List } from "immutable";
import { PubgMap } from "../../ds/game/pubg-map.enum";
import { GameMode } from "../../ds/game/game-mode.enum";

export class GameEventsWrapper {
  constructor(
    public phase: GamePhase | null = null,
    public nickname: string | null = null,
    public kills = 0,
    public headshots = 0,
    public totalDamageDealt = 0,
    public maxKillDistance = 0,
    public totalTeams: number | null = null,
    public localPlayerRank: number | null = null,
    public teamNicknames = List<string>(),
    public map: PubgMap | null = null,
    public mode: GameMode | null = null,
    public gameId: string | null = null
  ) {}
}
