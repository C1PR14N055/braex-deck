import { WindowState } from "./window-state.enum";

export class OverwolfWindow {
  constructor(
    public id?: string,
    public name?: string,
    public width?: number,
    public height?: number,
    public top?: number,
    public left?: number,
    public visible?: boolean,
    public state?: WindowState,
    public parent?: string | null
  ) {}
}
