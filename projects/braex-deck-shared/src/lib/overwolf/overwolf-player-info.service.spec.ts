import { fakeAsync, TestBed, tick } from "@angular/core/testing";

import { OverwolfPlayerInfoService } from "./overwolf-player-info.service";
import { OverwolfGameCommunicationService } from "./communication/overwolf-game-communication.service";
// tslint:disable-next-line:max-line-length
import { FakeOverwolfGameCommunicationService } from "./communication/testing/fake-overwolf-game-communication.service.spec";

describe("OverwolfPlayerInfoService", () => {
  let service: OverwolfPlayerInfoService;
  let fakeGameCommunication: FakeOverwolfGameCommunicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        OverwolfPlayerInfoService,
        {
          provide: OverwolfGameCommunicationService,
          useClass: FakeOverwolfGameCommunicationService
        }
      ]
    });
    service = TestBed.get(OverwolfPlayerInfoService);
    fakeGameCommunication = TestBed.get(
      OverwolfGameCommunicationService
    ) as FakeOverwolfGameCommunicationService;
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should set the player nickname when the me event is fired", fakeAsync(() => {
    const nickname = "TestPlayer";
    expect(service.pubgNickname).toBeFalsy();
    fakeGameCommunication.triggerGameInfoUpdate2({
      info: {
        me: {
          name: nickname
        },
        feature: "me"
      }
    });
    tick(200);
    expect(service.pubgNickname).toBe(nickname);
  }));
});
