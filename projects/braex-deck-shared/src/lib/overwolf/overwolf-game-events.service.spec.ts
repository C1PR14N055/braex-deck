import { async, fakeAsync, TestBed, tick } from "@angular/core/testing";
import { OverwolfGameEventsService } from "./overwolf-game-events.service";
import { OverwolfGameCommunicationService } from "./communication/overwolf-game-communication.service";
import { Vector3 } from "../map-canvas/ds/vector3";
import { GamePhase } from "../ds/game/game-phase.enum";
import { PubgMap } from "../ds/game/pubg-map.enum";
import { GameMode } from "../ds/game/game-mode.enum";
import { OverwolfCoreService } from "./overwolf-core.service";
import { Subscription } from "rxjs";
// tslint:disable-next-line:max-line-length
import { FakeOverwolfGameCommunicationService } from "./communication/testing/fake-overwolf-game-communication.service.spec";
import { List } from "immutable";
import { FakeOverwolfCoreService } from "./testing/fake-overwolf-core.service";
import { GameEventsWrapper } from "./ds/game-events-wrapper";
import { OverwolfError } from "../error/overwolf.error";
import GameInfo = overwolf.games.GameInfo;
import RosterPlayerStats = overwolf.games.events.pubg.info.RosterPlayerStats;

describe("OverwolfGameEventsService", () => {
  let service: OverwolfGameEventsService;
  let fakeCommService: FakeOverwolfGameCommunicationService;
  let fakeCoreService: FakeOverwolfCoreService;
  let gameInfo: GameInfo;
  let subs: Subscription[];

  beforeEach(async(() => {
    gameInfo = {
      allowsVideoCapture: true,
      commandLine: "C:\\test",
      detectedRenderer: "D3D11",
      executionPath: "C:\\some-path",
      height: 1280,
      id: 10906,
      isInFocus: true,
      isRunning: true,
      logicalHeight: 1920,
      logicalWidth: 1080,
      renderers: ["D3D11", "D3D9"],
      sessionId: "test-session-id",
      title: "PUBG",
      width: 720
    };

    TestBed.configureTestingModule({
      providers: [
        OverwolfGameEventsService,
        {
          provide: OverwolfGameCommunicationService,
          useClass: FakeOverwolfGameCommunicationService
        },
        {
          provide: OverwolfCoreService,
          useClass: FakeOverwolfCoreService
        }
      ]
    });

    fakeCoreService = TestBed.get(OverwolfCoreService);
    service = TestBed.get(OverwolfGameEventsService);
    fakeCommService = TestBed.get(OverwolfGameCommunicationService);
    fakeCommService.gameInfo = {
      status: "success",
      res: {
        game_info: {
          phase: "lobby",
          location: null
        },
        match_info: null,
        me: null
      }
    };
    fakeCoreService.triggerPubgLaunched(gameInfo);
    subs = [];
  }));

  it("should notify all observers when the location of the player changes", fakeAsync(() => {
    const location = Vector3.create(900, 1200, 2000);
    let receivedLocation: Vector3;
    service.onLocalPlayerLocationChanged().subscribe(loc => {
      receivedLocation = loc;
    });

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          location: JSON.stringify({
            x: location.x,
            y: location.y,
            z: location.z
          })
        },
        feature: "location"
      }
    });
    tick(400);
    expect(receivedLocation.equals(location)).toBeTruthy();
  }));

  it("should notify all observers when the phase of the game changes", fakeAsync(() => {
    const phase = GamePhase.LOBBY;
    let receivedPhase: GamePhase;
    service.onPhaseChanged().subscribe(p => {
      receivedPhase = p;
    });

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: phase
        },
        feature: "phase"
      }
    });
    tick(400);
    expect(receivedPhase).toBe(phase);
  }));

  it("should notify all observers when the player kills an enemy", fakeAsync(() => {
    let called = false;
    service.onOtherPlayerKilled().subscribe(() => {
      called = true;
    });

    fakeCommService.triggerNewEvent({
      events: [
        {
          kill: null
        }
      ]
    });
    tick(400);
    expect(called).toBeTruthy();
  }));

  it("should notify all observers when the player knocks out an enemy", fakeAsync(() => {
    let called = false;
    service.onOtherPlayerKnockedOut().subscribe(() => {
      called = true;
    });

    fakeCommService.triggerNewEvent({
      events: [
        {
          knockout: null
        }
      ]
    });
    tick(400);
    expect(called).toBeTruthy();
  }));

  it("should notify all observers when the player gives an enemy a headshot", fakeAsync(() => {
    let called = false;
    service.onOtherPlayerHeadshot().subscribe(() => {
      called = true;
    });

    fakeCommService.triggerNewEvent({
      events: [
        {
          headshot: null
        }
      ]
    });
    tick(400);
    expect(called).toBeTruthy();
  }));

  it("should notify all observers when the player has dealt damage", fakeAsync(() => {
    const damage = 132.24;
    let receivedDamage: number;
    service.onDamageDealt().subscribe(d => {
      receivedDamage = d;
    });

    fakeCommService.triggerNewEvent({
      events: [
        {
          damage_dealt: damage
        }
      ]
    });
    tick(400);
    expect(receivedDamage).toBeCloseTo(damage);
  }));

  it("should notify all observers when a player joins the game", fakeAsync(() => {
    const player1 = "TestPlayer1";
    let receivedName: string;
    service.onPlayerJoin().subscribe(name => {
      receivedName = name;
    });

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          roster_0: {
            player: player1,
            kills: 0,
            out: false
          }
        },
        feature: "roster"
      }
    });
    tick(400);
    expect(receivedName).toBe(player1);
  }));

  it("should notify all observers when the player died", fakeAsync(() => {
    let called = false;
    service.onLocalPlayerDied().subscribe(() => {
      called = true;
    });

    fakeCommService.triggerNewEvent({
      events: [
        {
          death: null
        }
      ]
    });
    tick(400);
    expect(called).toBeTruthy();
  }));

  it("should notify all observers when the player has been revived", fakeAsync(() => {
    let called = false;
    service.onLocalPlayerRevived().subscribe(() => {
      called = true;
    });

    fakeCommService.triggerNewEvent({
      events: [
        {
          revived: null
        }
      ]
    });
    tick(400);
    expect(called).toBeTruthy();
  }));

  it("should notify all observers when the player has been knocked out", fakeAsync(() => {
    let called = false;
    service.onLocalPlayerKnockedOut().subscribe(() => {
      called = true;
    });

    fakeCommService.triggerNewEvent({
      events: [
        {
          knockedout: null
        }
      ]
    });
    tick(400);
    expect(called).toBeTruthy();
  }));

  it("should notify all observers when the match has started", fakeAsync(() => {
    let called = false;
    service.onMatchStart().subscribe(() => {
      called = true;
    });

    fakeCommService.triggerNewEvent({
      events: [
        {
          name: "matchStart"
        }
      ]
    });
    tick(400);
    expect(called).toBeTruthy();
  }));

  it("should notify all observers when the match has ended", fakeAsync(() => {
    let called = false;
    service.onMatchCompleted().subscribe(() => {
      called = true;
    });

    fakeCommService.triggerNewEvent({
      events: [
        {
          name: "matchEnd"
        }
      ]
    });
    tick(400);
    expect(called).toBeTruthy();
  }));

  it("should notify all observers about the current map", fakeAsync(() => {
    let receivedMap: PubgMap;
    service.currentMap().subscribe(m => {
      receivedMap = m;
    });

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          map: "Erangel_Main"
        },
        feature: "map"
      }
    });
    tick(400);
    expect(receivedMap).toBe(PubgMap.ERANGEL_MAIN);
  }));

  it("should notify all observers about the current game mode", fakeAsync(() => {
    let receivedMode: GameMode;
    service.currentGameMode().subscribe(m => {
      receivedMode = m;
    });

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          mode: "duo"
        },
        feature: "match"
      }
    });
    tick(400);
    expect(receivedMode).toBe(GameMode.DUO);
  }));

  it("should notify all observers about the killer of the player", fakeAsync(() => {
    const killer = "TestKiller1";
    let receivedKiller: string;
    service.localPlayerKiller().subscribe(name => {
      receivedKiller = name;
    });

    fakeCommService.triggerNewEvent({
      events: [
        {
          killer: killer
        }
      ]
    });
    tick(400);
    expect(receivedKiller).toBe(killer);
  }));

  it("should notify all observers about the nicknames of the players team", fakeAsync(() => {
    const nicknames = [
      "TestPlayer1",
      "TestPlayer2",
      "TestPlayer3",
      "TestPlayer3"
    ];
    let receivedNicknames: List<string>;
    service.playerTeamNicknames().subscribe(nn => {
      receivedNicknames = nn;
    });

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          nicknames: JSON.stringify({
            team_members: nicknames.map(x => {
              return { player: x };
            })
          })
        },
        feature: "team"
      }
    });
    tick(400);
    nicknames.forEach(x => {
      expect(receivedNicknames).toContain(x);
    });
  }));

  it("should notify all observers about the total teams count", fakeAsync(() => {
    const totalTeams = 94;
    let receivedTeams: number;
    service.totalTeams().subscribe(total => {
      receivedTeams = total;
    });

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          total_teams: totalTeams
        },
        feature: "rank"
      }
    });
    tick(400);
    expect(receivedTeams).toBe(totalTeams);
  }));

  it("#onPlayerJoin should only notify about players which join the game", async(() => {
    const participants = [
      { nickname: "TestPlayer1", left: false },
      { nickname: "TestPlayer2", left: false },
      { nickname: "TestPlayer3", left: true }
    ];
    const expectedNicknames = participants
      .filter(x => !x.left)
      .map(x => x.nickname);
    service.onPlayerJoin().subscribe(nickname => {
      expect(expectedNicknames).toContain(nickname);
    });

    participants.forEach((x, i) => {
      const rosterId = `roster_${i}`;
      const matchInfo = {};
      matchInfo[rosterId] = {
        player: x.nickname,
        kills: 0,
        out: x.left
      };
      fakeCommService.triggerGameInfoUpdate2({
        info: {
          match_info: matchInfo,
          feature: "roster"
        }
      });
    });
  }));

  it("should notify all observers that a player has left the game", fakeAsync(() => {
    const playerName = "TestPlayer1";
    let receivedName: string;
    service.onPlayerLeft().subscribe(nickname => {
      receivedName = nickname;
    });

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          roster_0: {
            player: playerName,
            kills: 0,
            out: true
          }
        },
        feature: "roster"
      }
    });
    tick(400);
    expect(receivedName).toBe(playerName);
  }));

  it("#onPlayerLeft should only notify about player leaving the game", async(() => {
    const players = [
      {
        nickname: "TestPlayer1",
        left: true
      },
      {
        nickname: "TestPlayer2",
        left: false
      },
      {
        nickname: "TestPlayer3",
        left: true
      }
    ];
    const playerNames = players.filter(x => x.left).map(x => x.nickname);
    service.onPlayerLeft().subscribe(nickname => {
      expect(playerNames).toContain(nickname);
    });

    players.forEach((x, i) => {
      const rosterId = `roster_${i}`;
      const matchInfo = {};
      matchInfo[rosterId] = {
        player: x.nickname,
        kills: 0,
        out: x.left
      };
      fakeCommService.triggerGameInfoUpdate2({
        info: {
          match_info: matchInfo,
          feature: "roster"
        }
      });
    });
  }));

  it("should notify all observers about the local players name", fakeAsync(() => {
    const name = "TestPlayer";
    let receivedName: string;
    service.localPlayerName().subscribe(n => {
      receivedName = n;
    });

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        me: {
          name: name
        },
        feature: "me"
      }
    });
    tick(400);
    expect(receivedName).toBe(name);
  }));

  it("should buffer the name of the local player", fakeAsync(() => {
    const name = "TestPlayer";
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        me: {
          name: name
        },
        feature: "me"
      }
    });
    tick(1000);

    let receivedName: string;
    service.localPlayerName().subscribe(n => {
      receivedName = n;
    });
    tick(300);

    expect(receivedName).toBe(name);
  }));

  it("should parse a roster to json when it's a string", fakeAsync(() => {
    const player: RosterPlayerStats = {
      player: "TestPlayer",
      out: false,
      kills: 0
    };
    let receivedNickname: string;
    const sub = service.onPlayerJoin().subscribe(nickname => {
      receivedNickname = nickname;
    });
    subs.push(sub);

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          roster_0: JSON.stringify(player)
        },
        feature: "roster"
      }
    });
    tick(400);
    expect(receivedNickname).toBe(player.player);
  }));

  it("should cache the game phase", fakeAsync(() => {
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "lobby"
        },
        feature: "phase"
      }
    });
    tick(400);

    let receivedPhase: GamePhase;
    const sub = service.onPhaseChanged().subscribe(p => {
      receivedPhase = p;
    });
    subs.push(sub);
    tick(200);

    expect(receivedPhase).toEqual(GamePhase.LOBBY);
  }));

  it("should clear the cached game phase when the match ends", fakeAsync(() => {
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "landed"
        },
        feature: "phase"
      }
    });
    tick(400);

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "lobby"
        },
        feature: "phase"
      }
    });
    tick(400);

    let received: GamePhase;
    const sub = service.onPhaseChanged().subscribe(p => {
      received = p;
    });
    subs.push(sub);
    tick(200);
    expect(received).toBeUndefined();
  }));

  it("should clear the cached game phase when pubg is terminated", fakeAsync(() => {
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "freefly"
        },
        feature: "phase"
      }
    });
    tick(400);

    let receivedPhase: GamePhase;
    let sub = service.onPhaseChanged().subscribe(p => {
      receivedPhase = p;
    });
    subs.push(sub);
    tick(200);
    expect(receivedPhase).toEqual(GamePhase.FREE_FLY);

    fakeCoreService.triggerPubgTerminated();
    tick(200);

    receivedPhase = undefined;
    sub = service.onPhaseChanged().subscribe(p => {
      receivedPhase = p;
    });
    subs.push(sub);
    expect(receivedPhase).toBeUndefined();
  }));

  it("should cache the current map", fakeAsync(() => {
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          map: "Erangel_Main"
        },
        feature: "map"
      }
    });
    tick(4000);

    let received: PubgMap;
    const sub = service.currentMap().subscribe(x => {
      received = x;
    });
    subs.push(sub);
    tick(20);

    expect(received).toEqual(PubgMap.ERANGEL_MAIN);
  }));

  it("should remove the cached map when the game ends", fakeAsync(() => {
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          map: "Erangel_Main"
        },
        feature: "map"
      }
    });
    tick(4000);

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "airfield"
        },
        feature: "phase"
      }
    });
    tick(400);

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "lobby"
        },
        feature: "phase"
      }
    });
    tick(400);

    let received: PubgMap;
    const sub = service.currentMap().subscribe(x => {
      received = x;
    });
    subs.push(sub);
    tick(20);

    expect(received).toBeUndefined();
  }));

  it("should clear the cached map when pubg is terminated", fakeAsync(() => {
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          map: "Erangel_Main"
        },
        feature: "map"
      }
    });
    tick(4000);

    fakeCoreService.triggerPubgTerminated();
    tick(500);

    let received: PubgMap;
    const sub = service.currentMap().subscribe(x => {
      received = x;
    });
    subs.push(sub);

    expect(received).toBeFalsy();
  }));

  it("should notify all observers when the match ends", fakeAsync(() => {
    let called = false;
    const sub = service.onMatchLeft().subscribe(() => {
      called = true;
    });
    subs.push(sub);

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "airfield"
        },
        feature: "phase"
      }
    });
    tick(400);

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "lobby"
        },
        feature: "phase"
      }
    });
    tick(400);

    expect(called).toBeTruthy();
  }));

  it("should NOT notify all observers that the match has been left when only receiving the phase LOBBY", fakeAsync(() => {
    let called = false;
    const sub = service.onMatchLeft().subscribe(() => {
      called = true;
    });
    subs.push(sub);

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "lobby"
        },
        feature: "phase"
      }
    });
    tick(400);

    expect(called).toBeFalsy();
  }));

  it("should cache the player joins", fakeAsync(() => {
    const player1 = "Player1";
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          roster_0: {
            player: player1,
            kills: 0,
            out: false
          }
        },
        feature: "roster"
      }
    });
    tick(5000);

    let received: string;
    const sub = service.onPlayerJoin().subscribe(x => {
      received = x;
    });
    subs.push(sub);
    tick(20);

    expect(received).toEqual(player1);
  }));

  it("should cache the player joins of multiple players", fakeAsync(() => {
    const players = ["Player1", "AnotherPlayer", "Obama", "Progress"];
    players.forEach(nickname => {
      fakeCommService.triggerGameInfoUpdate2({
        info: {
          match_info: {
            roster_0: {
              player: nickname,
              kills: 0,
              out: false
            }
          },
          feature: "roster"
        }
      });
      tick(20);
    });
    tick(1000000);

    const received: string[] = [];
    const sub = service.onPlayerJoin().subscribe(x => {
      received.push(x);
    });
    subs.push(sub);

    tick(1000);
    players.forEach(nickname => {
      expect(received).toContain(nickname);
    });
  }));

  it("should clear the cached players joins when the player leaves", fakeAsync(() => {
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "airfield"
        },
        feature: "phase"
      }
    });
    tick(400);

    const players = ["Player1", "AnotherPlayer", "Obama", "Progress"];
    players.forEach(nickname => {
      fakeCommService.triggerGameInfoUpdate2({
        info: {
          match_info: {
            roster_0: {
              player: nickname,
              kills: 0,
              out: false
            }
          },
          feature: "roster"
        }
      });
      tick(20);
    });
    tick(1000000);

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "lobby"
        },
        feature: "phase"
      }
    });
    tick(400);

    const received: string[] = [];
    const sub = service.onPlayerJoin().subscribe(x => {
      received.push(x);
    });
    subs.push(sub);
    tick(1000);

    expect(received.length).toBeFalsy();
  }));

  it("should clear the cached player leaves when pubg is terminated", fakeAsync(() => {
    const player = "Player";
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          roster_0: {
            player: player,
            kills: 0,
            out: false
          }
        },
        feature: "roster"
      }
    });
    tick(20);

    fakeCoreService.triggerPubgTerminated();
    tick(50);

    let received: string;
    const sub = service.onPlayerJoin().subscribe(x => {
      received = x;
    });
    subs.push(sub);
    tick(500);

    expect(received).toBeFalsy();
  }));

  it("should cache the player leaves", fakeAsync(() => {
    const player1 = "WTF";
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          roster_0: {
            player: player1,
            kills: 0,
            out: true
          }
        },
        feature: "roster"
      }
    });
    tick(5000);

    let received: string;
    const sub = service.onPlayerLeft().subscribe(x => {
      received = x;
    });
    subs.push(sub);
    tick(20);

    expect(received).toEqual(player1);
  }));

  it("should cache the player leaves of multiple players", fakeAsync(() => {
    const players = ["Spec", "QL", "Tab", "Jump", "Phone"];
    players.forEach(nickname => {
      fakeCommService.triggerGameInfoUpdate2({
        info: {
          match_info: {
            roster_0: {
              player: nickname,
              kills: 0,
              out: true
            }
          },
          feature: "roster"
        }
      });
      tick(50);
    });
    tick(53023);

    const received: string[] = [];
    const sub = service.onPlayerLeft().subscribe(nickname => {
      received.push(nickname);
    });
    subs.push(sub);
    tick(555);

    players.forEach(nickname => {
      expect(received).toContain(nickname);
    });
  }));

  it("should clear the cached player leaves when the player leaves", fakeAsync(() => {
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "airfield"
        },
        feature: "phase"
      }
    });
    tick(400);

    const players = ["Spec", "QL", "Tab", "Jump", "Phone"];
    players.forEach(nickname => {
      fakeCommService.triggerGameInfoUpdate2({
        info: {
          match_info: {
            roster_0: {
              player: nickname,
              kills: 0,
              out: true
            }
          },
          feature: "roster"
        }
      });
      tick(50);
    });
    tick(53023);

    fakeCommService.triggerGameInfoUpdate2({
      info: {
        game_info: {
          phase: "lobby"
        },
        feature: "phase"
      }
    });
    tick(400);

    const received: string[] = [];
    const sub = service.onPlayerLeft().subscribe(x => {
      received.push(x);
    });
    subs.push(sub);

    expect(received.length).toBeFalsy();
  }));

  it("should clear the cached player leaves when pubg is terminated", fakeAsync(() => {
    const player = "Player";
    fakeCommService.triggerGameInfoUpdate2({
      info: {
        match_info: {
          roster_0: {
            player: player,
            kills: 0,
            out: true
          }
        },
        feature: "roster"
      }
    });
    tick(20);

    fakeCoreService.triggerPubgTerminated();
    tick(50);

    let received: string;
    const sub = service.onPlayerLeft().subscribe(x => {
      received = x;
    });
    subs.push(sub);
    tick(500);

    expect(received).toBeFalsy();
  }));

  it("should emit the currently available events", fakeAsync(() => {
    fakeCommService.gameInfo = {
      status: "success",
      res: {
        game_info: {
          phase: "airfield",
          location: null
        },
        match_info: {
          headshots: null,
          kills: "1",
          map: "DihorOtok_Main",
          match_id:
            "match.bro.official.pc-2018-03.steam.solo-fpp.eu.2019.05.12.10.d45c45c2-c296-43ff-ac91-a68a72b3ebe7",
          max_kill_distance: null,
          me: null,
          mode: "duo",
          total_teams: 97,
          total_damage_dealt: "0.923901",
          nicknames: JSON.stringify({ team_members: ["Jesus", "Moses"] })
        },
        me: {
          name: "Jesus",
          aiming: null,
          bodyPosition: null,
          view: "FPP",
          inVehicle: "false",
          freeView: null,
          movement: "normal"
        }
      }
    };

    let received: GameEventsWrapper;
    service.availableGameEvents().subscribe(x => {
      received = x;
    });
    tick(200);

    expect(received.gameId).toEqual(
      "match.bro.official.pc-2018-03.steam.solo-fpp.eu.2019.05.12.10.d45c45c2-c296-43ff-ac91-a68a72b3ebe7"
    );
    expect(received.phase).toEqual(GamePhase.AIRFIELD);
    expect(received.headshots).toEqual(0);
    expect(received.kills).toEqual(1);
    expect(received.map).toEqual(PubgMap.DIHOROTOK_MAIN);
    expect(received.maxKillDistance).toEqual(0);
    expect(received.localPlayerRank).toBeNull();
    expect(received.mode).toEqual(GameMode.DUO);
    expect(received.totalTeams).toEqual(97);
    expect(received.totalDamageDealt).toBeCloseTo(0.923901);
    expect(received.teamNicknames).toContain("Jesus");
    expect(received.teamNicknames).toContain("Moses");
    expect(received.nickname).toEqual("Jesus");
  }));

  it("should emit an error when it fails to get the currently available events", fakeAsync(() => {
    fakeCommService.gameInfo = {
      status: "error",
      res: null
    };

    let received: OverwolfError;
    service.availableGameEvents().subscribe(
      () => {},
      error => {
        received = error;
      }
    );
    tick(200);

    expect(received).toBeTruthy();
    expect(received instanceof OverwolfError).toBeTruthy();
  }));

  afterEach(() => {
    service.ngOnDestroy();
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
