import { fakeAsync, TestBed, tick } from "@angular/core/testing";
import { OverwolfCoreService } from "./overwolf-core.service";
import { OverwolfGameCommunicationService } from "./communication/overwolf-game-communication.service";
// tslint:disable-next-line:max-line-length
import { FakeOverwolfGameCommunicationService } from "./communication/testing/fake-overwolf-game-communication.service.spec";
import { PUBG_GAME_ID } from "./constant";
// tslint:disable-next-line:max-line-length
import { FakeOverwolfSettingsCommunicationService } from "./communication/testing/fake-overwolf-settings-communication.service.spec";
import { OverwolfSettingsCommunicationService } from "./communication/overwolf-settings-communication.service";
import { Subscription } from "rxjs";
import { OverwolfWindowCommunicationService } from "./communication/overwolf-window-communication.service";
// tslint:disable-next-line:max-line-length
import { FakeOverwolfWindowCommunicationService } from "./communication/testing/fake-overwolf-window-communication.service.spec";
import { OverwolfError } from "../error/overwolf.error";
import { WindowState } from "./ds/window-state.enum";
import { OverwolfWindow } from "./ds/overwolf-window";
import GameInfo = overwolf.games.GameInfo;

describe("OverwolfCoreService", () => {
  let service: OverwolfCoreService;
  let gameInfo: GameInfo;
  let fakeGameCommService: FakeOverwolfGameCommunicationService;
  let fakeSettingsCommService: FakeOverwolfSettingsCommunicationService;
  let fakeWindowComm: FakeOverwolfWindowCommunicationService;
  let subs: Subscription[];

  beforeEach(() => {
    gameInfo = {
      allowsVideoCapture: true,
      commandLine: "C:\\test",
      detectedRenderer: "D3D11",
      executionPath: "C:\\some-path",
      height: 1280,
      id: PUBG_GAME_ID,
      isInFocus: true,
      isRunning: true,
      logicalHeight: 1920,
      logicalWidth: 1080,
      renderers: ["D3D11", "D3D9"],
      sessionId: "test-session-id",
      title: "PUBG",
      width: 720
    };

    TestBed.configureTestingModule({
      providers: [
        OverwolfCoreService,
        {
          provide: OverwolfGameCommunicationService,
          useClass: FakeOverwolfGameCommunicationService
        },
        {
          provide: OverwolfSettingsCommunicationService,
          useClass: FakeOverwolfSettingsCommunicationService
        },
        {
          provide: OverwolfWindowCommunicationService,
          useClass: FakeOverwolfWindowCommunicationService
        }
      ]
    });
    service = TestBed.get(OverwolfCoreService);
    fakeGameCommService = TestBed.get(OverwolfGameCommunicationService);
    fakeSettingsCommService = TestBed.get(OverwolfSettingsCommunicationService);
    fakeWindowComm = TestBed.get(OverwolfWindowCommunicationService);
  });

  beforeEach(() => {
    subs = [];
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should notify all subscribers that PUBG has been launched", fakeAsync(() => {
    let receivedGameInfo: GameInfo;
    service.onPubgLaunched().subscribe(g => {
      receivedGameInfo = g;
    });

    fakeGameCommService.triggerGameLaunched(gameInfo);
    tick(500);
    expect(receivedGameInfo).toBe(gameInfo);
  }));

  it("should notify a subscriber that subscribed after PUBG has been launched", fakeAsync(() => {
    fakeGameCommService.triggerGameLaunched(gameInfo);
    tick(300);

    let receivedGameInfo: GameInfo;
    service.onPubgLaunched().subscribe(g => {
      receivedGameInfo = g;
    });
    expect(receivedGameInfo).toBe(gameInfo);
  }));

  it("should notify all observers even when the app is launched after PUBG", fakeAsync(() => {
    fakeGameCommService.currentlyRunningGame = gameInfo;
    const coreService = new OverwolfCoreService(
      fakeGameCommService,
      fakeSettingsCommService,
      fakeWindowComm
    );
    expect(coreService).toBeTruthy();
    tick(500);

    let receivedGameInfo: GameInfo;
    coreService.onPubgLaunched().subscribe(info => {
      receivedGameInfo = info;
    });
    expect(receivedGameInfo).toBe(gameInfo);
  }));

  it("should register a hotkey", fakeAsync(() => {
    service.registerHotkey("test_action", () => {});
    tick(200);
    expect(fakeSettingsCommService.registerHotkey).toHaveBeenCalled();
  }));

  it("should fail registering a hotkey", () => {
    fakeSettingsCommService.registerHotkey.and.callFake((...args) => {
      args[1]({ status: "error", error: "this registration should fail" });
    });
    expect(() => {
      service.registerHotkey("test_action", () => {});
    }).toThrow();
  });

  it("should notify all observers when pubg is terminated", fakeAsync(() => {
    let terminated = false;
    const sub = service.onPubgTerminated().subscribe(() => {
      terminated = true;
    });
    subs.push(sub);

    fakeGameCommService.triggerGameInfoUpdate({
      gameInfo: {
        allowsVideoCapture: true,
        commandLine: "C:\\test",
        detectedRenderer: "D3D11",
        executionPath: "C:\\some-path",
        height: 1280,
        id: PUBG_GAME_ID,
        isInFocus: false,
        isRunning: false,
        logicalHeight: 1920,
        logicalWidth: 1080,
        renderers: ["D3D11", "D3D9"],
        sessionId: "test-session-id",
        title: "PUBG",
        width: 720
      },
      runningChanged: true,
      focusChanged: false,
      gameChanged: false,
      resolutionChanged: false
    });
    expect(terminated).toBeTruthy();
  }));

  it("should get the main window", () => {
    service.getMainWindow();
    expect(fakeWindowComm.getMainWindow).toHaveBeenCalled();
  });

  it("should close the given window", fakeAsync(() => {
    let called = false;
    service.closeWindow("SomeWindow").subscribe(() => {
      called = true;
    });
    tick(200);

    expect(called).toBeTruthy();
  }));

  it("should minimize the given window", fakeAsync(() => {
    fakeWindowComm.minimizeEventResult = {
      status: "success",
      window_id: "Window_Extension_lkflknsdfl8r23lkjf8fh023fh0"
    };

    let called = false;
    service.minimizeWindow("FakeWindow").subscribe(() => {
      called = true;
    });
    tick(200);

    expect(called).toBeTruthy();
  }));

  it("should emit an error when minimizing the window fails", fakeAsync(() => {
    fakeWindowComm.minimizeEventResult = {
      status: "error",
      window_id: "Window_Extension_lkflknsdfl8r23lkjf8fh023fh0"
    };

    let receivedError: any;
    service.minimizeWindow("ABCWindow").subscribe(
      () => {},
      error => {
        receivedError = error;
      }
    );
    tick(200);

    expect(receivedError).toBeTruthy();
    expect(receivedError instanceof OverwolfError).toBeTruthy();
  }));

  it("should maximize the given window", fakeAsync(() => {
    fakeWindowComm.maximizeEventResult = {
      status: "success",
      window_id: "Window_Extension_lkflknsdfl8r23lkjf8fh023fh0"
    };

    let called = false;
    service.maximizeWindow("AnOverlayWindow").subscribe(() => {
      called = true;
    });
    tick(200);

    expect(called).toBeTruthy();
  }));

  it("should emit an error when maximizing the window fails", fakeAsync(() => {
    fakeWindowComm.maximizeEventResult = {
      status: "error",
      window_id: "Window_Extension_lkflknsdfl8r23lkjf8fh023fh0"
    };

    let receivedError: any;
    service.maximizeWindow("ADesktopOnlyWindow").subscribe(
      () => {},
      error => {
        receivedError = error;
      }
    );
    tick(200);

    expect(receivedError).toBeTruthy();
    expect(receivedError instanceof OverwolfError).toBeTruthy();
  }));

  it("should get the state of the given window", fakeAsync(() => {
    fakeWindowComm.windowStateResult = {
      status: "success",
      window_id: "Window_Extenstion_kjlcsadfjhi92fh2234",
      window_state_ex: "maximized"
    };

    let received: WindowState;
    service.getWindowState("TrashWindow").subscribe(state => {
      received = state;
    });
    tick(200);

    expect(received).toEqual(WindowState.MAXIMIZED);
  }));

  it("should emit an error when getting the window state fails", fakeAsync(() => {
    fakeWindowComm.windowStateResult = {
      status: "error",
      window_id: undefined,
      window_state_ex: undefined
    };

    let receivedError: any;
    service.getWindowState("NotAWindow").subscribe(
      () => {},
      error => {
        receivedError = error;
      }
    );
    tick(200);

    expect(receivedError).toBeTruthy();
    expect(receivedError instanceof OverwolfError).toBeTruthy();
  }));

  it("should obtain the declared window", fakeAsync(() => {
    fakeWindowComm.obtainDeclaredWindowResult = {
      status: "success",
      window: {
        height: 600,
        width: 400,
        id: "Window_Extension_jsfdoshjfopifhjkwef823i55ilhkjcjksfop2839r",
        isVisible: true,
        left: 12,
        name: "Richard",
        parent: null,
        stateEx: "normal",
        top: 100
      }
    };

    let receivedWindow: OverwolfWindow;
    service.createWindow("noPopupWindowISwear").subscribe(w => {
      receivedWindow = w;
    });
    tick(200);

    expect(receivedWindow).toBeTruthy();
    expect(receivedWindow.id).toEqual(
      fakeWindowComm.obtainDeclaredWindowResult.window.id
    );
    expect(receivedWindow.name).toEqual(
      fakeWindowComm.obtainDeclaredWindowResult.window.name
    );
    expect(receivedWindow.width).toEqual(
      fakeWindowComm.obtainDeclaredWindowResult.window.width
    );
    expect(receivedWindow.height).toEqual(
      fakeWindowComm.obtainDeclaredWindowResult.window.height
    );
    expect(receivedWindow.top).toEqual(
      fakeWindowComm.obtainDeclaredWindowResult.window.top
    );
    expect(receivedWindow.left).toEqual(
      fakeWindowComm.obtainDeclaredWindowResult.window.left
    );
    expect(receivedWindow.visible).toEqual(
      fakeWindowComm.obtainDeclaredWindowResult.window.isVisible
    );
    expect(receivedWindow.state).toEqual(WindowState.NORMAL);
    expect(receivedWindow.parent).toEqual(
      fakeWindowComm.obtainDeclaredWindowResult.window.parent
    );
  }));

  it("should emit an error when it failed to obtain the declared window", fakeAsync(() => {
    fakeWindowComm.obtainDeclaredWindowResult = {
      status: "error",
      window: undefined
    };

    let receivedError: any;
    service.createWindow("helloImAWindow").subscribe(
      () => {},
      error => {
        receivedError = error;
      }
    );
    tick(200);

    expect(receivedError).toBeTruthy();
    expect(receivedError instanceof OverwolfError).toBeTruthy();
  }));

  it("should restore the state of the window", fakeAsync(() => {
    fakeWindowComm.restoreEventResult = {
      status: "success",
      window_id: "Window_Extension_kljfas343lklhkj3s3d2f1gf98f34"
    };

    let called = false;
    service.showWindow("notNeededWindow").subscribe(() => {
      called = true;
    });
    tick(200);

    expect(called).toBeTruthy();
  }));

  it("should emit an error when restoring the window state fails", fakeAsync(() => {
    fakeWindowComm.restoreEventResult = {
      status: "error",
      window_id: "Window_Extension_kljfas343lklhkj3s3d2f1gf98f34"
    };

    let receivedError: any;
    service.showWindow("peter").subscribe(
      () => {},
      error => {
        receivedError = error;
      }
    );
    tick(200);

    expect(receivedError).toBeTruthy();
    expect(receivedError instanceof OverwolfError).toBeTruthy();
  }));

  it("should hide the window", fakeAsync(() => {
    let called = false;
    service.hideWindow("overwolf").subscribe(() => {
      called = true;
    });
    tick(200);

    expect(called).toBeTruthy();
  }));

  afterEach(() => {
    service.ngOnDestroy();
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
