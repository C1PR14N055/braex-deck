import { Pipe, PipeTransform } from "@angular/core";
import { PubgMap } from "../ds/game/pubg-map.enum";
import { MappingError } from "../error/mapping.error";

export const MAP_ICONS_BASE_PATH = "/assets/img/map-icon/";

@Pipe({
  name: "mapIcon"
})
export class MapIconPipe implements PipeTransform {
  transform(value: PubgMap, args?: any) {
    switch (value) {
      case PubgMap.DESERT_MAIN:
        return `${MAP_ICONS_BASE_PATH}desert-main-light.png`;
      case PubgMap.DIHOROTOK_MAIN:
        return `${MAP_ICONS_BASE_PATH}dihorotok-main-light.png`;
      case PubgMap.ERANGEL_MAIN:
        return `${MAP_ICONS_BASE_PATH}erangel-main-light.png`;
      case PubgMap.SAVAGE_MAIN:
        return `${MAP_ICONS_BASE_PATH}savage-main-light.png`;
      case PubgMap.RANGE_MAIN:
        return `${MAP_ICONS_BASE_PATH}range-main-light.png`;
    }

    throw new MappingError(`No map icon found for the PubgMap \`${value}\``);
  }
}
