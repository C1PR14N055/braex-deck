import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MapIconPipe } from "./map-icon.pipe";

@NgModule({
  declarations: [MapIconPipe],
  imports: [CommonModule],
  exports: [MapIconPipe]
})
export class MapIconModule {}
