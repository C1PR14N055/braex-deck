import { TestBed } from "@angular/core/testing";

import { QuickAccessMenuService } from "./quick-access-menu.service";
import { QuickAccessMenuModule } from "./quick-access-menu.module";

describe("QuickAccessMenuService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [QuickAccessMenuModule]
    })
  );

  it("should be created", () => {
    const service: QuickAccessMenuService = TestBed.get(QuickAccessMenuService);
    expect(service).toBeTruthy();
  });
});
