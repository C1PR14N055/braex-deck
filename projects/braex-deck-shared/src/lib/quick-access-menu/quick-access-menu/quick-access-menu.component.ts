import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild
} from "@angular/core";
import {
  faMapMarkerAlt,
  faMousePointer,
  faPaintBrush
} from "@fortawesome/free-solid-svg-icons";
import { MapToolService } from "../../map-canvas/services/map-tool.service";
import { MapCanvasTool } from "../../map-canvas/ds/map-canvas-tool.enum";
import { QuickAccessToolEntry } from "./ds/quick-access-tool-entry";
import { ToolConfig } from "../../map-canvas/ds/tool-config";
import { QuickAccessAnimState } from "./ds/quick-access-anim-state.enum";
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";

@Component({
  selector: "lib-quick-access-menu",
  templateUrl: "./quick-access-menu.component.html",
  styleUrls: ["./quick-access-menu.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger("quickAccessMenu", [
      state("visible", style({ opacity: 1, transform: "scale(1)" })),
      state("hidden", style({ opacity: 0, transform: "scale(0.7)" })),
      transition("hidden => visible", animate(90))
    ])
  ]
})
export class QuickAccessMenuComponent implements OnInit {
  private _animState = QuickAccessAnimState.HIDDEN;
  private _mapTools: QuickAccessToolEntry[] = [];
  private _visible = false;
  @ViewChild("quickAccess")
  private _quickAccess: ElementRef<HTMLDivElement>;
  @ViewChild("mainCircle")
  private _mainCircle: ElementRef<HTMLDivElement>;

  constructor(
    private _mapToolService: MapToolService,
    private _changeDetector: ChangeDetectorRef,
    private _renderer: Renderer2
  ) {}

  ngOnInit() {
    this._initTools();
  }

  private _initTools() {
    Object.keys(MapCanvasTool).forEach(key => {
      const tool = MapCanvasTool[key];
      let config = this._mapToolService.getToolConfig(tool);
      if (!config) {
        config = <ToolConfig>{
          icon: faMousePointer,
          cursor: "default",
          tool: MapCanvasTool.SELECTION
        };
      }

      this._mapTools.push(new QuickAccessToolEntry(tool, config));
    });
    this._changeDetector.detectChanges();
  }

  setActiveTool(mapTool: MapCanvasTool) {
    this._mapToolService.activeTool = mapTool;
  }

  mapToolTrackBy(index: number, item: QuickAccessToolEntry) {
    return item.tool;
  }

  /**
   * Open the QuickAccessMenu if possible at the wanted position.
   * If the menu won't fit on the position (e.g. because it will be off-screen)
   * it will be placed so that it's not cut off.
   * @param x The preferred x position
   * @param y The preferred y position
   */
  open(x: number, y: number) {
    const mainCircleRect = this._mainCircle.nativeElement.getBoundingClientRect();
    // FIXME: find the formula to compute the distance
    const distanceToMainCircle = 70;
    this._renderer.setStyle(
      this._quickAccess.nativeElement,
      "top",
      `${y - distanceToMainCircle}px`
    );
    this._renderer.setStyle(
      this._quickAccess.nativeElement,
      "left",
      `${x - mainCircleRect.width / 2}px`
    );

    const rect = this._quickAccess.nativeElement.getBoundingClientRect();
    // check if the menu is out of screen on the left side
    if (rect.left < 0) {
      this._renderer.setStyle(this._quickAccess.nativeElement, "left", "0px");
    }

    // check if the menu is out of screen on the right side
    if (rect.left + rect.width > document.body.offsetWidth) {
      this._renderer.setStyle(
        this._quickAccess.nativeElement,
        "left",
        `${document.body.offsetWidth -
          this._quickAccess.nativeElement.offsetWidth}px`
      );
    }

    // check if the menu is out of screen on the top
    if (rect.top < 0) {
      this._renderer.setStyle(this._quickAccess.nativeElement, "top", "0px");
    }

    // check if the menu is out of screen on the bottom
    if (rect.top + rect.height > document.body.offsetHeight) {
      this._renderer.setStyle(
        this._quickAccess.nativeElement,
        "top",
        `${document.body.offsetHeight -
          this._quickAccess.nativeElement.offsetHeight}px`
      );
    }

    setTimeout(() => {
      this._animState = QuickAccessAnimState.ORBIT_FWD;
      this._visible = true;
      this._changeDetector.detectChanges();

      setTimeout(() => {
        this._animState = QuickAccessAnimState.VISIBLE;
        this._changeDetector.detectChanges();
      }, 300);
    }, 0);
  }

  close() {
    this._visible = false;
    this._animState = QuickAccessAnimState.HIDDEN;
    this._changeDetector.detectChanges();
  }

  get activeIcon() {
    switch (this._mapToolService.activeTool) {
      case MapCanvasTool.SELECTION:
      default: {
        return faMousePointer;
      }
      case MapCanvasTool.BRUSH: {
        return faPaintBrush;
      }
      case MapCanvasTool.MARKER: {
        return faMapMarkerAlt;
      }
    }
  }

  get mapTools() {
    return this._mapTools;
  }

  get visible() {
    return this._visible;
  }

  get animState() {
    return this._animState;
  }
}
