import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { QuickAccessMenuComponent } from "./quick-access-menu.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

describe("QuickAccessMenuComponent", () => {
  let component: QuickAccessMenuComponent;
  let fixture: ComponentFixture<QuickAccessMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuickAccessMenuComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [BrowserAnimationsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickAccessMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
