export enum QuickAccessAnimState {
  HIDDEN = "hidden",
  ORBIT_FWD = "orbit_fwd",
  VISIBLE = "visible"
}
