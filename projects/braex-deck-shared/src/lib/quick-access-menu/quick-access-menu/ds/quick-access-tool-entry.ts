import { MapCanvasTool } from "../../../map-canvas/ds/map-canvas-tool.enum";
import { ToolConfig } from "../../../map-canvas/ds/tool-config";

export class QuickAccessToolEntry {
  constructor(public tool: MapCanvasTool, public config: ToolConfig) {}
}
