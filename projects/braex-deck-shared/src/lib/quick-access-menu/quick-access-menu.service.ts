import { ComponentRef, Injectable } from "@angular/core";
import { Overlay, OverlayRef } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";
import { QuickAccessMenuComponent } from "./quick-access-menu/quick-access-menu.component";

@Injectable()
export class QuickAccessMenuService {
  private _overlayRef: OverlayRef;
  private _portal: ComponentPortal<QuickAccessMenuComponent> | null = null;
  private _componentRef: ComponentRef<QuickAccessMenuComponent> | null = null;

  constructor(private _overlay: Overlay) {
    this._initQuickAccessMenu();
  }

  private _initQuickAccessMenu() {
    this._overlayRef = this._overlay.create();
    this._portal = new ComponentPortal(QuickAccessMenuComponent);
    this._componentRef = this._overlayRef.attach(this._portal);
  }

  /**
   * {@see QuickAccessMenuComponent#open}
   */
  open(x: number, y: number) {
    this._componentRef.instance.open(x, y);
  }

  close() {
    this._componentRef.instance.close();
  }
}
