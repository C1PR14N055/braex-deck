import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { QuickAccessMenuComponent } from "./quick-access-menu/quick-access-menu.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { OverlayModule } from "@angular/cdk/overlay";
import { QuickAccessMenuService } from "./quick-access-menu.service";

@NgModule({
  declarations: [QuickAccessMenuComponent],
  imports: [CommonModule, FontAwesomeModule, OverlayModule],
  exports: [QuickAccessMenuComponent],
  entryComponents: [QuickAccessMenuComponent],
  providers: [QuickAccessMenuService]
})
export class QuickAccessMenuModule {}
