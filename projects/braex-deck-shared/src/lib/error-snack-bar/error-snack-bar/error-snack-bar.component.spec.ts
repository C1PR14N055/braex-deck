import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ErrorSnackBarComponent } from "./error-snack-bar.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { MAT_SNACK_BAR_DATA } from "@angular/material";

describe("ErrorSnackBarComponent", () => {
  let component: ErrorSnackBarComponent;
  let fixture: ComponentFixture<ErrorSnackBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ErrorSnackBarComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: MAT_SNACK_BAR_DATA, useValue: "" }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorSnackBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
