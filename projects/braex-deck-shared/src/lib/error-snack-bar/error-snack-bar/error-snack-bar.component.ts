import { Component, Inject } from "@angular/core";
import { MAT_SNACK_BAR_DATA } from "@angular/material";
import { faTimesCircle } from "@fortawesome/free-regular-svg-icons";

class ViewContext {
  faTimesCircle = faTimesCircle;
}

@Component({
  selector: "lib-error-snack-bar",
  templateUrl: "./error-snack-bar.component.html",
  styleUrls: ["./error-snack-bar.component.scss"]
})
export class ErrorSnackBarComponent {
  readonly viewContext = new ViewContext();

  constructor(@Inject(MAT_SNACK_BAR_DATA) private _message: string) {}

  get message() {
    return this._message;
  }
}
