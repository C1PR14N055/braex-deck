import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ErrorSnackBarComponent } from "./error-snack-bar/error-snack-bar.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [ErrorSnackBarComponent],
  imports: [CommonModule, FontAwesomeModule],
  exports: [ErrorSnackBarComponent],
  entryComponents: [ErrorSnackBarComponent]
})
export class ErrorSnackBarModule {}
