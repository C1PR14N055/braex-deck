import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CompactGameEntryComponent } from "./compact-game-entry/compact-game-entry.component";
import { CopyButtonModule } from "../copy-button/copy-button.module";
import { MapIconModule } from "../map-icon/map-icon.module";
import { GameModeBadgeModule } from "../game-mode-badge/game-mode-badge.module";
import { MatButtonModule, MatRippleModule } from "@angular/material";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { DurationModule } from "../duration/duration.module";
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [CompactGameEntryComponent],
  imports: [
    CommonModule,
    CopyButtonModule,
    MapIconModule,
    GameModeBadgeModule,
    MatButtonModule,
    MatRippleModule,
    FontAwesomeModule,
    DurationModule,
    RouterModule
  ],
  exports: [CompactGameEntryComponent]
})
export class CompactGameEntryModule {}
