import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CompactGameEntryComponent } from "./compact-game-entry.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { MapIconModule } from "../../map-icon/map-icon.module";
import { LazyGame } from "../../ds/game/lazy-game";
import { PubgMap } from "../../ds/game/pubg-map.enum";
import { PubgGameMode } from "../../ds/game/pubg-game-mode.enum";
import * as Moment from "moment";
import { DurationModule } from "../../duration/duration.module";
import { RouterTestingModule } from "@angular/router/testing";
import { EagerGame } from "../../ds/game/eager-game";

describe("CompactGameEntryComponent", () => {
  let component: CompactGameEntryComponent;
  let fixture: ComponentFixture<CompactGameEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompactGameEntryComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [MapIconModule, DurationModule, RouterTestingModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompactGameEntryComponent);
    component = fixture.componentInstance;
    component.game = new EagerGame(
      new LazyGame(
        12,
        "c8d9a700-d9a8-48ad-8af6-c8d1ef50eb43",
        Moment().subtract(4, "d"),
        1235566,
        PubgGameMode.DUO_FPP,
        PubgMap.DIHOROTOK_MAIN
      )
    );
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
