import { Component, Input, OnInit } from "@angular/core";
import { environment } from "../../../environments/environment";
import { Router } from "@angular/router";
import { EagerGame } from "../../ds/game/eager-game";

class ViewContext {
  dateTimeFormat = environment.dateTimeFormat;
}

@Component({
  selector: "lib-compact-game-entry",
  templateUrl: "./compact-game-entry.component.html",
  styleUrls: [
    "./compact-game-entry.component.scss",
    "../../../../../styles/shared-game.scss"
  ]
})
export class CompactGameEntryComponent implements OnInit {
  readonly viewContext = new ViewContext();
  @Input()
  game: EagerGame;

  constructor(private _router: Router) {}

  ngOnInit() {}

  actionOpenGame() {
    this._router.navigate(["/gameDetails", this.game.info.gameId]);
  }
}
