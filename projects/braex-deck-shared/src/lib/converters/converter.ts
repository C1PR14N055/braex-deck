/**
 * S: The source type which should be converted.
 * T: The target type into which the source type will be converted.
 */
export interface Converter<S, T> {
  convert(source: S): T;
}
