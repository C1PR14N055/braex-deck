import { Converter } from "./converter";
import { Injectable } from "@angular/core";
import { Pageable } from "../ds/pageable";
import { ExtPageableResponse } from "../ds/external/ext-pageable-response";

@Injectable({
  providedIn: "root"
})
export class ExtPageableConverter
  implements Converter<ExtPageableResponse<any>, Pageable<any>> {
  convert(source: ExtPageableResponse<any>): Pageable<any> {
    const result = new Pageable<any>();
    result.pageSize = source.pageable.pageSize;
    result.pageNumber = source.pageable.pageNumber;
    result.offset = source.pageable.offset;
    result.last = source.last;
    result.totalPages = source.totalPages;
    result.totalElements = source.totalElements;
    result.numberOfElements = source.numberOfElements;

    return result;
  }
}
