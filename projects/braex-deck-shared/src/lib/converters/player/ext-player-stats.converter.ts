import { Converter } from "../converter";
import { ExtPlayerStatsWrapper } from "../../ds/external/ext-player-stats-wrapper";
import { PlayerStats } from "../../ds/player/player-stats";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ExtPlayerStatsConverter
  implements Converter<ExtPlayerStatsWrapper, PlayerStats> {
  convert(source: ExtPlayerStatsWrapper) {
    const result = new PlayerStats();
    result.nickname = source.playerName;
    if (source.stats) {
      result.kdRatio = source.stats.kdRate;
      if (source.stats.headshotRate > 0) {
        result.headshotRatio = 100 / source.stats.headshotRate;
      }
      result.averageKills = source.stats.avgKills;
      result.top10Ratio = source.stats.top10Rate * 100;
      result.averageDamage = source.stats.avgDmg;
      result.longestKill = source.stats.longestKill;
      result.averageSurvivedTime = source.stats.avgSurvivedTime;
      result.mostKills = source.stats.mostKills;
      result.playedMatches = source.stats.matches;
      result.winRatio = source.stats.winRate * 100;
      result.rankTitle = source.stats.rankPointsTitle;
      result.rankPoints = source.stats.rankPoints || 0;
    }

    return result;
  }
}
