import { TestBed } from "@angular/core/testing";
import { ExtPlayerStatsConverter } from "./ext-player-stats.converter";
import { ExtPlayerStatsWrapper } from "../../ds/external/ext-player-stats-wrapper";

describe("ExtPlayerStatsConverter", () => {
  let converter: ExtPlayerStatsConverter;
  let extPlayerStats: ExtPlayerStatsWrapper;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExtPlayerStatsConverter]
    });
    extPlayerStats = Object.freeze({
      season: "division.bro.official.pc-2018-02",
      playerName: "DerCrashi",
      gameMode: "DUO_FPP",
      stats: {
        avgKills: 0.9387,
        top10Rate: 0.44061,
        avgDmg: 142.61356,
        longestKill: 247.980130000000002610249794088304042816162109375,
        avgSurvivedTime: 900.04395,
        mostKills: 5,
        matches: 261,
        headshotRate: 3.71212,
        kdRate: 0.9387,
        winRate: 0.03831,
        rankPointsTitle: "0-0",
        rankPoints: 2529.8823000000002139131538569927215576171875
      }
    });
    converter = TestBed.get(ExtPlayerStatsConverter);
  });

  it("should create", () => {
    expect(converter).toBeTruthy();
  });

  it("should compute the winRatio correctly", () => {
    const result = converter.convert(extPlayerStats);
    expect(result.winRatio).toBeCloseTo(3.831);
  });

  it("should compute the top10Ratio correctly", () => {
    const result = converter.convert(extPlayerStats);
    expect(result.top10Ratio).toBeCloseTo(44.061);
  });

  it("should compute the headshotRatio correctly", () => {
    const result = converter.convert(extPlayerStats);
    expect(result.headshotRatio).toBeCloseTo(26.938784307);
  });
});
