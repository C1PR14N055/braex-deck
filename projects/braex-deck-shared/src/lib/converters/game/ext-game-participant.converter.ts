import { Converter } from "../converter";
import { ExtGameParticipant } from "../../ds/external/game/ext-game-participant";
import { GameParticipant } from "../../ds/game/game-participant";
import { Injectable } from "@angular/core";
import * as Moment from "moment";

@Injectable({
  providedIn: "root"
})
export class ExtGameParticipantConverter
  implements Converter<ExtGameParticipant, GameParticipant> {
  convert(source: ExtGameParticipant) {
    const result = new GameParticipant();
    result.id = source.id;
    result.created = Moment.unix(source.created);
    result.updated = Moment.unix(source.updated);
    result.dbnos = source.dbnos;
    result.assists = source.assists;
    result.boosts = source.boosts;
    result.damageDealt = source.damageDealt;
    result.deathType = source.deathType;
    result.headshotKills = source.headshotkills;
    result.heals = source.heals;
    result.killPlace = source.killPlace;
    result.killStreaks = source.killStreaks;
    result.kills = source.kills;
    result.lastKillPoints = source.lastKillPoints;
    result.lastWinPoints = source.lastWinPoints;
    result.mostDamage = source.mostDamage;
    result.playerId = source.playerId;
    result.rankPoints = source.rankPoints;
    result.revives = source.revives;
    result.rideDistance = source.rideDistance;
    result.roadKills = source.roadKills;
    result.swimDistance = source.swimDistance;
    result.teamKills = source.teamKills;
    result.timeSurvived = source.timeSurvived;
    result.vehiclesDestroyed = source.vehicleDestroys;
    result.walkDistance = source.walkDistance;
    result.weaponsAcquired = source.weaponsAcquired;
    result.winPlace = source.winPlace;
    result.name = source.name;

    return result;
  }
}
