import { Converter } from "../converter";
import { ExtLazyGame } from "../../ds/external/game/ext-lazy-game";
import { LazyGame } from "../../ds/game/lazy-game";
import { Injectable } from "@angular/core";
import * as Moment from "moment";
import { ExtGameModeConverter } from "./ext-game-mode.converter";
import { ExtPubgMapConverter } from "./ext-pubg-map.converter";

@Injectable({
  providedIn: "root"
})
export class ExtLazyGameConverter implements Converter<ExtLazyGame, LazyGame> {
  constructor(
    private _extGameMode: ExtGameModeConverter,
    private _extPubgMap: ExtPubgMapConverter
  ) {}

  convert(source: ExtLazyGame) {
    const result = new LazyGame();
    result.id = source.id;
    result.gameId = source.matchId;
    result.createdAt = Moment.unix(source.createdAt);
    result.duration = source.duration;
    result.gameMode = this._extGameMode.convert(source.gameMode);
    result.map = this._extPubgMap.convert(source.mapName);
    result.isCustomGame = source.isCustomMatch;
    result.telemetryUrl = source.telemetryUrl;

    return result;
  }
}
