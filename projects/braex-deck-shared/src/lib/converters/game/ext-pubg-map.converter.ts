import { Converter } from "../converter";
import { ExtPUBGMap } from "../../ds/external/game/ext-pubg-map.enum";
import { PubgMap } from "../../ds/game/pubg-map.enum";
import { Injectable } from "@angular/core";
import { EXT_PUBG_MAP_MAPPING } from "./mapping/ext-pubg-map.mapping";
import { MappingError } from "../../error/mapping.error";

@Injectable({
  providedIn: "root"
})
export class ExtPubgMapConverter implements Converter<ExtPUBGMap, PubgMap> {
  convert(source: ExtPUBGMap) {
    if (!EXT_PUBG_MAP_MAPPING.has(source)) {
      throw new MappingError(
        `No mapping found for the ExtPUBGMap \`${source}\``
      );
    }

    return EXT_PUBG_MAP_MAPPING.get(source);
  }
}
