import { Converter } from "../converter";
import { ExtGameRoster } from "../../ds/external/game/ext-game-roster";
import { GameRoster } from "../../ds/game/game-roster";
import { Injectable } from "@angular/core";
import { ExtGameParticipantConverter } from "./ext-game-participant.converter";
import { GameParticipant } from "../../ds/game/game-participant";
import { List } from "immutable";

@Injectable({
  providedIn: "root"
})
export class ExtGameRosterConverter
  implements Converter<ExtGameRoster, GameRoster> {
  constructor(private _extGameParticipant: ExtGameParticipantConverter) {}

  convert(source: ExtGameRoster) {
    const result = new GameRoster();
    result.id = source.id;
    result.rank = source.rank;
    result.teamId = source.teamId;
    result.won = source.won;
    result.participants = List<GameParticipant>(
      source.participants.map(x => this._extGameParticipant.convert(x))
    );

    return result;
  }
}
