import { ExtPlatformRegion } from "../../../ds/external/game/ext-platform-region.enum";
import { PlatformRegion } from "../../../ds/game/platform-region.enum";

export const EXT_PLATFORM_REGION_MAPPING = new Map<
  ExtPlatformRegion,
  PlatformRegion
>([
  [ExtPlatformRegion.STEAM, PlatformRegion.STEAM],
  [ExtPlatformRegion.PC_KRJP, PlatformRegion.STEAM],
  [ExtPlatformRegion.PC_JP, PlatformRegion.STEAM],
  [ExtPlatformRegion.PC_NA, PlatformRegion.STEAM],
  [ExtPlatformRegion.PC_EU, PlatformRegion.STEAM],
  [ExtPlatformRegion.PC_OC, PlatformRegion.STEAM],
  [ExtPlatformRegion.PC_KAKAO, PlatformRegion.STEAM],
  [ExtPlatformRegion.PC_SEA, PlatformRegion.STEAM],
  [ExtPlatformRegion.PC_SA, PlatformRegion.STEAM],
  [ExtPlatformRegion.PC_AS, PlatformRegion.STEAM],
  [ExtPlatformRegion.KAKAO, PlatformRegion.KAKAO],
  [ExtPlatformRegion.TOURNAMENT, PlatformRegion.TOURNAMENT],
  [ExtPlatformRegion.XBOX, PlatformRegion.XBOX],
  [ExtPlatformRegion.XBOX_AS, PlatformRegion.XBOX],
  [ExtPlatformRegion.XBOX_EU, PlatformRegion.XBOX],
  [ExtPlatformRegion.XBOX_NA, PlatformRegion.XBOX],
  [ExtPlatformRegion.XBOX_OC, PlatformRegion.XBOX]
]);
