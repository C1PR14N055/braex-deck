import { ExtGameMode } from "../../../ds/external/game/ext-game-mode.enum";
import { PubgGameMode } from "../../../ds/game/pubg-game-mode.enum";

export const EXT_GAME_MODE_MAPPING = new Map([
  [ExtGameMode.SOLO, PubgGameMode.SOLO],
  [ExtGameMode.DUO, PubgGameMode.DUO],
  [ExtGameMode.SQUAD, PubgGameMode.SQUAD],
  [ExtGameMode.NORMAL, PubgGameMode.NORMAL],
  [ExtGameMode.SOLO_FPP, PubgGameMode.SOLO_FPP],
  [ExtGameMode.DUO_FPP, PubgGameMode.DUO_FPP],
  [ExtGameMode.SQUAD_FPP, PubgGameMode.SQUAD_FPP],
  [ExtGameMode.NORMAL_SOLO, PubgGameMode.SOLO],
  [ExtGameMode.NORMAL_DUO, PubgGameMode.DUO],
  [ExtGameMode.NORMAL_SQUAD, PubgGameMode.SQUAD],
  [ExtGameMode.NORMAL_SOLO_FPP, PubgGameMode.SOLO_FPP],
  [ExtGameMode.NORMAL_DUO_FPP, PubgGameMode.DUO_FPP],
  [ExtGameMode.NORMAL_SQUAD_FPP, PubgGameMode.SQUAD_FPP],
  [ExtGameMode.WAR, PubgGameMode.WAR],
  [ExtGameMode.WAR_SOLO, PubgGameMode.WAR_SOLO],
  [ExtGameMode.WAR_SOLO_FPP, PubgGameMode.WAR_SOLO_FPP],
  [ExtGameMode.WAR_DUO, PubgGameMode.WAR_DUO],
  [ExtGameMode.WAR_DUO_FPP, PubgGameMode.WAR_DUO_FPP],
  [ExtGameMode.WAR_SQUAD, PubgGameMode.WAR_SQUAD],
  [ExtGameMode.WAR_SQUAD_FPP, PubgGameMode.WAR_SQUAD_FPP],
  [ExtGameMode.ZOMBIE, PubgGameMode.ZOMBIE],
  [ExtGameMode.ZOMBIE_SOLO, PubgGameMode.ZOMBIE_SOLO],
  [ExtGameMode.ZOMBIE_SOLO_FPP, PubgGameMode.ZOMBIE_SOLO_FPP],
  [ExtGameMode.ZOMBIE_DUO, PubgGameMode.ZOMBIE_DUO],
  [ExtGameMode.ZOMBIE_DUO_FPP, PubgGameMode.ZOMBIE_DUO_FPP],
  [ExtGameMode.ZOMBIE_SQUAD, PubgGameMode.ZOMBIE_SQUAD],
  [ExtGameMode.ZOMBIE_SQUAD_FPP, PubgGameMode.ZOMBIE_SQUAD_FPP],
  [ExtGameMode.CONQUEST, PubgGameMode.CONQUEST],
  [ExtGameMode.CONQUEST_SOLO, PubgGameMode.CONQUEST_SOLO],
  [ExtGameMode.CONQUEST_SOLO_FPP, PubgGameMode.CONQUEST_SOLO_FPP],
  [ExtGameMode.CONQUEST_DUO, PubgGameMode.CONQUEST_DUO],
  [ExtGameMode.CONQUEST_DUO_FPP, PubgGameMode.CONQUEST_DUO_FPP],
  [ExtGameMode.CONQUEST_SQUAD, PubgGameMode.CONQUEST_SQUAD],
  [ExtGameMode.CONQUEST_SQUAD_FPP, PubgGameMode.CONQUEST_SQUAD_FPP],
  [ExtGameMode.ESPORTS, PubgGameMode.ESPORTS],
  [ExtGameMode.ESPORTS_SOLO, PubgGameMode.ESPORTS_SOLO],
  [ExtGameMode.ESPORTS_SOLO_FPP, PubgGameMode.ESPORTS_SOLO_FPP],
  [ExtGameMode.ESPORTS_DUO, PubgGameMode.ESPORTS_DUO],
  [ExtGameMode.ESPORTS_DUO_FPP, PubgGameMode.ESPORTS_DUO_FPP],
  [ExtGameMode.ESPORTS_SQUAD, PubgGameMode.ESPORTS_SQUAD],
  [ExtGameMode.ESPORTS_SQUAD_FPP, PubgGameMode.ESPORTS_SQUAD_FPP]
]);
