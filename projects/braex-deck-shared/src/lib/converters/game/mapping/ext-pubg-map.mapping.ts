import { ExtPUBGMap } from "../../../ds/external/game/ext-pubg-map.enum";
import { PubgMap } from "../../../ds/game/pubg-map.enum";

export const EXT_PUBG_MAP_MAPPING = new Map([
  [ExtPUBGMap.ERANGEL_MAIN, PubgMap.ERANGEL_MAIN],
  [ExtPUBGMap.DESERT_MAIN, PubgMap.DESERT_MAIN],
  [ExtPUBGMap.SAVAGE_MAIN, PubgMap.SAVAGE_MAIN],
  [ExtPUBGMap.RANGE_MAIN, PubgMap.RANGE_MAIN],
  [ExtPUBGMap.DIHOROTOK_MAIN, PubgMap.DIHOROTOK_MAIN]
]);
