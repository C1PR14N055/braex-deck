import { Converter } from "../converter";
import { ExtPlatformRegion } from "../../ds/external/game/ext-platform-region.enum";
import { PlatformRegion } from "../../ds/game/platform-region.enum";
import { Injectable } from "@angular/core";
import { EXT_PLATFORM_REGION_MAPPING } from "./mapping/ext-platform-region.mapping";
import { MappingError } from "../../error/mapping.error";

@Injectable({
  providedIn: "root"
})
export class ExtPlatformRegionConverter
  implements Converter<ExtPlatformRegion, PlatformRegion> {
  convert(source: ExtPlatformRegion) {
    if (!EXT_PLATFORM_REGION_MAPPING.has(source)) {
      throw new MappingError(
        `No mapping found for the ExtPlatformRegion \`${source}\``
      );
    }

    return EXT_PLATFORM_REGION_MAPPING.get(source);
  }
}
