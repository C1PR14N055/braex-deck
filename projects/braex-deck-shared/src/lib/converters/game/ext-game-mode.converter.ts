import { Converter } from "../converter";
import { ExtGameMode } from "../../ds/external/game/ext-game-mode.enum";
import { PubgGameMode } from "../../ds/game/pubg-game-mode.enum";
import { Injectable } from "@angular/core";
import { EXT_GAME_MODE_MAPPING } from "./mapping/ext-game-mode.mapping";
import { MappingError } from "../../error/mapping.error";

@Injectable({
  providedIn: "root"
})
export class ExtGameModeConverter
  implements Converter<ExtGameMode, PubgGameMode> {
  convert(source: ExtGameMode) {
    if (!EXT_GAME_MODE_MAPPING.has(source)) {
      throw new MappingError(
        `No mapping found for the ExtGameMode \`${source}\``
      );
    }

    return EXT_GAME_MODE_MAPPING.get(source);
  }
}
