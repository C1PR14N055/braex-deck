import { Pipe, PipeTransform } from "@angular/core";
import { Perspective } from "../ds/game/perspective.enum";

@Pipe({
  name: "perspective"
})
export class PerspectivePipe implements PipeTransform {
  transform(value: Perspective, args?: any) {
    switch (value) {
      case Perspective.FPP:
        return "Fpp";
      case Perspective.TPP:
        return "Tpp";
    }

    return value;
  }
}
