import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GameModePipe } from "./game-mode.pipe";
import { PerspectivePipe } from "./perspective.pipe";

@NgModule({
  declarations: [GameModePipe, PerspectivePipe],
  imports: [CommonModule],
  exports: [GameModePipe, PerspectivePipe]
})
export class SharedPipesModule {}
