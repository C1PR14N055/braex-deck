import { Pipe, PipeTransform } from "@angular/core";
import { GameMode } from "../ds/game/game-mode.enum";

@Pipe({
  name: "gameMode"
})
export class GameModePipe implements PipeTransform {
  transform(value: GameMode, args?: any) {
    switch (value) {
      case GameMode.SOLO:
        return "Solo";
      case GameMode.DUO:
        return "Duo";
      case GameMode.SQUAD:
        return "Squad";
    }

    return value;
  }
}
