export interface WebSocketBrokerConfig {
  brokerUrl: string;
  pushPath: string;
  pullPath: string;
}
