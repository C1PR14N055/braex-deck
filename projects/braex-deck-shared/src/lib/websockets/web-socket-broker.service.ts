import { Injectable } from "@angular/core";
import { WebSocketBrokerConfig } from "./web-socket-broker-config";
import { WebSocketBroker } from "./web-socket-broker";

@Injectable({
  providedIn: "root"
})
export class WebSocketBrokerService {
  /**
   * Create a new {@link WebSocketBroker} for the message type {@link T}
   * @param config The {@link WebSocketBrokerConfig} which will be used by the created
   * message broker to connect to the web socket.
   */
  createBroker<T>(config: WebSocketBrokerConfig) {
    return new WebSocketBroker<T>(config);
  }
}
