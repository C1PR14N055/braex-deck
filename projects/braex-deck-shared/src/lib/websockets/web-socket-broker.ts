import { WebSocketBrokerConfig } from "./web-socket-broker-config";
import { Observable, Observer, Subscription } from "rxjs";
import { RxStomp } from "@stomp/rx-stomp";
import { environment } from "../../environments/environment";
import { map } from "rxjs/operators";

export class WebSocketBroker<T> {
  private readonly _stompClient: RxStomp;
  private _subs: Subscription[] = [];
  private _connectedObservers: Observer<void>[] = [];

  constructor(private _config: WebSocketBrokerConfig) {
    this._stompClient = new RxStomp();
    this._stompClient.configure({
      brokerURL: _config.brokerUrl,
      connectHeaders: {
        login: environment.webSocketsAuth.login,
        passcode: environment.webSocketsAuth.password
      },
      reconnectDelay: 400
    });
    this._stompClient.activate();
    const sub = this._stompClient.connected$.subscribe(() => {
      this._triggerConnected();
    });
    this._subs.push(sub);
  }

  /**
   * Send a message through the web socket.
   * @param message The message object which should be send through the websocket.
   */
  push(message?: T) {
    this._stompClient.publish({
      destination: this._config.pushPath,
      body: JSON.stringify(message)
    });
  }

  /**
   * An observable which emits the messages which are send through the web socket.
   * (This contains the own sent messages as well)
   */
  pull(): Observable<T> {
    return this._stompClient
      .watch(this._config.pullPath)
      .pipe(map(x => JSON.parse(x.body)));
  }

  private _triggerConnected() {
    this._connectedObservers.forEach(x => {
      x.next(null);
      x.complete();
    });
  }

  /**
   * An observable which emits when the connection to the web socket has been established.
   */
  connected(): Observable<void> {
    return Observable.create(observer => {
      this._connectedObservers.push(observer);
    });
  }

  /**
   * Destroy this web socket broker.
   */
  destroy() {
    this._subs.forEach(x => {
      x.unsubscribe();
    });
    this._stompClient.deactivate();
  }

  get config() {
    return this._config;
  }
}
