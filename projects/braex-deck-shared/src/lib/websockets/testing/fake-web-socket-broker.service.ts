import { FakeWebSocketBroker } from "./fake-web-socket-broker";
import { WebSocketBrokerConfig } from "../web-socket-broker-config";

export class FakeWebSocketBrokerService {
  // noinspection JSUnusedGlobalSymbols
  createBroker<T>(config: WebSocketBrokerConfig) {
    return new FakeWebSocketBroker<T>(config);
  }
}
