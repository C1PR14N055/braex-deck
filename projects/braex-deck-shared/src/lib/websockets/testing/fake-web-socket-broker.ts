import { Observable, Observer, of } from "rxjs";
import { WebSocketBrokerConfig } from "../web-socket-broker-config";

export class FakeWebSocketBroker<T> {
  private _pullObservers: Observer<T>[] = [];

  constructor(public config: WebSocketBrokerConfig) {}

  push(message: T) {
    this._pullObservers.forEach(x => {
      x.next(message);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  pull(): Observable<T> {
    return Observable.create(observer => {
      this._pullObservers.push(observer);
    });
  }

  // noinspection JSUnusedGlobalSymbols
  connected(): Observable<void> {
    return of(null);
  }

  // noinspection JSUnusedGlobalSymbols
  destroy() {
    this._pullObservers.forEach(x => {
      x.complete();
    });
  }
}
