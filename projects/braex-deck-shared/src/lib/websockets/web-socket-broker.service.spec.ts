import { TestBed } from "@angular/core/testing";

import { WebSocketBrokerService } from "./web-socket-broker.service";

describe("WebSocketBrokerService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: WebSocketBrokerService = TestBed.get(WebSocketBrokerService);
    expect(service).toBeTruthy();
  });
});
