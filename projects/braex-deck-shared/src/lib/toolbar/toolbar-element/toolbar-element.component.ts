import { Component } from "@angular/core";

@Component({
  selector: "lib-toolbar-element",
  templateUrl: "./toolbar-element.component.html",
  styleUrls: ["./toolbar-element.component.scss"]
})
export class ToolbarElementComponent {}
