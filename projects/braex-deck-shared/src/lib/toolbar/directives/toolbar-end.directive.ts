import { Directive } from "@angular/core";

@Directive({
  selector: "[libToolbarEnd]"
})
export class ToolbarEndDirective {}
