import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ToolbarComponent } from "./toolbar/toolbar.component";
import { ToolbarButtonGroupComponent } from "./toolbar-button-group/toolbar-button-group.component";
import { ToolbarButtonComponent } from "./toolbar-button/toolbar-button.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { ToolbarEndDirective } from "./directives/toolbar-end.directive";
import { ToolbarElementComponent } from "./toolbar-element/toolbar-element.component";

@NgModule({
  declarations: [
    ToolbarComponent,
    ToolbarButtonGroupComponent,
    ToolbarButtonComponent,
    ToolbarEndDirective,
    ToolbarElementComponent
  ],
  imports: [CommonModule, FontAwesomeModule],
  exports: [
    ToolbarComponent,
    ToolbarButtonGroupComponent,
    ToolbarButtonComponent,
    ToolbarEndDirective,
    ToolbarElementComponent
  ]
})
export class ToolbarModule {}
