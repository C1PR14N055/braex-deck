import {
  AfterContentInit,
  Component,
  ContentChildren,
  ElementRef,
  HostBinding,
  Input,
  QueryList,
  Renderer2
} from "@angular/core";
import { DEFAULT_TOOLBAR_MODE, ToolbarMode } from "../toolbar-mode";
import {
  MARGIN_TO_NEXT_BUTTON,
  ToolbarButtonComponent
} from "../toolbar-button/toolbar-button.component";

@Component({
  selector: "lib-toolbar-button-group",
  templateUrl: "./toolbar-button-group.component.html",
  styleUrls: ["./toolbar-button-group.component.scss"]
})
export class ToolbarButtonGroupComponent implements AfterContentInit {
  @Input()
  mode: ToolbarMode = DEFAULT_TOOLBAR_MODE;
  @ContentChildren(ToolbarButtonComponent, { read: ElementRef })
  private _buttons: QueryList<ElementRef<HTMLElement>>;

  constructor(private _renderer: Renderer2) {}

  ngAfterContentInit() {
    if (this._buttons && this._buttons.length > 1) {
      this._buttons.forEach(button => {
        if (button !== this._buttons.last) {
          if (this.mode === "horizontal") {
            this._renderer.setStyle(
              button.nativeElement,
              "margin-right",
              `${MARGIN_TO_NEXT_BUTTON}em`
            );
          } else {
            this._renderer.setStyle(
              button.nativeElement,
              "margin-bottom",
              `${MARGIN_TO_NEXT_BUTTON}em`
            );
          }
        }
      });
    }
  }

  @HostBinding("class")
  get hostClass() {
    if (this.mode === "horizontal") {
      return "h-100";
    } else {
      return "w-100";
    }
  }
}
