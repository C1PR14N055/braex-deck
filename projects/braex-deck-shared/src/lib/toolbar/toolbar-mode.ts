export const DEFAULT_TOOLBAR_MODE: ToolbarMode = "horizontal";
export type ToolbarMode = "horizontal" | "vertical";
