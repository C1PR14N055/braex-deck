import {
  AfterContentInit,
  Component,
  ContentChildren,
  ElementRef,
  Input,
  QueryList,
  Renderer2
} from "@angular/core";
import { ToolbarButtonGroupComponent } from "../toolbar-button-group/toolbar-button-group.component";
import { DEFAULT_TOOLBAR_MODE, ToolbarMode } from "../toolbar-mode";
import {
  MARGIN_TO_NEXT_BUTTON,
  ToolbarButtonComponent
} from "../toolbar-button/toolbar-button.component";

@Component({
  selector: "lib-toolbar",
  templateUrl: "./toolbar.component.html",
  styleUrls: ["./toolbar.component.scss"]
})
export class ToolbarComponent implements AfterContentInit {
  @Input()
  mode: ToolbarMode = DEFAULT_TOOLBAR_MODE;
  @ContentChildren(ToolbarButtonGroupComponent)
  private _buttonGroups: QueryList<ToolbarButtonGroupComponent>;
  @ContentChildren(ToolbarButtonComponent, { read: ElementRef })
  private _buttons: QueryList<ElementRef<HTMLElement>>;

  constructor(private _renderer: Renderer2) {}

  ngAfterContentInit() {
    this._buttonGroups.forEach(buttonGroup => {
      buttonGroup.mode = this.mode;
    });

    if (this._buttons && this._buttons.length > 1) {
      this._buttons.forEach(button => {
        if (button !== this._buttons.last) {
          if (this.mode === "horizontal") {
            this._renderer.setStyle(
              button.nativeElement,
              "margin-right",
              `${MARGIN_TO_NEXT_BUTTON}em`
            );
          } else {
            this._renderer.setStyle(
              button.nativeElement,
              "margin-bottom",
              `${MARGIN_TO_NEXT_BUTTON}em`
            );
          }
        }
      });
    }
  }
}
