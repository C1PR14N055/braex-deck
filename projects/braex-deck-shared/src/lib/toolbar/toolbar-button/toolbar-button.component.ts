import {
  Component,
  HostBinding,
  Input,
  ViewEncapsulation
} from "@angular/core";
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";

export const MARGIN_TO_NEXT_BUTTON = 0.3;

@Component({
  selector: "lib-toolbar-button",
  templateUrl: "./toolbar-button.component.html",
  styleUrls: ["./toolbar-button.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class ToolbarButtonComponent {
  @Input()
  icon: IconDefinition;
  @Input()
  disabled = false;
  @Input()
  active = false;

  @HostBinding("attr.disabled")
  get hostDisabled() {
    return this.disabled;
  }
}
