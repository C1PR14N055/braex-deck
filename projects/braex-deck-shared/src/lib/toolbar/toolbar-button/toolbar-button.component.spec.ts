import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ToolbarButtonComponent } from "./toolbar-button.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("ToolbarButtonComponent", () => {
  let component: ToolbarButtonComponent;
  let fixture: ComponentFixture<ToolbarButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ToolbarButtonComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
