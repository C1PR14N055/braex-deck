import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { InfoButtonComponent } from "./info-button.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("InfoButtonComponent", () => {
  let component: InfoButtonComponent;
  let fixture: ComponentFixture<InfoButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InfoButtonComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
