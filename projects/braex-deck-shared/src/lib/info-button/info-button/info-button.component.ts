import { Component, Input, ViewChild } from "@angular/core";
import { SatPopover } from "@ncstate/sat-popover";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";

export const DEFAULT_OPEN_DELAY = 200;

class ViewContext {
  faInfoCircle = faInfoCircle;
}

@Component({
  selector: "lib-info-button",
  templateUrl: "./info-button.component.html",
  styleUrls: ["./info-button.component.scss"]
})
export class InfoButtonComponent {
  readonly viewContext = new ViewContext();
  @Input()
  openDelay = DEFAULT_OPEN_DELAY;
  @ViewChild("infoPopover")
  private _popover: SatPopover;
  private _hovered = false;

  actionMouseEnter() {
    this._hovered = true;
    setTimeout(() => {
      if (this._hovered) {
        this._popover.open();
      }
    }, this.openDelay);
  }

  actionMouseLeave() {
    this._hovered = false;
    this._popover.close();
  }
}
