import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { InfoButtonComponent } from "./info-button/info-button.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { SatPopoverModule } from "@ncstate/sat-popover";

@NgModule({
  declarations: [InfoButtonComponent],
  imports: [CommonModule, FontAwesomeModule, SatPopoverModule],
  exports: [InfoButtonComponent]
})
export class InfoButtonModule {}
