import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from "@angular/core";
import * as ClipboardJS from "clipboard";
import { faCopy } from "@fortawesome/free-solid-svg-icons";

export type ButtonColor = "dark" | "light";

class ViewContext {
  public faCopy = faCopy;
}

@Component({
  selector: "lib-copy-button",
  templateUrl: "./copy-button.component.html",
  styleUrls: ["./copy-button.component.scss"]
})
export class CopyButtonComponent implements OnInit, OnDestroy {
  readonly viewContext = new ViewContext();
  /**
   * The value which should be copied to the clipboard.
   */
  @Input()
  valueToCopy: string;
  @Input()
  color: ButtonColor = "dark";
  private _clipboard: ClipboardJS;
  @ViewChild("button")
  private _button: ElementRef<HTMLButtonElement>;
  @ViewChild("copiedText")
  private _copiedText: ElementRef<HTMLSpanElement>;

  constructor(private _renderer: Renderer2) {}

  ngOnInit() {
    this._clipboard = new ClipboardJS(this._button.nativeElement, {
      text: () => {
        return this.valueToCopy;
      }
    });
  }

  ngOnDestroy() {
    this._clipboard.destroy();
  }

  actionShowCopyAnimation() {
    const copied: string = "copied";
    this._renderer.removeClass(this._copiedText.nativeElement, copied);
    this._renderer.addClass(this._copiedText.nativeElement, copied);
    setTimeout(() => {
      this._renderer.removeClass(this._copiedText.nativeElement, copied);
    }, 500);
  }
}
