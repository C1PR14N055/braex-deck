import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CopyButtonComponent } from "./copy-button.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

describe("CopyButtonComponent", () => {
  let component: CopyButtonComponent;
  let fixture: ComponentFixture<CopyButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyButtonComponent],
      imports: [FontAwesomeModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
