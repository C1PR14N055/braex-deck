import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CopyButtonComponent } from "./copy-button/copy-button.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  imports: [CommonModule, FontAwesomeModule],
  declarations: [CopyButtonComponent],
  exports: [CopyButtonComponent]
})
export class CopyButtonModule {}
