import { Pipe, PipeTransform } from "@angular/core";
import { RANK_TITLE_PATTERN } from "../shared/rank-shared";

export const RANK_ICONS_BASE_PATH = "/assets/img/ranks/";
export const OTHER_RANKS_DIR = "other/";
export const BEGINNER_RANKS_DIR = "beginner/";
export const EXPERIENCED_RANKS_DIR = "experienced/";
export const NOVICE_RANKS_DIR = "novice/";
export const SKILLED_RANKS_DIR = "skilled/";
export const SPECIALIST_RANKS_DIR = "specialist/";
export const BEGINNER_ICON_PREFIX = "beginner";
export const EXPERIENCED_ICON_PREFIX = "experienced";
export const NOVICE_ICON_PREFIX = "novice";
export const SKILLED_ICON_PREFIX = "skilled";
export const SPECIALIST_ICON_PREFIX = "specialist";

@Pipe({
  name: "rankIcon"
})
export class RankIconPipe implements PipeTransform {
  /**
   * Get the path of a rank icon by the rankTitle of the PUBG API.
   * Eg.
   * "1-2" -> /assets/img/ranks/beginner/beginner-2.png
   *
   * The info which rankTitle belongs to which icon can be found here:
   * https://github.com/pubg/api-assets/blob/master/survivalTitles.json
   *
   * @param value The rankTitle from the PUBG API.
   * @param args The args will be ignored.
   * @returns The whole path to the rank icon asset.
   */
  transform(value: string, args?: any) {
    let icon = `${OTHER_RANKS_DIR}unknown.png`;
    if (value) {
      const groups = RANK_TITLE_PATTERN.exec(value);
      if (groups && groups.length === 3) {
        switch (groups[1]) {
          case "1":
            icon = `${BEGINNER_RANKS_DIR}${BEGINNER_ICON_PREFIX}-${
              groups[2]
            }.png`;
            break;
          case "2":
            icon = `${NOVICE_RANKS_DIR}${NOVICE_ICON_PREFIX}-${groups[2]}.png`;
            break;
          case "3":
            icon = `${EXPERIENCED_RANKS_DIR}${EXPERIENCED_ICON_PREFIX}-${
              groups[2]
            }.png`;
            break;
          case "4":
            icon = `${SKILLED_RANKS_DIR}${SKILLED_ICON_PREFIX}-${
              groups[2]
            }.png`;
            break;
          case "5":
            icon = `${SPECIALIST_RANKS_DIR}${SPECIALIST_ICON_PREFIX}-${
              groups[2]
            }.png`;
            break;
          case "6":
            icon = `${OTHER_RANKS_DIR}expert.png`;
            break;
          case "7":
            icon = `${OTHER_RANKS_DIR}survivor.png`;
            break;
        }
      }
    }

    return `${RANK_ICONS_BASE_PATH}${icon}`;
  }
}
