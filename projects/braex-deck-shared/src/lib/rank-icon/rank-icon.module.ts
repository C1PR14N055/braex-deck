import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RankIconPipe } from "./rank-icon.pipe";

@NgModule({
  declarations: [RankIconPipe],
  imports: [CommonModule],
  exports: [RankIconPipe]
})
export class RankIconModule {}
