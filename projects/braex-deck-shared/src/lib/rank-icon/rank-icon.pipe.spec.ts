import {
  BEGINNER_ICON_PREFIX,
  BEGINNER_RANKS_DIR,
  EXPERIENCED_ICON_PREFIX,
  EXPERIENCED_RANKS_DIR,
  NOVICE_ICON_PREFIX,
  NOVICE_RANKS_DIR,
  OTHER_RANKS_DIR,
  RANK_ICONS_BASE_PATH,
  RankIconPipe,
  SKILLED_ICON_PREFIX,
  SKILLED_RANKS_DIR,
  SPECIALIST_ICON_PREFIX,
  SPECIALIST_RANKS_DIR
} from "./rank-icon.pipe";

describe("RankIconPipe", () => {
  let pipe: RankIconPipe;

  beforeEach(() => {
    pipe = new RankIconPipe();
  });

  it("should be created", () => {
    expect(pipe).toBeTruthy();
  });

  it("should return the unknown icon when passing undefined", () => {
    expect(pipe.transform(undefined)).toEqual(
      RANK_ICONS_BASE_PATH + OTHER_RANKS_DIR + "unknown.png"
    );
  });

  it("should return the unknown icon when passing null", () => {
    expect(pipe.transform(null)).toEqual(
      RANK_ICONS_BASE_PATH + OTHER_RANKS_DIR + "unknown.png"
    );
  });

  it("should return the unknown icon when the rankTitle cannot be identified", () => {
    expect(pipe.transform("something that definitely isn't a rank")).toEqual(
      RANK_ICONS_BASE_PATH + OTHER_RANKS_DIR + "unknown.png"
    );
  });

  it("should return the unknown icon when the rankTitle cannot ba identified 2", () => {
    expect(pipe.transform("9-2")).toEqual(
      RANK_ICONS_BASE_PATH + OTHER_RANKS_DIR + "unknown.png"
    );
  });

  it("should return the unknown icon", () => {
    expect(pipe.transform("0-0")).toEqual(
      RANK_ICONS_BASE_PATH + OTHER_RANKS_DIR + "unknown.png"
    );
  });

  it("should return the beginner-1 icon", () => {
    expect(pipe.transform("1-1")).toEqual(
      RANK_ICONS_BASE_PATH +
        BEGINNER_RANKS_DIR +
        BEGINNER_ICON_PREFIX +
        "-1.png"
    );
  });

  it("should return the beginner-2 icon", () => {
    expect(pipe.transform("1-2")).toEqual(
      RANK_ICONS_BASE_PATH +
        BEGINNER_RANKS_DIR +
        BEGINNER_ICON_PREFIX +
        "-2.png"
    );
  });

  it("should return the beginner-3 icon", () => {
    expect(pipe.transform("1-3")).toEqual(
      RANK_ICONS_BASE_PATH +
        BEGINNER_RANKS_DIR +
        BEGINNER_ICON_PREFIX +
        "-3.png"
    );
  });

  it("should return the beginner-4 icon", () => {
    expect(pipe.transform("1-4")).toEqual(
      RANK_ICONS_BASE_PATH +
        BEGINNER_RANKS_DIR +
        BEGINNER_ICON_PREFIX +
        "-4.png"
    );
  });

  it("should return the beginner-5 icon", () => {
    expect(pipe.transform("1-5")).toEqual(
      RANK_ICONS_BASE_PATH +
        BEGINNER_RANKS_DIR +
        BEGINNER_ICON_PREFIX +
        "-5.png"
    );
  });

  it("should return the novice-1 icon", () => {
    expect(pipe.transform("2-1")).toEqual(
      RANK_ICONS_BASE_PATH + NOVICE_RANKS_DIR + NOVICE_ICON_PREFIX + "-1.png"
    );
  });

  it("should return the novice-2 icon", () => {
    expect(pipe.transform("2-2")).toEqual(
      RANK_ICONS_BASE_PATH + NOVICE_RANKS_DIR + NOVICE_ICON_PREFIX + "-2.png"
    );
  });

  it("should return the novice-3 icon", () => {
    expect(pipe.transform("2-3")).toEqual(
      RANK_ICONS_BASE_PATH + NOVICE_RANKS_DIR + NOVICE_ICON_PREFIX + "-3.png"
    );
  });

  it("should return the novice-4 icon", () => {
    expect(pipe.transform("2-4")).toEqual(
      RANK_ICONS_BASE_PATH + NOVICE_RANKS_DIR + NOVICE_ICON_PREFIX + "-4.png"
    );
  });

  it("should return the novice-5 icon", () => {
    expect(pipe.transform("2-5")).toEqual(
      RANK_ICONS_BASE_PATH + NOVICE_RANKS_DIR + NOVICE_ICON_PREFIX + "-5.png"
    );
  });

  it("should return the experienced-1 icon", () => {
    expect(pipe.transform("3-1")).toEqual(
      RANK_ICONS_BASE_PATH +
        EXPERIENCED_RANKS_DIR +
        EXPERIENCED_ICON_PREFIX +
        "-1.png"
    );
  });

  it("should return the experienced-2 icon", () => {
    expect(pipe.transform("3-2")).toEqual(
      RANK_ICONS_BASE_PATH +
        EXPERIENCED_RANKS_DIR +
        EXPERIENCED_ICON_PREFIX +
        "-2.png"
    );
  });

  it("should return the experienced-3 icon", () => {
    expect(pipe.transform("3-3")).toEqual(
      RANK_ICONS_BASE_PATH +
        EXPERIENCED_RANKS_DIR +
        EXPERIENCED_ICON_PREFIX +
        "-3.png"
    );
  });

  it("should return the experienced-4 icon", () => {
    expect(pipe.transform("3-4")).toEqual(
      RANK_ICONS_BASE_PATH +
        EXPERIENCED_RANKS_DIR +
        EXPERIENCED_ICON_PREFIX +
        "-4.png"
    );
  });

  it("should return the experienced-5 icon", () => {
    expect(pipe.transform("3-5")).toEqual(
      RANK_ICONS_BASE_PATH +
        EXPERIENCED_RANKS_DIR +
        EXPERIENCED_ICON_PREFIX +
        "-5.png"
    );
  });

  it("should return the skilled-1 icon", () => {
    expect(pipe.transform("4-1")).toEqual(
      RANK_ICONS_BASE_PATH + SKILLED_RANKS_DIR + SKILLED_ICON_PREFIX + "-1.png"
    );
  });

  it("should return the skilled-2 icon", () => {
    expect(pipe.transform("4-2")).toEqual(
      RANK_ICONS_BASE_PATH + SKILLED_RANKS_DIR + SKILLED_ICON_PREFIX + "-2.png"
    );
  });

  it("should return the skilled-3 icon", () => {
    expect(pipe.transform("4-3")).toEqual(
      RANK_ICONS_BASE_PATH + SKILLED_RANKS_DIR + SKILLED_ICON_PREFIX + "-3.png"
    );
  });

  it("should return the skilled-4 icon", () => {
    expect(pipe.transform("4-4")).toEqual(
      RANK_ICONS_BASE_PATH + SKILLED_RANKS_DIR + SKILLED_ICON_PREFIX + "-4.png"
    );
  });

  it("should return the skilled-5 icon", () => {
    expect(pipe.transform("4-5")).toEqual(
      RANK_ICONS_BASE_PATH + SKILLED_RANKS_DIR + SKILLED_ICON_PREFIX + "-5.png"
    );
  });

  it("should return the specialist-1 icon", () => {
    expect(pipe.transform("5-1")).toEqual(
      RANK_ICONS_BASE_PATH +
        SPECIALIST_RANKS_DIR +
        SPECIALIST_ICON_PREFIX +
        "-1.png"
    );
  });

  it("should return the specialist-2 icon", () => {
    expect(pipe.transform("5-2")).toEqual(
      RANK_ICONS_BASE_PATH +
        SPECIALIST_RANKS_DIR +
        SPECIALIST_ICON_PREFIX +
        "-2.png"
    );
  });

  it("should return the specialist-3 icon", () => {
    expect(pipe.transform("5-3")).toEqual(
      RANK_ICONS_BASE_PATH +
        SPECIALIST_RANKS_DIR +
        SPECIALIST_ICON_PREFIX +
        "-3.png"
    );
  });

  it("should return the specialist-4 icon", () => {
    expect(pipe.transform("5-4")).toEqual(
      RANK_ICONS_BASE_PATH +
        SPECIALIST_RANKS_DIR +
        SPECIALIST_ICON_PREFIX +
        "-4.png"
    );
  });

  it("should return the specialist-5 icon", () => {
    expect(pipe.transform("5-5")).toEqual(
      RANK_ICONS_BASE_PATH +
        SPECIALIST_RANKS_DIR +
        SPECIALIST_ICON_PREFIX +
        "-5.png"
    );
  });

  it("should return the expert icon", () => {
    expect(pipe.transform("6-0")).toEqual(
      RANK_ICONS_BASE_PATH + OTHER_RANKS_DIR + "expert.png"
    );
  });

  it("should return the lone-survivor icon");

  it("should return the survivor icon", () => {
    expect(pipe.transform("7-0")).toEqual(
      RANK_ICONS_BASE_PATH + OTHER_RANKS_DIR + "survivor.png"
    );
  });
});
