import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from "@angular/core";
import {
  faSearch,
  faSyncAlt,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import {
  animate,
  keyframes,
  style,
  transition,
  trigger
} from "@angular/animations";

class ViewContext {
  faSearch = faSearch;
  faTimes = faTimes;
  faSyncAlt = faSyncAlt;
}

@Component({
  selector: "lib-search-input",
  templateUrl: "./search-input.component.html",
  styleUrls: ["./search-input.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger("clearSearchButton", [
      transition(
        ":enter",
        animate(
          140,
          keyframes([
            style({
              opacity: 0,
              transform: "translateY(-50%) translateX(30%)"
            }),
            style({ opacity: 1, transform: "translateY(-50%) translateX(0)" })
          ])
        )
      ),
      transition(
        ":leave",
        animate(
          160,
          keyframes([
            style({ opacity: 1, transform: "translateY(-50%) scale(1)" }),
            style({ opacity: 0, transform: "translateY(-50%) scale(0.8)" })
          ])
        )
      )
    ])
  ]
})
export class SearchInputComponent {
  readonly viewContext = new ViewContext();
  @Input()
  placeholder = "";
  searchKeywords = "";
  /**
   * Event emitted when the search is triggered. The emitted value
   * are the search keywords which should be searched.
   */
  @Output()
  search = new EventEmitter<string>();
  /**
   * Event emitted when the search should be refreshed.
   */
  @Output()
  refresh = new EventEmitter<void>();
  @ViewChild("searchInput")
  private _searchInput: ElementRef<HTMLInputElement>;
  private _showRefreshButton = false;

  actionSearch() {
    if (!this.searchKeywords.trim()) {
      return;
    }

    this.search.emit(this.searchKeywords.trim());
    this._searchInput.nativeElement.blur();
    this._showRefreshButton = true;
  }

  actionClearSearchInput() {
    this.searchKeywords = "";
  }

  actionRefreshSearch() {
    this.refresh.emit(null);
  }

  focus() {
    this._searchInput.nativeElement.focus();
  }

  blur() {
    this._searchInput.nativeElement.blur();
  }

  get showRefreshButton() {
    return this._showRefreshButton;
  }
}
