import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from "@angular/core/testing";

import { SearchInputComponent } from "./search-input.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { Subscription } from "rxjs";
import { By } from "@angular/platform-browser";

describe("SearchInputComponent", () => {
  let component: SearchInputComponent;
  let fixture: ComponentFixture<SearchInputComponent>;
  let subs: Subscription[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchInputComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [FontAwesomeModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    subs = [];
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should not trigger the search when the search keywords are only spaces", fakeAsync(() => {
    let receivedKeywords: string;
    const sub = component.search.subscribe(kw => {
      receivedKeywords = kw;
    });
    subs.push(sub);

    component.searchKeywords = "            ";
    component.actionSearch();
    tick(400);
    expect(receivedKeywords).toBeFalsy();
  }));

  it("should trigger the search", fakeAsync(() => {
    let receivedKeywords: string;
    const sub = component.search.subscribe(kw => {
      receivedKeywords = kw;
    });
    subs.push(sub);

    const keywords = "Nickname of a player";
    component.searchKeywords = keywords;
    component.actionSearch();
    tick(400);
    expect(receivedKeywords).toBe(keywords);
  }));

  it("should trim the search keywords", fakeAsync(() => {
    let receivedKeywords: string;
    const sub = component.search.subscribe(kw => {
      receivedKeywords = kw;
    });
    subs.push(sub);

    const keywords = " Some Nickname   ";
    component.searchKeywords = keywords;
    component.actionSearch();
    tick(400);
    expect(receivedKeywords).toBe(keywords.trim());
  }));

  it("should blur the search input when triggering the search", () => {
    const input = fixture.debugElement.query(By.css(".search-input"));
    const spy = spyOn(input.nativeElement as HTMLInputElement, "blur");

    component.searchKeywords = "Some keywords";
    component.actionSearch();
    expect(spy).toHaveBeenCalled();
  });

  it("should clear the search keywords", () => {
    component.searchKeywords = "some keywords";
    component.actionClearSearchInput();

    expect(component.searchKeywords).toBeFalsy();
  });

  it("should focus the search input when calling #focus", () => {
    const input = fixture.debugElement.query(By.css(".search-input"));
    const spy = spyOn(input.nativeElement as HTMLInputElement, "focus");

    component.focus();
    expect(spy).toHaveBeenCalled();
  });

  it("should blur the search input when calling #blur", () => {
    const input = fixture.debugElement.query(By.css(".search-input"));
    const spy = spyOn(input.nativeElement as HTMLInputElement, "blur");

    component.blur();
    expect(spy).toHaveBeenCalled();
  });

  afterEach(() => {
    subs.forEach(x => {
      x.unsubscribe();
    });
  });
});
