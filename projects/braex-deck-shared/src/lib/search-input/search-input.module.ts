import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SearchInputComponent } from "./search-input/search-input.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { MatButtonModule } from "@angular/material";
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [SearchInputComponent],
  exports: [SearchInputComponent],
  imports: [CommonModule, FontAwesomeModule, MatButtonModule, FormsModule]
})
export class SearchInputModule {}
