import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  Renderer2,
  ViewChild
} from "@angular/core";
import {
  animate,
  keyframes,
  style,
  transition,
  trigger
} from "@angular/animations";
import { DEFAULT_TOOLTIP_POSITION } from "../tooltip-position";

@Component({
  selector: "app-tooltip",
  templateUrl: "./tooltip.component.html",
  styleUrls: ["./tooltip.component.scss"],
  animations: [
    trigger("tooltip", [
      transition(
        ":enter",
        animate(
          "150ms ease-in",
          keyframes([
            style({ opacity: 0, transform: "scale(0.7)" }),
            style({ opacity: "*", transform: "scale(1)" })
          ])
        )
      )
    ])
  ]
})
export class TooltipComponent implements AfterViewInit {
  @Input()
  tooltip = "";
  @Input()
  x = 0;
  @Input()
  y = 0;
  @Input()
  position = DEFAULT_TOOLTIP_POSITION;
  @ViewChild("wrapper")
  private _tooltipWrapper: ElementRef<HTMLDivElement>;

  constructor(private _renderer: Renderer2) {}

  ngAfterViewInit() {
    this._renderer.setStyle(
      this._tooltipWrapper.nativeElement,
      "top",
      `${this.y + this._elementOffsetYByPosition()}px`
    );

    this._renderer.setStyle(
      this._tooltipWrapper.nativeElement,
      "left",
      `${this.x + this._elementOffsetXByPosition()}px`
    );
  }

  private _elementOffsetXByPosition() {
    if (["bottom", "top"].includes(this.position)) {
      return -(this._tooltipWrapper.nativeElement.offsetWidth / 2);
    } else if (this.position === "left") {
      return -this._tooltipWrapper.nativeElement.offsetWidth;
    }

    return 0;
  }

  private _elementOffsetYByPosition() {
    if (["left", "right"].includes(this.position)) {
      return -(this._tooltipWrapper.nativeElement.offsetHeight / 2);
    } else if (this.position === "top") {
      return -this._tooltipWrapper.nativeElement.offsetHeight;
    }

    return 0;
  }
}
