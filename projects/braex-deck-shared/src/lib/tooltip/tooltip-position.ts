export type TooltipPosition = "top" | "bottom" | "left" | "right";
export const DEFAULT_TOOLTIP_POSITION: TooltipPosition = "bottom";
