import {
  ComponentRef,
  Directive,
  ElementRef,
  HostListener,
  Input
} from "@angular/core";
import { TooltipComponent } from "./tooltip/tooltip.component";
import { Overlay, OverlayRef } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";
import { DEFAULT_TOOLTIP_POSITION } from "./tooltip-position";

/**
 * The offset to the host element in px.
 */
export const OFFSET_TO_ELEMENT = 10;
export const SHOW_TOOLTIP_DELAY = 200;

@Directive({
  selector: "[libTooltip]"
})
export class TooltipDirective {
  private _overlayRef: OverlayRef;
  private _portal: ComponentPortal<TooltipComponent>;
  private _componentRef: ComponentRef<TooltipComponent>;
  private _hovering = false;

  @Input("libTooltip")
  tooltip = "";
  @Input("libTooltipPosition")
  tooltipPosition = DEFAULT_TOOLTIP_POSITION;

  constructor(private _el: ElementRef, private _overlay: Overlay) {
    this._overlayRef = this._overlay.create();
  }

  @HostListener("mouseenter")
  actionShowTooltip() {
    if (
      this._el.nativeElement.hasAttribute("disabled") &&
      this._el.nativeElement.getAttribute("disabled") === "true"
    ) {
      return;
    }

    if (this.tooltip) {
      this._hovering = true;
      setTimeout(() => {
        if (this._hovering) {
          this._createTooltip();
        }
      }, SHOW_TOOLTIP_DELAY);
    }
  }

  private _createTooltip() {
    if (this._portal) {
      this._removeTooltip();
    }

    this._portal = new ComponentPortal(TooltipComponent);
    this._componentRef = this._overlayRef.attach(this._portal);
    this._componentRef.instance.tooltip = this.tooltip;
    this._componentRef.instance.position = this.tooltipPosition;
    this._positionTooltip();
  }

  private _positionTooltip() {
    let x = 0;
    let y = 0;
    const hostBounds = this._el.nativeElement.getBoundingClientRect();

    if (["top", "bottom"].includes(this.tooltipPosition)) {
      x = hostBounds.x + hostBounds.width / 2;
    }

    if (this.tooltipPosition === "bottom") {
      y = hostBounds.y + hostBounds.height + OFFSET_TO_ELEMENT;
    } else if (this.tooltipPosition === "top") {
      y = hostBounds.y - OFFSET_TO_ELEMENT;
    }

    if (["left", "right"].includes(this.tooltipPosition)) {
      y = hostBounds.y + hostBounds.height / 2;
    }

    if (this.tooltipPosition === "left") {
      x = hostBounds.x - OFFSET_TO_ELEMENT;
    } else if (this.tooltipPosition === "right") {
      x = hostBounds.x + hostBounds.width + OFFSET_TO_ELEMENT;
    }

    this._componentRef.instance.x = x;
    this._componentRef.instance.y = y;
  }

  @HostListener("mouseleave")
  @HostListener("click")
  actionHideTooltip() {
    this._hovering = false;
    this._removeTooltip();
  }

  private _removeTooltip() {
    if (!this._portal || !this._componentRef) {
      return;
    }

    this._overlayRef.detach();
    this._portal = undefined;
    this._componentRef = undefined;
  }
}
