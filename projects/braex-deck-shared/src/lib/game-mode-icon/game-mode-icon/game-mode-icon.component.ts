import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit
} from "@angular/core";
import { faRunning } from "@fortawesome/free-solid-svg-icons";
import { PubgGameMode } from "../../ds/game/pubg-game-mode.enum";
import { GameModeUtils } from "../../utils/game-mode-utils";

export type IconColor = "light" | "dark";

class ViewContext {
  faRunning = faRunning;
}

@Component({
  selector: "lib-game-mode-icon",
  templateUrl: "./game-mode-icon.component.html",
  styleUrls: ["./game-mode-icon.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameModeIconComponent implements OnInit {
  readonly viewContext = new ViewContext();
  @Input()
  color: IconColor = "dark";
  @Input()
  gameMode: PubgGameMode;
  private _teamSize: number;

  ngOnInit() {
    this._teamSize = GameModeUtils.teamSize(this.gameMode);
  }

  get teamSize() {
    return this._teamSize;
  }
}
