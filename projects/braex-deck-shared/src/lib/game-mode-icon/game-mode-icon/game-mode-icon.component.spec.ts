import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { GameModeIconComponent } from "./game-mode-icon.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";

describe("GameModeIconComponent", () => {
  let component: GameModeIconComponent;
  let fixture: ComponentFixture<GameModeIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameModeIconComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameModeIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
