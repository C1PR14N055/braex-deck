import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GameModeIconComponent } from "./game-mode-icon/game-mode-icon.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  imports: [CommonModule, FontAwesomeModule],
  declarations: [GameModeIconComponent],
  exports: [GameModeIconComponent]
})
export class GameModeIconModule {}
