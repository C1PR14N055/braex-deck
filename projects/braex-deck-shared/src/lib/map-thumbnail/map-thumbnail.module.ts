import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MapThumbnailPipe } from "./map-thumbnail.pipe";

@NgModule({
  declarations: [MapThumbnailPipe],
  exports: [MapThumbnailPipe],
  imports: [CommonModule]
})
export class MapThumbnailModule {}
