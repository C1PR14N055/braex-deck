import { Pipe, PipeTransform } from "@angular/core";
import { MappingError } from "../error/mapping.error";
import { PubgMap } from "../ds/game/pubg-map.enum";

export const MAP_THUMBNAIL_PATH = "/assets/img/map-thumbnails/";

@Pipe({
  name: "mapThumbnail"
})
export class MapThumbnailPipe implements PipeTransform {
  transform(value: PubgMap, args?: any) {
    switch (value) {
      case PubgMap.SAVAGE_MAIN:
      case PubgMap.DESERT_MAIN:
      case PubgMap.ERANGEL_MAIN:
      case PubgMap.DIHOROTOK_MAIN:
        return `${MAP_THUMBNAIL_PATH}${value}.png`;
      case PubgMap.RANGE_MAIN:
        return `${MAP_THUMBNAIL_PATH}${PubgMap.ERANGEL_MAIN}.png`;
    }

    throw new MappingError(`No thumbnail for the map ${value}`);
  }
}
