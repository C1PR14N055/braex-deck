import { Pipe, PipeTransform } from "@angular/core";

export const MINUTES_STRING = "min.";
export const SECONDS_STRING = "sec.";
export const LONG_TIME_SEPARATOR = ", ";
export const SHORT_TIME_SEPARATOR = ":";
export type DurationArgs = "short" | "long";

@Pipe({
  name: "duration"
})
export class DurationPipe implements PipeTransform {
  transform(value: any, args?: DurationArgs) {
    if (value === undefined || value === null) {
      return "";
    }

    if (args === "short") {
      return this._shortTime(value);
    }

    return this._longTime(value);
  }

  private _longTime(value: number) {
    const result: string[] = [];
    const minutes = this._getMinutes(value);
    result.push(minutes.toString() + " " + MINUTES_STRING);
    const seconds = this._getSeconds(value);
    result.push(seconds.toString() + " " + SECONDS_STRING);

    return result.join(LONG_TIME_SEPARATOR);
  }

  private _shortTime(value: number) {
    const result: string[] = [];
    const minutes = this._getMinutes(value);
    result.push(this._padWithZero(minutes));
    const seconds = this._getSeconds(value);
    result.push(this._padWithZero(seconds));

    return result.join(SHORT_TIME_SEPARATOR);
  }

  private _getMinutes(seconds: number) {
    return Math.floor(seconds / 60);
  }

  private _getSeconds(seconds: number) {
    return Number((seconds - Math.floor(seconds / 60) * 60).toFixed(0));
  }

  private _padWithZero(num: number) {
    if (num < 10) {
      return `0${num}`;
    }

    return num.toString();
  }
}
