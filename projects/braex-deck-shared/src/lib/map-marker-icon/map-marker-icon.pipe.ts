import { Pipe, PipeTransform } from "@angular/core";
import { MappingError } from "../error/mapping.error";
import { MapMarkerType } from "../map-canvas/ds/map-marker-type.enum";

export const MAP_MARKER_DIR = "/assets/img/map-markers/";

@Pipe({
  name: "mapMarkerIcon"
})
export class MapMarkerIconPipe implements PipeTransform {
  transform(value: MapMarkerType, args?: any) {
    switch (value) {
      case MapMarkerType.DEFAULT:
        return `${MAP_MARKER_DIR}map-marker.png`;
      case MapMarkerType.CAR:
        return `${MAP_MARKER_DIR}car-map-marker.png`;
      case MapMarkerType.WEAPON:
        return `${MAP_MARKER_DIR}weapon-map-marker.png`;
      case MapMarkerType.ITEM:
        return `${MAP_MARKER_DIR}item-map-marker.png`;
    }

    throw new MappingError(`No icon found for the MapMarkerType \`${value}\``);
  }
}
