import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MapMarkerIconPipe } from "./map-marker-icon.pipe";

@NgModule({
  declarations: [MapMarkerIconPipe],
  imports: [CommonModule],
  exports: [MapMarkerIconPipe]
})
export class MapMarkerIconModule {}
