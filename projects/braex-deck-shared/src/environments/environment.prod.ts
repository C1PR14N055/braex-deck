export const environment = {
  apiUrl: "https://api.cgs.gg",
  apiEndpoints: {
    singlePlayerStats:
      "/stats/players/{platform}/{nickname}/seasons/current/{gameMode}",
    multiplePlayerStats: "/stats/players/{platform}/seasons/current/{gameMode}"
  },
  webSocketsAuth: {
    login: "user",
    password: "g8S8W4FNuy3k5A9Gm4WGZPv9Q62n2Vdp865znyy9"
  },
  webSockets: {
    drawingSync: {
      brokerUrl: "ws://team.cgs.gg/teamComm/websocket",
      pushPath: "/topic/{sessionId}/paint",
      pullPath: "/topic/{sessionId}/paint"
    }
  }
};
