export const environment = {
  apiUrl: "https://api.cgs.gg",
  apiEndpoints: {
    singlePlayerStats:
      "/stats/players/{platform}/{nickname}/seasons/current/{gameMode}",
    multiplePlayerStats: "/stats/players/{platform}/seasons/current/{gameMode}",
    customGameById: "/match/STEAM/{gameId}",
    anyGameById: "/match/STEAM/{gameId}/all",
    gameRoster: "/match/STEAM/{gameId}/roster",
    searchPlayer: "/match/player/STEAM/{nickname}/all",
    getEventMatches: "/mono-service/api/v1/events/{eventId}/matches",
    listEvents: "/mono-service/api/v1/events/list?platform=TOURNAMENT"
  },
  webSocketsAuth: {
    login: "user",
    password: "g8S8W4FNuy3k5A9Gm4WGZPv9Q62n2Vdp865znyy9"
  },
  webSockets: {
    drawingSync: {
      brokerUrl: "ws://team.cgs.gg/teamComm/websocket",
      pushPath: "/topic/{sessionId}/event",
      pullPath: "/topic/{sessionId}/event"
    },
    positionSync: {
      brokerUrl: "ws://team.cgs.gg/teamComm/websocket",
      pushPath: "/topic/{sessionId}/position",
      pullPath: "/topic/{sessionId}/position"
    }
  },
  dateTimeFormat: "MMM dd, yyyy HH:mm"
};
